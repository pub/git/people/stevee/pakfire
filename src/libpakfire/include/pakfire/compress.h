/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_COMPRESS_H
#define PAKFIRE_COMPRESS_H

#ifdef PAKFIRE_PRIVATE

#include <archive.h>

#include <pakfire/pakfire.h>

// Automatically detect
FILE* pakfire_xfopen(FILE* f, const char* mode);

// XZ
FILE* pakfire_xzfopen(FILE* f, const char* mode);

// ZSTD
FILE* pakfire_zstdfopen(FILE* f, const char* mode);

// Walk

typedef int (*pakfire_walk_callback)
	(struct pakfire* pakfire, struct archive* a, struct archive_entry* e, void* p);
typedef int (*pakfire_walk_filter_callback)
	(struct pakfire* pakfire, struct archive* a, struct archive_entry* e, void* p);

enum pakfire_walk_codes {
	PAKFIRE_WALK_OK    = 0,
	PAKFIRE_WALK_ERROR = 1,

	// After this code has been sent, we will not process any further entries
	PAKFIRE_WALK_END   = -10,

	// Request the next entry (only in filter callback)
	PAKFIRE_WALK_SKIP  = -20,

	// Like PAKFIRE_WALK_OK, but the callback will not be called again
	PAKFIRE_WALK_DONE  = -30,
};

int pakfire_walk(struct pakfire* pakfire, struct archive* archive,
	pakfire_walk_callback callback, pakfire_walk_filter_callback filter_callback, void* p);

// Extract
enum pakfire_extract_flags {
	PAKFIRE_EXTRACT_DRY_RUN         = (1 << 0),
	PAKFIRE_EXTRACT_NO_PROGRESS     = (1 << 1),
	PAKFIRE_EXTRACT_SHOW_THROUGHPUT = (1 << 2),
};

int pakfire_extract(struct pakfire* pakfire, struct archive* archive,
	size_t size, struct pakfire_filelist* filelist, const char* prefix,
	const char* message, pakfire_walk_filter_callback filter_callback,
	int flags);

// Algorithms
enum pakfire_compressions {
	PAKFIRE_COMPRESS_ZSTD,
};

// Compress
enum pakfire_compress_flags {
	PAKFIRE_COMPRESS_NO_PROGRESS     = (1 << 1),
	PAKFIRE_COMPRESS_SHOW_THROUGHPUT = (1 << 2),
};

int pakfire_compress(struct pakfire* pakfire, struct archive* archive,
	struct pakfire_filelist* filelist, const char* message, int flags, int digests);

int pakfire_compress_create_archive(struct pakfire* pakfire, struct archive** archive,
	FILE* f, const enum pakfire_compressions compression, const unsigned int level);

#endif

#endif /* PAKFIRE_COMPRESS_H */
