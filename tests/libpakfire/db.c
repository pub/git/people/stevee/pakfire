/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/archive.h>
#include <pakfire/db.h>
#include <pakfire/package.h>
#include <pakfire/repo.h>
#include <pakfire/util.h>

#include "../testsuite.h"

static int test_open_ro(const struct test* t) {
	struct pakfire_db* db = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_db_open(&db, t->pakfire, PAKFIRE_DB_READONLY));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (db)
		pakfire_db_unref(db);

	return r;
}

static int test_open_rw(const struct test* t) {
	struct pakfire_db* db = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_db_open(&db, t->pakfire, PAKFIRE_DB_READWRITE));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (db)
		pakfire_db_unref(db);

	return r;
}

static int test_check(const struct test* t) {
	struct pakfire_db* db = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_db_open(&db, t->pakfire, PAKFIRE_DB_READWRITE));

	// Perform check
	ASSERT_SUCCESS(pakfire_db_check(db));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (db)
		pakfire_db_unref(db);

	return r;
}

static int test_add_package(const struct test* t) {
	struct pakfire_db* db = NULL;
	struct pakfire_repo* repo = NULL;
	struct pakfire_archive* archive = NULL;
	int r = EXIT_FAILURE;

	ASSERT(repo = pakfire_get_repo(t->pakfire, PAKFIRE_REPO_DUMMY));

	ASSERT_SUCCESS(pakfire_db_open(&db, t->pakfire, PAKFIRE_DB_READWRITE));

	// There must be no packages installed
	ssize_t packages = pakfire_db_packages(db);
	ASSERT(packages == 0);

	const char* path = TEST_SRC_PATH "/data/beep-1.3-2.ip3.x86_64.pfm";

	// Open archive
	ASSERT_SUCCESS(pakfire_archive_open(&archive, t->pakfire, path));
	ASSERT(archive);

	struct pakfire_package* package = NULL;

	// Get package
	ASSERT_SUCCESS(pakfire_archive_make_package(archive, repo, &package));
	ASSERT(package);

	// Try to add the package to the database
	ASSERT_SUCCESS(pakfire_db_add_package(db, package, archive, 1));

	// One package should be installed
	ASSERT(pakfire_db_packages(db) == 1);

	// Remove the package again
	ASSERT_SUCCESS(pakfire_db_remove_package(db, package));

	// No packages should be installed any more
	ASSERT(pakfire_db_packages(db) == 0);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (archive)
		pakfire_archive_unref(archive);
	if (db)
		pakfire_db_unref(db);
	if (package)
		pakfire_package_unref(package);
	if (repo)
		pakfire_repo_unref(repo);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_open_ro);
	testsuite_add_test(test_open_rw);
	testsuite_add_test(test_check);
	testsuite_add_test(test_add_package);

	return testsuite_run(argc, argv);
}
