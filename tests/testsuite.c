/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <linux/limits.h>
#include <stdlib.h>

#include "testsuite.h"

#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/util.h>

#define TMP_TEMPLATE "/tmp/pakfire-test.XXXXXX"

struct testsuite ts;

static int test_run(int i, struct test* t) {
	struct pakfire* p = NULL;

	char root[PATH_MAX] = TEST_ROOTFS "/pakfire-test-XXXXXX";
	int r;

	// Create test root directory
	char* tmp = pakfire_mkdtemp(root);
	if (!tmp) {
		LOG("Could not create temporary directory %s: %m\n", root);
		exit(1);
	}

	LOG("running %s (%s)\n", t->name, root);

	// Create a pakfire instance
	r = pakfire_create(&t->pakfire, root, NULL, TEST_SRC_PATH "/pakfire.conf",
		0, LOG_DEBUG, pakfire_log_stderr, NULL);
	if (r) {
		LOG("ERROR: Could not initialize pakfire: %m\n");
		goto ERROR;
	}

	// Check if the instance was created properly
	if (r == 0 && !t->pakfire) {
		LOG("ERROR: Pakfire was not initialized, but no error was raised: %m\n");
		goto ERROR;
	}

	// Copy command into environment
	r = pakfire_copy_in(t->pakfire, TEST_STUB_COMMAND, "/command");
	if (r) {
		LOG("ERROR: Could not copy command: %m\n");
		goto ERROR;
	}

	// Run test
	r = t->func(t);
	if (r)
		LOG("Test failed with error code: %d\n", r);

ERROR:
	// Release pakfire
	if (t->pakfire) {
		p = pakfire_unref(t->pakfire);

		// Check if Pakfire was actually released
		if (p) {
			LOG("Error: Pakfire instance was not released in test %d\n", i);
			return 1;
		}

		// Reset pointer (just in case)
		t->pakfire = NULL;
	}

	// Cleanup root
	pakfire_rmtree(root, 0);

	return r;
}

int __testsuite_add_test(const char* name, int (*func)(const struct test* t)) {
	// Check if any space is left
	if (ts.num >= MAX_TESTS) {
		LOG("ERROR: We are out of space for tests\n");
		exit(EXIT_FAILURE);
	}

	struct test* test = &ts.tests[ts.num++];

	// Set parameters
	test->name = name;
	test->func = func;

	return 0;
}

static int check_whether_to_run(const struct test* t, const int argc, const char* argv[]) {
	// Run all tests when nothing has been selected
	if (argc < 2)
		return 1;

	// Check if this test has been listed
	for (unsigned int i = 1; i < argc; i++) {
		if (strcmp(t->name, argv[i]) == 0)
			return 1;
	}

	return 0;
}

int testsuite_run(int argc, const char* argv[]) {
	for (unsigned int i = 0; i < ts.num; i++) {
		struct test* test = &ts.tests[i];

		// Skip any tests that should not be run
		if (!check_whether_to_run(test, argc, argv))
			continue;

		// Run the test
		int r = test_run(i, test);
		if (r)
			exit(r);
	}

	return EXIT_SUCCESS;
}

FILE* test_mktemp(char** path) {
	char* p = NULL;

	// Reset path
	if (path)
		*path = NULL;

	int r = asprintf(&p, "%s", TMP_TEMPLATE);
	if (r < 0)
		return NULL;

	int fd = mkstemp(p);
	if (fd < 0)
		return NULL;

	// If we want a named temporary file, we set path
	if (path) {
		*path = p;

	// Otherwise we unlink the path and free p
	} else {
		unlink(p);
		free(p);
	}

	return fdopen(fd, "w+");
}

char* test_mkdtemp() {
	char path[] = TMP_TEMPLATE;

	char* p = mkdtemp(path);
	if (!p)
		return NULL;

	return strdup(path);
}
