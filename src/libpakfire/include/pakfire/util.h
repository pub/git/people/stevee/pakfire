/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_UTIL_H
#define PAKFIRE_UTIL_H

#ifdef PAKFIRE_PRIVATE

#include <pwd.h>
#include <stdio.h>

#include <archive.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include <pakfire/digest.h>
#include <pakfire/pakfire.h>

char* pakfire_unquote_in_place(char* s);

int pakfire_path_exists(const char* path);
int pakfire_path_match(const char* p, const char* s);
time_t pakfire_path_age(const char* path);

int pakfire_path_strip_extension(char* path);

#define pakfire_path_replace_extension(path, extension) \
	__pakfire_path_replace_extension(path, sizeof(path), extension)
int __pakfire_path_replace_extension(char* path, const size_t length, const char* extension);

const char* pakfire_basename(const char* path);
const char* pakfire_dirname(const char* path);

char* pakfire_remove_trailing_newline(char* str);

const char* pakfire_hostname(void);
const char* pakfire_get_home(struct pakfire* pakfire, uid_t uid);

int pakfire_read_file_into_buffer(FILE* f, char** buffer, size_t* len);

#define pakfire_hexlify(digest) __pakfire_hexlify(digest, sizeof(digest))
char* __pakfire_hexlify(const unsigned char* digest, const size_t length);
#define pakfire_unhexlify(dst, src) __pakfire_unhexlify(dst, sizeof(dst), src)
int __pakfire_unhexlify(unsigned char* dst, const size_t l, const char* src);

#define pakfire_path_join(dest, first, second) \
	__pakfire_path_join(dest, sizeof(dest), first, second)
int __pakfire_path_join(char* dest, const size_t length,
	const char* first, const char* second);
int pakfire_path_is_absolute(const char* path);
const char* pakfire_path_abspath(const char* path);
const char* pakfire_path_relpath(const char* root, const char* path);

#define pakfire_path_realpath(dest, path) \
	__pakfire_path_realpath(dest, sizeof(dest), path)
int __pakfire_path_realpath(char* dest, const size_t length, const char* path);

// File stuff

int pakfire_file_write(struct pakfire* pakfire, const char* path,
	uid_t owner, gid_t group, mode_t mode, const char* format, ...);

int pakfire_touch(const char* path, mode_t mode);
int pakfire_mkparentdir(const char* path, mode_t mode);
int pakfire_mkdir(const char* path, mode_t mode);
FILE* pakfire_mktemp(char* path, const mode_t mode);
char* pakfire_mkdtemp(char* path);
int pakfire_rmtree(const char* path, int flags);

char* pakfire_generate_uuid(void);

int pakfire_tty_is_noninteractive(void);

// Archive Stuff

int pakfire_archive_copy_data_from_file(struct pakfire* pakfire,
	struct archive* archive, FILE* f);
int pakfire_archive_copy_data_to_buffer(struct pakfire* pakfire, struct archive* a,
	struct archive_entry* entry, char** data, size_t* data_size);

// JSON Stuff

struct json_object* pakfire_json_parse_from_file(struct pakfire* pakfire, const char* path);
int pakfire_json_add_string(struct pakfire* pakfire,
	struct json_object* json, const char* name, const char* value);
int pakfire_json_add_integer(struct pakfire* pakfire,
	struct json_object* json, const char* name, int value);
int pakfire_json_add_string_array(struct pakfire* pakfire,
	struct json_object* json, const char* name, char** array);

// Resource Limits

#define PAKFIRE_RLIMIT_NOFILE_MAX (512 * 1024)

int pakfire_rlimit_set(struct pakfire* pakfire, int limit);
int pakfire_rlimit_reset_nofile(struct pakfire* pakfire);

// Regex

int pakfire_compile_regex(struct pakfire* pakfire, pcre2_code** regex,
	const char* pattern);

#endif

#endif /* PAKFIRE_UTIL_H */
