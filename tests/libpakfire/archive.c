/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdlib.h>
#include <unistd.h>

#include <pakfire/archive.h>
#include <pakfire/package.h>
#include <pakfire/repo.h>
#include <pakfire/util.h>

#include "../testsuite.h"

#define TEST_PKG1_PATH "data/beep-1.3-2.ip3.x86_64.pfm"

static int test_open(const struct test* t) {
	struct pakfire_archive* archive = NULL;
	struct pakfire_package* package = NULL;
	int r = EXIT_FAILURE;

	// Open the archive
	ASSERT_SUCCESS(pakfire_archive_open(&archive, t->pakfire, TEST_SRC_PATH TEST_PKG1_PATH));

	// Check if path was set correctly
	ASSERT_STRING_EQUALS(pakfire_archive_get_path(archive), TEST_SRC_PATH TEST_PKG1_PATH);

	// Check if format matches
	ASSERT(pakfire_archive_get_format(archive) == 6);

	// Fetch package
	ASSERT_SUCCESS(pakfire_archive_make_package(archive, NULL, &package));
	ASSERT(package);

#if 0
	pakfire_archive_verify_status_t status;

	// Verify the archive
	ASSERT(pakfire_archive_verify(archive, &status, NULL) == 0);
	ASSERT(status == PAKFIRE_ARCHIVE_VERIFY_OK);
#endif

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (archive)
		pakfire_archive_unref(archive);
	if (package)
		pakfire_package_unref(package);

	return r;
}

static int test_open_directory(const struct test* t) {
	struct pakfire_archive* archive = NULL;
	int r = EXIT_FAILURE;

	// Open the archive
	ASSERT_ERRNO(pakfire_archive_open(&archive, t->pakfire, TEST_SRC_PATH), EISDIR);
	ASSERT_NULL(archive);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

static int test_read(const struct test* t) {
	struct pakfire_archive* archive = NULL;
	FILE* f = NULL;
	char data[20000];
	size_t length = 0;
	int r = EXIT_FAILURE;

	const char beep[] = {
		0x7f, 0x45, 0x4c, 0x46, 0x02, 0x01, 0x01, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	};

	// Open the archive
	ASSERT_SUCCESS(pakfire_archive_open(&archive, t->pakfire, TEST_SRC_PATH TEST_PKG1_PATH));

	// Read a file
	ASSERT(f = pakfire_archive_read(archive, "/usr/bin/beep"));

	// Read the entire content
	ASSERT(length = fread(data, 1, sizeof(data), f));

	// Check filesize
	ASSERT(length == 17192);

	// Check the first couple of bytes
	ASSERT_COMPARE(data, beep, sizeof(beep));

	// Try to access a file that does not exist
	ASSERT_ERRNO((pakfire_archive_read(archive, "/does/not/exist") == NULL), ENOENT);

	// Some invalid calls
	ASSERT_ERRNO((pakfire_archive_read(archive, NULL) == NULL), EINVAL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (archive)
		pakfire_archive_unref(archive);
	if (f)
		fclose(f);

	return r;
}

static int test_copy(const struct test* t) {
	struct pakfire_archive* archive = NULL;
	char* path = NULL;
	int r = EXIT_FAILURE;

	// Create a temporary file
	FILE* f = test_mktemp(&path);
	ASSERT(f && path);

	// Open the archive
	ASSERT_SUCCESS(pakfire_archive_open(&archive, t->pakfire, TEST_SRC_PATH TEST_PKG1_PATH));

	// Copy archive
	ASSERT_SUCCESS(pakfire_archive_copy(archive, path));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (archive)
		pakfire_archive_unref(archive);
	if (f)
		fclose(f);
	if (path)
		unlink(path);

	return r;
}

static int test_filelist(const struct test* t) {
	struct pakfire_archive* archive = NULL;
	struct pakfire_filelist* list = NULL;
	int r = EXIT_FAILURE;

	// Open the archive
	ASSERT_SUCCESS(pakfire_archive_open(&archive, t->pakfire, TEST_SRC_PATH TEST_PKG1_PATH));

	// Fetch the filelist
	ASSERT(list = pakfire_archive_get_filelist(archive));

	// This packages has 7 files
	ASSERT(pakfire_filelist_length(list) == 7);

	// Everything passed
	r =  EXIT_SUCCESS;

FAIL:
	if (list)
		pakfire_filelist_unref(list);
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

static int test_extract(const struct test* t) {
	struct pakfire_archive* archive = NULL;
	char path[PATH_MAX];
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_archive_open(&archive, t->pakfire, TEST_SRC_PATH TEST_PKG1_PATH));

	// Extract the archive payload
	ASSERT_SUCCESS(pakfire_archive_extract(archive));

	// Check if test file from the archive exists
	pakfire_path(t->pakfire, path, "%s", "/usr/bin/beep");
	ASSERT_SUCCESS(access(path, F_OK));

	r = EXIT_SUCCESS;

FAIL:
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

static int test_import(const struct test* t) {
	const char* path = TEST_SRC_PATH TEST_PKG1_PATH;
	int r = EXIT_FAILURE;

	struct pakfire_archive* archive = NULL;
	struct pakfire_repo* repo = NULL;
	struct pakfire_package* package = NULL;

	// Open archive
	ASSERT_SUCCESS(pakfire_archive_open(&archive, t->pakfire, path));
	ASSERT(archive);

	// Create a new repository
	ASSERT_SUCCESS(pakfire_repo_create(&repo, t->pakfire, "tmp"));
	ASSERT(repo);

	// Add the package to the repository
	ASSERT_SUCCESS(pakfire_repo_add_archive(repo, archive, &package));
	ASSERT(package);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (repo)
		pakfire_repo_unref(repo);
	if (package)
		pakfire_package_unref(package);
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_open);
	testsuite_add_test(test_open_directory);
	testsuite_add_test(test_read);
	testsuite_add_test(test_copy);
	testsuite_add_test(test_filelist);
	testsuite_add_test(test_extract);
	testsuite_add_test(test_import);

	return testsuite_run(argc, argv);
}
