/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <Python.h>

#include <pakfire/progressbar.h>

#include "progressbar.h"

static PyObject* Progressbar_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	ProgressbarObject* self = (ProgressbarObject *)type->tp_alloc(type, 0);
	if (self) {
		self->progressbar = NULL;
	}

	return (PyObject *)self;
}

static void Progressbar_dealloc(ProgressbarObject* self) {
	if (self->progressbar)
		pakfire_progressbar_unref(self->progressbar);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static int Progressbar_init(ProgressbarObject* self, PyObject* args, PyObject* kwds) {
	int r = pakfire_progressbar_create(&self->progressbar, NULL);
	if (r)
		return -1;

	return 0;
}

static PyObject* Progressbar_start(ProgressbarObject* self, PyObject* args) {
	unsigned long value = 0;

	if (!PyArg_ParseTuple(args, "k", &value))
		return NULL;

	int r = pakfire_progressbar_start(self->progressbar, value);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_update(ProgressbarObject* self, PyObject* args) {
	unsigned long value = 0;

	if (!PyArg_ParseTuple(args, "k", &value))
		return NULL;

	int r = pakfire_progressbar_update(self->progressbar, value);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_increment(ProgressbarObject* self, PyObject* args) {
	unsigned long value = 1;

	if (!PyArg_ParseTuple(args, "|k", &value))
		return NULL;

	int r = pakfire_progressbar_increment(self->progressbar, value);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_finish(ProgressbarObject* self) {
	int r = pakfire_progressbar_finish(self->progressbar);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_reset(ProgressbarObject* self) {
	int r = pakfire_progressbar_reset(self->progressbar);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_set_max(ProgressbarObject* self, PyObject* args) {
	unsigned long value = 0;

	if (!PyArg_ParseTuple(args, "k", &value))
		return NULL;

	pakfire_progressbar_set_max(self->progressbar, value);
	Py_RETURN_NONE;
}

static PyObject* Progressbar_add_string(ProgressbarObject* self, PyObject* args) {
	const char* string = NULL;

	if (!PyArg_ParseTuple(args, "s", &string))
		return NULL;

	int r = pakfire_progressbar_add_string(self->progressbar, "%s", string);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_add_counter(ProgressbarObject* self) {
	int r = pakfire_progressbar_add_counter(self->progressbar);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_add_percentage(ProgressbarObject* self) {
	int r = pakfire_progressbar_add_percentage(self->progressbar);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_add_bar(ProgressbarObject* self) {
	int r = pakfire_progressbar_add_bar(self->progressbar);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_add_timer(ProgressbarObject* self) {
	int r = pakfire_progressbar_add_timer(self->progressbar);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_add_bytes_transferred(ProgressbarObject* self) {
	int r = pakfire_progressbar_add_bytes_transferred(self->progressbar);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_add_eta(ProgressbarObject* self) {
	int r = pakfire_progressbar_add_eta(self->progressbar);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_add_transfer_speed(ProgressbarObject* self) {
	int r = pakfire_progressbar_add_transfer_speed(self->progressbar);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Progressbar_enter(ProgressbarObject* self) {
	int r = pakfire_progressbar_start(self->progressbar, 0);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	// Return self
	Py_INCREF(self);
	return (PyObject*)self;
}

static PyObject* Progressbar_exit(ProgressbarObject* self, PyObject* args) {
	int r = pakfire_progressbar_finish(self->progressbar);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static struct PyMethodDef Progressbar_methods[] = {
	{
		"start",
		(PyCFunction)Progressbar_start,
		METH_VARARGS,
		NULL,
	},
	{
		"update",
		(PyCFunction)Progressbar_update,
		METH_VARARGS,
		NULL,
	},
	{
		"increment",
		(PyCFunction)Progressbar_increment,
		METH_VARARGS,
		NULL,
	},
	{
		"finish",
		(PyCFunction)Progressbar_finish,
		METH_NOARGS,
		NULL,
	},
	{
		"reset",
		(PyCFunction)Progressbar_reset,
		METH_NOARGS,
		NULL,
	},
	{
		"set_max",
		(PyCFunction)Progressbar_set_max,
		METH_VARARGS,
		NULL,
	},
	{
		"add_string",
		(PyCFunction)Progressbar_add_string,
		METH_VARARGS,
		NULL,
	},
	{
		"add_counter",
		(PyCFunction)Progressbar_add_counter,
		METH_NOARGS,
		NULL,
	},
	{
		"add_percentage",
		(PyCFunction)Progressbar_add_percentage,
		METH_NOARGS,
		NULL,
	},
	{
		"add_bar",
		(PyCFunction)Progressbar_add_bar,
		METH_NOARGS,
		NULL,
	},
	{
		"add_timer",
		(PyCFunction)Progressbar_add_timer,
		METH_NOARGS,
		NULL,
	},
	{
		"add_bytes_transferred",
		(PyCFunction)Progressbar_add_bytes_transferred,
		METH_NOARGS,
		NULL,
	},
	{
		"add_eta",
		(PyCFunction)Progressbar_add_eta,
		METH_NOARGS,
		NULL,
	},
	{
		"add_transfer_speed",
		(PyCFunction)Progressbar_add_transfer_speed,
		METH_NOARGS,
		NULL,
	},
	{
		"__enter__",
		(PyCFunction)Progressbar_enter,
		METH_NOARGS,
		NULL,
	},
	{
		"__exit__",
		(PyCFunction)Progressbar_exit,
		METH_VARARGS,
		NULL,
	},
	{ NULL },
};

PyTypeObject ProgressbarType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	tp_name:            "_pakfire.Progressbar",
	tp_basicsize:       sizeof(ProgressbarObject),
	tp_flags:           Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	tp_new:             (newfunc)Progressbar_new,
	tp_dealloc:         (destructor)Progressbar_dealloc,
	tp_init:            (initproc)Progressbar_init,
	tp_doc:             "Progressbar Object",
	tp_methods:         Progressbar_methods,
};
