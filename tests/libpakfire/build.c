/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <unistd.h>

#include <pakfire/build.h>

#include "../testsuite.h"

static int test_create_and_free(const struct test* t) {
	struct pakfire_build* build = NULL;

	// Create a new build
	ASSERT_SUCCESS(pakfire_build_create(&build, t->pakfire, NULL, 0));

	// Check if build actually got allocated
	ASSERT(build);

	// Free the build
	ASSERT(pakfire_build_unref(build) == NULL);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_create_with_invalid_ids(const struct test* t) {
	struct pakfire_build* build = NULL;

	// Try to create a build with an invalid UUID
	ASSERT_ERRNO(pakfire_build_create(&build, t->pakfire, "ABC", 0), EINVAL);

	// Try to create a build with an empty UUID
	ASSERT_ERRNO(pakfire_build_create(&build, t->pakfire, "", 0), EINVAL);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create_and_free);
	testsuite_add_test(test_create_with_invalid_ids);

	return testsuite_run(argc, argv);
}
