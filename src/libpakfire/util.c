/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <ftw.h>
#include <libgen.h>
#include <math.h>
#include <pwd.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <archive.h>
#include <archive_entry.h>
#include <json.h>
#include <uuid/uuid.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include <pakfire/constants.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define BUFFER_SIZE 64 * 1024
#define NSEC_PER_SEC 1000000000

char* pakfire_unquote_in_place(char* s) {
	if (!s || !*s)
		return s;

	// Is the first character a quote?
	if (*s != '"')
		return s;

	// Find the end of value
	size_t l = strlen(s);
	if (!l)
		return s;

	// Is the last character a quote?
	if (s[l - 1] != '"')
		return s;

	// The string seems to be in quotes; remove them
	s[l - 1] = '\0';
	s++;

	return s;
}

int __pakfire_path_join(char* dest, const size_t length,
		const char* first, const char* second) {
	if (!first)
		return __pakfire_string_format(dest, length, "%s", second);

	if (!second)
		return __pakfire_string_format(dest, length, "%s", first);

	// Remove leading slashes from second argument
	while (*second == '/')
		second++;

	return __pakfire_string_format(dest, length, "%s/%s", first, second);
}

int pakfire_path_is_absolute(const char* path) {
	if (!path) {
		errno = EINVAL;
		return -1;
	}

	return (*path == '/');
}

const char* pakfire_path_abspath(const char* path) {
	static char buffer[PATH_MAX];
	int r;

	// Check input
	if (!path) {
		errno = EINVAL;
		return NULL;
	}

	// Return path if already absolute
	if (*path == '/')
		return path;

	// Otherwise prepend a /
	r = pakfire_string_format(buffer, "/%s", path);
	if (r)
		return NULL;

	// Return a reference to the buffer
	return buffer;
}

const char* pakfire_path_relpath(const char* root, const char* path) {
	// Check inputs
	if (!root || !path) {
		errno = EINVAL;
		return NULL;
	}

	if (pakfire_string_startswith(path, root))
		return path + strlen(root);

	return NULL;
}

int __pakfire_path_realpath(char* dest, const size_t length, const char* path) {
	char buffer[PATH_MAX];

	// Resolve path to its absolute path and store it in buffer
	char* p = realpath(path, buffer);
	if (!p)
		return 1;

	return __pakfire_string_set(dest, length, buffer);
}

int pakfire_path_exists(const char* path) {
	return !access(path, F_OK);
}

/*
	This function will handle any stars in the pattern matching

	stars will be set to non-zero if we encountered a double star
*/
static int __pakfire_path_match_star(const char* p, const char* s) {
	unsigned int stars = 0;
	int r;

	// Count how many stars we found
	while (p && *p == '*') {
		// Consume the star
		p++;

		// Increment the counter
		stars++;
	}

	// We do not allow more than two stars
	if (stars > 2) {
		errno = EINVAL;
		return -1;
	}

	// Consume the string...
	for (; *s; s++) {
		switch (*s) {
			// Found slash!
			case '/':
				if (stars == 1)
					return pakfire_path_match(p, s);

				// Otherwise fall through

			// Read as many characters as possible
			default:
				r = pakfire_path_match(p, s);
				if (r)
					return r;
		}
	}

	// The pattern has not entirely been consumed
	if (*p)
		return 0;

	// If we reached the end of the string, * has consumed everything
	return 1;
}

/*
	This is our custom implementation of fnmatch()
	which supports ** and stops at slashes.
*/
int pakfire_path_match(const char* p, const char* s) {
	// Empty pattern matches nothing
	if (!*p)
		return 0;

	// Consume the pattern and string...
	for (; *p; p++, s++) {
		switch (*p) {
			// Match any character
			case '?':
				// No match if we reached the end
				if (!*s)
					return 0;

				continue;

			// Match multiple characters
			case '*':
				return __pakfire_path_match_star(p, s);

			// All other characters
			default:
				// Character matches
				if (*s == *p)
					continue;

				// No match
				return 0;
		}
	}

	// There are unmatched characters left
	if (*s)
		return 0;

	// We reached the end of the string and all characters matched
	return 1;
}

time_t pakfire_path_age(const char* path) {
	struct stat st;

	int r = stat(path, &st);
	if (r == 0) {
		// Get current timestamp
		time_t now = time(NULL);

		// Return the difference since the file has been created and now
		return now - st.st_ctime;
	}

	return -1;
}

int pakfire_path_strip_extension(char* path) {
	char* ext = strrchr(path, '.');

	// If . could not be found, we return an error.
	if (!ext)
		return 1;

	// Otherwise, we will terminate the string
	*ext = '\0';

	return 0;
}

int __pakfire_path_replace_extension(char* path, const size_t length, const char* extension) {
	char buffer[PATH_MAX];
	int r;

	// Copy path to buffer
	r = pakfire_string_set(buffer, path);
	if (r)
		return r;

	// Strip any old extension
	r = pakfire_path_strip_extension(buffer);
	if (r)
		return r;

	// Compose the new string
	return __pakfire_string_format(path, length, "%s.%s", buffer, extension);
}

int pakfire_file_write(struct pakfire* pakfire, const char* path,
		uid_t owner, gid_t group, mode_t mode, const char* format, ...) {
	va_list args;
	int r = 1;

	// Open the destination file
	FILE* f = fopen(path, "w");
	if (!f)
		goto ERROR;

	// Fetch file-descriptor
	int fd = fileno(f);

	// Set owner/group
	if (owner && group) {
		r = fchown(fd, owner, group);
		if (r)
			goto ERROR;
	}

	// Set mode
	if (mode) {
		r = fchmod(fd, mode);
		if (r)
			goto ERROR;
	}

	// Write data
	va_start(args, format);
	r = vfprintf(f, format, args);
	va_end(args);

	// Check for writing error
	if (r < 0)
		goto ERROR;

	// Close the file
	return fclose(f);

ERROR:
	if (f)
		fclose(f);

	return r;
}

const char* pakfire_basename(const char* path) {
	static char buffer[PATH_MAX];
	int r;

	// Copy string
	r = pakfire_string_set(buffer, path);
	if (r)
		return NULL;

	return basename(buffer);
}

const char* pakfire_dirname(const char* path) {
	static char buffer[PATH_MAX];
	int r;

	// Copy string
	r = pakfire_string_set(buffer, path);
	if (r)
		return NULL;

	return dirname(buffer);
}

char* pakfire_remove_trailing_newline(char* str) {
	ssize_t pos = strlen(str) - 1;

	if (str[pos] == '\n')
		str[pos] = '\0';

	return str;
}

static char __hostname[256];

const char* pakfire_hostname() {
	int r = gethostname(__hostname, sizeof(__hostname));
	if (r)
		return NULL;

	return __hostname;
}

const char* pakfire_get_home(struct pakfire* pakfire, uid_t uid) {
	struct passwd* entry = getpwuid(uid);
	if (!entry)
		return NULL;

	return entry->pw_dir;
}

int pakfire_read_file_into_buffer(FILE* f, char** buffer, size_t* len) {
	if (!f)
		return -EBADF;

	int r = fseek(f, 0, SEEK_END);
	if (r)
		return r;

	// Save length of file
	*len = ftell(f);

	// Go back to the start
	r = fseek(f, 0, SEEK_SET);
	if (r)
		return r;

	// Allocate buffer
	*buffer = malloc((sizeof(**buffer) * *len) + 1);
	if (!*buffer)
		return -ENOMEM;

	// Read content
	fread(*buffer, *len, sizeof(**buffer), f);

	// Check we encountered any errors
	r = ferror(f);
	if (r) {
		free(*buffer);
		return r;
	}

	// Terminate the buffer
	(*buffer)[*len] = '\0';

	return 0;
}

char* pakfire_generate_uuid() {
	uuid_t uuid;

	// Generate a new random value
	uuid_generate_random(uuid);

	char* ret = malloc(UUID_STR_LEN + 1);
	if (!ret)
		return NULL;

	// Convert it to string
	uuid_unparse_lower(uuid, ret);

	// Terminate string
	ret[UUID_STR_LEN] = '\0';

	return ret;
}

int pakfire_tty_is_noninteractive(void) {
	const int fds[] = {
		STDIN_FILENO,
		STDOUT_FILENO,
		STDERR_FILENO,
		-1,
	};

	for (const int* fd = fds; *fd >= 0; fd++) {
		if (!isatty(*fd))
			return 1;
	}

	return 0;
}

char* __pakfire_hexlify(const unsigned char* digest, const size_t length) {
	const char* hexdigits = "0123456789abcdef";

	char* s = malloc((length * 2) + 1);
	if (!s)
		return NULL;

	for (unsigned int i = 0, j = 0; i < length; i++) {
		char b = digest[i];

		s[j++] = hexdigits[(b >> 4) & 0xf];
		s[j++] = hexdigits[(b & 0x0f)];
	}

	// Terminate result
	s[length * 2] = '\0';

	return s;
}

static int parse_nibble(char nibble) {
	// Handle digits
	if (nibble >= '0' && nibble <= '9')
		return nibble - '0';

	// Handle lowercase letters
	if (nibble >= 'a' && nibble <= 'f')
		return 10 + nibble - 'a';

	// Handle uppercase letters
	if (nibble >= 'A' && nibble <= 'F')
		return 10 + nibble - 'A';

	// Error
	return -1;
}

int __pakfire_unhexlify(unsigned char* dst, const size_t l, const char* src) {
	if (!src) {
		errno = EINVAL;
		return 1;
	}

	// Determine input length
	const size_t length = strlen(src);

	for (unsigned int i = 0, j = 0; i < length && j < l; i += 2, j++) {
		int high = parse_nibble(src[i]);
		int low  = parse_nibble(src[i+1]);

		// Check for invalid input
		if (high < 0 || low < 0) {
			errno = EINVAL;
			return 1;
		}

		// Store result
		dst[j] = high << 4 | low;
	}

	return 0;
}

int pakfire_touch(const char* path, mode_t mode) {
	int r = 1;

	FILE* f = fopen(path, "w");
	if (!f)
		goto ERROR;

	// Set the requested mode
	if (mode) {
		r = fchmod(fileno(f), mode);
		if (r)
			goto ERROR;
	}

	return fclose(f);

ERROR:
	if (f)
		fclose(f);

	return r;
}

int pakfire_mkparentdir(const char* path, mode_t mode) {
	const char* dirname = pakfire_dirname(path);
	if (!dirname)
		return 1;

	return pakfire_mkdir(dirname, mode);
}

static int pakfire_try_mkdir(const char* path, const mode_t mode) {
	struct stat st;
	int r;

	// Call stat() on path
	r = stat(path, &st);

	// Path exists, but is it a directory?
	if (r == 0) {
		if (S_ISDIR(st.st_mode))
			return 0;

		// Not a directory
		errno = ENOTDIR;
		return 1;

	// Path does not exist
	} else if (r && errno == ENOENT) {
		// Try to create it
		r = mkdir(path, mode);
		if (r)
			return r;

	// Raise any other errors
	} else {
		return r;
	}

	// Success
	return 0;
}

int pakfire_mkdir(const char* path, mode_t mode) {
	char buffer[PATH_MAX];
	int r;

	// Copy dirname into buffer
	r = pakfire_string_set(buffer, path);
	if (r)
		return r;

	// Recursively create all directories
	for (char* p = buffer + 1; *p; p++) {
		if (*p == '/') {
			// Cut off at slash
			*p = '\0';

			// Try to create partition directory
			r = pakfire_try_mkdir(buffer, mode);
			if (r)
				return r;

			// Reset slash
			*p = '/';
		}
	}

	// Create final directory
	return pakfire_try_mkdir(path, mode);
}

FILE* pakfire_mktemp(char* path, const mode_t mode) {
	int r = pakfire_mkparentdir(path, 0755);
	if (r)
		return NULL;

	// Create a temporary result file
	int fd = mkostemp(path, O_CLOEXEC);
	if (fd < 0)
		return NULL;

	// Set desired mode
	if (mode) {
		r = fchmod(fd, mode);
		if (r)
			return NULL;
	}

	// Re-open as file handle
	return fdopen(fd, "w+");
}

char* pakfire_mkdtemp(char* path) {
	int r = pakfire_mkparentdir(path, 0755);
	if (r)
		return NULL;

	return mkdtemp(path);
}

static int _unlink(const char* path, const struct stat* stat,
		const int type, struct FTW* ftwbuf) {
	// Delete directories using rmdir()
	if (type & FTW_D)
		return rmdir(path);

	// unlink() everything else
	return unlink(path);
}

int pakfire_rmtree(const char* path, int flags) {
	// Save errno
	int saved_errno = errno;

	// Walk through the entire tree and unlink everything
	int r = nftw(path, _unlink, 64, flags|FTW_DEPTH|FTW_PHYS);

	// Ignore if path didn't exist
	if (r < 0 && errno == ENOENT)
		r = 0;

	// Restore errno
	errno = saved_errno;

	return r;
}

// Archive Stuff

int pakfire_archive_copy_data_from_file(struct pakfire* pakfire,
		struct archive* archive, FILE* f) {
	char buffer[BUFFER_SIZE];

	size_t bytes_read = 0;
	ssize_t bytes_written = 0;

	// Read file from the very beginning - also allows calling this multiple times
	rewind(f);

	// Loop through the entire length of the file
	while (!feof(f)) {
		// Read a block from file
		bytes_read = fread(buffer, 1, sizeof(buffer), f);

		// Check if any error occured
		if (ferror(f)) {
			ERROR(pakfire, "Read error: %m\n");
			return 1;
		}

		// Write the block to the archive
		bytes_written = archive_write_data(archive, buffer, bytes_read);
		if (bytes_written < 0) {
			ERROR(pakfire, "Write error: %s\n", archive_error_string(archive));
			return 1;
		}
	}

	return 0;
}

int pakfire_archive_copy_data_to_buffer(struct pakfire* pakfire, struct archive* a,
		struct archive_entry* entry, char** data, size_t* data_size) {
	*data = NULL;
	*data_size = 0;

	size_t required_size = archive_entry_size(entry);
	if (!required_size)
		return 0;

	// Allocate a block of the required size
	*data = calloc(1, required_size + 1);
	if (!*data)
		return ENOMEM;

	ssize_t bytes_read = archive_read_data(a, *data, required_size);
	if (bytes_read < 0) {
		ERROR(pakfire, "Could not read from archive: %s\n", archive_error_string(a));
		free(*data);
		return 1;
	}

	*data_size = bytes_read;

	return 0;
}

// JSON Stuff

static struct json_object* pakfire_json_parse(struct pakfire* pakfire, FILE* f) {
	struct json_tokener* tokener = NULL;
	struct json_object* json = NULL;
	char* buffer = NULL;
	size_t length;

	// Read everything into memory
	int r = pakfire_read_file_into_buffer(f, &buffer, &length);
	if (r)
		goto ERROR;

	// Create tokener
	tokener = json_tokener_new();
	if (!tokener) {
		ERROR(pakfire, "Could not allocate JSON tokener: %m\n");
		goto ERROR;
	}

	// Parse JSON from path
	json = json_tokener_parse_ex(tokener, buffer, length);
	if (!json) {
		enum json_tokener_error error = json_tokener_get_error(tokener);

		ERROR(pakfire, "JSON parsing error: %s\n", json_tokener_error_desc(error));
		goto ERROR;
	}

	// Log what we have parsed
	DEBUG(pakfire, "Parsed JSON:\n%s\n",
		json_object_to_json_string_ext(json,
			JSON_C_TO_STRING_SPACED | JSON_C_TO_STRING_PRETTY)
	);

ERROR:
	if (buffer)
		free(buffer);

	if (tokener)
		json_tokener_free(tokener);

	return json;
}

struct json_object* pakfire_json_parse_from_file(struct pakfire* pakfire, const char* path) {
	FILE* f = fopen(path, "r");
	if (!f)
		return NULL;

	struct json_object* json = pakfire_json_parse(pakfire, f);
	fclose(f);

	return json;
}

int pakfire_json_add_string(struct pakfire* pakfire, struct json_object* json,
		const char* name, const char* value) {
	// No string? Nothing to do
	if (!value)
		return 0;

	// Convert string to JSON object
	struct json_object* object = json_object_new_string(value);
	if (!object)
		return 1;

	// Add the object
	return json_object_object_add(json, name, object);
}

int pakfire_json_add_string_array(struct pakfire* pakfire, struct json_object* json,
		const char* name, char** array) {
	int r = 1;

	// Allocate a new array
	struct json_object* object = json_object_new_array();
	if (!object)
		goto ERROR;

	// Add all items on list to the array
	for (char** item = array; *item; item++) {
		r = json_object_array_add(object, json_object_new_string(*item));
		if (r)
			goto ERROR;
	}

	// Add object
	r = json_object_object_add(json, name, object);
	if (r)
		goto ERROR;

ERROR:
	// Free JSON object on error
	if (r)
		json_object_put(object);

	return r;
}

int pakfire_json_add_integer(struct pakfire* pakfire, struct json_object* json,
		const char* name, int value) {
	// Convert integer to JSON object
	struct json_object* object = json_object_new_int64(value);
	if (!object)
		return 1;

	// Add the object
	return json_object_object_add(json, name, object);
}

// Resource Limits

int pakfire_rlimit_set(struct pakfire* pakfire, int limit) {
	struct rlimit rl;

	// Sanity check
	if (limit < 3) {
		errno = EINVAL;
		return 1;
	}

	// Fetch current configuration
	if (getrlimit(RLIMIT_NOFILE, &rl) < 0) {
		ERROR(pakfire, "Could not read RLIMIT_NOFILE: %m\n");
		return 1;
	}

	// Do not attempt to set higher than maximum
	if ((long unsigned int)limit > rl.rlim_max)
		limit = rl.rlim_max;

	rl.rlim_cur = limit;

	// Set the new limit
	if (setrlimit(RLIMIT_NOFILE, &rl) < 0) {
		ERROR(pakfire, "Could not set RLIMIT_NOFILE to %lu: %m\n", rl.rlim_cur);
		return 1;
	}

	DEBUG(pakfire, "RLIMIT_NOFILE set to %d\n", limit);

	return 0;
}

/*
	Resets RLIMIT_NOFILE to FD_SETSIZE (e.g. 1024)
	for compatibility with software that uses select()
*/
int pakfire_rlimit_reset_nofile(struct pakfire* pakfire) {
	return pakfire_rlimit_set(pakfire, FD_SETSIZE);
}

// Regex

int pakfire_compile_regex(struct pakfire* pakfire, pcre2_code** regex, const char* pattern) {
	int pcre2_errno;
	size_t pcre2_offset;
	PCRE2_UCHAR errmsg[256];

	// Compile the regular expression
	*regex = pcre2_compile((PCRE2_SPTR)pattern, PCRE2_ZERO_TERMINATED, 0,
		&pcre2_errno, &pcre2_offset, NULL);

	if (!*regex) {
		pcre2_get_error_message(pcre2_errno, errmsg, sizeof(errmsg));
		ERROR(pakfire, "PCRE2 compilation failed for '%s' at offset %zu: %s\n",
			pattern, pcre2_offset, errmsg);
		return 1;
	}

	// Enable JIT
	pcre2_errno = pcre2_jit_compile(*regex, PCRE2_JIT_COMPLETE);
	if (pcre2_errno) {
		pcre2_get_error_message(pcre2_errno, errmsg, sizeof(errmsg));
		ERROR(pakfire, "Enabling JIT on '%s' failed: %s\n", pattern, errmsg);
		return 1;
	}

	return 0;
}
