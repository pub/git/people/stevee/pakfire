/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <linux/capability.h>
#include <linux/sched.h>
#include <sys/wait.h>
#include <linux/wait.h>
#include <sched.h>
#include <signal.h>
#include <stdlib.h>
#include <syscall.h>
#include <sys/capability.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/mount.h>
#include <sys/personality.h>
#include <sys/prctl.h>
#include <sys/resource.h>
#include <sys/timerfd.h>
#include <sys/types.h>
#include <sys/wait.h>

// libseccomp
#include <seccomp.h>

// libuuid
#include <uuid.h>

#include <pakfire/arch.h>
#include <pakfire/cgroup.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/mount.h>
#include <pakfire/pakfire.h>
#include <pakfire/private.h>
#include <pakfire/pwd.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define BUFFER_SIZE      1024 * 64
#define ENVIRON_SIZE     128
#define EPOLL_MAX_EVENTS 2
#define MAX_MOUNTPOINTS  8

// The default environment that will be set for every command
static const struct environ {
	const char* key;
	const char* val;
} ENV[] = {
	{ "HOME", "/root" },
	{ "LANG", "C.utf-8" },
	{ "PATH", "/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin", },
	{ "TERM", "vt100" },

	// Tell everything that it is running inside a Pakfire container
	{ "container", "pakfire" },
	{ NULL, NULL },
};

struct pakfire_jail_mountpoint {
	char source[PATH_MAX];
	char target[PATH_MAX];
	int flags;
};

struct pakfire_jail {
	struct pakfire* pakfire;
	int nrefs;

	// A unique ID for each jail
	uuid_t uuid;
	char __uuid[UUID_STR_LEN];

	// Resource Limits
	int nice;

	// Timeout
	struct itimerspec timeout;

	// CGroup
	struct pakfire_cgroup* cgroup;

	// Environment
	char* env[ENVIRON_SIZE];

	// Mountpoints
	struct pakfire_jail_mountpoint mountpoints[MAX_MOUNTPOINTS];
	unsigned int num_mountpoints;
};

struct pakfire_log_buffer {
	char data[BUFFER_SIZE];
	size_t used;
};

struct pakfire_jail_exec {
	int flags;

	// PID (of the child)
	pid_t pid;
	int pidfd;

	// Process status (from waitid)
	siginfo_t status;

	// FD to notify the client that the parent has finished initialization
	int completed_fd;

	// Log pipes
	struct pakfire_jail_pipes {
		int stdin[2];
		int stdout[2];
		int stderr[2];

		// Logging
		int log_INFO[2];
		int log_ERROR[2];
		int log_DEBUG[2];
	} pipes;

	// Communicate
	struct pakfire_jail_communicate {
		pakfire_jail_communicate_in  in;
		pakfire_jail_communicate_out out;
		void* data;
	} communicate;

	// Log buffers
	struct pakfire_jail_buffers {
		struct pakfire_log_buffer stdout;
		struct pakfire_log_buffer stderr;

		// Logging
		struct pakfire_log_buffer log_INFO;
		struct pakfire_log_buffer log_ERROR;
		struct pakfire_log_buffer log_DEBUG;
	} buffers;

	struct pakfire_cgroup* cgroup;
	struct pakfire_cgroup_stats cgroup_stats;
};

static int clone3(struct clone_args* args, size_t size) {
	return syscall(__NR_clone3, args, size);
}

static int pidfd_send_signal(int pidfd, int sig, siginfo_t* info, unsigned int flags) {
	return syscall(SYS_pidfd_send_signal, pidfd, sig, info, flags);
}

static int pakfire_jail_exec_has_flag(
		const struct pakfire_jail_exec* ctx, const enum pakfire_jail_exec_flags flag) {
	return ctx->flags & flag;
}

static void pakfire_jail_free(struct pakfire_jail* jail) {
	DEBUG(jail->pakfire, "Freeing jail at %p\n", jail);

	// Free environment
	for (unsigned int i = 0; jail->env[i]; i++)
		free(jail->env[i]);

	if (jail->cgroup)
		pakfire_cgroup_unref(jail->cgroup);

	pakfire_unref(jail->pakfire);
	free(jail);
}

/*
	Passes any log messages on to the default pakfire log callback
*/
static int pakfire_jail_default_log_callback(struct pakfire* pakfire, void* data,
		int priority, const char* line, size_t length) {
	switch (priority) {
		case LOG_INFO:
			INFO(pakfire, "%s", line);
			break;

		case LOG_ERR:
			ERROR(pakfire, "%s", line);
			break;

#ifdef ENABLE_DEBUG
		case LOG_DEBUG:
			DEBUG(pakfire, "%s", line);
			break;
#endif
	}

	return 0;
}

static const char* pakfire_jail_uuid(struct pakfire_jail* jail) {
	if (!*jail->__uuid)
		uuid_unparse_lower(jail->uuid, jail->__uuid);

	return jail->__uuid;
}

static int pakfire_jail_setup_interactive_env(struct pakfire_jail* jail) {
	// Set PS1
	int r = pakfire_jail_set_env(jail, "PS1", "pakfire-jail \\w> ");
	if (r)
		return r;

	// Copy TERM
	char* TERM = secure_getenv("TERM");
	if (TERM) {
		r = pakfire_jail_set_env(jail, "TERM", TERM);
		if (r)
			return r;
	}

	// Copy LANG
	char* LANG = secure_getenv("LANG");
	if (LANG) {
		r = pakfire_jail_set_env(jail, "LANG", LANG);
		if (r)
			return r;
	}

	return 0;
}

PAKFIRE_EXPORT int pakfire_jail_create(struct pakfire_jail** jail, struct pakfire* pakfire) {
	int r;

	const char* arch = pakfire_get_arch(pakfire);

	// Allocate a new jail
	struct pakfire_jail* j = calloc(1, sizeof(*j));
	if (!j)
		return 1;

	// Reference Pakfire
	j->pakfire = pakfire_ref(pakfire);

	// Initialize reference counter
	j->nrefs = 1;

	// Generate a random UUID
	uuid_generate_random(j->uuid);

	DEBUG(j->pakfire, "Allocated new jail at %p\n", j);

	// Set default environment
	for (const struct environ* e = ENV; e->key; e++) {
		r = pakfire_jail_set_env(j, e->key, e->val);
		if (r)
			goto ERROR;
	}

	// Enable all CPU features that CPU has to offer
	if (!pakfire_arch_supported_by_host(arch)) {
		r = pakfire_jail_set_env(j, "QEMU_CPU", "max");
		if (r)
			goto ERROR;
	}

	// Set container UUID
	r = pakfire_jail_set_env(j, "container_uuid", pakfire_jail_uuid(j));
	if (r)
		goto ERROR;

	// Disable systemctl to talk to systemd
	if (!pakfire_on_root(j->pakfire)) {
		r = pakfire_jail_set_env(j, "SYSTEMD_OFFLINE", "1");
		if (r)
			goto ERROR;
	}

	// Done
	*jail = j;
	return 0;

ERROR:
	pakfire_jail_free(j);

	return r;
}

PAKFIRE_EXPORT struct pakfire_jail* pakfire_jail_ref(struct pakfire_jail* jail) {
	++jail->nrefs;

	return jail;
}

PAKFIRE_EXPORT struct pakfire_jail* pakfire_jail_unref(struct pakfire_jail* jail) {
	if (--jail->nrefs > 0)
		return jail;

	pakfire_jail_free(jail);
	return NULL;
}

// Resource Limits

PAKFIRE_EXPORT int pakfire_jail_nice(struct pakfire_jail* jail, int nice) {
	// Check if nice level is in range
	if (nice < -19 || nice > 20) {
		errno = EINVAL;
		return 1;
	}

	// Store nice level
	jail->nice = nice;

	return 0;
}

int pakfire_jail_set_cgroup(struct pakfire_jail* jail, struct pakfire_cgroup* cgroup) {
	// Free any previous cgroup
	if (jail->cgroup) {
		pakfire_cgroup_unref(jail->cgroup);
		jail->cgroup = NULL;
	}

	// Set any new cgroup
	if (cgroup) {
		DEBUG(jail->pakfire, "Setting cgroup %p\n", cgroup);

		jail->cgroup = pakfire_cgroup_ref(cgroup);
	}

	// Done
	return 0;
}

// Environment

// Returns the length of the environment
static unsigned int pakfire_jail_env_length(struct pakfire_jail* jail) {
	unsigned int i = 0;

	// Count everything in the environment
	for (char** e = jail->env; *e; e++)
		i++;

	return i;
}

// Finds an existing environment variable and returns its index or -1 if not found
static int pakfire_jail_find_env(struct pakfire_jail* jail, const char* key) {
	if (!key) {
		errno = EINVAL;
		return -1;
	}

	char buffer[strlen(key) + 2];
	pakfire_string_format(buffer, "%s=", key);

	for (unsigned int i = 0; jail->env[i]; i++) {
		if (pakfire_string_startswith(jail->env[i], buffer))
			return i;
	}

	// Nothing found
	return -1;
}

// Returns the value of an environment variable or NULL
PAKFIRE_EXPORT const char* pakfire_jail_get_env(struct pakfire_jail* jail,
		const char* key) {
	int i = pakfire_jail_find_env(jail, key);
	if (i < 0)
		return NULL;

	return jail->env[i] + strlen(key) + 1;
}

// Sets an environment variable
PAKFIRE_EXPORT int pakfire_jail_set_env(struct pakfire_jail* jail,
		const char* key, const char* value) {
	// Find the index where to write this value to
	int i = pakfire_jail_find_env(jail, key);
	if (i < 0)
		i = pakfire_jail_env_length(jail);

	// Return -ENOSPC when the environment is full
	if (i >= ENVIRON_SIZE) {
		errno = ENOSPC;
		return -1;
	}

	// Free any previous value
	if (jail->env[i])
		free(jail->env[i]);

	// Format and set environment variable
	asprintf(&jail->env[i], "%s=%s", key, value);

	DEBUG(jail->pakfire, "Set environment variable: %s\n", jail->env[i]);

	return 0;
}

// Imports an environment
PAKFIRE_EXPORT int pakfire_jail_import_env(struct pakfire_jail* jail, const char* env[]) {
	if (!env)
		return 0;

	char* key;
	char* val;
	int r;

	// Copy environment variables
	for (unsigned int i = 0; env[i]; i++) {
		r = pakfire_string_partition(env[i], "=", &key, &val);
		if (r)
			continue;

		// Set value
		r = pakfire_jail_set_env(jail, key, val);

		if (key)
			free(key);
		if (val)
			free(val);

		// Break on error
		if (r)
			return r;
	}

	return 0;
}

// Timeout

PAKFIRE_EXPORT int pakfire_jail_set_timeout(
		struct pakfire_jail* jail, unsigned int timeout) {
	// Store value
	jail->timeout.it_value.tv_sec = timeout;

	if (timeout > 0)
		DEBUG(jail->pakfire, "Timeout set to %d second(s)\n", timeout);
	else
		DEBUG(jail->pakfire, "Timeout disabled\n");

	return 0;
}

static int pakfire_jail_create_timer(struct pakfire_jail* jail) {
	int r;

	// Nothing to do if no timeout has been set
	if (!jail->timeout.it_value.tv_sec)
		return -1;

	// Create a new timer
	const int fd = timerfd_create(CLOCK_MONOTONIC, 0);
	if (fd < 0) {
		ERROR(jail->pakfire, "Could not create timer: %m\n");
		goto ERROR;
	}

	// Arm timer
	r = timerfd_settime(fd, 0, &jail->timeout, NULL);
	if (r) {
		ERROR(jail->pakfire, "Could not arm timer: %m\n");
		goto ERROR;
	}

	return fd;

ERROR:
	if (fd > 0)
		close(fd);

	return -1;
}

/*
	This function replaces any logging in the child process.

	All log messages will be sent to the parent process through their respective pipes.
*/
static void pakfire_jail_log(void* data, int priority, const char* file,
		int line, const char* fn, const char* format, va_list args) {
	struct pakfire_jail_pipes* pipes = (struct pakfire_jail_pipes*)data;
	int fd;

	switch (priority) {
		case LOG_INFO:
			fd = pipes->log_INFO[1];
			break;

		case LOG_ERR:
			fd = pipes->log_ERROR[1];
			break;

#ifdef ENABLE_DEBUG
		case LOG_DEBUG:
			fd = pipes->log_DEBUG[1];
			break;
#endif /* ENABLE_DEBUG */

		// Ignore any messages of an unknown priority
		default:
			return;
	}

	// Send the log message
	if (fd)
		vdprintf(fd, format, args);
}

static int pakfire_jail_log_buffer_is_full(const struct pakfire_log_buffer* buffer) {
	return (sizeof(buffer->data) == buffer->used);
}

/*
	This function reads as much data as it can from the file descriptor.
	If it finds a whole line in it, it will send it to the logger and repeat the process.
	If not newline character is found, it will try to read more data until it finds one.
*/
static int pakfire_jail_handle_log(struct pakfire_jail* jail,
		struct pakfire_jail_exec* ctx, int priority, int fd,
		struct pakfire_log_buffer* buffer, pakfire_jail_communicate_out callback, void* data) {
	char line[BUFFER_SIZE + 1];

	// Fill up buffer from fd
	if (buffer->used < sizeof(buffer->data)) {
		ssize_t bytes_read = read(fd, buffer->data + buffer->used,
				sizeof(buffer->data) - buffer->used);

		// Handle errors
		if (bytes_read < 0) {
			ERROR(jail->pakfire, "Could not read from fd %d: %m\n", fd);
			return -1;
		}

		// Update buffer size
		buffer->used += bytes_read;
	}

	// See if we have any lines that we can write
	while (buffer->used) {
		// Search for the end of the first line
		char* eol = memchr(buffer->data, '\n', buffer->used);

		// No newline found
		if (!eol) {
			// If the buffer is full, we send the content to the logger and try again
			// This should not happen in practise
			if (pakfire_jail_log_buffer_is_full(buffer)) {
				DEBUG(jail->pakfire, "Logging buffer is full. Sending all content\n");

				eol = buffer->data + sizeof(buffer->data) - 1;

			// Otherwise we might have only read parts of the output
			} else
				break;
		}

		// Find the length of the string
		size_t length = eol - buffer->data + 1;

		// Copy the line into the buffer
		memcpy(line, buffer->data, length);

		// Terminate the string
		line[length] = '\0';

		// Log the line
		if (callback) {
			int r = callback(jail->pakfire, data, priority, line, length);
			if (r) {
				ERROR(jail->pakfire, "The logging callback returned an error: %d\n", r);
				return r;
			}
		}

		// Remove line from buffer
		memmove(buffer->data, buffer->data + length, buffer->used - length);
		buffer->used -= length;
	}

	return 0;
}

static int pakfire_jail_stream_stdin(struct pakfire_jail* jail,
		struct pakfire_jail_exec* ctx, const int fd) {
	int r;

	// Nothing to do if there is no stdin callback set
	if (!ctx->communicate.in) {
		DEBUG(jail->pakfire, "Callback for standard input is not set\n");
		return 0;
	}

	// Skip if the writing pipe has already been closed
	if (!ctx->pipes.stdin[1])
		return 0;

	DEBUG(jail->pakfire, "Streaming standard input...\n");

	// Calling the callback
	r = ctx->communicate.in(jail->pakfire, ctx->communicate.data, fd);

	DEBUG(jail->pakfire, "Standard input callback finished: %d\n", r);

	// The callback signaled that it has written everything
	if (r == EOF) {
		DEBUG(jail->pakfire, "Closing standard input pipe\n");

		// Close the file-descriptor
		close(fd);

		// Reset the file-descriptor so it won't be closed again later
		ctx->pipes.stdin[1] = 0;

		// Report success
		r = 0;
	}

	return r;
}

static int pakfire_jail_setup_pipe(struct pakfire_jail* jail, int (*fds)[2], const int flags) {
	int r = pipe2(*fds, flags);
	if (r < 0) {
		ERROR(jail->pakfire, "Could not setup pipe: %m\n");
		return 1;
	}

	return 0;
}

static void pakfire_jail_close_pipe(struct pakfire_jail* jail, int fds[2]) {
	for (unsigned int i = 0; i < 2; i++)
		if (fds[i])
			close(fds[i]);
}

/*
	This is a convenience function to fetch the reading end of a pipe and
	closes the write end.
*/
static int pakfire_jail_get_pipe_to_read(struct pakfire_jail* jail, int (*fds)[2]) {
	// Give the variables easier names to avoid confusion
	int* fd_read  = &(*fds)[0];
	int* fd_write = &(*fds)[1];

	// Close the write end of the pipe
	if (*fd_write) {
		close(*fd_write);
		*fd_write = 0;
	}

	// Return the read end
	return *fd_read;
}

static int pakfire_jail_get_pipe_to_write(struct pakfire_jail* jail, int (*fds)[2]) {
	// Give the variables easier names to avoid confusion
	int* fd_read  = &(*fds)[0];
	int* fd_write = &(*fds)[1];

	// Close the read end of the pipe
	if (*fd_read) {
		close(*fd_read);
		*fd_read = 0;
	}

	// Return the write end
	return *fd_write;
}

static int pakfire_jail_wait(struct pakfire_jail* jail, struct pakfire_jail_exec* ctx) {
	int epollfd = -1;
	struct epoll_event ev;
	struct epoll_event events[EPOLL_MAX_EVENTS];
	char garbage[8];
	int r = 0;

	// Fetch file descriptors from context
	const int stdin = pakfire_jail_get_pipe_to_write(jail, &ctx->pipes.stdin);
	const int stdout = pakfire_jail_get_pipe_to_read(jail, &ctx->pipes.stdout);
	const int stderr = pakfire_jail_get_pipe_to_read(jail, &ctx->pipes.stderr);
	const int pidfd  = ctx->pidfd;

	// Timer
	const int timerfd = pakfire_jail_create_timer(jail);

	// Logging
	const int log_INFO  = pakfire_jail_get_pipe_to_read(jail, &ctx->pipes.log_INFO);
	const int log_ERROR = pakfire_jail_get_pipe_to_read(jail, &ctx->pipes.log_ERROR);
	const int log_DEBUG = pakfire_jail_get_pipe_to_read(jail, &ctx->pipes.log_DEBUG);

	// Make a list of all file descriptors we are interested in
	int fds[] = {
		stdin, stdout, stderr, pidfd, timerfd, log_INFO, log_ERROR, log_DEBUG,
	};

	// Setup epoll
	epollfd = epoll_create1(0);
	if (epollfd < 0) {
		ERROR(jail->pakfire, "Could not initialize epoll(): %m\n");
		r = 1;
		goto ERROR;
	}

	// Turn file descriptors into non-blocking mode and add them to epoll()
	for (unsigned int i = 0; i < sizeof(fds) / sizeof(*fds); i++) {
		int fd = fds[i];

		// Skip fds which were not initialized
		if (fd <= 0)
			continue;

		ev.events = EPOLLHUP;

		if (fd == stdin)
			ev.events |= EPOLLOUT;
		else
			ev.events |= EPOLLIN;

		// Read flags
		int flags = fcntl(fd, F_GETFL, 0);

		// Set modified flags
		if (fcntl(fd, F_SETFL, flags|O_NONBLOCK) < 0) {
			ERROR(jail->pakfire,
				"Could not set file descriptor %d into non-blocking mode: %m\n", fd);
			r = 1;
			goto ERROR;
		}

		ev.data.fd = fd;

		if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev) < 0) {
			ERROR(jail->pakfire, "Could not add file descriptor %d to epoll(): %m\n", fd);
			r = 1;
			goto ERROR;
		}
	}

	int ended = 0;

	// Loop for as long as the process is alive
	while (!ended) {
		int num = epoll_wait(epollfd, events, EPOLL_MAX_EVENTS, -1);
		if (num < 1) {
			// Ignore if epoll_wait() has been interrupted
			if (errno == EINTR)
				continue;

			ERROR(jail->pakfire, "epoll_wait() failed: %m\n");
			r = 1;

			goto ERROR;
		}

		for (int i = 0; i < num; i++) {
			int e  = events[i].events;
			int fd = events[i].data.fd;

			struct pakfire_log_buffer* buffer = NULL;
			pakfire_jail_communicate_out callback = NULL;
			void* data = NULL;
			int priority;

			// Check if there is any data to be read
			if (e & EPOLLIN) {
				// Handle any changes to the PIDFD
				if (fd == pidfd) {
					// Call waidid() and store the result
					r = waitid(P_PIDFD, ctx->pidfd, &ctx->status, WEXITED);
					if (r) {
						ERROR(jail->pakfire, "waitid() failed: %m\n");
						goto ERROR;
					}

					// Mark that we have ended so that we will process the remaining
					// events from epoll() now, but won't restart the outer loop.
					ended = 1;
					continue;

				// Handle timer events
				} else if (fd == timerfd) {
					DEBUG(jail->pakfire, "Timer event received\n");

					// Disarm the timer
					r = read(timerfd, garbage, sizeof(garbage));
					if (r < 1) {
						ERROR(jail->pakfire, "Could not disarm timer: %m\n");
						r = 1;
						goto ERROR;
					}

					// Terminate the process if it hasn't already ended
					if (!ended) {
						DEBUG(jail->pakfire, "Terminating process...\n");

						// Send SIGTERM to the process
						r = pidfd_send_signal(pidfd, SIGKILL, NULL, 0);
						if (r) {
							ERROR(jail->pakfire, "Could not kill process: %m\n");
							goto ERROR;
						}
					}

					// There is nothing else to do
					continue;

				// Handle logging messages
				} else if (fd == log_INFO) {
					buffer = &ctx->buffers.log_INFO;
					priority = LOG_INFO;

					callback = pakfire_jail_default_log_callback;

				} else if (fd == log_ERROR) {
					buffer = &ctx->buffers.log_ERROR;
					priority = LOG_ERR;

					callback = pakfire_jail_default_log_callback;

				} else if (fd == log_DEBUG) {
					buffer = &ctx->buffers.log_DEBUG;
					priority = LOG_DEBUG;

					callback = pakfire_jail_default_log_callback;

				// Handle anything from the log pipes
				} else if (fd == stdout) {
					buffer = &ctx->buffers.stdout;
					priority = LOG_INFO;

					callback = ctx->communicate.out;
					data     = ctx->communicate.data;

				} else if (fd == stderr) {
					buffer = &ctx->buffers.stderr;
					priority = LOG_ERR;

					callback = ctx->communicate.out;
					data     = ctx->communicate.data;

				} else {
					DEBUG(jail->pakfire, "Received invalid file descriptor %d\n", fd);
					continue;
				}

				// Handle log event
				r = pakfire_jail_handle_log(jail, ctx, priority, fd, buffer, callback, data);
				if (r)
					goto ERROR;
			}

			if (e & EPOLLOUT) {
				// Handle standard input
				if (fd == stdin) {
					r = pakfire_jail_stream_stdin(jail, ctx, fd);
					if (r) {
						switch (errno) {
							// Ignore if we filled up the buffer
							case EAGAIN:
								break;

							default:
								ERROR(jail->pakfire, "Could not write to stdin: %m\n");
								goto ERROR;
						}
					}
				}
			}

			// Check if any file descriptors have been closed
			if (e & EPOLLHUP) {
				// Remove the file descriptor
				r = epoll_ctl(epollfd, EPOLL_CTL_DEL, fd, NULL);
				if (r) {
					ERROR(jail->pakfire, "Could not remove closed file-descriptor %d: %m\n", fd);
					goto ERROR;
				}
			}
		}
	}

ERROR:
	if (epollfd > 0)
		close(epollfd);
	if (timerfd > 0)
		close(timerfd);

	return r;
}

int pakfire_jail_capture_stdout(struct pakfire* pakfire, void* data,
		int priority, const char* line, size_t length) {
	char** output = (char**)data;
	int r;

	// Append everything from stdout to a buffer
	if (output && priority == LOG_INFO) {
		r = asprintf(output, "%s%s", (output && *output) ? *output : "", line);
		if (r < 0)
			return 1;
		return 0;
	}

	// Send everything else to the default logger
	return pakfire_jail_default_log_callback(pakfire, NULL, priority, line, length);
}

// Capabilities

static int pakfire_jail_drop_capabilities(struct pakfire_jail* jail) {
	const int capabilities[] = {
		// Deny access to the kernel's audit system
		CAP_AUDIT_CONTROL,
		CAP_AUDIT_READ,
		CAP_AUDIT_WRITE,

		// Deny suspending block devices
		CAP_BLOCK_SUSPEND,

		// Deny any stuff with BPF
		CAP_BPF,

		// Deny checkpoint restore
		CAP_CHECKPOINT_RESTORE,

		// Deny opening files by inode number (open_by_handle_at)
		CAP_DAC_READ_SEARCH,

		// Deny setting SUID bits
		CAP_FSETID,

		// Deny locking more memory
		CAP_IPC_LOCK,

		// Deny modifying any Apparmor/SELinux/SMACK configuration
		CAP_MAC_ADMIN,
		CAP_MAC_OVERRIDE,

		// Deny creating any special devices
		CAP_MKNOD,

		// Deny reading from syslog
		CAP_SYSLOG,

		// Deny any admin actions (mount, sethostname, ...)
		CAP_SYS_ADMIN,

		// Deny rebooting the system
		CAP_SYS_BOOT,

		// Deny loading kernel modules
		CAP_SYS_MODULE,

		// Deny setting nice level
		CAP_SYS_NICE,

		// Deny access to /proc/kcore, /dev/mem, /dev/kmem
		CAP_SYS_RAWIO,

		// Deny circumventing any resource limits
		CAP_SYS_RESOURCE,

		// Deny setting the system time
		CAP_SYS_TIME,

		// Deny playing with suspend
		CAP_WAKE_ALARM,

		0,
	};

	DEBUG(jail->pakfire, "Dropping capabilities...\n");

	size_t num_caps = 0;
	int r;

	// Drop any capabilities
	for (const int* cap = capabilities; *cap; cap++) {
		r = prctl(PR_CAPBSET_DROP, *cap, 0, 0, 0);
		if (r) {
			ERROR(jail->pakfire, "Could not drop capability %d: %m\n", *cap);
			return r;
		}

		num_caps++;
	}

	// Fetch any capabilities
	cap_t caps = cap_get_proc();
	if (!caps) {
		ERROR(jail->pakfire, "Could not read capabilities: %m\n");
		return 1;
	}

	/*
		Set inheritable capabilities

		This ensures that no processes will be able to gain any of the listed
		capabilities again.
	*/
	r = cap_set_flag(caps, CAP_INHERITABLE, num_caps, capabilities, CAP_CLEAR);
	if (r) {
		ERROR(jail->pakfire, "cap_set_flag() failed: %m\n");
		goto ERROR;
	}

	// Restore capabilities
	r = cap_set_proc(caps);
	if (r) {
		ERROR(jail->pakfire, "Could not restore capabilities: %m\n");
		goto ERROR;
	}

ERROR:
	if (caps)
		cap_free(caps);

	return r;
}

// Syscall Filter

static int pakfire_jail_limit_syscalls(struct pakfire_jail* jail) {
	const int syscalls[] = {
		// The kernel's keyring isn't namespaced
		SCMP_SYS(keyctl),
		SCMP_SYS(add_key),
		SCMP_SYS(request_key),

		// Disable userfaultfd
		SCMP_SYS(userfaultfd),

		// Disable perf which could leak a lot of information about the host
		SCMP_SYS(perf_event_open),

		0,
	};
	int r = 1;

	DEBUG(jail->pakfire, "Applying syscall filter...\n");

	// Setup a syscall filter which allows everything by default
	scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_ALLOW);
	if (!ctx) {
		ERROR(jail->pakfire, "Could not setup seccomp filter: %m\n");
		goto ERROR;
	}

	// All all syscalls
	for (const int* syscall = syscalls; *syscall; syscall++) {
		r = seccomp_rule_add(ctx, SCMP_ACT_ERRNO(EPERM), *syscall, 0);
		if (r) {
			ERROR(jail->pakfire, "Could not configure syscall %d: %m\n", *syscall);
			goto ERROR;
		}
	}

	// Load syscall filter into the kernel
	r = seccomp_load(ctx);
	if (r) {
		ERROR(jail->pakfire, "Could not load syscall filter into the kernel: %m\n");
		goto ERROR;
	}

ERROR:
	if (ctx)
		seccomp_release(ctx);

	return r;
}

// Mountpoints

PAKFIRE_EXPORT int pakfire_jail_bind(struct pakfire_jail* jail,
		const char* source, const char* target, int flags) {
	struct pakfire_jail_mountpoint* mp = NULL;
	int r;

	// Check if there is any space left
	if (jail->num_mountpoints >= MAX_MOUNTPOINTS) {
		errno = ENOSPC;
		return 1;
	}

	// Check for valid inputs
	if (!source || !target) {
		errno = EINVAL;
		return 1;
	}

	// Select the next free slot
	mp = &jail->mountpoints[jail->num_mountpoints];

	// Copy source
	r = pakfire_string_set(mp->source, source);
	if (r) {
		ERROR(jail->pakfire, "Could not copy source: %m\n");
		return r;
	}

	// Copy target
	r = pakfire_string_set(mp->target, target);
	if (r) {
		ERROR(jail->pakfire, "Could not copy target: %m\n");
		return r;
	}

	// Copy flags
	mp->flags = flags;

	// Increment counter
	jail->num_mountpoints++;

	return 0;
}

static int pakfire_jail_mount_networking(struct pakfire_jail* jail) {
	int r;

	const char* paths[] = {
		"/etc/hosts",
		"/etc/resolv.conf",
		NULL,
	};

	// Bind-mount all paths read-only
	for (const char** path = paths; *path; path++) {
		r = pakfire_bind(jail->pakfire, *path, NULL, MS_RDONLY);
		if (r)
			return r;
	}

	return 0;
}

/*
	Mounts everything that we require in the new namespace
*/
static int pakfire_jail_mount(struct pakfire_jail* jail, struct pakfire_jail_exec* ctx) {
	struct pakfire_jail_mountpoint* mp = NULL;
	int r;

	// Mount all default stuff
	r = pakfire_mount_all(jail->pakfire);
	if (r)
		return r;

	// Mount networking stuff
	if (pakfire_jail_exec_has_flag(ctx, PAKFIRE_JAIL_HAS_NETWORKING)) {
		r = pakfire_jail_mount_networking(jail);
		if (r)
			return r;
	}

	// Mount all custom stuff
	for (unsigned int i = 0; i < jail->num_mountpoints; i++) {
		// Fetch mountpoint
		mp = &jail->mountpoints[i];

		// Mount it
		r = pakfire_bind(jail->pakfire, mp->source, mp->target, mp->flags);
		if (r)
			return r;
	}

	// Log all mountpoints
	pakfire_mount_list(jail->pakfire);

	return 0;
}

// UID/GID Mapping

static int pakfire_jail_setup_uid_mapping(struct pakfire_jail* jail, pid_t pid) {
	char path[PATH_MAX];
	int r;

	// Skip mapping anything when running on /
	if (pakfire_on_root(jail->pakfire))
		return 0;

	// Make path
	r = pakfire_string_format(path, "/proc/%d/uid_map", pid);
	if (r)
		return r;

	// Fetch UID
	const uid_t uid = pakfire_uid(jail->pakfire);

	// Fetch SUBUID
	const struct pakfire_subid* subuid = pakfire_subuid(jail->pakfire);
	if (!subuid)
		return 1;

	/* When running as root, we will map the entire range.

	   When running as a non-privileged user, we will map the root user inside the jail
	   to the user's UID outside of the jail, and we will map the rest starting from one.
	*/

	// Running as root
	if (uid == 0) {
		r = pakfire_file_write(jail->pakfire, path, 0, 0, 0,
			"0 %lu %lu\n", subuid->id, subuid->length);
	} else {
		r = pakfire_file_write(jail->pakfire, path, 0, 0, 0,
			"0 %lu 1\n1 %lu %lu\n", uid, subuid->id, subuid->length);
	}

	if (r) {
		ERROR(jail->pakfire, "Could not map UIDs: %m\n");
		return r;
	}

	return r;
}

static int pakfire_jail_setup_gid_mapping(struct pakfire_jail* jail, pid_t pid) {
	char path[PATH_MAX];
	int r;

	// Skip mapping anything when running on /
	if (pakfire_on_root(jail->pakfire))
		return 0;

	// Fetch GID
	const gid_t gid = pakfire_gid(jail->pakfire);

	// Fetch SUBGID
	const struct pakfire_subid* subgid = pakfire_subgid(jail->pakfire);
	if (!subgid)
		return 1;

	// Make path
	r = pakfire_string_format(path, "/proc/%d/gid_map", pid);
	if (r)
		return r;

	// Running as root
	if (gid == 0) {
		r = pakfire_file_write(jail->pakfire, path, 0, 0, 0,
			"0 %lu %lu\n", subgid->id, subgid->length);
	} else {
		r = pakfire_file_write(jail->pakfire, path, 0, 0, 0,
			"0 %lu 1\n%1 %lu %lu\n", gid, subgid->id, subgid->length);
	}

	if (r) {
		ERROR(jail->pakfire, "Could not map GIDs: %m\n");
		return r;
	}

	return r;
}

static int pakfire_jail_setgroups(struct pakfire_jail* jail, pid_t pid) {
	char path[PATH_MAX];
	int r = 1;

	// Make path
	r = pakfire_string_format(path, "/proc/%d/setgroups", pid);
	if (r)
		return r;

	// Open file for writing
	FILE* f = fopen(path, "w");
	if (!f) {
		ERROR(jail->pakfire, "Could not open %s for writing: %m\n", path);
		goto ERROR;
	}

	// Write content
	int bytes_written = fprintf(f, "deny\n");
	if (bytes_written <= 0) {
		ERROR(jail->pakfire, "Could not write to %s: %m\n", path);
		goto ERROR;
	}

	r = fclose(f);
	f = NULL;
	if (r) {
		ERROR(jail->pakfire, "Could not close %s: %m\n", path);
		goto ERROR;
	}

ERROR:
	if (f)
		fclose(f);

	return r;
}

static int pakfire_jail_send_signal(struct pakfire_jail* jail, int fd) {
	const uint64_t val = 1;
	int r = 0;

	DEBUG(jail->pakfire, "Sending signal...\n");

	// Write to the file descriptor
	ssize_t bytes_written = write(fd, &val, sizeof(val));
	if (bytes_written < 0 || (size_t)bytes_written < sizeof(val)) {
		ERROR(jail->pakfire, "Could not send signal: %m\n");
		r = 1;
	}

	// Close the file descriptor
	close(fd);

	return r;
}

static int pakfire_jail_wait_for_signal(struct pakfire_jail* jail, int fd) {
	uint64_t val = 0;
	int r = 0;

	DEBUG(jail->pakfire, "Waiting for signal...\n");

	ssize_t bytes_read = read(fd, &val, sizeof(val));
	if (bytes_read < 0 || (size_t)bytes_read < sizeof(val)) {
		ERROR(jail->pakfire, "Error waiting for signal: %m\n");
		r = 1;
	}

	// Close the file descriptor
	close(fd);

	return r;
}

/*
	Performs the initialisation that needs to happen in the parent part
*/
static int pakfire_jail_parent(struct pakfire_jail* jail, struct pakfire_jail_exec* ctx) {
	int r;

	// Setup UID mapping
	r = pakfire_jail_setup_uid_mapping(jail, ctx->pid);
	if (r)
		return r;

	// Write "deny" to /proc/PID/setgroups
	r = pakfire_jail_setgroups(jail, ctx->pid);
	if (r)
		return r;

	// Setup GID mapping
	r = pakfire_jail_setup_gid_mapping(jail, ctx->pid);
	if (r)
		return r;

	// Parent has finished initialisation
	DEBUG(jail->pakfire, "Parent has finished initialization\n");

	// Send signal to client
	r = pakfire_jail_send_signal(jail, ctx->completed_fd);
	if (r)
		return r;

	return 0;
}

static int pakfire_jail_child(struct pakfire_jail* jail, struct pakfire_jail_exec* ctx,
		const char* argv[]) {
	int r;

	// Redirect any logging to our log pipe
	pakfire_set_log_callback(jail->pakfire, pakfire_jail_log, &ctx->pipes);

	// Die with parent
	r = prctl(PR_SET_PDEATHSIG, SIGKILL, 0, 0, 0);
	if (r) {
		ERROR(jail->pakfire, "Could not configure to die with parent: %m\n");
		return 126;
	}

	// Fetch my own PID
	pid_t pid = getpid();

	DEBUG(jail->pakfire, "Launched child process in jail with PID %d\n", pid);

	// Wait for the parent to finish initialization
	r = pakfire_jail_wait_for_signal(jail, ctx->completed_fd);
	if (r)
		return r;

	// Perform further initialization

	// Fetch UID/GID
	uid_t uid = getuid();
	gid_t gid = getgid();

	// Fetch EUID/EGID
	uid_t euid = geteuid();
	gid_t egid = getegid();

	DEBUG(jail->pakfire, "  UID: %d (effective %d)\n", uid, euid);
	DEBUG(jail->pakfire, "  GID: %d (effective %d)\n", gid, egid);

	// Check if we are (effectively running as root)
	if (uid || gid || euid || egid) {
		ERROR(jail->pakfire, "Child process is not running as root\n");
		return 126;
	}

	const char* root = pakfire_get_path(jail->pakfire);
	const char* arch = pakfire_get_arch(jail->pakfire);

	// Change root (unless root is /)
	if (!pakfire_on_root(jail->pakfire)) {
		// Mount everything
		r = pakfire_jail_mount(jail, ctx);
		if (r)
			return r;

		// Call chroot()
		r = chroot(root);
		if (r) {
			ERROR(jail->pakfire, "chroot() to %s failed: %m\n", root);
			return 1;
		}

		// Change directory to /
		r = chdir("/");
		if (r) {
			ERROR(jail->pakfire, "chdir() after chroot() failed: %m\n");
			return 1;
		}
	}

	// Set personality
	unsigned long persona = pakfire_arch_personality(arch);
	if (persona) {
		r = personality(persona);
		if (r < 0) {
			ERROR(jail->pakfire, "Could not set personality (%x)\n", (unsigned int)persona);
			return 1;
		}
	}

	// Set nice level
	if (jail->nice) {
		DEBUG(jail->pakfire, "Setting nice level to %d\n", jail->nice);

		r = setpriority(PRIO_PROCESS, pid, jail->nice);
		if (r) {
			ERROR(jail->pakfire, "Could not set nice level: %m\n");
			return 1;
		}
	}

	// Close other end of log pipes
	close(ctx->pipes.log_INFO[0]);
	close(ctx->pipes.log_ERROR[0]);
#ifdef ENABLE_DEBUG
	close(ctx->pipes.log_DEBUG[0]);
#endif /* ENABLE_DEBUG */

	// Connect standard input
	if (ctx->pipes.stdin[0]) {
		r = dup2(ctx->pipes.stdin[0], STDIN_FILENO);
		if (r < 0) {
			ERROR(jail->pakfire, "Could not connect fd %d to stdin: %m\n",
				ctx->pipes.stdin[0]);

			return 1;
		}
	}

	// Connect standard output and error
	if (ctx->pipes.stdout[1] && ctx->pipes.stderr[1]) {
		r = dup2(ctx->pipes.stdout[1], STDOUT_FILENO);
		if (r < 0) {
			ERROR(jail->pakfire, "Could not connect fd %d to stdout: %m\n",
				ctx->pipes.stdout[1]);

			return 1;
		}

		r = dup2(ctx->pipes.stderr[1], STDERR_FILENO);
		if (r < 0) {
			ERROR(jail->pakfire, "Could not connect fd %d to stderr: %m\n",
				ctx->pipes.stderr[1]);

			return 1;
		}

		// Close the pipe (as we have moved the original file descriptors)
		pakfire_jail_close_pipe(jail, ctx->pipes.stdin);
		pakfire_jail_close_pipe(jail, ctx->pipes.stdout);
		pakfire_jail_close_pipe(jail, ctx->pipes.stderr);
	}

	// Reset open file limit (http://0pointer.net/blog/file-descriptor-limits.html)
	r = pakfire_rlimit_reset_nofile(jail->pakfire);
	if (r)
		return r;

	// Drop capabilities
	r = pakfire_jail_drop_capabilities(jail);
	if (r)
		return r;

	// Filter syscalls
	r = pakfire_jail_limit_syscalls(jail);
	if (r)
		return r;

	DEBUG(jail->pakfire, "Child process initialization done\n");
	DEBUG(jail->pakfire, "Launching command:\n");

	// Log argv
	for (unsigned int i = 0; argv[i]; i++)
		DEBUG(jail->pakfire, "  argv[%d] = %s\n", i, argv[i]);

	// exec() command
	r = execvpe(argv[0], (char**)argv, jail->env);
	if (r < 0) {
		// Translate errno into regular exit code
		switch (errno) {
			case ENOENT:
				// Ignore if the command doesn't exist
				if (ctx->flags & PAKFIRE_JAIL_NOENT_OK)
					r = 0;
				else
					r = 127;

				break;

			default:
				r = 1;
		}

		ERROR(jail->pakfire, "Could not execve(%s): %m\n", argv[0]);
	}

	// We should not get here
	return r;
}

// Run a command in the jail
static int __pakfire_jail_exec(struct pakfire_jail* jail, const char* argv[],
		const int interactive,
		pakfire_jail_communicate_in  communicate_in,
		pakfire_jail_communicate_out communicate_out,
		void* data, int flags) {
	int exit = -1;
	int r;

	// Check if argv is valid
	if (!argv || !argv[0]) {
		errno = EINVAL;
		return -1;
	}

	// Send any output to the default logger if no callback is set
	if (!communicate_out)
		communicate_out = pakfire_jail_default_log_callback;

	// Initialize context for this call
	struct pakfire_jail_exec ctx = {
		.flags = flags,

		.pipes = {
			.stdin  = { 0, 0 },
			.stdout = { 0, 0 },
			.stderr = { 0, 0 },
		},

		.communicate = {
			.in   = communicate_in,
			.out  = communicate_out,
			.data = data,
		},
	};

	DEBUG(jail->pakfire, "Executing jail...\n");

	// Enable networking in interactive mode
	if (interactive)
		ctx.flags |= PAKFIRE_JAIL_HAS_NETWORKING;

	/*
		Setup a file descriptor which can be used to notify the client that the parent
		has completed configuration.
	*/
	ctx.completed_fd = eventfd(0, EFD_CLOEXEC);
	if (ctx.completed_fd < 0) {
		ERROR(jail->pakfire, "eventfd() failed: %m\n");
		return -1;
	}

	// Create pipes to communicate with child process if we are not running interactively
	if (!interactive) {
		// stdin (only if callback is set)
		if (ctx.communicate.in) {
			r = pakfire_jail_setup_pipe(jail, &ctx.pipes.stdin, 0);
			if (r)
				goto ERROR;
		}

		// stdout
		r = pakfire_jail_setup_pipe(jail, &ctx.pipes.stdout, 0);
		if (r)
			goto ERROR;

		// stderr
		r = pakfire_jail_setup_pipe(jail, &ctx.pipes.stderr, 0);
		if (r)
			goto ERROR;
	}

	// Setup pipes for logging
	// INFO
	r = pakfire_jail_setup_pipe(jail, &ctx.pipes.log_INFO, O_CLOEXEC);
	if (r)
		goto ERROR;

	// ERROR
	r = pakfire_jail_setup_pipe(jail, &ctx.pipes.log_ERROR, O_CLOEXEC);
	if (r)
		goto ERROR;

#ifdef ENABLE_DEBUG
	// DEBUG
	r = pakfire_jail_setup_pipe(jail, &ctx.pipes.log_DEBUG, O_CLOEXEC);
	if (r)
		goto ERROR;
#endif /* ENABLE_DEBUG */

	// Configure child process
	struct clone_args args = {
		.flags =
			CLONE_NEWCGROUP |
			CLONE_NEWIPC |
			CLONE_NEWNS |
			CLONE_NEWPID |
			CLONE_NEWUSER |
			CLONE_NEWUTS |
			CLONE_PIDFD,
		.exit_signal = SIGCHLD,
		.pidfd = (long long unsigned int)&ctx.pidfd,
	};

	// Launch the process in a cgroup that is a leaf of the configured cgroup
	if (jail->cgroup) {
		args.flags |= CLONE_INTO_CGROUP;

		// Fetch our UUID
		const char* uuid = pakfire_jail_uuid(jail);

		// Create a temporary cgroup
		r = pakfire_cgroup_child(&ctx.cgroup, jail->cgroup, uuid, 0);
		if (r) {
			ERROR(jail->pakfire, "Could not create cgroup for jail: %m\n");
			goto ERROR;
		}

		// Clone into this cgroup
		args.cgroup = pakfire_cgroup_fd(ctx.cgroup);
	}

	// Setup networking
	if (!pakfire_jail_exec_has_flag(&ctx, PAKFIRE_JAIL_HAS_NETWORKING)) {
		args.flags |= CLONE_NEWNET;
	}

	// Fork this process
	ctx.pid = clone3(&args, sizeof(args));
	if (ctx.pid < 0) {
		ERROR(jail->pakfire, "Could not clone: %m\n");
		return -1;

	// Child process
	} else if (ctx.pid == 0) {
		r = pakfire_jail_child(jail, &ctx, argv);
		_exit(r);
	}

	// Parent process
	r = pakfire_jail_parent(jail, &ctx);
	if (r)
		goto ERROR;

	DEBUG(jail->pakfire, "Waiting for PID %d to finish its work\n", ctx.pid);

	// Read output of the child process
	r = pakfire_jail_wait(jail, &ctx);
	if (r)
		goto ERROR;

	// Handle exit status
	switch (ctx.status.si_code) {
		case CLD_EXITED:
			DEBUG(jail->pakfire, "The child process exited with code %d\n",
				ctx.status.si_status);

			// Pass exit code
			exit = ctx.status.si_status;
			break;

		case CLD_KILLED:
			ERROR(jail->pakfire, "The child process was killed\n");
			exit = 139;
			break;

		case CLD_DUMPED:
			ERROR(jail->pakfire, "The child process terminated abnormally\n");
			break;

		// Log anything else
		default:
			ERROR(jail->pakfire, "Unknown child exit code: %d\n", ctx.status.si_code);
			break;
	}

ERROR:
	// Destroy the temporary cgroup (if any)
	if (ctx.cgroup) {
		// Read cgroup stats
		r = pakfire_cgroup_stat(ctx.cgroup, &ctx.cgroup_stats);
		if (r) {
			ERROR(jail->pakfire, "Could not read cgroup stats: %m\n");
		} else {
			pakfire_cgroup_stat_dump(ctx.cgroup, &ctx.cgroup_stats);
		}

		pakfire_cgroup_destroy(ctx.cgroup);
		pakfire_cgroup_unref(ctx.cgroup);
	}

	// Close any file descriptors
	pakfire_jail_close_pipe(jail, ctx.pipes.stdin);
	pakfire_jail_close_pipe(jail, ctx.pipes.stdout);
	pakfire_jail_close_pipe(jail, ctx.pipes.stderr);
	if (ctx.pidfd)
		close(ctx.pidfd);
	pakfire_jail_close_pipe(jail, ctx.pipes.log_INFO);
	pakfire_jail_close_pipe(jail, ctx.pipes.log_ERROR);
	pakfire_jail_close_pipe(jail, ctx.pipes.log_DEBUG);

	return exit;
}

PAKFIRE_EXPORT int pakfire_jail_exec(
		struct pakfire_jail* jail,
		const char* argv[],
		pakfire_jail_communicate_in  callback_in,
		pakfire_jail_communicate_out callback_out,
		void* data, int flags) {
	return __pakfire_jail_exec(jail, argv, 0, callback_in, callback_out, data, flags);
}

static int pakfire_jail_exec_interactive(
		struct pakfire_jail* jail, const char* argv[], int flags) {
	int r;

	// Setup interactive stuff
	r = pakfire_jail_setup_interactive_env(jail);
	if (r)
		return r;

	return __pakfire_jail_exec(jail, argv, 1, NULL, NULL, NULL, flags);
}

int pakfire_jail_exec_script(struct pakfire_jail* jail,
		const char* script,
		const size_t size,
		const char* args[],
		pakfire_jail_communicate_in  callback_in,
		pakfire_jail_communicate_out callback_out,
		void* data) {
	char path[PATH_MAX];
	const char** argv = NULL;
	FILE* f = NULL;
	int r;

	const char* root = pakfire_get_path(jail->pakfire);

	// Write the scriptlet to disk
	r = pakfire_path_join(path, root, PAKFIRE_TMP_DIR "/pakfire-script.XXXXXX");
	if (r)
		goto ERROR;

	// Create a temporary file
	f = pakfire_mktemp(path, 0700);
	if (!f) {
		ERROR(jail->pakfire, "Could not create temporary file: %m\n");
		goto ERROR;
	}

	DEBUG(jail->pakfire, "Writing script to %s:\n%.*s\n", path, (int)size, script);

	// Write data
	r = fprintf(f, "%s", script);
	if (r < 0) {
		ERROR(jail->pakfire, "Could not write script to file %s: %m\n", path);
		goto ERROR;
	}

	// Close file
	r = fclose(f);
	if (r) {
		ERROR(jail->pakfire, "Could not close script file %s: %m\n", path);
		goto ERROR;
	}

	f = NULL;

	// Count how many arguments were passed
	unsigned int argc = 1;
	if (args) {
		for (const char** arg = args; *arg; arg++)
			argc++;
	}

	argv = calloc(argc + 1, sizeof(*argv));
	if (!argv) {
		ERROR(jail->pakfire, "Could not allocate argv: %m\n");
		goto ERROR;
	}

	// Set command
	argv[0] = (root) ? pakfire_path_relpath(root, path) : path;

	// Copy args
	for (unsigned int i = 1; i < argc; i++)
		argv[i] = args[i-1];

	// Run the script
	r = pakfire_jail_exec(jail, argv, callback_in, callback_out, data, 0);

ERROR:
	if (argv)
		free(argv);
	if (f)
		fclose(f);

	// Remove script from disk
	if (*path)
		unlink(path);

	return r;
}

/*
	A convenience function that creates a new jail, runs the given command and destroys
	the jail again.
*/
int pakfire_jail_run(struct pakfire* pakfire, const char* argv[], int flags, char** output) {
	struct pakfire_jail* jail = NULL;
	int r;

	// Create a new jail
	r = pakfire_jail_create(&jail, pakfire);
	if (r)
		goto ERROR;

	// Execute the command
	r = pakfire_jail_exec(jail, argv, NULL, pakfire_jail_capture_stdout, output, 0);

ERROR:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

int pakfire_jail_run_script(struct pakfire* pakfire,
		const char* script, const size_t length, const char* argv[], int flags) {
	struct pakfire_jail* jail = NULL;
	int r;

	// Create a new jail
	r = pakfire_jail_create(&jail, pakfire);
	if (r)
		goto ERROR;

	// Execute the command
	r = pakfire_jail_exec_script(jail, script, length, argv, NULL, NULL, NULL);

ERROR:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

int pakfire_jail_shell(struct pakfire_jail* jail) {
	const char* argv[] = {
		"/bin/bash", "--login", NULL,
	};

	// Execute /bin/bash
	return pakfire_jail_exec_interactive(jail, argv, 0);
}

static int pakfire_jail_run_if_possible(struct pakfire* pakfire, const char** argv) {
	char path[PATH_MAX];
	int r;

	r = pakfire_path(pakfire, path, "%s", *argv);
	if (r)
		return r;

	// Check if the file is executable
	r = access(path, X_OK);
	if (r) {
		DEBUG(pakfire, "%s is not executable. Skipping...\n", *argv);
		return 0;
	}

	return pakfire_jail_run(pakfire, argv, 0, NULL);
}

int pakfire_jail_ldconfig(struct pakfire* pakfire) {
	const char* argv[] = {
		"/sbin/ldconfig",
		NULL,
	};

	return pakfire_jail_run_if_possible(pakfire, argv);
}

int pakfire_jail_run_systemd_tmpfiles(struct pakfire* pakfire) {
	const char* argv[] = {
		"/usr/bin/systemd-tmpfiles",
		"--create",
		NULL,
	};

	return pakfire_jail_run_if_possible(pakfire, argv);
}
