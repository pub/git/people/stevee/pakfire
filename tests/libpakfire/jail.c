/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <sys/mount.h>

#include <pakfire/cgroup.h>
#include <pakfire/jail.h>

#include "../testsuite.h"

static const char* cmd_hello_world[] = {
	"/command", "echo", "Hello World!", NULL,
};

static const char* cmd_exhaust_memory[] = {
	"/command", "exhaust-memory", NULL,
};

static const char* cmd_fork_bomb[] = {
	"/command", "fork-bomb", NULL,
};

static const char* cmd_stat_ownership[] = {
	"/command", "stat-ownership", NULL,
};

static int test_create(const struct test* t) {
	struct pakfire_jail* jail = NULL;

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Destroy it
	ASSERT_NULL(pakfire_jail_unref(jail));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_exit_code(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	const char* argv[] = {
		"/command", "exit-with-code", "123", NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Check if we receive the correct exit code
	ASSERT(pakfire_jail_exec(jail, argv, NULL, NULL, NULL, 0) == 123);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int test_segv(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	const char* argv[] = {
		"/command", "segv", NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Check if we receive the correct exit code
	ASSERT(pakfire_jail_exec(jail, argv, NULL, NULL, NULL, 0) == 139);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int test_env(const struct test* t) {
	struct pakfire_jail* jail = NULL;

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Check if the default variables are set
	ASSERT(pakfire_jail_get_env(jail, "LANG"));
	ASSERT(pakfire_jail_get_env(jail, "TERM"));

	// Fetch a non-existing environment variable
	ASSERT_NULL(pakfire_jail_get_env(jail, "VARIABLE"));

	// Set a variable
	ASSERT_SUCCESS(pakfire_jail_set_env(jail, "VARIABLE", "VALUE"));

	// Read the value back
	ASSERT_STRING_EQUALS(pakfire_jail_get_env(jail, "VARIABLE"), "VALUE");

	// Destroy it
	ASSERT_NULL(pakfire_jail_unref(jail));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_exec(const struct test* t) {
	struct pakfire_jail* jail = NULL;

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Try to execute something
	ASSERT_SUCCESS(pakfire_jail_exec(jail, cmd_hello_world, NULL, NULL, NULL, 0));

	// Destroy it
	ASSERT_NULL(pakfire_jail_unref(jail));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_launch_into_cgroup(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	// Create a new cgroup
	ASSERT_SUCCESS(pakfire_cgroup_open(&cgroup, t->pakfire, "pakfire-test", 0));

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Connect jail to the cgroup
	ASSERT_SUCCESS(pakfire_jail_set_cgroup(jail, cgroup));

	// Run command
	ASSERT(pakfire_jail_exec(jail, cmd_hello_world, NULL, NULL, NULL, 0) == 0);

	r = EXIT_SUCCESS;

FAIL:
	if (cgroup) {
		pakfire_cgroup_destroy(cgroup);
		pakfire_cgroup_unref(cgroup);
	}
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int test_nice(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	char* output = NULL;
	int r = EXIT_FAILURE;

	const char* argv[] = {
		"/command", "print-nice", NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Set invalid nice levels
	ASSERT_ERRNO(pakfire_jail_nice(jail,  100), EINVAL);
	ASSERT_ERRNO(pakfire_jail_nice(jail, -100), EINVAL);

	// Set something sane
	ASSERT_SUCCESS(pakfire_jail_nice(jail, 5));

	// Check if the nice level has been set
	ASSERT_SUCCESS(pakfire_jail_exec(jail, argv,
		NULL, pakfire_jail_capture_stdout, &output, 0));
	ASSERT_STRING_EQUALS(output, "5\n");

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);
	if (output)
		free(output);

	return r;
}

static int test_memory_limit(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;


	// Create cgroup
	ASSERT_SUCCESS(pakfire_cgroup_open(&cgroup, t->pakfire, "pakfire-test", 0));

	// Create jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Connect jail to the cgroup
	ASSERT_SUCCESS(pakfire_jail_set_cgroup(jail, cgroup));

	// Set a memory limit of 100 MiB
	ASSERT_SUCCESS(pakfire_cgroup_set_memory_limit(cgroup, 100 * 1024 * 1024));

	// Try to exhaust all memory
	ASSERT_FAILURE(pakfire_jail_exec(jail, cmd_exhaust_memory, NULL, NULL, NULL, 0));

	// A fork bomb should also exhaust all memory
	ASSERT_FAILURE(pakfire_jail_exec(jail, cmd_fork_bomb, NULL, NULL, NULL, 0));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);
	if (cgroup) {
		pakfire_cgroup_destroy(cgroup);
		pakfire_cgroup_unref(cgroup);
	}

	return r;
}

static int test_pid_limit(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	// Create cgroup
	ASSERT_SUCCESS(pakfire_cgroup_open(&cgroup, t->pakfire, "pakfire-test", 0));

	// Create jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Connect jail to the cgroup
	ASSERT_SUCCESS(pakfire_jail_set_cgroup(jail, cgroup));

	// Set a PID limit of 100 processes
	ASSERT_SUCCESS(pakfire_cgroup_set_pid_limit(cgroup, 100));

	// Try to fork as many processes as possible
	ASSERT_FAILURE(pakfire_jail_exec(jail, cmd_fork_bomb, NULL, NULL, NULL, 0));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);
	if (cgroup) {
		pakfire_cgroup_destroy(cgroup);
		pakfire_cgroup_unref(cgroup);
	}

	return r;
}

static int test_file_ownership(const struct test* t) {
	int r = EXIT_FAILURE;
	char* output = NULL;

	// Execute a simple command
	ASSERT_SUCCESS(pakfire_jail_run(t->pakfire, cmd_stat_ownership, 0, &output));

	// Check if the file has been mapped to root/root
	ASSERT_STRING_EQUALS(output, "uid=0 gid=0\n");

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (output)
		free(output);

	return r;
}

static int test_bind(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	char* output = NULL;
	int r = EXIT_FAILURE;

	const char* source = "/";
	const char* target = "/oldroot";

	const char* argv[] = {
		"/command", "check-mountpoint", target, NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Bind-mount nonsense
	ASSERT_ERRNO(pakfire_jail_bind(jail, NULL, target, 0), EINVAL);
	ASSERT_ERRNO(pakfire_jail_bind(jail, source, NULL, 0), EINVAL);

	// Bind-mount something
	ASSERT_SUCCESS(pakfire_jail_bind(jail, source, target, MS_RDONLY));

	// Check if the mount actually works
	ASSERT_SUCCESS(pakfire_jail_exec(jail, argv, NULL, NULL, NULL, 0));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int callback_stdin(struct pakfire* pakfire, void* data, int fd) {
	int* lines = (int*)data;
	int r;

	while (*lines > 0) {
		r = dprintf(fd, "LINE %d\n", *lines);
		if (r < 0) {
			LOG_ERROR("Could not write line (%u) to stdin: %m\n", *lines);

			return 1;
		}

		// Decrement the lines counter
		(*lines)--;
	}

	return EOF;
}

static int test_communicate(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	// How many lines to send?
	int lines = 65535;

	const char* argv[] = {
		"/command", "pipe", NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Check if the mount actually works
	ASSERT_SUCCESS(pakfire_jail_exec(jail, argv, callback_stdin, NULL, &lines, 0));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int test_send_one_signal(const struct test* t,
		struct pakfire_jail* jail, const char* signal) {
	const char* argv[] = {
		"/command", "send-signal", signal, NULL,
	};

	// Perform the command
	return pakfire_jail_exec(jail, argv, NULL, NULL, NULL, 0);
}

static int test_send_signal(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Sending SIGTERM to ourselves
	ASSERT(test_send_one_signal(t, jail, "15") == 0);

	// Sending SIGKILL to ourselves
	ASSERT(test_send_one_signal(t, jail, "9") == 0);

	// Sending SIGSTOP to ourselves (this should be ignored by the jail)
	ASSERT(test_send_one_signal(t, jail, "23") == 0);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int test_timeout(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	const char* argv[] = {
		"/command", "sleep", "5", NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Set a timeout of one second
	ASSERT_SUCCESS(pakfire_jail_set_timeout(jail, 1));

	// Check if we receive the correct exit code
	ASSERT(pakfire_jail_exec(jail, argv, NULL, NULL, NULL, 0) == 139);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create);
	testsuite_add_test(test_exit_code);
	testsuite_add_test(test_segv);
	testsuite_add_test(test_env);
	testsuite_add_test(test_exec);
	testsuite_add_test(test_launch_into_cgroup);
	testsuite_add_test(test_nice);
	testsuite_add_test(test_memory_limit);
	testsuite_add_test(test_pid_limit);
	testsuite_add_test(test_file_ownership);
	testsuite_add_test(test_bind);
	testsuite_add_test(test_communicate);
	testsuite_add_test(test_send_signal);
	testsuite_add_test(test_timeout);

	return testsuite_run(argc, argv);
}
