/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <uuid/uuid.h>

#include <json.h>

#include <solv/evr.h>
#include <solv/pool.h>
#include <solv/repo.h>
#include <solv/solvable.h>

#include <pakfire/archive.h>
#include <pakfire/constants.h>
#include <pakfire/dependencies.h>
#include <pakfire/digest.h>
#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/i18n.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/private.h>
#include <pakfire/repo.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_package {
	struct pakfire* pakfire;
	int nrefs;

	// Reference to this package in the SOLV pool
	Id id;
	struct pakfire_repo* repo;

	char nevra[NAME_MAX];
	char source_nevra[NAME_MAX];

	char filename[NAME_MAX];
	char path[PATH_MAX];
};

static Solvable* get_solvable(struct pakfire_package* pkg) {
	Pool* pool = pakfire_get_solv_pool(pkg->pakfire);

	return pool_id2solvable(pool, pkg->id);
}

static int pakfire_package_dep2id(const enum pakfire_package_key key,
		Id* id, Id* marker) {
	switch (key) {
		case PAKFIRE_PKG_PROVIDES:
			*id = SOLVABLE_PROVIDES;
			*marker = -SOLVABLE_FILEMARKER;
			break;

		case PAKFIRE_PKG_PREREQUIRES:
			*id = SOLVABLE_REQUIRES;
			*marker = SOLVABLE_PREREQMARKER;
			break;

		case PAKFIRE_PKG_REQUIRES:
			*id = SOLVABLE_REQUIRES;
			*marker = -SOLVABLE_PREREQMARKER;
			break;

		case PAKFIRE_PKG_CONFLICTS:
			*id = SOLVABLE_CONFLICTS;
			break;

		case PAKFIRE_PKG_OBSOLETES:
			*id = SOLVABLE_OBSOLETES;
			break;

		case PAKFIRE_PKG_RECOMMENDS:
			*id = SOLVABLE_RECOMMENDS;
			break;

		case PAKFIRE_PKG_SUGGESTS:
			*id = SOLVABLE_SUGGESTS;
			break;

		case PAKFIRE_PKG_SUPPLEMENTS:
			*id = SOLVABLE_SUPPLEMENTS;
			break;

		case PAKFIRE_PKG_ENHANCES:
			*id = SOLVABLE_ENHANCES;
			break;

		// This operation is not possible for any other types
		default:
			errno = EINVAL;
			return 1;
	}

	return 0;
}

static int pakfire_package_add_depid(struct pakfire_package* pkg,
		const enum pakfire_package_key key, Id dep) {
	Solvable* s = get_solvable(pkg);
	int r;

	Id id = ID_NULL;
	Id marker = ID_NULL;

	// Translate the dependency type
	r = pakfire_package_dep2id(key, &id, &marker);
	if (r)
		return r;

	// Append to the dependency array
	solvable_add_deparray(s, id, dep, marker);

	return 0;
}

static int pakfire_package_add_self_provides(struct pakfire_package* pkg) {
	// Fetch the solvable
	Solvable* s = get_solvable(pkg);
	if (!s)
		return 1;

	// Get the self-provides ID
	Id dep = solvable_selfprovidedep(s);

	// Add it to the package
	return pakfire_package_add_depid(pkg, PAKFIRE_PKG_PROVIDES, dep);
}

int pakfire_package_create_from_solvable(struct pakfire_package** package,
		struct pakfire* pakfire, Id id) {
	struct pakfire_package* pkg = calloc(1, sizeof(*pkg));
	if (!pkg)
		return 1;

	pkg->pakfire = pakfire_ref(pakfire);
	pkg->nrefs = 1;

	// Store the ID
	pkg->id = id;

	// Success
	*package = pkg;
	return 0;
}

PAKFIRE_EXPORT int pakfire_package_create(struct pakfire_package** package,
		struct pakfire* pakfire, struct pakfire_repo* repo,
		const char* name, const char* evr, const char* arch) {
	struct pakfire_repo* dummy = NULL;
	int r;

	// Check for some valid input
	if (!name || !evr || !arch) {
		errno = EINVAL;
		return 1;
	}

	// Default to dummy repository
	if (!repo) {
		dummy = pakfire_get_repo(pakfire, PAKFIRE_REPO_DUMMY);
		if (!dummy) {
			errno = ENOENT;
			return 1;
		}

		repo = dummy;
	}

	// Allocate a new solvable
	Id id = pakfire_repo_add_solvable(repo);
	if (!id) {
		ERROR(pakfire, "Could not allocate a solvable: %m\n");
		r = 1;
		goto ERROR;
	}

	// Create a new package object
	r = pakfire_package_create_from_solvable(package, pakfire, id);
	if (r)
		goto ERROR;

	// Reference the repository
	(*package)->repo = pakfire_repo_ref(repo);

	// Set the name
	r = pakfire_package_set_string(*package, PAKFIRE_PKG_NAME, name);
	if (r) {
		ERROR(pakfire, "Could not set package name '%s': %m\n", name);
		goto ERROR;
	}

	// Set EVR
	r = pakfire_package_set_string(*package, PAKFIRE_PKG_EVR, evr);
	if (r) {
		ERROR(pakfire, "Could not set package EVR '%s': %m\n", evr);
		goto ERROR;
	}

	// Set arch
	r = pakfire_package_set_string(*package, PAKFIRE_PKG_ARCH, arch);
	if (r) {
		ERROR(pakfire, "Could not set package arch '%s': %m\n", arch);
		goto ERROR;
	}

	// Add self-provides
	r = pakfire_package_add_self_provides(*package);
	if (r) {
		ERROR(pakfire, "Could not create self-provides: %m\n");
		goto ERROR;
	}

ERROR:
	if (dummy)
		pakfire_repo_unref(dummy);

	return r;
}

static void pakfire_package_free(struct pakfire_package* pkg) {
	if (pkg->repo)
		pakfire_repo_unref(pkg->repo);

	pakfire_unref(pkg->pakfire);
	free(pkg);
}

PAKFIRE_EXPORT struct pakfire_package* pakfire_package_ref(struct pakfire_package* pkg) {
	pkg->nrefs++;

	return pkg;
}

PAKFIRE_EXPORT struct pakfire_package* pakfire_package_unref(struct pakfire_package* pkg) {
	if (--pkg->nrefs > 0)
		return pkg;

	pakfire_package_free(pkg);
	return NULL;
}

PAKFIRE_EXPORT struct pakfire* pakfire_package_get_pakfire(struct pakfire_package* pkg) {
	return pakfire_ref(pkg->pakfire);
}

PAKFIRE_EXPORT int pakfire_package_eq(struct pakfire_package* pkg1, struct pakfire_package* pkg2) {
	return pkg1->id == pkg2->id;
}

PAKFIRE_EXPORT int pakfire_package_cmp(struct pakfire_package* pkg1, struct pakfire_package* pkg2) {
	Pool* pool = pakfire_get_solv_pool(pkg1->pakfire);

	Solvable* s1 = get_solvable(pkg1);
	Solvable* s2 = get_solvable(pkg2);

	// Check names
	const char* str1 = pool_id2str(pool, s1->name);
	const char* str2 = pool_id2str(pool, s2->name);

	int ret = strcmp(str1, str2);
	if (ret)
		return ret;

	// Check the version string
	ret = pakfire_package_evr_cmp(pkg1, pkg2);
	if (ret)
		return ret;

	// Check repositories
	struct pakfire_repo* repo1 = pakfire_package_get_repo(pkg1);
	struct pakfire_repo* repo2 = pakfire_package_get_repo(pkg2);

	if (repo1 && repo2) {
		ret = pakfire_repo_cmp(repo1, repo2);
	}

	pakfire_repo_unref(repo1);
	pakfire_repo_unref(repo2);

	if (ret)
		return ret;

	// Check package architectures
	str1 = pool_id2str(pool, s1->arch);
	str2 = pool_id2str(pool, s2->arch);

	return strcmp(str1, str2);
}

PAKFIRE_EXPORT int pakfire_package_evr_cmp(struct pakfire_package* pkg1, struct pakfire_package* pkg2) {
	Pool* pool = pakfire_get_solv_pool(pkg1->pakfire);

	Solvable* s1 = get_solvable(pkg1);
	Solvable* s2 = get_solvable(pkg2);

	return pool_evrcmp(pool, s1->evr, s2->evr, EVRCMP_COMPARE);
}

PAKFIRE_EXPORT unsigned int pakfire_package_id(struct pakfire_package* pkg) {
	return pkg->id;
}

char* pakfire_package_join_evr(const char* e, const char* v, const char* r) {
	char* buffer = NULL;

	// Check for valid input
	if (!e || !v || !r) {
		errno = EINVAL;
		return NULL;
	}

	// Skip any zeroes in epoch
	while (*e && *e == '0')
		e++;

	// Format string with epoch
	if (*e) {
		if (asprintf(&buffer, "%s:%s-%s", e, v, r) < 0)
			return NULL;

	// Or format it without epoch
	} else {
		if (asprintf(&buffer, "%s-%s", v, r) < 0)
			return NULL;
	}

	return buffer;
}

int pakfire_package_is_source(struct pakfire_package* pkg) {
	const char* arch = pakfire_package_get_string(pkg, PAKFIRE_PKG_ARCH);
	if (!arch)
		return 1;

	return (strcmp(arch, "src") == 0);
}

static void pakfire_package_internalize_repo(struct pakfire_package* pkg) {
	struct pakfire_repo* repo = pakfire_package_get_repo(pkg);
	if (repo) {
		pakfire_repo_internalize(repo, 0);
		pakfire_repo_unref(repo);
	}
}

static void pakfire_package_has_changed(struct pakfire_package* pkg) {
	struct pakfire_repo* repo = pakfire_package_get_repo(pkg);
	if (repo) {
		pakfire_repo_has_changed(repo);
		pakfire_repo_unref(repo);
	}
}

// Removes epoch
static const char* evr2vr(const char* evr) {
	const char* p = evr;

	// Skip any leading digits
	for (; *p >= '0' && *p <= '9'; p++);

	// If after the leading digits, we found :, we return the rest of the string
	if (p != evr && *p == ':')
		return ++p;

	return evr;
}

static const char* pakfire_package_make_filename(struct pakfire_package* pkg) {
	int r;

	if (!*pkg->filename) {
		const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME);
		const char* evr  = pakfire_package_get_string(pkg, PAKFIRE_PKG_EVR);
		const char* arch = pakfire_package_get_string(pkg, PAKFIRE_PKG_ARCH);

		if (!name || !evr || !arch)
			return NULL;

		const char* vr = evr2vr(evr);
		if (!vr)
			return NULL;

		r = pakfire_string_format(pkg->filename, "%s-%s.%s.pfm", name, vr, arch);
		if (r)
			return NULL;
	}

	return pkg->filename;
}

static int pakfire_package_make_cache_path(struct pakfire_package* pkg) {
	const char* filename = pakfire_package_get_string(pkg, PAKFIRE_PKG_FILENAME);

	enum pakfire_digest_types digest_type = PAKFIRE_DIGEST_UNDEFINED;
	size_t digest_length = 0;

	// Fetch the digest
	const unsigned char* digest = pakfire_package_get_digest(pkg,
		&digest_type, &digest_length);

	if (digest && digest_length >= 4)
		return pakfire_cache_path(pkg->pakfire, pkg->path,
			"%02x/%02x/%02x/%02x/%s", digest[0], digest[1], digest[2], digest[3], filename);

	return pakfire_cache_path(pkg->pakfire, pkg->path, "%s", filename);
}

PAKFIRE_EXPORT const char* pakfire_package_get_string(
		struct pakfire_package* pkg, const enum pakfire_package_key key) {
	const char* ret = NULL;
	int r;

	pakfire_package_internalize_repo(pkg);

	Pool* pool = pakfire_get_solv_pool(pkg->pakfire);
	Solvable* s = get_solvable(pkg);

	switch (key) {
		case PAKFIRE_PKG_NAME:
			return pool_id2str(pool, s->name);

		case PAKFIRE_PKG_EVR:
			return pool_id2str(pool, s->evr);

		case PAKFIRE_PKG_ARCH:
			return pool_id2str(pool, s->arch);

		case PAKFIRE_PKG_NEVRA:
			if (!*pkg->nevra) {
				r = pakfire_string_set(pkg->nevra, pool_solvable2str(pool, s));
				if (r)
					return NULL;
			}

			return pkg->nevra;

		case PAKFIRE_PKG_UUID:
			ret = solvable_lookup_str(s, SOLVABLE_PKGID);
			break;

		case PAKFIRE_PKG_SUMMARY:
			ret = solvable_lookup_str(s, SOLVABLE_SUMMARY);
			break;

		case PAKFIRE_PKG_DESCRIPTION:
			ret = solvable_lookup_str(s, SOLVABLE_DESCRIPTION);
			break;

		case PAKFIRE_PKG_LICENSE:
			ret = solvable_lookup_str(s, SOLVABLE_LICENSE);
			break;

		case PAKFIRE_PKG_URL:
			ret = solvable_lookup_str(s, SOLVABLE_URL);
			break;

		case PAKFIRE_PKG_GROUPS:
			ret = solvable_lookup_str(s, SOLVABLE_GROUP);
			break;

		case PAKFIRE_PKG_VENDOR:
			ret = solvable_lookup_str(s, SOLVABLE_VENDOR);
			break;

		case PAKFIRE_PKG_DISTRO:
			ret = solvable_lookup_str(s, SOLVABLE_DISTRIBUTION);
			break;

		case PAKFIRE_PKG_PACKAGER:
			ret = solvable_lookup_str(s, SOLVABLE_PACKAGER);
			break;

		case PAKFIRE_PKG_PATH:
			if (!*pkg->path) {
				const char* base = solvable_lookup_str(s, SOLVABLE_MEDIABASE);
				if (base) {
					const char* filename = pakfire_package_get_string(pkg, PAKFIRE_PKG_FILENAME);
					if (!filename)
						return NULL;

					pakfire_string_format(pkg->path, "%s/%s", base, filename);
				} else {
					r = pakfire_package_make_cache_path(pkg);
					if (r)
						return NULL;
				}
			}

			return pkg->path;

		case PAKFIRE_PKG_FILENAME:
			ret = solvable_lookup_str(s, SOLVABLE_MEDIAFILE);

			// Generate the filename if not set
			if (!ret)
				ret = pakfire_package_make_filename(pkg);
			break;

		case PAKFIRE_PKG_BUILD_HOST:
			ret = solvable_lookup_str(s, SOLVABLE_BUILDHOST);
			break;

		case PAKFIRE_PKG_BUILD_ID:
			ret = solvable_lookup_str(s, SOLVABLE_BUILDVERSION);
			break;

		case PAKFIRE_PKG_SOURCE_PKG:
			if (!*pkg->source_nevra) {
				const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_NAME);
				const char* evr  = pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_EVR);
				const char* arch = pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_ARCH);

				// Return nothing if we don't have all information
				if (!name || !evr || !arch)
					return NULL;

				// Format package name
				r = pakfire_string_format(pkg->source_nevra, "%s-%s.%s", name, evr, arch);
				if (r)
					return NULL;
			}

			return pkg->source_nevra;

		case PAKFIRE_PKG_SOURCE_NAME:
			ret = solvable_lookup_str(s, SOLVABLE_SOURCENAME);
			break;

		case PAKFIRE_PKG_SOURCE_EVR:
			ret = solvable_lookup_str(s, SOLVABLE_SOURCEEVR);
			break;

		case PAKFIRE_PKG_SOURCE_ARCH:
			ret = solvable_lookup_str(s, SOLVABLE_SOURCEARCH);
			break;

		default:
			break;
	}

	return ret;
}

PAKFIRE_EXPORT int pakfire_package_set_string(
		struct pakfire_package* pkg, const enum pakfire_package_key key, const char* value) {
	Id id = ID_NULL;

	Pool* pool = pakfire_get_solv_pool(pkg->pakfire);
	Solvable* s = get_solvable(pkg);

	const char* basename = NULL;
	const char* dirname = NULL;

	switch (key) {
		// Do not allow to change name, evr, or arch
		case PAKFIRE_PKG_NAME:
			// Reset nevra & filename
			*pkg->nevra = *pkg->filename = '\0';

			s->name = pool_str2id(pool, value, 1);
			return 0;

		case PAKFIRE_PKG_EVR:
			// Reset nevra & filename
			*pkg->nevra = *pkg->filename = '\0';

			// Skip empty epoch
			if (pakfire_string_startswith(value, "0:"))
				value += 2;

			s->evr = pool_str2id(pool, value, 1);
			return 0;

		case PAKFIRE_PKG_ARCH:
			// Reset nevra & filename
			*pkg->nevra = *pkg->filename = '\0';

			s->arch = pool_str2id(pool, value, 1);
			return 0;

		case PAKFIRE_PKG_NEVRA:
			errno = EINVAL;
			return 1;

		case PAKFIRE_PKG_UUID:
			id = SOLVABLE_PKGID;
			break;

		case PAKFIRE_PKG_SUMMARY:
			id = SOLVABLE_SUMMARY;
			break;

		case PAKFIRE_PKG_DESCRIPTION:
			id = SOLVABLE_DESCRIPTION;
			break;

		case PAKFIRE_PKG_LICENSE:
			id = SOLVABLE_LICENSE;
			break;

		case PAKFIRE_PKG_URL:
			id = SOLVABLE_URL;
			break;

		case PAKFIRE_PKG_GROUPS:
			id = SOLVABLE_GROUP;
			break;

		case PAKFIRE_PKG_VENDOR:
			id = SOLVABLE_VENDOR;
			break;

		case PAKFIRE_PKG_DISTRO:
			id = SOLVABLE_DISTRIBUTION;
			break;

		case PAKFIRE_PKG_PACKAGER:
			id = SOLVABLE_PACKAGER;
			break;

		case PAKFIRE_PKG_PATH:
			if (value) {
				basename = pakfire_basename(value);
				dirname  = pakfire_dirname(value);
			}

			if (basename)
				solvable_set_str(s, SOLVABLE_MEDIAFILE, basename);
			else
				solvable_unset(s, SOLVABLE_MEDIAFILE);

			if (dirname)
				solvable_set_str(s, SOLVABLE_MEDIABASE, dirname);
			else
				solvable_unset(s, SOLVABLE_MEDIABASE);

			// Cache the path
			pakfire_string_set(pkg->path, value);

			// Mark the package as changed
			pakfire_package_has_changed(pkg);

			return 0;

		case PAKFIRE_PKG_FILENAME:
			id = SOLVABLE_MEDIAFILE;
			break;

		case PAKFIRE_PKG_BUILD_HOST:
			id = SOLVABLE_BUILDHOST;
			break;

		case PAKFIRE_PKG_BUILD_ID:
			id = SOLVABLE_BUILDVERSION;
			break;

		case PAKFIRE_PKG_SOURCE_PKG:
			// The source package name cannot be set
			break;

		case PAKFIRE_PKG_SOURCE_NAME:
			id = SOLVABLE_SOURCENAME;
			break;

		case PAKFIRE_PKG_SOURCE_EVR:
			// Skip empty epoch
			if (pakfire_string_startswith(value, "0:"))
				value += 2;

			id = SOLVABLE_SOURCEEVR;
			break;

		case PAKFIRE_PKG_SOURCE_ARCH:
			id = SOLVABLE_SOURCEARCH;
			break;

		default:
			break;
	}

	// Check if we have found a valid ID
	if (id == ID_NULL) {
		errno = EINVAL;
		return 1;
	}

	// Unset on empty string
	if (!value)
		solvable_unset(s, id);

	// Store string
	else
		solvable_set_str(s, id, value);

	// Mark the package as changed
	pakfire_package_has_changed(pkg);

	return 0;
}

PAKFIRE_EXPORT int pakfire_package_get_uuid(struct pakfire_package* pkg,
		const enum pakfire_package_key key, uuid_t uuid) {
	const char* buffer = NULL;
	int r;

	switch (key) {
		case PAKFIRE_PKG_UUID:
		case PAKFIRE_PKG_BUILD_ID:
			// Clear the UUID
			uuid_clear(uuid);

			// Fetch the value
			buffer = pakfire_package_get_string(pkg, key);
			if (!buffer)
				return 1;

			// Read buffer into the output
			r = uuid_parse(buffer, uuid);
			if (r)
				return r;

			return 0;

		default:
			errno = EINVAL;
			return 1;
	}
}

PAKFIRE_EXPORT int pakfire_package_set_uuid(struct pakfire_package* pkg,
		const enum pakfire_package_key key, const uuid_t uuid) {
	char buffer[UUID_STR_LEN];

	switch (key) {
		case PAKFIRE_PKG_UUID:
		case PAKFIRE_PKG_BUILD_ID:
			// Convert the UUID to string
			uuid_unparse_lower(uuid, buffer);

			// Store the UUID as string
			return pakfire_package_set_string(pkg, key, buffer);

		default:
			errno = EINVAL;
			return 1;
	}
}

PAKFIRE_EXPORT unsigned long long pakfire_package_get_num(struct pakfire_package* pkg,
		const enum pakfire_package_key key, unsigned long long notfound) {
	Id id = ID_NULL;

	switch (key) {
		case PAKFIRE_PKG_DBID:
			id = RPM_RPMDBID;
			break;

		case PAKFIRE_PKG_DOWNLOADSIZE:
			id = SOLVABLE_DOWNLOADSIZE;
			break;

		case PAKFIRE_PKG_INSTALLSIZE:
			id = SOLVABLE_INSTALLSIZE;
			break;

		case PAKFIRE_PKG_BUILD_TIME:
			id = SOLVABLE_BUILDTIME;
			break;

		case PAKFIRE_PKG_INSTALLTIME:
			id = SOLVABLE_INSTALLTIME;
			break;

		// Return zero for all unhandled keys
		default:
			errno = EINVAL;
			return 1;
	}

	Solvable* s = get_solvable(pkg);

	pakfire_package_internalize_repo(pkg);

	return solvable_lookup_num(s, id, notfound);
}

PAKFIRE_EXPORT int pakfire_package_set_num(struct pakfire_package* pkg,
		const enum pakfire_package_key key, unsigned long long num) {
	Id id = ID_NULL;

	switch (key) {
		case PAKFIRE_PKG_DBID:
			id = RPM_RPMDBID;
			break;

		case PAKFIRE_PKG_DOWNLOADSIZE:
			id = SOLVABLE_DOWNLOADSIZE;
			break;

		case PAKFIRE_PKG_INSTALLSIZE:
			id = SOLVABLE_INSTALLSIZE;
			break;

		case PAKFIRE_PKG_BUILD_TIME:
			id = SOLVABLE_BUILDTIME;
			break;

		case PAKFIRE_PKG_INSTALLTIME:
			id = SOLVABLE_INSTALLTIME;
			break;

		// Return zero for all unhandled keys
		default:
			errno = EINVAL;
			return 1;
	}

	Solvable* s = get_solvable(pkg);

	// Store the number
	solvable_set_num(s, id, num);

	// Mark the package as changed
	pakfire_package_has_changed(pkg);

	return 0;
}

static enum pakfire_digest_types pakfire_package_id2digest(Id id) {
	switch (id) {
		case REPOKEY_TYPE_SHA512:
			return PAKFIRE_DIGEST_SHA2_512;

		case REPOKEY_TYPE_SHA256:
			return PAKFIRE_DIGEST_SHA2_256;
	}

	return 0;
}

PAKFIRE_EXPORT const unsigned char* pakfire_package_get_digest(
		struct pakfire_package* pkg, enum pakfire_digest_types* type, size_t* length) {
	Solvable* s = get_solvable(pkg);
	Id id = 0;

	const unsigned char* checksum = solvable_lookup_bin_checksum(s, SOLVABLE_CHECKSUM, &id);

	// Convert ID to digest type
	*type = pakfire_package_id2digest(id);
	if (!*type) {
		errno = ENOTSUP;
		checksum = NULL;
	}

	// Store the length (if requested)
	if (length)
		*length = pakfire_digest_length(*type);

	return checksum;
}

PAKFIRE_EXPORT int pakfire_package_set_digest(struct pakfire_package* pkg,
		enum pakfire_digest_types type, const unsigned char* digest, const size_t length) {
	Solvable* s = get_solvable(pkg);
	Pool* pool = s->repo->pool;
	Id id;
	int r = 1;

	switch (type) {
		case PAKFIRE_DIGEST_SHA2_256:
			id = REPOKEY_TYPE_SHA256;
			break;

		case PAKFIRE_DIGEST_SHA2_512:
			id = REPOKEY_TYPE_SHA512;
			break;

		default:
			errno = ENOTSUP;
			return 1;
	}

	// Check if the digest length matches
	if (pakfire_digest_length(type) != length) {
		errno = EINVAL;
		return 1;
	}

	struct pakfire_repo* repo = pakfire_package_get_repo(pkg);

	Repodata* data = pakfire_repo_get_repodata(repo);
	if (!data)
		goto ERROR;

	repodata_set_bin_checksum(data, s - pool->solvables, SOLVABLE_CHECKSUM, id, digest);

	// Success
	r = 0;

ERROR:
	pakfire_repo_unref(repo);

	return r;
}

int pakfire_package_is_installed(struct pakfire_package* pkg) {
	Pool* pool = pakfire_get_solv_pool(pkg->pakfire);
	Solvable* s = get_solvable(pkg);

	return pool->installed == s->repo;
}

PAKFIRE_EXPORT size_t pakfire_package_get_size(struct pakfire_package* pkg) {
	if (pakfire_package_is_installed(pkg))
		return pakfire_package_get_num(pkg, PAKFIRE_PKG_INSTALLSIZE, 0);

	return pakfire_package_get_num(pkg, PAKFIRE_PKG_DOWNLOADSIZE, 0);
}

// Dependencies

PAKFIRE_EXPORT char** pakfire_package_get_deps(struct pakfire_package* pkg,
		const enum pakfire_package_key key) {
	Solvable* s = get_solvable(pkg);
	char** ret = NULL;
	int r;

	Queue q;
	Id id = ID_NULL;
	Id marker = ID_NULL;
	const char* dep = NULL;

	r = pakfire_package_dep2id(key, &id, &marker);
	if (r)
		return NULL;

	// Initialize the output queue
	queue_init(&q);

	// Fetch all deps
	solvable_lookup_deparray(s, id, &q, marker);

	// Allocate array
	ret = calloc(q.count + 1, sizeof(*ret));
	if (!ret)
		goto ERROR;

	for (int i = 0; i < q.count; i++) {
		dep = pakfire_dep2str(pkg->pakfire, q.elements[i]);
		if (!dep)
			goto ERROR;

		ret[i] = strdup(dep);
		if (!ret[i])
			goto ERROR;
	}

	// All good!
	goto SUCCESS;

ERROR:
	if (ret) {
		for (char** e = ret; *e; e++)
			free(*e);
		free(ret);

		ret = NULL;
	}

SUCCESS:
	queue_free(&q);

	return ret;
}

int pakfire_package_add_dep(struct pakfire_package* pkg,
		const enum pakfire_package_key key, const char* dep) {
	// Parse the dependency
	Id id = pakfire_str2dep(pkg->pakfire, dep);

	// Silently ignore any invalid dependencies
	if (!id)
		return 0;

	return pakfire_package_add_depid(pkg, key, id);
}

PAKFIRE_EXPORT int pakfire_package_get_reverse_requires(struct pakfire_package* pkg,
		struct pakfire_packagelist* list) {
	Queue matches;
	queue_init(&matches);

	// Get the pool ready
	pakfire_pool_internalize(pkg->pakfire);

	Pool* pool = pakfire_get_solv_pool(pkg->pakfire);

	// Search for any matches
	pool_whatmatchessolvable(pool, SOLVABLE_REQUIRES, pkg->id, &matches, 0);

	// Import the result to the package list
	int r = pakfire_packagelist_import_solvables(list, &matches);
	if (r)
		goto ERROR;

ERROR:
	queue_free(&matches);

	return r;
}

static int pakfire_package_has_rich_deps_in_deparray(
		struct pakfire_package* pkg, Id type) {
	int r = 0;

	Solvable* s = get_solvable(pkg);

	Queue q;
	queue_init(&q);

	// Fetch all deps
	solvable_lookup_deparray(s, type, &q, 0);

	// Nothing to do if the array was empty
	if (!q.count)
		goto ERROR;

	for (int i = 0; i < q.count; i++) {
		const char* dep = pakfire_dep2str(pkg->pakfire, q.elements[i]);

		// Is this a rich dependency?
		if (dep && *dep == '(') {
			r = 1;
			break;
		}
	}

ERROR:
	queue_free(&q);

	return r;
}

int pakfire_package_has_rich_deps(struct pakfire_package* pkg) {
	const Id types[] = {
		// Requires (includes pre-requires)
		SOLVABLE_REQUIRES,
		SOLVABLE_PROVIDES,
		SOLVABLE_CONFLICTS,
		SOLVABLE_OBSOLETES,
		SOLVABLE_RECOMMENDS,
		SOLVABLE_SUGGESTS,
		SOLVABLE_SUPPLEMENTS,
		SOLVABLE_ENHANCES,
		0,
	};

	for (const Id* type = types; *type; type++) {
		int r = pakfire_package_has_rich_deps_in_deparray(pkg, *type);
		if (r)
			return r;
	}

	// No match
	return 0;
}

int pakfire_package_matches_dep(struct pakfire_package* pkg,
		const enum pakfire_package_key key, const char* dep) {
	int r;

	Id id = ID_NULL;
	int marker = 0;

	// Translate the dependency type
	r = pakfire_package_dep2id(key, &id, &marker);
	if (r)
		return r;

	// Get the dependency
	Id depid = pakfire_str2dep(pkg->pakfire, dep);
	if (!depid)
		return -1;

	// Fetch the solvable
	Solvable* s = get_solvable(pkg);

	// Check whether this solvable matches the requested dependency
	return solvable_matchesdep(s, id, depid, marker);
}

PAKFIRE_EXPORT struct pakfire_repo* pakfire_package_get_repo(struct pakfire_package* pkg) {
	if (!pkg->repo) {
		Solvable* s = get_solvable(pkg);

		pkg->repo = pakfire_repo_create_from_repo(pkg->pakfire, s->repo);
	}

	return pakfire_repo_ref(pkg->repo);
}

static void pakfire_package_dump_add_line(char** str, const char* key, const char* val) {
	if (val)
		asprintf(str, "%s%-15s: %s\n", *str, key ? key : "", val);
}

static void pakfire_package_dump_add_lines(char** str, const char* key, const char* val) {
	char** lines = pakfire_string_split(val, '\n');
	if (!lines)
		return;

	while (*lines) {
		pakfire_package_dump_add_line(str, key, *lines);
		lines++;
	}
}

static void pakfire_package_dump_add_line_date(char** str, const char* key, time_t date) {
	char buffer[1024];
	int r;

	// Format time
	r = pakfire_strftime(buffer, "%a, %d %b %Y %T %z", date);
	if (r)
		return;

	pakfire_package_dump_add_line(str, key, buffer);
}

static void pakfire_package_dump_add_line_hex(char** str,
		const char* key, const unsigned char* buffer, const size_t length) {
	char* hex = __pakfire_hexlify(buffer, length);
	if (!hex)
		return;

	pakfire_package_dump_add_line(str, key, hex);
	free(hex);
}

static void pakfire_package_dump_add_line_size(char** str, const char* key, size_t size) {
	char buffer[128];

	// Format size in human-readable format
	int r = pakfire_format_size(buffer, size);
	if (r < 0)
		return;

	pakfire_package_dump_add_line(str, key, buffer);
}

static int pakfire_sort_dependencies(const void* p1, const void* p2) {
	const char* dep1 = *(const char**)p1;
	const char* dep2 = *(const char**)p2;

	return strcmp(dep1, dep2);
}

PAKFIRE_EXPORT char* pakfire_package_dump(struct pakfire_package* pkg, int flags) {
	char* string = "";

	// Name
	const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME);
	if (name)
		pakfire_package_dump_add_line(&string, _("Name"), name);

	// EVR
	const char* evr = pakfire_package_get_string(pkg, PAKFIRE_PKG_EVR);
	if (evr)
		pakfire_package_dump_add_line(&string, _("Version"), evr);

	// Arch
	const char* arch = pakfire_package_get_string(pkg, PAKFIRE_PKG_ARCH);
	if (arch)
		pakfire_package_dump_add_line(&string, _("Arch"), arch);

	// Size
	size_t size = pakfire_package_get_size(pkg);
	if (size)
		pakfire_package_dump_add_line_size(&string, _("Size"), size);

	// Installed Size
	if (pakfire_package_is_installed(pkg)) {
		size_t installsize = pakfire_package_get_num(pkg, PAKFIRE_PKG_INSTALLSIZE, 0);
		if (installsize)
			pakfire_package_dump_add_line_size(&string, _("Installed Size"), installsize);

	// Download Size
	} else {
		size_t downloadsize = pakfire_package_get_num(pkg, PAKFIRE_PKG_DOWNLOADSIZE, 0);
		if (downloadsize)
			pakfire_package_dump_add_line_size(&string, _("Download Size"), downloadsize);
	}

	// Repository
	struct pakfire_repo* repo = pakfire_package_get_repo(pkg);
	if (repo) {
		if (!pakfire_repo_name_equals(repo, PAKFIRE_REPO_DUMMY)) {
			const char* repo_name = pakfire_repo_get_name(repo);
			pakfire_package_dump_add_line(&string, _("Repo"), repo_name);
		}

		pakfire_repo_unref(repo);
	}

	// Summary
	const char* summary = pakfire_package_get_string(pkg, PAKFIRE_PKG_SUMMARY);
	if (summary)
		pakfire_package_dump_add_line(&string, _("Summary"), summary);

	// Description
	const char* description = pakfire_package_get_string(pkg, PAKFIRE_PKG_DESCRIPTION);
	if (description)
		pakfire_package_dump_add_lines(&string, _("Description"), description);

	// Groups
	const char* groups = pakfire_package_get_string(pkg, PAKFIRE_PKG_GROUPS);
	if (groups)
		pakfire_package_dump_add_lines(&string, _("Groups"), groups);

	// URL
	const char* url = pakfire_package_get_string(pkg, PAKFIRE_PKG_URL);
	if (url)
		pakfire_package_dump_add_line(&string, _("URL"), url);

	// License
	const char* license = pakfire_package_get_string(pkg, PAKFIRE_PKG_LICENSE);
	if (license)
		pakfire_package_dump_add_line(&string, _("License"), license);

	if (flags & PAKFIRE_PKG_DUMP_LONG) {
		// Install Time
		time_t install_time = pakfire_package_get_num(pkg, PAKFIRE_PKG_INSTALLTIME, 0);
		if (install_time)
			pakfire_package_dump_add_line_date(&string, _("Install Time"), install_time);

		// Distribution
		const char* distro = pakfire_package_get_string(pkg, PAKFIRE_PKG_DISTRO);
		if (distro)
			pakfire_package_dump_add_line(&string, _("Distribution"), distro);

		// Packager
		const char* packager = pakfire_package_get_string(pkg, PAKFIRE_PKG_PACKAGER);
		if (packager)
			pakfire_package_dump_add_line(&string, _("Packager"), packager);

		// Vendor
		const char* vendor = pakfire_package_get_string(pkg, PAKFIRE_PKG_VENDOR);
		if (vendor)
			pakfire_package_dump_add_line(&string, _("Vendor"), vendor);

		// UUID
		const char* uuid = pakfire_package_get_string(pkg, PAKFIRE_PKG_UUID);
		if (uuid)
			pakfire_package_dump_add_line(&string, _("UUID"), uuid);

		// Build ID
		const char* build_id = pakfire_package_get_string(pkg, PAKFIRE_PKG_BUILD_ID);
		if (build_id)
			pakfire_package_dump_add_line(&string, _("Build ID"), build_id);

		enum pakfire_digest_types digest_type = PAKFIRE_DIGEST_UNDEFINED;
		size_t digest_length = 0;

		// Digest
		const unsigned char* digest = pakfire_package_get_digest(pkg,
			&digest_type, &digest_length);
		if (digest) {
			switch (digest_type) {
				case PAKFIRE_DIGEST_SHA2_512:
					pakfire_package_dump_add_line_hex(&string,
						_("SHA2-512 Digest"), digest, digest_length);
					break;

				case PAKFIRE_DIGEST_SHA2_256:
					pakfire_package_dump_add_line_hex(&string,
						_("SHA2-256 Digest"), digest, digest_length);
					break;

				case PAKFIRE_DIGEST_SHA3_512:
				case PAKFIRE_DIGEST_SHA3_256:
				case PAKFIRE_DIGEST_BLAKE2B512:
				case PAKFIRE_DIGEST_BLAKE2S256:
				case PAKFIRE_DIGEST_UNDEFINED:
					break;
			}
		}

		// Source package
		const char* source_package = pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_PKG);
		if (source_package)
			pakfire_package_dump_add_line(&string, _("Source Package"), source_package);

		// Build time
		time_t build_time = pakfire_package_get_num(pkg, PAKFIRE_PKG_BUILD_TIME, 0);
		if (build_time)
			pakfire_package_dump_add_line_date(&string, _("Build Time"), build_time);

		// Build host
		const char* build_host = pakfire_package_get_string(pkg, PAKFIRE_PKG_BUILD_HOST);
		if (build_host)
			pakfire_package_dump_add_line(&string, _("Build Host"), build_host);

		// Dependencies
		for (const struct pakfire_dep* dep = pakfire_deps; dep->key; dep++) {
			char** deps = pakfire_package_get_deps(pkg, dep->key);
			if (deps) {
				size_t count = 0;

				// Count elements in the list
				for (char** d = deps; *d; d++)
					count++;

				// Sort the list
				qsort(deps, count, sizeof(*deps), pakfire_sort_dependencies);

				switch (dep->key) {
					case PAKFIRE_PKG_PROVIDES:
						name = _("Provides");
						break;

					case PAKFIRE_PKG_PREREQUIRES:
						name = _("Pre-Requires");
						break;

					case PAKFIRE_PKG_REQUIRES:
						name = _("Requires");
						break;

					case PAKFIRE_PKG_CONFLICTS:
						name = _("Conflicts");
						break;

					case PAKFIRE_PKG_OBSOLETES:
						name = _("Obsoletes");
						break;

					case PAKFIRE_PKG_RECOMMENDS:
						name = _("Recommends");
						break;

					case PAKFIRE_PKG_SUGGESTS:
						name = _("Suggests");
						break;

					case PAKFIRE_PKG_SUPPLEMENTS:
						name = _("Supplements");
						break;

					case PAKFIRE_PKG_ENHANCES:
						name = _("Enhances");
						break;

					default:
						name = NULL;
						break;
				}

				// Write it to the console
				for (char** d = deps; *d; d++) {
					pakfire_package_dump_add_line(&string, name, *d);
					free(*d);

					// Clear name after first line
					name = NULL;
				}
				free(deps);
			}
		}
	}

	if (flags & PAKFIRE_PKG_DUMP_FILELIST) {
		struct pakfire_filelist* filelist = pakfire_package_get_filelist(pkg);

		const char* prefix = _("Filelist");

		for (unsigned int i = 0; i < pakfire_filelist_length(filelist); i++) {
			struct pakfire_file* file = pakfire_filelist_get(filelist, i);

			const char* path = pakfire_file_get_path(file);
			pakfire_package_dump_add_line(&string, prefix, path);

			pakfire_file_unref(file);

			// Only prefix the first line
			prefix = NULL;
		}

		pakfire_filelist_unref(filelist);
	}

	return string;
}

PAKFIRE_EXPORT struct pakfire_archive* pakfire_package_get_archive(struct pakfire_package* pkg) {
	struct pakfire_archive* archive = NULL;

	// Otherwise open the archive from the cache
	const char* path = pakfire_package_get_string(pkg, PAKFIRE_PKG_PATH);
	if (!path)
		return NULL;

	// Open archive
	int r = pakfire_archive_open(&archive, pkg->pakfire, path);
	if (r)
		return NULL;

	return archive;
}

struct pakfire_package_filelist_search {
	struct pakfire* pakfire;
	struct pakfire_filelist* filelist;
	int r;
};

static int __pakfire_package_fetch_filelist(void* data, Solvable* s, Repodata* repodata,
		Repokey* key, struct s_KeyValue* kv) {
	struct pakfire_package_filelist_search* search = (struct pakfire_package_filelist_search*)data;

	struct pakfire_file* file = NULL;
	int r;

	// Skip any results of the wrong type
	if (key->type != REPOKEY_TYPE_DIRSTRARRAY)
		return 0;

	// Fetch the path
	const char* path = repodata_dir2str(repodata, kv->id, kv->str);
	if (!path) {
		r = 1;
		goto ERROR;
	}

	// Create a new file entry
	r = pakfire_file_create(&file, search->pakfire);
	if (r)
		goto ERROR;

	// Set path
	r = pakfire_file_set_path(file, path);
	if (r)
		goto ERROR;

	// Append the file to the filelist
	r = pakfire_filelist_add(search->filelist, file);
	if (r)
		goto ERROR;

ERROR:
	// Store the error code
	search->r = r;

	if (file)
		pakfire_file_unref(file);

	return r;
}

static int pakfire_package_fetch_filelist(struct pakfire_package* pkg, struct pakfire_filelist* filelist) {
	struct pakfire_package_filelist_search search = {
		.pakfire  = pkg->pakfire,
		.filelist = filelist,
		.r = 0,
	};

	pakfire_package_internalize_repo(pkg);

	Solvable* s = get_solvable(pkg);

	Repodata* repodata = repo_last_repodata(s->repo);

	// Search for all files
	repodata_search(repodata, pkg->id, SOLVABLE_FILELIST, SEARCH_FILES,
		__pakfire_package_fetch_filelist, &search);

	// Break on any error
	if (search.r)
		return search.r;

	return search.r;
}

PAKFIRE_EXPORT struct pakfire_filelist* pakfire_package_get_filelist(struct pakfire_package* pkg) {
	struct pakfire_filelist* filelist = NULL;

	// Create a new filelist
	int r = pakfire_filelist_create(&filelist, pkg->pakfire);
	if (r)
		goto ERROR;

	// Fetch all entries from the repository database
	r = pakfire_package_fetch_filelist(pkg, filelist);
	if (r)
		goto ERROR;

	return filelist;

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return NULL;
}

int pakfire_package_append_file(struct pakfire_package* pkg, const char* path) {
	// Fetch repodata
	struct pakfire_repo* repo = pakfire_package_get_repo(pkg);
	if (!repo) {
		ERROR(pkg->pakfire, "Could not find repository for %s: %m\n",
			pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA));
		return 1;
	}

	Repodata* repodata = pakfire_repo_get_repodata(repo);

	const char* basename = pakfire_basename(path);
	const char* dirname  = pakfire_dirname(path);

	// Convert directory into ID
	Id did = repodata_str2dir(repodata, dirname, 1);
	if (!did)
		did = repodata_str2dir(repodata, "/", 1);

	// Add data to list
	repodata_add_dirstr(repodata, pkg->id,
		SOLVABLE_FILELIST, did, basename);

	pakfire_repo_unref(repo);

	// This package has changed
	pakfire_package_has_changed(pkg);

	return 0;
}

static int __pakfire_package_set_filelist(struct pakfire* pakfire,
		struct pakfire_file* file, void* data) {
	struct pakfire_package* pkg = (struct pakfire_package*)data;

	// Fetch the path
	const char* path = pakfire_file_get_path(file);

	return pakfire_package_append_file(pkg, path);
}

PAKFIRE_EXPORT int pakfire_package_set_filelist(
		struct pakfire_package* pkg, struct pakfire_filelist* filelist) {
	return pakfire_filelist_walk(filelist, __pakfire_package_set_filelist, pkg, 0);
}

int pakfire_package_set_filelist_from_string(struct pakfire_package* pkg, const char* files) {
	char* p = NULL;
	int r = 0;

	// Copy files
	char* buffer = strdup(files);
	if (!buffer)
		goto ERROR;

	// Walk through all files
	char* path = strtok_r(buffer, "\n", &p);
	while (path) {
		r = pakfire_package_append_file(pkg, path);
		if (r)
			goto ERROR;

		path = strtok_r(NULL, "\n", &p);
	}

ERROR:
	if (buffer)
		free(buffer);

	return r;
}

static int _pakfire_package_add_json_dependencies(
		struct pakfire_package* pkg,
		struct json_object* json,
		const char* name,
		const enum pakfire_package_key key) {
	// Fetch dependencies
	char** dependencies = pakfire_package_get_deps(pkg, key);
	if (!dependencies)
		return 1;

	// Add dependencies
	int r = pakfire_json_add_string_array(pkg->pakfire, json, name, dependencies);
	if (r)
		goto ERROR;

ERROR:
	if (dependencies) {
		for (char** dep = dependencies; *dep; dep++)
			free(*dep);
		free(dependencies);
	}

	return r;
}

static int pakfire_package_add_json_dependencies(
		struct pakfire_package* pkg, struct json_object* md) {
	int r = 0;

	// Create new dependencies object
	struct json_object* object = json_object_new_object();
	if (!object)
		return 1;

	// Pre-requires
	r = _pakfire_package_add_json_dependencies(pkg, object,
			"prerequires", PAKFIRE_PKG_PREREQUIRES);
	if (r)
		goto ERROR;

	// Requires
	r = _pakfire_package_add_json_dependencies(pkg, object,
			"requires", PAKFIRE_PKG_REQUIRES);
	if (r)
		goto ERROR;

	// Provides
	r = _pakfire_package_add_json_dependencies(pkg, object,
			"provides", PAKFIRE_PKG_PROVIDES);
	if (r)
		goto ERROR;

	// Conflicts
	r = _pakfire_package_add_json_dependencies(pkg, object,
			"conflicts", PAKFIRE_PKG_CONFLICTS);
	if (r)
		goto ERROR;

	// Obsoletes
	r = _pakfire_package_add_json_dependencies(pkg, object,
			"obsoletes", PAKFIRE_PKG_OBSOLETES);
	if (r)
		goto ERROR;

	// Recommends
	r = _pakfire_package_add_json_dependencies(pkg, object,
			"recommends", PAKFIRE_PKG_RECOMMENDS);
	if (r)
		goto ERROR;

	// Suggests
	r = _pakfire_package_add_json_dependencies(pkg, object,
			"suggests", PAKFIRE_PKG_SUGGESTS);
	if (r)
		goto ERROR;

	// Supplements
	r = _pakfire_package_add_json_dependencies(pkg, object,
			"supplements", PAKFIRE_PKG_SUPPLEMENTS);
	if (r)
		goto ERROR;

	// Enhances
	r = _pakfire_package_add_json_dependencies(pkg, object,
			"enhances", PAKFIRE_PKG_ENHANCES);
	if (r)
		goto ERROR;

	// Add object
	r = json_object_object_add(md, "dependencies", object);
	if (r)
		goto ERROR;

ERROR:
	if (r)
		json_object_put(object);

	return r;
}

static int __pakfire_package_add_json_filelist(struct pakfire* pakfire,
		struct pakfire_file* file, void* data) {
	struct json_object* filelist = (struct json_object*)data;
	int r = 1;

	// Fetch the file path
	const char* path = pakfire_file_get_path(file);

	// Create a new JSON string
	struct json_object* object = json_object_new_string(path);
	if (!object)
		goto ERROR;

	// Append the string
	r = json_object_array_add(filelist, object);
	if (r)
		goto ERROR;

	return 0;

ERROR:
	// Free JSON string
	if (object)
		json_object_put(object);

	return r;
}

static int pakfire_package_add_json_filelist(
		struct pakfire_package* pkg, struct json_object* md) {
	struct pakfire_filelist* filelist = NULL;
	struct json_object* object = NULL;
	int r;

	// Fetch the filelist
	filelist = pakfire_package_get_filelist(pkg);
	if (!filelist) {
		ERROR(pkg->pakfire, "Could not fetch package filelist\n");
		r = 1;
		goto ERROR;
	}

	// Create a new JSON array
	object = json_object_new_array();
	if (!object) {
		ERROR(pkg->pakfire, "Could not create a JSON array\n");
		r = 1;
		goto ERROR;
	}

	// Walk through the filelist
	r = pakfire_filelist_walk(filelist, __pakfire_package_add_json_filelist, object, 0);
	if (r)
		goto ERROR;

	// Add object
	r = json_object_object_add(md, "filelist", object);
	if (r)
		goto ERROR;

ERROR:
	// Free JSON object on error
	if (r)
		json_object_put(object);
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

static int __pakfire_package_add_build_packages(struct pakfire* pakfire,
		struct pakfire_package* pkg, void* p) {
	struct json_object* object = (struct json_object*)p;
	int r;

	const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME);
	const char* evr = pakfire_package_get_string(pkg, PAKFIRE_PKG_EVR);

	if (!name || !evr) {
		ERROR(pakfire, "Could not fetch package information: %m\n");
		return 1;
	}

	// Add package information to the list
	r = pakfire_json_add_string(pakfire, object, name, evr);
	if (r) {
		ERROR(pakfire, "Could not add package to packages list: %m\n");
		return r;
	}

	return 0;
}

static int pakfire_package_add_build_packages(struct pakfire_package* pkg,
		struct json_object* md) {
	struct pakfire_repo* repo = NULL;
	struct pakfire_packagelist* packages = NULL;
	struct json_object* object = NULL;
	int r;

	// Create a new JSON array
	object = json_object_new_object();
	if (!object) {
		ERROR(pkg->pakfire, "Could not create a new JSON object: %m\n");
		r = 1;
		goto ERROR;
	}

	// Fetch the installed repository
	repo = pakfire_get_installed_repo(pkg->pakfire);
	if (!repo) {
		ERROR(pkg->pakfire, "Could not fetch the installed repository: %m\n");
		r = 1;
		goto ERROR;
	}

	// Create a new list
	r = pakfire_packagelist_create(&packages, pkg->pakfire);
	if (r)
		goto ERROR;

	// Fetch all installed packages
	r = pakfire_repo_to_packagelist(repo, packages);
	if (r) {
		ERROR(pkg->pakfire, "Could not fetch packages from installed repository: %m\n");
		goto ERROR;
	}

	// Add all packages to the array
	r = pakfire_packagelist_walk(packages, __pakfire_package_add_build_packages, object);
	if (r)
		goto ERROR;

	// Add object
	r = json_object_object_add(md, "packages", json_object_get(object));
	if (r)
		goto ERROR;

ERROR:
	if (packages)
		pakfire_packagelist_unref(packages);
	if (repo)
		pakfire_repo_unref(repo);
	if (object)
		json_object_put(object);

	return r;
}

static int pakfire_package_add_build_metadata(struct pakfire_package* pkg,
		struct json_object* md) {
	int r;

	// Create a new JSON object
	struct json_object* object = json_object_new_object();
	if (!object)
		return 1;

	// Add pakfire version that generated this metadata
	r = pakfire_json_add_string(pkg->pakfire, object, "pakfire", PACKAGE_VERSION);
	if (r)
		goto ERROR;

	// Write build host
	const char* build_host = pakfire_package_get_string(pkg, PAKFIRE_PKG_BUILD_HOST);
	if (build_host) {
		r = pakfire_json_add_string(pkg->pakfire, object, "host", build_host);
		if (r)
			goto ERROR;
	}

	// Write build id
	const char* build_id = pakfire_package_get_string(pkg, PAKFIRE_PKG_BUILD_ID);
	if (build_id) {
		r = pakfire_json_add_string(pkg->pakfire, object, "id", build_id);
		if (r)
			goto ERROR;
	}

	// Write build host
	time_t build_time = pakfire_package_get_num(pkg, PAKFIRE_PKG_BUILD_TIME, 0);
	if (build_time) {
		r = pakfire_json_add_integer(pkg->pakfire, object, "time", build_time);
		if (r)
			goto ERROR;
	}

	// Source package name
	const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_NAME);
	if (name) {
		r = pakfire_json_add_string(pkg->pakfire, object, "source-name", name);
		if (r)
			goto ERROR;
	}

	// Source package EVR
	const char* evr = pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_EVR);
	if (evr) {
		r = pakfire_json_add_string(pkg->pakfire, object, "source-evr", evr);
		if (r)
			goto ERROR;
	}

	// Source package arch
	const char* arch = pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_ARCH);
	if (arch) {
		r = pakfire_json_add_string(pkg->pakfire, object, "source-arch", arch);
		if (r)
			goto ERROR;
	}

	// Build Packages
	if (!pakfire_package_is_source(pkg)) {
		r = pakfire_package_add_build_packages(pkg, object);
		if (r)
			goto ERROR;
	}

	// Add object
	r = json_object_object_add(md, "build", object);
	if (r)
		goto ERROR;

ERROR:
	if (r)
		json_object_put(object);

	return r;
}

struct json_object* pakfire_package_to_json(struct pakfire_package* pkg) {
	struct json_object* md = json_object_new_object();
	int r = 0;

	// Name
	const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME);
	if (name) {
		r = pakfire_json_add_string(pkg->pakfire, md, "name", name);
		if (r)
			goto ERROR;
	}

	// EVR
	const char* evr = pakfire_package_get_string(pkg, PAKFIRE_PKG_EVR);
	if (evr) {
		r = pakfire_json_add_string(pkg->pakfire, md, "evr", evr);
		if (r)
			goto ERROR;
	}

	// Arch
	const char* arch = pakfire_package_get_string(pkg, PAKFIRE_PKG_ARCH);
	if (arch) {
		r = pakfire_json_add_string(pkg->pakfire, md, "arch", arch);
		if (r)
			goto ERROR;
	}

	// Vendor
	const char* vendor = pakfire_package_get_string(pkg, PAKFIRE_PKG_VENDOR);
	if (vendor) {
		r = pakfire_json_add_string(pkg->pakfire, md, "vendor", vendor);
		if (r)
			goto ERROR;
	}

	// Distribution
	const char* distribution = pakfire_package_get_string(pkg, PAKFIRE_PKG_DISTRO);
	if (distribution) {
		r = pakfire_json_add_string(pkg->pakfire, md, "distribution", distribution);
		if (r)
			goto ERROR;
	}

	// UUID
	const char* uuid = pakfire_package_get_string(pkg, PAKFIRE_PKG_UUID);
	if (uuid) {
		r = pakfire_json_add_string(pkg->pakfire, md, "uuid", uuid);
		if (r)
			goto ERROR;
	}

	// Groups
	const char* groups = pakfire_package_get_string(pkg, PAKFIRE_PKG_GROUPS);
	if (groups) {
		r = pakfire_json_add_string(pkg->pakfire, md, "groups", groups);
		if (r)
			goto ERROR;
	}

	// Packager
	const char* packager = pakfire_package_get_string(pkg, PAKFIRE_PKG_PACKAGER);
	if (packager) {
		r = pakfire_json_add_string(pkg->pakfire, md, "packager", packager);
		if (r)
			goto ERROR;
	}

	// URL
	const char* url = pakfire_package_get_string(pkg, PAKFIRE_PKG_URL);
	if (url) {
		r = pakfire_json_add_string(pkg->pakfire, md, "url", url);
		if (r)
			goto ERROR;
	}

	// License
	const char* license = pakfire_package_get_string(pkg, PAKFIRE_PKG_LICENSE);
	if (license) {
		r = pakfire_json_add_string(pkg->pakfire, md, "license", license);
		if (r)
			goto ERROR;
	}

	// Summary
	const char* summary = pakfire_package_get_string(pkg, PAKFIRE_PKG_SUMMARY);
	if (summary) {
		r = pakfire_json_add_string(pkg->pakfire, md, "summary", summary);
		if (r)
			goto ERROR;
	}

	// Description
	const char* description = pakfire_package_get_string(pkg, PAKFIRE_PKG_DESCRIPTION);
	if (description) {
		r = pakfire_json_add_string(pkg->pakfire, md, "description", description);
		if (r)
			goto ERROR;
	}

	// Installed package size
	size_t installsize = pakfire_package_get_num(pkg, PAKFIRE_PKG_INSTALLSIZE, 0);

	r = pakfire_json_add_integer(pkg->pakfire, md, "size", installsize);
	if (r)
		goto ERROR;

	// Generate dependency metadata
	r = pakfire_package_add_json_dependencies(pkg, md);
	if (r)
		goto ERROR;

	// Generate filelist
	r = pakfire_package_add_json_filelist(pkg, md);
	if (r)
		goto ERROR;

	// Generate build metadata
	r = pakfire_package_add_build_metadata(pkg, md);
	if (r)
		goto ERROR;

ERROR:
	if (r)
		json_object_put(md);

	return md;
}
