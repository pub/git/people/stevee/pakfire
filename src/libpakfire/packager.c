/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <archive.h>
#include <archive_entry.h>

#include <json.h>

#include <pakfire/archive.h>
#include <pakfire/compress.h>
#include <pakfire/constants.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/packager.h>
#include <pakfire/pakfire.h>
#include <pakfire/pwd.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_packager {
	struct pakfire* pakfire;
	int nrefs;
	time_t time_created;

	struct pakfire_package* pkg;
	char filename[PATH_MAX];

	// Reader
	struct archive* reader;

	// Payload
	struct pakfire_filelist* filelist;

	// Scriptlets
	struct pakfire_scriptlet** scriptlets;
	unsigned int num_scriptlets;

	// Digests
	unsigned int digests;
};

static void pakfire_packager_free(struct pakfire_packager* packager) {
	// Scriptlets
	if (packager->scriptlets) {
		for (unsigned int i = 0; i < packager->num_scriptlets; i++)
			pakfire_scriptlet_unref(packager->scriptlets[i]);
		free(packager->scriptlets);
	}

	if (packager->reader)
		archive_read_free(packager->reader);

	if (packager->filelist)
		pakfire_filelist_unref(packager->filelist);
	pakfire_package_unref(packager->pkg);
	pakfire_unref(packager->pakfire);
	free(packager);
}

int pakfire_packager_create(struct pakfire_packager** packager,
		struct pakfire* pakfire, struct pakfire_package* pkg) {
	struct pakfire_packager* p = calloc(1, sizeof(*p));
	if (!p)
		return ENOMEM;

	int r = 1;

	// Save creation time
	p->time_created = time(NULL);

	// Initialize reference counting
	p->nrefs = 1;

	// Store a reference to Pakfire
	p->pakfire = pakfire_ref(pakfire);

	// Store a reference to the package
	p->pkg = pakfire_package_ref(pkg);

	// Use the default digests
	p->digests = PAKFIRE_PACKAGER_DIGESTS;

	// Set distribution
	const char* tag = pakfire_get_distro_tag(p->pakfire);
	if (!tag) {
		ERROR(p->pakfire, "Distribution tag is not configured: %m\n");
		goto ERROR;
	}

	pakfire_package_set_string(pkg, PAKFIRE_PKG_DISTRO, tag);

	// Set build host
	pakfire_package_set_string(pkg, PAKFIRE_PKG_BUILD_HOST, pakfire_hostname());

	// Set build time
	pakfire_package_set_num(pkg, PAKFIRE_PKG_BUILD_TIME, p->time_created);

	// Create reader
	p->reader = pakfire_make_archive_disk_reader(p->pakfire, 1);
	if (!p->reader)
		goto ERROR;

	// Create filelist
	r = pakfire_filelist_create(&p->filelist, p->pakfire);
	if (r)
		goto ERROR;

	// Add a requirement for the cryptographic algorithms we are using
	if (p->digests & PAKFIRE_DIGEST_SHA3_512) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-SHA3-512)");
		if (r)
			goto ERROR;
	}
	if (p->digests & PAKFIRE_DIGEST_SHA3_256) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-SHA3-256)");
		if (r)
			goto ERROR;
	}
	if (p->digests & PAKFIRE_DIGEST_BLAKE2B512) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-BLAKE2b512)");
		if (r)
			goto ERROR;
	}
	if (p->digests & PAKFIRE_DIGEST_BLAKE2S256) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-BLAKE2s256)");
		if (r)
			goto ERROR;
	}
	if (p->digests & PAKFIRE_DIGEST_SHA2_512) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-SHA2-512)");
		if (r)
			goto ERROR;
	}
	if (p->digests & PAKFIRE_DIGEST_SHA2_256) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-SHA2-256)");
		if (r)
			goto ERROR;
	}

	*packager = p;

	return 0;

ERROR:
	pakfire_packager_free(p);

	return r;
}

struct pakfire_packager* pakfire_packager_ref(
		struct pakfire_packager* packager) {
	++packager->nrefs;

	return packager;
}

struct pakfire_packager* pakfire_packager_unref(
		struct pakfire_packager* packager) {
	if (--packager->nrefs > 0)
		return packager;

	pakfire_packager_free(packager);
	return NULL;
}

const char* pakfire_packager_filename(struct pakfire_packager* packager) {
	if (!*packager->filename) {
		const char* filename = pakfire_package_get_string(packager->pkg, PAKFIRE_PKG_FILENAME);

		// Add arch
		if (pakfire_package_is_source(packager->pkg))
			pakfire_string_set(packager->filename, filename);
		else {
			const char* arch = pakfire_package_get_string(packager->pkg, PAKFIRE_PKG_ARCH);

			pakfire_string_format(packager->filename, "%s/%s", arch, filename);
		}
	}

	return packager->filename;
}

static struct archive_entry* pakfire_packager_create_file(
		struct pakfire_packager* packager, const char* filename, size_t size, mode_t mode) {
	// Create a new file entry
	struct archive_entry* entry = archive_entry_new();
	if (!entry)
		return NULL;

	// Set filename
	archive_entry_set_pathname(entry, filename);

	// This is a regular file
    archive_entry_set_filetype(entry, AE_IFREG);
    archive_entry_set_perm(entry, mode);

	// Set size
	archive_entry_set_size(entry, size);

	// Set ownership
	archive_entry_set_uname(entry, "root");
	archive_entry_set_uid(entry, 0);
	archive_entry_set_gname(entry, "root");
	archive_entry_set_gid(entry, 0);

	// Set times
	archive_entry_set_birthtime(entry, packager->time_created, 0);
	archive_entry_set_ctime(entry, packager->time_created, 0);
	archive_entry_set_mtime(entry, packager->time_created, 0);
	archive_entry_set_atime(entry, packager->time_created, 0);

	return entry;
}

static int pakfire_packager_write_file_from_buffer(struct pakfire_packager* packager,
		struct archive* a, const char* filename, mode_t mode, const char* buffer) {
	size_t size = strlen(buffer);

	// Create a new file
	struct archive_entry* entry = pakfire_packager_create_file(packager, filename, size, mode);
	if (!entry) {
		ERROR(packager->pakfire, "Could not create file '%s'\n", filename);
		return 1;
	}

	// This is the end of the header
	int r = archive_write_header(a, entry);
	if (r) {
		ERROR(packager->pakfire, "Error writing header: %s\n", archive_error_string(a));
		goto ERROR;
	}

	// Write content
	r = archive_write_data(a, buffer, strlen(buffer));
	if (r < 0) {
		ERROR(packager->pakfire, "Error writing data: %s\n", archive_error_string(a));
		goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	archive_entry_free(entry);

	return r;
}

static int pakfire_packager_write_format(struct pakfire_packager* packager,
		struct archive* a) {
	const char buffer[] = TO_STRING(PACKAGE_FORMAT) "\n";

	DEBUG(packager->pakfire, "Writing package format\n");

	int r = pakfire_packager_write_file_from_buffer(packager, a,
		"pakfire-format", 0444, buffer);
	if (r)
		return r;

	// Add package format marker
	r = pakfire_package_add_dep(packager->pkg, PAKFIRE_PKG_REQUIRES,
		"pakfire(PackageFormat-" TO_STRING(PACKAGE_FORMAT) ")");
	if (r)
		return r;

	return 0;
}

static char* pakfire_packager_make_metadata(struct pakfire_packager* packager) {
	char* result = NULL;

	// Convert all package metadata to JSON
	struct json_object* md = pakfire_package_to_json(packager->pkg);
	if (!md)
		goto ERROR;

	// Serialize JSON to file
	const char* s = json_object_to_json_string_ext(md, 0);
	if (!s)
		goto ERROR;

	// Copy result onto heap
	result = strdup(s);
	if (!result)
		goto ERROR;

ERROR:
	// Free metadata
	if (md)
		json_object_put(md);

	return result;
}

static int pakfire_packager_write_metadata(struct pakfire_packager* packager,
		struct archive* a) {
	// Make metadata
	char* buffer = pakfire_packager_make_metadata(packager);
	if (!buffer)
		return 1;

	DEBUG(packager->pakfire, "Generated package metadata:\n%s\n", buffer);

	// Write buffer
	int r = pakfire_packager_write_file_from_buffer(packager, a,
		".PKGINFO", 0444, buffer);

	free(buffer);

	return r;
}

static int pakfire_packager_write_scriptlet(struct pakfire_packager* packager,
		struct archive* a, struct pakfire_scriptlet* scriptlet) {
	char filename[PATH_MAX];
	size_t size;
	int r;

	// Fetch type
	const char* type = pakfire_scriptlet_get_type(scriptlet);

	DEBUG(packager->pakfire, "Writing scriptlet '%s' to package\n", type);

	// Make filename
	r = pakfire_string_format(filename, ".scriptlets/%s", type);
	if (r)
		return r;

	// Fetch scriptlet
	const char* data = pakfire_scriptlet_get_data(scriptlet, &size);

	// Write file
	return pakfire_packager_write_file_from_buffer(packager, a, filename, 0544, data);
}

/*
	This function is being called at the end when all data has been added to the package.

	It will create a new archive and write the package to the given file descriptor.
*/
int pakfire_packager_finish(struct pakfire_packager* packager, FILE* f) {
	struct archive* a = NULL;
	unsigned int level;
	int r = 1;

	const char* nevra = pakfire_package_get_string(packager->pkg, PAKFIRE_PKG_NEVRA);

	// Add requires feature markers
	if (pakfire_package_has_rich_deps(packager->pkg)) {
		r = pakfire_package_add_dep(packager->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(RichDependencies)");
		if (r)
			goto ERROR;
	}

	// Add filelist
	r = pakfire_package_set_filelist(packager->pkg, packager->filelist);
	if (r)
		goto ERROR;

	const size_t installsize = pakfire_filelist_total_size(packager->filelist);

	// Store total install size
	r = pakfire_package_set_num(packager->pkg, PAKFIRE_PKG_INSTALLSIZE, installsize);
	if (r)
		goto ERROR;

	// Select compression level
	if (pakfire_package_is_source(packager->pkg))
		level = 1;
	else
		level = 22;

	// Create a new archive that is being compressed as fast as possible
	r = pakfire_compress_create_archive(packager->pakfire, &a, f,
		PAKFIRE_COMPRESS_ZSTD, level);
	if (r)
		goto ERROR;

	// Add feature marker
	pakfire_package_add_dep(packager->pkg,
		PAKFIRE_PKG_REQUIRES, "pakfire(Compress-Zstandard)");

	// Start with the format file
	r = pakfire_packager_write_format(packager, a);
	if (r) {
		ERROR(packager->pakfire, "Could not add format file to archive: %s\n",
			archive_error_string(a));
		goto ERROR;
	}

	// Write the metadata
	r = pakfire_packager_write_metadata(packager, a);
	if (r) {
		ERROR(packager->pakfire, "Could not add metadata file to archive: %s\n",
			archive_error_string(a));
		goto ERROR;
	}

	// Write scriptlets
	for (unsigned int i = 0; i < packager->num_scriptlets; i++) {
		r = pakfire_packager_write_scriptlet(packager, a, packager->scriptlets[i]);
		if (r) {
			ERROR(packager->pakfire, "Could not add scriptlet to the archive: %m\n");
			goto ERROR;
		}
	}

	// Write the payload
	r = pakfire_compress(packager->pakfire, a, packager->filelist, nevra,
		PAKFIRE_COMPRESS_SHOW_THROUGHPUT, PAKFIRE_PACKAGER_DIGESTS);
	if (r)
		goto ERROR;

	// Flush all buffers to disk
	fflush(f);

	// Success
	r = 0;

ERROR:
	if (a)
		archive_write_free(a);

	return r;
}

int pakfire_packager_finish_to_directory(struct pakfire_packager* packager,
		const char* target, char** result) {
	char path[PATH_MAX];
	char tmppath[PATH_MAX];
	FILE* f = NULL;
	int r = 1;

	// target cannot be empty
	if (!target) {
		errno = EINVAL;
		return 1;
	}

	// Get the filename of the package
	const char* filename = pakfire_packager_filename(packager);
	if (!filename) {
		ERROR(packager->pakfire, "Could not generate filename for package: %m\n");
		r = 1;
		goto ERROR;
	}

	// Make the package path
	r = pakfire_string_format(path, "%s/%s", target, filename);
	if (r)
		goto ERROR;

	// Create the parent directory
	r = pakfire_mkparentdir(path, 0755);
	if (r)
		goto ERROR;

	// Create a temporary file in the target directory
	r = pakfire_string_format(tmppath, "%s.XXXXXX", path);
	if (r)
		goto ERROR;

	// Create a temporary result file
	f = pakfire_mktemp(tmppath, 0);
	if (!f)
		goto ERROR;

	// Write the finished package
	r = pakfire_packager_finish(packager, f);
	if (r) {
		ERROR(packager->pakfire, "pakfire_packager_finish() failed: %m\n");
		goto ERROR;
	}

	// Move the temporary file to destination
	r = rename(tmppath, path);
	if (r) {
		ERROR(packager->pakfire, "Could not move %s to %s: %m\n", tmppath, path);
		goto ERROR;
	}

	DEBUG(packager->pakfire, "Package written to %s\n", path);

	// Store result path if requested
	if (result) {
		*result = strdup(path);
		if (!*result) {
			r = 1;
			goto ERROR;
		}
	}

	// Success
	r = 0;

ERROR:
	if (f)
		fclose(f);

	// Remove temporary file
	if (r && *tmppath)
		unlink(tmppath);

	return r;
}

int pakfire_packager_add_file(struct pakfire_packager* packager, struct pakfire_file* file) {
	int r;

	// Check input
	if (!file) {
		errno = EINVAL;
		return 1;
	}

	// Fetch path
	const char* path = pakfire_file_get_path(file);

	// Files cannot have an empty path
	if (!*path) {
		ERROR(packager->pakfire, "Cannot add a file with an empty path\n");
		errno = EPERM;
		return 1;

	// Hidden files cannot be added
	} else if (*path == '.') {
		ERROR(packager->pakfire, "Hidden files cannot be added to a package: %s\n", path);
		errno = EPERM;
		return 1;
	}

	DEBUG(packager->pakfire, "Adding file to payload: %s\n", path);

	// Detect the MIME type
	r = pakfire_file_detect_mimetype(file);
	if (r)
		return r;

	// Overwrite a couple of things for source archives
	if (pakfire_package_is_source(packager->pkg)) {
		// Reset permissions
		pakfire_file_set_perms(file, 0644);

		// Reset file ownership
		pakfire_file_set_uname(file, "root");
		pakfire_file_set_gname(file, "root");
	}

	// Handle systemd sysusers
	if (pakfire_file_matches(file, "/usr/lib/sysusers.d/*.conf")) {
		pakfire_package_add_dep(packager->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(systemd-sysusers)");

		// Ask to pre-install /usr/bin/systemd-sysusers
		pakfire_package_add_dep(packager->pkg,
			PAKFIRE_PKG_PREREQUIRES, "/usr/bin/systemd-sysusers");
	}

	// Handle systemd tmpfiles
	if (pakfire_file_matches(file, "/usr/lib/tmpfiles.d/*.conf")) {
		pakfire_package_add_dep(packager->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(systemd-tmpfiles)");

		// Ask to pre-install systemd
		pakfire_package_add_dep(packager->pkg,
			PAKFIRE_PKG_PREREQUIRES, "systemd");
	}

	// Append the file to the filelist
	return pakfire_filelist_add(packager->filelist, file);
}

int pakfire_packager_add(struct pakfire_packager* packager,
		const char* sourcepath, const char* path) {
	struct pakfire_file* file = NULL;
	int r = 1;

	// Create file
	r = pakfire_file_create_from_path(&file, packager->pakfire, sourcepath);
	if (r)
		goto ERROR;

	// Assign a new path for inside the archive
	if (path) {
		r = pakfire_file_set_path(file, path);
		if (r)
			goto ERROR;
	}

	// Call the main function
	r = pakfire_packager_add_file(packager, file);

ERROR:
	if (file)
		pakfire_file_unref(file);

	return r;
}

static int __pakfire_packager_add_files(struct pakfire* pakfire,
		struct pakfire_file* file, void* p) {
	struct pakfire_packager* packager = (struct pakfire_packager*)p;

	return pakfire_packager_add_file(packager, file);
}

int pakfire_packager_add_files(
		struct pakfire_packager* packager, struct pakfire_filelist* filelist) {
	// Add all files on the filelist
	return pakfire_filelist_walk(filelist, __pakfire_packager_add_files, packager, 0);
}

int pakfire_packager_add_scriptlet(struct pakfire_packager* packager,
		struct pakfire_scriptlet* scriptlet) {
	if (!scriptlet) {
		errno = EINVAL;
		return 1;
	}

	// Extend array
	packager->scriptlets = reallocarray(packager->scriptlets,
		packager->num_scriptlets + 1, sizeof(*packager->scriptlets));
	if (!packager->scriptlets)
		return 1;

	// Append scriptlet
	packager->scriptlets[packager->num_scriptlets++] = pakfire_scriptlet_ref(scriptlet);

	return 0;
}

/*
	Removes all files on the filelist
*/
int pakfire_packager_cleanup(struct pakfire_packager* packager) {
	// Delete all files on the filelist
	return pakfire_filelist_cleanup(packager->filelist, PAKFIRE_FILE_CLEANUP_TIDY);
}
