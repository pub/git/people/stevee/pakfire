/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdio.h>

#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/parser.h>
#include <pakfire/util.h>

int main(int argc, const char* argv[]) {
	struct pakfire* pakfire = NULL;
	struct pakfire_parser* parser = NULL;
	struct pakfire_parser_error* error = NULL;
	const char* message = NULL;
	FILE* f = NULL;
	int r = 1;

	char root[PATH_MAX] = "/tmp/pakfire-parser-test.XXXXXX";

	// Create test root directory
	char* tmp = pakfire_mkdtemp(root);
	if (!tmp) {
		fprintf(stderr, "Could not create temporary directory %s: %m\n", root);
		goto ERROR;
	}

	// Create a pakfire instance
	r = pakfire_create(&pakfire, root, NULL, NULL,
		0, LOG_DEBUG, pakfire_log_stderr, NULL);
	if (r) {
		fprintf(stderr, "Could not create Pakfire: %m\n");
		goto ERROR;
	}

	// Create a new parser
	parser = pakfire_parser_create(pakfire, NULL, NULL, 0);
	if (!parser) {
		fprintf(stderr, "Could not create a parser: %m\n");
		goto ERROR;
	}

	// Try parsing all files that have been passed on the command line
	for (unsigned int i = 1; i < argc; i++) {
		printf("Parsing %s...\n", argv[i]);

		// Try opening the file
		f = fopen(argv[i], "r");
		if (!f) {
			fprintf(stderr, "Could not open %s: %m\n", argv[i]);
			r = 1;
			goto ERROR;
		}

		// Try parsing the file
		r = pakfire_parser_read(parser, f, &error);
		if (r) {
			if (error) {
				message = pakfire_parser_error_get_message(error);
				fprintf(stderr, "PARSER ERROR: %s\n", message);

				fclose(f);
				goto ERROR;
			}
		}

		fclose(f);
	}

	// Dump the entire parser
	char* dump = pakfire_parser_dump(parser);
	if (dump) {
		printf("%s\n", dump);
		free(dump);
	}

ERROR:
	if (error)
		pakfire_parser_error_unref(error);
	if (parser)
		pakfire_parser_unref(parser);
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}
