/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

%lex-param {yyscan_t* scanner}

%parse-param
	{yyscan_t* scanner}
	{struct pakfire* pakfire}
	{struct pakfire_parser** parser}
	{struct pakfire_parser* parent}
	{struct pakfire_parser_error** error}

// Make the parser reentrant
%define api.pure full

// Generate verbose error messages
%define parse.error verbose

%{

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <pakfire/constants.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/parser.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define YYERROR_VERBOSE 1

// Enable to debug the grammar
#define YYDEBUG 0
#ifdef ENABLE_DEBUG
	int yydebug = YYDEBUG;
#endif

typedef void* yyscan_t;
extern int yylex_init(yyscan_t* scanner);
int yylex_destroy(yyscan_t scanner);

typedef struct yy_buffer_state* YY_BUFFER_STATE;
extern YY_BUFFER_STATE yy_scan_bytes(const char* buffer, int len, yyscan_t scanner);
extern void yy_delete_buffer(YY_BUFFER_STATE buffer, yyscan_t scanner);

#include "grammar.h"

extern int yylex (YYSTYPE* yylval_param, yyscan_t yyscanner);

extern int num_lines;

#define ABORT do { YYABORT; } while (0);

enum operator {
	OP_EQUALS = 0,
};

static void yyerror(yyscan_t* scanner, struct pakfire* pakfire, struct pakfire_parser** parser,
		struct pakfire_parser* parent, struct pakfire_parser_error** error, const char* s) {
	ERROR(pakfire, "Error (line %d): %s\n", num_lines, s);

	// Create a new error object
	if (error) {
		int r = pakfire_parser_error_create(error, *parser, NULL, num_lines, s);
		if (r) {
			ERROR(pakfire, "Could not create error object: %s\n", strerror(errno));
		}
	}
}

static struct pakfire_parser* make_if_stmt(struct pakfire* pakfire, const enum operator op,
		const char* val1, const char* val2, struct pakfire_parser* if_block, struct pakfire_parser* else_block);

static int pakfire_parser_new_declaration(
		struct pakfire_parser_declaration** declaration, const char* name, const char* value, int flags) {
	if (!name)
		return EINVAL;

	struct pakfire_parser_declaration* d = calloc(1, sizeof(*d));
	if (!d)
		return ENOMEM;

	// Set name
	pakfire_string_set(d->name, name);

	// Copy value
	if (value)
		d->value = strdup(value);
	else
		d->value = NULL;

	// Copy flags
	d->flags = flags;

	*declaration = d;

	return 0;
}

static void pakfire_parser_free_declaration(struct pakfire_parser_declaration* declaration) {
	if (declaration->value)
		free(declaration->value);

	free(declaration);
}

%}

%token						T_INDENT
%token						T_OUTDENT

%token <string>				T_KEY
%token <string>				T_STRING

%token						T_EOL

%token						T_END
%token						T_IF
%token						T_ELSE
%token						T_EXPORT

%token						T_EQUALS
%token						T_ASSIGN
%token						T_APPEND

%token <string>				T_SUBPARSER

%type <parser>				grammar
%type <parser>				block
%type <parser>				subparser
%type <string>				subparser_name
%type <parser>				if_stmt
%type <parser>				else_stmt

%type <string>				key
%type <string>				value
%type <declaration>			declaration

%type <string>				lines
%type <string>				line

%union {
	struct pakfire_parser* parser;
	char* string;
	struct pakfire_parser_declaration* declaration;
}

%initial-action {
	*parser = pakfire_parser_create(pakfire, parent, NULL, 0);
};

%destructor { if ($$) pakfire_parser_unref($$); } <parser>
%destructor { pakfire_parser_free_declaration($$); } <declaration>

%start grammar

%%

grammar						: %empty
							{
								$$ = pakfire_parser_create(pakfire, *parser, NULL, 0);
								if (!$$)
									ABORT;

								// This becomes the new top parser
								if (*parser)
									pakfire_parser_unref(*parser);
								*parser = pakfire_parser_ref($$);
							}
							| grammar declaration
							{
								$$ = $1;

								int r = pakfire_parser_apply_declaration($1, $2);
								if (r)
									ABORT;

								pakfire_parser_free_declaration($2);
							}
							| grammar subparser
							{
								$$ = $1;

								int r = pakfire_parser_merge($1, $2);
								pakfire_parser_unref($2);
								if (r)
									ABORT;
							}
							| grammar if_stmt
							{
								$$ = $1;

								if ($2) {
									int r = pakfire_parser_merge($1, $2);
									pakfire_parser_unref($2);
									if (r)
										ABORT;
								}
							}
							| grammar empty
							{
								$$ = $1;
							}
							;

block						: T_INDENT grammar T_OUTDENT
							{
								// Reset the top parser
								struct pakfire_parser* p = pakfire_parser_get_parent(*parser);
								pakfire_parser_unref(*parser);
								*parser = p;

								$$ = $2;
							}
							;

declaration					: key T_ASSIGN value T_EOL
							{
								int r = pakfire_parser_new_declaration(&$$, $1, $3, 0);
								if (r)
									ABORT;
							}
							| key T_APPEND value T_EOL
							{
								int r = pakfire_parser_new_declaration(&$$, $1, $3,
									PAKFIRE_PARSER_DECLARATION_APPEND);
								if (r)
									ABORT;
							}
							| key T_EOL empty_lines T_INDENT lines T_OUTDENT T_END T_EOL
							{
								int r = pakfire_parser_new_declaration(&$$, $1, $5, 0);
								if (r)
									ABORT;
							}
							| key T_EOL empty_lines T_END T_EOL
							{
								int r = pakfire_parser_new_declaration(&$$, $1, "", 0);
								if (r)
									ABORT;
							}
							| T_EXPORT key T_ASSIGN value T_EOL
							{
								int r = pakfire_parser_new_declaration(&$$, $key, $value,
									PAKFIRE_PARSER_DECLARATION_EXPORT);
								if (r)
									ABORT;
							}
							| T_EXPORT key T_APPEND value T_EOL
							{
								int r = pakfire_parser_new_declaration(&$$, $key, $value,
									PAKFIRE_PARSER_DECLARATION_EXPORT|PAKFIRE_PARSER_DECLARATION_APPEND);
								if (r)
									ABORT;
							}
							;

empty						: T_EOL
							;

empty_lines					: %empty
							| empty_lines empty;

key							: T_KEY
							;

value						: T_STRING
							{
								$$ = $1;
							}
							;

lines						: lines line
							{
								int r = asprintf(&$$, "%s\n%s", $1, $2);
								if (r < 0)
									ABORT;
							}
							| line;

line						: T_STRING T_EOL
							{
								$$ = $1;
							}
							| T_EOL
							{
								// Empty line
								$$ = "";
							}
							;

subparser					: subparser_name T_EOL block T_END T_EOL
							{
								$$ = $3;

								pakfire_parser_set_namespace($$, $1);

								char* key;
								char* value;

								int r = pakfire_string_partition($1, ":", &key, &value);
								if (r == 0) {
									if (strcmp("package", key) == 0) {
										pakfire_parser_set($$, NULL, "name", value, 0);
									}

									if (key)
										free(key);
									if (value)
										free(value);
								}
							}
							| subparser_name T_EOL
							{
								char* key;
								char* value;

								// Create a new parser
								$$ = pakfire_parser_create(pakfire, *parser, NULL, 0);
								if (!$$)
									ABORT;

								int r = pakfire_string_partition($1, ":", &key, &value);
								if (r)
									ABORT;

								// Handle packages
								if (strcmp("package", key) == 0) {
									pakfire_parser_set_namespace($$, $1);

									// Set the name (because we cannot have empty parsers)
									pakfire_parser_set($$, NULL, "name", value, 0);

								// Handle all other cases
								} else {
									pakfire_parser_set($$, NULL, key, value, 0);
								}

								if (key)
									free(key);
								if (value)
									free(value);
							}
							;

subparser_name				: T_SUBPARSER
							| T_SUBPARSER T_STRING
							{
								int r = asprintf(&$$, "%s:%s", $1, $2);
								if (r < 0)
									ABORT;
							}
							;

if_stmt						: T_IF T_STRING T_EQUALS T_STRING T_EOL block else_stmt T_END T_EOL
							{
								$$ = make_if_stmt(pakfire, OP_EQUALS, $2, $4, $6, $7);

								if ($6)
									pakfire_parser_unref($6);
								if ($7)
									pakfire_parser_unref($7);
							}
							;

else_stmt					: T_ELSE T_EOL block
							{
								$$ = $3;
							}
							| %empty
							{
								$$ = NULL;
							}
							;

%%

int pakfire_parser_parse_data(struct pakfire_parser* parent, const char* data, size_t len,
		struct pakfire_parser_error** error) {
	struct pakfire* pakfire = pakfire_parser_get_pakfire(parent);
	yyscan_t scanner;

#ifdef ENABLE_DEBUG
	DEBUG(pakfire, "Parsing the following data (%zu):\n%.*s\n",
		len, (int)len, data);

	// Save start time
	clock_t t_start = clock();
#endif

	// Initialise scanner
	yylex_init(&scanner);

	// Create a new sub-parser
	struct pakfire_parser* parser = NULL;

	num_lines = 1;

	YY_BUFFER_STATE buffer = yy_scan_bytes(data, len, scanner);
	int r = yyparse(scanner, pakfire, &parser, parent, error);
	yy_delete_buffer(buffer, scanner);

	// If everything was parsed successfully, we merge the sub-parser into
	// the parent parser. That way, it will be untouched if something could
	// not be successfully parsed.
	if (r == 0) {
		if (parser)
			pakfire_parser_merge(parent, parser);
	}

	// Destroy the parser
	if (parser)
		 pakfire_parser_unref(parser);

#ifdef ENABLE_DEBUG
	// Save end time
	clock_t t_end = clock();

	// Log what we have in the parent parser now
	char* dump = pakfire_parser_dump(parent);

	DEBUG(pakfire, "Status of the parser %p:\n%s\n", parent, dump);
	free(dump);

	// Log time we needed to parse data
	DEBUG(pakfire, "Parser finished in %.4fms\n",
		(double)(t_end - t_start) * 1000 / CLOCKS_PER_SEC);
#endif

	// Cleanup
	pakfire_unref(pakfire);
	yylex_destroy(scanner);

	return r;
}

static struct pakfire_parser* make_if_stmt(struct pakfire* pakfire, const enum operator op,
		const char* val1, const char* val2, struct pakfire_parser* if_block, struct pakfire_parser* else_block) {
	switch (op) {
		case OP_EQUALS:
			DEBUG(pakfire, "Evaluating if statement: %s == %s?\n", val1, val2);
			break;
	}

	struct pakfire_parser* parent = pakfire_parser_get_parent(if_block);

	DEBUG(pakfire, "  parent = %p, if = %p, else = %p\n", parent, if_block, else_block);

	// Expand values
	char* v1 = pakfire_parser_expand(parent, NULL, val1);
	char* v2 = pakfire_parser_expand(parent, NULL, val2);

	struct pakfire_parser* result = NULL;

	switch (op) {
		case OP_EQUALS:
			DEBUG(pakfire, "  '%s' == '%s'?\n", v1, v2);

			if (strcmp(v1, v2) == 0)
				result = if_block;
			else
				result = else_block;

			break;
	}

	free(v1);
	free(v2);
	pakfire_parser_unref(parent);

	if (result)
		pakfire_parser_ref(result);

	return result;
}
