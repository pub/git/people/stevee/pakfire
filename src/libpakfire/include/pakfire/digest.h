/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_DIGEST_H
#define PAKFIRE_DIGEST_H

// Pakfire knows these digests
enum pakfire_digest_types {
	PAKFIRE_DIGEST_UNDEFINED  = 0,
	PAKFIRE_DIGEST_SHA2_256   = (1 << 0),
	PAKFIRE_DIGEST_SHA2_512   = (1 << 1),
	PAKFIRE_DIGEST_BLAKE2S256 = (1 << 2),
	PAKFIRE_DIGEST_BLAKE2B512 = (1 << 3),
	PAKFIRE_DIGEST_SHA3_256   = (1 << 4),
	PAKFIRE_DIGEST_SHA3_512   = (1 << 5),
};

#define PAKFIRE_DIGESTS_ALL  (~PAKFIRE_DIGEST_UNDEFINED)

const char* pakfire_digest_name(const enum pakfire_digest_types type);
int pakfire_digest_get_by_name(const char* name);

#ifdef PAKFIRE_PRIVATE

#include <stdio.h>

#include <openssl/sha.h>

#include <pakfire/pakfire.h>

// libsolv only supports storing one checksum which has to be of a supported type
#define PAKFIRE_ARCHIVE_CHECKSUM PAKFIRE_DIGEST_SHA2_512

// Define BLAKE2's digest lengths
#ifndef BLAKE2S256_DIGEST_LENGTH
#  define BLAKE2S256_DIGEST_LENGTH 32
#endif /* BLAKE2S256_DIGEST_LENGTH */

#ifndef BLAKE2B512_DIGEST_LENGTH
#  define BLAKE2B512_DIGEST_LENGTH 64
#endif /* BLAKE2B512_DIGEST_LENGTH */

// Digests
struct pakfire_digests {
	// SHA-3-512
	unsigned char sha3_512[SHA512_DIGEST_LENGTH];

	// SHA-3-256
	unsigned char sha3_256[SHA256_DIGEST_LENGTH];

	// BLAKE2b512
	unsigned char blake2b512[BLAKE2B512_DIGEST_LENGTH];

	// BLAKE2s256
	unsigned char blake2s256[BLAKE2S256_DIGEST_LENGTH];

	// SHA2-512
	unsigned char sha2_512[SHA512_DIGEST_LENGTH];

	// SHA2-256
	unsigned char sha2_256[SHA256_DIGEST_LENGTH];
};

#define PAKFIRE_DIGESTS_INIT \
	{ \
		.sha3_512 = 0, \
		.sha3_256 = 0, \
		.blake2b512 = 0, \
		.blake2s256 = 0, \
		.sha2_512 = 0, \
		.sha2_256 = 0, \
	}

size_t pakfire_digest_length(const enum pakfire_digest_types digest);

const unsigned char* pakfire_digest_get(struct pakfire_digests* digests,
	const enum pakfire_digest_types type, size_t* length);

#define pakfire_digest_set(digest) __pakfire_digest_set(digest, sizeof(digest))
int __pakfire_digest_set(const unsigned char* digest, const size_t length);

int pakfire_digest_has_any(const struct pakfire_digests* digests);

void pakfire_digests_reset(struct pakfire_digests* digests, int types);

int pakfire_digests_compute_from_file(struct pakfire* pakfire,
	struct pakfire_digests* digests, int types, FILE* f);

int pakfire_digests_compare(struct pakfire* pakfire, const struct pakfire_digests* digests1,
	const struct pakfire_digests* digests2, const int types);
int pakfire_digests_compare_one(struct pakfire* pakfire, struct pakfire_digests* digests1,
	const enum pakfire_digest_types type, const unsigned char* digest, const size_t length);

#endif

#endif /* PAKFIRE_DIGEST_H */
