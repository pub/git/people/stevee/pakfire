#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import cpuinfo
import functools
import hashlib
import json
import kerberos
import logging
import os.path
import psutil
import tornado.escape
import tornado.httpclient
import tornado.websocket
import urllib.parse

from . import _pakfire
from . import util
from .constants import *
from .i18n import _

# Setup logging
log = logging.getLogger("pakfire.hub")

DEFAULT_KEYTAB = "/etc/krb5.keytab"

# Configure some useful defaults for all requests
tornado.httpclient.AsyncHTTPClient.configure(
	None, defaults = {
		"user_agent" : "pakfire/%s" % PAKFIRE_VERSION,
	},
)

class AuthError(Exception):
	"""
		Raised when the client could not authenticate against the hub
	"""
	pass


class Hub(object):
	def __init__(self, url, keytab=None):
		self.url = url

		# Store path to keytab
		self.keytab = keytab or DEFAULT_KEYTAB

		# Initialise the HTTP client
		self.client = tornado.httpclient.AsyncHTTPClient()

		# XXX support proxies

	async def _request(self, method, path, websocket=False, authenticate=True,
			body=None, body_producer=None, on_message_callback=None, **kwargs):
		headers = {}
		query_args = {}

		# Make absolute URL
		url = urllib.parse.urljoin(self.url, path)

		# Change scheme for websocket
		if websocket and url.startswith("https://"):
			url = url.replace("https://", "wss://")

		# Filter all query arguments
		for arg in kwargs:
			# Skip anything that is None
			if kwargs[arg] is None:
				continue

			# Add to query arguments
			query_args[arg] = kwargs[arg]

		# Encode query arguments
		query_args = urllib.parse.urlencode(query_args, doseq=True)

		# Add query arguments
		if method in ("GET", "PUT", "DELETE"):
			url = "%s?%s" % (url, query_args)

		# Add any arguments to the body
		elif method == "POST":
			if body is None:
				body = query_args

		# Perform Kerberos authentication
		if authenticate:
			krb5_context = self._setup_krb5_context(url)

			# Fetch the Kerberos client response
			krb5_client_response = kerberos.authGSSClientResponse(krb5_context)

			# Set the Negotiate header
			headers |= {
				"Authorization" : "Negotiate %s" % krb5_client_response,
			}

		# Make the request
		req = tornado.httpclient.HTTPRequest(
			method=method, url=url, headers=headers, body=body,

			# Add all the rest
			body_producer=body_producer,
		)

		# Is this a web socket request?
		if websocket:
			return await tornado.websocket.websocket_connect(
				req,
				on_message_callback=on_message_callback,
			)

		# Send the request and wait for a response
		res = await self.client.fetch(req)

		# XXX Do we have to catch any errors here?

		# Perform mutual authentication
		if authenticate:
			for header in res.headers.get_list("WWW-Authenticate"):
				# Skip anything that isn't a Negotiate header
				if not header.startswith("Negotiate "):
					continue

				# Fetch the server response
				krb5_server_response = header.removeprefix("Negotiate ")

				# Validate the server response
				result = kerberos.authGSSClientStep(krb5_context, krb5_server_response)
				if not result == kerberos.AUTH_GSS_COMPLETE:
					raise AuthError("Could not verify the Kerberos server response")

				log.debug("Kerberos Server Response validating succeeded")

				# Call this so that we won't end in the else block
				break

			# If there were no headers
			else:
				raise AuthError("Mutual authentication failed")

		# Decode JSON response
		if res.body:
			return json.loads(res.body)

		# Empty response
		return {}

	def _setup_krb5_context(self, url):
		"""
			Creates the Kerberos context that can be used to perform client
			authentication against the server, and mutual authentication for the server.
		"""
		# Parse the input URL
		url = urllib.parse.urlparse(url)

		# Create a new client context
		result, krb5_context = kerberos.authGSSClientInit("HTTP@%s" % url.hostname)

		if not result == kerberos.AUTH_GSS_COMPLETE:
			raise AuthError("Could not create Kerberos Client context")

		# Next step...
		try:
			result = kerberos.authGSSClientStep(krb5_context, "")

		except kerberos.GSSError as e:
			log.error("Kerberos authentication failed: %s" % e)

			raise AuthError("%s" % e) from e

		if not result == kerberos.AUTH_GSS_CONTINUE:
			raise AuthError("Cloud not continue Kerberos authentication")

		return krb5_context

	# Test functions

	async def test(self):
		"""
			Tests the connection
		"""
		return await self._request("GET", "/test")

	# Build actions

	def get_build(self, uuid):
		return self._request("/builds/%s" % uuid, decode="json")

	async def build(self, path, repo=None, arches=None):
		"""
			Create a new build on the hub
		"""
		# Upload the souce file to the server
		upload_id = await self.upload(path)

		log.debug("%s has been uploaded as %s" % (path, upload_id))

		# Create a new build
		build_id = await self._request("POST", "/builds",
			upload_id=upload_id, repo=repo, arches=arches)

		log.debug("Build creates as %s" % build_id)

		return build_id

	# Job actions

	def get_job(self, uuid):
		return self._request("/jobs/%s" % uuid, decode="json")

	# Package actions

	def get_package(self, uuid):
		return self._request("/packages/%s" % uuid, decode="json")

	# Uploads

	async def list_uploads(self):
		"""
			Returns a list of all uploads
		"""
		response = await self._request("GET", "/uploads")

		return response.get("uploads")

	async def upload(self, path, filename=None, show_progress=True):
		"""
			Uploads the file to the hub returning the upload ID
		"""
		log.debug("Uploading %s..." % path)

		# Use the basename of the file if no name was given
		if filename is None:
			filename = os.path.basename(path)

		# Determine the filesize
		size = os.path.getsize(path)

		# Make progressbar
		if show_progress:
			p = _pakfire.Progressbar()
			p.add_string(_("Uploading %s") % filename)
			p.add_percentage()
			p.add_bar()
			p.add_transfer_speed()
			p.add_string("|")
			p.add_bytes_transferred()
			p.add_eta()
		else:
			p = None

		# Compute a digest
		digest = self._compute_digest("blake2b", path)

		# Prepare the file for streaming
		body_producer = functools.partial(self._stream_file, path, size, p)

		# Perform upload
		response = await self._request("PUT", "/uploads",
			body_producer=body_producer,
			filename=filename, size=size, digest=digest
		)

		# Return the upload ID
		return response.get("id")

	async def delete_upload(self, upload_id):
		await self._request("DELETE", "/uploads", id=upload_id)

	async def upload_multi(self, *paths, show_progress=True):
		"""
			Upload multiple files

			If one file could not be uploaded, any other uploads will be deleted
		"""
		uploads = []

		# Upload everything
		try:
			for path in paths:
				upload = await self.upload(path, show_progress=show_progress)

				# Store the upload ID
				uploads.append(upload)

		except Exception as e:
			# Remove any previous uploads
			await asyncio.gather(
				*(self.delete_upload(upload) for upload in uploads),
			)

			# Raise the exception
			raise e

		# Return the IDs of the uploads
		return uploads

	@staticmethod
	def _stream_file(path, size, p, write):
		# Start the progressbar
		if p:
			p.start(size)

		try:
			with open(path, "rb") as f:
				while True:
					buf = f.read(64 * 1024)
					if not buf:
						break

					# Update progressbar
					if p:
						l = len(buf)
						p.increment(l)

					write(buf)
		finally:
			# Finish the progressbar
			if p:
				p.finish()

	@staticmethod
	def _compute_digest(algo, path):
		h = hashlib.new(algo)

		with open(path, "rb") as f:
			while True:
				buf = f.read(64 * 1024)
				if not buf:
					break

				h.update(buf)

		return "%s:%s" % (algo, h.hexdigest())

	# Builder

	async def send_builder_info(self):
		"""
			Sends information about this host to the hub.

			This information is something that doesn't change during
			the lifetime of the daemon.
		"""
		log.info(_("Sending builder information to hub..."))

		# Fetch processor information
		cpu = cpuinfo.get_cpu_info()

		data = {
			# CPU info
			"cpu_model"       : cpu.get("brand_raw"),
			"cpu_count"       : cpu.get("count"),
			"cpu_arch"        : _pakfire.native_arch(),

			# Pakfire + OS
			"pakfire_version" : PAKFIRE_VERSION,
			"os_name"         : util.get_distro_name(),
		}

		# Send request
		await self._request("POST", "/builders/info", **data)

	async def send_builder_stats(self):
		log.debug("Sending stat message to hub...")

		# Fetch processor information
		cpu = psutil.cpu_times_percent()

		# Fetch memory/swap information
		mem  = psutil.virtual_memory()
		swap = psutil.swap_memory()

		# Fetch load average
		loadavg = psutil.getloadavg()

		data = {
			# CPU
			"cpu_user"       : cpu.user,
			"cpu_nice"       : cpu.nice,
			"cpu_system"     : cpu.system,
			"cpu_idle"       : cpu.idle,
			"cpu_iowait"     : cpu.iowait,
			"cpu_irq"        : cpu.irq,
			"cpu_softirq"    : cpu.softirq,
			"cpu_steal"      : cpu.steal,
			"cpu_guest"      : cpu.guest,
			"cpu_guest_nice" : cpu.guest_nice,

			# Load average
			"loadavg1"       : loadavg[0],
			"loadavg5"       : loadavg[1],
			"loadavg15"      : loadavg[2],

			# Memory
			"mem_total"      : mem.total,
			"mem_available"  : mem.available,
			"mem_used"       : mem.used,
			"mem_free"       : mem.free,
			"mem_active"     : mem.active,
			"mem_inactive"   : mem.inactive,
			"mem_buffers"    : mem.buffers,
			"mem_cached"     : mem.cached,
			"mem_shared"     : mem.shared,

			# Swap
			"swap_total"     : swap.total,
			"swap_used"      : swap.used,
			"swap_free"      : swap.free,
		}

		# Send request
		await self._request("POST", "/builders/stats", **data)

	async def queue(self, job_received_callback):
		"""
			Connects to the hub and asks for a build job
		"""
		on_message_callback = functools.partial(
			self._decode_json_message, job_received_callback)

		# Join the queue
		queue = await self._request("GET", "/queue", websocket=True, ping=10,
			on_message_callback=on_message_callback)

		log.debug("Joined the queue")

		return queue

	@staticmethod
	def _decode_json_message(callback, message):
		"""
			Takes a received message and decodes it.

			It will then call the callback with the decoded message.
		"""
		# Ignore empty messages
		if message is None:
			return

		try:
			message = json.loads(message)
		except json.JSONDecodeError:
			log.error("Could not decode JSON message:\n%s" % message)
			return

		return callback(message)

	async def job(self, id):
		"""
			Connects to the given job
		"""
		# Connect to the hub
		conn = await self._request("GET", "/jobs/%s/builder" % id,
			websocket=True, ping=10)

		# Return a Job proxy
		return Job(self, id, conn)

	async def finish_job(self, job_id, success, packages=None, log=None):
		"""
			Will tell the hub that a job has finished
		"""
		# Upload the log file
		if log:
			log = await self.upload(log, filename="%s.log" % job_id)

		# Upload the packages
		if packages:
			packages = await self.upload_multi(*packages)

		# Send the request
		response = await self._request("POST", "/jobs/%s/finished" % job_id,
			success=success, log=log, packages=packages)

		# Handle the response
		# XXX TODO


class Job(object):
	"""
		Proxy for Build Jobs
	"""
	def __init__(self, hub, id, conn):
		self.hub  = hub
		self.id   = id
		self.conn = conn

	async def _write_message(self, message, **kwargs):
		"""
			Sends a message but encodes it into JSON first
		"""
		if isinstance(message, dict):
			message = tornado.escape.json_encode(message)

		return await self.conn.write_message(message, **kwargs)

	async def status(self, status):
		"""
			Sends a new status to the hub
		"""
		await self._write_message({
			"message" : "status",
			"status"  : status,
		})

	async def log(self, level, message):
		"""
			Sends a log message to the hub
		"""
		await self._write_message({
			"message" : "log",
			"level"   : level,
			"log"     : message,
		})
