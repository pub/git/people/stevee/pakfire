/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_DOWNLOADER_H
#define PAKFIRE_DOWNLOADER_H

#ifdef PAKFIRE_PRIVATE

struct pakfire_downloader;
struct pakfire_mirrorlist;

#include <pakfire/digest.h>
#include <pakfire/pakfire.h>

enum pakfire_transfer_flags {
	PAKFIRE_TRANSFER_NONE				= 0,
	PAKFIRE_TRANSFER_NOPROGRESS			= (1 << 0),
	PAKFIRE_TRANSFER_NOTEMP				= (1 << 1),
};

int pakfire_downloader_create(struct pakfire_downloader** downloader, struct pakfire* pakfire);

struct pakfire_downloader* pakfire_downloader_ref(struct pakfire_downloader* downloader);
struct pakfire_downloader* pakfire_downloader_unref(struct pakfire_downloader* downloader);

int pakfire_downloader_retrieve(
	struct pakfire_downloader* downloader,
	const char* baseurl,
	struct pakfire_mirrorlist* mirrors,
	const char* title,
	const char* url,
	const char* path,
	enum pakfire_digest_types md,
	const unsigned char* expected_digest,
	const size_t expected_digest_length,
	enum pakfire_transfer_flags flags);
int pakfire_downloader_add_transfer(
	struct pakfire_downloader* downloader,
	const char* baseurl,
	struct pakfire_mirrorlist* mirrors,
	const char* title,
	const char* url,
	const char* path,
	enum pakfire_digest_types md,
	const unsigned char* expected_digest,
	const size_t expected_digest_length,
	enum pakfire_transfer_flags flags);

int pakfire_downloader_run(struct pakfire_downloader* downloader);

// Mirror lists

int pakfire_mirrorlist_create(struct pakfire_mirrorlist** ml, struct pakfire* pakfire);

struct pakfire_mirrorlist* pakfire_mirrorlist_ref(struct pakfire_mirrorlist* ml);
struct pakfire_mirrorlist* pakfire_mirrorlist_unref(struct pakfire_mirrorlist* ml);

int pakfire_mirrorlist_read(struct pakfire_mirrorlist* ml, const char* path);
int pakfire_mirrorlist_add_mirror(struct pakfire_mirrorlist* ml, const char* url);

int pakfire_mirrorlist_empty(struct pakfire_mirrorlist* ml);
struct pakfire_mirror* pakfire_mirrorlist_first(struct pakfire_mirrorlist* ml);

#endif

#endif /* PAKFIRE_DOWNLOADER_H */
