/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <pakfire/dependencies.h>
#include <pakfire/parser.h>
#include <pakfire/util.h>

#include "../testsuite.h"

static const char* relations[] = {
	// Simple packages
	"beep",
	"bash",
	"kernel-devel",

	// Packages with a specific version
	"beep = 1",
	"bash = 2",
	"kernel-devel = 3",

	// Packages with version and release
	"beep = 1-1.ip3",
	"bash = 2-2.ip3",
	"kernel-devel = 3-3.ip3",

	// Packages with version, release and arch
	"beep = 1-1.ip3.x86_64",
	"bash = 2-2.ip3.aarch64",
	"kernel-devel = 3-3.x86_64",

	// Relations where there is no space
	//"ncurses-base=5.9-11.20150117.ip3",

	// Packages with a version greater/smaller/greater or equal/smaller or equal than
	"beep > 1",
	"bash > 2",
	"kernel-devel > 3",
	"beep < 1",
	"bash < 2",
	"kernel-devel < 3",
	"beep >= 1",
	"bash >= 2",
	"kernel-devel >= 3",
	"beep >= 1",
	"bash >= 2",
	"kernel-devel >= 3",

	// Namespaces
	"pakfire(test)",
	"arch(x86_64)",

	// Rich dependencies
	"(foo if bar else baz)",
	"(foo if bar else baz >= 2)",
	"(foo >= 1.0 with foo < 2.0)",
	"(kernel with flavor = desktop)",
	"(beep or bash or kernel)",
	"((pkg1 with feature2) or (pkg2 without feature1))",
	"(foo or (bar and baz))",

	// Nested rich dependencies
	"(beep or (bash and kernel))",
	"(beep or (bash and kernel) or foo)",

	NULL,
};

static const char* invalid_relations[] = {
	"beep something-else",
	"beep >= 1 something else",

	// Broken rich deps
	"()",
	"(a ",
	"(a unless)",
	"(a unless )",

	// Broken nested rich deps
	"(a and (b or c)",

	NULL,
};

static const char* more_relations[] = {
	// Dependencies with excess space
	"pkgconfig(blah) ",
	"pkgconfig(glib-2.0)  ",
	"kernel ",
	" kernel",
	"kernel >= 1 ",
	" kernel >= 1",

	// With a trailing newline
	"dependency\n",

	NULL,
};

static int test_dependencies(const struct test* t) {
	Id dep = 0;
	const char* result = NULL;

	// Check some invalid inputs
	ASSERT_ERRNO(pakfire_str2dep(t->pakfire, NULL) == 0, EINVAL);
	ASSERT_ERRNO(pakfire_str2dep(t->pakfire, "") == 0, EINVAL);

	// Check some valid inputs
	for (const char** relation = relations; *relation; relation++) {
		printf("Parsing '%s'...\n", *relation);

		// Parse relation
		dep = pakfire_str2dep(t->pakfire, *relation);

		// Convert it back to string
		result = pakfire_dep2str(t->pakfire, dep);
		ASSERT(result);

		// Check if the output matches the input
		ASSERT_STRING_EQUALS(*relation, result);
	}

	// Check invalid inputs
	for (const char** relation = invalid_relations; *relation; relation++) {
		printf("Parsing '%s'...\n", *relation);

		// Parse relation
		dep = pakfire_str2dep(t->pakfire, *relation);

		// The return value must be ID_NULL
		ASSERT(dep == ID_NULL);
	}

	// Check some more relations without comparing the result
	for (const char** relation = more_relations; *relation; relation++) {
		printf("Parsing '%s'...\n", *relation);

		// Parse relation
		ASSERT(dep = pakfire_str2dep(t->pakfire, *relation));

		// Convert it back to string
		ASSERT(result = pakfire_dep2str(t->pakfire, dep));

		printf("  as '%s'\n", result);
	}

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_dep_match(const struct test* t) {
	struct pakfire_package* pkg = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_package_create(&pkg, t->pakfire, NULL,
		"test", "1.0-1", "x86_64"));

	// Check if the package matches itself
	ASSERT_TRUE(pakfire_package_matches_dep(pkg, PAKFIRE_PKG_PROVIDES, "test"));
	ASSERT_TRUE(pakfire_package_matches_dep(pkg, PAKFIRE_PKG_PROVIDES, "test = 1.0-1"));

	// Add a couple of things this package provides
	ASSERT_SUCCESS(pakfire_package_add_dep(pkg, PAKFIRE_PKG_PROVIDES, "a = 1"));
	ASSERT_SUCCESS(pakfire_package_add_dep(pkg, PAKFIRE_PKG_PROVIDES, "b"));

	// Check if the package matches those dependencies
	ASSERT_TRUE(pakfire_package_matches_dep(pkg, PAKFIRE_PKG_PROVIDES, "a"));
	ASSERT_TRUE(pakfire_package_matches_dep(pkg, PAKFIRE_PKG_PROVIDES, "a = 1"));
	ASSERT_TRUE(pakfire_package_matches_dep(pkg, PAKFIRE_PKG_PROVIDES, "a >= 1"));
	ASSERT_TRUE(pakfire_package_matches_dep(pkg, PAKFIRE_PKG_PROVIDES, "a <= 1"));

	ASSERT_TRUE(pakfire_package_matches_dep(pkg, PAKFIRE_PKG_PROVIDES, "b"));
	ASSERT_TRUE(pakfire_package_matches_dep(pkg, PAKFIRE_PKG_PROVIDES, "b = 1"));

	// Check for something that doesn't exist
	ASSERT_FALSE(pakfire_package_matches_dep(pkg, PAKFIRE_PKG_PROVIDES, "c"));
	ASSERT_FALSE(pakfire_package_matches_dep(pkg, PAKFIRE_PKG_PROVIDES, "c = 2"));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (pkg)
		pakfire_package_unref(pkg);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_dependencies);
	testsuite_add_test(test_dep_match);

	return testsuite_run(argc, argv);
}
