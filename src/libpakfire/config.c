/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>

#include <pakfire/config.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define KEY_MAX_LENGTH		32
#define VALUE_MAX_LENGTH	4096
#define SECTION_MAX_LENGTH	32
#define LINE_MAX_LENGTH		1024

struct pakfire_config_entry {
	STAILQ_ENTRY(pakfire_config_entry) nodes;

	// Section name
	char section[SECTION_MAX_LENGTH];

	// Key & Value
	char key[KEY_MAX_LENGTH];
	char value[VALUE_MAX_LENGTH];
};

struct pakfire_config {
	int nrefs;

	STAILQ_HEAD(entries, pakfire_config_entry) entries;
};

int pakfire_config_create(struct pakfire_config** config) {
	struct pakfire_config* c = calloc(1, sizeof(*c));
	if (!c)
		return 1;

	// Initialize reference counter
	c->nrefs = 1;

	// Initialise entries
	STAILQ_INIT(&c->entries);

	*config = c;
	return 0;
}

static void pakfire_config_entry_free(struct pakfire_config_entry* entry) {
	free(entry);
}

static void pakfire_config_free(struct pakfire_config* config) {
	while (!STAILQ_EMPTY(&config->entries)) {
		struct pakfire_config_entry* entry = STAILQ_FIRST(&config->entries);
		STAILQ_REMOVE_HEAD(&config->entries, nodes);

		pakfire_config_entry_free(entry);
	}

	free(config);
}

struct pakfire_config* pakfire_config_ref(struct pakfire_config* config) {
	++config->nrefs;

	return config;
}

struct pakfire_config* pakfire_config_unref(struct pakfire_config* config) {
	if (--config->nrefs > 0)
		return config;

	pakfire_config_free(config);
	return NULL;
}

static int pakfire_config_entry_is_multiline(const struct pakfire_config_entry* entry) {
	const char* p = strchr(entry->value, '\n');

	if (p)
		return 1;

	return 0;
}

static struct pakfire_config_entry* pakfire_config_create_entry(
		struct pakfire_config* config, const char* section, const char* key) {
	struct pakfire_config_entry* entry = calloc(1, sizeof(*entry));
	if (!entry)
		return NULL;

	int r;

	// Store section
	r = pakfire_string_set(entry->section, section);
	if (r)
		goto ERROR;

	// Store key
	r = pakfire_string_set(entry->key, key);
	if (r)
		goto ERROR;

	// Append it to the list of entries
	STAILQ_INSERT_TAIL(&config->entries, entry, nodes);

	return entry;

ERROR:
	pakfire_config_entry_free(entry);
	return NULL;
}

static struct pakfire_config_entry* pakfire_config_find(struct pakfire_config* config,
		const char* section, const char* key) {
	struct pakfire_config_entry* entry = NULL;

	// Check for valid input
	if (!key) {
		errno = EINVAL;
		return NULL;
	}

	// Treat NULL as an empty section
	if (!section)
		section = "";

	STAILQ_FOREACH(entry, &config->entries, nodes) {
		// Section must match
		if (strcmp(entry->section, section) != 0)
			continue;

		// Key must match
		if (strcmp(entry->key, key) != 0)
			continue;

		// Match!
		return entry;
	}

	// No match
	return NULL;
}

int pakfire_config_set(struct pakfire_config* config,
		const char* section, const char* key, const char* value) {
	// Check if this entry exists
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	// Create a new entry if it doesn't
	if (!entry) {
		entry = pakfire_config_create_entry(config, section, key);
		if (!entry)
			return 1;
	}

	// Store the value
	return pakfire_string_set(entry->value, value);
}

int pakfire_config_set_format(struct pakfire_config* config,
		const char* section, const char* key, const char* format, ...) {
	char* buffer = NULL;
	va_list args;
	int r;

	va_start(args, format);
	r = vasprintf(&buffer, format, args);
	va_end(args);

	// Break on any errors
	if (r < 0)
		return r;

	// Set the value
	r = pakfire_config_set(config, section, key, buffer);

	// Cleanup
	if (buffer)
		free(buffer);

	return r;
}

static int pakfire_config_append(struct pakfire_config* config,
		const char* section, const char* key, const char* value2) {
	char* buffer = NULL;
	int r;

	// Fetch the current value
	const char* value1 = pakfire_config_get(config, section, key, NULL);

	// Short cut if value1 is empty
	if (!value1 || !*value1)
		return pakfire_config_set(config, section, key, value2);

	// Join both values together
	r = asprintf(&buffer, "%s\n%s", value1, value2);
	if (r < 0)
		return 1;

	// Set the new value
	r = pakfire_config_set(config, section, key, buffer);

	if (buffer)
		free(buffer);

	return r;
}

const char* pakfire_config_get(struct pakfire_config* config,
		const char* section, const char* key, const char* _default) {
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	// Return the value if set
	if (entry && *entry->value)
		return entry->value;

	// Otherwise return the default value
	return _default;
}

long int pakfire_config_get_int(struct pakfire_config* config,
		const char* section, const char* key, long int _default) {
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	// Return the value if set
	if (entry && *entry->value)
		return strtol(entry->value, NULL, 10);

	// Otherwise return the default value
	return _default;
}

int pakfire_config_get_bool(struct pakfire_config* config,
		const char* section, const char* key, int _default) {
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	// Return the value if set
	if (entry && *entry->value) {
		static const struct boolean {
			const char* string;
			int ret;
		} booleans[] = {
			{ "true",  1 },
			{ "yes",   1 },
			{ "on",    1 },
			{ "1",     1 },
			{ "false", 0 },
			{ "no",    0 },
			{ "off",   0 },
			{ "0",     0 },
			{ NULL,    0 },
		};

		for (const struct boolean* boolean = booleans; boolean->string; boolean++) {
			if (strcmp(entry->value, boolean->string) == 0)
				return boolean->ret;
		}
	}

	// Otherwise return the default value
	return _default;
}

size_t pakfire_config_get_bytes(struct pakfire_config* config,
		const char* section, const char* key, const size_t _default) {
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	if (entry && *entry->value)
		return pakfire_string_parse_bytes(entry->value);

	// Otherwise return the default
	return _default;
}

FILE* pakfire_config_get_fopen(struct pakfire_config* config,
		const char* section, const char* key) {
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	// Return the value if set
	if (entry && *entry->value)
		return fmemopen(entry->value, strlen(entry->value), "r");

	return NULL;
}

static int pakfire_section_in_sections(char** sections, const char* section) {
	if (!sections)
		return 0;

	for (char** s = sections; *s; s++) {
		if (strcmp(section, *s) == 0)
			return 1;
	}

	return 0;
}

char** pakfire_config_sections(struct pakfire_config* config) {
	struct pakfire_config_entry* entry = NULL;

	char** sections = NULL;
	size_t num_sections = 0;

	STAILQ_FOREACH(entry, &config->entries, nodes) {
		// Skip empty sections
		if (!*entry->section)
			continue;

		// Skip if this section is already on the list
		if (pakfire_section_in_sections(sections, entry->section))
			continue;

		// Make space in the array
		sections = reallocarray(sections, sizeof(*sections), ++num_sections + 1);
		if (!sections)
			return NULL;

		sections[num_sections - 1] = strdup(entry->section);

		// Terminate the array
		sections[num_sections] = NULL;
	}

	return sections;
}

int pakfire_config_has_section(struct pakfire_config* config, const char* section) {
	struct pakfire_config_entry* entry = NULL;

	STAILQ_FOREACH(entry, &config->entries, nodes) {
		if (strcmp(entry->section, section) == 0)
			return 1;
	}

	return 0;
}

static ssize_t lstrip(char* s) {
	if (!s)
		return 0;

	size_t l = strlen(s);

	// Remove leading space
	while (isspace(s[0]))
		memmove(s, s + 1, l--);

	return l;
}

static ssize_t rstrip(char* s) {
	if (!s)
		return 0;

	size_t l = strlen(s);

	// Remove trailing space
	while (isspace(s[l - 1]))
		s[l-- - 1] = '\0';

	return l;
}

static ssize_t strip(char* s) {
	ssize_t l = lstrip(s);

	if (l)
		l = rstrip(s);

	return l;
}

int pakfire_config_read(struct pakfire_config* config, FILE* f) {
	char section[SECTION_MAX_LENGTH] = "";
	char key[KEY_MAX_LENGTH] = "";
	int r;

	char* line = NULL;
	size_t l = 0;

	while (!feof(f)) {
		ssize_t length = getline(&line, &l, f);
		if (length < 0)
			break;

		// Break if there was an error
		if (ferror(f))
			break;

		// Terminate line after comment sign
		char* comment = strchr(line, '#');
		if (comment) {
			*comment = '\0';
			length = comment - line;
		}

		// Remove trailing space
		length = rstrip(line);

		// Skip empty lines
		if (*line == '\0')
			continue;

		// Is this a new section?
		if (line[0] == '[' && line[length - 1] == ']') {
			line[length - 1] = '\0';

			// Clear the previous key
			*key = '\0';

			r = pakfire_string_set(section, line + 1);
			if (r)
				goto ERROR;

			continue;
		}

		// Check if this is a multiline value
		if (isspace(*line)) {
			// We cannot assign this value anywhere
			if (!*key) {
				errno = EINVAL;
				r = 1;
				goto ERROR;
			}

			// Strip any leading whitespace
			lstrip(line);

			// Append the value to the existing one
			r = pakfire_config_append(config, section, key, line);
			if (r)
				goto ERROR;

			// Skip to the next line
			continue;
		}

		// Handle any assignments

		// Find the separator
		char* value = strchr(line, '=');
		if (!value)
			continue;

		// Split the string
		*value++ = '\0';

		// Remove trailing space
		strip(line);
		strip(value);

		// Update the key
		pakfire_string_set(key, line);

		// Store the value
		r = pakfire_config_set(config, section, key, value);
		if (r)
			goto ERROR;
	}

ERROR:
	if (line)
		free(line);

	return r;
}

static int pakfire_config_dump_multiline_entry(struct pakfire_config_entry* entry, FILE* f) {
	char* value = NULL;
	char* p = NULL;
	int r;

	// Write the key line
	r = fprintf(f, "%s =\n", entry->key);
	if (r <= 0)
		goto ERROR;

	// Copy the value
	value = strdup(entry->value);
	if (!value) {
		r = 1;
		goto ERROR;
	}

	// Write it line by line
	char* line = strtok_r(value, "\n", &p);
	while (line) {
		r = fprintf(f, "  %s\n", line);
		if (r <= 0)
			goto ERROR;

		line = strtok_r(NULL, "\n", &p);
	}

	// Success
	r = 0;

ERROR:
	if (value)
		free(value);

	return r;
}

static int pakfire_config_dump_entry(struct pakfire_config_entry* entry, FILE* f) {
	int r;

	// Handle multiline entries separately
	if (pakfire_config_entry_is_multiline(entry))
		return pakfire_config_dump_multiline_entry(entry, f);

	// Write the entry
	r = fprintf(f, "%s = %s\n", entry->key, entry->value);
	if (r <= 0)
		return r;

	return 0;
}

static int pakfire_config_dump_section(struct pakfire_config* config,
		const char* section, FILE* f) {
	struct pakfire_config_entry* entry = NULL;
	int r = 0;

	// Print the section name
	if (*section) {
		r = fprintf(f, "\n[%s]\n", section);
		if (r <= 0)
			goto ERROR;
	}

	// Iterate over all entries
	STAILQ_FOREACH(entry, &config->entries, nodes) {
		// Dump all entries that match this section
		if (strcmp(entry->section, section) == 0) {
			r = pakfire_config_dump_entry(entry, f);
			if (r)
				goto ERROR;
		}
	}

	// Success
	r = 0;

ERROR:
	return r;
}

int pakfire_config_dump(struct pakfire_config* config, FILE* f) {
	int r;

	// Fetch a list of all available sections
	char** sections = pakfire_config_sections(config);

	// Dump the default section
	r = pakfire_config_dump_section(config, "", f);
	if (r)
		goto ERROR;

	// Dump all sections
	if (sections) {
		for (char** section = sections; *section; section++) {
			r = pakfire_config_dump_section(config, *section, f);
			if (r)
				goto ERROR;
		}
	}

ERROR:
	if (sections) {
		for (char** section = sections; *section; section++)
			free(*section);
		free(sections);
	}

	return r;
}
