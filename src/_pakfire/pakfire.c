/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <errno.h>

#include <pakfire/archive.h>
#include <pakfire/build.h>
#include <pakfire/constants.h>
#include <pakfire/dist.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/mount.h>
#include <pakfire/packagelist.h>
#include <pakfire/pakfire.h>
#include <pakfire/key.h>
#include <pakfire/repo.h>
#include <pakfire/repolist.h>
#include <pakfire/request.h>
#include <pakfire/transaction.h>
#include <pakfire/util.h>

#include "archive.h"
#include "errors.h"
#include "key.h"
#include "pakfire.h"
#include "repo.h"
#include "util.h"

static PyObject* Pakfire_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	PakfireObject* self = (PakfireObject *)type->tp_alloc(type, 0);
	if (self) {
		self->pakfire = NULL;

		// Callbacks
		self->callbacks.log = NULL;
		self->callbacks.confirm = NULL;
	}

	return (PyObject *)self;
}

static void Pakfire_log_callback(void* data, int priority, const char* file, int line,
		const char* fn, const char* format, va_list args) {
	PyObject* callback = (PyObject*)data;
	PyObject* exception = NULL;

	// Do nothing if callback isn't set
	if (!callback)
		return;

	PyGILState_STATE state = PyGILState_Ensure();

	// Translate priority to Python logging priorities
	switch (priority) {
		case LOG_DEBUG:
			priority = 10;
			break;

		case LOG_INFO:
			priority = 20;
			break;

		case LOG_ERR:
			priority = 40;
			break;

		// Drop messages of an unknown priority
		default:
			return;
	}

	PyObject* tuple = NULL;
	PyObject* result = NULL;
	char* buffer = NULL;

	// Make line
	int r = vasprintf(&buffer, format, args);
	if (r < 0)
		goto ERROR;

	// Build a tuple with the priority and the log message
	tuple = Py_BuildValue("(is)", priority, buffer);
	if (!tuple)
		goto ERROR;

	// Call the callback
	result = PyObject_CallObject(callback, tuple);

ERROR:
	/*
		We cannot really catch any Python errors here, since we cannot send
		any error codes up the chain.

		So, in order to debug the problem, We will check if an exception has
		occurred and if so, print it to the console.
	*/
	exception = PyErr_Occurred();
	if (exception)
		PyErr_Print();

	if (buffer)
		free(buffer);
	Py_XDECREF(tuple);
	Py_XDECREF(result);

	// Release the GIL
	PyGILState_Release(state);
}

static int Pakfire_confirm_callback(struct pakfire* pakfire, void* data,
		const char* message, const char* question) {
	PyObject* callback = (PyObject*)data;
	PyObject* result = NULL;
	int r = 0;

	// Do nothing if callback isn't set
	if (!callback)
		return 0;

	// Acquire GIL
	PyGILState_STATE state = PyGILState_Ensure();

	PyObject* args = Py_BuildValue("(ss)", message, question);
	if (!args) {
		r = -1;
		goto ERROR;
	}

	result = PyObject_CallObject(callback, args);

	// If the callback raised an exception, we will ignore it and indicate
	// that an error happened, but we cannot re-raise the exception
	if (!result) {
		r = -1;
		goto ERROR;
	}

	// Set the return code
	if (result == Py_True)
		r = 0;
	else
		r = 1;

ERROR:
	Py_DECREF(args);
	Py_DECREF(result);

	// Release the GIL
	PyGILState_Release(state);

	return r;
}

static int Pakfire_init(PakfireObject* self, PyObject* args, PyObject* kwds) {
	char* kwlist[] = {
		"path",
		"arch",
		"logger",
		"offline",
		"conf",
		"confirm_callback",
		NULL,
	};
	const char* path = NULL;
	const char* arch = NULL;
	const char* conf = NULL;
	int offline = 0;
	int r;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "|zzOpzO", kwlist,
			&path, &arch, &self->callbacks.log, &offline, &conf, &self->callbacks.confirm))
		return -1;

	// Check if log callback is callable
	if (self->callbacks.log && !PyCallable_Check(self->callbacks.log)) {
		PyErr_SetString(PyExc_TypeError, "logger must be callable\n");
		return -1;
	}

	// Check if confirm callback is callable
	if (self->callbacks.confirm && !PyCallable_Check(self->callbacks.confirm)) {
		PyErr_SetString(PyExc_TypeError, "Confirm callback is not callable");
		return -1;
	}

	int flags = 0;

	// Enable offline mode
	if (offline)
		flags |= PAKFIRE_FLAGS_OFFLINE;

	// Configure callbacks
	if (self->callbacks.log)
		Py_INCREF(self->callbacks.log);

	Py_BEGIN_ALLOW_THREADS

	// Create a new Pakfire instance
	r = pakfire_create(&self->pakfire, path, arch, conf, flags,
		LOG_DEBUG, Pakfire_log_callback, self->callbacks.log);

	Py_END_ALLOW_THREADS

	if (r) {
		switch (errno) {
			// Invalid architecture or path
			case EINVAL:
				PyErr_SetString(PyExc_ValueError, "Invalid architecture or path");
				break;

			// Anything else
			default:
				PyErr_SetFromErrno(PyExc_OSError);
		}

		return -1;
    }

	// Configure confirm callback
	if (self->callbacks.confirm) {
		pakfire_set_confirm_callback(self->pakfire,
			Pakfire_confirm_callback, self->callbacks.confirm);

		Py_INCREF(self->callbacks.confirm);
	}

	return 0;
}

static void Pakfire_dealloc(PakfireObject* self) {
	if (self->pakfire) {
		// Reset log callback
		if (self->callbacks.log) {
			pakfire_set_log_callback(self->pakfire, NULL, NULL);
			Py_DECREF(self->callbacks.log);
		}

		// Reset confirm callback
		if (self->callbacks.confirm) {
			pakfire_set_confirm_callback(self->pakfire, NULL, NULL);
			Py_DECREF(self->callbacks.confirm);
		}

		Py_BEGIN_ALLOW_THREADS

		pakfire_unref(self->pakfire);

		Py_END_ALLOW_THREADS
	}

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static PyObject* Pakfire_repr(PakfireObject* self) {
	const char* path = pakfire_get_path(self->pakfire);
	const char* arch = pakfire_get_arch(self->pakfire);

	return PyUnicode_FromFormat("<_pakfire.Pakfire %s (%s)>", path, arch);
}

static PyObject* Pakfire_get_path(PakfireObject* self) {
	const char* path = pakfire_get_path(self->pakfire);

	return PyUnicode_FromString(path);
}

static PyObject* Pakfire_get_arch(PakfireObject* self) {
	const char* arch = pakfire_get_arch(self->pakfire);

	return PyUnicode_FromString(arch);
}

static PyObject* Pakfire_get_repo(PakfireObject* self, PyObject* args) {
	const char* name = NULL;

	if (!PyArg_ParseTuple(args, "s", &name))
		return NULL;

	struct pakfire_repo* repo = pakfire_get_repo(self->pakfire, name);
	if (!repo)
		Py_RETURN_NONE;

	PyObject* obj = new_repo(&RepoType, repo);
	pakfire_repo_unref(repo);

	return obj;
}

static void Pakfire_status_callback(struct pakfire* pakfire, void* data,
		int progress, const char* status) {
	PyObject* callback = (PyObject*)data;

	// Do not attempt to call nothing
	if (!callback)
		return;

	// Acquire GIL
	PyGILState_STATE state = PyGILState_Ensure();

	// Compile arguments
	PyObject* args = Py_BuildValue("(is)", progress, status);
	if (args) {
		// Call the callback
		PyObject* result = PyObject_CallObject(callback, args);

		Py_XDECREF(result);
		Py_DECREF(args);
	}

	// Release the GIL
	PyGILState_Release(state);
}

static int convert_packages(PyObject* object, void* address) {
	char*** packages = (char***)address;

	// Called for cleanup
	if (!object)
		goto ERROR;

	// Nothing to do when object is None
	if (object == Py_None)
		return Py_CLEANUP_SUPPORTED;

	if (!PySequence_Check(object)) {
		PyErr_SetString(PyExc_ValueError, "Packages must be a sequence");
		goto ERROR;
	}

	const unsigned int length = PySequence_Length(object);
	if (!length)
		return Py_CLEANUP_SUPPORTED;

	// Allocate array
	*packages = calloc(length + 1, sizeof(*packages));
	if (!*packages) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	for (unsigned int i = 0; i < length; i++) {
		PyObject* item = PySequence_GetItem(object, i);

		// Check if input is a string
		if (!PyUnicode_Check(item)) {
			Py_DECREF(item);

			PyErr_SetString(PyExc_AttributeError, "Expected a string");
			goto ERROR;
		}

		// Fetch string
		const char* package = PyUnicode_AsUTF8(item);
		if (!package) {
			Py_DECREF(item);
			goto ERROR;
		}

		// Add package to array
		(*packages)[i] = strdup(package);
		if (!(*packages)[i]) {
			Py_DECREF(item);
			goto ERROR;
		}

		Py_DECREF(item);
	}

	// Success
	return Py_CLEANUP_SUPPORTED;

ERROR:
	if (*packages) {
		for (char** package = *packages; *package; package++)
			free(*package);
		free(*packages);
	}

	return 0;
}

static PyObject* Pakfire_install(PakfireObject* self, PyObject* args, PyObject* kwargs) {
	char* kwlist[] = {
		"packages",
		"dryrun",
		"without_recommended",
		"allow_uninstall",
		"allow_downgrade",
		"status_callback",
		NULL
	};
	char** packages = NULL;
	int dryrun = 0;
	int without_recommended = 0;
	int allow_uninstall = 0;
	int allow_downgrade = 0;
	int solver_flags = 0;
	int transaction_flags = 0;
	PyObject* status_callback = NULL;
	int r;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O&|$ppppO", kwlist,
			convert_packages, &packages, &dryrun, &without_recommended, &allow_uninstall,
			&allow_downgrade, &status_callback))
		return NULL;

	// Check if callback is callable
	if (status_callback && !PyCallable_Check(status_callback)) {
		PyErr_SetString(PyExc_TypeError, "status_callback must be callable");
		return NULL;
	}

	// Enable dry-run mode
	if (dryrun)
		transaction_flags |= PAKFIRE_TRANSACTION_DRY_RUN;

	// Do not install recommended packages
	if (without_recommended)
		solver_flags |= PAKFIRE_REQUEST_WITHOUT_RECOMMENDED;

	// Can the solver uninstall packages?
	if (allow_uninstall)
		solver_flags |= PAKFIRE_REQUEST_ALLOW_UNINSTALL;

	// Can the solver downgrade packages?
	if (allow_downgrade)
		solver_flags |= PAKFIRE_REQUEST_ALLOW_DOWNGRADE;

	Py_BEGIN_ALLOW_THREADS

	// Run pakfire_install
	r = pakfire_install(self->pakfire, transaction_flags, solver_flags,
		(const char**)packages, NULL, 0, NULL, Pakfire_status_callback, status_callback);

	Py_END_ALLOW_THREADS

	if (r)
		PyErr_SetFromErrno(PyExc_OSError);

	if (packages) {
		for (char** package = packages; *package; package++)
			free(*package);
		free(packages);
	}

	if (r)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject* Pakfire_erase(PakfireObject* self, PyObject* args, PyObject* kwargs) {
	char* kwlist[] = {
		"packages",
		"dryrun",
		"keep_dependencies",
		"status_callback",
		NULL
	};
	char** packages = NULL;
	int dryrun = 0;
	int keep_dependencies = 0;
	int transaction_flags = 0;
	int flags = 0;
	PyObject* status_callback = NULL;
	int r;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O&|$ppO", kwlist,
			convert_packages, &packages, &dryrun, &keep_dependencies, &status_callback))
		return NULL;

	// Check if callback is callable
	if (status_callback && !PyCallable_Check(status_callback)) {
		PyErr_SetString(PyExc_TypeError, "status_callback must be callable");
		return NULL;
	}

	if (dryrun)
		transaction_flags |= PAKFIRE_TRANSACTION_DRY_RUN;

	if (keep_dependencies)
		flags |= PAKFIRE_REQUEST_KEEP_DEPS;

	Py_BEGIN_ALLOW_THREADS

	// Run pakfire_erase
	r = pakfire_erase(self->pakfire, transaction_flags, 0, (const char**)packages,
		NULL, flags, NULL, Pakfire_status_callback, status_callback);

	Py_END_ALLOW_THREADS

	if (r)
		PyErr_SetFromErrno(PyExc_OSError);

	if (packages) {
		for (char** package = packages; *package; package++)
			free(*package);
		free(packages);
	}

	if (r)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject* Pakfire_update(PakfireObject* self, PyObject* args, PyObject* kwargs) {
	char* kwlist[] = {
		"packages",
		"dryrun",
		"excludes",
		"allow_uninstall",
		"allow_downgrade",
		"status_callback",
		NULL
	};
	char** packages = NULL;
	char** excludes = NULL;
	int dryrun = 0;
	int allow_uninstall = 0;
	int allow_downgrade = 0;
	int solver_flags = 0;
	int transaction_flags = 0;
	PyObject* status_callback = NULL;
	int r;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O&|$pO&ppO", kwlist,
			convert_packages, &packages, &dryrun, convert_packages, &excludes,
			&allow_uninstall, &allow_downgrade, &status_callback))
		return NULL;

	// Check if callback is callable
	if (status_callback && !PyCallable_Check(status_callback)) {
		PyErr_SetString(PyExc_TypeError, "status_callback must be callable");
		return NULL;
	}

	if (dryrun)
		transaction_flags = PAKFIRE_TRANSACTION_DRY_RUN;

	// Can the solver uninstall packages?
	if (allow_uninstall)
		solver_flags |= PAKFIRE_REQUEST_ALLOW_UNINSTALL;

	// Can the solver downgrade packages?
	if (allow_downgrade)
		solver_flags |= PAKFIRE_REQUEST_ALLOW_DOWNGRADE;

	Py_BEGIN_ALLOW_THREADS

	// Run pakfire_update
	r = pakfire_update(self->pakfire, transaction_flags, solver_flags,
		(const char**)packages, (const char**)excludes, 0, NULL,
		Pakfire_status_callback, status_callback);

	Py_END_ALLOW_THREADS

	if (r)
		PyErr_SetFromErrno(PyExc_OSError);

	if (packages) {
		for (char** package = packages; *package; package++)
			free(*package);
		free(packages);
	}

	if (excludes) {
		for (char** exclude = excludes; *exclude; exclude++)
			free(*exclude);
		free(excludes);
	}

	if (r)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject* Pakfire_keys_to_list(struct pakfire_key** keys) {
	PyObject* list = PyList_New(0);

	// Empty input?
	if (!keys)
		return list;

	// Push all keys onto the list
	for (struct pakfire_key** key = keys; *key; key++) {
		PyObject* object = new_key(&KeyType, *key);
		if (!object)
			goto ERROR;

		PyList_Append(list, object);
		Py_DECREF(object);
	}

	return list;

ERROR:
	Py_DECREF(list);
	return NULL;
}

static PyObject* Pakfire_get_keys(PakfireObject* self) {
	struct pakfire_key** keys = NULL;

	int r = pakfire_list_keys(self->pakfire, &keys);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	// Convert keys to list
	PyObject* list = Pakfire_keys_to_list(keys);

	// Free keys
	if (keys) {
		for (struct pakfire_key** key = keys; *key; key++)
			pakfire_key_unref(*key);
		free(keys);
	}

	return list;
}

static PyObject* Pakfire_get_key(PakfireObject* self, PyObject* args) {
	const char* pattern = NULL;

	if (!PyArg_ParseTuple(args, "s", &pattern))
		return NULL;

	// Try finding the key
	struct pakfire_key* key = pakfire_key_get(self->pakfire, pattern);
	if (!key)
		Py_RETURN_NONE;

	PyObject* object = new_key(&KeyType, key);
	pakfire_key_unref(key);

	return object;
}

static PyObject* Pakfire_generate_key(PakfireObject* self, PyObject* args, PyObject* kwds) {
	char* kwlist[] = { "userid", "algorithm", NULL };
	struct pakfire_key* key = NULL;
	const char* userid = NULL;
	const char* algo = NULL;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "s|$z", kwlist, &userid, &algo))
		return NULL;

	// Generate a new key
	int r = pakfire_key_generate(&key, self->pakfire, algo, userid);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	PyObject* object = new_key(&KeyType, key);
	pakfire_key_unref(key);

	return object;
}

static PyObject* Pakfire_import_key(PakfireObject* self, PyObject* args) {
	PyObject* object = NULL;

	if (!PyArg_ParseTuple(args, "O", &object))
		return NULL;

	// Get a file descriptor from object
	int fd = PyObject_AsFileDescriptor(object);
	if (fd < 0)
		return NULL;

	// Convert to FILE*
	FILE* f = fdopen(fd, "r");
	if (!f) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	struct pakfire_key** keys = NULL;

	// Import keys from f
	int r = pakfire_key_import(self->pakfire, f, &keys);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	// Convert keys to list
	PyObject* list = Pakfire_keys_to_list(keys);

	// Free keys
	for (struct pakfire_key** key = keys; *key; key++)
		pakfire_key_unref(*key);
	free(keys);

	return list;
}

static PyObject* Pakfire_fetch_key(PakfireObject* self, PyObject* args, PyObject* kwds) {
	char* kwlist[] = { "userid", "fingerprint", NULL };
	struct pakfire_key* key = NULL;
	const char* userid = NULL;
	const char* fingerprint = NULL;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "|$zz", kwlist, &userid, &fingerprint))
		return NULL;

	// Fetch the key
	int r = pakfire_key_fetch(&key, self->pakfire, userid, fingerprint);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	// Return the result
	if (key) {
		PyObject* object = new_key(&KeyType, key);
		pakfire_key_unref(key);

		return object;
	}

	Py_RETURN_NONE;
}

static PyObject* Pakfire_whatprovides(PakfireObject* self, PyObject* args) {
	const char* provides = NULL;
	struct pakfire_packagelist* list = NULL;
	PyObject* ret = NULL;
	int r;

	if (!PyArg_ParseTuple(args, "s", &provides))
		return NULL;

	// Create a new list
	r = pakfire_packagelist_create(&list, self->pakfire);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	r = pakfire_whatprovides(self->pakfire, provides, 0, list);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Create a Python list from the package list
	ret = PyList_FromPackageList(list);

ERROR:
	if (list)
		pakfire_packagelist_unref(list);

	return ret;
}

static PyObject* Pakfire_whatrequires(PakfireObject* self, PyObject* args) {
	const char* requires = NULL;
	struct pakfire_packagelist* list = NULL;
	PyObject* ret = NULL;
	int r;

	if (!PyArg_ParseTuple(args, "s", &requires))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	// Create a new list
	r = pakfire_packagelist_create(&list, self->pakfire);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	r = pakfire_whatrequires(self->pakfire, requires, 0, list);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	Py_END_ALLOW_THREADS

	// Create a Python list from the package list
	ret = PyList_FromPackageList(list);

ERROR:
	if (list)
		pakfire_packagelist_unref(list);

	return ret;
}

static PyObject* Pakfire_search(PakfireObject* self, PyObject* args, PyObject* kwds) {
	char* kwlist[] = { "pattern", "name_only", NULL };
	struct pakfire_packagelist* list = NULL;
	const char* pattern = NULL;
	int name_only = 0;
	int flags = 0;
	PyObject* ret = NULL;
	int r;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "s|$p", kwlist, &pattern, &name_only))
		return NULL;

	// Search for package names only
	if (name_only)
		flags |= PAKFIRE_SEARCH_NAME_ONLY;

	r = pakfire_packagelist_create(&list, self->pakfire);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	r = pakfire_search(self->pakfire, pattern, flags, list);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	ret = PyList_FromPackageList(list);

ERROR:
	if (list)
		pakfire_packagelist_unref(list);

	return ret;
}

static PyObject* Pakfire_version_compare(PakfireObject* self, PyObject* args) {
	const char* evr1 = NULL;
	const char* evr2 = NULL;

	if (!PyArg_ParseTuple(args, "ss", &evr1, &evr2))
		return NULL;

	int cmp = pakfire_version_compare(self->pakfire, evr1, evr2);

	return PyLong_FromLong(cmp);
}

static int Pakfire_execute_output_callback(struct pakfire* pakfire, void* data,
		int priority, const char* line, size_t length) {
	PyObject* callback = (PyObject*)data;
	int r = 0;

	// Do nothing if callback isn't set
	if (!callback)
		return 0;

	// Translate priority to Python logging priorities
	switch (priority) {
		case LOG_INFO:
			priority = 20;
			break;

		case LOG_ERR:
			priority = 40;
			break;
	}

	// Remove the trailing newline
	if (line && line[length - 1] == '\n')
		length--;

	// Create tuple with arguments for the callback function
	PyObject* args = Py_BuildValue("(is#)", priority, line, (Py_ssize_t)length);
	if (!args)
		return 1;

	PyObject* result = PyObject_CallObject(callback, args);
    if (result && PyLong_Check(result)) {
        r = PyLong_AsLong(result);
    }

	Py_XDECREF(args);
	Py_XDECREF(result);

	return r;
}

static PyObject* Pakfire_execute(PakfireObject* self, PyObject* args, PyObject* kwds) {
	char* kwlist[] = {
		"command",
		"environ",
		"bind",
		"callback",
		"nice",
		NULL
	};

	struct pakfire_jail* jail = NULL;
	const char** argv = NULL;
	int r;
	PyObject* ret = NULL;

	PyObject* command = NULL;
	PyObject* environ = NULL;
	PyObject* bind = NULL;
	PyObject* callback = NULL;
	int nice = 0;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "O|OOOi", kwlist, &command, &environ,
			&bind, &callback, &nice))
		return NULL;

	// Check if command is a list
	if (!PyList_Check(command)) {
		PyErr_SetString(PyExc_TypeError, "command must be a list");
		goto ERROR;
	}

	const ssize_t command_length = PyList_Size(command);

	// Check if command is not empty
	if (command_length == 0) {
		PyErr_SetString(PyExc_ValueError, "command is empty");
		goto ERROR;
	}

	// Allocate argv
	argv = calloc(command_length + 1, sizeof(*argv));
	if (!argv)
		goto ERROR;

	// All arguments in command must be strings
	for (unsigned int i = 0; i < command_length; i++) {
		PyObject* item = PyList_GET_ITEM(command, i);

		if (!PyUnicode_Check(item)) {
			PyErr_Format(PyExc_TypeError, "Item %u in command is not a string", i);
			return NULL;
		}

		// Copy to argv
		argv[i] = PyUnicode_AsUTF8(item);
	}

	// Check if bind is a sequence
	if (bind && !PySequence_Check(bind)) {
		PyErr_SetString(PyExc_ValueError, "bind is not a sequence");
		goto ERROR;
	}

	// Check callback
	if (callback && !PyCallable_Check(callback)) {
		PyErr_SetString(PyExc_TypeError, "callback must be callable\n");
		goto ERROR;
	}

	// Create jail
	r = pakfire_jail_create(&jail, self->pakfire);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Check callback
	if (callback && !PyCallable_Check(callback)) {
		PyErr_SetString(PyExc_TypeError, "callback must be callable\n");
		goto ERROR;
	}

	// Set nice
	if (nice) {
		r = pakfire_jail_nice(jail, nice);
		if (r) {
			PyErr_SetFromErrno(PyExc_OSError);
			goto ERROR;
		}
	}

	PyObject* key = NULL;
	PyObject* value = NULL;
	Py_ssize_t p = 0;

	// Parse the environment
	if (environ) {
		// Check if environ is a dictionary
		if (!PyDict_Check(environ)) {
			PyErr_SetString(PyExc_TypeError, "environ must be a dictionary");
			goto ERROR;
		}

		// All keys and values must be strings
		while (PyDict_Next(environ, &p, &key, &value)) {
			if (!PyUnicode_Check(key) || !PyUnicode_Check(value)) {
				PyErr_SetString(PyExc_TypeError, "Environment contains a non-string object");
				goto ERROR;
			}

			// Set environment value
			r = pakfire_jail_set_env(jail, PyUnicode_AsUTF8(key), PyUnicode_AsUTF8(value));
			if (r) {
				PyErr_SetFromErrno(PyExc_OSError);
				goto ERROR;
			}
		}
	}

	const Py_ssize_t num_bind = PySequence_Length(bind);

	// Bind
	for (unsigned int i = 0; i < num_bind; i++) {
		PyObject* b = PySequence_ITEM(bind, i);
		if (!b)
			goto ERROR;

		// Check if this is a Unicode object
		if (!PyUnicode_Check(b)) {
			PyErr_SetString(PyExc_ValueError, "bind contains a non-Unicode object");
			Py_DECREF(b);
			goto ERROR;
		}

		const char* path = PyUnicode_AsUTF8(b);

		// Perform bind
		r = pakfire_jail_bind(jail, path, path, 0);
		if (r) {
			PyErr_SetFromErrno(PyExc_OSError);
			Py_DECREF(b);
			goto ERROR;
		}

		Py_DECREF(b);
	}

	Py_BEGIN_ALLOW_THREADS

	// Execute command
	r = pakfire_jail_exec(jail, argv,
		NULL, Pakfire_execute_output_callback, callback, 0);

	Py_END_ALLOW_THREADS

	// If the return code was negative, we had some internal error
	if (r < 0) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;

	// Otherwise the executed command returned some error code
	} else if (r > 0) {
		PyObject* code = PyLong_FromLong(r);

		// Raise CommandExecutionError
		PyErr_SetObject(PyExc_CommandExecutionError, code);
		Py_DECREF(code);

		goto ERROR;
	}

	// The process has exited successfully

	// Return None
	ret = Py_None;
	Py_INCREF(ret);

ERROR:
	if (argv)
		free(argv);
	if (jail)
		pakfire_jail_unref(jail);

	return ret;
}

static PyObject* Pakfire_dist(PakfireObject* self, PyObject* args) {
	const char* path = NULL;
	const char* target = NULL;
	char* result = NULL;
	int r;

	if (!PyArg_ParseTuple(args, "s|z", &path, &target))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	r = pakfire_dist(self->pakfire, path, target, &result);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	PyObject* ret = PyUnicode_FromString(result);
	free(result);

	return ret;
}

static PyObject* Pakfire_copy_in(PakfireObject* self, PyObject* args) {
	const char* src = NULL;
	const char* dst = NULL;

	if (!PyArg_ParseTuple(args, "ss", &src, &dst))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	int r = pakfire_copy_in(self->pakfire, src, dst);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	Py_RETURN_NONE;
}

static PyObject* Pakfire_copy_out(PakfireObject* self, PyObject* args) {
	const char* src = NULL;
	const char* dst = NULL;

	if (!PyArg_ParseTuple(args, "ss", &src, &dst))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	int r = pakfire_copy_out(self->pakfire, src, dst);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	Py_RETURN_NONE;
}

static PyObject* Pakfire_get_repos(PakfireObject* self) {
	struct pakfire_repolist* repos = pakfire_get_repos(self->pakfire);
	if (!repos) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	const size_t l = pakfire_repolist_size(repos);

	PyObject* list = PyList_New(l);
	if (!list)
		goto ERROR;

	for (unsigned int i = 0; i < l; i++) {
		struct pakfire_repo* repo = pakfire_repolist_get(repos, i);
		if (!repo)
			continue;

		PyObject* obj = new_repo(&RepoType, repo);
		PyList_SET_ITEM(list, i, obj);

		pakfire_repo_unref(repo);
	}

ERROR:
	pakfire_repolist_unref(repos);

	return list;
}

static PyObject* execute_return_value(int r) {
	// Raise an OS error if r < 0
	if (r < 0) {
		errno = -r;

		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;

	// Raise exception when the command failed
	} else if (r > 0) {
		PyObject* code = PyLong_FromLong(r);

		PyErr_SetObject(PyExc_CommandExecutionError, code);
		Py_DECREF(code);

		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Pakfire_build(PakfireObject* self, PyObject* args, PyObject* kwargs) {
	char* kwlist[] = {
		"path",
		"target",
		"build_id",
		"interactive",
		"disable_snapshot",
		"disable_ccache",
		"disable_tests",
		NULL,
	};
	const char* path = NULL;
	const char* target = NULL;
	const char* build_id = NULL;
	int interactive = 0;
	int disable_snapshot = 0;
	int disable_ccache = 0;
	int disable_tests = 0;
	int r;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "s|zzpppp", kwlist, &path, &target,
			&build_id, &interactive, &disable_snapshot, &disable_ccache, &disable_tests))
		return NULL;

	int flags = 0;

	if (interactive)
		flags |= PAKFIRE_BUILD_INTERACTIVE;

	// Disable snapshot if requested
	if (disable_snapshot)
		flags |= PAKFIRE_BUILD_DISABLE_SNAPSHOT;

	// Disable ccache if requested
	if (disable_ccache)
		flags |= PAKFIRE_BUILD_DISABLE_CCACHE;

	// Disable tests if requested
	if (disable_tests)
		flags |= PAKFIRE_BUILD_DISABLE_TESTS;

	Py_BEGIN_ALLOW_THREADS

	// Run build
	r = pakfire_build(self->pakfire, path, target, build_id, flags);

	Py_END_ALLOW_THREADS

	return execute_return_value(r);
}

static PyObject* Pakfire_shell(PakfireObject* self, PyObject* args, PyObject* kwargs) {
	char* kwlist[] = {
		"install",
		"disable_snapshot",
		NULL,
	};
	char** packages = NULL;
	int disable_snapshot = 0;
	int r;

	// Parse everything
	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "|O&p", kwlist,
			convert_packages, &packages, &disable_snapshot))
		return NULL;

	int flags = 0;

	if (disable_snapshot)
		flags |= PAKFIRE_BUILD_DISABLE_SNAPSHOT;

	Py_BEGIN_ALLOW_THREADS

	r = pakfire_shell(self->pakfire, (const char**)packages, flags);

	Py_END_ALLOW_THREADS

	return execute_return_value(r);
}

static PyObject* Pakfire_clean(PakfireObject* self) {
	int r;

	Py_BEGIN_ALLOW_THREADS

	r = pakfire_clean(self->pakfire, 0);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	Py_RETURN_NONE;
}

static PyObject* Pakfire_refresh(PakfireObject* self, PyObject* args) {
	int force = 0;
	int r;

	if (!PyArg_ParseTuple(args, "|p", &force))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	r = pakfire_refresh(self->pakfire, force);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	Py_RETURN_NONE;
}

static PyObject* Pakfire_check(PakfireObject* self) {
	struct pakfire_filelist* errors = NULL;
	int r;

	// Allocate a filelist for errors
	r = pakfire_filelist_create(&errors, self->pakfire);
	if (r)
		goto ERROR;

	Py_BEGIN_ALLOW_THREADS

	// Perform check
	r = pakfire_check(self->pakfire, errors);

	Py_END_ALLOW_THREADS

	if (r)
		goto ERROR;

	const size_t num_errors = pakfire_filelist_length(errors);

	// Did we find any errors?
	if (num_errors) {
		PyObject* _errors = PyList_FromFileList(errors);
		if (!_errors)
			goto ERROR;

		pakfire_filelist_unref(errors);

		// Raise CheckFileVerificationError
		PyErr_SetObject(PyExc_CheckFileVerificationError, _errors);
		Py_DECREF(_errors);
		return NULL;
	}

	pakfire_filelist_unref(errors);
	Py_RETURN_NONE;

ERROR:
	// Cleanup
	if (errors)
		pakfire_filelist_unref(errors);

	// Raise exception from errno
	PyErr_SetFromErrno(PyExc_OSError);
	return NULL;
}

static PyObject* Pakfire_sync(PakfireObject* self, PyObject* args, PyObject* kwargs) {
	char* kwlist[] = {
		"keep_orphaned",
		"status_callback",
		NULL,
	};
	int keep_orphaned = 0;
	int flags = 0;
	PyObject* status_callback = NULL;
	int r;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "|$pO", kwlist,
			&keep_orphaned, &status_callback))
		return NULL;

	if (keep_orphaned)
		flags |= PAKFIRE_REQUEST_KEEP_ORPHANED;

	Py_BEGIN_ALLOW_THREADS

	r = pakfire_sync(self->pakfire, 0, flags, NULL,
			Pakfire_status_callback, status_callback);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	Py_RETURN_NONE;
}

static PyObject* Pakfire_open(PakfireObject* self, PyObject* args) {
	struct pakfire_archive* archive = NULL;
	const char* path = NULL;

	if (!PyArg_ParseTuple(args, "s", &path))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	int r = pakfire_archive_open(&archive, self->pakfire, path);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	// Create Python object
	PyObject* object = new_archive(&ArchiveType, archive);
	pakfire_archive_unref(archive);

	return object;
}

static PyObject* Pakfire_repo_compose(PakfireObject* self, PyObject* args, PyObject* kwargs) {
	char* kwlist[] = { "path", "files", NULL };
	const char* path = NULL;
	PyObject* list = NULL;

	PyObject* ret = NULL;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "sO", kwlist, &path, &list))
		return NULL;

	// List must be a sequence
	if (!PySequence_Check(list)) {
		PyErr_SetString(PyExc_ValueError, "Expected a sequence.");
		return NULL;
	}

	const int flags = 0;

	// How many new files do we have?
	ssize_t num_files = PySequence_Length(list);
	if (num_files < 0)
		return NULL;

	// Allocate files array
	const char** files = calloc(num_files + 1, sizeof(*files));
	if (!files) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	for (int i = 0; i < num_files; i++) {
		PyObject* file = PySequence_GetItem(list, i);
		if (!file)
			goto ERROR;

		// Check if file is a Unicode object
		if (!PyUnicode_Check(file)) {
			PyErr_SetString(PyExc_ValueError, "Expected a string.");
			goto ERROR;
		}

		// Add pointer to string to files array
		files[i] = PyUnicode_AsUTF8(file);
		if (!files[i])
			goto ERROR;

		Py_DECREF(file);
	}

	Py_BEGIN_ALLOW_THREADS

	int r = pakfire_repo_compose(self->pakfire, path, flags, files);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	// Return None on success
	ret = Py_None;
	Py_INCREF(ret);

ERROR:
	if (files)
		free(files);

	return ret;
}

static struct PyMethodDef Pakfire_methods[] = {
	{
		"build",
		(PyCFunction)Pakfire_build,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"check",
		(PyCFunction)Pakfire_check,
		METH_NOARGS,
		NULL,
	},
	{
		"clean",
		(PyCFunction)Pakfire_clean,
		METH_NOARGS,
		NULL,
	},
	{
		"copy_in",
		(PyCFunction)Pakfire_copy_in,
		METH_VARARGS,
		NULL,
	},
	{
		"copy_out",
		(PyCFunction)Pakfire_copy_out,
		METH_VARARGS,
		NULL,
	},
	{
		"dist",
		(PyCFunction)Pakfire_dist,
		METH_VARARGS,
		NULL
	},
	{
		"erase",
		(PyCFunction)Pakfire_erase,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"execute",
		(PyCFunction)Pakfire_execute,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"fetch_key",
		(PyCFunction)Pakfire_fetch_key,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"generate_key",
		(PyCFunction)Pakfire_generate_key,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"get_key",
		(PyCFunction)Pakfire_get_key,
		METH_VARARGS,
		NULL
	},
	{
		"get_repo",
		(PyCFunction)Pakfire_get_repo,
		METH_VARARGS,
		NULL
	},
	{
		"import_key",
		(PyCFunction)Pakfire_import_key,
		METH_VARARGS,
		NULL
	},
	{
		"install",
		(PyCFunction)Pakfire_install,
		METH_VARARGS|METH_KEYWORDS,
		NULL,
	},
	{
		"open",
		(PyCFunction)Pakfire_open,
		METH_VARARGS,
		NULL
	},
	{
		"refresh",
		(PyCFunction)Pakfire_refresh,
		METH_VARARGS,
		NULL,
	},
	{
		"repo_compose",
		(PyCFunction)Pakfire_repo_compose,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"search",
		(PyCFunction)Pakfire_search,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"shell",
		(PyCFunction)Pakfire_shell,
		METH_VARARGS|METH_KEYWORDS,
		NULL,
	},
	{
		"sync",
		(PyCFunction)Pakfire_sync,
		METH_VARARGS|METH_KEYWORDS,
		NULL,
	},
	{
		"update",
		(PyCFunction)Pakfire_update,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"version_compare",
		(PyCFunction)Pakfire_version_compare,
		METH_VARARGS,
		NULL
	},
	{
		"whatprovides",
		(PyCFunction)Pakfire_whatprovides,
		METH_VARARGS,
		NULL
	},
	{
		"whatrequires",
		(PyCFunction)Pakfire_whatrequires,
		METH_VARARGS,
		NULL
	},
	{ NULL },
};

static struct PyGetSetDef Pakfire_getsetters[] = {
	{
		"arch",
		(getter)Pakfire_get_arch,
		NULL,
		NULL,
		NULL
	},
	{
		"keys",
		(getter)Pakfire_get_keys,
		NULL,
		NULL,
		NULL
	},
    {
		"path",
		(getter)Pakfire_get_path,
		NULL,
		NULL,
		NULL
	},
	{
		"repos",
		(getter)Pakfire_get_repos,
		NULL,
		NULL,
		NULL
	},
    { NULL },
};

PyTypeObject PakfireType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	tp_name:            "_pakfire.Pakfire",
	tp_basicsize:       sizeof(PakfireObject),
	tp_flags:           Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	tp_new:             Pakfire_new,
	tp_dealloc:         (destructor)Pakfire_dealloc,
	tp_init:            (initproc)Pakfire_init,
	tp_doc:             "Pakfire object",
	tp_methods:         Pakfire_methods,
	tp_getset:          Pakfire_getsetters,
	tp_repr:            (reprfunc)Pakfire_repr,
};
