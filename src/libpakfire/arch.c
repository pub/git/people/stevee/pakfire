/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <errno.h>
#include <fts.h>
#include <linux/limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/personality.h>
#include <sys/utsname.h>

#include <pakfire/arch.h>
#include <pakfire/constants.h>
#include <pakfire/private.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_arch {
	const char* name;
	const char* platform;
	const char* compatible[5];
	unsigned long personality;
	const char magic[41];
};

static const struct pakfire_arch PAKFIRE_ARCHES[] = {
	// x86
	{
		.name = "x86_64",
		.platform = "x86",
		.personality = PER_LINUX,
		.magic = "7f454c4602010100000000000000000002003e00",
	},

	// ARM
	{
		.name = "aarch64",
		.platform = "arm",
		.personality = PER_LINUX,
		.magic = "7f454c460201010000000000000000000200b700",
	},

	// RISC-V
	{
		.name = "riscv64",
		.platform = "riscv",
		.personality = PER_LINUX,
		.magic = "7f454c460201010000000000000000000200f300",
	},

	// The end
	{ NULL },
};

static const struct pakfire_arch* pakfire_arch_find(const char* name) {
	for (const struct pakfire_arch* arch = PAKFIRE_ARCHES; arch->name; arch++) {
		if (strcmp(arch->name, name) == 0)
			return arch;
	}

	return NULL;
}

PAKFIRE_EXPORT int pakfire_arch_supported(const char* name) {
	const struct pakfire_arch* arch = pakfire_arch_find(name);

	if (arch)
		return 1;

	return 0;
}

const char** __pakfire_supported_arches = NULL;

PAKFIRE_EXPORT const char** pakfire_supported_arches() {
	unsigned int counter = 0;

	if (!__pakfire_supported_arches) {
		for (const struct pakfire_arch* arch = PAKFIRE_ARCHES; arch->name; arch++) {
			__pakfire_supported_arches = reallocarray(__pakfire_supported_arches,
				counter + 2, sizeof(*__pakfire_supported_arches));

			// Exit if the allocation failed
			if (!__pakfire_supported_arches)
				return NULL;

			__pakfire_supported_arches[counter++] = arch->name;
		}

		// Sentinel
		if (__pakfire_supported_arches)
			__pakfire_supported_arches[counter] = NULL;
	}

	return __pakfire_supported_arches;
}

const char* pakfire_arch_platform(const char* name) {
	const struct pakfire_arch* arch = pakfire_arch_find(name);

	if (arch && arch->platform)
		return arch->platform;

	return NULL;
}

unsigned long pakfire_arch_personality(const char* name) {
	const struct pakfire_arch* arch = pakfire_arch_find(name);

	if (arch)
		return arch->personality;

	return 0;
}

int __pakfire_arch_machine(char* buffer, size_t length, const char* arch, const char* vendor) {
	char buildtarget[1024];

	// Fetch buildtarget
	int r = __pakfire_arch_buildtarget(buildtarget, sizeof(buildtarget), arch, vendor);
	if (r)
		return r;

	// Append -gnu
	return __pakfire_string_format(buffer, length, "%s-gnu", buildtarget);
}

int __pakfire_arch_buildtarget(char* buffer, size_t length, const char* arch, const char* vendor) {
	if (!vendor)
		vendor = "unknown";

	// Determine the length of the vendor string
	int vendor_length = strlen(vendor);

	// Cut off suffix if it contains spaces
	char* space = strchr(vendor, ' ');
	if (space)
		vendor_length = space - vendor;

	// Format string
	int r = __pakfire_string_format(buffer, length, "%s-%.*s-linux",
		arch, vendor_length, vendor);
	if (r)
		return r;

	// Make everything lowercase
	for (char* p = buffer; *p; p++)
		*p = tolower(*p);

	return 0;
}

static const char* __pakfire_arch_native = NULL;

PAKFIRE_EXPORT const char* pakfire_arch_native() {
	struct utsname buf;

	if (!__pakfire_arch_native) {
		if (uname(&buf) < 0)
			return NULL;

		__pakfire_arch_native = strdup(buf.machine);
	}

	return __pakfire_arch_native;
}

int pakfire_arch_is_compatible(const char* name, const char* compatible_arch) {
	if (!name || !compatible_arch) {
		errno = EINVAL;
		return 1;
	}

	// Every architecture is compatible with itself
	if (strcmp(name, compatible_arch) == 0)
		return 1;

	const struct pakfire_arch* arch = pakfire_arch_find(name);
	if (!arch)
		return 0;

	for (unsigned int i = 0; arch->compatible[i]; i++) {
		if (strcmp(arch->compatible[i], compatible_arch) == 0)
			return 1;
	}

	return 0;
}

int pakfire_arch_supported_by_host(const char* name) {
	if (!name) {
		errno = EINVAL;
		return 1;
	}

	const char* native_arch = pakfire_arch_native();

	// Check if those two architectures are compatible
	return pakfire_arch_is_compatible(native_arch, name);
}

static char* find_interpreter(const char* path, const char* magic) {
	FILE* f = fopen(path, "r");
	if (!f)
		return NULL;

	char* line = NULL;
	size_t length = 0;

	int enabled = 0;
	int match = 0;
	char interpreter[PATH_MAX];

	while (1) {
		ssize_t bytes_read = getline(&line, &length, f);
		if (bytes_read < 0)
			break;

		// Remove the newline
		pakfire_remove_trailing_newline(line);

		// Look for the "enabled" line
		if (strcmp("enabled", line) == 0) {
			enabled = 1;

		// Store the interpreter for later
		} else if (pakfire_string_startswith(line, "interpreter ")) {
			pakfire_string_set(interpreter, line + strlen("interpreter "));

		// If we found the magic, we check if it is a match
		} else if (pakfire_string_startswith(line, "magic ")) {
			const char* m = line + strlen("magic ");

			if (strcmp(magic, m) == 0)
				match = 1;
		}
	}

	// Free resources
	if (line)
		free(line);
	fclose(f);

	// Return the interpreter if it is a match
	if (enabled && match && *interpreter)
		return strdup(interpreter);

	// Otherwise return NULL
	return NULL;
}

char* pakfire_arch_find_interpreter(const char* name) {
	if (!name) {
		errno = EINVAL;
		return NULL;
	}

	// If the host supports this architecture natively,
	// we do not need to search for the interpreter
	if (pakfire_arch_supported_by_host(name))
		return NULL;

	const struct pakfire_arch* arch = pakfire_arch_find(name);
	if (!arch)
		return NULL;

	char* interpreter = NULL;

	char* paths[] = {
		"/proc/sys/fs/binfmt_misc", NULL,
	};

	FTS* f = fts_open(paths, FTS_NOCHDIR|FTS_NOSTAT, NULL);
	if (!f)
		goto ERROR;

	for (;;) {
		FTSENT* fent = fts_read(f);
		if (!fent)
			break;

		// Only handle files
		if (!(fent->fts_info & FTS_F))
			continue;

		interpreter = find_interpreter(fent->fts_path, arch->magic);

		// End search if we have found a match
		if (interpreter)
			break;
	}

ERROR:
	if (f)
		fts_close(f);

	return interpreter;
}
