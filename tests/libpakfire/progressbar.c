/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stddef.h>
#include <unistd.h>

#include <pakfire/progressbar.h>

#include "../testsuite.h"

static int test_run(const struct test* t) {
	struct pakfire_progressbar* p = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_progressbar_create(&p, NULL));

	ASSERT(pakfire_progressbar_finish(p) == EINVAL);

	// Add a string
	ASSERT_SUCCESS(pakfire_progressbar_add_string(p, "STRING"));

	// Add a counter
	ASSERT_SUCCESS(pakfire_progressbar_add_counter(p));

	// Add a bar
	ASSERT_SUCCESS(pakfire_progressbar_add_bar(p));

	// Add the percentage
	ASSERT_SUCCESS(pakfire_progressbar_add_percentage(p));

	// Add a timer
	ASSERT_SUCCESS(pakfire_progressbar_add_timer(p));

	ASSERT_SUCCESS(pakfire_progressbar_start(p, 1000));

	for (unsigned int i = 0; i < 1000; i++) {
		ASSERT_SUCCESS(pakfire_progressbar_increment(p, 1));
		usleep(2500);
	}

	ASSERT_SUCCESS(pakfire_progressbar_finish(p));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (p)
		pakfire_progressbar_unref(p);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_run);

	return testsuite_run(argc, argv);
}
