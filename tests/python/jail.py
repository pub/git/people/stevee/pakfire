#!/usr/bin/python3

import logging
import os
import pakfire
import unittest

class JailTests(unittest.TestCase):
	"""
		This tests the execute command
	"""
	def setUp(self):
		path = os.environ.get("TEST_STUB_ROOT")
		if not path:
			raise RuntimeError("TEST_STUB_ROOT is not defined")

		self.pakfire = pakfire.Pakfire(path)

	def test_execute(self):
		r = self.pakfire.execute(["/command", "exit-with-code", "0"])

		self.assertIsNone(r)

	def test_return_value(self):
		with self.assertRaises(pakfire.CommandExecutionError) as e:
			self.pakfire.execute(["/command", "exit-with-code", "123"])

		# Extract return code
		code, = e.exception.args

		self.assertTrue(code == 123)

	def test_environ(self):
		r = self.pakfire.execute(["/command", "echo-environ", "VAR1"],
			environ={"VAR1" : "VAL1"})

		self.assertIsNone(r)

	def test_invalid_inputs(self):
		# Arguments
		with self.assertRaises(TypeError):
			self.pakfire.execute("/command")

		with self.assertRaises(TypeError):
			self.pakfire.execute(["/command", 1])

		with self.assertRaises(TypeError):
			self.pakfire.execute(("/command", "--help"))

		# Environment
		with self.assertRaises(TypeError):
			self.pakfire.execute(["/command", "--help"], environ={"VAR1" : 1})

		with self.assertRaises(TypeError):
			self.pakfire.execute(["/command", "--help"], environ={1 : "VAL1"})

		with self.assertRaises(TypeError):
			self.pakfire.execute(["/command", "--help"], environ="VAR1=VAL1")

	def test_execute_non_existant_command(self):
		"""
			Executing non-existant commands should raise an error
		"""
		with self.assertRaises(pakfire.CommandExecutionError):
			self.pakfire.execute(["/command-does-not-exist"])

	def test_execute_output(self):
		self.pakfire.execute(["/command", "echo", "123"])

		# Multiple newlines in one read
		self.pakfire.execute(["/command", "echo", "1\n2\n3"])

		# Run a command with a lot of output which exceeds the buffer size
		self.pakfire.execute(["/command", "lines", "1", "65536"])

		# Run a command that generates lots of lines
		self.pakfire.execute(["/command", "lines", "100", "40"])

	def test_execute_logger(self):
		def log(priority, message):
			# Priority must be INFO
			self.assertEqual(priority, logging.INFO)

			# All lines must be 20 characters long
			self.assertEqual(len(message), 20)

		self.pakfire.execute(["/command", "lines", "10", "20"], logging_callback=log)

	# XXX This does not work
	#def test_execute_logger_exceptions(self):
	#	"""
	#		The logger callback will raise an Exception which should be handled
	#	"""
	#	def log(priority, message):
	#		raise RuntimeError(message)
	#
	#	with self.assertRaises(RuntimeError):
	#		self.pakfire.execute(
	#			["/command", "echo", "Hello World!"],
	#			logging_callback=log,
	#		)

	def test_pid(self):
		def checkpid(priority, message):
			# The PID must be 1
			self.assertEqual(message, "1")

		self.pakfire.execute(["/command", "print-pid"], logging_callback=checkpid)

	def test_nice(self):
		self.pakfire.execute(["/command", "print-nice"], nice=5)

	def test_nice_invalid_input(self):
		"""
			Tries using an invalid nice value
		"""
		with self.assertRaises(OSError):
			self.pakfire.execute(["/command", "print-nice"], nice=100)

	def test_check_open_file_descriptors(self):
		"""
			Since we are spawning child processes, it might happen that we leak file
			descriptors to the child process.
		"""
		self.pakfire.execute(["/command", "check-open-file-descriptors"])

	# Signals

	def test_send_signal_DEFAULT(self):
		"""
			Sends a stupid signal which doesn't do anything
		"""
		self.pakfire.execute(["/command", "send-signal", "0"])

	def test_send_signal_KILL(self):
		"""
			Test the process killing itself
		"""
		self.pakfire.execute(["/command", "send-signal", "9"])

	def test_send_signal_TERM(self):
		"""
			Test the process terminating itself
		"""
		self.pakfire.execute(["/command", "send-signal", "15"])

	# This is an interactive test which cannot be performed automatically
	#def test_shell(self):
	#	self.pakfire.execute(["/bin/bash", "-i"])


if __name__ == "__main__":
	unittest.main()
