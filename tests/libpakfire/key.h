/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

const char* TEST_KEY_FINGERPRINT = "E63B4976A575A3B2544AC15E726D8B0B0889B04E";

const char* TEST_KEY_DATA = "-----BEGIN PGP PUBLIC KEY BLOCK-----\n\n"
"mQINBE/ndRQBEAC5ykX6wgPdR42ZkTdrcVS9fIACn/n/ed7K/dWlUtANnAtoH27Qp+I/vKy9\n"
"DwGUjDe5ST8l4td+gzZNwWowTnLr6zULG+dBDPNL1SZrL7sxLfjuEyOAAFcMTI8hKqwCQfSz\n"
"OJVX4X7mfSDgewqFjDO1a1gkJUXnq8xMNJa6fTH61qVPXmwBpR4JgzrtH4FmvuXjocKZ4+/B\n"
"NwD80ChfiwwNjwTv0ykdXhV6aEtqg9sfFRB/BGUIBDUOlG6i8VCNQ0X1rAcyO2waj6cAfYVE\n"
"HbD4POMIWhaBvfKnYUuUyPoSDjcgIWIrHmdBHuM/zbLjAfIoh/Lk16UnU6DBjsMdtq6d9HWD\n"
"OWRQOXigwKHiRZD9QQDPxTb8w6EAu/UFzHNUCD7O0rS2KmugZMk/Q0ijhVCfC6MXTu7Vjg3c\n"
"tjf7ZE2kvhKs5I6j3lTCLm23WYcRefKJQ/9Ab1d6bYoOwNpQFdP0duYyP2TC1cSC5Cs8h7pE\n"
"nI1yB9B+Fu6fqNyIhgo9fbh29+F8K/KYT0UifsTvBNTv2bf+23IbFR3EC3WWEdvars1h9mTK\n"
"g8bbyKNxXrWXrPUb+TWovaTpzpRNqOYDa5tqh9Q/0e1mkNcjJbBK/2HxAT4hi69Dfswr0cYh\n"
"IwpH3fTwlZPpBjEOVz5VyAaZyjjJHweRhz5YJUgPt7J9/tTmGQARAQABtEVJUEZpcmUgQnVp\n"
"bGRzZXJ2ZXIgY29ybmVsaXVzLmlwZmlyZS5vcmcgPHBha2ZpcmUtYnVpbGRlcnNAaXBmaXJl\n"
"Lm9yZz6JAjgEEwECACIFAk/ndRQCGwMGCwkIBwMCBhUIAgkKCwQWAgMBAh4BAheAAAoJEHJt\n"
"iwsIibBOdqsP/1EVKVgFxiMVcRTP5VYV2+EV/CaiaqMzC488AGRq8LYMzq/hqj0w+lbCl6AG\n"
"25Aqf6UX8VMgkPr5pIy/IZqZgQ0N7fX6Vw0mhR/2zAHItvuff8zCM2Sp682CRahn1SB9oWjl\n"
"bJZwHJPVkLVqAxs5f1EyXstl8NNrVJQ0rZKWvfTKJL6kCYitnNuat4BB/uv2aNB8MiMlwJj6\n"
"DtO6uGagw3lkR9w+PL0lW2auQqE6Ia6CIdOT43qQ/Is7SnmL5uS5/uwNgdZ1ayCMPM+xeJrH\n"
"A7MTQKVDcyTmkIriO7qRaaPsPkhqfD/vjurWQ+Hx9XTUUk3QeCpZfXLGB+ZNLg2+kxanSoQ0\n"
"JbB8V7OMOi7Z8y0U0vXKTn1cKIzrXB1/GvwpYPZAmFgcOqXoNF3Qs7ilud3bxomG8nBHWNBA\n"
"1w+rzMYQbwCow7GkknniaB+ZzkPB+8+1jul4U5rtGeFyloyHy6WO2WKk7nsou/i3vfpVHRzh\n"
"9XVkEYesGrAIJqrpfJYgAbzRSUyPoFoIBhC7p8emKr/TTzzc90s5EPlqkxE/BRPvkqW+4HjS\n"
"ybOFv4fii7qGdpNrnXTACB2v06Y+TpUGXVA5+Ac+5Zn5F/++tUnvZ9eCT1VmUhw+wDV7qQtq\n"
"gVwRjTWnu8cc9uoZbKK1Qifehl11xi4hp+YZmv/7eOg1LWeR\n"
"=XdHO\n\n"
"-----END PGP PUBLIC KEY BLOCK-----\n";
