/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <sys/stat.h>

#include <pakfire/fhs.h>
#include <pakfire/file.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/util.h>

/*
	This struct defines any FHS checks.

	They are being processed in order from top to bottom which is why we are starting
	with some more prominent matches and have the less important stuff at the bottom.
*/
static const struct pakfire_fhs_check {
	const char* path;
	const mode_t type;
	const mode_t perms;
	const char* uname;
	const char* gname;
	enum pakfire_fhs_check_flags {
		PAKFIRE_FHS_MUSTNOTEXIST = (1 << 0),
		PAKFIRE_FHS_NOEXEC       = (1 << 1),
	} flags;
} pakfire_fhs_check[] = {
	// /usr
	{ "/usr",                 S_IFDIR, 0755, "root", "root", 0 },
	{ "/usr/bin",             S_IFDIR, 0755, "root", "root", 0 },
	{ "/usr/include",         S_IFDIR, 0755, "root", "root", 0 },
	{ "/usr/lib",             S_IFDIR, 0755, "root", "root", 0 },
	{ "/usr/lib64",           S_IFDIR, 0755, "root", "root", 0 },
	{ "/usr/sbin",            S_IFDIR, 0755, "root", "root", 0 },
	{ "/usr/share",           S_IFDIR, 0755, "root", "root", 0 },
	{ "/usr/src",             S_IFDIR, 0755, "root", "root", 0 },
	{ "/usr/src/kernels",     S_IFDIR, 0755, "root", "root", 0 },

	// Allow no further files in /usr
	{ "/usr/*",                     0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// Allow no files in /usr/src except some kernel source
	{ "/usr/src/kernels/**",        0,    0, "root", "root", 0 },
	{ "/usr/src/**",                0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// There cannot be any subdirectories in /usr/bin & /usr/sbin
	{ "/usr/bin/*",           S_IFDIR,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },
	{ "/usr/sbin/*",          S_IFDIR,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// Permitted setuid binaries
	{ "/usr/bin/gpasswd",     S_IFREG, S_ISUID|0755, "root", "root", 0 },
	{ "/usr/bin/ksu",         S_IFREG, S_ISUID|0755, "root", "root", 0 },
	{ "/usr/bin/passwd",      S_IFREG, S_ISUID|0755, "root", "root", 0 },
	{ "/usr/bin/pkexec",      S_IFREG, S_ISUID|0755, "root", "root", 0 },
	{ "/usr/bin/sudo",        S_IFREG, S_ISUID|0755, "root", "root", 0 },

	// Any files in /usr/{,s}bin must be owned by root and have 0755
	{ "/usr/bin/*",           S_IFREG, 0755, "root", "root", 0 },
	{ "/usr/sbin/*",          S_IFREG, 0755, "root", "root", 0 },

	// Shared Libraries must be executable
	{ "/usr/lib64/*.so.*",    S_IFREG, 0755, "root", "root", 0 },
	{ "/usr/lib64/**/*.so",   S_IFREG, 0755, "root", "root", 0 },

	// Tolerate runtime linkers in /usr/lib
	{ "/usr/lib/ld-*.so.*",   S_IFREG, 0755, "root", "root", 0 },

	// Shared Libraries must not exist in /usr/lib
	{ "/usr/lib/*.so*",       S_IFREG,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// /usr/include: Ensure that:
	//   * All files are non-executable and belong to root
	//   * All directories have 0755 and belong to root
	{ "/usr/include/**",      S_IFREG, 0644, "root", "root", 0 },
	{ "/usr/include/**",      S_IFDIR, 0755, "root", "root", 0 },

	// Firmware must not be executable
	{ "/usr/lib/firmware/**", S_IFREG, 0644, "root", "root", 0 },
	{ "/usr/lib/firmware/**", S_IFDIR, 0755, "root", "root", 0 },

	// /var
	{ "/var",                 S_IFDIR, 0755, "root", "root", 0 },
	{ "/var/cache",           S_IFDIR, 0755, "root", "root", 0 },
	{ "/var/db",              S_IFDIR, 0755, "root", "root", 0 },
	{ "/var/empty",           S_IFDIR, 0755, "root", "root", 0 },
	{ "/var/lib",             S_IFDIR, 0755, "root", "root", 0 },
	{ "/var/log",             S_IFDIR, 0755, "root", "root", 0 },
	{ "/var/mail",            S_IFDIR, 0755, "root", "mail", 0 },
	{ "/var/opt",             S_IFDIR, 0755, "root", "root", 0 },
	{ "/var/run",             S_IFLNK, 0755, "root", "root", 0 },
	{ "/var/spool",           S_IFDIR, 0755, "root", "root", 0 },
	{ "/var/tmp",             S_IFDIR, 0755, "root", "root", 0 },

	// Do not allow any subdirectories in /var
	{ "/var/*",                     0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },
	{ "/var/empty/**",              0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },
	{ "/var/tmp/**",                0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// No files in /var may be executable
	{ "/var/**",              S_IFREG,    0,   NULL,   NULL, PAKFIRE_FHS_NOEXEC },

	// /boot
	{ "/boot",                S_IFDIR, 0755, "root", "root", 0 },
	{ "/boot/efi",            S_IFDIR, 0755, "root", "root", 0 },

	// All files in /boot must be owned by root
	{ "/boot/**",             S_IFREG,    0, "root", "root", 0, },
	{ "/boot/**",             S_IFDIR,    0, "root", "root", 0, },

	// /dev (nothing may exist in it)
	{ "/dev",                 S_IFDIR, 0755, "root", "root", 0 },
	{ "/dev/**",                    0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// /etc
	{ "/etc",                 S_IFDIR, 0755, "root", "root", 0 },

	// /home
	{ "/home",                S_IFDIR, 0755, "root", "root", 0 },
	{ "/home/**",                   0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// /opt
	{ "/opt",                 S_IFDIR, 0755, "root", "root", 0 },
	// These directories belong to the "local administrator"
	// https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch03s13.html
	{ "/opt/bin",                   0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },
	{ "/opt/doc",                   0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },
	{ "/opt/include",               0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },
	{ "/opt/info",                  0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },
	{ "/opt/lib",                   0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },
	{ "/opt/man",                   0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// /proc
	{ "/proc",                S_IFDIR, 0755, "root", "root", 0 },
	{ "/proc/**",                   0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// root
	{ "/root",                S_IFDIR, 0700, "root", "root", 0 },
	{ "/root/.*",             S_IFREG, 0644, "root", "root", 0 },
	{ "/root/**",                   0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// /run
	{ "/run",                 S_IFDIR, 0755, "root", "root", 0 },
	{ "/run/**",                    0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// /sys
	{ "/sys",                 S_IFDIR, 0755, "root", "root", 0 },
	{ "/sys/**",                    0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// /tmp
	{ "/tmp",                 S_IFDIR, 1755, "root", "root", 0 },
	{ "/tmp/**",                    0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// FHS Directories
	{ "/media",               S_IFDIR, 0755, "root", "root", 0 },
	{ "/mnt",                 S_IFDIR, 0755, "root", "root", 0 },
	{ "/srv",                 S_IFDIR, 0755, "root", "root", 0 },

	// /bin, /sbin, /lib, and /lib64 have to be symlinks
	{ "/bin",                 S_IFLNK, 0777,   NULL,   NULL, 0 },
	{ "/lib",                 S_IFLNK, 0777,   NULL,   NULL, 0 },
	{ "/lib64",               S_IFLNK, 0777,   NULL,   NULL, 0 },
	{ "/sbin",                S_IFLNK, 0777,   NULL,   NULL, 0 },

	// There cannot be anything else in /
	{ "/*",                         0,    0,   NULL,   NULL, PAKFIRE_FHS_MUSTNOTEXIST },

	// Catch all so that we won't throw an error
	{ "/**",                        0,    0,   NULL,   NULL, 0 },

	// Sentinel
	{ NULL },
};

static const struct pakfire_fhs_check* pakfire_fhs_find_check(
		struct pakfire* pakfire, struct pakfire_file* file) {
	const struct pakfire_fhs_check* check = NULL;
	int r;

	// Fetch the file type
	const mode_t type = pakfire_file_get_type(file);

	// Fetch the path
	const char* path = pakfire_file_get_path(file);

	// Walk through all possible checks
	for (check = pakfire_fhs_check; check->path; check++) {
		// Skip this check if the filetype doesn't match
		if (check->type && check->type != type)
			continue;

		// Check path
		r = pakfire_path_match(check->path, path);
		switch (r) {
			// No match
			case 0:
				continue;

			// Match!
			case 1:
				DEBUG(pakfire, "%s matches check '%s'\n", path, check->path);

				return check;

			// Error :(
			default:
				goto ERROR;
		}
	}

ERROR:
	ERROR(pakfire, "Could not find FHS entry for %s: %m\n", path);

	return NULL;
}

static int pakfire_fhs_check_world_writable(
		struct pakfire* pakfire, struct pakfire_file* file) {
	// Run this check only for regular files
	switch (pakfire_file_get_type(file)) {
		case S_IFREG:
			break;

		default:
			return 0;
	}

	// Fetch path
	const char* path = pakfire_file_get_path(file);

	// Fetch permissions
	const mode_t perms = pakfire_file_get_perms(file);

	// Check that none of the executable bits are set
	if ((perms & (S_IWUSR|S_IWGRP|S_IWOTH)) == (S_IWUSR|S_IWGRP|S_IWOTH)) {
		DEBUG(pakfire, "%s is world-writable\n", path);
		return 1;
	}

	return 0;
}

static int pakfire_fhs_check_perms(struct pakfire* pakfire,
		const struct pakfire_fhs_check* check, struct pakfire_file* file) {
	// No permissions defined. Skipping check...
	if (!check->perms)
		return 0;

	const char* path = pakfire_file_get_path(file);

	// Fetch perms
	const mode_t perms = pakfire_file_get_perms(file);

	// Check if they match
	if (check->perms != perms) {
		DEBUG(pakfire, "%s: Permissions do not match\n", path);
		return 1;
	}

	// Check passed
	return 0;
}

static int pakfire_fhs_check_ownership(struct pakfire* pakfire,
		const struct pakfire_fhs_check* check, struct pakfire_file* file) {
	const char* path = pakfire_file_get_path(file);

	// Check uname
	if (check->uname) {
		const char* uname = pakfire_file_get_uname(file);
		if (!uname)
			return 1;

		if (strcmp(check->uname, uname) != 0) {
			DEBUG(pakfire, "%s: uname does not match\n", path);
			return 1;
		}
	}

	// Check gname
	if (check->gname) {
		const char* gname = pakfire_file_get_gname(file);
		if (!gname)
			return 1;

		if (strcmp(check->gname, gname) != 0) {
			DEBUG(pakfire, "%s: gname does not match\n", path);
			return 1;
		}
	}

	// Pass
	return 0;
}

static int pakfire_fhs_check_noexec(struct pakfire* pakfire,
		const struct pakfire_fhs_check* check, struct pakfire_file* file) {
	// Skip this check if PAKFIRE_FHS_NOEXEC is not set
	if (!(check->flags & PAKFIRE_FHS_NOEXEC))
		return 0;

	// Fetch path
	const char* path = pakfire_file_get_path(file);

	// Fetch permissions
	const mode_t perms = pakfire_file_get_perms(file);

	// Check that none of the executable bits are set
	if (perms & (S_IXUSR|S_IXGRP|S_IXOTH)) {
		DEBUG(pakfire, "%s must not be executable\n", path);
		return 1;
	}

	return 0;
}

int pakfire_fhs_check_file(struct pakfire* pakfire, struct pakfire_file* file) {
	const struct pakfire_fhs_check* check = NULL;
	int r;

	// Get the file path
	const char* path = pakfire_file_get_path(file);
	if (!path)
		return 1;

	// Check for world-writable permissions
	r = pakfire_fhs_check_world_writable(pakfire, file);
	if (r)
		return r;

	// Find a check
	check = pakfire_fhs_find_check(pakfire, file);
	if (!check) {
		ERROR(pakfire, "Could not match file %s: %m\n", path);
		return 1;
	}

	// Should this file exist at all?
	if (check->flags & PAKFIRE_FHS_MUSTNOTEXIST) {
		DEBUG(pakfire, "%s must not exist here\n", path);
		return 1;
	}

	// Check permissions
	r = pakfire_fhs_check_perms(pakfire, check, file);
	if (r)
		return r;

	// Check ownership
	r = pakfire_fhs_check_ownership(pakfire, check, file);
	if (r)
		return r;

	// Check for PAKFIRE_FHS_NOEXEC
	r = pakfire_fhs_check_noexec(pakfire, check, file);
	if (r)
		return r;

	// Check passed!
	return 0;
}
