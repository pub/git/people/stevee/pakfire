/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>

#include <pakfire/downloader.h>

#include "../testsuite.h"

#define DOWNLOAD_URL "file://" ABS_TOP_SRCDIR "/tests/data/beep-1.3-2.ip3.x86_64.pfm"

#define DOWNLOAD_PATH TEST_ROOTFS "/downloaded.file"

static int test_simple(const struct test* t) {
	struct pakfire_downloader* d = NULL;
	int r = EXIT_FAILURE;

	// Create downloader
	ASSERT_SUCCESS(pakfire_downloader_create(&d, t->pakfire));

	// Retrieve a file
	ASSERT_SUCCESS(
		pakfire_downloader_retrieve(d, NULL, NULL, NULL, DOWNLOAD_URL, DOWNLOAD_PATH,
			0, NULL, 0, 0)
	);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (d)
		pakfire_downloader_unref(d);

	return r;
}

static int test_retrieve_with_pending_transfers(const struct test* t) {
	struct pakfire_downloader* d = NULL;
	int r = EXIT_FAILURE;

	// Create downloader
	ASSERT_SUCCESS(pakfire_downloader_create(&d, t->pakfire));

	// Add a transfer
	ASSERT_SUCCESS(
		pakfire_downloader_add_transfer(d, NULL, NULL, NULL, DOWNLOAD_URL, DOWNLOAD_PATH,
			0, NULL, 0, 0)
	);

	// Retrieve a file
	ASSERT_SUCCESS(
		pakfire_downloader_retrieve(d, NULL, NULL, NULL, DOWNLOAD_URL, DOWNLOAD_PATH,
			0, NULL, 0, 0)
	);


	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (d)
		pakfire_downloader_unref(d);

	return r;
}

static int test_retrieve_with_mirrors(const struct test* t) {
	struct pakfire_downloader* d = NULL;
	struct pakfire_mirrorlist* m = NULL;
	int r = EXIT_FAILURE;

	// Create downloader
	ASSERT_SUCCESS(pakfire_downloader_create(&d, t->pakfire));

	// Create mirrorlist
	ASSERT_SUCCESS(pakfire_mirrorlist_create(&m, t->pakfire));

	// Add a couple of mirrors
	ASSERT_SUCCESS(pakfire_mirrorlist_add_mirror(m, "file://" ABS_TOP_SRCDIR "/tests/data/mirror-1"));
	ASSERT_SUCCESS(pakfire_mirrorlist_add_mirror(m, "file://" ABS_TOP_SRCDIR "/tests/data/mirror-2"));
	ASSERT_SUCCESS(pakfire_mirrorlist_add_mirror(m, "file://" ABS_TOP_SRCDIR "/tests/data/mirror-3"));
	ASSERT_SUCCESS(pakfire_mirrorlist_add_mirror(m, "file://" ABS_TOP_SRCDIR "/tests/data"));

	// Retrieve a file which doesn't exist
	//ASSERT_FAILURE(pakfire_downloader_retrieve(d, m, "doesnt-exist.pfm", DOWNLOAD_PATH));

	// Retrieve a file which exists
	ASSERT_SUCCESS(pakfire_downloader_retrieve(d, NULL, m,
		NULL, "beep-1.3-2.ip3.x86_64.pfm", DOWNLOAD_PATH, 0, NULL, 0, 0));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (d)
		pakfire_downloader_unref(d);
	if (m)
		pakfire_mirrorlist_unref(m);

	return r;
}

#ifdef ENABLE_ONLINE_TESTS

static const unsigned char expected_digest[] = {
	0xb7, 0x9b, 0xd6, 0xd2, 0x6a, 0x24, 0xe8, 0xeb, 0x46, 0x09, 0x26, 0x55, 0x70, 0x95,
	0x2f, 0xf5, 0xd2, 0xfc, 0xd0, 0x8e, 0x14, 0x0d, 0x68, 0x36, 0x87, 0x70, 0xa8, 0x86,
	0x46, 0xa9, 0x4e, 0xa7
};

static const unsigned char incorrect_digest[] = {
	0x01, 0x02, 0x03, 0x04,
};

static int test_retrieve_online(const struct test* t) {
	struct pakfire_downloader* d = NULL;
	int r = EXIT_FAILURE;

	// Create downloader
	ASSERT_SUCCESS(pakfire_downloader_create(&d, t->pakfire));

	// Retrieve a file
	ASSERT_SUCCESS(
		pakfire_downloader_retrieve(d, NULL, NULL, NULL,
			"https://mirror1.ipfire.org/releases/pakfire/pakfire-0.9.27.tar.gz",
			DOWNLOAD_PATH, 0, NULL, 0, 0)
	);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (d)
		pakfire_downloader_unref(d);

	return r;
}

static int test_retrieve_online_with_digest(const struct test* t) {
	struct pakfire_downloader* d = NULL;
	int r = EXIT_FAILURE;

	// Create downloader
	ASSERT_SUCCESS(pakfire_downloader_create(&d, t->pakfire));

	// Retrieve a file
	ASSERT_SUCCESS(
		pakfire_downloader_retrieve(d, NULL, NULL, NULL,
			"https://mirror1.ipfire.org/releases/pakfire/pakfire-0.9.27.tar.gz",
			DOWNLOAD_PATH, PAKFIRE_DIGEST_SHA2_256, expected_digest, sizeof(expected_digest), 0)
	);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (d)
		pakfire_downloader_unref(d);

	return r;
}

static int test_retrieve_online_with_incorrect_digest(const struct test* t) {
	struct pakfire_downloader* d = NULL;
	int r = EXIT_FAILURE;

	// Create downloader
	ASSERT_SUCCESS(pakfire_downloader_create(&d, t->pakfire));

	// Retrieve a file
	ASSERT_FAILURE(
		pakfire_downloader_retrieve(d, NULL, NULL, NULL,
			"https://mirror1.ipfire.org/releases/pakfire/pakfire-0.9.27.tar.gz",
			DOWNLOAD_PATH, PAKFIRE_DIGEST_SHA2_256, incorrect_digest, sizeof(incorrect_digest), 0)
	);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (d)
		pakfire_downloader_unref(d);

	return r;
}

#endif /* ENABLE_ONLINE_TESTS */

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_simple);
	testsuite_add_test(test_retrieve_with_pending_transfers);
	testsuite_add_test(test_retrieve_with_mirrors);

#ifdef ENABLE_ONLINE_TESTS
	testsuite_add_test(test_retrieve_online);
	testsuite_add_test(test_retrieve_online_with_digest);
	testsuite_add_test(test_retrieve_online_with_incorrect_digest);
#endif /* ENABLE_ONLINE_TESTS */

	return testsuite_run(argc, argv);
}
