/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/digest.h>
#include <pakfire/util.h>

#include "../testsuite.h"

// Just a file with some random data
const char* RANDOM_FILE = ABS_TOP_SRCDIR "/tests/data/digest/random";

static const struct pakfire_digests expected_digests = {
	.sha2_512 = {
		0xa1, 0x98, 0x97, 0x9b, 0x13, 0x44, 0xf3, 0x64, 0x48, 0x0e, 0x0f, 0xd1,
		0xbc, 0xde, 0x8f, 0x50, 0xf3, 0x3c, 0x92, 0x8e, 0xf0, 0xe7, 0x5d, 0x6e,
		0x1a, 0x28, 0x67, 0x1e, 0x16, 0x49, 0x7b, 0xc2, 0xd6, 0x64, 0xe5, 0x21,
		0x12, 0x60, 0x0f, 0x56, 0x3d, 0x8f, 0x0a, 0x7c, 0x24, 0x1a, 0x7e, 0x5a,
		0x16, 0xe7, 0xe7, 0x27, 0x33, 0xd9, 0x66, 0xb5, 0x0a, 0x39, 0x6d, 0xa8,
		0xb2, 0xb4, 0xac, 0x05,
	},
	.sha2_256 = {
		0x16, 0xf9, 0x01, 0xc1, 0x83, 0x31, 0x43, 0x88, 0x4f, 0x16, 0x6a, 0xe4,
		0x81, 0x34, 0x51, 0x99, 0xcf, 0x91, 0x94, 0xa3, 0xb6, 0x01, 0x75, 0xd9,
		0xb9, 0xca, 0x93, 0xd9, 0xd7, 0xa0, 0x4b, 0xa6,
	},
};

static int test_init(const struct test* t) {
	struct pakfire_digests digests = PAKFIRE_DIGESTS_INIT;

	// Check if everything is initialized correctly
	ASSERT(pakfire_digest_set(digests.sha2_512) == 0);
	ASSERT(pakfire_digest_set(digests.sha2_256) == 0);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_random(const struct test* t) {
	struct pakfire_digests digests = PAKFIRE_DIGESTS_INIT;
	FILE* f = NULL;
	int r = EXIT_FAILURE;

	ASSERT(f = fopen(RANDOM_FILE, "r"));

	// Compute digests
	ASSERT_SUCCESS(pakfire_digests_compute_from_file(t->pakfire, &digests,
		PAKFIRE_DIGESTS_ALL, f));

	// Compare the digests
	ASSERT_SUCCESS(pakfire_digests_compare(t->pakfire,
		&digests, &expected_digests, PAKFIRE_DIGEST_SHA2_512|PAKFIRE_DIGEST_SHA2_256));

	// Check if the individual digests match
	ASSERT_SUCCESS(pakfire_digests_compare_one(t->pakfire, &digests,
		PAKFIRE_DIGEST_SHA2_512, expected_digests.sha2_512, sizeof(expected_digests.sha2_512)));
	ASSERT_SUCCESS(pakfire_digests_compare_one(t->pakfire, &digests,
		PAKFIRE_DIGEST_SHA2_256, expected_digests.sha2_256, sizeof(expected_digests.sha2_256)));

	r = EXIT_SUCCESS;

FAIL:
	if (f)
		fclose(f);

	return r;
}

static int test_get_by_name(const struct test* t) {
	// Positive cases
	ASSERT(pakfire_digest_get_by_name("sha2-512") == PAKFIRE_DIGEST_SHA2_512);
	ASSERT(pakfire_digest_get_by_name("sha2-256") == PAKFIRE_DIGEST_SHA2_256);
	ASSERT(pakfire_digest_get_by_name("blake2b512") == PAKFIRE_DIGEST_BLAKE2B512);
	ASSERT(pakfire_digest_get_by_name("blake2s256") == PAKFIRE_DIGEST_BLAKE2S256);
	ASSERT(pakfire_digest_get_by_name("sha3-512") == PAKFIRE_DIGEST_SHA3_512);
	ASSERT(pakfire_digest_get_by_name("sha3-256") == PAKFIRE_DIGEST_SHA3_256);

	// Negative cases
	ASSERT(pakfire_digest_get_by_name("XXX") == 0);
	ASSERT(pakfire_digest_get_by_name(NULL) == 0);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_init);
	testsuite_add_test(test_random);
	testsuite_add_test(test_get_by_name);

	return testsuite_run(argc, argv);
}
