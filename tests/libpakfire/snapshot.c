/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdio.h>

#include <pakfire/snapshot.h>

#include "../testsuite.h"

static int test_create_and_restore(const struct test* t) {
	int r = EXIT_FAILURE;

	// Create a temporary file
	FILE* f = test_mktemp(NULL);
	ASSERT(f);

	// Create the snapshot
	ASSERT_SUCCESS(pakfire_snapshot_compress(t->pakfire, f));

	// Determine the size of the snapshot
	size_t size = ftell(f);

	LOG("Snapshot has a size of %jd bytes\n", size);

	// Check if the snapshot has something in it
	ASSERT(size >= 1024);

	// Rewind the file descriptor to the beginning
	rewind(f);

	// Perform a restore
	ASSERT_SUCCESS(pakfire_snapshot_extract(t->pakfire, f));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (f)
		fclose(f);

	return r;
}

static int test_invalid_inputs(const struct test* t) {
	int r = EXIT_FAILURE;

	// pakfire_snapshot_create
	ASSERT_ERRNO(pakfire_snapshot_compress(t->pakfire, NULL), EINVAL);

	// pakfire_snapshot_restore
	ASSERT_ERRNO(pakfire_snapshot_extract(t->pakfire, NULL), EINVAL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create_and_restore);
	testsuite_add_test(test_invalid_inputs);

	return testsuite_run(argc, argv);
}
