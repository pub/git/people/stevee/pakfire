/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_SNAPSHOT_H
#define PAKFIRE_SNAPSHOT_H

#ifdef PAKFIRE_PRIVATE

#include <stdio.h>

#include <pakfire/pakfire.h>

int pakfire_snapshot_store(struct pakfire* pakfire);
int pakfire_snapshot_restore(struct pakfire* pakfire);

int pakfire_snapshot_compress(struct pakfire* pakfire, FILE* f);
int pakfire_snapshot_extract(struct pakfire* pakfire, FILE* f);

#endif

#endif /* PAKFIRE_SNAPSHOT_H */
