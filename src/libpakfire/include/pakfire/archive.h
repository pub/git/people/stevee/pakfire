/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_ARCHIVE_H
#define PAKFIRE_ARCHIVE_H

#include <stddef.h>

struct pakfire_archive;

#include <pakfire/digest.h>
#include <pakfire/filelist.h>
#include <pakfire/package.h>
#include <pakfire/repo.h>
#include <pakfire/scriptlet.h>

int pakfire_archive_open(struct pakfire_archive** archive, struct pakfire* pakfire, const char* path);
struct pakfire_archive* pakfire_archive_ref(struct pakfire_archive* archive);
struct pakfire_archive* pakfire_archive_unref(struct pakfire_archive* archive);

FILE* pakfire_archive_read(struct pakfire_archive* archive, const char* filename);
int pakfire_archive_extract(struct pakfire_archive* archive);

const char* pakfire_archive_get_path(struct pakfire_archive* archive);

unsigned int pakfire_archive_get_format(struct pakfire_archive* archive);

struct pakfire_filelist* pakfire_archive_get_filelist(struct pakfire_archive* archive);

int pakfire_archive_verify(struct pakfire_archive* archive, int* status);

ssize_t pakfire_archive_get_size(struct pakfire_archive* archive);
int pakfire_archive_make_package(struct pakfire_archive* archive,
	struct pakfire_repo* repo, struct pakfire_package** package);

#ifdef PAKFIRE_PRIVATE

#include <pakfire/pakfire.h>

int pakfire_archive_copy(struct pakfire_archive* archive, const char* path);
int pakfire_archive_link_or_copy(struct pakfire_archive* archive, const char* path);

int pakfire_archive_check_digest(struct pakfire_archive* archive,
	const enum pakfire_digest_types type, const unsigned char* digest, const size_t length);

struct pakfire_scriptlet* pakfire_archive_get_scriptlet(
	struct pakfire_archive* archive, const char* type);

int pakfire_archive_apply_systemd_sysusers(struct pakfire_archive* archive);

#endif

#endif /* PAKFIRE_ARCHIVE_H */
