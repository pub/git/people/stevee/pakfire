/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_TRANSACTION_H
#define PAKFIRE_TRANSACTION_H

struct pakfire_transaction;

enum pakfire_transaction_flags {
	PAKFIRE_TRANSACTION_DRY_RUN     = (1 << 0),
};

struct pakfire_transaction* pakfire_transaction_ref(struct pakfire_transaction* transaction);
struct pakfire_transaction* pakfire_transaction_unref(struct pakfire_transaction* transaction);

size_t pakfire_transaction_count(struct pakfire_transaction* transaction);
char* pakfire_transaction_dump(struct pakfire_transaction* transaction, size_t width);

void pakfire_transaction_set_status_callback(
	struct pakfire_transaction* transaction, pakfire_status_callback callback, void* data);

int pakfire_transaction_run(struct pakfire_transaction* transaction, int flags);

int pakfire_transaction_download(struct pakfire_transaction* transaction);

#ifdef PAKFIRE_PRIVATE

#include <solv/solver.h>

int pakfire_transaction_create(struct pakfire_transaction** transaction,
	struct pakfire* pakfire, Solver* solver);

#endif

#endif /* PAKFIRE_TRANSACTION_H */
