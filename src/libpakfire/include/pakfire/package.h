/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PACKAGE_H
#define PAKFIRE_PACKAGE_H

#include <time.h>

#include <uuid/uuid.h>

struct pakfire_package;

#include <pakfire/digest.h>
#include <pakfire/filelist.h>
#include <pakfire/packagelist.h>
#include <pakfire/pakfire.h>
#include <pakfire/repo.h>

enum pakfire_package_key {
	PAKFIRE_PKG_NAME,
	PAKFIRE_PKG_EVR,
	PAKFIRE_PKG_ARCH,
	PAKFIRE_PKG_NEVRA,
	PAKFIRE_PKG_DBID,
	PAKFIRE_PKG_UUID,
	PAKFIRE_PKG_SUMMARY,
	PAKFIRE_PKG_DESCRIPTION,
	PAKFIRE_PKG_LICENSE,
	PAKFIRE_PKG_URL,
	PAKFIRE_PKG_GROUPS,
	PAKFIRE_PKG_VENDOR,
	PAKFIRE_PKG_DISTRO,
	PAKFIRE_PKG_PACKAGER,
	PAKFIRE_PKG_PATH,
	PAKFIRE_PKG_FILENAME,
	PAKFIRE_PKG_DOWNLOADSIZE,
	PAKFIRE_PKG_INSTALLSIZE,
	PAKFIRE_PKG_INSTALLTIME,
	PAKFIRE_PKG_BUILD_HOST,
	PAKFIRE_PKG_BUILD_ID,
	PAKFIRE_PKG_BUILD_TIME,
	PAKFIRE_PKG_SOURCE_PKG,
	PAKFIRE_PKG_SOURCE_NAME,
	PAKFIRE_PKG_SOURCE_EVR,
	PAKFIRE_PKG_SOURCE_ARCH,

	// Dependencies
	PAKFIRE_PKG_PROVIDES,
	PAKFIRE_PKG_PREREQUIRES,
	PAKFIRE_PKG_REQUIRES,
	PAKFIRE_PKG_CONFLICTS,
	PAKFIRE_PKG_OBSOLETES,
	PAKFIRE_PKG_RECOMMENDS,
	PAKFIRE_PKG_SUGGESTS,
	PAKFIRE_PKG_SUPPLEMENTS,
	PAKFIRE_PKG_ENHANCES,
};

int pakfire_package_create(struct pakfire_package** package, struct pakfire* pakfire,
	struct pakfire_repo* repo, const char* name, const char* evr, const char* arch);

struct pakfire_package* pakfire_package_ref(struct pakfire_package* pkg);
struct pakfire_package* pakfire_package_unref(struct pakfire_package* pkg);
struct pakfire* pakfire_package_get_pakfire(struct pakfire_package* pkg);

int pakfire_package_eq(struct pakfire_package* pkg1, struct pakfire_package* pkg2);
int pakfire_package_cmp(struct pakfire_package* pkg1, struct pakfire_package* pkg2);
int pakfire_package_evr_cmp(struct pakfire_package* pkg1, struct pakfire_package* pkg2);

unsigned int pakfire_package_id(struct pakfire_package* pkg);

// String
const char* pakfire_package_get_string(struct pakfire_package* pkg,
	const enum pakfire_package_key key);
int pakfire_package_set_string(struct pakfire_package* pkg,
	const enum pakfire_package_key key, const char* value);

// UUID
int pakfire_package_get_uuid(struct pakfire_package* pkg,
	const enum pakfire_package_key key, uuid_t uuid);
int pakfire_package_set_uuid(struct pakfire_package* pkg,
	const enum pakfire_package_key key, const uuid_t uuid);

// Numeric
unsigned long long pakfire_package_get_num(struct pakfire_package* pkg,
	const enum pakfire_package_key key, unsigned long long notfound);
int pakfire_package_set_num(struct pakfire_package* pkg,
	const enum pakfire_package_key key, unsigned long long num);

// Dependencies
char** pakfire_package_get_deps(struct pakfire_package* pkg,
	const enum pakfire_package_key key);

const unsigned char* pakfire_package_get_digest(struct pakfire_package* pkg,
	enum pakfire_digest_types* type, size_t* length);
int pakfire_package_set_digest(struct pakfire_package* pkg,
	enum pakfire_digest_types type, const unsigned char* digest, const size_t length);
size_t pakfire_package_get_size(struct pakfire_package* pkg);

int pakfire_package_get_reverse_requires(struct pakfire_package* pkg,
	struct pakfire_packagelist* list);

struct pakfire_repo* pakfire_package_get_repo(struct pakfire_package* pkg);

char* pakfire_package_dump(struct pakfire_package* pkg, int flags);

struct pakfire_archive* pakfire_package_get_archive(struct pakfire_package* pkg);

struct pakfire_filelist* pakfire_package_get_filelist(struct pakfire_package* pkg);
int pakfire_package_set_filelist(struct pakfire_package* pkg, struct pakfire_filelist* filelist);
int pakfire_package_set_filelist_from_string(struct pakfire_package* pkg, const char* files);

enum pakfire_package_dump_flags {
	PAKFIRE_PKG_DUMP_FILELIST = 1 << 0,
	PAKFIRE_PKG_DUMP_LONG     = 1 << 1,
};

#ifdef PAKFIRE_PRIVATE

#include <stdint.h>

#include <json.h>

#include <solv/pooltypes.h>

int pakfire_package_create_from_solvable(struct pakfire_package** package,
	struct pakfire* pakfire, Id id);

int pakfire_package_is_source(struct pakfire_package* pkg);

char* pakfire_package_join_evr(const char* e, const char* v, const char* r);

int pakfire_package_is_installed(struct pakfire_package* pkg);

// Filelist
int pakfire_package_append_file(struct pakfire_package* pkg, const char* path);

// Dependencies
int pakfire_package_add_dep(struct pakfire_package* pkg,
	const enum pakfire_package_key key, const char* dep);

int pakfire_package_has_rich_deps(struct pakfire_package* pkg);
int pakfire_package_matches_dep(struct pakfire_package* pkg,
	const enum pakfire_package_key key, const char* dep);

struct json_object* pakfire_package_to_json(struct pakfire_package* pkg);

#endif

#endif /* PAKFIRE_PACKAGE_H */
