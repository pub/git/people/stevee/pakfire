/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <errno.h>
#include <fts.h>
#include <linux/limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/file.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

#include <archive.h>
#include <archive_entry.h>
#include <gpgme.h>
#include <magic.h>
#include <solv/evr.h>
#include <solv/pool.h>
#include <solv/poolarch.h>
#include <solv/queue.h>

#include <pakfire/arch.h>
#include <pakfire/build.h>
#include <pakfire/config.h>
#include <pakfire/constants.h>
#include <pakfire/db.h>
#include <pakfire/dependencies.h>
#include <pakfire/keystore.h>
#include <pakfire/logging.h>
#include <pakfire/mount.h>
#include <pakfire/package.h>
#include <pakfire/packagelist.h>
#include <pakfire/pakfire.h>
#include <pakfire/parser.h>
#include <pakfire/private.h>
#include <pakfire/pwd.h>
#include <pakfire/repo.h>
#include <pakfire/request.h>
#include <pakfire/string.h>
#include <pakfire/transaction.h>
#include <pakfire/ui.h>
#include <pakfire/util.h>

#define KEYSTORE_DIR "/etc/pakfire/trusted.keys.d"
#define LOCK_PATH PAKFIRE_PRIVATE_DIR "/.lock"

struct pakfire {
	int nrefs;

	char path[PATH_MAX];
	char lock_path[PATH_MAX];
	char cache_path[PATH_MAX];
	char arch[ARCH_MAX];
	char keystore_path[PATH_MAX];

	int flags;

	// Lock
	FILE* lock;

	// UID/GID of running user
	uid_t uid;
	gid_t gid;

	// Mapped UID/GID
	struct pakfire_subid subuid;
	struct pakfire_subid subgid;

	// Pool
	Pool* pool;

	// Callbacks
	struct pakfire_callbacks {
		// Logging
		pakfire_log_callback log;
		void* log_data;

		// Confirm
		pakfire_confirm_callback confirm;
		void* confirm_data;
	} callbacks;

	// Logging
	int log_priority;

	struct pakfire_config* config;

	struct pakfire_distro {
		char pretty_name[256];
		char name[64];
		char id[32];
		char slogan[256];
		char vendor[64];
		char version[64];
		char version_codename[32];
		char version_id[8];
		char tag[40];
	} distro;

	// GPG Context
	gpgme_ctx_t gpgctx;

	// Magic Context
	magic_t magic;

	// States
	int destroy_on_free:1;
	int pool_ready:1;
	int in_free:1;
};

/*
	This is a list of all features that are supported by this version of Pakfire
*/
static const struct pakfire_feature {
	const char* name;
} features[] = {
	{ "RichDependencies" },

	// Package Formats
	{ "PackageFormat-6" },
	{ "PackageFormat-5" },

	// Compression
	{ "Compress-XZ" },
	{ "Compress-Zstandard" },

	// Digests
	{ "Digest-BLAKE2b512" },
	{ "Digest-BLAKE2s256" },
	{ "Digest-SHA3-512" },
	{ "Digest-SHA3-256" },
	{ "Digest-SHA2-512" },
	{ "Digest-SHA2-256" },

	// Systemd
	{ "systemd-sysusers" },
	{ "systemd-tmpfiles" },

	// The end
	{ NULL },
};

int pakfire_on_root(struct pakfire* pakfire) {
	return (strcmp(pakfire->path, "/") == 0);
}

uid_t pakfire_uid(struct pakfire* pakfire) {
	return pakfire->uid;
}

gid_t pakfire_gid(struct pakfire* pakfire) {
	return pakfire->gid;
}

const struct pakfire_subid* pakfire_subuid(struct pakfire* pakfire) {
	return &pakfire->subuid;
}

const struct pakfire_subid* pakfire_subgid(struct pakfire* pakfire) {
	return &pakfire->subgid;
}

/*
	Maps any UID/GIDs to the SUBUID/SUBGIDs so that we can transparently
	copy files in and out of the jail environment.
*/
static unsigned int pakfire_map_id(struct pakfire* pakfire,
		const struct pakfire_subid* subid, const unsigned int id) {
	// Nothing to do if we are running on root
	if (pakfire_on_root(pakfire))
		return id;

	// Map the ID
	unsigned int mapped_id = subid->id + id;

	// Check if the ID is in range
	if (id > subid->length) {
		ERROR(pakfire, "Mapped ID is out of range. Setting to %u\n", subid->id);
		mapped_id = subid->id;
	}

	DEBUG(pakfire, "Mapping UID/GID %u to %u\n", id, mapped_id);

	return mapped_id;
}

static unsigned int pakfire_unmap_id(struct pakfire* pakfire,
		const struct pakfire_subid* subid, const unsigned int id) {
	// Nothing to do if we are running on root
	if (pakfire_on_root(pakfire))
		return id;

	// Unmap the ID
	int unmapped_id = id - subid->id;

	// Check if the ID is in range
	if (unmapped_id < 0) {
		ERROR(pakfire, "Mapped ID is out of range. Setting to %d\n", subid->id);
		unmapped_id = subid->id;
	}

	DEBUG(pakfire, "Mapping UID/GID %d from %d\n", unmapped_id, id);

	return unmapped_id;
}

static int log_priority(const char* priority) {
	char* end;

	int prio = strtol(priority, &end, 10);
	if (*end == '\0' || isspace(*end))
		return prio;

	if (strncmp(priority, "error", strlen("error")) == 0)
		return LOG_ERR;

	if (strncmp(priority, "info", strlen("info")) == 0)
		return LOG_INFO;

	if (strncmp(priority, "debug", strlen("debug")) == 0)
		return LOG_DEBUG;

	return 0;
}

static void pool_log(Pool* pool, void* data, int type, const char* s) {
	struct pakfire* pakfire = (struct pakfire*)data;

	DEBUG(pakfire, "pool: %s", s);
}

static Id pakfire_handle_ns_pakfire(struct pakfire* pakfire, const char* name) {
	// Find all supported features
	for (const struct pakfire_feature* feature = features; feature->name; feature++) {
		if (strcmp(feature->name, name) == 0)
			return 1;
	}

	// Not supported
	return 0;
}

static Id pakfire_handle_ns_arch(struct pakfire* pakfire, const char* name) {
	const char* arch = pakfire_get_arch(pakfire);

	return strcmp(arch, name) == 0;
}

static Id pakfire_namespace_callback(Pool* pool, void* data, Id ns, Id id) {
	struct pakfire* pakfire = (struct pakfire*)data;

	const char* namespace = pool_id2str(pool, ns);
	const char* name = pakfire_dep2str(pakfire, id);

	DEBUG(pakfire, "Namespace callback called for %s(%s)\n", namespace, name);

	// Handle the pakfire() namespace
	if (strcmp(namespace, "pakfire") == 0)
		return pakfire_handle_ns_pakfire(pakfire, name);

	// Handle the arch() namespace
	else if (strcmp(namespace, "arch") == 0)
		return pakfire_handle_ns_arch(pakfire, name);

	// Not handled here
	else
		return 0;
}

static int pakfire_populate_pool(struct pakfire* pakfire) {
	struct pakfire_db* db = NULL;
	struct pakfire_repo* commandline = NULL;
	struct pakfire_repo* dummy = NULL;
	struct pakfire_repo* system = NULL;
	int r;

	// Initialize the pool
	Pool* pool = pakfire->pool = pool_create();
	pool_setdisttype(pool, DISTTYPE_RPM);

#ifdef ENABLE_DEBUG
	// Enable debug output
	pool_setdebuglevel(pool, 3);
#endif

	// Set architecture of the pool
	pool_setarch(pool, pakfire->arch);

	// Set path
	pool_set_rootdir(pool, pakfire->path);

	// Set debug callback
	pool_setdebugcallback(pool, pool_log, pakfire);

	// Install namespace callback
	pool_setnamespacecallback(pool, pakfire_namespace_callback, pakfire);

	// Open database in read-only mode and try to load all installed packages
	r = pakfire_db_open(&db, pakfire, PAKFIRE_DB_READWRITE);
	if (r)
		goto ERROR;

	// Create a dummy repository
	r = pakfire_repo_create(&dummy, pakfire, PAKFIRE_REPO_DUMMY);
	if (r)
		goto ERROR;

	// Disable the repository
	pakfire_repo_set_enabled(dummy, 0);

	// Create the system repository
	r = pakfire_repo_create(&system, pakfire, PAKFIRE_REPO_SYSTEM);
	if (r)
		goto ERROR;

	// Set this repository as the installed one
	pool_set_installed(pool, pakfire_repo_get_repo(system));

	// Create the command line repo
	r = pakfire_repo_create(&commandline, pakfire, PAKFIRE_REPO_COMMANDLINE);
	if (r)
		goto ERROR;

	// Load database content
	r = pakfire_db_load(db, system);
	if (r)
		goto ERROR;

ERROR:
	if (db)
		pakfire_db_unref(db);
	if (commandline)
		pakfire_repo_unref(commandline);
	if (dummy)
		pakfire_repo_unref(dummy);
	if (system)
		pakfire_repo_unref(system);

	return r;
}

static void pakfire_free(struct pakfire* pakfire) {
	struct pakfire_repo* repo = NULL;
	int r;

	// Avoid recursive free
	if (pakfire->in_free++)
		return;

	// Destroy the commandline repository
	repo = pakfire_get_repo(pakfire, PAKFIRE_REPO_COMMANDLINE);
	if (repo) {
		r = pakfire_repo_clean(repo, PAKFIRE_REPO_CLEAN_FLAGS_DESTROY);
		if (r)
			ERROR(pakfire, "Could not cleanup %s repository: %m\n", PAKFIRE_REPO_COMMANDLINE);

		pakfire_repo_unref(repo);
	}

	// Release Magic Context
	if (pakfire->magic)
		magic_close(pakfire->magic);

	// Release GPGME context
	if (pakfire->gpgctx)
		pakfire_keystore_destroy(pakfire, &pakfire->gpgctx);

	// Release lock (if not already done so)
	pakfire_release_lock(pakfire);

	if (pakfire->destroy_on_free && *pakfire->path) {
		DEBUG(pakfire, "Destroying %s\n", pakfire->path);

		// Destroy the temporary directory
		pakfire_rmtree(pakfire->path, 0);
	}

	pakfire_repo_free_all(pakfire);

	if (pakfire->pool)
		pool_free(pakfire->pool);

	if (pakfire->config)
		pakfire_config_unref(pakfire->config);

	free(pakfire);
}

// Safety check in case this is being launched on the host system
static int pakfire_safety_checks(struct pakfire* pakfire) {
	// Nothing to do if we are not working on root
	if (!pakfire_on_root(pakfire))
		return 0;

	// We must be root in order to operate in /
	if (pakfire->uid) {
		ERROR(pakfire, "Must be running as root on /\n");
		errno = EPERM;
		return 1;
	}

	if (strcmp(pakfire->distro.id, "ipfire") != 0) {
		ERROR(pakfire, "Not an IPFire system\n");
		errno = EPERM;
		return 1;
	}

	return 0;
}

static int pakfire_read_repo_config(struct pakfire* pakfire) {
	char path[PATH_MAX];
	int r;

	// Make path absolute
	r = pakfire_path(pakfire, path, "%s", PAKFIRE_CONFIG_DIR "/repos");
	if (r)
		return r;

	DEBUG(pakfire, "Reading repository configuration from %s\n", path);

	char* paths[2] = {
		path, NULL,
	};
	r = 1;

	FTS* d = fts_open(paths, FTS_NOCHDIR|FTS_NOSTAT, NULL);
	if (!d)
		goto ERROR;

	for (;;) {
		FTSENT* fent = fts_read(d);
		if (!fent)
			break;

		// Only handle files
		if (fent->fts_info & FTS_F) {
			// Skip everything that doesn't end in .repo
			if (!pakfire_string_endswith(fent->fts_name, ".repo"))
				continue;

			DEBUG(pakfire, "Reading %s\n", fent->fts_path);

			FILE* f = fopen(fent->fts_path, "r");
			if (!f)
				goto ERROR;

			// Parse the configuration file
			r = pakfire_config_read(pakfire->config, f);
			fclose(f);

			if (r)
				goto ERROR;
		}
	}

	// Success
	r = 0;

ERROR:
	if (d)
		fts_close(d);

	return r;
}

const char* pakfire_get_distro_name(struct pakfire* pakfire) {
	if (*pakfire->distro.name)
		return pakfire->distro.name;

	return NULL;
}

const char* pakfire_get_distro_id(struct pakfire* pakfire) {
	if (*pakfire->distro.id)
		return pakfire->distro.id;

	return NULL;
}

const char* pakfire_get_distro_vendor(struct pakfire* pakfire) {
	if (*pakfire->distro.vendor)
		return pakfire->distro.vendor;

	return NULL;
}

const char* pakfire_get_distro_version(struct pakfire* pakfire) {
	if (*pakfire->distro.version)
		return pakfire->distro.version;

	return NULL;
}

const char* pakfire_get_distro_version_id(struct pakfire* pakfire) {
	if (*pakfire->distro.version_id)
		return pakfire->distro.version_id;

	return NULL;
}

const char* pakfire_get_distro_tag(struct pakfire* pakfire) {
	int r;

	if (*pakfire->distro.tag)
		return pakfire->distro.tag;

	const char* id         = pakfire_get_distro_id(pakfire);
	const char* version_id = pakfire_get_distro_version_id(pakfire);

	// Break if the configuration is incomplete
	if (!id || !version_id) {
		errno = EINVAL;
		return NULL;
	}

	// Format string
	r = pakfire_string_format(pakfire->distro.tag, "%s%s", id, version_id);
	if (r)
		return NULL;

	// Return the tag
	return pakfire->distro.tag;
}

static int pakfire_config_import_distro(struct pakfire* pakfire) {
	// Nothing to do if there is no distro section
	if (!pakfire_config_has_section(pakfire->config, "distro"))
		return 0;

	// Name
	const char* name = pakfire_config_get(pakfire->config, "distro", "name", NULL);
	if (name)
		pakfire_string_set(pakfire->distro.name, name);

	// ID
	const char* id = pakfire_config_get(pakfire->config, "distro", "id", NULL);
	if (id)
		pakfire_string_set(pakfire->distro.id, id);

	// Version ID
	const char* version_id = pakfire_config_get(pakfire->config, "distro", "version_id", NULL);
	if (version_id)
		pakfire_string_set(pakfire->distro.version_id, version_id);

	// Codename
	const char* codename = pakfire_config_get(pakfire->config, "distro", "codename", NULL);
	if (codename)
		pakfire_string_set(pakfire->distro.version_codename, codename);

	// Fill in version
	if (*pakfire->distro.version_codename)
		pakfire_string_format(pakfire->distro.version, "%s (%s)",
			pakfire->distro.version_id, pakfire->distro.version_codename);
	else
		pakfire_string_set(pakfire->distro.version, pakfire->distro.version_id);

	// Fill in pretty name
	pakfire_string_format(pakfire->distro.pretty_name, "%s %s",
		pakfire->distro.name, pakfire->distro.version);

	// Vendor
	const char* vendor = pakfire_config_get(pakfire->config, "distro", "vendor", NULL);
	if (vendor)
		pakfire_string_set(pakfire->distro.vendor, vendor);

	// Slogan
	const char* slogan = pakfire_config_get(pakfire->config, "distro", "slogan", NULL);
	if (slogan)
		pakfire_string_set(pakfire->distro.slogan, slogan);

	return 0;
}

static int pakfire_read_config(struct pakfire* pakfire, const char* path) {
	char default_path[PATH_MAX];
	int r;

	// Use default path if none set
	if (!path) {
		r = pakfire_path(pakfire, default_path, "%s", PAKFIRE_CONFIG_DIR "/general.conf");
		if (r)
			return r;

		path = default_path;
	}

	DEBUG(pakfire, "Reading configuration from %s\n", path);

	FILE* f = fopen(path, "r");
	if (!f) {
		// Silently ignore when there is no default configuration file
		if (*default_path && errno == ENOENT)
			return 0;

		ERROR(pakfire, "Could not open configuration file %s: %m\n", path);
		return 1;
	}

	// Read configuration from file
	r = pakfire_config_read(pakfire->config, f);
	if (r) {
		ERROR(pakfire, "Could not parse configuration file %s: %m\n", path);
		goto ERROR;
	}

	// Import distro configuration
	r = pakfire_config_import_distro(pakfire);
	if (r)
		goto ERROR;

	// Read repository configuration
	r = pakfire_read_repo_config(pakfire);
	if (r)
		goto ERROR;

ERROR:
	fclose(f);

	return r;
}

static int pakfire_read_os_release(struct pakfire* pakfire) {
	char path[PATH_MAX];
	char* line = NULL;
	size_t l = 0;

	int r = pakfire_path(pakfire, path, "%s", "/etc/os-release");
	if (r)
		return r;

	r = 1;

	FILE* f = fopen(path, "r");
	if (!f) {
		// Ignore when the file does not exist
		if (errno == ENOENT) {
			r = 0;
			goto ERROR;
		}

		ERROR(pakfire, "Could not open %s: %m\n", path);
		goto ERROR;
	}

	while (1) {
		ssize_t bytes_read = getline(&line, &l, f);
		if (bytes_read < 0)
			break;

		// Remove trailing newline
		pakfire_remove_trailing_newline(line);

		// Find =
		char* delim = strchr(line, '=');
		if (!delim)
			continue;

		// Replace = by NULL
		*delim = '\0';

		// Set key and val to the start of the strings
		char* key = line;
		char* val = delim + 1;

		// Unquote val
		val = pakfire_unquote_in_place(val);

		if (strcmp(key, "PRETTY_NAME") == 0)
			r = pakfire_string_set(pakfire->distro.pretty_name, val);
		else if (strcmp(key, "NAME") == 0)
			r = pakfire_string_set(pakfire->distro.name, val);
		else if (strcmp(key, "ID") == 0)
			r = pakfire_string_set(pakfire->distro.id, val);
		else if (strcmp(key, "VERSION") == 0)
			r = pakfire_string_set(pakfire->distro.version, val);
		else if (strcmp(key, "VERSION_CODENAME") == 0)
			r = pakfire_string_set(pakfire->distro.version_codename, val);
		else if (strcmp(key, "VERSION_ID") == 0)
			r = pakfire_string_set(pakfire->distro.version_id, val);
		else
			continue;

		// Catch any errors
		if (r)
			goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (f)
		fclose(f);
	if (line)
		free(line);

	return r;
}

static int pakfire_set_cache_path(struct pakfire* pakfire) {
	char basepath[PATH_MAX];
	int r;

	// Fetch the path from the configuration file
	const char* path = pakfire_config_get(pakfire->config, "general", "cache_path", NULL);
	if (!path) {
		const char* home = pakfire_get_home(pakfire, pakfire->uid);
		if (!home)
			return 1;

		// Append a suffix to the home directory
		r = pakfire_string_format(basepath, "%s/.pakfire/cache", home);
		if (r)
			return 1;

		// Use the basepath as path
		path = basepath;
	}

	// Format the final path
	return pakfire_string_format(pakfire->cache_path, "%s/%s/%s/%s",
		path, pakfire->distro.id, pakfire->distro.version_id, pakfire->arch);
}

PAKFIRE_EXPORT int pakfire_create(struct pakfire** pakfire, const char* path,
		const char* arch, const char* conf, int flags,
		int loglevel, pakfire_log_callback log_callback, void* log_data) {
	char tempdir[PATH_MAX] = PAKFIRE_TMP_DIR "/pakfire-root-XXXXXX";
	char private_dir[PATH_MAX];
	int r = 1;

	// Reset pakfire pointer
	*pakfire = NULL;

	// Default to the native architecture
	if (!arch)
		arch = pakfire_arch_native();

	// Check if the architecture is supported
	if (!pakfire_arch_supported(arch)) {
		errno = EINVAL;
		return 1;
	}

	// Path must be absolute
	if (path && !pakfire_string_startswith(path, "/")) {
		errno = EINVAL;
		return 1;
	}

	struct pakfire* p = calloc(1, sizeof(*p));
	if (!p)
		return 1;

	p->nrefs = 1;
	p->flags = flags;

	// Store the UID/GID we are running as
	p->uid = geteuid();
	p->gid = getegid();

	// Set architecture
	pakfire_string_set(p->arch, arch);

	// Setup logging
	if (log_callback)
		pakfire_set_log_callback(p, log_callback, log_data);
	else
		pakfire_set_log_callback(p, pakfire_log_syslog, NULL);

	// Set log level
	if (loglevel) {
		pakfire_log_set_priority(p, loglevel);
	} else {
		const char* env = secure_getenv("PAKFIRE_LOG");
		if (env)
			pakfire_log_set_priority(p, log_priority(env));
	}

	// Initialise configuration
	r = pakfire_config_create(&p->config);
	if (r)
		goto ERROR;

	// Generate a random path if none is set
	if (!path) {
		path = pakfire_mkdtemp(tempdir);
		if (!path) {
			r = 1;
			goto ERROR;
		}

		// Destroy everything when done
		p->destroy_on_free = 1;
	}

	// Set path
	pakfire_string_set(p->path, path);

	// Read /etc/os-release
	r = pakfire_read_os_release(p);
	if (r && errno != ENOENT)
		goto ERROR;

	// Bump RLIMIT_NOFILE to maximum
	r = pakfire_rlimit_set(p, PAKFIRE_RLIMIT_NOFILE_MAX);
	if (r)
		goto ERROR;

	DEBUG(p, "Pakfire initialized at %p\n", p);
	DEBUG(p, "  arch   = %s\n", pakfire_get_arch(p));
	DEBUG(p, "  path   = %s\n", pakfire_get_path(p));

	// Fetch sub UID/GIDs
	if (!pakfire_on_root(p)) {
		// UID
		r = pakfire_getsubid(p, "/etc/subuid", p->uid, &p->subuid);
		if (r) {
			ERROR(p, "Could not fetch subuid: %m\n");
			goto ERROR;
		}

		// GID
		r = pakfire_getsubid(p, "/etc/subgid", p->uid, &p->subgid);
		if (r) {
			ERROR(p, "Could not fetch subgid: %m\n");
			goto ERROR;
		}

		// Log
		DEBUG(p, "  subuid = %u - %zu\n", p->subuid.id, p->subuid.id + p->subuid.length);
		DEBUG(p, "  subgid = %u - %zu\n", p->subgid.id, p->subgid.id + p->subgid.length);
	}

	// Perform some safety checks
	r = pakfire_safety_checks(p);
	if (r)
		goto ERROR;

	// Read configuration file
	r = pakfire_read_config(p, conf);
	if (r)
		goto ERROR;

	// Dump distribution configuration
	DEBUG(p, "  Distribution: %s\n", p->distro.pretty_name);
	DEBUG(p, "    name       = %s\n", p->distro.name);
	DEBUG(p, "    id         = %s\n", p->distro.id);
	DEBUG(p, "    version    = %s\n", p->distro.version);
	DEBUG(p, "    version_id = %s\n", p->distro.version_id);
	if (*p->distro.version_codename)
		DEBUG(p, "    codename   = %s\n", p->distro.version_codename);
	if (*p->distro.vendor)
		DEBUG(p, "    vendor     = %s\n", p->distro.vendor);
	if (*p->distro.slogan)
		DEBUG(p, "    slogan     = %s\n", p->distro.slogan);

	// Set lock path
	r = pakfire_path(p, p->lock_path, "%s", LOCK_PATH);
	if (r) {
		ERROR(p, "Could not set lock path: %m\n");
		goto ERROR;
	}

	// Set cache path
	r = pakfire_set_cache_path(p);
	if (r) {
		ERROR(p, "Could not set cache path: %m\n");
		goto ERROR;
	}

	// Set keystore path
	r = pakfire_path(p, p->keystore_path, "%s", KEYSTORE_DIR);
	if (r) {
		ERROR(p, "Could not set keystore path: %m\n");
		goto ERROR;
	}

	// Make path for private files
	r = pakfire_path(p, private_dir, "%s", PAKFIRE_PRIVATE_DIR);
	if (r)
		goto ERROR;

	// Make sure that our private directory exists
	r = pakfire_mkdir(private_dir, 0755);
	if (r) {
		ERROR(p, "Could not create private directory %s: %m\n", private_dir);
		goto ERROR;
	}

	// Initialize keystore
	r = pakfire_keystore_init(p, &p->gpgctx);
	if (r)
		goto ERROR;

	// Populate pool
	r = pakfire_populate_pool(p);
	if (r)
		goto ERROR;

	// Create repositories
	r = pakfire_repo_import(p, p->config);
	if (r)
		goto ERROR;

	*pakfire = p;

	return 0;

ERROR:
	pakfire_free(p);

	return r;
}

PAKFIRE_EXPORT struct pakfire* pakfire_ref(struct pakfire* pakfire) {
	++pakfire->nrefs;

	return pakfire;
}

PAKFIRE_EXPORT struct pakfire* pakfire_unref(struct pakfire* pakfire) {
	if (--pakfire->nrefs > 0)
		return pakfire;

	pakfire_free(pakfire);

	return NULL;
}

int pakfire_has_flag(struct pakfire* pakfire, int flag) {
	return pakfire->flags & flag;
}

struct pakfire_config* pakfire_get_config(struct pakfire* pakfire) {
	if (!pakfire->config)
		return NULL;

	return pakfire_config_ref(pakfire->config);
}

PAKFIRE_EXPORT const char* pakfire_get_path(struct pakfire* pakfire) {
	return pakfire->path;
}

int pakfire_acquire_lock(struct pakfire* pakfire) {
	int r;

	// Check if the lock is already held
	if (pakfire->lock) {
		ERROR(pakfire, "Lock is already been acquired by this process\n");
		errno = ENOLCK;
		return 1;
	}

	DEBUG(pakfire, "Acquire lock...\n");

	// Ensure the parent directory exists
	pakfire_mkparentdir(pakfire->lock_path, 0755);

	// Open the lock file
	pakfire->lock = fopen(pakfire->lock_path, "w");
	if (!pakfire->lock) {
		ERROR(pakfire, "Could not open lock file %s: %m\n", pakfire->lock_path);
		return 1;
	}

	// Attempt to lock the file exclusively
	while (1) {
		r = flock(fileno(pakfire->lock), LOCK_EX|LOCK_NB);

		// Success!
		if (r == 0)
			goto SUCCESS;

		DEBUG(pakfire, "Could not acquire lock %s: %m\n", pakfire->lock_path);

		// Wait 500ms until the next attempt
		usleep(500000);
	}

SUCCESS:
	DEBUG(pakfire, "Lock acquired\n");

	return 0;
}

void pakfire_release_lock(struct pakfire* pakfire) {
	if (!pakfire->lock)
		return;

	DEBUG(pakfire, "Releasing lock\n");

	fclose(pakfire->lock);
	pakfire->lock = NULL;

	// Attempt to unlink the lock file
	unlink(pakfire->lock_path);
}

const char* pakfire_get_keystore_path(struct pakfire* pakfire) {
	return pakfire->keystore_path;
}

int __pakfire_path(struct pakfire* pakfire, char* path, const size_t length,
		const char* format, ...) {
	char buffer[PATH_MAX];
	va_list args;
	int r;

	// Format input into buffer
	va_start(args, format);
	r = __pakfire_string_vformat(buffer, sizeof(buffer), format, args);
	va_end(args);

	// Break on any errors
	if (r)
		return r;

	// Join paths together
	return __pakfire_path_join(path, length, pakfire->path, buffer);
}

const char* pakfire_relpath(struct pakfire* pakfire, const char* path) {
	return pakfire_path_relpath(pakfire->path, path);
}

int __pakfire_cache_path(struct pakfire* pakfire, char* path, size_t length,
		const char* format, ...) {
	char buffer[PATH_MAX];
	va_list args;
	int r;

	// Format input into buffer
	va_start(args, format);
	r = __pakfire_string_vformat(buffer, sizeof(buffer), format, args);
	va_end(args);

	// Break on any errors
	if (r)
		return r;

	// Join paths together
	return __pakfire_path_join(path, length, pakfire->cache_path, buffer);
}

gpgme_ctx_t pakfire_get_gpgctx(struct pakfire* pakfire) {
	return pakfire->gpgctx;
}

magic_t pakfire_get_magic(struct pakfire* pakfire) {
	int r;

	// Initialize the context if not already done
	if (!pakfire->magic) {
		// Allocate a new context
		pakfire->magic = magic_open(MAGIC_MIME_TYPE | MAGIC_ERROR | MAGIC_NO_CHECK_TOKENS);
		if (!pakfire->magic) {
			ERROR(pakfire, "Could not allocate magic context: %m\n");
			return NULL;
		}

		// Load the database
		r = magic_load(pakfire->magic, NULL);
		if (r) {
			ERROR(pakfire, "Could not open the magic database: %s\n",
				magic_error(pakfire->magic));
			goto ERROR;
		}
	}

	return pakfire->magic;

ERROR:
	if (pakfire->magic)
		magic_close(pakfire->magic);

	// Reset the pointer
	pakfire->magic = NULL;

	return NULL;
}

PAKFIRE_EXPORT int pakfire_list_keys(struct pakfire* pakfire, struct pakfire_key*** keys) {
	// Reset keys
	*keys = NULL;

	// Fetch GPGME context
	gpgme_ctx_t gpgctx = pakfire_get_gpgctx(pakfire);
	if (!gpgctx)
		return 1;

	struct pakfire_key* key = NULL;
	gpgme_key_t gpgkey = NULL;
	size_t length = 0;
	int r = 1;

	// Iterate over all keys and import them to the list
	gpgme_error_t e = gpgme_op_keylist_start(gpgctx, NULL, 0);
	if (e)
		goto ERROR;

	while (!e) {
		e = gpgme_op_keylist_next(gpgctx, &gpgkey);
		if (e)
			break;

		// Create key
		r = pakfire_key_create(&key, pakfire, gpgkey);
		gpgme_key_unref(gpgkey);
		if (r)
			goto ERROR;

		// Append to keys
		*keys = reallocarray(*keys, length + 2, sizeof(**keys));
		if (!*keys) {
			r = 1;
			goto ERROR;
		}

		// Store key in array
		(*keys)[length++] = key;

		// Terminate the array
		(*keys)[length] = NULL;
	}

	// Done
	return 0;

ERROR:
	if (*keys) {
		for (struct pakfire_key** _key = *keys; *_key; _key++)
			pakfire_key_unref(*_key);
		free(*keys);
		keys = NULL;
	}

	return r;
}

int pakfire_repo_walk(struct pakfire* pakfire,
		pakfire_repo_walk_callback callback, void* p) {
	struct pakfire_repo* repo = NULL;
	Repo* solv_repo = NULL;
	int i = 0;
	int r;

	Pool* pool = pakfire->pool;

	// Run func for every repository
	FOR_REPOS(i, solv_repo) {
		repo = pakfire_repo_create_from_repo(pakfire, solv_repo);
		if (!repo)
			return 1;

		// Run callback
		r = callback(pakfire, repo, p);
		pakfire_repo_unref(repo);

		// Raise any errors
		if (r)
			break;
	}

	return r;
}

static int __pakfire_repo_clean(struct pakfire* pakfire, struct pakfire_repo* repo,
		void* p) {
	int flags = *(int*)p;

	return pakfire_repo_clean(repo, flags);
}

PAKFIRE_EXPORT int pakfire_clean(struct pakfire* pakfire, int flags) {
	int r;

	// Clean all repositories
	r = pakfire_repo_walk(pakfire, __pakfire_repo_clean, &flags);
	if (r)
		return r;

	// Clean build environments
	r = pakfire_build_clean(pakfire, flags);
	if (r)
		return r;

	// Remove the cache
	return pakfire_rmtree(PAKFIRE_CACHE_DIR, 0);
}

static int __pakfire_repo_refresh(struct pakfire* pakfire, struct pakfire_repo* repo,
		void* p) {
	int flags = *(int*)p;

	return pakfire_repo_refresh(repo, flags);
}

PAKFIRE_EXPORT int pakfire_refresh(struct pakfire* pakfire, int flags) {
	return pakfire_repo_walk(pakfire, __pakfire_repo_refresh, &flags);
}

enum pakfire_copy_direction {
	PAKFIRE_COPY_IN,
	PAKFIRE_COPY_OUT,
};

static int pakfire_copy(struct pakfire* pakfire, const char* src, const char* dst,
		enum pakfire_copy_direction direction) {
	struct archive* reader = NULL;
	struct archive* writer = NULL;
	struct archive_entry* entry = NULL;
	FILE* f = NULL;
	int r = 1;

	DEBUG(pakfire, "Copying %s to %s\n", src, dst);

	// Open the source file
	f = fopen(src, "r");
	if (!f) {
		ERROR(pakfire, "Could not open %s: %m\n", src);
		goto ERROR;
	}

	// Allocate reader
	reader = pakfire_make_archive_disk_reader(pakfire, (direction == PAKFIRE_COPY_OUT));
	if (!reader)
		goto ERROR;

	// Allocate writer
	writer = pakfire_make_archive_disk_writer(pakfire, (direction == PAKFIRE_COPY_IN));
	if (!writer)
		goto ERROR;

	// Create a new entry
	entry = archive_entry_new();
	if (!entry)
		goto ERROR;

	// Set the source path
	archive_entry_copy_sourcepath(entry, src);

	// Set the destination path
	archive_entry_set_pathname(entry, dst);

	// Read everything from source file
	r = archive_read_disk_entry_from_file(reader, entry, fileno(f), NULL);
	if (r) {
		ERROR(pakfire, "Could not read from %s: %m\n", src);
		goto ERROR;
	}

	// Write file to destination
	r = archive_write_header(writer, entry);
	if (r) {
		ERROR(pakfire, "Could not write %s: %m\n", dst);
		goto ERROR;
	}

	// Copy payload
	if (archive_entry_filetype(entry) == AE_IFREG) {
		r = pakfire_archive_copy_data_from_file(pakfire, writer, f);
		if (r)
			goto ERROR;
	}

ERROR:
	if (f)
		fclose(f);
	if (entry)
		archive_entry_free(entry);
	if (reader)
		archive_read_free(reader);
	if (writer)
		archive_write_free(writer);

	return r;
}

PAKFIRE_EXPORT int pakfire_copy_in(struct pakfire* pakfire, const char* src, const char* dst) {
	char path[PATH_MAX];
	int r;

	if (pakfire_on_root(pakfire)) {
		errno = ENOTSUP;
		return 1;
	}

	r = pakfire_path(pakfire, path, "%s", dst);
	if (r)
		return r;

	return pakfire_copy(pakfire, src, path, PAKFIRE_COPY_IN);
}

PAKFIRE_EXPORT int pakfire_copy_out(struct pakfire* pakfire, const char* src, const char* dst) {
	char path[PATH_MAX];
	int r;

	if (pakfire_on_root(pakfire)) {
		errno = ENOTSUP;
		return 1;
	}

	r = pakfire_path(pakfire, path, "%s", src);
	if (r)
		return r;

	return pakfire_copy(pakfire, path, dst, PAKFIRE_COPY_OUT);
}

PAKFIRE_EXPORT const char* pakfire_get_arch(struct pakfire* pakfire) {
	return pakfire->arch;
}

PAKFIRE_EXPORT int pakfire_version_compare(struct pakfire* pakfire, const char* evr1, const char* evr2) {
	return pool_evrcmp_str(pakfire->pool, evr1, evr2, EVRCMP_COMPARE);
}

Pool* pakfire_get_solv_pool(struct pakfire* pakfire) {
	return pakfire->pool;
}

void pakfire_pool_has_changed(struct pakfire* pakfire) {
	pakfire->pool_ready = 0;
}

static int __pakfire_repo_internalize(struct pakfire* pakfire, struct pakfire_repo* repo,
		void* p) {
	int flags = *(int*)p;

	return pakfire_repo_internalize(repo, flags);
}

void pakfire_pool_internalize(struct pakfire* pakfire) {
	int flags = 0;

	// Nothing to do if the pool is ready
	if (pakfire->pool_ready)
		return;

	// Internalize all repositories
	pakfire_repo_walk(pakfire, __pakfire_repo_internalize, &flags);

	// Create fileprovides
	pool_addfileprovides(pakfire->pool);

	// Create whatprovides index
	pool_createwhatprovides(pakfire->pool);

	// Mark the pool as ready
	pakfire->pool_ready = 1;
}

PAKFIRE_EXPORT struct pakfire_repolist* pakfire_get_repos(struct pakfire* pakfire) {
	struct pakfire_repolist* list;

	int r = pakfire_repolist_create(&list);
	if (r)
		return NULL;

	Pool* pool = pakfire_get_solv_pool(pakfire);
	Repo* solv_repo;
	int i;

	FOR_REPOS(i, solv_repo) {
		// Skip the dummy repository
		if (strcmp(solv_repo->name, PAKFIRE_REPO_DUMMY) == 0)
			continue;

		struct pakfire_repo* repo = pakfire_repo_create_from_repo(pakfire, solv_repo);
		if (!repo) {
			r = 1;
			goto ERROR;
		}

		r = pakfire_repolist_append(list, repo);
		if (r) {
			pakfire_repo_unref(repo);
			goto ERROR;
		}

		pakfire_repo_unref(repo);
	}

	return list;

ERROR:
	pakfire_repolist_unref(list);
	return NULL;
}

PAKFIRE_EXPORT struct pakfire_repo* pakfire_get_repo(struct pakfire* pakfire, const char* name) {
	Pool* pool = pakfire_get_solv_pool(pakfire);
	if (!pool)
		return NULL;

	Repo* repo;
	int i;

	FOR_REPOS(i, repo) {
		if (strcmp(repo->name, name) == 0)
			return pakfire_repo_create_from_repo(pakfire, repo);
	}

	// Nothing found
	return NULL;
}

struct pakfire_repo* pakfire_get_installed_repo(struct pakfire* pakfire) {
	if (!pakfire->pool->installed)
		return NULL;

	return pakfire_repo_create_from_repo(pakfire, pakfire->pool->installed);
}

/*
	Convenience function to add a package to the @commandline repository
*/
int pakfire_commandline_add(struct pakfire* pakfire, const char* path,
		struct pakfire_package** package) {
	struct pakfire_repo* repo = NULL;
	int r;

	// Find the commandline repository
	repo = pakfire_get_repo(pakfire, PAKFIRE_REPO_COMMANDLINE);
	if (!repo) {
		ERROR(pakfire, "Could not find the commandline repository: %m\n");
		return 1;
	}

	// Add the package
	r = pakfire_repo_add(repo, path, package);
	if (r)
		goto ERROR;

ERROR:
	if (repo)
		pakfire_repo_unref(repo);

	return r;
}

static int __pakfire_search(struct pakfire* pakfire, struct pakfire_packagelist* list,
		const Id* keys, const char* what, int flags) {
	Dataiterator di;
	Queue matches;
	int r;

	// Get the pool ready
	pakfire_pool_internalize(pakfire);

	// Initialize the result queue
	queue_init(&matches);

	// Setup the data interator
	dataiterator_init(&di, pakfire->pool, 0, 0, 0, what, flags);

	// Search through these keys and add matches to the queue
	for (const Id* key = keys; *key; key++) {
		dataiterator_set_keyname(&di, *key);
		dataiterator_set_search(&di, 0, 0);

		while (dataiterator_step(&di))
			queue_pushunique(&matches, di.solvid);
	}

	// Import matches into the package list
	r = pakfire_packagelist_import_solvables(list, &matches);
	if (r)
		goto ERROR;

ERROR:
	dataiterator_free(&di);
	queue_free(&matches);

	return r;
}

static int pakfire_search_filelist(struct pakfire* pakfire, const char* what, int flags,
		struct pakfire_packagelist* list) {
	const Id keys[] = {
		SOLVABLE_FILELIST,
		ID_NULL,
	};

	return __pakfire_search(pakfire, list, keys, what, SEARCH_FILES|SEARCH_GLOB);
}

static int pakfire_search_dep(struct pakfire* pakfire, Id type, const char* what, int flags,
		struct pakfire_packagelist* list) {
	int r;

	// Get the pool ready
	pakfire_pool_internalize(pakfire);

	// Translate dependency to ID
	Id dep = pakfire_str2dep(pakfire, what);
	if (!dep) {
		errno = EINVAL;
		return 1;
	}

	Queue matches;
	queue_init(&matches);

	// Search for anything that matches
	pool_whatmatchesdep(pakfire->pool, type, dep, &matches, 0);

	// Add the result to the packagelist
	r = pakfire_packagelist_import_solvables(list, &matches);
	if (r)
		goto ERROR;

ERROR:
	queue_free(&matches);

	return r;
}

PAKFIRE_EXPORT int pakfire_whatprovides(struct pakfire* pakfire, const char* what, int flags,
		struct pakfire_packagelist* list) {
	int r;

	// Check for valid input
	if (!what || !list) {
		errno = EINVAL;
		return 1;
	}

	// Search for all packages that match this dependency
	r = pakfire_search_dep(pakfire, SOLVABLE_PROVIDES, what, flags, list);
	if (r)
		return r;

	// Search the filelist
	if (*what == '/') {
		r = pakfire_search_filelist(pakfire, what, flags, list);
		if (r)
			return r;
	}

	return 0;
}

static int __pakfire_whatrequires(struct pakfire* pakfire, struct pakfire_package* pkg, void* data) {
	struct pakfire_packagelist* list = (struct pakfire_packagelist*)data;

	return pakfire_package_get_reverse_requires(pkg, list);
}

PAKFIRE_EXPORT int pakfire_whatrequires(struct pakfire* pakfire, const char* what, int flags,
		struct pakfire_packagelist* list) {
	struct pakfire_packagelist* packages = NULL;
	int r;

	const Id keys[] = {
		SOLVABLE_NAME,
		ID_NULL,
	};

	// Create a new package list
	r = pakfire_packagelist_create(&packages, pakfire);
	if (r)
		goto ERROR;

	// Find any packages that match the name
	r = __pakfire_search(pakfire, packages, keys, what, SEARCH_STRING);
	if (r)
		goto ERROR;

	// Find everything for all packages
	r = pakfire_packagelist_walk(packages, __pakfire_whatrequires, list);
	if (r)
		goto ERROR;

	// Append any simple dependencies
	r = pakfire_search_dep(pakfire, SOLVABLE_REQUIRES, what, flags, list);
	if (r)
		goto ERROR;

ERROR:
	if (packages)
		pakfire_packagelist_unref(packages);

	return r;
}

PAKFIRE_EXPORT int pakfire_search(struct pakfire* pakfire, const char* what, int flags,
		struct pakfire_packagelist* list) {
	const Id keys[] = {
		SOLVABLE_NAME,
		SOLVABLE_SUMMARY,
		SOLVABLE_DESCRIPTION,
		ID_NULL
	};

	const Id keys_name_only[] = {
		SOLVABLE_NAME,
		ID_NULL
	};

	return __pakfire_search(pakfire,
		list,
		(flags & PAKFIRE_SEARCH_NAME_ONLY) ? keys_name_only : keys,
		what,
		SEARCH_SUBSTRING|SEARCH_NOCASE);
}

// Logging

PAKFIRE_EXPORT int pakfire_log_get_priority(struct pakfire* pakfire) {
	return pakfire->log_priority;
}

PAKFIRE_EXPORT void pakfire_log_set_priority(struct pakfire* pakfire, int priority) {
	pakfire->log_priority = priority;
}

PAKFIRE_EXPORT void pakfire_set_log_callback(struct pakfire* pakfire,
		pakfire_log_callback callback, void* data) {
	pakfire->callbacks.log = callback;
	pakfire->callbacks.log_data = data;
}

void pakfire_log(struct pakfire* pakfire, int priority, const char* file, int line,
		const char* fn, const char* format, ...) {
	va_list args;

	// Do not do anything if callback isn't set
	if (!pakfire->callbacks.log)
		return;

	// Save errno
	int saved_errno = errno;

	va_start(args, format);
	pakfire->callbacks.log(pakfire->callbacks.log_data,
		priority, file, line, fn, format, args);
	va_end(args);

	// Restore errno
	errno = saved_errno;
}

// UI

PAKFIRE_EXPORT void pakfire_set_confirm_callback(struct pakfire* pakfire,
		pakfire_confirm_callback callback, void* data) {
	pakfire->callbacks.confirm = callback;
	pakfire->callbacks.confirm_data = data;
}

int pakfire_confirm(struct pakfire* pakfire, const char* message, const char* question) {
	// Run callback
	if (pakfire->callbacks.confirm)
		return pakfire->callbacks.confirm(
			pakfire, pakfire->callbacks.confirm_data, message, question);

	// If no callback is set, we just log the message
	INFO(pakfire, "%s\n", message);

	return 0;
}

static const char* pakfire_user_lookup(void* data, la_int64_t uid) {
	struct pakfire* pakfire = (struct pakfire*)data;

	DEBUG(pakfire, "Looking up name for UID %ld\n", uid);

	// Unmap the UID first
	uid = pakfire_unmap_id(pakfire, &pakfire->subuid, uid);

	// Fast path for "root"
	if (uid == 0)
		return "root";

	// Find a matching entry in /etc/passwd
	struct passwd* entry = pakfire_getpwuid(pakfire, uid);
	if (!entry) {
		ERROR(pakfire, "Could not retrieve uname for %ld: %m\n", uid);
		return 0;
	}

	DEBUG(pakfire, "Mapping UID %ld to %s\n", uid, entry->pw_name);

	return entry->pw_name;
}

static const char* pakfire_group_lookup(void* data, la_int64_t gid) {
	struct pakfire* pakfire = (struct pakfire*)data;

	DEBUG(pakfire, "Looking up name for GID %ld\n", gid);

	// Unmap the GID first
	gid = pakfire_unmap_id(pakfire, &pakfire->subgid, gid);

	// Fast path for "root"
	if (gid == 0)
		return "root";

	// Find a matching entry in /etc/group
	struct group* entry = pakfire_getgrgid(pakfire, gid);
	if (!entry) {
		ERROR(pakfire, "Could not retrieve gname for %ld: %m\n", gid);
		return 0;
	}

	DEBUG(pakfire, "Mapping GID %ld to %s\n", gid, entry->gr_name);

	return entry->gr_name;
}

struct archive* pakfire_make_archive_disk_reader(struct pakfire* pakfire, int internal) {
	struct archive* archive = archive_read_disk_new();
	if (!archive)
		return NULL;

	// Do not read fflags
	int r = archive_read_disk_set_behavior(archive, ARCHIVE_READDISK_NO_FFLAGS);
	if (r) {
		ERROR(pakfire, "Could not change behavior of reader: %s\n",
			archive_error_string(archive));
		archive_read_free(archive);
		return NULL;
	}

	// Install user/group lookups
	if (internal) {
		archive_read_disk_set_uname_lookup(archive, pakfire, pakfire_user_lookup, NULL);
		archive_read_disk_set_gname_lookup(archive, pakfire, pakfire_group_lookup, NULL);
	} else {
		archive_read_disk_set_standard_lookup(archive);
	}

	return archive;
}

static la_int64_t pakfire_uid_lookup(void* data, const char* name, la_int64_t uid) {
	struct pakfire* pakfire = (struct pakfire*)data;

	DEBUG(pakfire, "Looking up UID for '%s' (%ld)\n", name, uid);

	// Fast path for "root"
	if (strcmp(name, "root") == 0)
		return pakfire_map_id(pakfire, &pakfire->subuid, 0);

	// Find a matching entry in /etc/passwd
	struct passwd* entry = pakfire_getpwnam(pakfire, name);
	if (!entry) {
		ERROR(pakfire, "Could not retrieve UID for '%s': %m\n", name);
		return pakfire_map_id(pakfire, &pakfire->subuid, 0);
	}

	DEBUG(pakfire, "Mapping %s to UID %d\n", name, entry->pw_uid);

	return pakfire_map_id(pakfire, &pakfire->subuid, entry->pw_uid);
}

static la_int64_t pakfire_gid_lookup(void* data, const char* name, la_int64_t gid) {
	struct pakfire* pakfire = (struct pakfire*)data;

	DEBUG(pakfire, "Looking up GID for '%s' (%ld)\n", name, gid);

	// Fast path for "root"
	if (strcmp(name, "root") == 0)
		return pakfire_map_id(pakfire, &pakfire->subgid, 0);

	// Find a matching entry in /etc/group
	struct group* entry = pakfire_getgrnam(pakfire, name);
	if (!entry) {
		ERROR(pakfire, "Could not retrieve GID for '%s': %m\n", name);
		return pakfire_map_id(pakfire, &pakfire->subgid, 0);
	}

	DEBUG(pakfire, "Mapping %s to GID %d\n", name, entry->gr_gid);

	return pakfire_map_id(pakfire, &pakfire->subgid, entry->gr_gid);
}

struct archive* pakfire_make_archive_disk_writer(struct pakfire* pakfire, int internal) {
	struct archive* archive = archive_write_disk_new();
	if (!archive)
		return NULL;

	// Set flags for extracting files
	const int flags =
		ARCHIVE_EXTRACT_ACL |
		ARCHIVE_EXTRACT_OWNER |
		ARCHIVE_EXTRACT_PERM |
		ARCHIVE_EXTRACT_TIME |
		ARCHIVE_EXTRACT_UNLINK |
		ARCHIVE_EXTRACT_XATTR;

	archive_write_disk_set_options(archive, flags);

	// Install our own routine for user/group lookups
	if (internal) {
		archive_write_disk_set_user_lookup(archive, pakfire, pakfire_uid_lookup, NULL);
		archive_write_disk_set_group_lookup(archive, pakfire, pakfire_gid_lookup, NULL);
	} else {
		archive_write_disk_set_standard_lookup(archive);
	}

	return archive;
}

// Convenience functions to install/erase/update packages

static int pakfire_perform_transaction(
		struct pakfire* pakfire,
		int transaction_flags,
		int solver_flags,
		const enum pakfire_request_action action,
		const char** packages,
		const char** locks,
		int job_flags,
		int* changed,
		pakfire_status_callback status_callback,
		void* status_callback_data) {
	struct pakfire_request* request = NULL;
	struct pakfire_transaction* transaction = NULL;
	int r = 1;

	// Packages cannot be NULL
	if (!packages) {
		errno = EINVAL;
		return 1;
	}

	// Acquire lock
	r = pakfire_acquire_lock(pakfire);
	if (r)
		goto ERROR;

	// Create a new request
	r = pakfire_request_create(&request, pakfire, solver_flags);
	if (r)
		goto ERROR;

	// Lock anything that should be locked
	if (locks) {
		for (const char** lock = locks; *lock; lock++) {
			r = pakfire_request_add(request, PAKFIRE_REQ_LOCK, *lock, 0);
			if (r) {
				ERROR(pakfire, "Could not lock '%s': %m\n", *lock);
				goto ERROR;
			}
		}
	}

	// Perform action on all packages
	for (const char** package = packages; *package; package++) {
		r = pakfire_request_add(request, action, *package, job_flags);
		if (r) {
			ERROR(pakfire, "Could not find '%s': %m\n", *package);
			goto ERROR;
		}
	}

	// Solve the request
	r = pakfire_request_solve(request);
	if (r)
		goto ERROR;

	// Fetch the transaction
	r = pakfire_request_get_transaction(request, &transaction);
	if (r)
		goto ERROR;

	// Set how many packages have been changed
	if (changed)
		*changed = pakfire_transaction_count(transaction);

	// Set status callback
	if (status_callback)
		pakfire_transaction_set_status_callback(
			transaction, status_callback, status_callback_data);

	// Run the transaction
	r = pakfire_transaction_run(transaction, transaction_flags);
	if (r)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	// Release lock
	pakfire_release_lock(pakfire);

	if (transaction)
		pakfire_transaction_unref(transaction);
	if (request)
		pakfire_request_unref(request);

	return r;
}

PAKFIRE_EXPORT int pakfire_install(
		struct pakfire* pakfire,
		int transaction_flags,
		int solver_flags,
		const char** packages,
		const char** locks,
		int flags,
		int* changed,
		pakfire_status_callback status_callback,
		void* status_callback_data) {
	return pakfire_perform_transaction(
		pakfire,
		transaction_flags,
		solver_flags,
		PAKFIRE_REQ_INSTALL,
		packages,
		locks,
		flags,
		changed,
		status_callback,
		status_callback_data);
}

PAKFIRE_EXPORT int pakfire_erase(
		struct pakfire* pakfire,
		int transaction_flags,
		int solver_flags,
		const char** packages,
		const char** locks,
		int flags,
		int* changed,
		pakfire_status_callback status_callback,
		void* status_callback_data) {
	return pakfire_perform_transaction(
		pakfire,
		transaction_flags,
		solver_flags,
		PAKFIRE_REQ_ERASE,
		packages,
		locks,
		flags,
		changed,
		status_callback,
		status_callback_data);
}

static int pakfire_perform_transaction_simple(struct pakfire* pakfire, int solver_flags,
		const enum pakfire_request_action action, int job_flags, int* changed,
		pakfire_status_callback status_callback, void* status_callback_data) {
	struct pakfire_request* request = NULL;
	struct pakfire_transaction* transaction = NULL;
	int r = 1;

	// Acquire lock
	r = pakfire_acquire_lock(pakfire);
	if (r)
		goto ERROR;

	// Create a new request
	r = pakfire_request_create(&request, pakfire, solver_flags);
	if (r)
		goto ERROR;

	// Perform action
	r = pakfire_request_add(request, action, NULL, job_flags);
	if (r)
		goto ERROR;

	// Solve the request
	r = pakfire_request_solve(request);
	if (r)
		goto ERROR;

	// Fetch the transaction
	r = pakfire_request_get_transaction(request, &transaction);
	if (r)
		goto ERROR;

	// Set how many packages have been changed
	if (changed)
		*changed = pakfire_transaction_count(transaction);

	// Set status callback
	if (status_callback)
		pakfire_transaction_set_status_callback(
			transaction, status_callback, status_callback_data);

	// Run the transaction
	r = pakfire_transaction_run(transaction, 0);
	if (r)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	// Release lock
	pakfire_release_lock(pakfire);

	if (transaction)
		pakfire_transaction_unref(transaction);
	if (request)
		pakfire_request_unref(request);

	return r;
}

PAKFIRE_EXPORT int pakfire_update(
		struct pakfire* pakfire,
		int transaction_flags,
		int solver_flags,
		const char** packages,
		const char** locks,
		int flags,
		int* changed,
		pakfire_status_callback status_callback,
		void* status_callback_data) {
	// If no packages are being passed, we will try to update everything
	// XXX add locks
	if (!packages)
		return pakfire_perform_transaction_simple(
			pakfire, solver_flags, PAKFIRE_REQ_UPDATE_ALL, flags, changed,
			status_callback, status_callback_data);

	return pakfire_perform_transaction(pakfire, transaction_flags, solver_flags,
		PAKFIRE_REQ_UPDATE, packages, locks, flags, changed,
		status_callback, status_callback_data);
}

static int pakfire_verify(struct pakfire* pakfire, int *changed) {
	return pakfire_perform_transaction_simple(pakfire, 0, PAKFIRE_REQ_VERIFY,
		0, changed, NULL, NULL);
}

static int pakfire_check_files(struct pakfire* pakfire,
		struct pakfire_db* db, struct pakfire_filelist* errors) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	// Fetch the filelist
	r = pakfire_db_filelist(db, &filelist);
	if (r)
		goto ERROR;

	// Verify the filelist
	r = pakfire_filelist_verify(filelist, errors);

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

PAKFIRE_EXPORT int pakfire_check(struct pakfire* pakfire, struct pakfire_filelist* errors) {
	struct pakfire_db* db = NULL;
	int r;

	// Open database in read-only mode and try to load all installed packages
	r = pakfire_db_open(&db, pakfire, PAKFIRE_DB_READWRITE);
	if (r)
		goto ERROR;

	// Perform a database integrity check
	r = pakfire_db_check(db);
	if (r)
		goto ERROR;

	// Check if all dependencies are intact
	r = pakfire_verify(pakfire, NULL);
	if (r)
		goto ERROR;

	// Check files
	r = pakfire_check_files(pakfire, db, errors);
	if (r)
		goto ERROR;

ERROR:
	if (db)
		pakfire_db_unref(db);

	return r;
}

PAKFIRE_EXPORT int pakfire_sync(struct pakfire* pakfire, int solver_flags, int flags,
		int* changed, pakfire_status_callback status_callback, void* status_callback_data) {
	return pakfire_perform_transaction_simple(pakfire, solver_flags,
		PAKFIRE_REQ_SYNC, flags, changed, status_callback, status_callback_data);
}
