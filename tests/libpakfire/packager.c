/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdio.h>

#include <pakfire/package.h>
#include <pakfire/packager.h>
#include <pakfire/repo.h>
#include <pakfire/util.h>

#include "../testsuite.h"

static int test_create(const struct test* t) {
	struct pakfire_packager* packager = NULL;
	struct pakfire_package* pkg = NULL;
	struct pakfire_repo* repo = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_repo_create(&repo, t->pakfire, "test"));
	ASSERT(repo);

	ASSERT_SUCCESS(pakfire_package_create(&pkg, t->pakfire, repo, "test", "1.0-1", "src"));

	// Create packager
	ASSERT_SUCCESS(pakfire_packager_create(&packager, t->pakfire, pkg));

	// Add a file to the package
	const char* path = TEST_SRC_PATH "data/beep-1.3-2.ip3.x86_64.pfm";

	ASSERT_SUCCESS(pakfire_packager_add(packager, path, NULL));

	// Write archive
	FILE* f = test_mktemp(NULL);
	r = pakfire_packager_finish(packager, f);
	ASSERT(r == 0);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (packager)
		pakfire_packager_unref(packager);
	if (pkg)
		pakfire_package_unref(pkg);
	if (repo)
		pakfire_repo_unref(repo);

	return r;
}

static int test_compare_metadata(const struct test* t) {
	struct pakfire_archive* archive = NULL;
	struct pakfire_package* pkg1 = NULL;
	struct pakfire_package* pkg2 = NULL;
	struct pakfire_packager* packager = NULL;
	char* path = NULL;
	const char* s = NULL;
	int r = EXIT_FAILURE;

	// Fetch the dummy repository
	struct pakfire_repo* repo = pakfire_get_repo(t->pakfire, PAKFIRE_REPO_DUMMY);
	ASSERT(repo);

	ASSERT_SUCCESS(pakfire_package_create(&pkg1, t->pakfire, repo,
		"test", "1.0-1", "x86_64"));

	// Set all metadata
	pakfire_package_set_string(pkg1, PAKFIRE_PKG_SUMMARY, "SUMMARY");
	pakfire_package_set_string(pkg1, PAKFIRE_PKG_DESCRIPTION, "DESCRIPTION");

	// Create packager
	ASSERT_SUCCESS(pakfire_packager_create(&packager, t->pakfire, pkg1));

	// Write archive
	FILE* f = test_mktemp(&path);
	ASSERT(f);

	ASSERT_SUCCESS(pakfire_packager_finish(packager, f));
	ASSERT_SUCCESS(fclose(f));

	printf("Archive written to %s\n", path);

	// Try to open the archive
	ASSERT_SUCCESS(pakfire_archive_open(&archive, t->pakfire, path));

	// Parse package from archive
	ASSERT_SUCCESS(pakfire_archive_make_package(archive, repo, &pkg2));

	// Dump everything for debugging
	char* dump = pakfire_package_dump(pkg2, PAKFIRE_PKG_DUMP_LONG|PAKFIRE_PKG_DUMP_FILELIST);
	if (dump) {
		printf("%s\n", dump);
		free(dump);
	}

	// Compare name
	s = pakfire_package_get_string(pkg2, PAKFIRE_PKG_NAME);
	ASSERT_STRING_EQUALS(s, "test");

	// Compare evr
	s = pakfire_package_get_string(pkg2, PAKFIRE_PKG_EVR);
	ASSERT_STRING_EQUALS(s, "1.0-1");

	// Compare arch
	s = pakfire_package_get_string(pkg2, PAKFIRE_PKG_ARCH);
	ASSERT_STRING_EQUALS(s, "x86_64");

	// Compare summary
	s = pakfire_package_get_string(pkg2, PAKFIRE_PKG_SUMMARY);
	ASSERT_STRING_EQUALS(s, "SUMMARY");

	// Compare description
	s = pakfire_package_get_string(pkg2, PAKFIRE_PKG_DESCRIPTION);
	ASSERT_STRING_EQUALS(s, "DESCRIPTION");

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (path)
		free(path);
	if (archive)
		pakfire_archive_unref(archive);
	if (packager)
		pakfire_packager_unref(packager);
	if (pkg1)
		pakfire_package_unref(pkg1);
	if (pkg2)
		pakfire_package_unref(pkg2);
	if (repo)
		pakfire_repo_unref(repo);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create);
	testsuite_add_test(test_compare_metadata);

	return testsuite_run(argc, argv);
}
