/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <unistd.h>
#include <utime.h>

#include <curl/curl.h>
#include <json.h>
#include <openssl/err.h>
#include <openssl/evp.h>

#include <pakfire/digest.h>
#include <pakfire/downloader.h>
#include <pakfire/i18n.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/progressbar.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

// Retry a mirror up to N times before marking it as broken
#define MIRROR_RETRIES			3

// The number of concurrent downloads
#define MAX_PARALLEL			4

struct pakfire_mirror {
	STAILQ_ENTRY(pakfire_mirror) nodes;

	char url[PATH_MAX];

	unsigned int retries_left;
	int broken;
};

struct pakfire_transfer {
	struct pakfire_downloader* downloader;

	TAILQ_ENTRY(pakfire_transfer) nodes;
	CURL* handle;

	char title[NAME_MAX];
	char url[PATH_MAX];
	char path[PATH_MAX];
	enum pakfire_transfer_flags flags;
	int tries;

	// Temporary file
	char tempfile[PATH_MAX];
	FILE* f;

	// Crypto Stuff
	EVP_MD_CTX* evp;
	const EVP_MD* md;
	unsigned char computed_digest[EVP_MAX_MD_SIZE];
	unsigned int computed_digest_length;
	unsigned char expected_digest[EVP_MAX_MD_SIZE];
	unsigned int expected_digest_length;

	// Mirrors
	char baseurl[PATH_MAX];
	struct pakfire_mirrorlist* mirrors;
	struct pakfire_mirror* mirror;
};

struct pakfire_downloader {
	struct pakfire* pakfire;
	int nrefs;

	struct pakfire_progressbar* progressbar;
	unsigned int parallel;

	// cURL multi handle
	CURLM* curl;
	TAILQ_HEAD(transfers, pakfire_transfer) transfers;
};

static char* pakfire_url_join(const char* part1, const char* part2) {
	char* url = NULL;

	int r = asprintf(&url, "%s/%s", part1, part2);
	if (r < 0)
		return NULL;

	return url;
}

static int pakfire_downloader_setup_curl(struct pakfire_downloader* downloader) {
	// Create a new multi handle
	downloader->curl = curl_multi_init();
	if (!downloader->curl) {
		ERROR(downloader->pakfire, "Could not create cURL multi handle\n");
		return 1;
	}

	// Limit the amount of parallel downloads
	curl_multi_setopt(downloader->curl, CURLMOPT_MAXCONNECTS, (long)MAX_PARALLEL);

	return 0;
}

static void pakfire_transfer_free(struct pakfire_transfer* transfer) {
	if (transfer->handle)
		curl_easy_cleanup(transfer->handle);

	// Unlink the temporary file
	if (*transfer->tempfile)
		unlink(transfer->tempfile);

	// Close temporary file
	if (transfer->f)
		fclose(transfer->f);

	// Free OpenSSL EVP context
	if (transfer->evp)
		EVP_MD_CTX_free(transfer->evp);

	if (transfer->mirrors)
		pakfire_mirrorlist_unref(transfer->mirrors);

	free(transfer);
}

static void pakfire_downloader_free(struct pakfire_downloader* downloader) {
	// Free any unprocessed transfers
	struct pakfire_transfer* transfer;
	while (!TAILQ_EMPTY(&downloader->transfers)) {
		transfer = TAILQ_LAST(&downloader->transfers, transfers);
		TAILQ_REMOVE(&downloader->transfers, transfer, nodes);
		pakfire_transfer_free(transfer);
	}

	if (downloader->progressbar)
		pakfire_progressbar_unref(downloader->progressbar);

	if (downloader->curl)
		curl_multi_cleanup(downloader->curl);

	pakfire_unref(downloader->pakfire);
	free(downloader);
}

int pakfire_downloader_create(struct pakfire_downloader** downloader, struct pakfire* pakfire) {
	// Fail if pakfire is running in offline mode
	if (pakfire_has_flag(pakfire, PAKFIRE_FLAGS_OFFLINE)) {
		ERROR(pakfire, "Cannot initialize downloader in offline mode\n");
		errno = ENOTSUP;
		return 1;
	}

	struct pakfire_downloader* d = calloc(1, sizeof(*d));
	if (!d)
		return ENOMEM;

	// Store reference to Pakfire
	d->pakfire = pakfire_ref(pakfire);

	// Initialize reference counting
	d->nrefs = 1;

	// Set parallelism
	d->parallel = MAX_PARALLEL;

	// Init transfers queue
	TAILQ_INIT(&d->transfers);

	// Setup cURL
	int r = pakfire_downloader_setup_curl(d);
	if (r)
		goto ERROR;

	// Create the progressbar
	r = pakfire_progressbar_create(&d->progressbar, NULL);
	if (r)
		goto ERROR;

	*downloader = d;

	return 0;

ERROR:
	pakfire_downloader_free(d);

	return r;
}

struct pakfire_downloader* pakfire_downloader_ref(struct pakfire_downloader* downloader) {
	++downloader->nrefs;

	return downloader;
}

struct pakfire_downloader* pakfire_downloader_unref(struct pakfire_downloader* downloader) {
	if (--downloader->nrefs > 0)
		return downloader;

	pakfire_downloader_free(downloader);
	return NULL;
}

#ifdef ENABLE_DEBUG
static int debug_callback(CURL *handle, curl_infotype type,
		char* data, size_t size, void* private) {
	struct pakfire* pakfire = (struct pakfire*)private;

	switch (type) {
		case CURLINFO_TEXT:
			DEBUG(pakfire, "cURL: %s", data);
			break;

		// Log headers
		case CURLINFO_HEADER_IN:
			DEBUG(pakfire, "cURL: < %s", data);
			break;

		case CURLINFO_HEADER_OUT:
			DEBUG(pakfire, "cURL: > %s", data);
			break;

		// Ignore everything else
		default:
			break;
	}

	return 0;
}
#endif

static struct pakfire_transfer* pakfire_downloader_create_transfer(
		struct pakfire_downloader* downloader,
		const char* baseurl,
		struct pakfire_mirrorlist* mirrors,
		const char* title,
		const char* url,
		const char* path,
		enum pakfire_digest_types md,
		const unsigned char* expected_digest,
		const size_t expected_digest_length,
		enum pakfire_transfer_flags flags) {
	struct pakfire_config* config = NULL;

	DEBUG(downloader->pakfire, "Adding download of %s\n", url);

	// Reset baseurl it points to an empty string
	if (baseurl && !*baseurl)
		baseurl = NULL;

	// Do not allow relative URLs when no mirrors are set
	if (!pakfire_string_is_url(url) && !(mirrors || baseurl)) {
		ERROR(downloader->pakfire, "Relative URLs cannot be used without a baseurl and/or mirrorlist\n");
		errno = EINVAL;
		return NULL;
	}

	// Check if expected digest is set
	if (md) {
		if (!expected_digest || !expected_digest_length) {
			ERROR(downloader->pakfire, "Expected message digest or size is not set\n");
			errno = EINVAL;
			return NULL;
		}
	}

	// Expected digest length cannot be too long
	if (expected_digest_length > EVP_MAX_MD_SIZE) {
		ERROR(downloader->pakfire, "Digest length is too long\n");
		errno = EMSGSIZE;
		return NULL;
	}

	struct pakfire_transfer* transfer = calloc(1, sizeof(*transfer));
	if (!transfer)
		return NULL;

	// Keep a reference to downloader (without increasing the refcounter to avoid
	// circular connections)
	transfer->downloader = downloader;

	// Copy title
	if (title) {
		pakfire_string_set(transfer->title, title);

	// Or use the filename
	} else {
		const char* filename = pakfire_basename(url);
		if (filename)
			pakfire_string_set(transfer->title, filename);
	}

	// Copy URL
	pakfire_string_set(transfer->url, url);

	// Copy path
	pakfire_string_set(transfer->path, path);

	// Open the desination file if we do not want a temporary file
	if (flags & PAKFIRE_TRANSFER_NOTEMP) {
		transfer->f = fopen(path, "w+");
		if (!transfer->f)
			goto ERROR;
	}

	// Copy baseurl
	if (baseurl)
		pakfire_string_set(transfer->baseurl, baseurl);

	// Keep a reference to the mirrorlist
	if (mirrors)
		transfer->mirrors = pakfire_mirrorlist_ref(mirrors);

	// Copy flags
	transfer->flags = flags;

	// Set message digest
	switch (md) {
		case PAKFIRE_DIGEST_SHA3_512:
			transfer->md = EVP_sha3_512();
			break;

		case PAKFIRE_DIGEST_SHA3_256:
			transfer->md = EVP_sha3_256();
			break;

		case PAKFIRE_DIGEST_BLAKE2B512:
			transfer->md = EVP_blake2b512();
			break;

		case PAKFIRE_DIGEST_BLAKE2S256:
			transfer->md = EVP_blake2s256();
			break;

		case PAKFIRE_DIGEST_SHA2_512:
			transfer->md = EVP_sha512();
			break;

		case PAKFIRE_DIGEST_SHA2_256:
			transfer->md = EVP_sha256();
			break;

		case PAKFIRE_DIGEST_UNDEFINED:
			break;
	}

	// Copy the expected digest
	if (transfer->md && expected_digest) {
		memcpy(transfer->expected_digest, expected_digest, expected_digest_length);
		transfer->expected_digest_length = expected_digest_length;
	}

	// Allocate handle
	transfer->handle = curl_easy_init();
	if (!transfer->handle)
		goto ERROR;

	// Fetch global configuration
	config = pakfire_get_config(downloader->pakfire);

	// Set global configuration
	if (config) {
		const char* proxy = pakfire_config_get(config, "general", "proxy", NULL);
		if (proxy) {
			curl_easy_setopt(transfer->handle, CURLOPT_PROXY, proxy);
		}

		pakfire_config_unref(config);
	}

	// Be a good net citizen and set a user agent
	curl_easy_setopt(transfer->handle, CURLOPT_USERAGENT, PACKAGE_NAME "/" PACKAGE_VERSION);

	// Enable logging/debugging
	curl_easy_setopt(transfer->handle, CURLOPT_VERBOSE, 0);

#ifdef ENABLE_DEBUG
	curl_easy_setopt(transfer->handle, CURLOPT_DEBUGFUNCTION, debug_callback);
	curl_easy_setopt(transfer->handle, CURLOPT_DEBUGDATA, downloader->pakfire);
#endif

	// Limit protocols to HTTPS, HTTP, FTP and FILE
	curl_easy_setopt(transfer->handle, CURLOPT_PROTOCOLS,
		CURLPROTO_HTTPS|CURLPROTO_HTTP|CURLPROTO_FTP|CURLPROTO_FILE);

	// Reference back to this transfer
	curl_easy_setopt(transfer->handle, CURLOPT_PRIVATE, transfer);

	// Follow any redirects
	curl_easy_setopt(transfer->handle, CURLOPT_FOLLOWLOCATION, 1);

	// Success
	return transfer;

ERROR:
	pakfire_transfer_free(transfer);

	return NULL;
}

int pakfire_downloader_add_transfer(
		struct pakfire_downloader* downloader,
		const char* baseurl,
		struct pakfire_mirrorlist* mirrors,
		const char* title,
		const char* url,
		const char* path,
		enum pakfire_digest_types md,
		const unsigned char* expected_digest,
		const size_t expected_digest_length,
		enum pakfire_transfer_flags flags) {
	struct pakfire_transfer* transfer = pakfire_downloader_create_transfer(
		downloader, baseurl, mirrors, title, url, path, md, expected_digest, expected_digest_length, flags);
	if (!transfer)
		return 1;

	// Push this transfer onto the queue
	TAILQ_INSERT_HEAD(&downloader->transfers, transfer, nodes);

	return 0;
}

static int pakfire_transfer_select_mirror(struct pakfire_downloader* downloader,
		struct pakfire_transfer* transfer) {
	// Choose the next mirror
	if (transfer->mirror)
		transfer->mirror = STAILQ_NEXT(transfer->mirror, nodes);

	// If no mirror has been selected yet, choose the first one
	else
		transfer->mirror = pakfire_mirrorlist_first(transfer->mirrors);

	// Skip this mirror if it is broken
	while (transfer->mirror && transfer->mirror->broken) {
		// Move on to the next mirror
		transfer->mirror = STAILQ_NEXT(transfer->mirror, nodes);
	}

	// No mirror found
	if (!transfer->mirror) {
		ERROR(downloader->pakfire, "No mirrors left to try\n");

		// No mirrors left
		return ENOENT;
	}

	DEBUG(downloader->pakfire, "Selected mirror %s\n", transfer->mirror->url);

	return 0;
}

static const char* curl_http_version(long v) {
	switch (v) {
#ifdef CURL_HTTP_VERSION_3_0
		case CURL_HTTP_VERSION_3_0:
			return "HTTP/3.0";
#endif

		case CURL_HTTP_VERSION_2_0:
			return "HTTP/2.0";

		case CURL_HTTP_VERSION_1_1:
			return "HTTP/1.1";

		case CURL_HTTP_VERSION_1_0:
			return "HTTP/1.0";
	}

	return "unknown";
}

static int pakfire_transfer_save(struct pakfire_downloader* downloader,
		struct pakfire_transfer* transfer) {
	struct utimbuf times;
	int r;

	DEBUG(downloader->pakfire,
		"Download successful. Storing result in %s\n", transfer->path);

	if (!(transfer->flags & PAKFIRE_TRANSFER_NOTEMP)) {
		// Remove destination (if it exists)
		unlink(transfer->path);

		// Move the temporary file to its destination
		r = link(transfer->tempfile, transfer->path);
		if (r) {
			ERROR(downloader->pakfire, "Could not link destination file %s: %m\n",
				transfer->path);
			return r;
		}
	}

	// Filetime
	curl_easy_getinfo(transfer->handle, CURLINFO_FILETIME_T, &times.modtime);
	r = utime(transfer->path, &times);
	if (r)
		ERROR(downloader->pakfire, "Could not set mtime of %s: %m\n", transfer->path);

	return 0;
}

static int pakfire_transfer_fail(struct pakfire_downloader* downloader,
		struct pakfire_transfer* transfer, int code) {
	int r;

	DEBUG(downloader->pakfire, "Transfer failed\n");

	// Get file descriptor
	int fd = fileno(transfer->f);

	// Truncate downloaded data
	r = ftruncate(fd, 0);
	if (r)
		return r;

	// Did we use a mirror?
	if (transfer->mirror) {
		// Decrease retries and potentially mark mirror as broken
		if (!transfer->mirror->retries_left--) {
			DEBUG(downloader->pakfire, "Mark mirror %s as broken\n",
				transfer->mirror->url);
			transfer->mirror->broken = 1;
		}

		// Try again with another mirror
		return EAGAIN;
	}

	return 0;
}

static int pakfire_transfer_done(struct pakfire_downloader* downloader,
		struct pakfire_transfer* transfer, int code) {
	CURL* h = transfer->handle;

	int r;
	long protocol;
	const char* url;
	long response_code;
	long http_version;
	double total_time;
	curl_off_t download_size;
	curl_off_t speed;

	DEBUG(downloader->pakfire, "cURL transfer done: %d - %s\n",
		code, curl_easy_strerror(code));

	// Finish message digest computation
	if (transfer->evp) {
		r = EVP_DigestFinal_ex(transfer->evp, transfer->computed_digest, &transfer->computed_digest_length);
		if (r != 1) {
			ERROR(downloader->pakfire, "Could not finish message digest computation: %s\n",
				ERR_error_string(ERR_get_error(), NULL));
			return 1;
		}
	}

	// Protocol
	curl_easy_getinfo(h, CURLINFO_PROTOCOL, &protocol);

	// Effective URL
	curl_easy_getinfo(h, CURLINFO_EFFECTIVE_URL, &url);
	if (url)
		DEBUG(downloader->pakfire, "  Effective URL: %s\n", url);

	// Response code
	curl_easy_getinfo(h, CURLINFO_RESPONSE_CODE, &response_code);
	if (response_code)
		DEBUG(downloader->pakfire, "  Response code: %ld\n", response_code);

	// HTTP Version
	curl_easy_getinfo(h, CURLINFO_HTTP_VERSION, &http_version);
	if (http_version)
		DEBUG(downloader->pakfire, "  HTTP Version: %s\n", curl_http_version(http_version));

	// Total Times
	curl_easy_getinfo(h, CURLINFO_TOTAL_TIME, &total_time);
	DEBUG(downloader->pakfire, "  Total Time: %.2fs\n", total_time);

	// Download Size
	curl_easy_getinfo(h, CURLINFO_SIZE_DOWNLOAD_T, &download_size);
	if (download_size)
		DEBUG(downloader->pakfire, "  Download Size: %ld bytes\n", download_size);

	// Download Speed
	curl_easy_getinfo(h, CURLINFO_SPEED_DOWNLOAD_T, &speed);
	if (speed)
		DEBUG(downloader->pakfire, "  Download Speed: %ld bps\n", speed);

	// Message Digest
	char* hexdigest = __pakfire_hexlify(transfer->computed_digest, transfer->computed_digest_length);
	if (hexdigest) {
		DEBUG(downloader->pakfire, "  Message Digest: %s\n", hexdigest);
		free(hexdigest);
	}

	// Check if digests match
	if (transfer->evp) {
		r = CRYPTO_memcmp(transfer->computed_digest, transfer->expected_digest,
			transfer->computed_digest_length);

		// If they don't match, log an error and try again
		if (r) {
			char* computed_hexdigest = __pakfire_hexlify(transfer->computed_digest,
				transfer->computed_digest_length);
			char* expected_hexdigest = __pakfire_hexlify(transfer->expected_digest,
				transfer->expected_digest_length);

			ERROR(downloader->pakfire, "Download checksum for %s didn't match:\n", transfer->url);
			ERROR(downloader->pakfire, "  Expected: %s\n", expected_hexdigest);
			ERROR(downloader->pakfire, "  Computed: %s\n", computed_hexdigest);

			if (computed_hexdigest)
				free(computed_hexdigest);
			if (expected_hexdigest)
				free(expected_hexdigest);

			// Make this download fail
			r = pakfire_transfer_fail(downloader, transfer, 0);
			if (r)
				return r;

			return 1;
		}
	}

	switch (protocol) {
		case CURLPROTO_FILE:
			// Handle any errors
			if (code)
				r = pakfire_transfer_fail(downloader, transfer, code);
			else
				r = pakfire_transfer_save(downloader, transfer);

			return r;

		case CURLPROTO_HTTPS:
		case CURLPROTO_HTTP:
			switch (response_code) {
				// 200 - OK
				case 200:
					r = pakfire_transfer_save(downloader, transfer);
					if (r)
						return r;
					break;

				// Treat all other response codes as an error
				default:
					r = pakfire_transfer_fail(downloader, transfer, code);
					if (r)
						return r;

					// Error
					return 1;
			}
			break;

		case CURLPROTO_FTP:
			if (response_code == 226)
				r = pakfire_transfer_save(downloader, transfer);
			else
				r = pakfire_transfer_fail(downloader, transfer, code);

			return r;
	}

	// Success
	return 0;
}

static size_t pakfire_downloader_write(char* data, size_t size, size_t nmemb, void* userdata) {
	struct pakfire_transfer* transfer = (struct pakfire_transfer*)userdata;
	int r;

	// Do not write empty blocks
	if (!nmemb)
		return nmemb;

	// Update message digest
	if (transfer->evp) {
		r = EVP_DigestUpdate(transfer->evp, data, nmemb);
		if (r != 1) {
			ERROR(transfer->downloader->pakfire, "EVP_DigestUpdate failed: %s\n",
				ERR_error_string(ERR_get_error(), NULL));
			return 0;
		}
	}

	// Write everything to the allocated file descriptor
	return fwrite(data, size, nmemb, transfer->f);
}

static int pakfire_downloader_prepare_transfer(struct pakfire_downloader* downloader,
		struct pakfire_transfer* transfer) {
	int r;

	// Increment tries
	transfer->tries++;

	// Simply set absolute URLs
	if (pakfire_string_is_url(transfer->url)) {
		curl_easy_setopt(transfer->handle, CURLOPT_URL, transfer->url);

	// Join path if we are using mirrors
	} else if (transfer->mirrors && !pakfire_mirrorlist_empty(transfer->mirrors)) {
		r = pakfire_transfer_select_mirror(downloader, transfer);
		if (r)
			return r;

		char* url = pakfire_url_join(transfer->mirror->url, transfer->url);
		if (!url) {
			ERROR(downloader->pakfire, "Error composing download URL: %m\n");
			return 1;
		}

		curl_easy_setopt(transfer->handle, CURLOPT_URL, url);
		free(url);

	// Use baseurl
	} else if (*transfer->baseurl) {
		char* url = pakfire_url_join(transfer->baseurl, transfer->url);
		if (!url) {
			ERROR(downloader->pakfire, "Error composing download URL: %m\n");
			return 1;
		}

		curl_easy_setopt(transfer->handle, CURLOPT_URL, url);
		free(url);

	// Cannot compose URL
	} else {
		ERROR(downloader->pakfire, "Could not set URL\n");
		return 1;
	}

	// Open a temporary file to write the output to
	if (!transfer->f) {
		// Make path for the temporary file (must be on the same file system)
		r = pakfire_string_format(transfer->tempfile, "%s.XXXXXX", transfer->path);
		if (r)
			return 1;

		transfer->f = pakfire_mktemp(transfer->tempfile, 0);
		if (!transfer->f) {
			ERROR(downloader->pakfire, "Could not create temporary file for download %s: %m\n",
				transfer->tempfile);
			return 1;
		}
	}

	// Drop any previously used EVP contexts
	if (transfer->evp) {
		EVP_MD_CTX_free(transfer->evp);
		transfer->evp = NULL;
	}

	// Create a new EVP context
	if (transfer->md) {
		transfer->evp = EVP_MD_CTX_new();
		if (!transfer->evp) {
			ERROR(downloader->pakfire, "Could not create EVP context: %m\n");
			return 1;
		}

		// Initialize the EVP context
		r = EVP_DigestInit_ex(transfer->evp, transfer->md, NULL);
		if (r != 1) {
			ERROR(downloader->pakfire, "Could not initialize EVP context: %s\n",
				ERR_error_string(ERR_get_error(), NULL));
			return 1;
		}
	}

	// Write all data to the callback function
	curl_easy_setopt(transfer->handle, CURLOPT_WRITEFUNCTION, pakfire_downloader_write);
	curl_easy_setopt(transfer->handle, CURLOPT_WRITEDATA, transfer);

	return 0;
}

static int pakfire_downloader_update_single_progressbar(void* data,
		curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow) {
	struct pakfire_downloader* downloader = (struct pakfire_downloader*)data;

	// Set maximum (because this might have changed)
	pakfire_progressbar_set_max(downloader->progressbar, dltotal);

	// Update current value
	return pakfire_progressbar_update(downloader->progressbar, dlnow);
}

static int pakfire_downloader_make_single_progressbar(
		struct pakfire_downloader* downloader, struct pakfire_transfer* transfer) {
	int r;

	// Reset the progressbar
	pakfire_progressbar_reset(downloader->progressbar);

	// Add title
	if (*transfer->title) {
		r = pakfire_progressbar_add_string(downloader->progressbar, "%s", transfer->title);
		if (r)
			return r;
	}

	// Add percentage
	r = pakfire_progressbar_add_percentage(downloader->progressbar);
	if (r)
		return r;

	// Add the bar
	r = pakfire_progressbar_add_bar(downloader->progressbar);
	if (r)
		return r;

	// Add transfer speed
	r = pakfire_progressbar_add_transfer_speed(downloader->progressbar);
	if (r)
		return r;

	// Add delimiter
	r = pakfire_progressbar_add_string(downloader->progressbar, "|");
	if (r)
		return r;

	// Add bytes transferred
	r = pakfire_progressbar_add_bytes_transferred(downloader->progressbar);
	if (r)
		return r;

	// Add ETA
	r = pakfire_progressbar_add_eta(downloader->progressbar);
	if (r)
		return r;

	// Update the progressbar
	curl_easy_setopt(transfer->handle, CURLOPT_XFERINFOFUNCTION,
		pakfire_downloader_update_single_progressbar);
	curl_easy_setopt(transfer->handle, CURLOPT_XFERINFODATA, downloader);
	curl_easy_setopt(transfer->handle, CURLOPT_NOPROGRESS, 0L);

	// Start the progressbar
	r = pakfire_progressbar_start(downloader->progressbar, 0);
	if (r)
		return r;

	return 0;
}

/*
	This function performs one single transfer in blocking mode.
*/
static int pakfire_downloader_perform(
		struct pakfire_downloader* downloader, struct pakfire_transfer* transfer) {
	const int has_progress = !(transfer->flags & PAKFIRE_TRANSFER_NOPROGRESS);
	int r;

	// Create progressbar
	if (has_progress) {
		r = pakfire_downloader_make_single_progressbar(downloader, transfer);
		if (r)
			return r;
	}

AGAIN:
	// Prepare the transfer
	r = pakfire_downloader_prepare_transfer(downloader, transfer);
	if (r)
		return r;

	// Perform the download
	r = curl_easy_perform(transfer->handle);

	// Handle result
	r = pakfire_transfer_done(downloader, transfer, r);

	// Try again?
	if (r == EAGAIN)
		goto AGAIN;

	// Finish the progressbar
	if (has_progress)
		pakfire_progressbar_finish(downloader->progressbar);

	return r;
}

int pakfire_downloader_retrieve(
		struct pakfire_downloader* downloader,
		const char* baseurl,
		struct pakfire_mirrorlist* mirrors,
		const char* title,
		const char* url,
		const char* path,
		enum pakfire_digest_types md,
		const unsigned char* expected_digest,
		const size_t expected_digest_length,
		enum pakfire_transfer_flags flags) {
	struct pakfire_transfer* transfer = pakfire_downloader_create_transfer(
		downloader, baseurl, mirrors, title, url, path, md, expected_digest, expected_digest_length, flags);
	if (!transfer)
		return 1;

	// Perform the download
	int r = pakfire_downloader_perform(downloader, transfer);
	pakfire_transfer_free(transfer);

	return r;
}

int pakfire_downloader_run(struct pakfire_downloader* downloader) {
	struct pakfire_transfer* transfer;
	unsigned int transfers = 0;

	int r;
	int still_running;
	int msgs_left = -1;
	int success = 0;

	do {
		// Make sure that we have up to parallel transfers active
		while (!TAILQ_EMPTY(&downloader->transfers) && transfers < downloader->parallel) {
			// Fetch first item added
			transfer = TAILQ_LAST(&downloader->transfers, transfers);
			TAILQ_REMOVE(&downloader->transfers, transfer, nodes);

			// Prepare the transfer
			r = pakfire_downloader_prepare_transfer(downloader, transfer);
			if (r) {
				pakfire_transfer_free(transfer);
				return r;
			}

			// Add the handle to cURL
			r = curl_multi_add_handle(downloader->curl, transfer->handle);
			if (r) {
				ERROR(downloader->pakfire, "Adding handle failed\n");
				pakfire_transfer_free(transfer);
				return r;
			}

			transfers++;
		}

		curl_multi_perform(downloader->curl, &still_running);

		CURLMsg* msg;
		while ((msg = curl_multi_info_read(downloader->curl, &msgs_left))) {
			if (msg->msg == CURLMSG_DONE) {
				transfers--;

				// Update reference to transfer
				curl_easy_getinfo(msg->easy_handle, CURLINFO_PRIVATE, &transfer);
				curl_multi_remove_handle(downloader->curl, transfer->handle);

				// Handle whatever comes after the transfer
				r = pakfire_transfer_done(downloader, transfer, msg->data.result);
				if (r) {
					// We will try another mirror and therefore insert this
					// transfer to be picked up again next
					if (r == EAGAIN) {
						TAILQ_INSERT_TAIL(&downloader->transfers, transfer, nodes);
						continue;
					}

					success = r;
				}

				// Destroy transfer
				pakfire_transfer_free(transfer);
			// Log any other messages
			} else {
				ERROR(downloader->pakfire, "cURL reported error %d\n", msg->msg);
			}
		}

		// Wait a little before going through the loop again
		if (still_running)
			curl_multi_wait(downloader->curl, NULL, 0, 100, NULL);
	} while (still_running || !TAILQ_EMPTY(&downloader->transfers));

	// Success
	return success;
}

struct pakfire_mirrorlist {
	struct pakfire* pakfire;
	int nrefs;

	STAILQ_HEAD(mirrors, pakfire_mirror) mirrors;
};

static void pakfire_mirrorlist_clear(struct pakfire_mirrorlist* ml) {
	while (!STAILQ_EMPTY(&ml->mirrors)) {
		struct pakfire_mirror* mirror = STAILQ_FIRST(&ml->mirrors);
		STAILQ_REMOVE_HEAD(&ml->mirrors, nodes);
		free(mirror);
	}
}

static void pakfire_mirrorlist_free(struct pakfire_mirrorlist* ml) {
	pakfire_mirrorlist_clear(ml);

	pakfire_unref(ml->pakfire);
	free(ml);
}

int pakfire_mirrorlist_create(struct pakfire_mirrorlist** mirrorlist, struct pakfire* pakfire) {
	struct pakfire_mirrorlist* ml = calloc(1, sizeof(*ml));
	if (!ml)
		return ENOMEM;

	ml->pakfire = pakfire_ref(pakfire);
	ml->nrefs = 1;

	// Init mirrors
	STAILQ_INIT(&ml->mirrors);

	*mirrorlist = ml;

	return 0;
}

struct pakfire_mirrorlist* pakfire_mirrorlist_ref(struct pakfire_mirrorlist* ml) {
	++ml->nrefs;

	return ml;
}

struct pakfire_mirrorlist* pakfire_mirrorlist_unref(struct pakfire_mirrorlist* ml) {
	if (--ml->nrefs > 0)
		return ml;

	pakfire_mirrorlist_free(ml);

	return NULL;
}

static int pakfire_mirrorlist_check_mirrorlist(struct pakfire_mirrorlist* ml,
		struct json_object* root) {
	struct json_object* typeobj = NULL;
	int r = 1;

	r = json_object_object_get_ex(root, "type", &typeobj);
	if (!r) {
		ERROR(ml->pakfire, "mirrorlist does not have a 'type' attribute\n");
		goto ERROR;
	}

	const char* type = json_object_get_string(typeobj);
	if (!type) {
		ERROR(ml->pakfire, "mirrorlist has an empty or unknown 'type' attribute\n");
		goto ERROR;
	}

	if (strcmp(type, "mirrorlist") != 0) {
		ERROR(ml->pakfire, "Unexpected type: %s\n", type);
		goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (typeobj)
		json_object_put(typeobj);

	return r;
}

int pakfire_mirrorlist_read(struct pakfire_mirrorlist* ml, const char* path) {
	if (!path || !*path) {
		errno = EINVAL;
		return 1;
	}

	DEBUG(ml->pakfire, "Reading mirrorlist from %s\n", path);

	struct json_object* json = pakfire_json_parse_from_file(ml->pakfire, path);
	if (!json) {
		// Ignore if path does not exist
		if (errno == ENOENT)
			return 0;

		ERROR(ml->pakfire, "Could not parse mirrorlist from %s: %m\n", path);
		return 1;
	}

	struct json_object* mirrors = NULL;

	// Check if we found a valid mirrorlist
	int r = pakfire_mirrorlist_check_mirrorlist(ml, json);
	if (r)
		goto ERROR;

	// Clear all existing mirrors
	pakfire_mirrorlist_clear(ml);

	// Add the new mirrors
	r = json_object_object_get_ex(json, "mirrors", &mirrors);
	if (!r) {
		DEBUG(ml->pakfire, "Mirrorlist has no mirrors\n");
		r = 0;
		goto ERROR;
	}

	size_t num_mirrors = json_object_array_length(mirrors);

	for (unsigned int i = 0; i < num_mirrors; i++) {
		struct json_object* mirror = json_object_array_get_idx(mirrors, i);
		if (!mirror)
			continue;

		// Find URL
		struct json_object* urlobj;
		r = json_object_object_get_ex(mirror, "url", &urlobj);
		if (!r)
			goto NEXT;

		const char* url = json_object_get_string(urlobj);

		// Add the mirror to the downloader
		r = pakfire_mirrorlist_add_mirror(ml, url);
		if (r) {
			ERROR(ml->pakfire, "Could not add mirror %s: %m\n", url);
			goto NEXT;
		}

NEXT:
		json_object_put(mirror);
	}

	// Success
	r = 0;

ERROR:
	if (mirrors)
		json_object_put(mirrors);
	if (json)
		json_object_put(json);

	return r;
}

int pakfire_mirrorlist_add_mirror(struct pakfire_mirrorlist* ml, const char* url) {
	// Allocate a new mirror object
	struct pakfire_mirror* mirror = calloc(1, sizeof(*mirror));
	if (!mirror)
		return ENOMEM;

	// Copy URL
	pakfire_string_set(mirror->url, url);

	// Set retries
	mirror->retries_left = MIRROR_RETRIES;

	// Append it to the list
	STAILQ_INSERT_TAIL(&ml->mirrors, mirror, nodes);

	DEBUG(ml->pakfire, "Added mirror %s\n", mirror->url);

	return 0;
}

int pakfire_mirrorlist_empty(struct pakfire_mirrorlist* ml) {
	return STAILQ_EMPTY(&ml->mirrors);
}

struct pakfire_mirror* pakfire_mirrorlist_first(struct pakfire_mirrorlist* ml) {
	return STAILQ_FIRST(&ml->mirrors);
}
