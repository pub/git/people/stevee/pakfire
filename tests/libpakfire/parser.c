/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>

#include <pakfire/parser.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#include "../testsuite.h"

static int test_parser(const struct test* t) {
	struct pakfire_parser* subparser = NULL;
	char* value = NULL;
	int r = EXIT_FAILURE;

	// Create a new parser
	struct pakfire_parser* parser = pakfire_parser_create(t->pakfire, NULL, NULL, 0);

	// Retrieve a value that does not exist
	value = pakfire_parser_get(parser, NULL, "null");
	ASSERT(!value);

	// Set a value
	ASSERT_SUCCESS(pakfire_parser_set(parser, NULL, "a", "a", 0));

	// Retrieve the value again
	value = pakfire_parser_get(parser, NULL, "a");
	ASSERT_STRING_EQUALS(value, "a");

	// Append something to the value
	ASSERT_SUCCESS(pakfire_parser_append(parser, NULL, "a", "b"));

	// Retrieve the value again
	value = pakfire_parser_get(parser, NULL, "a");
	ASSERT_STRING_EQUALS(value, "a b");

	// Make a child parser
	subparser = pakfire_parser_create_child(parser, "child");
	ASSERT(subparser);

	// Try to get a again
	value = pakfire_parser_get(subparser, NULL, "a");
	ASSERT_STRING_EQUALS(value, "a b");

	// Append something to the subparser
	ASSERT_SUCCESS(pakfire_parser_append(subparser, NULL, "a", "c"));

	// The subparser should return "a b c"
	value = pakfire_parser_get(subparser, NULL, "a");
	ASSERT_STRING_EQUALS(value, "a b c");

	// The original parser should remain unchanged
	value = pakfire_parser_get(parser, NULL, "a");
	ASSERT_STRING_EQUALS(value, "a b");

	// Set another value
	ASSERT_SUCCESS(pakfire_parser_append(subparser, NULL, "b", "1"));

	// Merge the two parsers
	pakfire_parser_merge(parser, subparser);

	// Set a variable
	ASSERT_SUCCESS(pakfire_parser_set(parser, NULL, "c", "%{b}", 0));

	// Get the value of c
	value = pakfire_parser_get(parser, NULL, "c");
	ASSERT_STRING_EQUALS(value, "");

	// Dump the parser
	char* s = pakfire_parser_dump(parser);
	printf("%s\n", s);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (subparser)
		pakfire_parser_unref(subparser);
	if (parser)
		pakfire_parser_unref(parser);
	if (value)
		free(value);

	return r;
}

static const char* files[] = {
	"data/parser/test-comments.txt",
	"data/parser/test-conditionals.txt",
	"data/parser/test-declarations.txt",
	"data/parser/test-subparsers.txt",

	// Some real life examples that caused trouble
	"data/parser/perl.info",
	NULL,
};

static int test_parser_files(const struct test* t) {
	const char** file = files;
	char path[PATH_MAX];
	int r = EXIT_FAILURE;

	while (*file) {
		ASSERT_SUCCESS(pakfire_string_format(path, "%s/%s", TEST_SRC_PATH, *file));

		// Create a new parser
		struct pakfire_parser* parser = pakfire_parser_create(t->pakfire, NULL, NULL, 0);

		FILE* f = fopen(path, "r");
		ASSERT(f);

		r = pakfire_parser_read(parser, f, NULL);
		if (r) {
			fprintf(stderr, "Could not parse %s\n", path);
			return EXIT_FAILURE;
		}

		fclose(f);
		pakfire_parser_unref(parser);

		// Next file
		file++;
	}

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	return r;
}

static int test_append(const struct test* t) {
	int r = EXIT_FAILURE;

	// Create a new parser
	struct pakfire_parser* parser = pakfire_parser_create(t->pakfire, NULL, NULL, 0);

	const char* str1 =
		"a  = 1\n"
		"a += 2\n";

	// Parse something
	ASSERT_SUCCESS(pakfire_parser_parse(parser, str1, strlen(str1), NULL));

	ASSERT_STRING_EQUALS(pakfire_parser_get(parser, NULL, "a"), "1 2");

	const char* str2 =
		"build\n"

		// Append something to a variable that exists outside of this block
		"	a += 3\n"

		// Check if we can inherit a local value correctly
		"	b = %{a}\n"

		// Define a new variable in this block and append something
		"	c = 10\n"
		"	c += 20\n"
		"end\n";

	ASSERT_SUCCESS(pakfire_parser_parse(parser, str2, strlen(str2), NULL));

	ASSERT_STRING_EQUALS(pakfire_parser_get(parser, "build", "a"), "1 2 3");
	ASSERT_STRING_EQUALS(pakfire_parser_get(parser, "build", "b"), "1 2 3");
	ASSERT_STRING_EQUALS(pakfire_parser_get(parser, "build", "c"), "10 20");

	// Dump the parser
	char* s = pakfire_parser_dump(parser);
	printf("%s\n", s);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (parser)
		pakfire_parser_unref(parser);

	return r;
}
#if 0
static int test_parser_command(const struct test* t) {
	const char* command = "%(echo \"ABC\")";
	struct pakfire_parser* parser = NULL;
	int r = EXIT_FAILURE;

	parser = pakfire_parser_create(t->pakfire, NULL, NULL,
		PAKFIRE_PARSER_FLAGS_EXPAND_COMMANDS);
	ASSERT(parser);

	ASSERT_SUCCESS(pakfire_parser_set(parser, NULL, "command", command, 0));

	// Retrieve the expanded value
	char* value = pakfire_parser_get(parser, NULL, "command");
	ASSERT_STRING_EQUALS(value, "ABC");

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (parser)
		pakfire_parser_unref(parser);

	return r;
}
#endif

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_parser);
	testsuite_add_test(test_parser_files);
	testsuite_add_test(test_append);
#if 0
	testsuite_add_test(test_parser_command);
#endif

	return testsuite_run(argc, argv);
}
