/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2011 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <Python.h>
#include <libintl.h>

#include <pakfire/arch.h>

#include "archive.h"
#include "archive_file.h"
#include "errors.h"
#include "file.h"
#include "key.h"
#include "package.h"
#include "pakfire.h"
#include "problem.h"
#include "progressbar.h"
#include "repo.h"
#include "solution.h"
#include "util.h"

PyMODINIT_FUNC PyInit__pakfire(void);

PyObject* PyExc_BadSignatureError;
PyObject* PyExc_CommandExecutionError;
PyObject* PyExc_DependencyError;

PyObject* PyExc_CheckError;
PyObject* PyExc_CheckFileVerificationError;

static PyObject* _pakfire_native_arch(void) {
	const char* arch = pakfire_arch_native();
	if (!arch)
		Py_RETURN_NONE;

	return PyUnicode_FromString(arch);
}

static PyObject* _pakfire_supported_arches(void) {
	int r;

	// Fetch all architectures
	const char** arches = pakfire_supported_arches();
	if (!arches) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	// Create a new list
	PyObject* list = PyList_New(0);
	if (!list)
		return NULL;

	// Append all architectures
	for (const char** arch = arches; *arch; arch++) {
		PyObject* object = PyUnicode_FromString(*arch);
		if (!object)
			goto ERROR;

		// Append to list
		r = PyList_Append(list, object);
		if (r) {
			Py_DECREF(object);
			goto ERROR;
		}

		Py_DECREF(object);
	}

	return list;

ERROR:
	Py_DECREF(list);
	return NULL;
}

static PyMethodDef pakfireModuleMethods[] = {
	{"native_arch", (PyCFunction)_pakfire_native_arch, METH_NOARGS, NULL },
	{"supported_arches", (PyCFunction)_pakfire_supported_arches, METH_NOARGS, NULL },
	{ NULL, NULL, 0, NULL }
};

static struct PyModuleDef moduledef = {
	.m_base = PyModuleDef_HEAD_INIT,
	.m_name = "_pakfire",
	.m_size = -1,
	.m_methods = pakfireModuleMethods,
};

PyMODINIT_FUNC PyInit__pakfire(void) {
	/* Initialize locale */
	setlocale(LC_ALL, "");
	bindtextdomain(PACKAGE_TARNAME, "/usr/share/locale");
	textdomain(PACKAGE_TARNAME);

	// Create the module
	PyObject* module = PyModule_Create(&moduledef);
	if (!module)
		return NULL;

	PyExc_BadSignatureError = PyErr_NewException("_pakfire.BadSignatureError", NULL, NULL);
	Py_INCREF(PyExc_BadSignatureError);
	PyModule_AddObject(module, "BadSignatureError", PyExc_BadSignatureError);

	PyExc_CommandExecutionError = PyErr_NewException("_pakfire.CommandExecutionError", NULL, NULL);
	Py_INCREF(PyExc_CommandExecutionError);
	PyModule_AddObject(module, "CommandExecutionError", PyExc_CommandExecutionError);

	PyExc_DependencyError = PyErr_NewException("_pakfire.DependencyError", NULL, NULL);
	Py_INCREF(PyExc_DependencyError);
	PyModule_AddObject(module, "DependencyError", PyExc_DependencyError);

	// Check Exceptions

	// CheckError
	PyExc_CheckError = PyErr_NewException("_pakfire.CheckError", NULL, NULL);
	Py_INCREF(PyExc_CheckError);
	if (PyModule_AddObject(module, "CheckError", PyExc_CheckError) < 0) {
		Py_XDECREF(PyExc_CheckError);
		Py_CLEAR(PyExc_CheckError);
		goto ERROR;
	}

	// CheckFileVerificationError based on CheckError
	PyExc_CheckFileVerificationError = PyErr_NewExceptionWithDoc(
		"_pakfire.CheckFileVerificationError",
		"The file verification process failed",
		PyExc_CheckError,
		NULL
	);
	Py_INCREF(PyExc_CheckFileVerificationError);
	if (PyModule_AddObject(module, "CheckFileVerificationError", PyExc_CheckFileVerificationError) < 0) {
		Py_XDECREF(PyExc_CheckFileVerificationError);
		Py_CLEAR(PyExc_CheckFileVerificationError);
		goto ERROR;
	}

	// Pakfire
	if (PyType_Ready(&PakfireType) < 0)
		return NULL;

	Py_INCREF(&PakfireType);
	PyModule_AddObject(module, "Pakfire", (PyObject *)&PakfireType);

	// Archive
	if (PyType_Ready(&ArchiveType) < 0)
		return NULL;

	Py_INCREF(&ArchiveType);
	PyModule_AddObject(module, "Archive", (PyObject *)&ArchiveType);

	// Archive File
	if (PyType_Ready(&ArchiveFileType) < 0)
		return NULL;

	Py_INCREF(&ArchiveFileType);
	PyModule_AddObject(module, "ArchiveFile", (PyObject*)&ArchiveFileType);

	// File
	if (PyType_Ready(&FileType) < 0)
		return NULL;

	Py_INCREF(&FileType);
	PyModule_AddObject(module, "File", (PyObject *)&FileType);

	// Key
	if (PyType_Ready(&KeyType) < 0)
		return NULL;

	Py_INCREF(&KeyType);
	PyModule_AddObject(module, "Key", (PyObject *)&KeyType);

	// Package
	if (PyType_Ready(&PackageType) < 0)
		return NULL;

	Py_INCREF(&PackageType);
	PyModule_AddObject(module, "Package", (PyObject *)&PackageType);

	// Problem
	if (PyType_Ready(&ProblemType) < 0)
		return NULL;
	Py_INCREF(&ProblemType);
	PyModule_AddObject(module, "Problem", (PyObject *)&ProblemType);

	// Progressbar
	if (PyType_Ready(&ProgressbarType) < 0)
		return NULL;
	Py_INCREF(&ProgressbarType);
	PyModule_AddObject(module, "Progressbar", (PyObject *)&ProgressbarType);

	// Repo
	if (PyType_Ready(&RepoType) < 0)
		return NULL;

	Py_INCREF(&RepoType);
	PyModule_AddObject(module, "Repo", (PyObject *)&RepoType);

	// Solution
	if (PyType_Ready(&SolutionType) < 0)
		return NULL;

	Py_INCREF(&SolutionType);
	PyModule_AddObject(module, "Solution", (PyObject *)&SolutionType);

	return module;

ERROR:
	Py_DECREF(module);

	return NULL;
}
