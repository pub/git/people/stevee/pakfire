/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <Python.h>

#include <pakfire/key.h>
#include <pakfire/util.h>

#include "key.h"
#include "pakfire.h"
#include "util.h"

PyObject* new_key(PyTypeObject* type, struct pakfire_key* key) {
	KeyObject* self = (KeyObject *)type->tp_alloc(type, 0);
	if (self) {
		self->key = pakfire_key_ref(key);
	}

	return (PyObject *)self;
}

static PyObject* Key_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	KeyObject* self = (KeyObject *)type->tp_alloc(type, 0);
	if (self) {
		self->key = NULL;
	}

	return (PyObject *)self;
}

static void Key_dealloc(KeyObject* self) {
	pakfire_key_unref(self->key);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static int Key_init(KeyObject* self, PyObject* args, PyObject* kwds) {
	PakfireObject* pakfire;
	const char* fingerprint = NULL;

	if (!PyArg_ParseTuple(args, "O!s", &PakfireType, &pakfire, &fingerprint))
		return -1;

	self->key = pakfire_key_get(pakfire->pakfire, fingerprint);
	if (!self->key)
		return -1;

	return 0;
}

static PyObject* Key_repr(KeyObject* self) {
	const char* fingerprint = pakfire_key_get_fingerprint(self->key);

	return PyUnicode_FromFormat("<_pakfire.Key (%s)>", fingerprint);
}

static PyObject* Key_str(KeyObject* self) {
	char* string = pakfire_key_dump(self->key);

	PyObject* object = PyUnicode_FromString(string);
	free(string);

	return object;
}

static PyObject* Key_get_created_at(KeyObject* self) {
	const time_t t = pakfire_key_get_created(self->key);

	return PyDateTime_FromTime_t(&t);
}

static PyObject* Key_get_expires_at(KeyObject* self) {
	const time_t t = pakfire_key_get_expires(self->key);

	// Return None if the key does not expire
	if (!t)
		Py_RETURN_NONE;

	return PyDateTime_FromTime_t(&t);
}

static PyObject* Key_get_fingerprint(KeyObject* self) {
	const char* fingerprint = pakfire_key_get_fingerprint(self->key);

	return PyUnicode_FromString(fingerprint);
}

static PyObject* Key_get_uid(KeyObject* self) {
	const char* uid = pakfire_key_get_uid(self->key);

	// Raise an error on no input
	if (!uid) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicode_FromString(uid);
}

static PyObject* Key_get_name(KeyObject* self) {
	const char* name = pakfire_key_get_name(self->key);

	// Raise an error on no input
	if (!name) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicode_FromString(name);
}

static PyObject* Key_get_email(KeyObject* self) {
	const char* email = pakfire_key_get_email(self->key);

	// Raise an error on no input
	if (!email) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicode_FromString(email);
}

static PyObject* Key_get_algo(KeyObject* self) {
	const char* algo = pakfire_key_get_pubkey_algo(self->key);

	// Raise an error on no input
	if (!algo) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicode_FromString(algo);
}

static PyObject* Key_get_length(KeyObject* self) {
	const size_t length = pakfire_key_get_pubkey_length(self->key);

	return PyLong_FromLong(length);
}

static PyObject* Key_export(KeyObject* self, PyObject* args) {
	PyObject* file = NULL;
	int secret = 0;

	if (!PyArg_ParseTuple(args, "O|p", &file, &secret))
		return NULL;

	// Get a file descriptor
	int fd = PyObject_AsFileDescriptor(file);
	if (fd < 0)
		return NULL;

	// Convert to FILE*
	FILE* f = fdopen(fd, "w");
	if (!f) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	pakfire_key_export_mode_t mode;
	if (secret)
		mode = PAKFIRE_KEY_EXPORT_MODE_SECRET;
	else
		mode = PAKFIRE_KEY_EXPORT_MODE_PUBLIC;

	// Export the key
	int r = pakfire_key_export(self->key, f, mode);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Key_delete(KeyObject* self) {
	int r = pakfire_key_delete(self->key);
	if (r == 0)
		Py_RETURN_TRUE;

	return NULL;
}

static PyObject* Key_get_public_key(KeyObject* self) {
	char* buffer = NULL;
	size_t length = 0;
	int r;

	PyObject* object = NULL;

	// Fetch the public key
	r = pakfire_key_get_public_key(self->key, &buffer, &length);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Create a unicode object
	object = PyUnicode_FromStringAndSize(buffer, length);

ERROR:
	if (buffer)
		free(buffer);

	return object;
}

static PyObject* Key_get_secret_key(KeyObject* self) {
	char* buffer = NULL;
	size_t length = 0;
	int r;

	PyObject* object = NULL;

	// Fetch the secret key
	r = pakfire_key_get_secret_key(self->key, &buffer, &length);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Create a unicode object
	object = PyUnicode_FromStringAndSize(buffer, length);

ERROR:
	if (buffer)
		free(buffer);

	return object;
}

static struct PyMethodDef Key_methods[] = {
	{
		"delete",
		(PyCFunction)Key_delete,
		METH_NOARGS,
		NULL
	},
	{
		"export",
		(PyCFunction)Key_export,
		METH_VARARGS,
		NULL
	},
	{ NULL },
};

static struct PyGetSetDef Key_getsetters[] = {
	{
		"algo",
		(getter)Key_get_algo,
		NULL,
		NULL,
		NULL,
	},
	{
		"created_at",
		(getter)Key_get_created_at,
		NULL,
		NULL,
		NULL,
	},
	{
		"email",
		(getter)Key_get_email,
		NULL,
		NULL,
		NULL,
	},
	{
		"expires_at",
		(getter)Key_get_expires_at,
		NULL,
		NULL,
		NULL,
	},
	{
		"fingerprint",
		(getter)Key_get_fingerprint,
		NULL,
		NULL,
		NULL,
	},
	{
		"length",
		(getter)Key_get_length,
		NULL,
		NULL,
		NULL,
	},
	{
		"name",
		(getter)Key_get_name,
		NULL,
		NULL,
		NULL,
	},
	{
		"public_key",
		(getter)Key_get_public_key,
		NULL,
		NULL,
		NULL,
	},
	{
		"secret_key",
		(getter)Key_get_secret_key,
		NULL,
		NULL,
		NULL,
	},
	{
		"uid",
		(getter)Key_get_uid,
		NULL,
		NULL,
		NULL,
	},
	{ NULL },
};

PyTypeObject KeyType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	tp_name:            "_pakfire.Key",
	tp_basicsize:       sizeof(KeyObject),
	tp_flags:           Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	tp_new:             Key_new,
	tp_dealloc:         (destructor)Key_dealloc,
	tp_init:            (initproc)Key_init,
	tp_doc:             "Key object",
	tp_methods:         Key_methods,
	tp_getset:          Key_getsetters,
	tp_repr:            (reprfunc)Key_repr,
	tp_str:             (reprfunc)Key_str,
};
