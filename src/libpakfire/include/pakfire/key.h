/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_KEY_H
#define PAKFIRE_KEY_H

#include <time.h>

struct pakfire_key;

#include <pakfire/pakfire.h>

typedef enum pakfire_key_export_mode {
	PAKFIRE_KEY_EXPORT_MODE_PUBLIC = 0,
	PAKFIRE_KEY_EXPORT_MODE_SECRET,
} pakfire_key_export_mode_t;

struct pakfire_key* pakfire_key_ref(struct pakfire_key* key);
void pakfire_key_unref(struct pakfire_key* key);

int pakfire_key_fetch(struct pakfire_key** key, struct pakfire* pakfire,
	const char* uid, const char* fingerprint);

struct pakfire_key* pakfire_key_get(struct pakfire* pakfire, const char* fingerprint);
int pakfire_key_delete(struct pakfire_key* key);

// Access key properties
const char* pakfire_key_get_fingerprint(struct pakfire_key* key);
const char* pakfire_key_get_uid(struct pakfire_key* key);
const char* pakfire_key_get_name(struct pakfire_key* key);
const char* pakfire_key_get_email(struct pakfire_key* key);
const char* pakfire_key_get_pubkey_algo(struct pakfire_key* key);
size_t pakfire_key_get_pubkey_length(struct pakfire_key* key);
int pakfire_key_has_secret(struct pakfire_key* key);
time_t pakfire_key_get_created(struct pakfire_key* key);
time_t pakfire_key_get_expires(struct pakfire_key* key);
int pakfire_key_is_revoked(struct pakfire_key* key);

int pakfire_key_generate(struct pakfire_key** key, struct pakfire* pakfire,
	const char* algo, const char* userid);
int pakfire_key_export(struct pakfire_key* key, FILE* f, pakfire_key_export_mode_t mode);
int pakfire_key_import(struct pakfire* pakfire, FILE* f, struct pakfire_key*** keys);

int pakfire_key_get_public_key(struct pakfire_key* key, char** buffer, size_t* length);
int pakfire_key_get_secret_key(struct pakfire_key* key, char** buffer, size_t* length);

char* pakfire_key_dump(struct pakfire_key* key);

#ifdef PAKFIRE_PRIVATE

#include <gpgme.h>

#define PAKFIRE_KEY_FPR_MAXLEN 41

int pakfire_key_create(struct pakfire_key** key, struct pakfire* pakfire, gpgme_key_t gpgkey);

int pakfire_key_sign(struct pakfire_key* key, const char* buffer, const size_t buffer_length,
	char** signature, size_t* signature_length, time_t* timestamp);

#endif

#endif /* PAKFIRE_KEY_H */
