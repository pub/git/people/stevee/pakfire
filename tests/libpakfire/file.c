/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/file.h>
#include <pakfire/filelist.h>

#include "../testsuite.h"

static int test_create(const struct test* t) {
	struct pakfire_file* file = NULL;

	// Create a new file
	ASSERT_SUCCESS(pakfire_file_create(&file, t->pakfire));

	// Set path & check
	ASSERT_SUCCESS(pakfire_file_set_path(file, "/abc"));
	ASSERT_STRING_EQUALS(pakfire_file_get_path(file), "/abc");

	// Destroy it
	ASSERT_NULL(pakfire_file_unref(file));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_create_invalid(const struct test* t) {
	struct pakfire_file* file = NULL;

	// Create a new file
	ASSERT_SUCCESS(pakfire_file_create(&file, t->pakfire));

	// Set path
	ASSERT_ERRNO(pakfire_file_set_path(file, NULL), EINVAL);

	// It should not be possible to set relative absolute paths
	ASSERT_ERRNO(pakfire_file_set_abspath(file, "abc/abc"), EINVAL);

	// Destroy it
	ASSERT_NULL(pakfire_file_unref(file));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_create_filelist(const struct test* t) {
	struct pakfire_filelist* list = NULL;
	struct pakfire_file* file1 = NULL;
	struct pakfire_file* file2 = NULL;
	struct pakfire_file* file3 = NULL;
	int r = EXIT_FAILURE;

	// Create a new filelist
	ASSERT_SUCCESS(pakfire_filelist_create(&list, t->pakfire));

	// Create some files
	ASSERT_SUCCESS(pakfire_file_create(&file1, t->pakfire));
	ASSERT_SUCCESS(pakfire_file_create(&file2, t->pakfire));
	ASSERT_SUCCESS(pakfire_file_create(&file3, t->pakfire));

	// Set some paths
	ASSERT_SUCCESS(pakfire_file_set_path(file1, "/1"));
	ASSERT_SUCCESS(pakfire_file_set_path(file2, "/2"));
	ASSERT_SUCCESS(pakfire_file_set_path(file3, "/3"));

	// Add the files to the list
	ASSERT_SUCCESS(pakfire_filelist_add(list, file1));
	ASSERT_SUCCESS(pakfire_filelist_add(list, file3));
	ASSERT_SUCCESS(pakfire_filelist_add(list, file2));

	// Dump the filelist
	pakfire_filelist_dump(list, 0);

	ASSERT(pakfire_filelist_length(list) == 3);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (list)
		pakfire_filelist_unref(list);
	if (file1)
		pakfire_file_unref(file1);
	if (file2)
		pakfire_file_unref(file2);
	if (file3)
		pakfire_file_unref(file3);

	return r;
}


int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create);
	testsuite_add_test(test_create_invalid);

	testsuite_add_test(test_create_filelist);

	return testsuite_run(argc, argv);
}
