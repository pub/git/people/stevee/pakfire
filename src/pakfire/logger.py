#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2011 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import logging
import sys
import systemd.journal
import time

def setup(name, syslog_identifier="pakfire", enable_console=True, debug=False):
	level = logging.INFO

	# Enable debug logging
	if debug:
		level = logging.DEBUG

	log = logging.getLogger(name)
	log.setLevel(level)

	# Do not propagate anything
	log.propagate = False

	# Clear any previous handlers
	log.handlers.clear()

	# Enable console output
	if enable_console:
		console = ConsoleHandler()
		log.addHandler(console)

		# Do not send any debug output to the console
		console.setLevel(logging.INFO)

	# Enable logging to journald
	journal = systemd.journal.JournalHandler(
		SYSLOG_IDENTIFIER=syslog_identifier,
	)
	log.addHandler(journal)

	return log

class BuildFormatter(logging.Formatter):
	def __init__(self):
		self._fmt = "[%(asctime)s] %(message)s"
		self.datefmt = None

		self.starttime = time.time()

	def converter(self, recordtime):
		"""
			This returns a timestamp relatively to the time when we started
			the build.
		"""
		recordtime -= self.starttime

		return time.gmtime(recordtime)

	def formatTime(self, record, datefmt=None):
		ct = self.converter(record.created)
		t = time.strftime("%H:%M:%S", ct)
		s = "%s,%03d" % (t, record.msecs)
		return s


class ConsoleHandler(logging.Handler):
	"""
		This simply writes everything to the console it receives.
	"""
	def emit(self, record):
		try:
			msg = self.format(record)

			# Append newline if necessary
			if not msg.endswith("\n"):
				msg = "%s\n" % msg

			# Select output file
			if record.levelno == logging.DEBUG or record.levelno >= logging.ERROR:
				fd = sys.stderr
			else:
				fd = sys.stdout

			# Write the output
			fd.write(msg)

			# Immediately flush
			self.flush()
		except Exception:
			self.handleError(record)

	def flush(self):
		sys.stdout.flush()
		sys.stderr.flush()
