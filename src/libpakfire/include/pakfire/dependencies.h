/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_DEPENDENCIES_H
#define PAKFIRE_DEPENDENCIES_H

#ifdef PAKFIRE_PRIVATE

#include <solv/pool.h>

#include <pakfire/package.h>
#include <pakfire/pakfire.h>

extern const struct pakfire_dep {
	const enum pakfire_package_key key;
	const char* name;
} pakfire_deps[];

const char* pakfire_dep2str(struct pakfire* pakfire, Id id);
Id pakfire_str2dep(struct pakfire* pakfire, const char* s);

int pakfire_str2deps(struct pakfire* pakfire, struct pakfire_package* pkg,
	const enum pakfire_package_key key, const char* deps);

#endif /* PAKFIRE_PRIVATE */

#endif /* PAKFIRE_DEPENDENCIES_H */
