/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fts.h>
#include <fcntl.h>
#include <glob.h>
#include <stddef.h>
#include <stdlib.h>

#include <pakfire/arch.h>
#include <pakfire/dist.h>
#include <pakfire/downloader.h>
#include <pakfire/linter.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/packager.h>
#include <pakfire/pakfire.h>
#include <pakfire/parser.h>
#include <pakfire/private.h>
#include <pakfire/repo.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

// XXX for now
#define BASEURL "https://source.ipfire.org/source-3.x/"

#define PAKFIRE_MACROS_DIR				"/usr/lib/pakfire/macros"
#define PAKFIRE_MACROS_GLOB_PATTERN		PAKFIRE_MACROS_DIR "/*.macro"

static const char* pakfire_dist_excludes[] = {
	// Don't package any backup files
	"*~",
	"*.bak",

	// Don't package any other package files
	"*.pfm",

	NULL,
};

static int pakfire_makefile_set_defaults(struct pakfire* pakfire,
		struct pakfire_parser* parser, const char* path) {
	char buffer[1024];
	int r;

	// Set epoch
	pakfire_parser_set(parser, NULL, "epoch", "0", 0);

	// Set EVR
	pakfire_parser_set(parser, NULL, "evr", "%{epoch}:%{version}-%{_release}", 0);

	// Set vendor
	pakfire_parser_set(parser, NULL, "vendor", "%{DISTRO_VENDOR}", 0);

	// Set DISTRO_NAME
	const char* name = pakfire_get_distro_name(pakfire);
	if (name)
		pakfire_parser_set(parser, NULL, "DISTRO_NAME", name, 0);

	// Set DISTRO_SNAME
	const char* id = pakfire_get_distro_id(pakfire);
	if (id)
		pakfire_parser_set(parser, NULL, "DISTRO_SNAME", name, 0);

	// Set DISTRO_RELEASE
	const char* version_id = pakfire_get_distro_version_id(pakfire);
	if (version_id)
		pakfire_parser_set(parser, NULL, "DISTRO_RELEASE", version_id, 0);

	// Set DISTRO_DISTTAG
	const char* tag = pakfire_get_distro_tag(pakfire);
	if (tag)
		pakfire_parser_set(parser, NULL, "DISTRO_DISTTAG", tag, 0);

	// Set DISTRO_VENDOR
	const char* vendor = pakfire_get_distro_vendor(pakfire);
	if (vendor)
		pakfire_parser_set(parser, NULL, "DISTRO_VENDOR", vendor, 0);

	// Set DISTRO_ARCH
	const char* arch = pakfire_get_arch(pakfire);
	if (arch) {
		pakfire_parser_set(parser, NULL, "DISTRO_ARCH", arch, 0);

		const char* platform = pakfire_arch_platform(arch);
		if (platform)
			pakfire_parser_set(parser, NULL, "DISTRO_PLATFORM", platform, 0);

		if (vendor) {
			// Set DISTRO_MACHINE
			r = pakfire_arch_machine(buffer, arch, vendor);
			if (!r)
				pakfire_parser_set(parser, NULL, "DISTRO_MACHINE", buffer, 0);

			// Set DISTRO_BUILDTARGET
			r = pakfire_arch_buildtarget(buffer, arch, vendor);
			if (!r)
				pakfire_parser_set(parser, NULL, "DISTRO_BUILDTARGET", buffer, 0);
		}
	}

	// Set BASEDIR
	const char* dirname = pakfire_dirname(path);
	if (dirname) {
		const char* root = pakfire_get_path(pakfire);

		pakfire_parser_set(parser, NULL, "BASEDIR",
			pakfire_path_relpath(root, dirname), 0);
	}

	long processors_online = sysconf(_SC_NPROCESSORS_ONLN);

	// Set PARALLELISMFLAGS
	if (processors_online) {
		pakfire_string_format(buffer, "-j%ld", processors_online);
		pakfire_parser_set(parser, "build", "PARALLELISMFLAGS", buffer, 0);
	}

	return 0;
}

int pakfire_read_makefile(struct pakfire_parser** parser, struct pakfire* pakfire,
		const char* path, struct pakfire_parser_error** error) {
	int r = 1;

	*parser = pakfire_parser_create(pakfire, NULL, NULL, PAKFIRE_PARSER_FLAGS_EXPAND_COMMANDS);
	if (!*parser) {
		r = 1;
		goto ERROR;
	}

	// Set defaults
	r = pakfire_makefile_set_defaults(pakfire, *parser, path);
	if (r)
		goto ERROR;

	// Find all macros

	DEBUG(pakfire, "Searching for macros in %s\n", PAKFIRE_MACROS_GLOB_PATTERN);

	glob_t globmacros;
	r = glob(PAKFIRE_MACROS_GLOB_PATTERN, 0, NULL, &globmacros);

	// Handle any errors
	switch (r) {
		case 0:
		case GLOB_NOMATCH:
			break;

		case GLOB_NOSPACE:
			errno = ENOMEM;
			goto ERROR;

		case GLOB_ABORTED:
			goto ERROR;

		default:
			ERROR(pakfire, "glob() returned an unhandled error: %d\n", r);
			goto ERROR;
	}

	DEBUG(pakfire, "Found %zu macro(s)\n", globmacros.gl_pathc);

	// Read all macros
	for (unsigned int i = 0; i < globmacros.gl_pathc; i++) {
		// Parse the file
		r = pakfire_parser_read_file(*parser, globmacros.gl_pathv[i], error);
		if (r)
			goto ERROR;
	}

	globfree(&globmacros);

	// Finally, parse the makefile
	r = pakfire_parser_read_file(*parser, path, error);
	if (r) {
		ERROR(pakfire, "Could not read makefile %s: %m\n", path);
		goto ERROR;
	}

	return 0;

ERROR:
	globfree(&globmacros);

	if (*parser) {
		pakfire_parser_unref(*parser);
		*parser = NULL;
	}

	return r;
}

static int pakfire_dist_create_downloader_and_mirrorlist(
		struct pakfire* pakfire, struct pakfire_parser* makefile,
		struct pakfire_downloader** downloader, struct pakfire_mirrorlist** mirrorlist) {
	char* p = NULL;

	// Create the downloader
	int r = pakfire_downloader_create(downloader, pakfire);
	if (r) {
		ERROR(pakfire, "Could not initialize downloader\n");
		return r;
	}

	// Fetch source_dl
	char* source_dl = pakfire_parser_get(makefile, NULL, "source_dl");

	// We do not need to create a mirrorlist if this isn't set
	if (!source_dl)
		return 0;

	// Create mirrorlist
	r = pakfire_mirrorlist_create(mirrorlist, pakfire);
	if (r) {
		ERROR(pakfire, "Could not create the mirrorlist\n");
		goto ERROR;
	}

	// Add all mirrors
	const char* mirror = strtok_r(source_dl, " ", &p);

	while (mirror) {
		r = pakfire_mirrorlist_add_mirror(*mirrorlist, mirror);
		if (r)
			goto ERROR;

		mirror = strtok_r(NULL, " ", &p);
	}

	// Success
	r = 0;

ERROR:
	if (source_dl)
		free(source_dl);

	return r;
}

static int pakfire_dist_add_source(struct pakfire* pakfire, struct pakfire_packager* packager,
		struct pakfire_package* pkg, struct pakfire_downloader* downloader,
		struct pakfire_mirrorlist* mirrorlist, const char* filename) {
	int r;
	char archive_path[PATH_MAX];
	char cache_path[PATH_MAX];

	const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME);

	// Compose path
	r = pakfire_cache_path(pakfire, cache_path, "sources/%s/%s", name, filename);
	if (r)
		return r;

	// Download the file if it does not exist in the cache
	if (access(cache_path, R_OK) != 0) {
		r = pakfire_downloader_retrieve(downloader, BASEURL, mirrorlist,
			NULL, filename, cache_path, 0, NULL, 0, 0);
		if (r)
			return r;
	}

	pakfire_string_format(archive_path, "/files/%s", filename);

	// Add file to package
	return pakfire_packager_add(packager, cache_path, archive_path);
}

static int pakfire_dist_add_sources(struct pakfire* pakfire, struct pakfire_packager* packager,
		struct pakfire_package* pkg, struct pakfire_parser* makefile) {
	// Fetch sources
	char** sources = pakfire_parser_get_split(makefile, NULL, "sources", ' ');

	// Nothing to do if this variable is empty
	if (!sources)
		return 1;

	struct pakfire_downloader* downloader = NULL;
	struct pakfire_mirrorlist* mirrorlist = NULL;

	// Create a downloader and mirrorlist
	int r = pakfire_dist_create_downloader_and_mirrorlist(pakfire, makefile,
		&downloader, &mirrorlist);
	if (r)
		goto ERROR;

	// Add all files one by one
	for (char** source = sources; *source; source++) {
		DEBUG(pakfire, "Adding source file %s\n", *source);

		r = pakfire_dist_add_source(pakfire, packager, pkg, downloader, mirrorlist, *source);
		if (r) {
			ERROR(pakfire, "Could not add '%s' to package: %m\n", *source);
			goto ERROR;
		}
	}

	// Success
	r = 0;

ERROR:
	if (downloader)
		pakfire_downloader_unref(downloader);
	if (mirrorlist)
		pakfire_mirrorlist_unref(mirrorlist);

	if (sources) {
		for (char** source = sources; *source; source++)
			free(*source);
		free(sources);
	}

	return r;
}

static const char* pakfire_dist_find_root(struct pakfire* pakfire, const char* file) {
	char path[PATH_MAX];

	// Find the absolute path of the makefile
	char* p = realpath(file, path);
	if (!p) {
		ERROR(pakfire, "realpath(%s) failed: %m\n", file);
		return NULL;
	}

	// Return the parent directory
	return pakfire_dirname(p);
}

static int pakfire_dist_add_files(struct pakfire* pakfire,
		struct pakfire_packager* packager, const char* file) {
	struct pakfire_filelist* filelist = NULL;
	int r = 1;

	// Find the package directory
	const char* root = pakfire_dist_find_root(pakfire, file);
	if (!root)
		return 1;

	DEBUG(pakfire, "Adding all files in '%s' to package...\n", root);

	// Create a new filelist
	r = pakfire_filelist_create(&filelist, pakfire);
	if (r)
		goto ERROR;

	// Scan for any files
	r = pakfire_filelist_scan(filelist, root, NULL, pakfire_dist_excludes);
	if (r)
		goto ERROR;

	// Add all files to the package
	r = pakfire_packager_add_files(packager, filelist);
	if (r)
		goto ERROR;

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

PAKFIRE_EXPORT int pakfire_dist(struct pakfire* pakfire, const char* path,
		const char* target, char** result) {
	struct pakfire_parser* makefile;
	struct pakfire_parser_error* error = NULL;

	struct pakfire_packager* packager = NULL;
	struct pakfire_package* pkg = NULL;

	// Load makefile
	int r = pakfire_read_makefile(&makefile, pakfire, path, &error);
	if (r) {
		if (error)
			pakfire_parser_error_unref(error);
		else
			ERROR(pakfire, "Could not read makefile: %m\n");

		return r;
	}

	// The architecture is always "src"
	r = pakfire_parser_set(makefile, NULL, "arch", "src", 0);
	if (r) {
		ERROR(pakfire, "Could not set architecture to 'src': %m\n");
		goto ERROR;
	}

	// Get the package object
	r = pakfire_parser_create_package(makefile, &pkg, NULL, NULL, "src");
	if (r)
		goto ERROR;

	// Perform some linting
	r = pakfire_lint_makefile(pakfire, makefile, path, pkg);
	if (r)
		goto ERROR;

	// Create a packager
	r = pakfire_packager_create(&packager, pakfire, pkg);
	if (r)
		goto ERROR;

	// Add all files in the directory
	r = pakfire_dist_add_files(pakfire, packager, path);
	if (r)
		goto ERROR;

	// Add all source files (which might need to be downloaded)
	r = pakfire_dist_add_sources(pakfire, packager, pkg, makefile);
	if (r)
		goto ERROR;

	// Write the finished package
	r = pakfire_packager_finish_to_directory(packager, target, result);
	if (r)
		goto ERROR;

ERROR:
	if (packager)
		pakfire_packager_unref(packager);
	if (pkg)
		pakfire_package_unref(pkg);
	pakfire_parser_unref(makefile);

	return r;
}
