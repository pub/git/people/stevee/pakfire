/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_STRING_H
#define PAKFIRE_STRING_H

#ifdef PAKFIRE_PRIVATE

#include <stdarg.h>

/*
	Formats a string and stores it in the given buffer.
*/
#define pakfire_string_format(s, format, ...) \
	__pakfire_string_format(s, sizeof(s), format, __VA_ARGS__)
int __pakfire_string_format(char* s, const size_t length,
	const char* format, ...) __attribute__((format(printf, 3, 4)));
int __pakfire_string_vformat(char* s, const size_t length,
	const char* format, va_list args) __attribute__((format(printf, 3, 0)));

/*
	Simpler version when a string needs to be copied.
*/
#define pakfire_string_set(s, value) \
	__pakfire_string_set(s, sizeof(s), value)
int __pakfire_string_set(char* s, const size_t length, const char* value);

int pakfire_string_startswith(const char* s, const char* prefix);
int pakfire_string_endswith(const char* s, const char* suffix);
int pakfire_string_matches(const char* s, const char* pattern);

int pakfire_string_partition(const char* s, const char* delim, char** s1, char** s2);
char* pakfire_string_replace(const char* s, const char* pattern, const char* repl);
char** pakfire_string_split(const char* s, char delim);
char* pakfire_string_join(char** list, const char* delim);

#define TIME_STRING_MAX 32

#define pakfire_format_size(dst, value) \
	__pakfire_format_size(dst, sizeof(dst), value)
int __pakfire_format_size(char* dst, size_t length, double value);
int pakfire_format_speed(char* dst, size_t length, double value);

#define pakfire_strftime(buffer, format, t) \
	__pakfire_strftime(buffer, sizeof(buffer), format, t)
int __pakfire_strftime(char* buffer, const size_t length,
	const char* format, const time_t t) __attribute__((format(strftime, 3, 0)));

#define pakfire_strftime_now(buffer, format) \
	__pakfire_strftime_now(buffer, sizeof(dest), format)
int __pakfire_strftime_now(char* buffer, const size_t length, const char* format)
	__attribute__((format(strftime, 3, 0)));

#define pakfire_format_time(buffer, t) \
	__pakfire_format_time(buffer, sizeof(buffer), t)
int __pakfire_format_time(char* buffer, const size_t length, const time_t t);

size_t pakfire_string_parse_bytes(const char* s);

int pakfire_string_is_url(const char* s);

#endif

#endif /* PAKFIRE_STRING_H */
