/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PROGRESSBAR_H
#define PAKFIRE_PROGRESSBAR_H

#include <stdio.h>

struct pakfire_progressbar;

int pakfire_progressbar_create(struct pakfire_progressbar** progressbar, FILE* f);

struct pakfire_progressbar* pakfire_progressbar_ref(struct pakfire_progressbar* p);
struct pakfire_progressbar* pakfire_progressbar_unref(struct pakfire_progressbar* p);

int pakfire_progressbar_start(struct pakfire_progressbar* p, unsigned long value);
int pakfire_progressbar_update(struct pakfire_progressbar* p, unsigned long value);
int pakfire_progressbar_increment(struct pakfire_progressbar* p, unsigned long value);
int pakfire_progressbar_finish(struct pakfire_progressbar* p);
int pakfire_progressbar_reset(struct pakfire_progressbar* p);

void pakfire_progressbar_set_max(struct pakfire_progressbar* p, unsigned long value);

int pakfire_progressbar_add_string(struct pakfire_progressbar* p, const char* format, ...);
int pakfire_progressbar_add_counter(struct pakfire_progressbar* p);
int pakfire_progressbar_add_percentage(struct pakfire_progressbar* p);
int pakfire_progressbar_add_bar(struct pakfire_progressbar* p);
int pakfire_progressbar_add_timer(struct pakfire_progressbar* p);
int pakfire_progressbar_add_bytes_transferred(struct pakfire_progressbar* p);
int pakfire_progressbar_add_eta(struct pakfire_progressbar* p);
int pakfire_progressbar_add_transfer_speed(struct pakfire_progressbar* p);

#endif /* PAKFIRE_PROGRESSBAR_H */
