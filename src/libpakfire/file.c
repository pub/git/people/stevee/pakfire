/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fnmatch.h>
#include <libgen.h>
#include <limits.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>
#include <sys/capability.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#include <archive_entry.h>

#include <gelf.h>

#include <pakfire/constants.h>
#include <pakfire/digest.h>
#include <pakfire/fhs.h>
#include <pakfire/file.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/private.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

enum pakfire_file_verification_status {
	PAKFIRE_FILE_NOENT                = (1 << 0),
	PAKFIRE_FILE_TYPE_CHANGED         = (1 << 1),
	PAKFIRE_FILE_PERMISSIONS_CHANGED  = (1 << 2),
	PAKFIRE_FILE_DEV_CHANGED          = (1 << 3),
	PAKFIRE_FILE_SIZE_CHANGED         = (1 << 4),
	PAKFIRE_FILE_OWNER_CHANGED        = (1 << 5),
	PAKFIRE_FILE_GROUP_CHANGED        = (1 << 6),
	PAKFIRE_FILE_CTIME_CHANGED        = (1 << 7),
	PAKFIRE_FILE_MTIME_CHANGED        = (1 << 8),
	PAKFIRE_FILE_PAYLOAD_CHANGED      = (1 << 9),
};

struct pakfire_file {
	struct pakfire* pakfire;
	int nrefs;

	// The relative path
	char path[PATH_MAX];

	// The absolute path in the file system
	char abspath[PATH_MAX];

	// File Ownership
	char uname[LOGIN_NAME_MAX];
	char gname[LOGIN_NAME_MAX];

	// Stat
	struct stat st;

	// Capabilities
	cap_t caps;

	// Flags
	int flags;

	// Link destinations
	char hardlink[PATH_MAX];
	char symlink[PATH_MAX];

	// Digests
	struct pakfire_digests digests;

	// MIME Type
	char mimetype[NAME_MAX];

	// Class
	int class;

	// Verification Status
	int verify_status;

	// File Issues
	int issues;
	int check_done:1;

	#warning TODO data
	//int is_datafile;
};

/*
	Capabilities
*/
static int pakfire_file_read_fcaps(struct pakfire_file* file,
		const struct vfs_cap_data* cap_data, size_t length) {
	int max = 0;
	int r;

	uint32_t magic_etc = le32toh(cap_data->magic_etc);

	// Which version are we dealing with?
	switch (magic_etc & VFS_CAP_REVISION_MASK) {
		case VFS_CAP_REVISION_1:
			length -= XATTR_CAPS_SZ_1;
			max = VFS_CAP_U32_1;
			break;

		case VFS_CAP_REVISION_2:
			length -= XATTR_CAPS_SZ_2;
			max = VFS_CAP_U32_2;
			break;

		case VFS_CAP_REVISION_3:
			length -= XATTR_CAPS_SZ_3;
			max = VFS_CAP_U32_3;
			break;

		// Unknown version
		default:
			errno = EINVAL;
			return 1;
	}

	// Check if we have received the correct data
	if (length) {
		errno = EINVAL;
		return 1;
	}

	// Allocate capabilities
	file->caps = cap_init();
	if (!file->caps) {
		ERROR(file->pakfire, "Could not allocate capabilities: %m\n");
		r = 1;
		goto ERROR;
	}

	int mask;
	int index;

	int cap_permitted;
	int cap_inheritable;
	int cap_effective = 0;

	for (unsigned int cap = 0; cap_valid(cap); cap++) {
		// Find the index where to find this cap
		index = CAP_TO_INDEX(cap);
		mask  = CAP_TO_MASK(cap);

		// End if we have reached the end of the data
		if (index > max)
			break;

		// Check for permitted/inheritable flag set
		cap_permitted   = le32toh(cap_data->data[index].permitted)   & mask;
		cap_inheritable = le32toh(cap_data->data[index].inheritable) & mask;

		// Check for effective
		if (magic_etc & VFS_CAP_FLAGS_EFFECTIVE)
			cap_effective = cap_permitted | cap_inheritable;

		cap_value_t caps[] = { cap };

		if (cap_permitted) {
			r = cap_set_flag(file->caps, CAP_PERMITTED, 1, caps, CAP_SET);
			if (r) {
				ERROR(file->pakfire, "Could not set capability %d: %m\n", cap);
				goto ERROR;
			}
		}

		if (cap_inheritable) {
			r = cap_set_flag(file->caps, CAP_INHERITABLE, 1, caps, CAP_SET);
			if (r) {
				ERROR(file->pakfire, "Could not set capability %d: %m\n", cap);
				goto ERROR;
			}
		}

		if (cap_effective) {
			r = cap_set_flag(file->caps, CAP_EFFECTIVE, 1, caps, CAP_SET);
			if (r) {
				ERROR(file->pakfire, "Could not set capability %d: %m\n", cap);
				goto ERROR;
			}
		}
	}

#ifdef ENABLE_DEBUG
	char* text = cap_to_text(file->caps, NULL);
	if (text) {
		DEBUG(file->pakfire, "%s: Capabilities %s\n", file->path, text);
		cap_free(text);
	}
#endif

	return 0;

ERROR:
	if (file->caps) {
		cap_free(file->caps);
		file->caps = NULL;
	}

	return r;
}

int pakfire_file_write_fcaps(struct pakfire_file* file, struct vfs_cap_data* cap_data) {
	cap_flag_value_t cap_permitted;
	cap_flag_value_t cap_inheritable;
	cap_flag_value_t cap_effective;
	int r;

	// This should not be called when we have no caps
	if (!file->caps) {
		errno = EINVAL;
		return 1;
	}

	uint32_t magic = VFS_CAP_REVISION_2;

	for (unsigned int cap = 0; cap_valid(cap); cap++) {
		// Find the index where to find this cap
		int index = CAP_TO_INDEX(cap);
		int mask  = CAP_TO_MASK(cap);

		// Fetch CAP_PERMITTED
		r = cap_get_flag(file->caps, cap, CAP_PERMITTED, &cap_permitted);
		if (r) {
			ERROR(file->pakfire, "Could not fetch capability %d: %m\n", cap);
			goto ERROR;
		}

		// Fetch CAP_INHERITABLE
		r = cap_get_flag(file->caps, cap, CAP_INHERITABLE, &cap_inheritable);
		if (r) {
			ERROR(file->pakfire, "Could not fetch capability %d: %m\n", cap);
			goto ERROR;
		}

		// Fetch CAP_EFFECTIVE
		r = cap_get_flag(file->caps, cap, CAP_EFFECTIVE, &cap_effective);
		if (r) {
			ERROR(file->pakfire, "Could not fetch capability %d: %m\n", cap);
			goto ERROR;
		}

		// Store CAP_PERMITTED
		if (cap_permitted)
			cap_data->data[index].permitted |= htole32(mask);

		// Store CAP_INHERITED
		if (cap_inheritable)
			cap_data->data[index].inheritable |= htole32(mask);

		// Set EFFECTIVE flag if CAP_EFFECTIVE is set
		if (cap_effective)
			magic |= VFS_CAP_FLAGS_EFFECTIVE;
	}

	// Store the magic value
	cap_data->magic_etc = htole32(magic);

ERROR:
	return r;
}

static int pakfire_file_from_archive_entry(struct pakfire_file* file, struct archive_entry* entry) {
	char* buffer = NULL;
	const char* path = NULL;
	const char* attr = NULL;
	const void* value = NULL;
	size_t size = 0;
	int r = 0;

	// Set abspath
	path = archive_entry_sourcepath(entry);
	if (path) {
		// Make path absolute
		path = pakfire_path_abspath(path);
		if (!path) {
			r = 1;
			goto ERROR;
		}

		// Set
		r = pakfire_file_set_abspath(file, path);
		if (r) {
			ERROR(file->pakfire, "Could not set abspath: %m\n");
			goto ERROR;
		}
	}

	// Set path
	path = archive_entry_pathname(entry);
	if (path) {
		r = pakfire_file_set_path(file, path);
		if (r) {
			ERROR(file->pakfire, "Could not set path: %m\n");
			goto ERROR;
		}
	}

	// Set links
	pakfire_file_set_hardlink(file, archive_entry_hardlink(entry));
	pakfire_file_set_symlink(file, archive_entry_symlink(entry));

	pakfire_file_set_nlink(file, archive_entry_nlink(entry));
	pakfire_file_set_inode(file, archive_entry_ino64(entry));
	pakfire_file_set_dev(file, archive_entry_dev(entry));

	// Set size
	pakfire_file_set_size(file, archive_entry_size(entry));

	// Set mode
	pakfire_file_set_mode(file, archive_entry_mode(entry));

	// Set dev type
	if (archive_entry_dev_is_set(entry))
		pakfire_file_set_dev(file, archive_entry_dev(entry));

	// Set uname
	pakfire_file_set_uname(file, archive_entry_uname(entry));

	// Set gname
	pakfire_file_set_gname(file, archive_entry_gname(entry));

	// Set times
	pakfire_file_set_ctime(file, archive_entry_ctime(entry));
	pakfire_file_set_mtime(file, archive_entry_mtime(entry));

	// Reset iterating over extended attributes
	archive_entry_xattr_reset(entry);

	// Read any extended attributes
	while (archive_entry_xattr_next(entry, &attr, &value, &size) == ARCHIVE_OK) {
		// Config Files
		if (strcmp(attr, "PAKFIRE.config") == 0) {
			r = pakfire_file_set_flags(file, PAKFIRE_FILE_CONFIG);
			if (r)
				goto ERROR;

		// MIME type
		} else if (strcmp(attr, "PAKFIRE.mimetype") == 0) {
			// Copy the value into a NULL-terminated buffer
			r = asprintf(&buffer, "%.*s", (int)size, (const char*)value);
			if (r < 0)
				goto ERROR;

			// Assign the value
			r = pakfire_file_set_mimetype(file, buffer);
			if (r)
				goto ERROR;

		// Digest: SHA-3-512
		} else if (strcmp(attr, "PAKFIRE.digests.sha3_512") == 0) {
			r = pakfire_file_set_digest(file, PAKFIRE_DIGEST_SHA3_512, value, size);
			if (r)
				goto ERROR;

		// Digest: SHA-3-256
		} else if (strcmp(attr, "PAKFIRE.digests.sha3_256") == 0) {
			r = pakfire_file_set_digest(file, PAKFIRE_DIGEST_SHA3_256, value, size);
			if (r)
				goto ERROR;

		// Digest: BLAKE2b512
		} else if (strcmp(attr, "PAKFIRE.digests.blake2b512") == 0) {
			r = pakfire_file_set_digest(file, PAKFIRE_DIGEST_BLAKE2B512, value, size);
			if (r)
				goto ERROR;

		// Digest: BLAKE2s256
		} else if (strcmp(attr, "PAKFIRE.digests.blake2s256") == 0) {
			r = pakfire_file_set_digest(file, PAKFIRE_DIGEST_BLAKE2S256, value, size);
			if (r)
				goto ERROR;

		// Digest: SHA-2-512
		} else if (strcmp(attr, "PAKFIRE.digests.sha2_512") == 0) {
			r = pakfire_file_set_digest(file, PAKFIRE_DIGEST_SHA2_512, value, size);
			if (r)
				goto ERROR;

		// Digest: SHA-2-256
		} else if (strcmp(attr, "PAKFIRE.digests.sha2_256") == 0) {
			r = pakfire_file_set_digest(file, PAKFIRE_DIGEST_SHA2_256, value, size);
			if (r)
				goto ERROR;

		// Capabilities
		} else if (strcmp(attr, "security.capability") == 0) {
			r = pakfire_file_read_fcaps(file, value, size);
			if (r)
				goto ERROR;

		} else {
			DEBUG(file->pakfire, "Received an unknown extended attribute: %s\n", attr);
		}
	}

ERROR:
	if (buffer)
		free(buffer);

	return r;
}

PAKFIRE_EXPORT int pakfire_file_create(struct pakfire_file** file, struct pakfire* pakfire) {
	struct pakfire_file* f = calloc(1, sizeof(*f));
	if (!f)
		return 1;

	// Store reference to Pakfire
	f->pakfire = pakfire_ref(pakfire);

	// Initialize reference counter
	f->nrefs = 1;

	*file = f;
	return 0;
}

int pakfire_file_create_from_path(struct pakfire_file** file,
		struct pakfire* pakfire, const char* path) {
	struct archive* reader = NULL;
	struct archive_entry* entry = NULL;
	int r = 1;

	// Allocate a reader
	reader = pakfire_make_archive_disk_reader(pakfire, 0);
	if (!reader)
		goto ERROR;

	// Allocate a new archive entry
	entry = archive_entry_new();
	if (!entry)
		goto ERROR;

	// Set source path
	archive_entry_copy_sourcepath(entry, path);

	// Read all file attributes from disk
	r = archive_read_disk_entry_from_file(reader, entry, -1, NULL);
	if (r) {
		ERROR(pakfire, "Could not read from %s: %m\n", path);
		goto ERROR;
	}

	// Create file
	r = pakfire_file_create_from_archive_entry(file, pakfire, entry);
	if (r)
		goto ERROR;

ERROR:
	if (r)
		ERROR(pakfire, "Could not create file from path %s: %m\n", path);
	if (entry)
		archive_entry_free(entry);
	if (reader)
		archive_read_free(reader);

	return r;
}

int pakfire_file_create_from_archive_entry(struct pakfire_file** file, struct pakfire* pakfire,
		struct archive_entry* entry) {
	int r = pakfire_file_create(file, pakfire);
	if (r)
		return r;

	// Copy archive entry
	r = pakfire_file_from_archive_entry(*file, entry);
	if (r)
		goto ERROR;

	return 0;

ERROR:
	pakfire_file_unref(*file);
	*file = NULL;

	return r;
}

struct archive_entry* pakfire_file_archive_entry(struct pakfire_file* file, int digest_types) {
	struct vfs_cap_data cap_data = {};
	const char* path = NULL;
	int r;

	struct archive_entry* entry = archive_entry_new();
	if (!entry) {
		ERROR(file->pakfire, "Could not allocate archive entry: %m\n");
		return NULL;
	}

	// Set source path
	archive_entry_copy_sourcepath(entry, file->abspath);

	// Set path
	path = pakfire_file_get_path(file);
	if (path && *path == '/') {
		archive_entry_copy_pathname(entry, path + 1);
	}

	// Set links
	if (*file->hardlink)
		archive_entry_set_hardlink(entry, file->hardlink);
	if (*file->symlink)
		archive_entry_set_symlink(entry, file->symlink);

	archive_entry_set_nlink(entry, pakfire_file_get_nlink(file));
	archive_entry_set_ino64(entry, pakfire_file_get_inode(file));
	archive_entry_set_dev(entry, pakfire_file_get_dev(file));

	// Set size
	archive_entry_set_size(entry, pakfire_file_get_size(file));

	// Set mode
	archive_entry_set_mode(entry, pakfire_file_get_mode(file));

	// Set uname
	archive_entry_set_uname(entry, pakfire_file_get_uname(file));

	// Set gname
	archive_entry_set_gname(entry, pakfire_file_get_gname(file));

	// Set times
	archive_entry_set_ctime(entry, pakfire_file_get_ctime(file), 0);
	archive_entry_set_mtime(entry, pakfire_file_get_mtime(file), 0);

	// Flags
	if (pakfire_file_has_flag(file, PAKFIRE_FILE_CONFIG)) {
		archive_entry_xattr_add_entry(entry,
			"PAKFIRE.config", "1", strlen("1"));
	}

	// Set MIME type
	const char* mimetype = pakfire_file_get_mimetype(file);
	if (mimetype) {
		archive_entry_xattr_add_entry(entry,
			"PAKFIRE.mimetype", mimetype, strlen(mimetype));
	}

	// Compute any required file digests
	r = pakfire_file_compute_digests(file, digest_types);
	if (r)
		goto ERROR;

	// Copy digests

	// SHA-3-512
	if ((digest_types && PAKFIRE_DIGEST_SHA3_512)
			&& pakfire_digest_set(file->digests.sha3_512))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.sha3_512",
			file->digests.sha3_512, sizeof(file->digests.sha3_512));

	// SHA-3-256
	if ((digest_types && PAKFIRE_DIGEST_SHA3_256) &&
			pakfire_digest_set(file->digests.sha3_256))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.sha3_256",
			file->digests.sha3_256, sizeof(file->digests.sha3_256));

	// BLAKE2b512
	if ((digest_types && PAKFIRE_DIGEST_BLAKE2B512) &&
			pakfire_digest_set(file->digests.blake2b512))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.blake2b512",
			file->digests.blake2b512, sizeof(file->digests.blake2b512));

	// BLAKE2s256
	if ((digest_types && PAKFIRE_DIGEST_BLAKE2S256) &&
			pakfire_digest_set(file->digests.blake2s256))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.blake2s256",
			file->digests.blake2s256, sizeof(file->digests.blake2s256));

	// SHA-2-512
	if ((digest_types && PAKFIRE_DIGEST_SHA2_512) &&
			pakfire_digest_set(file->digests.sha2_512))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.sha2_512",
			file->digests.sha2_512, sizeof(file->digests.sha2_512));

	// SHA-2-256
	if ((digest_types && PAKFIRE_DIGEST_SHA2_512) &&
			pakfire_digest_set(file->digests.sha2_256))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.sha2_256",
			file->digests.sha2_256, sizeof(file->digests.sha2_256));

	// Capabilities
	if (file->caps) {
		r = pakfire_file_write_fcaps(file, &cap_data);
		if (r) {
			ERROR(file->pakfire, "Could not export capabilities: %m\n");
			goto ERROR;
		}

		// Store capabilities in archive entry
		archive_entry_xattr_add_entry(entry,
			"security.capability", &cap_data, sizeof(cap_data));
	}

	return entry;

ERROR:
	if (entry)
		archive_entry_free(entry);

	return NULL;
}

static void pakfire_file_free(struct pakfire_file* file) {
	// Free capabilities
	if (file->caps)
		cap_free(file->caps);

	pakfire_unref(file->pakfire);
	free(file);
}

PAKFIRE_EXPORT struct pakfire_file* pakfire_file_ref(struct pakfire_file* file) {
	file->nrefs++;

	return file;
}

PAKFIRE_EXPORT struct pakfire_file* pakfire_file_unref(struct pakfire_file* file) {
	if (--file->nrefs > 0)
		return file;

	pakfire_file_free(file);
	return NULL;
}

/*
	Flags
*/

int pakfire_file_has_flag(struct pakfire_file* file, int flag) {
	return file->flags & flag;
}

int pakfire_file_set_flags(struct pakfire_file* file, int flag) {
	file->flags |= flag;

	return 0;
}

#define pakfire_file_strmode(file, buffer) \
	__pakfire_file_strmode(file, buffer, sizeof(buffer))

static int __pakfire_file_strmode(struct pakfire_file* file, char* s, const size_t length) {
	int r;

	static const mode_t permbits[] = {
		0400,
		0200,
		0100,
		0040,
		0020,
		0010,
		0004,
		0002,
		0001
	};

	const mode_t mode = pakfire_file_get_mode(file);
	const mode_t type = pakfire_file_get_type(file);

	// Set some default string
	r = __pakfire_string_set(s, length, "?rwxrwxrwx ");
	if (r)
		return r;

	switch (type) {
		case S_IFREG:
			s[0] = '-';
			break;

		case S_IFBLK:
			s[0] = 'b';
			break;

		case S_IFCHR:
			s[0] = 'c';
			break;

		case S_IFDIR:
			s[0] = 'd';
			break;

		case S_IFLNK:
			s[0] = 'l';
			break;

		case S_IFSOCK:
			s[0] = 's';
			break;

		case S_IFIFO:
			s[0] = 'p';
			break;

		default:
			if (*file->hardlink) {
				s[0] = 'h';
				break;
			}
	}

	for (unsigned int i = 0; i < 9; i++) {
		if (mode & permbits[i])
			continue;

		s[i+1] = '-';
	}

	if (mode & S_ISUID) {
		if (mode & 0100)
			s[3] = 's';
		else
			s[3] = 'S';
	}

	if (mode & S_ISGID) {
		if (mode & 0010)
			s[6] = 's';
		else
			s[6] = 'S';
	}

	if (mode & S_ISVTX) {
		if (mode & 0001)
			s[9] = 't';
		else
			s[9] = 'T';
	}

#if 0
	if (file->caps)
		s[10] = '+';
#endif

	return 0;
}

char* pakfire_file_dump(struct pakfire_file* file, int flags) {
	char* buffer = "";
	int r;

	char mode[12];
	char time[32];

	// Format mode
	if (flags & PAKFIRE_FILE_DUMP_MODE) {
		r = pakfire_file_strmode(file, mode);
		if (r)
			goto ERROR;

		r = asprintf(&buffer, "%s %s", buffer, mode);
		if (r < 0)
			goto ERROR;
	}

	// Format ownership
	if (flags & PAKFIRE_FILE_DUMP_OWNERSHIP) {
		r = asprintf(&buffer, "%s %s/%s", buffer, file->uname, file->gname);
		if (r < 0)
			goto ERROR;
	}

	// Format size
	if (flags & PAKFIRE_FILE_DUMP_SIZE) {
		r = asprintf(&buffer, "%s %8zu", buffer, file->st.st_size);
		if (r < 0)
			goto ERROR;
	}

	// Format time
	if (flags & PAKFIRE_FILE_DUMP_TIME) {
		r = pakfire_strftime(time, "%Y-%m-%d %H:%M", file->st.st_ctime);
		if (r)
			goto ERROR;

		r = asprintf(&buffer, "%s %s", buffer, time);
		if (r < 0)
			goto ERROR;
	}

	// Format path
	r = asprintf(&buffer, "%s %s", buffer, file->path);
	if (r < 0)
		goto ERROR;

	// Append symlink target
	if (flags & PAKFIRE_FILE_DUMP_LINK_TARGETS) {
		switch (pakfire_file_get_type(file)) {
			case S_IFLNK:
				r = asprintf(&buffer, "%s -> %s", buffer, file->symlink);
				if (r < 0)
					return NULL;

			default:
				break;
		}
	}

	// Dump Issues
	if (flags & PAKFIRE_FILE_DUMP_ISSUES) {
		if (file->issues & PAKFIRE_FILE_FHS_ERROR) {
			r = asprintf(&buffer, "%s [FHS-ERROR]", buffer);
			if (r < 0)
				goto ERROR;
		}

		if (file->issues & PAKFIRE_FILE_MISSING_DEBUGINFO) {
			r = asprintf(&buffer, "%s [MISSING-DEBUGINFO]", buffer);
			if (r < 0)
				goto ERROR;
		}

		// Stack-smashing Protection
		if (file->issues & PAKFIRE_FILE_MISSING_SSP) {
			r = asprintf(&buffer, "%s [MISSING-SSP]", buffer);
			if (r < 0)
				goto ERROR;
		}

		// Position-independent Executable
		if (file->issues & PAKFIRE_FILE_MISSING_PIE) {
			r = asprintf(&buffer, "%s [MISSING-PIE]", buffer);
			if (r < 0)
				goto ERROR;
		}

		// Executable Stack
		if (file->issues & PAKFIRE_FILE_EXECSTACK) {
			r = asprintf(&buffer, "%s [EXECSTACK]", buffer);
			if (r < 0)
				goto ERROR;
		}

		// Not RELRO
		if (file->issues & PAKFIRE_FILE_NO_RELRO) {
			r = asprintf(&buffer, "%s [NO-RELRO]", buffer);
			if (r < 0)
				goto ERROR;
		}

		// Has RUNPATH?
		if (file->issues & PAKFIRE_FILE_HAS_RUNPATH) {
			r = asprintf(&buffer, "%s [HAS-RUNPATH]", buffer);
			if (r < 0)
				goto ERROR;
		}

		// Invalid capabilities
		if (file->issues & PAKFIRE_FILE_INVALID_CAPS) {
			r = asprintf(&buffer, "%s [INVALID-CAPS]", buffer);
			if (r < 0)
				goto ERROR;
		}
	}

	return buffer;

ERROR:
	if (buffer)
		free(buffer);

	return NULL;
}

PAKFIRE_EXPORT int pakfire_file_cmp(struct pakfire_file* file1, struct pakfire_file* file2) {
	const char* path1 = pakfire_file_get_path(file1);
	const char* path2 = pakfire_file_get_path(file2);

	return strcmp(path1, path2);
}

const char* pakfire_file_get_abspath(struct pakfire_file* file) {
	return file->abspath;
}

int pakfire_file_set_abspath(struct pakfire_file* file, const char* path) {
	int r;

	// Check if path is set and absolute
	if (!path || *path != '/') {
		errno = EINVAL;
		return 1;
	}

	// Store the abspath
	r = pakfire_string_set(file->abspath, path);
	if (r)
		goto ERROR;

	// Store path if it isn't set, yet
	if (!*file->path) {
		r = pakfire_file_set_path(file, path);
		if (r)
			goto ERROR;
	}

	return r;

ERROR:
	ERROR(file->pakfire, "Could not set abspath '%s': %m\n", path);
	return r;
}

PAKFIRE_EXPORT const char* pakfire_file_get_path(struct pakfire_file* file) {
	return file->path;
}

PAKFIRE_EXPORT int pakfire_file_set_path(struct pakfire_file* file, const char* path) {
	int r = 1;

	// Check if path is set
	if (!path) {
		errno = EINVAL;
		goto ERROR;
	}

	// Strip any leading dots from paths
	if (pakfire_string_startswith(path, "./"))
		path++;

	switch (*path) {
		// Just store the path if it is absolute
		case '/':
			r = pakfire_string_set(file->path, path);
			if (r)
				goto ERROR;
			break;

		// Handle relative paths
		default:
			r = pakfire_string_format(file->path, "/%s", path);
			if (r)
				goto ERROR;
			break;
	}

	// Set abspath if it isn't set, yet
	if (!*file->abspath) {
		r = pakfire_file_set_abspath(file, file->path);
		if (r)
			goto ERROR;
	}

	return r;

ERROR:
	ERROR(file->pakfire, "Could not set path '%s': %m\n", path);
	return r;
}

PAKFIRE_EXPORT const char* pakfire_file_get_hardlink(struct pakfire_file* file) {
	if (!*file->hardlink)
		return NULL;

	return file->hardlink;
}

PAKFIRE_EXPORT void pakfire_file_set_hardlink(struct pakfire_file* file, const char* link) {
	pakfire_string_set(file->hardlink, link);
}

PAKFIRE_EXPORT const char* pakfire_file_get_symlink(struct pakfire_file* file) {
	if (!*file->symlink)
		return NULL;

	return file->symlink;
}

PAKFIRE_EXPORT void pakfire_file_set_symlink(struct pakfire_file* file, const char* link) {
	pakfire_string_set(file->symlink, link);
}

PAKFIRE_EXPORT nlink_t pakfire_file_get_nlink(struct pakfire_file* file) {
	return file->st.st_nlink;
}

PAKFIRE_EXPORT void pakfire_file_set_nlink(struct pakfire_file* file, const nlink_t nlink) {
	file->st.st_nlink = nlink;
}

PAKFIRE_EXPORT ino_t pakfire_file_get_inode(struct pakfire_file* file) {
	return file->st.st_ino;
}

PAKFIRE_EXPORT void pakfire_file_set_inode(struct pakfire_file* file, const ino_t ino) {
	file->st.st_ino = ino;
}

PAKFIRE_EXPORT dev_t pakfire_file_get_dev(struct pakfire_file* file) {
	return file->st.st_dev;
}

PAKFIRE_EXPORT void pakfire_file_set_dev(struct pakfire_file* file, const dev_t dev) {
	file->st.st_dev = dev;
}

PAKFIRE_EXPORT int pakfire_file_get_type(struct pakfire_file* file) {
	return file->st.st_mode & S_IFMT;
}

PAKFIRE_EXPORT off_t pakfire_file_get_size(struct pakfire_file* file) {
	return file->st.st_size;
}

PAKFIRE_EXPORT void pakfire_file_set_size(struct pakfire_file* file, off_t size) {
	file->st.st_size = size;
}

PAKFIRE_EXPORT const char* pakfire_file_get_uname(struct pakfire_file* file) {
	return file->uname;
}

PAKFIRE_EXPORT int pakfire_file_set_uname(struct pakfire_file* file, const char* uname) {
	return pakfire_string_set(file->uname, uname);
}

PAKFIRE_EXPORT const char* pakfire_file_get_gname(struct pakfire_file* file) {
	return file->gname;
}

PAKFIRE_EXPORT int pakfire_file_set_gname(struct pakfire_file* file, const char* gname) {
	return pakfire_string_set(file->gname, gname);
}

PAKFIRE_EXPORT mode_t pakfire_file_get_mode(struct pakfire_file* file) {
	return file->st.st_mode;
}

PAKFIRE_EXPORT void pakfire_file_set_mode(struct pakfire_file* file, mode_t mode) {
	file->st.st_mode = mode;
}

PAKFIRE_EXPORT mode_t pakfire_file_get_perms(struct pakfire_file* file) {
	return file->st.st_mode & ~S_IFMT;
}

PAKFIRE_EXPORT void pakfire_file_set_perms(struct pakfire_file* file, const mode_t perms) {
	// Clear any previous permissions
	file->st.st_mode &= S_IFMT;

	// Set new bits (with format cleared)
	file->st.st_mode |= ~S_IFMT & perms;
}

static int pakfire_file_is_executable(struct pakfire_file* file) {
	return file->st.st_mode & (S_IXUSR|S_IXGRP|S_IXOTH);
}

PAKFIRE_EXPORT time_t pakfire_file_get_ctime(struct pakfire_file* file) {
	return file->st.st_ctime;
}

PAKFIRE_EXPORT void pakfire_file_set_ctime(struct pakfire_file* file, time_t time) {
	file->st.st_ctime = time;
}

PAKFIRE_EXPORT time_t pakfire_file_get_mtime(struct pakfire_file* file) {
	return file->st.st_mtime;
}

PAKFIRE_EXPORT void pakfire_file_set_mtime(struct pakfire_file* file, time_t time) {
	file->st.st_mtime = time;
}

PAKFIRE_EXPORT const unsigned char* pakfire_file_get_digest(
		struct pakfire_file* file, const enum pakfire_digest_types type, size_t* length) {

	switch (type) {
		case PAKFIRE_DIGEST_SHA3_512:
			if (!pakfire_digest_set(file->digests.sha3_512))
				return NULL;

			if (length)
				*length = sizeof(file->digests.sha3_512);

			return file->digests.sha3_512;

		case PAKFIRE_DIGEST_SHA3_256:
			if (!pakfire_digest_set(file->digests.sha3_256))
				return NULL;

			if (length)
				*length = sizeof(file->digests.sha3_256);

			return file->digests.sha3_256;

		case PAKFIRE_DIGEST_BLAKE2B512:
			if (!pakfire_digest_set(file->digests.blake2b512))
				return NULL;

			if (length)
				*length = sizeof(file->digests.blake2b512);

			return file->digests.blake2b512;

		case PAKFIRE_DIGEST_BLAKE2S256:
			if (!pakfire_digest_set(file->digests.blake2s256))
				return NULL;

			if (length)
				*length = sizeof(file->digests.blake2s256);

			return file->digests.blake2s256;

		case PAKFIRE_DIGEST_SHA2_512:
			if (!pakfire_digest_set(file->digests.sha2_512))
				return NULL;

			if (length)
				*length = sizeof(file->digests.sha2_512);

			return file->digests.sha2_512;

		case PAKFIRE_DIGEST_SHA2_256:
			if (!pakfire_digest_set(file->digests.sha2_256))
				return NULL;

			if (length)
				*length = sizeof(file->digests.sha2_256);

			return file->digests.sha2_256;

		case PAKFIRE_DIGEST_UNDEFINED:
			break;
	}

	return NULL;
}

PAKFIRE_EXPORT int pakfire_file_set_digest(struct pakfire_file* file,
		const enum pakfire_digest_types type, const unsigned char* digest, const size_t length) {
	if (!digest) {
		errno = EINVAL;
		return 1;
	}

	// Check buffer length
	if (pakfire_digest_length(type) != length) {
		ERROR(file->pakfire, "Digest has an incorrect length of %zu byte(s)\n", length);
		errno = ENOMSG;
		return 1;
	}

	// Store the digest
	switch (type) {
		case PAKFIRE_DIGEST_SHA3_512:
			memcpy(file->digests.sha3_512, digest, sizeof(file->digests.sha3_512));
			break;

		case PAKFIRE_DIGEST_SHA3_256:
			memcpy(file->digests.sha3_256, digest, sizeof(file->digests.sha3_256));
			break;

		case PAKFIRE_DIGEST_BLAKE2B512:
			memcpy(file->digests.blake2b512, digest, sizeof(file->digests.blake2b512));
			break;

		case PAKFIRE_DIGEST_BLAKE2S256:
			memcpy(file->digests.blake2s256, digest, sizeof(file->digests.blake2s256));
			break;

		case PAKFIRE_DIGEST_SHA2_512:
			memcpy(file->digests.sha2_512, digest, sizeof(file->digests.sha2_512));
			break;

		case PAKFIRE_DIGEST_SHA2_256:
			memcpy(file->digests.sha2_256, digest, sizeof(file->digests.sha2_256));
			break;

		case PAKFIRE_DIGEST_UNDEFINED:
			errno = ENOTSUP;
			return 1;
	}

	return 0;
}

PAKFIRE_EXPORT int pakfire_file_has_caps(struct pakfire_file* file) {
	if (file->caps)
		return 1;

	return 0;
}

PAKFIRE_EXPORT char* pakfire_file_get_caps(struct pakfire_file* file) {
	char* copy = NULL;
	char* text = NULL;
	ssize_t length = 0;

	if (file->caps) {
		text = cap_to_text(file->caps, &length);
		if (!text) {
			ERROR(file->pakfire, "Could not export capabilities: %m\n");
			goto ERROR;
		}

		// libcap is being weird and uses its own allocator so we have to copy the string
		copy = strndup(text, length);
		if (!copy)
			goto ERROR;
	}

ERROR:
	if (text)
		cap_free(text);

	return copy;
}

static int pakfire_file_levels(struct pakfire_file* file) {
	if (!*file->path)
		return 0;

	int levels = 0;

	for (char* p = file->path; *p; p++) {
		if (*p == '/')
			levels++;
	}

	return levels;
}

FILE* pakfire_file_open(struct pakfire_file* file) {
	FILE* f = fopen(file->abspath, "r");
	if (!f)
		ERROR(file->pakfire, "Could not open %s: %m\n", file->abspath);

	return f;
}

int pakfire_file_payload_matches(struct pakfire_file* file,
		const void* needle, const size_t length) {
	char buffer[1024 * 1024];
	FILE* f = NULL;
	void* p = NULL;
	int r;

	// Only run for regular files
	if (!S_ISREG(file->st.st_mode))
		return 0;

	// Skip empty files
	if (!file->st.st_size)
		return 0;

	// Open the file
	f = pakfire_file_open(file);
	if (!f) {
		r = 1;
		goto ERROR;
	}

	while (!feof(f)) {
		size_t bytes_read = fread(buffer, 1, sizeof(buffer), f);

		// Raise any reading errors
		if (ferror(f)) {
			r = 1;
			goto ERROR;
		}

		// Search for the needle
		p = memmem(buffer, bytes_read, needle, length);
		if (p) {
			r = 1;
			goto ERROR;
		}
	}

	// No match
	r = 0;

ERROR:
	if (f)
		fclose(f);

	return r;
}

static int __pakfire_file_compute_digests(struct pakfire_file* file,
		struct pakfire_digests* digests, const int types) {
	FILE* f = NULL;
	int r = 1;

	// Skip this for anything that isn't a regular file
	if (!S_ISREG(file->st.st_mode))
		return 0;

	// Reset digests
	pakfire_digests_reset(digests, types);

	// Open the file
	f = pakfire_file_open(file);
	if (!f)
		goto ERROR;

	// Compute digests
	r = pakfire_digests_compute_from_file(file->pakfire, digests, types, f);
	if (r)
		goto ERROR;

ERROR:
	if (f)
		fclose(f);

	return r;
}

int pakfire_file_compute_digests(struct pakfire_file* file, const int types) {
	return __pakfire_file_compute_digests(file, &file->digests, types);
}

static int pakfire_file_remove(struct pakfire_file* file) {
	int r;

	if (!*file->abspath) {
		errno = EINVAL;
		return 1;
	}

	DEBUG(file->pakfire, "Removing %s...\n", file->path);

	switch (file->st.st_mode & S_IFMT) {
		// Call rmdir() for directories
		case S_IFDIR:
			r = rmdir(file->abspath);
			if (r) {
				switch (errno) {
					// Ignore when the directory is not empty
					case ENOTEMPTY:
						r = 0;
						break;

					// Ignore if the directory didn't exist
					case ENOENT:
						r = 0;
						break;

					// Ignore if the directory changed type
					case ENOTDIR:
						r = 0;
						break;

					// Log anything else
					default:
						ERROR(file->pakfire, "Could not remove directory %s: %m\n", file->path);
						break;
				}
			}
			break;

		// Call unlink() for everything else
		default:
			r = unlink(file->abspath);
			if (r) {
				switch (errno) {
					// Ignore if the file didn't exist
					case ENOENT:
						r = 0;
						break;

					default:
						ERROR(file->pakfire, "Could not remove %s: %m\n", file->path);
						break;
				}
			}
			break;
	}

	return r;
}

int pakfire_file_symlink_target_exists(struct pakfire_file* file) {
	// Fail if this got called for anything that isn't a symlink
	if (!S_ISLNK(file->st.st_mode)) {
		errno = EINVAL;
		return -1;
	}

	return pakfire_path_exists(file->abspath);
}

/*
	MIME Type
*/

int pakfire_file_detect_mimetype(struct pakfire_file* file) {
	// Only process regular files
	if (!S_ISREG(file->st.st_mode))
		return 0;

	// Skip if MIME type is already set
	if (*file->mimetype)
		return 0;

	// Fetch the magic cookie
	magic_t magic = pakfire_get_magic(file->pakfire);
	if (!magic)
		return 1;

	// Check the file
	const char* mimetype = magic_file(magic, file->abspath);
	if (!mimetype) {
		ERROR(file->pakfire, "Could not classify %s: %s\n", file->path, magic_error(magic));
		return 1;
	}

	DEBUG(file->pakfire, "Classified %s as %s\n", file->path, mimetype);

	// Store the value
	return pakfire_file_set_mimetype(file, mimetype);
}

PAKFIRE_EXPORT const char* pakfire_file_get_mimetype(struct pakfire_file* file) {
	// Return nothing on an empty mimetype
	if (!*file->mimetype)
		return NULL;

	return file->mimetype;
}

PAKFIRE_EXPORT int pakfire_file_set_mimetype(
		struct pakfire_file* file, const char* mimetype) {
	// Store the value
	return pakfire_string_set(file->mimetype, mimetype);
}

/*
	Classification
*/

static int setup_libelf(struct pakfire* pakfire) {
	// Initialize libelf
	if (elf_version(EV_CURRENT) == EV_NONE) {
		ERROR(pakfire, "Could not initialize libelf: %s\n", elf_errmsg(-1));

		return 1;
	}

	return 0;
}

static int pakfire_file_classify_mode(struct pakfire_file* file) {
	// Check for regular files
	if (S_ISREG(file->st.st_mode)) {
		file->class |= PAKFIRE_FILE_REGULAR;

		// Does the file have executable permissions?
		if (file->st.st_mode & (S_IXUSR|S_IXGRP|S_IXOTH))
			file->class |= PAKFIRE_FILE_EXECUTABLE;

	// Check for directories
	} else if (S_ISDIR(file->st.st_mode))
		file->class |= PAKFIRE_FILE_DIRECTORY;

	// Check for symlinks
	else if (S_ISLNK(file->st.st_mode))
		file->class |= PAKFIRE_FILE_SYMLINK;

	// Check for character devices
	else if (S_ISCHR(file->st.st_mode))
		file->class |= PAKFIRE_FILE_CHARACTER;

	// Check for block devices
	else if (S_ISBLK(file->st.st_mode))
		file->class |= PAKFIRE_FILE_BLOCK;

	// Check for FIFO pipes
	else if (S_ISFIFO(file->st.st_mode))
		file->class |= PAKFIRE_FILE_FIFO;

	// Check for sockets
	else if (S_ISSOCK(file->st.st_mode))
		file->class |= PAKFIRE_FILE_SOCKET;

	return 0;
}

static const struct pattern {
	const char* pattern;
	int class;
} patterns[] = {
	{ "*.a",  PAKFIRE_FILE_STATIC_LIBRARY },
	{ "*.la", PAKFIRE_FILE_LIBTOOL_ARCHIVE },
	{ "*.pm", PAKFIRE_FILE_PERL },
	{ "*.pc", PAKFIRE_FILE_PKGCONFIG },
	{ "/usr/lib/firmware/*", PAKFIRE_FILE_FIRMWARE },
	{ "/usr/lib/grub/*", PAKFIRE_FILE_FIRMWARE },
	{ "/usr/lib*/ld-*.so*", PAKFIRE_FILE_RUNTIME_LINKER },
	{ NULL },
};

static int pakfire_file_classify_pattern(struct pakfire_file* file) {
	for (const struct pattern* p = patterns; p->pattern; p++) {
		if (pakfire_file_matches(file, p->pattern)) {
			file->class |= p->class;
			break;
		}
	}

	return 0;
}

static const struct mimetype {
	const char* mimetype;
	int class;
} mimetypes[] = {
 	{ "text/x-perl", PAKFIRE_FILE_PERL },
	{ NULL, 0 },
};

static int pakfire_file_classify_magic(struct pakfire_file* file) {
	int r;

	// Detect the MIME type
	r = pakfire_file_detect_mimetype(file);
	if (r)
		return r;

	// Fetch the MIME type
	const char* mimetype = pakfire_file_get_mimetype(file);
	if (!mimetype)
		return 1;

	// Match the MIME type with a flag
	for (const struct mimetype* m = mimetypes; m->mimetype; m++) {
		if (strcmp(m->mimetype, mimetype) == 0) {
			file->class |= m->class;
			break;
		}
	}

	return 0;
}

static int pakfire_file_classify_elf(struct pakfire_file* file) {
	FILE* f = NULL;
	Elf* elf = NULL;
	int r;

	// Don't run this if we already know that file is an ELF file
	if (file->class & PAKFIRE_FILE_ELF)
		return 0;

	// Setup libelf
	r = setup_libelf(file->pakfire);
	if (r)
		return r;

	// Open the file
	f = fopen(file->abspath, "r");
	if (!f) {
		ERROR(file->pakfire, "Could not open %s: %m\n", file->path);
		return 1;
	}

	// Try to open the ELF file
	elf = elf_begin(fileno(f), ELF_C_READ, NULL);
	if (!elf) {
		// We fail silently here, because this file might be in a different format
		goto ERROR;
	}

	switch (elf_kind(elf)) {
		// Mark this file as an ELF file
		case ELF_K_ELF:
			file->class |= PAKFIRE_FILE_ELF;
			break;

		// Ignore everything else
		default:
			break;
	}

ERROR:
	if (elf)
		elf_end(elf);
	if (f)
		fclose(f);

	return 0;
}

int pakfire_file_classify(struct pakfire_file* file) {
	int r;

	if (!file->class) {
		// First, check the mode so that we won't run magic on directories, symlinks, ...
		r = pakfire_file_classify_mode(file);
		if (r)
			goto ERROR;

		// Only run this for regular files
		if (file->class & PAKFIRE_FILE_REGULAR) {
			// Then check for patterns
			r = pakfire_file_classify_pattern(file);
			if (r)
				goto ERROR;

			// After that, we will use libmagic...
			r = pakfire_file_classify_magic(file);
			if (r)
				goto ERROR;

			// Check if the file is an ELF file
			r = pakfire_file_classify_elf(file);
			if (r)
				goto ERROR;
		}
	}

	return file->class;

ERROR:
	// Reset the class
	file->class = PAKFIRE_FILE_UNKNOWN;

	return r;
}

int pakfire_file_matches_class(struct pakfire_file* file, const int class) {
	return pakfire_file_classify(file) & class;
}

/*
	This function tries to remove the file after it has been packaged.

	It will try to delete any parent directories as well and ignore if directories
	cannot be deleted because they might contain other files
*/
int pakfire_file_cleanup(struct pakfire_file* file, int flags) {
	char path[PATH_MAX];

	// Try removing the file
	int r = pakfire_file_remove(file);
	if (r)
		return r;

	// Try to tidy up afterwards
	if (flags & PAKFIRE_FILE_CLEANUP_TIDY) {
		// Create a working copy of abspath
		r = pakfire_string_set(path, file->abspath);
		if (r)
			return r;

		// See how many levels this file has
		int levels = pakfire_file_levels(file);

		// Walk all the way up and remove all parent directories if possible
		while (--levels) {
			dirname(path);

			// Break if path is suddenly empty
			if (!*path)
				break;

			DEBUG(file->pakfire, "Trying to remove parent directory %s\n", path);

			r = rmdir(path);

			// Break on any error
			if (r)
				break;
		}
	}

	return 0;
}

static int pakfire_file_verify_mode(struct pakfire_file* file, const struct stat* st) {
	const mode_t type = pakfire_file_get_type(file);

	// Did the type change?
	if (type != (st->st_mode & S_IFMT)) {
		file->verify_status |= PAKFIRE_FILE_TYPE_CHANGED;

		DEBUG(file->pakfire, "%s: File Type changed\n", file->path);
	}

	const mode_t perms = pakfire_file_get_perms(file);

	// Check permissions
	if (perms != (st->st_mode & 0777)) {
		file->verify_status |= PAKFIRE_FILE_PERMISSIONS_CHANGED;

		DEBUG(file->pakfire, "%s: Permissions changed\n", file->path);
	}

#if 0
	// XXX This does not check what it is supposed to check

	// Check if device node changed
	if (S_ISCHR(st->st_mode) || S_ISBLK(st->st_mode)) {
		const dev_t dev = pakfire_file_get_dev(file);

		if (dev != st->st_dev) {
			file->verify_status |= PAKFIRE_FILE_DEV_CHANGED;

			DEBUG(file->pakfire, "%s: Device Node changed\n", file->path);
		}
	}
#endif

	return 0;
}

static int pakfire_file_verify_size(struct pakfire_file* file, const struct stat* st) {
	// Nothing to do if size matches
	if (file->st.st_size == st->st_size)
		return 0;

	// Size differs
	file->verify_status |= PAKFIRE_FILE_SIZE_CHANGED;

	DEBUG(file->pakfire, "%s: Filesize differs (expected %zu, got %zu byte(s))\n",
		file->path, file->st.st_size, st->st_size);

	return 0;
}

static int pakfire_file_verify_ownership(struct pakfire_file* file, const struct stat* st) {
	// Fetch UID/GID
#if 0
	const uid_t uid = pakfire_unmap_id(file->pakfire, st->st_uid);
	const gid_t gid = pakfire_unmap_id(file->pakfire, st->st_gid);
#else
	const uid_t uid = st->st_uid;
	const gid_t gid = st->st_gid;
#endif

	// Fetch owner & group
	struct passwd* owner = pakfire_getpwnam(file->pakfire, file->uname);
	struct group*  group = pakfire_getgrnam(file->pakfire, file->gname);

	// Check if owner matches
	if (!owner || owner->pw_uid != uid) {
		file->verify_status |= PAKFIRE_FILE_OWNER_CHANGED;

		DEBUG(file->pakfire, "%s: Owner differs\n", file->path);
	}

	// Check if group matches
	if (!group || group->gr_gid != gid) {
		file->verify_status |= PAKFIRE_FILE_GROUP_CHANGED;

		DEBUG(file->pakfire, "%s: Group differs\n", file->path);
	}

	return 0;
}

static int pakfire_file_verify_timestamps(struct pakfire_file* file, const struct stat* st) {
	// Check creation time
	if (file->st.st_ctime != st->st_ctime) {
		file->verify_status |= PAKFIRE_FILE_CTIME_CHANGED;

		DEBUG(file->pakfire, "%s: Creation time changed\n", file->path);
	}

	// Check modification time
	if (file->st.st_mtime != st->st_mtime) {
		file->verify_status |= PAKFIRE_FILE_MTIME_CHANGED;

		DEBUG(file->pakfire, "%s: Modification time changed\n", file->path);
	}

	return 0;
}

static int pakfire_file_verify_payload(struct pakfire_file* file, const struct stat* st) {
	int r;

	struct pakfire_digests computed_digests;
	int digest_types = PAKFIRE_DIGEST_UNDEFINED;

	// Nothing to do for anything that isn't a regular file
	if (!S_ISREG(st->st_mode))
		return 0;

	// Fast-path if size changed. The payload will have changed, too
	if (file->verify_status & PAKFIRE_FILE_SIZE_CHANGED) {
		file->verify_status |= PAKFIRE_FILE_PAYLOAD_CHANGED;
		return 0;
	}

	// Check if this file has any digests at all
	digest_types = pakfire_digest_has_any(&file->digests);

	if (!digest_types) {
		ERROR(file->pakfire, "%s: No digests available\n", file->path);
		return 0;
	}

	// Compute digests
	r = __pakfire_file_compute_digests(file, &computed_digests, digest_types);
	if (r)
		goto ERROR;

	// Compare digests
	r = pakfire_digests_compare(file->pakfire, &file->digests, &computed_digests, digest_types);
	if (r) {
		file->verify_status |= PAKFIRE_FILE_PAYLOAD_CHANGED;

		DEBUG(file->pakfire, "%s: Digest(s) do not match\n", file->path);
	}

ERROR:
	return r;
}

/*
	Verify the file - i.e. does the metadata match what is on disk?
*/
int pakfire_file_verify(struct pakfire_file* file, int* status) {
	struct stat st;
	int r;

	DEBUG(file->pakfire, "Verifying %s...\n", file->path);

	// stat() the file
	r = lstat(file->abspath, &st);
	if (r) {
		// File does not exist
		if (errno == ENOENT) {
			file->verify_status |= PAKFIRE_FILE_NOENT;
			return 1;
		}

		// Raise any other errors from stat()
		return r;
	}

	// Verify mode
	r = pakfire_file_verify_mode(file, &st);
	if (r)
		return r;

	// Verify size
	r = pakfire_file_verify_size(file, &st);
	if (r)
		return r;

	// Verify ownership
	r = pakfire_file_verify_ownership(file, &st);
	if (r)
		return r;

	// Verify timestamps
	r = pakfire_file_verify_timestamps(file, &st);
	if (r)
		return r;

	// Verify payload
	r = pakfire_file_verify_payload(file, &st);
	if (r)
		return r;

	return 0;
}

PAKFIRE_EXPORT int pakfire_file_matches(struct pakfire_file* file, const char* pattern) {
	int r;

	// Don't match on no pattern
	if (!pattern)
		return 0;

	// Check if the pattern matches
	r = fnmatch(pattern, file->path, 0);
	switch (r) {
		// Match
		case 0:
			return 1;

		// No Match
		case FNM_NOMATCH:
			return 0;

		default:
			return -1;
	}
}

/*
	ELF Stuff
*/

static int pakfire_file_open_elf(struct pakfire_file* file,
		int (*callback)(struct pakfire_file* file, Elf* elf, void* data), void* data) {
	FILE* f = NULL;
	Elf* elf = NULL;
	int r;

	// Don't run this for non-ELF files
	if (!pakfire_file_matches_class(file, PAKFIRE_FILE_ELF)) {
		errno = EINVAL;
		return 1;
	}

	// Setup libelf
	r = setup_libelf(file->pakfire);
	if (r)
		return r;

	// Open the file
	f = fopen(file->abspath, "r");
	if (!f) {
		ERROR(file->pakfire, "Could not open %s: %m\n", file->abspath);
		return 1;
	}

	// Parse the ELF header
	elf = elf_begin(fileno(f), ELF_C_READ, NULL);
	if (!elf) {
		ERROR(file->pakfire, "Could not open ELF file: %s\n", elf_errmsg(-1));
		r = 1;
		goto ERROR;
	}

	// Check if this is an ELF file
	switch (elf_kind(elf)) {
		case ELF_K_ELF:
			break;

		default:
			ERROR(file->pakfire, "%s is not an ELF object\n", file->path);
			r = 1;
			goto ERROR;
	}

	// Call the callback
	r = callback(file, elf, data);

ERROR:
	if (elf)
		elf_end(elf);
	if (f)
		fclose(f);

	return r;
}

static int pakfire_file_get_elf_section(struct pakfire_file* file,
		Elf* elf, const Elf64_Word type, Elf_Scn** section, GElf_Shdr* header, Elf_Data** data) {
	Elf_Scn* s = NULL;

	GElf_Shdr shdr;

	// Walk through all sections
	for (;;) {
		s = elf_nextscn(elf, s);
		if (!s)
			break;

		// Fetch the section header
		gelf_getshdr(s, &shdr);

		// Return any matching sections
		if (shdr.sh_type == type) {
			*section = s;

			// Send header if requested
			if (header)
				gelf_getshdr(s, header);

			// Send data if requested
			if (data)
				*data = elf_getdata(s, NULL);

			return 0;
		}
	}

	// No section found
	return 1;
}

static int __pakfire_file_get_elf_type(struct pakfire_file* file, Elf* elf, void* data) {
	int* type = (int*)data;
	GElf_Ehdr ehdr;

	// Fetch the ELF header
	if (!gelf_getehdr(elf, &ehdr)) {
		ERROR(file->pakfire, "Could not parse ELF header: %s\n", elf_errmsg(-1));
		return 1;
	}

	// Store the type
	*type = ehdr.e_type;

	return 0;
}

static int pakfire_file_get_elf_type(struct pakfire_file* file) {
	int type = ET_NONE;
	int r;

	r = pakfire_file_open_elf(file, __pakfire_file_get_elf_type, &type);
	if (r)
		return -1;

	return type;
}

static int pakfire_file_elf_dyn_walk(struct pakfire_file* file, Elf* elf,
		int (*callback)(struct pakfire_file* file,
			Elf* elf, const GElf_Shdr* shdr, const GElf_Dyn* dyn, void* data),
		void* data) {
	Elf_Scn* dynamic = NULL;
	GElf_Shdr shdr;
	Elf_Data* elf_data = NULL;
	GElf_Dyn dyn;
	int r;

	// Find the dynamic linking information
	r = pakfire_file_get_elf_section(file, elf, SHT_DYNAMIC, &dynamic, &shdr, &elf_data);
	if (r) {
		ERROR(file->pakfire, "%s does not have a dynamic section\n", file->path);
		return 1;
	}

	// Walk through all entries...
	for (unsigned int i = 0; ; i++)  {
		// Fetch the next entry
		if (!gelf_getdyn(elf_data, i, &dyn))
			break;

		// Call the callback
		r = callback(file, elf, &shdr, &dyn, data);
		if (r)
			return r;
	}

	return 0;
}

static int __pakfire_file_check_debuginfo(struct pakfire_file* file, Elf* elf, void* data) {
	Elf_Scn* symtab = NULL;
	int r;

	// Fetch the symbol table
	r = pakfire_file_get_elf_section(file, elf, SHT_SYMTAB, &symtab, NULL, NULL);

	// Not found
	if (r) {
		DEBUG(file->pakfire, "%s has no debug sections\n", file->path);

		// Store the result
		file->issues |= PAKFIRE_FILE_MISSING_DEBUGINFO;
	}

	return 0;
}

static int pakfire_file_check_debuginfo(struct pakfire_file* file) {
	switch (pakfire_file_get_elf_type(file)) {
		// Do not check Relocatable Objects
		case ET_REL:
			return 0;

		// Check everything else
		default:
			break;
	}

	return pakfire_file_open_elf(file, __pakfire_file_check_debuginfo, NULL);
}

static int __pakfire_file_check_ssp(
		struct pakfire_file* file, Elf* elf, void* data) {
	Elf_Scn* symtab = NULL;
	GElf_Shdr shdr;
	Elf_Data* elf_data = NULL;
	GElf_Sym symbol;
	const char* name = NULL;
	int r;

	// Fetch the symbol table
	r = pakfire_file_get_elf_section(file, elf, SHT_SYMTAB, &symtab, &shdr, &elf_data);
	if (r) {
		ERROR(file->pakfire, "%s has no symbol table\n", file->path);
		return 1;
	}

	// Count any global functions
	size_t counter = 0;

	// Walk through all symbols
	for (unsigned int i = 0; i < shdr.sh_size / shdr.sh_entsize; i++) {
		gelf_getsym(elf_data, i, &symbol);

		// Fetch the symbol name
		name = elf_strptr(elf, shdr.sh_link, symbol.st_name);

		// Skip empty section names
		if (!name || !*name)
			continue;

		// Exit if there is a symbol called "__stack_chk_fail"
		if (pakfire_string_startswith(name, "__stack_chk_fail"))
			return 0;

		// Count any global functions
		if ((ELF64_ST_BIND(symbol.st_info) == STB_GLOBAL) &&
				(ELF64_ST_TYPE(symbol.st_info) == STT_FUNC))
			counter++;
	}

	// We do not perform the check for libraries that do not contain any functions.
	// Some packages use shared libraries to provide data.
	if (!counter) {
		DEBUG(file->pakfire, "%s: File has no functions. Skipping SSP check.\n", file->path);
		return 0;
	}

	// The file does not seem to have SSP enabled
	file->issues |= PAKFIRE_FILE_MISSING_SSP;

	return 0;
}

static int pakfire_file_check_ssp(struct pakfire_file* file) {
	// This check will be skipped for these files
	static const char* whitelist[] = {
		"/usr/lib64/libgcc_s.so.*",
		"/usr/lib64/libmvec.so.*",
		NULL,
	};

	// Do not perform this check for runtime linkers
	if (pakfire_file_matches_class(file, PAKFIRE_FILE_RUNTIME_LINKER))
		return 0;

	// We cannot perform this check if we don't have debuginfo
	if (file->issues & PAKFIRE_FILE_MISSING_DEBUGINFO)
		return 0;

	// Check if this file is whitelisted
	for (const char** path = whitelist; *path; path++) {
		if (pakfire_file_matches(file, *path)) {
			DEBUG(file->pakfire, "Skipping SSP check for whitelisted file %s\n",
				pakfire_file_get_path(file));
			return 0;
		}
	}

	return pakfire_file_open_elf(file, __pakfire_file_check_ssp, NULL);
}

static int pakfire_file_check_pie(struct pakfire_file* file) {
	switch (pakfire_file_get_elf_type(file)) {
		// Shared Object files are good
		case ET_DYN:
			break;

		// Everything else is bad
		default:
			file->issues |= PAKFIRE_FILE_MISSING_PIE;
			break;
	}

	return 0;
}

static int __pakfire_file_check_execstack(
		struct pakfire_file* file, Elf* elf, void* data) {
	GElf_Phdr phdr;
	int r;

	size_t phnum = 0;

	// Fetch the total numbers of program headers
	r = elf_getphdrnum(elf, &phnum);
	if (r) {
		ERROR(file->pakfire, "Could not fetch number of program headers: %s\n",
			elf_errmsg(-1));
		return 1;
	}

	// Walk through all program headers
	for (unsigned int i = 0; i < phnum; i++) {
		if (!gelf_getphdr(elf, i, &phdr)) {
			ERROR(file->pakfire, "Could not parse program header: %s\n", elf_errmsg(-1));
			return 1;
		}

		switch (phdr.p_type) {
			case PT_GNU_STACK:
				DEBUG(file->pakfire,
					"%s: GNU_STACK flags: %c%c%c\n",
					file->path,
					(phdr.p_flags & PF_R) ? 'R' : '-',
					(phdr.p_flags & PF_W) ? 'W' : '-',
					(phdr.p_flags & PF_X) ? 'X' : '-'
				);

				// The stack cannot be writable and executable
				if ((phdr.p_flags & PF_W) && (phdr.p_flags & PF_X))
					file->issues |= PAKFIRE_FILE_EXECSTACK;

				// Done
				return 0;

			default:
				break;
		}
	}

	return 0;
}

static int pakfire_file_check_execstack(struct pakfire_file* file) {
	return pakfire_file_open_elf(file, __pakfire_file_check_execstack, NULL);
}

static int __pakfire_file_process_bind_now(struct pakfire_file* file,
		Elf* elf, const GElf_Shdr* shdr, const GElf_Dyn* dyn, void* data) {
	int* has_bind_now = (int*)data;

	switch (dyn->d_tag) {
		case DT_BIND_NOW:
			*has_bind_now = 1;
			break;

		case DT_FLAGS:
			if (dyn->d_un.d_val & DF_BIND_NOW)
				*has_bind_now = 1;
			break;

		case DT_FLAGS_1:
			if (dyn->d_un.d_val & DF_1_NOW)
				*has_bind_now = 1;
			break;

		default:
			break;
	}

	return 0;
}

static int __pakfire_file_check_relro(
		struct pakfire_file* file, Elf* elf, void* data) {
	int has_bind_now = 0;
	GElf_Phdr phdr;
	int r;

	// Check if we have BIND_NOW
	r = pakfire_file_elf_dyn_walk(file, elf,
		__pakfire_file_process_bind_now, &has_bind_now);
	if (r)
		return r;

	// We are not fully RELRO
	if (!has_bind_now) {
		file->issues |= PAKFIRE_FILE_NO_RELRO;

		return 0;
	}

	// Walk through all program headers
	for (unsigned int i = 0;; i++) {
		if (!gelf_getphdr(elf, i, &phdr))
			break;

		switch (phdr.p_type) {
			case PT_GNU_RELRO:
				return 0;

			default:
				break;
		}
	}

	// This file does not seem to have PT_GNU_RELRO set
	file->issues |= PAKFIRE_FILE_NO_RELRO;

	return 0;
}

static int pakfire_file_check_relro(struct pakfire_file* file) {
	return pakfire_file_open_elf(file, __pakfire_file_check_relro, NULL);
}

/*
	RPATH/RUNPATH
*/
static int __pakfire_file_process_runpath(struct pakfire_file* file,
		Elf* elf, const GElf_Shdr* shdr, const GElf_Dyn* dyn, void* data) {
	const char* value = NULL;
	const char* runpath = NULL;
	char buffer[PATH_MAX];
	char* p = NULL;
	int r;

	switch (dyn->d_tag) {
		case DT_RUNPATH:
		case DT_RPATH:
			// Fetch the value
			value = elf_strptr(elf, shdr->sh_link, dyn->d_un.d_val);
			if (!value)
				return 1;

			DEBUG(file->pakfire, "%s has a RUNPATH: %s\n", file->path, value);

			// Copy the value into a buffer we can modify
			r = pakfire_string_set(buffer, value);
			if (r)
				return r;

			// Split the value by :
			runpath = strtok_r(buffer, ":", &p);

			// Iterate over all elements
			while (runpath) {
				ERROR(file->pakfire, "Checking RUNPATH %s\n", runpath);

				// We do not allow any relative RUNPATHs
				if (pakfire_path_match(runpath, "**/../**"))
					goto RUNPATH_DENIED;

				// We allow /usr/lib64 as libtool seems to link it in quite a lot
				if (pakfire_path_match("/usr/lib64", runpath))
					goto RUNPATH_PERMITTED;

				// We allow any subdirectories of /usr/lib64
				if (pakfire_path_match( "/usr/lib64/**", runpath))
					goto RUNPATH_PERMITTED;

RUNPATH_DENIED:
				// If we make it here, this check has failed
				file->issues |= PAKFIRE_FILE_HAS_RUNPATH;
				break;

RUNPATH_PERMITTED:
				// Move on to the next RUNPATH
				runpath = strtok_r(NULL, ":", &p);
			}

		default:
			break;
	}

	return 0;
}

static int __pakfire_file_check_runpath(struct pakfire_file* file, Elf* elf, void* data) {
	return pakfire_file_elf_dyn_walk(file, elf, __pakfire_file_process_runpath, data);
}

static int pakfire_file_check_runpath(struct pakfire_file* file) {
	return pakfire_file_open_elf(file, __pakfire_file_check_runpath, NULL);
}

static int pakfire_file_check_capabilities(struct pakfire_file* file) {
	// Files cannot have capabilities but not be executable
	if (!pakfire_file_is_executable(file) && pakfire_file_has_caps(file))
		file->issues |= PAKFIRE_FILE_INVALID_CAPS;

	return 0;
}


int pakfire_file_check(struct pakfire_file* file, int* issues) {
	int r;

	// Return previous result if this has been run before
	if (!file->check_done) {
		// Perform FHS check
		r = pakfire_fhs_check_file(file->pakfire, file);
		if (r)
			file->issues |= PAKFIRE_FILE_FHS_ERROR;

		// Perform capability check
		r = pakfire_file_check_capabilities(file);
		if (r)
			return r;

		// Do not perform the following checks on firmware
		if (pakfire_file_matches_class(file, PAKFIRE_FILE_FIRMWARE))
			goto DONE;

		// Run these checks only for ELF files
		if (pakfire_file_matches_class(file, PAKFIRE_FILE_ELF)) {
			switch (pakfire_file_get_elf_type(file)) {
				// Do not check Relocatable Objects
				case ET_REL:
					goto DONE;

				// Check everything else
				default:
					break;
			}

			// Check if the file has debug info
			r = pakfire_file_check_debuginfo(file);
			if (r)
				return r;

			// Check for SSP
			r = pakfire_file_check_ssp(file);
			if (r)
				return r;

			// Check for PIE
			r = pakfire_file_check_pie(file);
			if (r)
				return r;

			// Check for executable stacks
			r = pakfire_file_check_execstack(file);
			if (r)
				return r;

			// Check for RELRO
			r = pakfire_file_check_relro(file);
			if (r)
				return r;

			// Check for RUNPATH
			r = pakfire_file_check_runpath(file);
			if (r)
				return r;
		}

DONE:
		// All checks done
		file->check_done = 1;
	}

	// Return any issues
	if (issues)
		*issues = file->issues;

	return 0;
}
