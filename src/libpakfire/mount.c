/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <linux/limits.h>
#include <stddef.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <sys/types.h>

// libmount
#include <libmount/libmount.h>

#include <pakfire/arch.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/mount.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

static const struct pakfire_mountpoint {
	const char* source;
	const char* target;
	const char* fstype;
	int flags;
	const char* options;
} mountpoints[] = {
	// Mount a new instance of /proc
	{ "pakfire_proc",        "proc",               "proc",
		MS_NOSUID|MS_NOEXEC|MS_NODEV, NULL, },

	// Make /proc/sys read-only (except /proc/sys/net)
	{ "/proc/sys",           "proc/sys",           "bind",   MS_BIND|MS_REC, NULL, },
	{ "/proc/sys/net",       "proc/sys/net",       "bind",   MS_BIND|MS_REC, NULL, },
	{ "/proc/sys",           "proc/sys",           "bind",
		MS_BIND|MS_RDONLY|MS_NOSUID|MS_NOEXEC|MS_NODEV|MS_REMOUNT, NULL, },

	// Deny write access to /proc/sysrq-trigger (can be used to restart the host)
	{ "/proc/sysrq-trigger", "proc/sysrq-trigger", "bind",   MS_BIND|MS_REC, NULL, },
	{ "/proc/sysrq-trigger", "proc/sysrq-trigger", "bind",
		MS_BIND|MS_RDONLY|MS_NOSUID|MS_NOEXEC|MS_NODEV|MS_REMOUNT, NULL, },

	// Make /proc/irq read-only
	{ "/proc/irq",           "proc/irq",           "bind",   MS_BIND|MS_REC, NULL, },
	{ "/proc/irq",           "proc/irq",           "bind",
		MS_BIND|MS_RDONLY|MS_NOSUID|MS_NOEXEC|MS_NODEV|MS_REMOUNT, NULL, },

	// Make /proc/bus read-only
	{ "/proc/bus",           "proc/bus",           "bind",   MS_BIND|MS_REC, NULL, },
	{ "/proc/bus",           "proc/bus",           "bind",
		MS_BIND|MS_RDONLY|MS_NOSUID|MS_NOEXEC|MS_NODEV|MS_REMOUNT, NULL, },

	// Bind-Mount /sys ready-only
	{ "/sys",                "sys",                "bind",   MS_BIND|MS_REC, NULL, },
	{ "/sys",                "sys",                "bind",
		MS_BIND|MS_RDONLY|MS_NOSUID|MS_NOEXEC|MS_NODEV|MS_REMOUNT, NULL, },

	// Create a new /dev
	{ "pakfire_dev",         "dev",                "tmpfs",  MS_NOSUID|MS_NOEXEC,
		"mode=0755,size=4m,nr_inodes=64k", },
	{ "pakfire_dev_pts",     "dev/pts",            "devpts", MS_NOSUID|MS_NOEXEC,
		"newinstance,ptmxmode=0666,mode=620", },

	// Create a new /dev/shm
	{ "pakfire_dev_shm",     "dev/shm",            "tmpfs",
		MS_NOSUID|MS_NODEV|MS_STRICTATIME, "mode=1777,size=1024m", },

	// Mount /dev/mqueue
	{ "mqueue",               "dev/mqueue",        "mqueue",
		MS_NOSUID|MS_NOEXEC|MS_NODEV, NULL },

	// Create a new /run
	{ "pakfire_run",          "run",               "tmpfs",  MS_NOSUID|MS_NOEXEC|MS_NODEV,
		"mode=755,size=256m,nr_inodes=1k", },

	// Create a new /tmp
	{ "pakfire_tmp",          "tmp",               "tmpfs",
		MS_NOSUID|MS_NODEV|MS_STRICTATIME, "mode=1777,size=4096m", },

	// The end
	{ NULL },
};

static const struct pakfire_devnode {
	const char* path;
	int major;
	int minor;
	mode_t mode;
} devnodes[] = {
	{ "/dev/null",      1,  3, S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, },
	{ "/dev/zero",      1,  5, S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, },
	{ "/dev/full",      1,  7, S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, },
	{ "/dev/random",    1,  8, S_IFCHR|S_IRUSR|S_IRGRP|S_IROTH, },
	{ "/dev/urandom",   1,  9, S_IFCHR|S_IRUSR|S_IRGRP|S_IROTH, },
	{ "/dev/kmsg",      1, 11, S_IFCHR|S_IRUSR|S_IRGRP|S_IROTH, },
	{ "/dev/tty",       5,  0, S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, },
	{ "/dev/console",   5,  1, S_IFCHR|S_IRUSR|S_IWUSR, },
	{ "/dev/rtc0",    252,  0, S_IFCHR|S_IRUSR|S_IWUSR, },
	{ NULL },
};

static const struct pakfire_symlink {
	const char* target;
	const char* path;
} symlinks[] = {
	{ "/dev/pts/ptmx",   "/dev/ptmx", },
	{ "/proc/self/fd",   "/dev/fd", },
	{ "/proc/self/fd/0", "/dev/stdin" },
	{ "/proc/self/fd/1", "/dev/stdout" },
	{ "/proc/self/fd/2", "/dev/stderr" },
	{ "/proc/kcore",     "/dev/core" },
	{ NULL },
};

/*
	Easy way to iterate through all mountpoints
*/
static int pakfire_mount_foreach(struct pakfire* pakfire, int direction,
		int (*callback)(struct pakfire* pakfire, struct libmnt_fs* fs, const void* data),
		const void* data) {
	const char* root = pakfire_get_path(pakfire);
	int r = 0;

	struct libmnt_iter* iterator = NULL;
	struct libmnt_table* tab = NULL;
	struct libmnt_fs* fs = NULL;

	// Create an iterator
	iterator = mnt_new_iter(direction);
	if (!iterator) {
		ERROR(pakfire, "Could not setup iterator: %m\n");
		goto ERROR;
	}

	// Read /proc/mounts
	tab = mnt_new_table_from_file("/proc/mounts");
	if (!tab) {
		ERROR(pakfire, "Could not open /proc/mounts: %m\n");
		goto ERROR;
	}

	while (mnt_table_next_fs(tab, iterator, &fs) == 0) {
		const char* target = mnt_fs_get_target(fs);

		// Ignore any mointpoints that don't belong to us
		if (!pakfire_string_startswith(target, root))
			continue;

		// Call the callback for each relevant mountpoint
		r = callback(pakfire, fs, data);
		if (r)
			break;
	}

ERROR:
	// Tidy up
	if (fs)
		mnt_unref_fs(fs);
	if (tab)
		mnt_unref_table(tab);
	if (iterator)
		mnt_free_iter(iterator);

	return r;
}

static int __pakfire_is_mountpoint(struct pakfire* pakfire,
		struct libmnt_fs* fs, const void* data) {
	const char* path = (const char*)data;

	return mnt_fs_streq_target(fs, path);
}

int pakfire_is_mountpoint(struct pakfire* pakfire, const char* path) {
	return pakfire_mount_foreach(pakfire, MNT_ITER_FORWARD,
		__pakfire_is_mountpoint, path);
}

static int pakfire_mount(struct pakfire* pakfire, const char* source, const char* target,
		const char* fstype, unsigned long mflags, const void* data) {
	const char* options = (const char*)data;

	// Check for some basic inputs
	if (!source || !target) {
		errno = EINVAL;
		return 1;
	}

	DEBUG(pakfire, "Mounting %s from %s (%s - %s)\n", target, source, fstype, options);

	// Perform mount()
	int r = mount(source, target, fstype, mflags, data);
	if (r) {
		ERROR(pakfire, "Could not mount %s: %m\n", target);
	}

	return r;
}

static int __pakfire_mount_print(struct pakfire* pakfire,
		struct libmnt_fs* fs, const void* data) {
	DEBUG(pakfire,
		"  %s %s %s %s\n",
		mnt_fs_get_source(fs),
		mnt_fs_get_target(fs),
		mnt_fs_get_fstype(fs),
		mnt_fs_get_fs_options(fs)
	);

	return 0;
}

int pakfire_mount_list(struct pakfire* pakfire) {
	DEBUG(pakfire, "Mountpoints:\n");

	return pakfire_mount_foreach(pakfire, MNT_ITER_FORWARD,
		__pakfire_mount_print, NULL);
}

static int pakfire_populate_dev(struct pakfire* pakfire) {
	char path[PATH_MAX];

	// Create device nodes
	for (const struct pakfire_devnode* devnode = devnodes; devnode->path; devnode++) {
		DEBUG(pakfire, "Creating device node %s\n", devnode->path);

		int r = pakfire_path(pakfire, path, "%s", devnode->path);
		if (r)
			return r;

		dev_t dev = makedev(devnode->major, devnode->minor);

		r = mknod(path, devnode->mode, dev);

		// Continue if mknod was successful
		if (r == 0)
			continue;

		// If we could not create the device node because of permission issues,
		// it might be likely that we are running in a user namespace where creating
		// device nodes is not permitted. Try bind-mounting them.
		if (errno == EPERM)
			goto MOUNT;

		// Otherwise log an error and end
		ERROR(pakfire, "Could not create %s: %m\n", devnode->path);
		return r;

MOUNT:
		// Create an empty file
		r = pakfire_touch(path, 0444);
		if (r) {
			ERROR(pakfire, "Could not create %s: %m\n", path);
			return r;
		}

		// Create a bind-mount over the file
		r = pakfire_mount(pakfire, devnode->path, path, "bind", MS_BIND, NULL);
		if (r)
			return r;
	}

	// Create symlinks
	for (const struct pakfire_symlink* s = symlinks; s->target; s++) {
		DEBUG(pakfire, "Creating symlink %s -> %s\n", s->path, s->target);

		int r = pakfire_path(pakfire, path, "%s", s->path);
		if (r)
			return r;

		r = symlink(s->target, path);
		if (r) {
			ERROR(pakfire, "Could not create symlink %s: %m\n", s->path);
			return r;
		}
	}

	return 0;
}

static int pakfire_mount_interpreter(struct pakfire* pakfire) {
	char target[PATH_MAX];

	// Fetch the target architecture
	const char* arch = pakfire_get_arch(pakfire);

	// Can we emulate this architecture?
	char* interpreter = pakfire_arch_find_interpreter(arch);

	// No interpreter required
	if (!interpreter)
		return 0;

	DEBUG(pakfire, "Mounting interpreter %s for %s\n", interpreter, arch);

	// Where to mount this?
	int r = pakfire_path(pakfire, target, "%s", interpreter);
	if (r)
		return r;

	// Create directory
	r = pakfire_mkparentdir(target, 0755);
	if (r)
		return r;

	// Create an empty file
	FILE* f = fopen(target, "w");
	if (!f)
		return 1;
	fclose(f);

	r = pakfire_mount(pakfire, interpreter, target, NULL, MS_BIND|MS_RDONLY, NULL);
	if (r)
		ERROR(pakfire, "Could not mount interpreter %s to %s: %m\n", interpreter, target);

	return r;
}

int pakfire_mount_all(struct pakfire* pakfire) {
	char target[PATH_MAX];
	int r;

	// Fetch Pakfire's root directory
	const char* root = pakfire_get_path(pakfire);

	for (const struct pakfire_mountpoint* mp = mountpoints; mp->source; mp++) {
		// Figure out where to mount
		r = pakfire_path_join(target, root, mp->target);
		if (r)
			return r;

		// Create target if it doesn't exist
		if (!pakfire_path_exists(target)) {
			r = pakfire_mkdir(target, 0755);
			if (r) {
				ERROR(pakfire, "Could not create %s: %m\n", target);
				return r;
			}
		}

		// Perform mount()
		r = pakfire_mount(pakfire, mp->source, target, mp->fstype, mp->flags, mp->options);
		if (r)
			return r;
	}

	// Populate /dev
	r = pakfire_populate_dev(pakfire);
	if (r)
		return r;

	// Mount the interpreter (if needed)
	r = pakfire_mount_interpreter(pakfire);
	if (r)
		return r;

	return 0;
}

int pakfire_bind(struct pakfire* pakfire, const char* src, const char* dst, int flags) {
	struct stat st;
	char mountpoint[PATH_MAX];

	if (!dst)
		dst = src;

	int r = pakfire_path(pakfire, mountpoint, "%s", dst);
	if (r)
		return r;

	DEBUG(pakfire, "Bind-mounting %s to %s\n", src, mountpoint);

	r = stat(src, &st);
	if (r < 0) {
		ERROR(pakfire, "Could not stat %s: %m\n", src);
		return 1;
	}

	// Make sure the mountpoint exists
	switch (st.st_mode & S_IFMT) {
		case S_IFDIR:
			r = pakfire_mkdir(mountpoint, st.st_mode);
			if (r && errno != EEXIST)
				return r;
			break;

		case S_IFREG:
		case S_IFLNK:
			// Make parent directory
			r = pakfire_mkparentdir(mountpoint, 0755);
			if (r)
				return r;

			// Create a file
			FILE* f = fopen(mountpoint, "w");
			if (!f)
				return 1;
			fclose(f);
			break;

		default:
			errno = ENOTSUP;
			return 1;
	}

	// The Linux kernel seems to be quite funny when trying to bind-mount something
	// as read-only and requires us to mount the source first, and then remount it
	// again using MS_RDONLY.
	if (flags & MS_RDONLY) {
		r = pakfire_mount(pakfire, src, mountpoint, NULL, MS_BIND|MS_REC, NULL);
		if (r)
			return r;

		// Add the remount flag
		flags |= MS_REMOUNT;
	}

	// Perform mount
	return pakfire_mount(pakfire, src, mountpoint, NULL, flags|MS_BIND|MS_REC, NULL);
}
