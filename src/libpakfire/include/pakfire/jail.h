/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_JAIL_H
#define PAKFIRE_JAIL_H

#include <pakfire/pakfire.h>

struct pakfire_jail;

int pakfire_jail_create(struct pakfire_jail** jail, struct pakfire* pakfire);

struct pakfire_jail* pakfire_jail_ref(struct pakfire_jail* jail);
struct pakfire_jail* pakfire_jail_unref(struct pakfire_jail* jail);

// Mountpoints
int pakfire_jail_bind(struct pakfire_jail* jail,
	const char* source, const char* target, int flags);

// Resource Limits
int pakfire_jail_nice(struct pakfire_jail* jail, int nice);

// Timeout
int pakfire_jail_set_timeout(struct pakfire_jail* jail, unsigned int timeout);

// Environment
const char* pakfire_jail_get_env(struct pakfire_jail* jail, const char* key);
int pakfire_jail_set_env(struct pakfire_jail* jail, const char* key, const char* value);
int pakfire_jail_import_env(struct pakfire_jail* jail, const char* env[]);

// Execute
typedef int (*pakfire_jail_communicate_in)
	(struct pakfire* pakfire, void* data, int fd);
typedef int (*pakfire_jail_communicate_out)
	(struct pakfire* pakfire, void* data, int priority, const char* line, const size_t length);

enum pakfire_jail_exec_flags {
	PAKFIRE_JAIL_HAS_NETWORKING = (1 << 0),
	PAKFIRE_JAIL_NOENT_OK       = (1 << 1),
};

int pakfire_jail_exec(
	struct pakfire_jail* jail,
	const char* argv[],
	pakfire_jail_communicate_in  callback_in,
	pakfire_jail_communicate_out callback_out,
	void* data,
	int flags);

#ifdef PAKFIRE_PRIVATE

#include <pakfire/cgroup.h>

// Capture
int pakfire_jail_capture_stdout(struct pakfire* pakfire, void* data,
	int priority, const char* line, size_t length);

// Resource limits
int pakfire_jail_set_cgroup(struct pakfire_jail* jail, struct pakfire_cgroup* cgroup);

// Convenience functions
int pakfire_jail_run(struct pakfire* pakfire, const char* argv[], int flags, char** output);
int pakfire_jail_run_script(struct pakfire* pakfire,
	const char* script, const size_t length, const char* argv[], int flags);

int pakfire_jail_exec_script(struct pakfire_jail* jail,
	const char* script,
	const size_t size,
	const char* args[],
	pakfire_jail_communicate_in  callback_in,
	pakfire_jail_communicate_out callback_out,
	void* data);

int pakfire_jail_shell(struct pakfire_jail* jail);
int pakfire_jail_ldconfig(struct pakfire* pakfire);
int pakfire_jail_run_systemd_tmpfiles(struct pakfire* pakfire);

#endif /* PAKFIRE_PRIVATE */

#endif /* PAKFIRE_JAIL_H */
