/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <unistd.h>

#include <pakfire/arch.h>
#include <pakfire/util.h>

#include "../testsuite.h"

static int test_native(const struct test* t) {
	// First call
	const char* arch1 = pakfire_arch_native();
	ASSERT(arch1);

	// Second call
	const char* arch2 = pakfire_arch_native();
	ASSERT(arch2);

	// Must be the same pointer
	ASSERT(arch1 == arch2);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_SUCCESS;
}

static int test_supported(const struct test* t) {
	int r;

	r = pakfire_arch_supported("x86_64");
	ASSERT(r);

	// Check non-existant architecture
	r = pakfire_arch_supported("ABC");
	ASSERT(!r);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_SUCCESS;
}

static int test_machine(const struct test* t) {
	char machine[128];
	int r;

	r = pakfire_arch_machine(machine, "x86_64", "ipfire");
	ASSERT_STRING_EQUALS(machine, "x86_64-ipfire-linux-gnu");
	ASSERT(r == 0);

	r = pakfire_arch_machine(machine, "x86_64", "IPFIRE");
	ASSERT_STRING_EQUALS(machine, "x86_64-ipfire-linux-gnu");
	ASSERT(r == 0);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_SUCCESS;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_native);
	testsuite_add_test(test_supported);
	testsuite_add_test(test_machine);

	return testsuite_run(argc, argv);
}
