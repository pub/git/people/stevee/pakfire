#!/usr/bin/python3

import pakfire
import time
import unittest

from pakfire._pakfire import Progressbar

class Test(unittest.TestCase):
	"""
		This tests the progressbar command
	"""
	def test_simple(self):
		p = Progressbar()

		# Add a string
		p.add_string("Progress...")

		# Add the bar
		p.add_bar()

		# Show the percentage
		p.add_percentage()

		# Show ETA
		p.add_eta()

		# Start the progressbar
		p.start(1000)

		for i in range(1000):
			p.update(i)

		p.finish()

	def test_context(self):
		p = Progressbar()

		p.add_string("Progress...")
		p.add_bar()

		p.set_max(1000)

		with p:
			p.set_max(1000)

			for i in range(1000):
				p.increment()


if __name__ == "__main__":
	unittest.main()
