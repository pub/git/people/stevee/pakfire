/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <sys/mount.h>
#include <time.h>
#include <unistd.h>
#include <uuid/uuid.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include <pakfire/build.h>
#include <pakfire/cgroup.h>
#include <pakfire/dependencies.h>
#include <pakfire/dist.h>
#include <pakfire/file.h>
#include <pakfire/i18n.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/mount.h>
#include <pakfire/package.h>
#include <pakfire/packager.h>
#include <pakfire/parser.h>
#include <pakfire/private.h>
#include <pakfire/problem.h>
#include <pakfire/repo.h>
#include <pakfire/request.h>
#include <pakfire/scriptlet.h>
#include <pakfire/snapshot.h>
#include <pakfire/solution.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define CCACHE_DIR "/var/cache/ccache"

// We guarantee 2 GiB of memory to every build container
#define PAKFIRE_BUILD_MEMORY_GUARANTEED		(size_t)2 * 1024 * 1024 * 1024

// We allow only up to 2048 processes/threads for every build container
#define PAKFIRE_BUILD_PID_LIMIT				(size_t)2048

struct pakfire_build {
	struct pakfire* pakfire;
	int nrefs;

	// Flags
	int flags;

	// Build ID
	uuid_t id;
	char _id[UUID_STR_LEN];

	char target[PATH_MAX];

	// Times
	time_t time_start;

	// cgroup
	struct pakfire_cgroup* cgroup;

	// Jail
	struct pakfire_jail* jail;

	// The build repository
	struct pakfire_repo* repo;

	// A list of all built packages
	struct pakfire_packagelist* packages;

	// Buildroot
	char buildroot[PATH_MAX];

	// States
	int init:1;
};

#define TEMPLATE \
	"#!/bin/bash --login\n" \
	"\n" \
	"set -e\n" \
	"set -x\n" \
	"\n" \
	"%%{_%s}\n" \
	"\n" \
	"exit 0\n"

static int pakfire_build_has_flag(struct pakfire_build* build, int flag) {
	return build->flags & flag;
}

static time_t pakfire_build_duration(struct pakfire_build* build) {
	// What time is it now?
	time_t now = time(NULL);

	// Return any errors
	if (now < 0)
		return now;

	return now - build->time_start;
}

static int __pakfire_build_setup_repo(struct pakfire* pakfire,
		struct pakfire_repo* repo, void* p) {
	char path[PATH_MAX];
	FILE* f = NULL;
	int r;

	struct pakfire_build* build = (struct pakfire_build*)p;

	// Skip processing the installed repository
	if (pakfire_repo_is_installed_repo(repo))
		return 0;

	// Skip processing any other internal repositories
	if (pakfire_repo_is_internal(repo))
		return 0;

	const char* name = pakfire_repo_get_name(repo);

	DEBUG(pakfire, "Exporting repository configuration for '%s'\n", name);

	// Make path for configuration file
	r = pakfire_path(build->pakfire, path, PAKFIRE_CONFIG_DIR "/repos/%s.repo", name);
	if (r) {
		ERROR(pakfire, "Could not make repository configuration path for %s: %m\n", name);
		goto ERROR;
	}

	// Create the parent directory
	r = pakfire_mkparentdir(path, 0755);
	if (r)
		goto ERROR;

	// Open the repository configuration
	f = fopen(path, "w");
	if (!f) {
		ERROR(pakfire, "Could not open %s for writing: %m\n", path);
		goto ERROR;
	}

	// Write repository configuration
	r = pakfire_repo_write_config(repo, f);
	if (r) {
		ERROR(pakfire, "Could not write repository configuration for %s: %m\n", name);
		goto ERROR;
	}

	// Bind-mount any local repositories
	if (pakfire_repo_is_local(repo)) {
		const char* _path = pakfire_repo_get_path(repo);

		// Bind-mount the repository data read-only
		r = pakfire_jail_bind(build->jail, _path, _path, MS_RDONLY);
		if (r) {
			ERROR(pakfire, "Could not bind-mount the repository at %s: %m\n", _path);
			goto ERROR;
		}
	}

ERROR:
	if (f)
		fclose(f);

	return r;
}

/*
	This function enables the local repository so that it can be accessed by
	a pakfire instance running inside the chroot.
*/
static int pakfire_build_enable_repos(struct pakfire_build* build) {
	return pakfire_repo_walk(build->pakfire, __pakfire_build_setup_repo, build);
}

/*
	Drops the user into a shell
*/
static int pakfire_build_shell(struct pakfire_build* build) {
	int r;

	// Export local repository if running in interactive mode
	r = pakfire_build_enable_repos(build);
	if (r)
		return r;

	// Run shell
	return pakfire_jail_shell(build->jail);
}

static int pakfire_build_read_script(struct pakfire_build* build,
		const char* filename, char** buffer, size_t* length) {
	char path[PATH_MAX];
	FILE* f = NULL;
	int r;

	// Compose the source path
	r = pakfire_path_join(path, PAKFIRE_SCRIPTS_DIR, filename);
	if (r) {
		ERROR(build->pakfire, "Could not compose path for script '%s': %m\n", filename);
		goto ERROR;
	}

	DEBUG(build->pakfire, "Reading script from %s...\n", path);

	// Open the file
	f = fopen(path, "r");
	if (!f) {
		ERROR(build->pakfire, "Could not open script %s: %m\n", path);
		goto ERROR;
	}

	// Read the file into a the buffer
	r = pakfire_read_file_into_buffer(f, buffer, length);
	if (r) {
		ERROR(build->pakfire, "Could not read script: %m\n");
		goto ERROR;
	}

ERROR:
	if (f)
		fclose(f);

	return r;
}

static int pakfire_build_run_script(
		struct pakfire_build* build,
		const char* filename,
		const char* args[],
		pakfire_jail_communicate_in communicate_in,
		pakfire_jail_communicate_out communicate_out,
		void* data) {
	int r;

	char* script = NULL;
	size_t length = 0;

	DEBUG(build->pakfire, "Running build script '%s'...\n", filename);

	// Read the script
	r = pakfire_build_read_script(build, filename, &script, &length);
	if (r) {
		ERROR(build->pakfire, "Could not read script %s: %m\n", filename);
		return r;
	}

	// Execute the script
	r = pakfire_jail_exec_script(build->jail, script, length, args,
			communicate_in, communicate_out, data);
	if (r) {
		ERROR(build->pakfire, "Script '%s' failed with status %d\n", filename, r);
	}

	if (script)
		free(script);

	return r;
}

struct pakfire_find_deps_ctx {
	struct pakfire_package* pkg;
	int dep;
	struct pakfire_scriptlet* scriptlet;
	int class;
	const pcre2_code* filter;

	struct pakfire_filelist* filelist;
	unsigned int i;
};

static int pakfire_build_make_filter(struct pakfire_build* build, pcre2_code** regex,
		struct pakfire_parser* makefile, const char* namespace, const char* filter) {
	char* pattern = NULL;
	int r = 0;

	// Fetch the pattern
	pattern = pakfire_parser_get(makefile, namespace, filter);

	// Nothing if to if there is no or an empty pattern
	if (!pattern || !*pattern)
		goto ERROR;

	DEBUG(build->pakfire, "Found filter for %s: %s\n", filter, pattern);

	// Compile the regular expression
	r = pakfire_compile_regex(build->pakfire, regex, pattern);
	if (r)
		goto ERROR;

ERROR:
	if (pattern)
		free(pattern);

	return r;
}

static int pakfire_build_send_filelist(struct pakfire* pakfire, void* data, int fd) {
	struct pakfire_find_deps_ctx* ctx = (struct pakfire_find_deps_ctx*)data;
	struct pakfire_file* file = NULL;
	int r = 0;

	const size_t length = pakfire_filelist_length(ctx->filelist);

	// Check if we have reached the end of the filelist
	if (ctx->i >= length)
		return EOF;

	// Fetch the next file
	file = pakfire_filelist_get(ctx->filelist, ctx->i);
	if (!file) {
		DEBUG(pakfire, "Could not fetch file %d: %m\n", ctx->i);
		r = 1;
		goto ERROR;
	}

	// Fetch the path of the file
	const char* path = pakfire_file_get_path(file);
	if (!path) {
		ERROR(pakfire, "Received a file with an empty path\n");
		r = 1;
		goto ERROR;
	}

	// Skip files that don't match what we are looking for
	if (ctx->class && !pakfire_file_matches_class(file, ctx->class))
		goto SKIP;

	// Write path to stdin
	r = dprintf(fd, "%s\n", path);
	if (r < 0)
		return r;

SKIP:
	// Move on to the next file
	ctx->i++;

	// Success
	r = 0;

ERROR:
	if (file)
		pakfire_file_unref(file);

	return r;
}

static int pakfire_build_process_deps(struct pakfire* pakfire,
		void* data, int priority, const char* buffer, const size_t length) {
	const struct pakfire_find_deps_ctx* ctx = (struct pakfire_find_deps_ctx*)data;
	char dep[PATH_MAX];
	pcre2_match_data* match = NULL;
	int r = 0;

	// Nothing to do for an empty buffer
	if (!buffer || !*buffer)
		return 0;

	switch (priority) {
		// Add every dependency that we have received
		case LOG_INFO:
			// Copy the dependency to the stack (and remove the trailing newline)
			r = pakfire_string_format(dep, "%.*s", (int)length - 1, buffer);
			if (r)
				return r;

			DEBUG(pakfire, "Processing dependency: %s\n", dep);

			// Filter out any dependencies that are provided by this package
			if (ctx->dep == PAKFIRE_PKG_REQUIRES) {
				// If this is a file, we check if it is on the filelist
				if (pakfire_filelist_contains(ctx->filelist, dep))
					goto SKIP;

				// Otherwise check if this dependency is provided by this package
				else if (pakfire_package_matches_dep(ctx->pkg, PAKFIRE_PKG_PROVIDES, dep))
					goto SKIP;
			}

			// Check if this dependency should be filtered
			if (ctx->filter) {
				match = pcre2_match_data_create_from_pattern(ctx->filter, NULL);
				if (!match) {
					ERROR(pakfire, "Could not allocate PCRE match data: %m\n");
					goto ERROR;
				}

				// Perform matching
				r = pcre2_jit_match(ctx->filter, (PCRE2_SPTR)dep, length - 1,
					0, 0, match, NULL);

				// No match
				if (r == PCRE2_ERROR_NOMATCH) {
					// Fall through...

				// Handle any errors
				} else if (r < 0) {
					char error[120];

					// Fetch the error message
					r = pcre2_get_error_message(r, (PCRE2_UCHAR*)error, sizeof(error));
					if (r < 0) {
						ERROR(pakfire, "Could not fetch PCRE error message: %m\n");
						r = 1;
						goto ERROR;
					}

					ERROR(pakfire, "Could not match the filter: %s\n", error);
					r = 1;
					goto ERROR;

				// Match!
				} else {
					DEBUG(pakfire, "Skipping dependency that has been filtered: %s\n", dep);
					r = 0;
					goto ERROR;
				}
			}

			// Add dependency
			r = pakfire_package_add_dep(ctx->pkg, ctx->dep, buffer);
			if (r) {
				ERROR(pakfire, "Could not process dependency '%s': %m\n", buffer);
				return r;
			}
			break;

		// Send everything else to the default logger
		default:
			ERROR(pakfire, "%s\n", buffer);
			break;
	}

	goto ERROR;

SKIP:
	DEBUG(pakfire, "Skipping dependency that is provided by the package itself: %s\n", dep);

ERROR:
	if (match)
		pcre2_match_data_free(match);

	return r;
}

/*
	This function is a special way to run a script.

	It will pipe the filelist into the standard input of the called script
	and will read any dependencies from the standard output.
*/
static int pakfire_build_find_deps(struct pakfire_build* build,
		struct pakfire_package* pkg, int dep, const char* script,
		struct pakfire_filelist* filelist, int class, const pcre2_code* filter) {
	// Construct the context
	struct pakfire_find_deps_ctx ctx = {
		.pkg      = pkg,
		.dep      = dep,
		.class    = class,
		.filter   = filter,

		// Filelist
		.filelist = filelist,
		.i        = 0,
	};
	int r;

	// Skip calling the script if class doesn't match
	if (class && !pakfire_filelist_matches_class(filelist, class)) {
		DEBUG(build->pakfire, "Skipping calling %s as class does not match\n", script);
		return 0;
	}

	// Pass the buildroot as first argument
	const char* args[] = {
		pakfire_relpath(build->pakfire, build->buildroot),
		NULL,
	};

	// Run the script
	r = pakfire_build_run_script(build, script, args,
		pakfire_build_send_filelist, pakfire_build_process_deps, &ctx);
	if (r)
		ERROR(build->pakfire, "%s returned with error %d\n", script, r);

	return r;
}

static int pakfire_build_find_dependencies(struct pakfire_build* build,
		struct pakfire_parser* makefile, const char* namespace,
		struct pakfire_package* pkg, struct pakfire_filelist* filelist) {
	pcre2_code* filter_provides = NULL;
	pcre2_code* filter_requires = NULL;
	int r;

	// Fetch the provides filter
	r = pakfire_build_make_filter(build, &filter_provides,
		makefile, namespace, "filter_provides");
	if (r) {
		ERROR(build->pakfire, "Provides filter is broken: %m\n");
		goto ERROR;
	}

	// Fetch the requires filter
	r = pakfire_build_make_filter(build, &filter_requires,
		makefile, namespace, "filter_requires");
	if (r) {
		ERROR(build->pakfire, "Requires filter is broken: %m\n");
		goto ERROR;
	}

	// Find all provides
	r = pakfire_build_find_deps(build, pkg,
		PAKFIRE_PKG_PROVIDES, "find-provides", filelist, 0, filter_provides);
	if (r)
		goto ERROR;

	// Find all Perl provides
	r = pakfire_build_find_deps(build, pkg,
		PAKFIRE_PKG_PROVIDES, "perl.prov", filelist, PAKFIRE_FILE_PERL, filter_provides);
	if (r)
		goto ERROR;

	// Find all requires
	r = pakfire_build_find_deps(build, pkg,
		PAKFIRE_PKG_REQUIRES, "find-requires", filelist, 0, filter_requires);
	if (r)
		goto ERROR;

	// Find all Perl requires
	r = pakfire_build_find_deps(build, pkg,
		PAKFIRE_PKG_REQUIRES, "perl.req", filelist, PAKFIRE_FILE_PERL, filter_requires);
	if (r)
		goto ERROR;

ERROR:
	if (filter_provides)
		pcre2_code_free(filter_provides);
	if (filter_requires)
		pcre2_code_free(filter_requires);

	return r;
}

static int __pakfire_build_package_mark_config_files(
		struct pakfire* pakfire, struct pakfire_file* file, void* data) {
	char** configfiles = (char**)data;
	int r;

	// Skip anything that isn't a regular file
	if (pakfire_file_get_type(file) == S_IFREG)
		return 0;

	// Fetch path
	const char* path = pakfire_file_get_path(file);

	// Check if any configfile entry matches against this
	for (char** configfile = configfiles; *configfile; configfile++) {
		if (pakfire_string_startswith(path, *configfile)) {
			r = pakfire_file_set_flags(file, PAKFIRE_FILE_CONFIG);
			if (r)
				return r;

			// Done
			break;
		}
	}

	return 0;
}

static int pakfire_build_package_mark_config_files(struct pakfire_build* build,
		struct pakfire_filelist* filelist, struct pakfire_parser* makefile, const char* namespace) {
	char** configfiles = NULL;
	int r;

	// Fetch configfiles from makefile
	r = pakfire_parser_get_filelist(makefile, namespace, "configfiles", &configfiles, NULL);

	// If configfiles is not set, there is nothing to do
	if (!configfiles)
		return 0;

	r = pakfire_filelist_walk(filelist, __pakfire_build_package_mark_config_files,
		configfiles, 0);

	// Cleanup
	if (configfiles) {
		for (char** configfile = configfiles; *configfile; configfile++)
			free(*configfile);
		free(configfiles);
	}

	return r;
}

static int pakfire_build_package_add_files(struct pakfire_build* build,
		struct pakfire_parser* makefile, const char* buildroot, const char* namespace,
		struct pakfire_package* pkg, struct pakfire_packager* packager) {
	struct pakfire_filelist* filelist = NULL;
	char** includes = NULL;
	char** excludes = NULL;
	int r;

	// Fetch filelist from makefile
	r = pakfire_parser_get_filelist(makefile, namespace, "files", &includes, &excludes);

	// No files to package?
	if (!includes && !excludes)
		return 0;

	// Allocate a new filelist
	r = pakfire_filelist_create(&filelist, build->pakfire);
	if (r)
		goto ERROR;

	// Scan for files
	r = pakfire_filelist_scan(filelist, build->buildroot,
		(const char**)includes, (const char**)excludes);
	if (r)
		goto ERROR;

	DEBUG(build->pakfire, "%zu file(s) found\n", pakfire_filelist_length(filelist));

	// Nothing to do if the filelist is empty
	if (pakfire_filelist_is_empty(filelist))
		goto ERROR;

	// Dump the filelist
	pakfire_filelist_dump(filelist, PAKFIRE_FILE_DUMP_FULL);

	// Find dependencies
	r = pakfire_build_find_dependencies(build, makefile, namespace, pkg, filelist);
	if (r) {
		ERROR(build->pakfire, "Finding dependencies failed: %m\n");
		goto ERROR;
	}

	// Mark configuration files
	r = pakfire_build_package_mark_config_files(build, filelist, makefile, namespace);
	if (r)
		goto ERROR;

	// Add all files to the package
	r = pakfire_packager_add_files(packager, filelist);
	if (r)
		goto ERROR;

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);
	if (includes) {
		for (char** include = includes; *include; include++)
			free(*include);
		free(includes);
	}
	if (excludes) {
		for (char** exclude = excludes; *exclude; exclude++)
			free(*exclude);
		free(excludes);
	}

	return r;
}

static int pakfire_build_send_scriptlet(struct pakfire* pakfire, void* data, int fd) {
	const struct pakfire_find_deps_ctx* ctx = (struct pakfire_find_deps_ctx*)data;
	size_t length = 0;

	// Fetch the scriptlet
	const char* p = pakfire_scriptlet_get_data(ctx->scriptlet, &length);
	if (!p) {
		ERROR(pakfire, "Could not fetch scriptlet: %m\n");
		return 1;
	}

	// Write it into the pipe
	ssize_t bytes_written = write(fd, p, length);
	if (bytes_written < 0) {
		ERROR(pakfire, "Could not send scriptlet: %m\n");
		return 1;
	}

	return EOF;
}

static int pakfire_build_add_scriptlet_requires(struct pakfire_build* build,
		struct pakfire_package* pkg, struct pakfire_scriptlet* scriptlet) {
	int r;

	struct pakfire_find_deps_ctx ctx = {
		.pkg       = pkg,
		.dep       = PAKFIRE_PKG_PREREQUIRES,
		.scriptlet = scriptlet,
	};

	// Find all pre-requires
	r = pakfire_build_run_script(build, "find-prerequires", NULL,
		pakfire_build_send_scriptlet, pakfire_build_process_deps, &ctx);
	if (r)
		goto ERROR;

ERROR:
	return r;
}

static int pakfire_build_package_add_scriptlet(struct pakfire_build* build,
		struct pakfire_package* pkg, struct pakfire_packager* packager,
		const char* type, const char* data) {
	struct pakfire_scriptlet* scriptlet = NULL;
	char* shell = NULL;
	int r;

	// Wrap scriptlet into a shell script
	r = asprintf(&shell, "#!/bin/sh\n\nset -e\n\n%s\n\nexit 0\n", data);
	if (r < 0)
		goto ERROR;

	// Create a scriptlet
	r = pakfire_scriptlet_create(&scriptlet, build->pakfire, type, shell, 0);
	if (r)
		goto ERROR;

	// Add it to the package
	r = pakfire_packager_add_scriptlet(packager, scriptlet);
	if (r) {
		ERROR(build->pakfire, "Could not add scriptlet %s\n", type);
		goto ERROR;
	}

	// Add scriptlet requirements
	r = pakfire_build_add_scriptlet_requires(build, pkg, scriptlet);
	if (r) {
		ERROR(build->pakfire, "Could not add scriptlet requirements: %m\n");
		goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (scriptlet)
		pakfire_scriptlet_unref(scriptlet);
	if (shell)
		free(shell);

	return r;
}

static int pakfire_build_package_add_scriptlets(struct pakfire_build* build,
		struct pakfire_parser* makefile, const char* namespace,
		struct pakfire_package* pkg, struct pakfire_packager* packager) {
	char name[NAME_MAX];
	int r;

	for (const char** type = pakfire_scriptlet_types; *type; type++) {
		r = pakfire_string_format(name, "scriptlet:%s", *type);
		if (r)
			return r;

		// Fetch the scriptlet
		char* data = pakfire_parser_get(makefile, namespace, name);
		if (!data)
			continue;

		// Add it to the package
		r = pakfire_build_package_add_scriptlet(build, pkg, packager, *type, data);
		if (r) {
			free(data);
			return r;
		}

		free(data);
	}

	return 0;
}

static int pakfire_build_package(struct pakfire_build* build, struct pakfire_parser* makefile,
		const char* buildroot, const char* namespace) {
	struct pakfire_package* pkg = NULL;
	struct pakfire_packager* packager = NULL;

	int r = 1;

	// Expand the handle into the package name
	char* name = pakfire_parser_expand(makefile, "packages", namespace);
	if (!name) {
		ERROR(build->pakfire, "Could not get package name: %m\n");
		goto ERROR;
	}

	INFO(build->pakfire, "Building package '%s'...\n", name);
	DEBUG(build->pakfire, "  buildroot = %s\n", buildroot);

	// Fetch build architecture
	const char* arch = pakfire_get_arch(build->pakfire);
	if (!arch)
		goto ERROR;

	// Fetch package from makefile
	r = pakfire_parser_create_package(makefile, &pkg, NULL, namespace, arch);
	if (r) {
		ERROR(build->pakfire, "Could not create package from makefile: %m\n");
		goto ERROR;
	}

	// Set distribution
	const char* distribution = pakfire_parser_get(makefile, NULL, "DISTRO_NAME");
	if (distribution) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_DISTRO, distribution);
		if (r)
			goto ERROR;
	}

	// Set build ID
	pakfire_package_set_uuid(pkg, PAKFIRE_PKG_BUILD_ID, build->id);

	// Set source package
	const char* source_name = pakfire_parser_get(makefile, NULL, "name");
	if (source_name) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_NAME, source_name);
		if (r)
			goto ERROR;
	}

	// Set source EVR
	const char* source_evr = pakfire_parser_get(makefile, NULL, "evr");
	if (source_evr) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_EVR, source_evr);
		if (r)
			goto ERROR;
	}

	// Set source arch
	r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_ARCH, "src");
	if (r)
		goto ERROR;

	// Create a packager
	r = pakfire_packager_create(&packager, build->pakfire, pkg);
	if (r)
		goto ERROR;

	// Add files
	r = pakfire_build_package_add_files(build, makefile, buildroot, namespace,
		pkg, packager);
	if (r)
		goto ERROR;

	// Add scriptlets
	r = pakfire_build_package_add_scriptlets(build, makefile, namespace,
		pkg, packager);
	if (r)
		goto ERROR;

	const char* path = pakfire_repo_get_path(build->repo);

	// Write the finished package
	r = pakfire_packager_finish_to_directory(packager, path, NULL);
	if (r) {
		ERROR(build->pakfire, "pakfire_packager_finish() failed: %m\n");
		goto ERROR;
	}

	// Cleanup all packaged files
	r = pakfire_packager_cleanup(packager);
	if (r)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (packager)
		pakfire_packager_unref(packager);
	if (pkg)
		pakfire_package_unref(pkg);
	if (name)
		free(name);

	return r;
}

static int pakfire_build_package_dump(struct pakfire* pakfire,
		struct pakfire_package* pkg, void* p) {
	char* dump = pakfire_package_dump(pkg, PAKFIRE_PKG_DUMP_LONG);
	if (!dump)
		return 1;

	INFO(pakfire, "%s\n", dump);
	free(dump);

	return 0;
}

static int pakfire_build_packages(struct pakfire_build* build,
		struct pakfire_parser* makefile) {
	DEBUG(build->pakfire, "Creating packages...");
	int r = 1;

	const char* buildroot = pakfire_relpath(build->pakfire, build->buildroot);

	// Fetch a list all all packages
	char** packages = pakfire_parser_list_namespaces(makefile, "packages.package:*");
	if (!packages) {
		ERROR(build->pakfire, "Could not find any packages: %m\n");
		goto ERROR;
	}

	unsigned int num_packages = 0;

	// Count how many packages we have
	for (char** package = packages; *package; package++)
		num_packages++;

	DEBUG(build->pakfire, "Found %d package(s)\n", num_packages);

	// Build packages in reverse order
	for (int i = num_packages - 1; i >= 0; i--) {
		r = pakfire_build_package(build, makefile, buildroot, packages[i]);
		if (r)
			goto ERROR;
	}

	// Rescan the build repository to import all packages again
	r = pakfire_repo_scan(build->repo, 0);
	if (r)
		goto ERROR;

	// Create a new packagelist
	r = pakfire_packagelist_create(&build->packages, build->pakfire);
	if (r)
		goto ERROR;

	// Fetch all packages
	r = pakfire_repo_to_packagelist(build->repo, build->packages);
	if (r)
		goto ERROR;

	// Dump them all
	r = pakfire_packagelist_walk(build->packages, pakfire_build_package_dump, NULL);
	if (r)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (packages)
		free(packages);

	return r;
}

static int pakfire_build_stage(struct pakfire_build* build,
		struct pakfire_parser* makefile, const char* stage) {
	char template[1024];

	// Prepare template for this stage
	int r = pakfire_string_format(template, TEMPLATE, stage);
	if (r)
		return r;

	// Fetch the environment
	char** envp = pakfire_parser_make_environ(makefile);

	// Create the build script
	char* script = pakfire_parser_expand(makefile, "build", template);
	if (!script) {
		ERROR(build->pakfire, "Could not generate the build script for stage '%s': %m\n",
			stage);
		goto ERROR;
	}

	INFO(build->pakfire, "Running build stage '%s'\n", stage);

	// Import environment
	// XXX is this a good idea?
	r = pakfire_jail_import_env(build->jail, (const char**)envp);
	if (r) {
		ERROR(build->pakfire, "Could not import environment: %m\n");
		goto ERROR;
	}

	// Run the script
	r = pakfire_jail_exec_script(build->jail, script, strlen(script), NULL, NULL, NULL, NULL);
	if (r) {
		ERROR(build->pakfire, "Build stage '%s' failed with status %d\n", stage, r);
	}

ERROR:
	if (envp) {
		for (char** e = envp; *e; e++)
			free(*e);
		free(envp);
	}
	if (script)
		free(script);

	return r;
}

enum {
	PAKFIRE_BUILD_CLEANUP_FILES         = (1 << 0),
	PAKFIRE_BUILD_ERROR_IF_NOT_EMPTY    = (1 << 1),
	PAKFIRE_BUILD_SHOW_PROGRESS         = (1 << 2),
};

/*
	This helper function takes a callback which is expected to add any files
	to the given filelist which will optionally be all removed after.
*/
static int pakfire_build_post_process_files(struct pakfire_build* build,
		struct pakfire_filelist* filelist, const char* description,
		int (*callback)(struct pakfire* pakfire, struct pakfire_file* file, void* data),
		int flags) {
	struct pakfire_filelist* removees = NULL;
	int r;

	// Create a filelist with objects that need to be removed
	r = pakfire_filelist_create(&removees, build->pakfire);
	if (r)
		goto ERROR;

	// Find all files that need to be removed
	r = pakfire_filelist_walk(filelist, callback, removees,
		(flags & PAKFIRE_BUILD_SHOW_PROGRESS) ? PAKFIRE_FILELIST_SHOW_PROGRESS : 0);
	if (r)
		goto ERROR;

	if (!pakfire_filelist_is_empty(removees)) {
		if (description)
			INFO(build->pakfire, "%s\n", description);

		// Show all files which will be removed
		pakfire_filelist_dump(removees, PAKFIRE_FILE_DUMP_FULL|PAKFIRE_FILE_DUMP_ISSUES);

		// Remove all files on the removee list
		if (flags & PAKFIRE_BUILD_CLEANUP_FILES) {
			r = pakfire_filelist_cleanup(removees, PAKFIRE_FILE_CLEANUP_TIDY);
			if (r)
				goto ERROR;

			// Remove all files from the filelist
			r = pakfire_filelist_remove_all(filelist, removees);
			if (r)
				goto ERROR;
		}

		// Report an error if any files have been found
		if (flags & PAKFIRE_BUILD_ERROR_IF_NOT_EMPTY)
			r = 1;
	}

ERROR:
	if (removees)
		pakfire_filelist_unref(removees);

	return r;
}

static int __pakfire_build_remove_static_libraries(
		struct pakfire* pakfire, struct pakfire_file* file, void* data) {
	struct pakfire_filelist* removees = (struct pakfire_filelist*)data;
	char path[PATH_MAX];
	int r;

	// Find all static libraries
	if (pakfire_file_matches_class(file, PAKFIRE_FILE_STATIC_LIBRARY)) {
		// Copy the filename
		r = pakfire_string_set(path, pakfire_file_get_abspath(file));
		if (r)
			return -1;

		// Remove the extension
		r = pakfire_path_replace_extension(path, "so");
		if (r)
			return -1;

		// Only delete if there is a shared object with the same name
		if (pakfire_path_exists(path))
			return pakfire_filelist_add(removees, file);
	}

	return 0;
}

static int pakfire_build_post_remove_static_libraries(
		struct pakfire_build* build, struct pakfire_filelist* filelist) {
	return pakfire_build_post_process_files(build, filelist,
		"Removing static libaries...",
		__pakfire_build_remove_static_libraries,
		PAKFIRE_BUILD_CLEANUP_FILES);
}

static int __pakfire_build_remove_libtool_archives(
		struct pakfire* pakfire, struct pakfire_file* file, void* data) {
	struct pakfire_filelist* removees = (struct pakfire_filelist*)data;

	// Find all libtool archive files
	if (pakfire_file_matches_class(file, PAKFIRE_FILE_LIBTOOL_ARCHIVE))
		return pakfire_filelist_add(removees, file);

	return 0;
}

static int pakfire_build_post_remove_libtool_archives(
		struct pakfire_build* build, struct pakfire_filelist* filelist) {
	return pakfire_build_post_process_files(build, filelist,
		"Removing libtool archives...",
		__pakfire_build_remove_libtool_archives,
		PAKFIRE_BUILD_CLEANUP_FILES);
}

static int __pakfire_build_check_broken_symlinks(
		struct pakfire* pakfire, struct pakfire_file* file, void* data) {
	struct pakfire_filelist* broken = (struct pakfire_filelist*)data;
	int r;

	// Ignore anything that isn't a symlink
	switch (pakfire_file_get_type(file)) {
		case S_IFLNK:
			if (!pakfire_file_symlink_target_exists(file)) {
				r = pakfire_filelist_add(broken, file);
				if (r)
					return r;
			}

			break;

		// Ignore anything that isn't a symlink
		default:
			break;
	}

	return 0;
}

static int pakfire_build_post_check_broken_symlinks(
		struct pakfire_build* build, struct pakfire_filelist* filelist) {
	return pakfire_build_post_process_files(build, filelist,
		"Broken symlinks have been found:",
		__pakfire_build_check_broken_symlinks,
		PAKFIRE_BUILD_ERROR_IF_NOT_EMPTY);
}

/*
	BUILDROOT Check
*/
static int pakfire_build_post_check_buildroot(
		struct pakfire_build* build, struct pakfire_filelist* filelist) {
	const char* buildroot = pakfire_relpath(build->pakfire, build->buildroot);

	// Nested function to keep a reference to buildroot
	int __pakfire_build_post_check_buildroot(
			struct pakfire* pakfire, struct pakfire_file* file, void* data) {
		struct pakfire_filelist* matches = (struct pakfire_filelist*)data;
		int r;

		// Fetch the path
		const char* path = pakfire_file_get_path(file);
		if (!path)
			return 1;

		// Do not run this for Python bytecode files
		if (pakfire_path_match("**.pyc", path))
			return 0;

		if (pakfire_file_payload_matches(file, buildroot, strlen(buildroot))) {
			r = pakfire_filelist_add(matches, file);
			if (r)
				return r;
		}

		return 0;
	}

	return pakfire_build_post_process_files(
		build, filelist, "Files containing BUILDROOT:",
		__pakfire_build_post_check_buildroot, PAKFIRE_BUILD_ERROR_IF_NOT_EMPTY);
}

/*
	File Issues
*/

static int __pakfire_build_post_check_files(
		struct pakfire* pakfire, struct pakfire_file* file, void* data) {
	struct pakfire_filelist* broken = (struct pakfire_filelist*)data;
	int issues = 0;
	int r;

	// Check file for issues
	r = pakfire_file_check(file, &issues);
	if (r) {
		ERROR(pakfire, "%s: File Check failed: %m\n", pakfire_file_get_path(file));
		return r;
	}

	// If any issues have been found, consider this file to be on the list
	if (issues) {
		r = pakfire_filelist_add(broken, file);
		if (r)
			return r;
	}

	return 0;
}

static int pakfire_build_post_check_files(
		struct pakfire_build* build, struct pakfire_filelist* filelist) {
	return pakfire_build_post_process_files(
		build,
		filelist,
		"File Issues:",
		__pakfire_build_post_check_files,
		PAKFIRE_BUILD_ERROR_IF_NOT_EMPTY|PAKFIRE_BUILD_SHOW_PROGRESS);
}

static int pakfire_build_run_post_build_checks(struct pakfire_build* build) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	// Create a filelist of all files in the build
	r = pakfire_filelist_create(&filelist, build->pakfire);
	if (r) {
		ERROR(build->pakfire, "Could not create filelist: %m\n");
		goto ERROR;
	}

	// Scan for all files in BUILDROOT
	r = pakfire_filelist_scan(filelist, build->buildroot, NULL, NULL);
	if (r)
		goto ERROR;

	// If the filelist is empty, we can are done
	if (pakfire_filelist_is_empty(filelist)) {
		DEBUG(build->pakfire, "Empty BUILDROOT. Skipping post build checks...\n");
		r = 0;
		goto ERROR;
	}

	// Remove any static libraries
	r = pakfire_build_post_remove_static_libraries(build, filelist);
	if (r)
		goto ERROR;

	// Remove any libtool archives
	r = pakfire_build_post_remove_libtool_archives(build, filelist);
	if (r)
		goto ERROR;

	// Check for any broken symlinks
	r = pakfire_build_post_check_broken_symlinks(build, filelist);
	if (r)
		goto ERROR;

	// Check for BUILDROOT
	r = pakfire_build_post_check_buildroot(build, filelist);
	if (r)
		goto ERROR;

	// Check files
	r = pakfire_build_post_check_files(build, filelist);
	if (r)
		goto ERROR;

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

static const char* post_build_scripts[] = {
	"check-interpreters",
	"compress-man-pages",
	"strip",
	NULL,
};

static int pakfire_build_run_post_build_scripts(struct pakfire_build* build) {
	// Fetch buildroot
	const char* buildroot = pakfire_relpath(build->pakfire, build->buildroot);

	// Set default arguments for build scripts
	const char* args[] = {
		buildroot, NULL
	};

	// Run them one by one
	for (const char** script = post_build_scripts; *script; script++) {
		int r = pakfire_build_run_script(build, *script, args, NULL, NULL, NULL);
		if (r)
			return r;
	}

	return 0;
}

static void pakfire_build_free(struct pakfire_build* build) {
	if (build->packages)
		pakfire_packagelist_unref(build->packages);

	if (build->repo) {
		pakfire_repo_clean(build->repo, PAKFIRE_REPO_CLEAN_FLAGS_DESTROY);
		pakfire_repo_unref(build->repo);
	}

	if (build->jail)
		pakfire_jail_unref(build->jail);

	if (build->cgroup) {
		// Destroy the cgroup
		pakfire_cgroup_destroy(build->cgroup);

		// Free it
		pakfire_cgroup_unref(build->cgroup);
	}

	pakfire_unref(build->pakfire);
	free(build);
}

static int pakfire_build_parse_id(struct pakfire_build* build, const char* id) {
	int r;

	// Try parsing the Build ID
	if (id) {
		r = uuid_parse(id, build->id);
		if (r) {
			ERROR(build->pakfire, "Could not parse build ID '%s'\n", id);
			errno = EINVAL;
			return r;
		}

	// Otherwise initialize the Build ID with something random
	} else {
		uuid_generate_random(build->id);
	}

	// Store the ID as string, too
	uuid_unparse_lower(build->id, build->_id);

	return 0;
}

/*
	Sets up a new cgroup for this build
*/
static int pakfire_build_setup_cgroup(struct pakfire_build* build) {
	struct pakfire_config* config = NULL;
	char path[PATH_MAX];
	int r;

	// Compose path
	r = pakfire_string_format(path, "pakfire/build-%s", build->_id);
	if (r) {
		ERROR(build->pakfire, "Could not compose path for cgroup: %m\n");
		goto ERROR;
	}

	// Create a new cgroup
	r = pakfire_cgroup_open(&build->cgroup, build->pakfire, path,
		PAKFIRE_CGROUP_ENABLE_ACCOUNTING);
	if (r) {
		ERROR(build->pakfire, "Could not create cgroup for build %s: %m\n", build->_id);
		goto ERROR;
	}

	// Fetch config
	config = pakfire_get_config(build->pakfire);
	if (!config)
		goto ERROR;

	// Guarantee some minimum memory
	size_t memory_guaranteed = pakfire_config_get_bytes(config, "build",
		"memory_guaranteed", PAKFIRE_BUILD_MEMORY_GUARANTEED);
	if (memory_guaranteed) {
		r = pakfire_cgroup_set_guaranteed_memory(build->cgroup, memory_guaranteed);
		if (r)
			goto ERROR;
	}

	// Limit memory
	size_t memory_limit = pakfire_config_get_bytes(config, "build", "memory_limit", 0);
	if (memory_limit) {
		r = pakfire_cgroup_set_memory_limit(build->cgroup, memory_limit);
		if (r)
			goto ERROR;
	}

	// Set PID limit
	size_t pid_limit = pakfire_config_get_int(config, "build",
		"pid_limit", PAKFIRE_BUILD_PID_LIMIT);
	if (pid_limit) {
		r = pakfire_cgroup_set_pid_limit(build->cgroup, pid_limit);
		if (r)
			goto ERROR;
	}

ERROR:
	if (config)
		pakfire_config_unref(config);

	return r;
}

/*
	Sets up a new jail for this build
*/
static int pakfire_build_setup_jail(struct pakfire_build* build) {
	int r;

	// Create a new jail
	r = pakfire_jail_create(&build->jail, build->pakfire);
	if (r) {
		ERROR(build->pakfire, "Could not create jail for build %s: %m\n", build->_id);
		return r;
	}

	// Connect the jail to our cgroup
	r = pakfire_jail_set_cgroup(build->jail, build->cgroup);
	if (r) {
		ERROR(build->pakfire, "Could not set cgroup for jail: %m\n");
		return r;
	}

	// Done
	return 0;
}

/*
	Sets up the ccache for this build
*/
static int pakfire_build_setup_ccache(struct pakfire_build* build) {
	char path[PATH_MAX];
	int r;

	// Check if we want a ccache
	if (pakfire_build_has_flag(build, PAKFIRE_BUILD_DISABLE_CCACHE)) {
		DEBUG(build->pakfire, "ccache usage has been disabled for this build\n");

		// Set CCACHE_DISABLE=1 so that if ccache is installed, it will disable itself
		r = pakfire_jail_set_env(build->jail, "CCACHE_DISABLE", "1");
		if (r) {
			ERROR(build->pakfire, "Could not disable ccache: %m\n");
			return r;
		}

		return 0;
	}

	// Compose path
	r = pakfire_cache_path(build->pakfire, path, "%s", "ccache");
	if (r) {
		ERROR(build->pakfire, "Could not compose ccache path: %m\n");
		return 1;
	}

	DEBUG(build->pakfire, "Mounting ccache from %s\n", path);

	// Ensure path exists
	r = pakfire_mkdir(path, 0755);
	if (r && errno != EEXIST) {
		ERROR(build->pakfire, "Could not create ccache directory %s: %m\n", path);
		return r;
	}

	// Bind-mount the directory
	r = pakfire_jail_bind(build->jail, path, CCACHE_DIR, MS_NOSUID|MS_NOEXEC|MS_NODEV);
	if (r) {
		ERROR(build->pakfire, "Could not mount ccache: %m\n");
		return r;
	}

	return 0;
}

static int pakfire_build_setup_repo(struct pakfire_build* build) {
	char path[PATH_MAX] = PAKFIRE_TMP_DIR "/pakfire-build-repo.XXXXXX";
	char url[PATH_MAX];
	int r;

	// Create a new repository
	r = pakfire_repo_create(&build->repo, build->pakfire, PAKFIRE_REPO_RESULT);
	if (r) {
		ERROR(build->pakfire, "Could not create repository %s: %m", PAKFIRE_REPO_RESULT);
		return r;
	}

	// Set description
	pakfire_repo_set_description(build->repo, _("Build Repository"));

	// Create a temporary directory
	const char* p = pakfire_mkdtemp(path);
	if (!p) {
		ERROR(build->pakfire, "Could not create a the build repository: %m\n");
		return 1;
	}

	// Format the URL
	r = pakfire_string_format(url, "file://%s", path);
	if (r)
		return r;

	// Set the URL
	pakfire_repo_set_baseurl(build->repo, url);

	return r;
}

static int pakfire_build_set_time_start(struct pakfire_build* build) {
	const time_t now = time(NULL);

	if (now < 0) {
		ERROR(build->pakfire, "Could not fetch start time: %m\n");
		return 1;
	}

	build->time_start = now;

	return 0;
}

PAKFIRE_EXPORT int pakfire_build_create(struct pakfire_build** build,
		struct pakfire* pakfire, const char* id, int flags) {
	int r;

	// Allocate build object
	struct pakfire_build* b = calloc(1, sizeof(*b));
	if (!b)
		return 1;

	// Reference pakfire
	b->pakfire = pakfire_ref(pakfire);

	// Initialize reference counter
	b->nrefs = 1;

	// Copy flags
	b->flags = flags;

	// Store start time
	r = pakfire_build_set_time_start(b);
	if (r)
		goto ERROR;

	// Parse ID
	r = pakfire_build_parse_id(b, id);
	if (r)
		goto ERROR;

	// Setup repo
	r = pakfire_build_setup_repo(b);
	if (r)
		goto ERROR;

	// Create cgroup
	r = pakfire_build_setup_cgroup(b);
	if (r)
		goto ERROR;

	// Create jail
	r = pakfire_build_setup_jail(b);
	if (r)
		goto ERROR;

	// Setup ccache
	r = pakfire_build_setup_ccache(b);
	if (r)
		goto ERROR;

	*build = b;
	return 0;

ERROR:
	pakfire_build_free(b);
	return r;
}

PAKFIRE_EXPORT struct pakfire_build* pakfire_build_ref(struct pakfire_build* build) {
	++build->nrefs;

	return build;
}

PAKFIRE_EXPORT struct pakfire_build* pakfire_build_unref(struct pakfire_build* build) {
	if (--build->nrefs > 0)
		return build;

	pakfire_build_free(build);
	return NULL;
}

PAKFIRE_EXPORT int pakfire_build_set_target(
		struct pakfire_build* build, const char* target) {
	return pakfire_string_set(build->target, target);
}

static int pakfire_build_install_packages(struct pakfire_build* build,
		int* snapshot_needs_update) {
	int r;

	const char* packages[] = {
		"build-essential",
		NULL,
	};

	int changed = 0;

	// Install everything
	r = pakfire_install(build->pakfire, 0, 0, packages, NULL, 0,
		&changed, NULL, NULL);
	if (r) {
		ERROR(build->pakfire, "Could not install build dependencies: %m\n");
		return r;
	}

	// Mark snapshot as changed if new packages were installed
	if (changed)
		*snapshot_needs_update = 1;

	// Update everything
	r = pakfire_sync(build->pakfire, 0, 0, &changed, NULL, NULL);
	if (r) {
		ERROR(build->pakfire, "Could not update packages: %m\n");
		return r;
	}

	// Has anything changed?
	if (changed)
		*snapshot_needs_update = 1;

	return 0;
}

/*
	Initializes the build environment
*/
static int pakfire_build_init(struct pakfire_build* build) {
	int r;

	// Don't do it again
	if (build->init) {
		DEBUG(build->pakfire, "Build environment has already been initialized\n");
		return 0;
	}

	// Tells us whether we need to (re-)create the snapshot
	int snapshot_needs_update = 0;

	// Extract snapshot
	if (!pakfire_build_has_flag(build, PAKFIRE_BUILD_DISABLE_SNAPSHOT)) {
		r = pakfire_snapshot_restore(build->pakfire);
		if (r)
			return r;
	}

	// Install or update any build dependencies
	r = pakfire_build_install_packages(build, &snapshot_needs_update);
	if (r)
		return r;

	// Update the snapshot if there were changes
	if (!pakfire_build_has_flag(build, PAKFIRE_BUILD_DISABLE_SNAPSHOT) && snapshot_needs_update) {
		// Store the snapshot
		r = pakfire_snapshot_store(build->pakfire);
		if (r)
			return r;
	}

	// Mark as initialized
	build->init = 1;

	return 0;
}

static int pakfire_build_read_makefile(struct pakfire_build* build,
		struct pakfire_parser** parser, struct pakfire_package* package) {
	char path[PATH_MAX];
	int r;

	struct pakfire_parser_error* error = NULL;

	const char* nevra = pakfire_package_get_string(package, PAKFIRE_PKG_NEVRA);
	const char* name  = pakfire_package_get_string(package, PAKFIRE_PKG_NAME);

	// Compose path to makefile
	r = pakfire_path(build->pakfire, path, "/usr/src/packages/%s/%s.nm", nevra, name);
	if (r < 0)
		return 1;

	// Read makefile
	r = pakfire_read_makefile(parser, build->pakfire, path, &error);
	if (r) {
		if (error) {
			ERROR(build->pakfire, "Could not parse makefile %s: %s\n", path,
				pakfire_parser_error_get_message(error));
		} else {
			ERROR(build->pakfire, "Could not parse makefile %s: %m\n", path);
		}

		goto ERROR;
	}

	// Set BUILDROOT
	const char* buildroot = pakfire_relpath(build->pakfire, build->buildroot);
	if (buildroot)
		pakfire_parser_set(*parser, NULL, "BUILDROOT", buildroot, 0);

ERROR:
	if (error)
		pakfire_parser_error_unref(error);

	return r;
}

static int pakfire_build_perform(struct pakfire_build* build,
		struct pakfire_parser* makefile) {
	int r;

	// Prepare the build
	r = pakfire_build_stage(build, makefile, "prepare");
	if (r)
		goto ERROR;

	// Perform the build
	r = pakfire_build_stage(build, makefile, "build");
	if (r)
		goto ERROR;

	// Test the build
	if (!pakfire_build_has_flag(build, PAKFIRE_BUILD_DISABLE_TESTS)) {
		r = pakfire_build_stage(build, makefile, "test");
		if (r)
			goto ERROR;
	}

	// Install everything
	r = pakfire_build_stage(build, makefile, "install");
	if (r)
		goto ERROR;

	// Run post build checks
	r = pakfire_build_run_post_build_checks(build);
	if (r) {
		ERROR(build->pakfire, "Post build checks failed\n");
		goto ERROR;
	}

	// Run post build scripts
	r = pakfire_build_run_post_build_scripts(build);
	if (r)
		goto ERROR;

	// Done!
	return 0;

ERROR:
	// Drop to a shell for debugging
	if (pakfire_build_has_flag(build, PAKFIRE_BUILD_INTERACTIVE))
		pakfire_build_shell(build);

	return r;
}

static int __pakfire_build_unpackaged_file(struct pakfire* pakfire,
		struct pakfire_file* file, void* p) {
	char* s = pakfire_file_dump(file, PAKFIRE_FILE_DUMP_FULL);
	if (s) {
		ERROR(pakfire, "%s\n", s);
		free(s);
	}

	return 0;
}

static int pakfire_build_check_unpackaged_files(struct pakfire_build* build) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	// Create a new filelist
	r = pakfire_filelist_create(&filelist, build->pakfire);
	if (r)
		goto ERROR;

	// Scan for all files in BUILDROOT
	r = pakfire_filelist_scan(filelist, build->buildroot, NULL, NULL);
	if (r)
		goto ERROR;

	if (!pakfire_filelist_is_empty(filelist)) {
		ERROR(build->pakfire, "Unpackaged files found:\n");

		r = pakfire_filelist_walk(filelist, __pakfire_build_unpackaged_file, NULL, 0);
		if (r)
			goto ERROR;

		// Report an error
		r = 1;
	}

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

static int pakfire_build_install_package(struct pakfire* pakfire,
		struct pakfire_package* pkg, void* p) {
	struct pakfire_request* request = (struct pakfire_request*)p;

	return pakfire_request_add_package(request, PAKFIRE_REQ_INSTALL, pkg,
		PAKFIRE_REQUEST_ESSENTIAL);
}

static int pakfire_build_install_test(struct pakfire_build* build) {
	struct pakfire_request* request = NULL;
	struct pakfire_problem* problem = NULL;
	struct pakfire_solution* solution = NULL;
	const char* s = NULL;
	int r;

	// Create a new request
	r = pakfire_request_create(&request, build->pakfire, 0);
	if (r)
		goto ERROR;

	// Add all packages
	r = pakfire_packagelist_walk(build->packages, pakfire_build_install_package, request);

	// Solve the request
	r = pakfire_request_solve(request);
	switch (r) {
		// All okay
		case 0:
			break;

		// Dependency Error
		case 2:
			ERROR(build->pakfire, "Install test failed:\n");

			// Walk through all problems
			for (;;) {
				r = pakfire_request_next_problem(request, &problem);
				if (r)
					goto ERROR;

				// There are no more problems
				if (!problem)
					break;

				// Format the problem into something human-readable
				s = pakfire_problem_to_string(problem);
				if (!s)
					continue;

				ERROR(build->pakfire, "  * %s\n", s);

				// Walk through all solutions
				for (;;) {
					r = pakfire_problem_next_solution(problem, &solution);
					if (r)
						goto ERROR;

					// There are no more solutions
					if (!solution)
						break;

					// Format the solution into something human-readable
					s = pakfire_solution_to_string(solution);
					if (!s)
						continue;

					ERROR(build->pakfire, "    * %s\n", s);
				}
			}

			break;

		// Any other errors
		default:
			goto ERROR;
	}

ERROR:
	if (r)
		ERROR(build->pakfire, "Install test failed: %m\n");
	if (request)
		pakfire_request_unref(request);
	if (problem)
		pakfire_problem_unref(problem);
	if (solution)
		pakfire_solution_unref(solution);

	return r;
}

static int pakfire_build_post_check(struct pakfire_build* build) {
	int r;

	// Check for unpackaged files
	r = pakfire_build_check_unpackaged_files(build);
	if (r)
		return r;

	// Perform install test
	r = pakfire_build_install_test(build);
	if (r)
		return r;

	return 0;
}

static int pakfire_build_copy_package(struct pakfire* pakfire,
		struct pakfire_package* pkg, void* p) {
	struct pakfire_archive* archive = NULL;
	char path[PATH_MAX];
	int r;

	const char* target = (const char*)p;

	if (!target) {
		errno = EINVAL;
		return 1;
	}

	// Fetch the package filename
	const char* filename = pakfire_package_get_string(pkg, PAKFIRE_PKG_FILENAME);
	if (!filename) {
		r = 1;
		goto ERROR;
	}

	// Format the destination path
	r = pakfire_string_format(path, "%s/%s", target, filename);
	if (r)
		goto ERROR;

	// Open the archive
	archive = pakfire_package_get_archive(pkg);
	if (!archive) {
		r = 1;
		goto ERROR;
	}

	// Copy it to its destination
	r = pakfire_archive_copy(archive, path);
	if (r)
		goto ERROR;

ERROR:
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

static int pakfire_build_copy_packages(struct pakfire_build* build) {
	struct pakfire_repo* local = NULL;
	int r = 0;

	DEBUG(build->pakfire, "Copying built packages\n");

	// Fetch local repository
	local = pakfire_get_repo(build->pakfire, PAKFIRE_REPO_LOCAL);

	// Copy all packages to the target path
	if (*build->target) {
		r = pakfire_packagelist_walk(build->packages,
			pakfire_build_copy_package, build->target);
		if (r)
			goto ERROR;
	}

	// If we have a local repository, we copy all packages to it
	if (local) {
		const char* path = pakfire_repo_get_path(local);
		if (path) {
			r = pakfire_packagelist_walk(build->packages,
				pakfire_build_copy_package, (void*)path);
			if (r)
				goto ERROR;
		}
	}

ERROR:
	if (local)
		pakfire_repo_unref(local);

	return r;
}

static int pakfire_build_install_source_package(
		struct pakfire_build* build, struct pakfire_package* package) {
	struct pakfire_request* request = NULL;
	struct pakfire_transaction* transaction = NULL;
	int r;

	// Create a new request
	r = pakfire_request_create(&request, build->pakfire, 0);
	if (r)
		goto ERROR;

	// Add the package
	r = pakfire_request_add_package(request, PAKFIRE_REQ_INSTALL, package,
		PAKFIRE_REQUEST_ESSENTIAL);
	if (r)
		goto ERROR;

	// Solve the request
	r = pakfire_request_solve(request);
	if (r)
		goto ERROR;

	// Fetch the transaction
	r = pakfire_request_get_transaction(request, &transaction);
	if (r)
		goto ERROR;

	// Set how many packages have been changed
	const size_t changes = pakfire_transaction_count(transaction);

	// Sanity check to see if we actually try to install anything
	if (!changes) {
		ERROR(build->pakfire, "The source package did not get installed\n");
		r = 1;
		goto ERROR;
	}

	// Run the transaction
	r = pakfire_transaction_run(transaction, 0);
	if (r)
		goto ERROR;

ERROR:
	if (transaction)
		pakfire_transaction_unref(transaction);
	if (request)
		pakfire_request_unref(request);

	return r;
}

PAKFIRE_EXPORT int pakfire_build_exec(struct pakfire_build* build, const char* path) {
	struct pakfire_package* package = NULL;
	struct pakfire_parser* makefile = NULL;
	char* buildroot = NULL;
	char duration[TIME_STRING_MAX];
	int r;

	// Set buildroot
	r = pakfire_path(build->pakfire, build->buildroot, "%s",
		PAKFIRE_TMP_DIR "/pakfire-buildroot.XXXXXX");
	if (r)
		goto ERROR;

	// Open the source package
	r = pakfire_commandline_add(build->pakfire, path, &package);
	if (r)
		goto ERROR;

	const char* nevra = pakfire_package_get_string(package, PAKFIRE_PKG_NEVRA);

	INFO(build->pakfire, "Building %s...\n", nevra);

	// Initialize the build environment
	r = pakfire_build_init(build);
	if (r)
		goto ERROR;

	// Install the source package
	r = pakfire_build_install_source_package(build, package);
	if (r) {
		ERROR(build->pakfire, "Could not install the source package: %m\n");
		goto ERROR;
	}

	// Create BUILDROOT
	buildroot = pakfire_mkdtemp(build->buildroot);
	if (!buildroot) {
		ERROR(build->pakfire, "Could not create BUILDROOT: %m\n");
		goto ERROR;
	}

	// Open the makefile
	r = pakfire_build_read_makefile(build, &makefile, package);
	if (r)
		goto ERROR;

	// Perform the actual build
	r = pakfire_build_perform(build, makefile);
	if (r)
		goto ERROR;

	// Create the packages
	r = pakfire_build_packages(build, makefile);
	if (r) {
		ERROR(build->pakfire, "Could not create packages: %m\n");
		goto ERROR;
	}

	// Perform post build checks
	r = pakfire_build_post_check(build);
	if (r)
		goto ERROR;

	// Copy packages to their destination
	r = pakfire_build_copy_packages(build);
	if (r)
		goto ERROR;

	// Log duration
	r = pakfire_format_time(duration, pakfire_build_duration(build));
	if (r)
		goto ERROR;

	INFO(build->pakfire, "Build successfully completed in %s\n", duration);

ERROR:
	if (makefile)
		pakfire_parser_unref(makefile);
	if (package)
		pakfire_package_unref(package);

	// Cleanup buildroot
	if (buildroot)
		pakfire_rmtree(buildroot, 0);

	return r;
}

/*
	Compatibility function to keep the legacy API.
*/
PAKFIRE_EXPORT int pakfire_build(struct pakfire* pakfire, const char* path,
		const char* target, const char* id, int flags) {
	struct pakfire_build* build = NULL;
	int r;

	// Check if path is set
	if (!path) {
		errno = EINVAL;
		return 1;
	}

	// Create a new build environment
	r = pakfire_build_create(&build, pakfire, id, flags);
	if (r)
		goto ERROR;

	// Set target
	if (target) {
		r = pakfire_build_set_target(build, target);
		if (r)
			goto ERROR;
	}

	// Run build
	r = pakfire_build_exec(build, path);

ERROR:
	if (build)
		pakfire_build_unref(build);

	return r;
}

int pakfire_build_clean(struct pakfire* pakfire, int flags) {
	struct pakfire_repo* local = NULL;
	int r = 0;

	// Fetch local repository
	local = pakfire_get_repo(pakfire, PAKFIRE_REPO_LOCAL);
	if (!local) {
		ERROR(pakfire, "Could not find repository %s: %m\n", PAKFIRE_REPO_LOCAL);
		goto ERROR;
	}

	// Destroy everything in it
	r = pakfire_repo_clean(local, PAKFIRE_REPO_CLEAN_FLAGS_DESTROY);
	if (r)
		goto ERROR;

ERROR:
	if (local)
		pakfire_repo_unref(local);

	return r;
}

/*
	This is a convenience function that sets up a build environment and
	then drops the user into an interactive shell.
*/
PAKFIRE_EXPORT int pakfire_shell(struct pakfire* pakfire, const char** packages, int flags) {
	struct pakfire_build* build = NULL;
	int r;

	// Shells are always interactive
	flags |= PAKFIRE_BUILD_INTERACTIVE;

	// Create a new build environment
	r = pakfire_build_create(&build, pakfire, NULL, flags);
	if (r) {
		ERROR(pakfire, "Could not create build: %m\n");
		goto ERROR;
	}

	// Initialize the build environment
	r = pakfire_build_init(build);
	if (r)
		goto ERROR;

	// Install any additional packages
	if (packages) {
		r = pakfire_install(build->pakfire, 0, 0, packages, NULL, 0, NULL, NULL, NULL);
		if (r)
			return r;
	}

	// Run shell
	r = pakfire_build_shell(build);

ERROR:
	if (build)
		pakfire_build_unref(build);

	return r;
}
