/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <pakfire/string.h>

int __pakfire_string_format(char* s, const size_t length, const char* format, ...) {
	va_list args;
	int r;

	// Call __pakfire_string_vformat
	va_start(args, format);
	r = __pakfire_string_vformat(s, length, format, args);
	va_end(args);

	return r;
}

int __pakfire_string_vformat(char* s, const size_t length, const char* format, va_list args) {
	// Write string to buffer
	const ssize_t required = vsnprintf(s, length, format, args);

	// Catch any errors
	if (required < 0)
		return 1;

	// Check if the entire string could be written
	if ((size_t)required >= length) {
		errno = ENOMEM;
		return 1;
	}

	// Success
	return 0;
}

int __pakfire_string_set(char* s, const size_t length, const char* value) {
	// If value is NULL or an empty, we will overwrite the buffer with just zeros
	if (!value || !*value) {
		for (unsigned int i = 0; i < length; i++)
			s[i] = '\0';

		return 0;
	}

	// Otherwise just copy
	return __pakfire_string_format(s, length, "%s", value);
}

int pakfire_string_startswith(const char* s, const char* prefix) {
	// Validate input
	if (!s || !prefix) {
		errno = EINVAL;
		return 1;
	}

	return !strncmp(s, prefix, strlen(prefix));
}

int pakfire_string_endswith(const char* s, const char* suffix) {
	// Validate input
	if (!s || !suffix) {
		errno = EINVAL;
		return 1;
	}

	return !strcmp(s + strlen(s) - strlen(suffix), suffix);
}

int pakfire_string_matches(const char* s, const char* pattern) {
	// Validate input
	if (!s || !pattern) {
		errno = EINVAL;
		return 1;
	}

	return !!strstr(s, pattern);
}

int pakfire_string_partition(const char* s, const char* delim, char** s1, char** s2) {
	char* p = strstr(s, delim);

	// Delim was not found
	if (!p) {
		*s1 = NULL;
		*s2 = NULL;
		return 1;
	}

	// Length of string before delim
	size_t l = p - s;

	char* buffer = malloc(l + 1);
	if (!buffer)
		return 1;

	// Copy first part
	*s1 = memcpy(buffer, s, l);
	buffer[l] = '\0';

	// Copy second part
	*s2 = strdup(p + strlen(delim));

	return 0;
}

char* pakfire_string_replace(const char* s, const char* pattern, const char* repl) {
	// Return NULL on no input or no pattern
	if (!s || !pattern) {
		errno = EINVAL;
		return NULL;
	}

	// Replace with nothing when repl is NULL
	if (!repl)
		repl = "";

	char* result = NULL;
	const char** cache = NULL;
	unsigned int count = 0;

	const size_t pattern_length = strlen(pattern);

	// Working pointer
	const char* p = s;

	// Find all occurrences of pattern and store their location
	while (1) {
		const char* needle = strstr(p, pattern);
		if (!needle)
			break;

		// Make space in the cache
		cache = reallocarray(cache, sizeof(*cache), count + 1);
		cache[count++] = needle;

		// Move p forward
		p = needle + pattern_length;
	}

	// Copy the string if no occurence was found
	if (count == 0) {
		result = strdup(s);
		goto ERROR;
	}

	// Store the end pointer
	cache = reallocarray(cache, sizeof(*cache), count + 1);
	cache[count] = s + strlen(s);

	const size_t repl_length = strlen(repl);

	// Determine the length of the final string
	const size_t length = strlen(s) + ((repl_length - pattern_length) * count);

	// Allocate enough memory for the result
	result = malloc(length + 1);
	if (!result)
		goto ERROR;

	// Reset p
	p = s;

	// Working pointer for the result
	char* r = result;

	// Copy everything up to the first match
	ssize_t l = cache[0] - s;
	memcpy(r, p, l);
	r += l;
	p += l;

	for (unsigned int i = 0; i < count; i++) {
		// Put replacement here
		memcpy(r, repl, repl_length);
		r += repl_length;
		p += pattern_length;

		// Determine the length between two matches
		l = cache[i+1] - (cache[i] + pattern_length);

		memcpy(r, p, l);
		r += l;
		p += l;
	}

	// Terminate the string
	result[length] = '\0';

ERROR:
	if (cache)
		free(cache);

	return result;
}

static unsigned int pakfire_chrcnt(const char* s, char delim) {
	size_t length = strlen(s);

	unsigned int count = 0;

	for (unsigned int i = 0; i < length; i++)
		if (s[i] == delim)
			count++;

	return count;
}

char** pakfire_string_split(const char* s, char delim) {
	char** array = NULL;

	if (!s) {
		errno = EINVAL;
		return NULL;
	}

	// Count how often we need to split
	unsigned int count = pakfire_chrcnt(s, delim) + 1;

	// Allocate array
	array = calloc(count + 1, sizeof(*array));
	if (!array)
		return NULL;

	// Copy string to stack
	char* p = strdupa(s);
	if (!p)
		return NULL;

	unsigned int i = 0;
	while (*p) {
		char* e = strchr(p, delim);

		// Terminate the string
		if (e)
			*e = '\0';

		// Add string to the array
		array[i++] = strdup(p);

		// End loop when we reached the end
		if (!e)
			break;

		// Or continue at the next line
		p = e + 1;
	}

	return array;
}

char* pakfire_string_join(char** list, const char* delim) {
	// Validate input
	if (!list || !delim) {
		errno = EINVAL;
		return NULL;
	}

	size_t length = 0;
	unsigned int elements = 0;

	// Count the number of elements and the total length
	for (char** item = list; *item; item++) {
		length += strlen(*item);
		elements++;
	}

	// Empty list?
	if (!elements)
		return NULL;

	// Add the delimiters
	length += strlen(delim) * (elements - 1);

	// Allocate the result string
	char* string = malloc(length + 1);
	if (!string)
		return NULL;

	// Pointer to where we are writing
	char* p = string;

	size_t bytes_left = length + 1;
	size_t bytes_written;

	for (char** item = list; *item; item++) {
		bytes_written = snprintf(p, bytes_left, "%s", *item);

		bytes_left -= bytes_written;
		p += bytes_written;

		// Write the delimiter
		if (bytes_left) {
			bytes_written = snprintf(p, bytes_left, "%s", delim);

			bytes_left -= bytes_written;
			p += bytes_written;
		}
	}

	return string;
}

int __pakfire_format_size(char* dst, size_t length, double value) {
	const char* units[] = {
		"%.0f ",
		"%.0fk",
		"%.1fM",
		"%.1fG",
		"%.1fT",
		NULL
	};
	const char** unit = units;

	while (*(unit + 1) && value >= 1024.0) {
		value /= 1024.0;
		unit++;
	}

	return __pakfire_string_format(dst, length, *unit, value);
}

int pakfire_format_speed(char* dst, size_t length, double value) {
	const char* units[] = {
		"%4.0fB/s",
		"%4.0fkB/s",
		"%4.1fMB/s",
		"%4.1fGB/s",
		"%4.1fTB/s",
		NULL
	};
	const char** unit = units;

	while (*(unit + 1) && value >= 1024.0) {
		value /= 1024.0;
		unit++;
	}

	return __pakfire_string_format(dst, length, *unit, value);
}

int __pakfire_strftime(char* buffer, const size_t length,
		const char* format, const time_t t) {
	struct tm* tm = gmtime(&t);
	int r;

	// Format time
	r = strftime(buffer, length, format, tm);
	if (r == 0)
		return 1;

	return 0;
}

int __pakfire_strftime_now(char* buffer, size_t length, const char* format) {
	// Fetch the current time
	const time_t t = time(NULL);
	if (t < 0)
		return 1;

	return __pakfire_strftime(buffer, length, format, t);
}

int __pakfire_format_time(char* buffer, const size_t length, const time_t t) {
	const char* format = NULL;

	// Values smaller than zero for t are invalid
	if (t < 0)
		return 1;

	if (t >= 86400)
		format = "%dd%Hh%Mm";
	else if (t >= 3600)
		format = "%Hh%Mm%Ss";
	else if (t >= 60)
		format = "%Mm%Ss";
	else
		format = "%Ss";

	return __pakfire_strftime(buffer, length, format, t);
}

size_t pakfire_string_parse_bytes(const char* s) {
	const char* units = "kMGT";
	char* unit = NULL;

	// Read the numeric value
	size_t bytes = strtoul(s, &unit, 10);

	// Return bytes if there is no unit
	if (!unit || !*unit)
		return bytes;

	// Is the unit of the correct length?
	const size_t length = strlen(unit);
	if (length > 1)
		goto ERROR;

	// Walk through all units and find a match
	for (const char* u = units; *u; u++) {
		bytes *= 1024;

		// End if the unit matches
		if (*unit == *u)
			return bytes;
	}

ERROR:
	errno = EINVAL;
	return 0;
}

int pakfire_string_is_url(const char* s) {
	static const char* known_schemas[] = {
		"https://",
		"http://",
		"file://",
		NULL,
	};

	for (const char** schema = known_schemas; *schema; schema++) {
		if (pakfire_string_startswith(s, *schema))
			return 1;
	}

	return 0;
}
