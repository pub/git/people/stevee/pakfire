/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <Python.h>

#include <pakfire/problem.h>
#include <pakfire/solution.h>

#include "problem.h"
#include "solution.h"

static ProblemObject* Problem_new_core(PyTypeObject* type, struct pakfire_problem* problem) {
	ProblemObject* self = (ProblemObject *)type->tp_alloc(type, 0);
	if (self) {
		self->problem = problem;
	}

	return self;
}

PyObject* new_problem(struct pakfire_problem* problem) {
	ProblemObject* p = Problem_new_core(&ProblemType, problem);

	return (PyObject*)p;
}

static PyObject* Problem_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	ProblemObject* self = Problem_new_core(type, NULL);

	return (PyObject *)self;
}

static void Problem_dealloc(ProblemObject* self) {
	pakfire_problem_unref(self->problem);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static int Problem_init(ProblemObject* self, PyObject* args, PyObject* kwds) {
	return 0;
}

static PyObject* Problem_repr(ProblemObject* self) {
	const char* string = pakfire_problem_to_string(self->problem);

	return PyUnicode_FromFormat("<_pakfire.Problem %s>", string);
}

static PyObject* Problem_string(ProblemObject* self) {
	const char* string = pakfire_problem_to_string(self->problem);

	return PyUnicode_FromString(string);
}

static PyObject* Problem_get_solutions(ProblemObject* self) {
	struct pakfire_solution* solution = NULL;
	PyObject* solution_obj = NULL;
	int r;

	PyObject* list = PyList_New(0);
	if (!list)
		return NULL;

	for (;;) {
		r = pakfire_problem_next_solution(self->problem, &solution);
		if (r)
			goto ERROR;

		// No more solutions
		if (!solution)
			break;

		// Create a new Solution object
		solution_obj = new_solution(solution);
		if (!solution_obj) {
			pakfire_solution_unref(solution);
			goto ERROR;
		}

		// Append the solution to the list
		r = PyList_Append(list, solution_obj);
		if (r) {
			pakfire_solution_unref(solution);
			goto ERROR;
		}

		Py_DECREF(solution_obj);
	}

	return list;

ERROR:
	Py_DECREF(list);
	return NULL;
}

static struct PyMethodDef Problem_methods[] = {
	{ NULL }
};

static struct PyGetSetDef Problem_getsetters[] = {
	{
		"solutions",
		(getter)Problem_get_solutions,
		NULL,
		NULL,
		NULL,
	},
	{ NULL },
};

PyTypeObject ProblemType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	tp_name:            "_pakfire.Problem",
	tp_basicsize:       sizeof(ProblemObject),
	tp_flags:           Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	tp_new:             Problem_new,
	tp_dealloc:         (destructor)Problem_dealloc,
	tp_init:            (initproc)Problem_init,
	tp_doc:             "Problem object",
	tp_methods:         Problem_methods,
	tp_getset:          Problem_getsetters,
	tp_repr:            (reprfunc)Problem_repr,
	tp_str:             (reprfunc)Problem_string,
};
