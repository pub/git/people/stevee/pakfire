/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <pakfire/string.h>

#include "../testsuite.h"

static int test_string_set(const struct test* t) {
	// Allocate a small buffer
	char buffer[4];

	// Write a string into a buffer which just has just enough space
	ASSERT_SUCCESS(pakfire_string_set(buffer, "ABC"));
	ASSERT_STRING_EQUALS(buffer, "ABC");

	// Write a string which would not fit
	ASSERT_ERRNO(pakfire_string_set(buffer, "1234"), ENOMEM);
	ASSERT_STRING_EQUALS(buffer, "123");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_startswith(const struct test* t) {
	ASSERT_TRUE(pakfire_string_startswith("ABC", "A"));
	ASSERT_FALSE(pakfire_string_startswith("ABC", "B"));

	// Check for invalid inputs
	ASSERT_ERRNO(pakfire_string_startswith("ABC", NULL), EINVAL);
	ASSERT_ERRNO(pakfire_string_startswith(NULL, "ABC"), EINVAL);
	ASSERT_ERRNO(pakfire_string_startswith(NULL, NULL), EINVAL);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_endswith(const struct test* t) {
	ASSERT_TRUE(pakfire_string_endswith("ABC", "C"));
	ASSERT_FALSE(pakfire_string_endswith("ABC", "B"));

	// Check for invalid inputs
	ASSERT_ERRNO(pakfire_string_endswith("ABC", NULL), EINVAL);
	ASSERT_ERRNO(pakfire_string_endswith(NULL, "ABC"), EINVAL);
	ASSERT_ERRNO(pakfire_string_endswith(NULL, NULL), EINVAL);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_matches(const struct test* t) {
	ASSERT_TRUE(pakfire_string_matches("ABC", "A"));
	ASSERT_TRUE(pakfire_string_matches("ABC", "B"));
	ASSERT_TRUE(pakfire_string_matches("ABC", "C"));
	ASSERT_TRUE(pakfire_string_matches("ABC", "AB"));
	ASSERT_TRUE(pakfire_string_matches("ABC", "BC"));
	ASSERT_TRUE(pakfire_string_matches("ABC", "ABC"));
	ASSERT_FALSE(pakfire_string_matches("ABC", "D"));
	ASSERT_FALSE(pakfire_string_matches("ABC", "ABCD"));

	// Check for invalid inputs
	ASSERT_ERRNO(pakfire_string_matches("ABC", NULL), EINVAL);
	ASSERT_ERRNO(pakfire_string_matches(NULL, "ABC"), EINVAL);
	ASSERT_ERRNO(pakfire_string_matches(NULL, NULL), EINVAL);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_partition(const struct test* t) {
	char* part1;
	char* part2;

	// Regular case
	int r = pakfire_string_partition("ABC:DEF", ":", &part1, &part2);
	ASSERT(r == 0);
	ASSERT_STRING_EQUALS(part1, "ABC");
	ASSERT_STRING_EQUALS(part2, "DEF");

	free(part1);
	free(part2);

	// No delimiter
	r = pakfire_string_partition("ABCDEF", ":", &part1, &part2);
	ASSERT(r == 1);
	ASSERT(part1 == NULL);
	ASSERT(part2 == NULL);

	// Nothing after the delimiter
	r = pakfire_string_partition("ABC:", ":", &part1, &part2);
	ASSERT(r == 0);
	ASSERT_STRING_EQUALS(part1, "ABC");
	ASSERT_STRING_EQUALS(part2, "");

	free(part1);
	free(part2);

	// Nothing before the delimiter
	r = pakfire_string_partition(":ABC", ":", &part1, &part2);
	ASSERT(r == 0);
	ASSERT_STRING_EQUALS(part1, "");
	ASSERT_STRING_EQUALS(part2, "ABC");

	free(part1);
	free(part2);

	// Multi-character delimiter
	r = pakfire_string_partition("ABC:-:DEF", ":-:", &part1, &part2);
	ASSERT(r == 0);
	ASSERT_STRING_EQUALS(part1, "ABC");
	ASSERT_STRING_EQUALS(part2, "DEF");

	free(part1);
	free(part2);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_replace(const struct test* t) {
	const char* result = pakfire_string_replace(
		"ABCABCABCABC", "AB", "CC"
	);
	ASSERT_STRING_EQUALS(result, "CCCCCCCCCCCC");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_split(const struct test* t) {
	char** result = pakfire_string_split(NULL, 'X');

	// Must return on invalid input
	ASSERT_ERRNO(!result, EINVAL);

	// Split a string
	result = pakfire_string_split("ABCXABCXABC", 'X');
	ASSERT(result);

	ASSERT_STRING_EQUALS(result[0], "ABC");
	ASSERT_STRING_EQUALS(result[1], "ABC");
	ASSERT_STRING_EQUALS(result[2], "ABC");
	ASSERT_NULL(result[3]);

	// Split a string withtout the delimiter
	result = pakfire_string_split("ABCABC", 'X');
	ASSERT(result);

	ASSERT_STRING_EQUALS(result[0], "ABCABC");
	ASSERT_NULL(result[1]);

	// String with only delimiters
	result = pakfire_string_split("XXXX", 'X');
	ASSERT(result);

	ASSERT_STRING_EQUALS(result[0], "");
	ASSERT_STRING_EQUALS(result[1], "");
	ASSERT_STRING_EQUALS(result[2], "");
	ASSERT_STRING_EQUALS(result[3], "");
	ASSERT_NULL(result[4]);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_join(const struct test* t) {
	char* s = NULL;

	// Some test elements
	char* elements1[] = {
		"A",
		"B",
		"C",
		NULL,
	};

	// Join with newline
	s = pakfire_string_join(elements1, "\n");
	ASSERT_STRING_EQUALS(s, "A\nB\nC");

	if (s) {
		free(s);
		s = NULL;
	}

	// Join with empty delimiter
	s = pakfire_string_join(elements1, "");
	ASSERT_STRING_EQUALS(s, "ABC");

	if (s) {
		free(s);
		s = NULL;
	}

	char* elements2[] = {
		"",
		"",
		"",
		NULL,
	};

	// Join list with empty elements
	s = pakfire_string_join(elements2, "X");
	ASSERT_STRING_EQUALS(s, "XX");

	if (s) {
		free(s);
		s = NULL;
	}

	// Invalid inputs
	s = pakfire_string_join(NULL, NULL);
	ASSERT_ERRNO(!s, EINVAL);

	s = pakfire_string_join(elements1, NULL);
	ASSERT_ERRNO(!s, EINVAL);

	s = pakfire_string_join(NULL, "\n");
	ASSERT_ERRNO(!s, EINVAL);

	char* elements3[] = {
		NULL,
	};

	// Join empty elements
	ASSERT_NULL(pakfire_string_join(elements3, "\n"));

	return EXIT_SUCCESS;

FAIL:
	if (s)
		free(s);

	return EXIT_FAILURE;
}

static int test_format_size(const struct test* t) {
	char buffer[128];
	char small_buffer[2];
	int r;

	ASSERT_SUCCESS(pakfire_format_size(buffer, 0));
	ASSERT_STRING_EQUALS(buffer, "0 ");

	ASSERT_SUCCESS(pakfire_format_size(buffer, 1024));
	ASSERT_STRING_EQUALS(buffer, "1k");

	ASSERT_SUCCESS(pakfire_format_size(buffer, 1024 * 1024) );
	ASSERT_STRING_EQUALS(buffer, "1.0M");

	ASSERT_ERRNO(pakfire_format_size(small_buffer, 0), ENOMEM);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_parse_bytes(const struct test* t) {
	// Zero without unit
	ASSERT(pakfire_string_parse_bytes("0") == (size_t)0);

	// Zero with valid units
	ASSERT(pakfire_string_parse_bytes("0k") == (size_t)0);
	ASSERT(pakfire_string_parse_bytes("0M") == (size_t)0);
	ASSERT(pakfire_string_parse_bytes("0G") == (size_t)0);
	ASSERT(pakfire_string_parse_bytes("0T") == (size_t)0);

	// Zero with invalid units
	ASSERT_ERRNO(pakfire_string_parse_bytes("0X")  == (size_t)0, EINVAL);
	ASSERT_ERRNO(pakfire_string_parse_bytes("0XX") == (size_t)0, EINVAL);
	ASSERT_ERRNO(pakfire_string_parse_bytes("0MM") == (size_t)0, EINVAL);

	// One with valid units
	ASSERT(pakfire_string_parse_bytes("1")  == (size_t)1);
	ASSERT(pakfire_string_parse_bytes("1k") == (size_t)1024);
	ASSERT(pakfire_string_parse_bytes("1M") == (size_t)1024 * 1024);
	ASSERT(pakfire_string_parse_bytes("1G") == (size_t)1024 * 1024 * 1024);
	ASSERT(pakfire_string_parse_bytes("1T") == (size_t)1024 * 1024 * 1024 * 1024);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_string_set);
	testsuite_add_test(test_string_startswith);
	testsuite_add_test(test_string_endswith);
	testsuite_add_test(test_string_matches);
	testsuite_add_test(test_string_partition);
	testsuite_add_test(test_string_replace);
	testsuite_add_test(test_string_split);
	testsuite_add_test(test_string_join);
	testsuite_add_test(test_format_size);
	testsuite_add_test(test_parse_bytes);

	return testsuite_run(argc, argv);
}
