/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_KEYSTORE_H
#define PAKFIRE_KEYSTORE_H

#ifdef PAKFIRE_PRIVATE

#include <gpgme.h>

#include <pakfire/pakfire.h>

int pakfire_keystore_init(struct pakfire* pakfire, gpgme_ctx_t* ctx);
int pakfire_keystore_destroy(struct pakfire* pakfire, gpgme_ctx_t* ctx);

#endif /* /PAKFIRE_PRIVATE */

#endif /* PAKFIRE_KEYSTORE_H */
