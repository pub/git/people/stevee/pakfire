/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_MOUNT_H
#define PAKFIRE_MOUNT_H

#ifdef PAKFIRE_PRIVATE

#include <pakfire/pakfire.h>

int pakfire_bind(struct pakfire* pakfire, const char* src, const char* dst, int flags);

int pakfire_mount_list(struct pakfire* pakfire);

int pakfire_mount_all(struct pakfire* pakfire);

#endif /* PAKFIRE_PRIVATE */

#endif /* PAKFIRE_MOUNT_H */
