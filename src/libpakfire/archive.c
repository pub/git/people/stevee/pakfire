/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/sendfile.h>
#include <sys/stat.h>

// libarchive
#include <archive.h>
#include <archive_entry.h>

// JSON-C
#include <json.h>

#include <pakfire/archive.h>
#include <pakfire/compress.h>
#include <pakfire/dependencies.h>
#include <pakfire/digest.h>
#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/i18n.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/private.h>
#include <pakfire/repo.h>
#include <pakfire/scriptlet.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define MAX_SCRIPTLETS 9

struct pakfire_archive {
	struct pakfire* pakfire;
	int nrefs;

	char path[PATH_MAX];
	FILE* f;
	struct stat stat;

	struct pakfire_package* package;

	// metadata
	unsigned int format;
	struct json_object* metadata;

	struct pakfire_filelist* filelist;

	// Scriptlets
	struct pakfire_scriptlet* scriptlets[MAX_SCRIPTLETS];
	unsigned int num_scriptlets;

	// Digests
	struct pakfire_digests digests;

	// Verify Status
	int verify;
};

static FILE* pakfire_archive_clone_file(struct pakfire_archive* archive) {
	int fd = fileno(archive->f);
	if (fd < 0) {
		ERROR(archive->pakfire, "Could not fetch the archive's file descriptor: %m\n");
		return NULL;
	}

	// Duplicate the file descriptor
	fd = dup(fd);
	if (fd < 0) {
		ERROR(archive->pakfire, "Could not duplicate the file descriptor: %m\n");
		return NULL;
	}

	// Re-open a file handle
	return fdopen(fd, "r");
}

static int pakfire_archive_compute_digests(struct pakfire_archive* archive) {
	int r;

	// Start reading at the beginning
	rewind(archive->f);

	// Calculate digest
	r = pakfire_digests_compute_from_file(archive->pakfire, &archive->digests,
			PAKFIRE_ARCHIVE_CHECKSUM, archive->f);
	if (r)
		ERROR(archive->pakfire, "Could not calculate digest of %s: %m\n", archive->path);

	return r;
}

/*
	A helper function to close the archive and reset our data structures
*/
static void close_archive(struct pakfire_archive* archive, struct archive* a) {
	if (a)
		archive_read_free(a);
}

/*
	A helper function that opens the archive for reading
*/
static struct archive* open_archive(struct pakfire_archive* archive, FILE* f) {
	// If no special file descriptor has been given we use the open one
	if (!f)
		f = archive->f;

	// Create a new archive object
	struct archive* a = archive_read_new();
	if (!a)
		return NULL;

	// Archives must be uncompressed tarballs
	archive_read_support_format_tar(a);

	// Archives are compressed using Zstandard
	archive_read_support_filter_zstd(a);

	// Start reading from the beginning
	rewind(f);

	// Try opening the archive file
	int r = archive_read_open_FILE(a, f);
	if (r) {
		ERROR(archive->pakfire, "Could not open archive %s: %s\n",
			archive->path, archive_error_string(a));
		goto ERROR;
	}

	// Success
	return a;

ERROR:
	close_archive(archive, a);

	return NULL;
}

static int pakfire_archive_walk(struct pakfire_archive* archive,
		pakfire_walk_callback callback, pakfire_walk_filter_callback filter_callback, void* data) {
	int r;

	// Open the archive file
	struct archive* a = open_archive(archive, NULL);
	if (!a)
		return 1;

	// Walk through the archive
	r = pakfire_walk(archive->pakfire, a, callback, filter_callback, data);

	// Close the archive
	if (a)
		close_archive(archive, a);

	return r;
}

static void pakfire_archive_free(struct pakfire_archive* archive) {
	// Close the file
	if (archive->f)
		fclose(archive->f);

	// Free scriptlets
	for (unsigned int i = 0; i < archive->num_scriptlets; i++)
		pakfire_scriptlet_unref(archive->scriptlets[i]);

	if (archive->filelist)
		pakfire_filelist_unref(archive->filelist);
	if (archive->package)
		pakfire_package_unref(archive->package);
	if (archive->metadata)
		json_object_put(archive->metadata);
	pakfire_unref(archive->pakfire);
	free(archive);
}

static int pakfire_archive_create(struct pakfire_archive** archive, struct pakfire* pakfire) {
	struct pakfire_archive* a = calloc(1, sizeof(*a));
	if (!a)
		return ENOMEM;

	a->pakfire = pakfire_ref(pakfire);
	a->nrefs = 1;

	*archive = a;
	return 0;
}

PAKFIRE_EXPORT struct pakfire_archive* pakfire_archive_ref(struct pakfire_archive* archive) {
	++archive->nrefs;

	return archive;
}

PAKFIRE_EXPORT struct pakfire_archive* pakfire_archive_unref(struct pakfire_archive* archive) {
	if (--archive->nrefs > 0)
		return archive;

	pakfire_archive_free(archive);

	return NULL;
}

static struct pakfire_package* pakfire_archive_get_package(struct pakfire_archive* archive) {
	if (!archive->package) {
		int r = pakfire_archive_make_package(archive, NULL, &archive->package);
		if (r)
			return NULL;
	}

	return pakfire_package_ref(archive->package);
}

// Metadata

static int pakfire_archive_parse_json_metadata(struct pakfire_archive* archive,
		const char* data, const size_t length) {
	int r = 1;

	// Create tokener
	struct json_tokener* tokener = json_tokener_new();
	if (!tokener) {
		ERROR(archive->pakfire, "Could not allocate JSON tokener: %m\n");
		goto ERROR;
	}

	// Parse JSON from buffer
	archive->metadata = json_tokener_parse_ex(tokener, data, length);
	if (!archive->metadata) {
		enum json_tokener_error error = json_tokener_get_error(tokener);

		ERROR(archive->pakfire, "JSON parsing error: %s\n",
			json_tokener_error_desc(error));
		goto ERROR;
	}

	DEBUG(archive->pakfire, "Successfully parsed package metadata:\n%s\n",
		json_object_to_json_string_ext(archive->metadata,
			JSON_C_TO_STRING_PRETTY|JSON_C_TO_STRING_PRETTY_TAB));

	// Success
	r = 0;

ERROR:
	if (tokener)
		json_tokener_free(tokener);

	return r;
}

static int pakfire_archive_parse_format(struct pakfire_archive* archive,
		const char* data, const size_t length) {
	// Check if format has already been set
	if (archive->format) {
		ERROR(archive->pakfire, "Archive format has already been parsed\n");
		errno = EINVAL;
		return 1;
	}

	// Parse the format
	archive->format = strtoul(data, NULL, 10);

	switch (archive->format) {
		// Handle all supported formats
		case 6:
			break;

		// Break on anything else
		default:
			ERROR(archive->pakfire, "This version of Pakfire does not support "
				"archive format %d\n", archive->format);
			errno = EINVAL;
			return 1;
	}

	DEBUG(archive->pakfire, "Archive format is %d\n", archive->format);

	return 0;
}

static int pakfire_archive_parse_scriptlet(struct pakfire_archive* archive,
		const char* path, const char* data, const size_t length) {
	struct pakfire_scriptlet* scriptlet = NULL;
	const char* type = NULL;
	int r;

	// Check for any available space
	if (archive->num_scriptlets >= MAX_SCRIPTLETS) {
		ERROR(archive->pakfire, "Too many scriptlets\n");
		errno = ENOBUFS;
		return 1;
	}

	// Determine type
	type = pakfire_path_relpath(".scriptlets/", path);
	if (!type) {
		ERROR(archive->pakfire, "Could not determine the scriptlet type from '%s'\n", path);
		errno = EINVAL;
		return 1;
	}

	// Allocate a scriptlet
	r = pakfire_scriptlet_create(&scriptlet, archive->pakfire, type, data, length);
	if (r)
		return r;

	// Store scriptlet
	archive->scriptlets[archive->num_scriptlets++] = scriptlet;

	return 0;
}

static int __pakfire_archive_read_metadata(struct pakfire* pakfire, struct archive* a,
		struct archive_entry* entry, void* p) {
	struct pakfire_archive* archive = (struct pakfire_archive*)p;

	char* data = NULL;
	size_t length = 0;
	int r;

	const char* path = archive_entry_pathname(entry);

	DEBUG(pakfire, "Reading metadata file: %s\n", path);

	// Load the file into memory
	r = pakfire_archive_copy_data_to_buffer(archive->pakfire, a, entry, &data, &length);
	if (r) {
		ERROR(archive->pakfire, "Could not read data from archive: %s\n",
			archive_error_string(a));
		goto ERROR;
	}

	// Format >= 6
	if (archive->format >= 6) {
		// Parse PKGINFO
		if (strcmp(path, ".PKGINFO") == 0) {
			r = pakfire_archive_parse_json_metadata(archive, data, length);
			if (r)
				goto ERROR;

		// Parse scriptlets
		} else if (pakfire_string_startswith(path, ".scriptlets/")) {
			r = pakfire_archive_parse_scriptlet(archive, path, data, length);
			if (r)
				goto ERROR;
		}

	// pakfire-format
	} else if (strcmp(path, "pakfire-format") == 0) {
		r = pakfire_archive_parse_format(archive, data, length);
		if (r)
			goto ERROR;
	}

ERROR:
	if (data)
		free(data);

	return r;
}

static int __pakfire_archive_filter_metadata(struct pakfire* pakfire,
		struct archive* a, struct archive_entry* entry, void* p) {
	struct pakfire_archive* archive = (struct pakfire_archive*)p;

	const char* path = archive_entry_pathname(entry);

	// Format >= 6
	if (archive->format >= 6) {
		// Anything that starts with "." is a metadata file
		if (*path == '.')
			return PAKFIRE_WALK_OK;

		// Otherwise, the payload begins
		return PAKFIRE_WALK_END;

	// The pakfire-format file is part of the metadata
	} else if (strcmp(path, "pakfire-format") == 0) {
		return PAKFIRE_WALK_OK;
	}

	// Unknown file
	return PAKFIRE_WALK_ERROR;
}

static int pakfire_archive_read_metadata(struct pakfire_archive* archive) {
	int r;

	// Check if the archive file actually has any contect
	if (!archive->stat.st_size) {
		ERROR(archive->pakfire, "Trying to open an empty archive file\n");
		errno = EINVAL;
		return 1;
	}

	DEBUG(archive->pakfire, "Reading archive metadata...\n");

	// Walk through the archive
	r = pakfire_archive_walk(archive, __pakfire_archive_read_metadata,
		__pakfire_archive_filter_metadata, archive);
	if (r)
		return r;

	// Check if we could successfully read something
	if (!archive->format) {
		ERROR(archive->pakfire, "Archive has an unknown format\n");
		errno = EINVAL;
		return 1;
	}

	// Check if we have read some metadata
	if (!archive->metadata) {
		ERROR(archive->pakfire, "Archive has no metadata\n");
		errno = EINVAL;
		return 1;
	}

	return 0;
}

static int pakfire_archive_try_open(struct pakfire_archive* archive, const char* path) {
	int r;

	if (!path) {
		errno = EINVAL;
		return 1;
	}

	DEBUG(archive->pakfire, "Opening archive %s\n", path);

	// Store path
	pakfire_string_set(archive->path, path);

	// Open the file (and keep the file descriptor open)
	archive->f = fopen(archive->path, "r");
	if (!archive->f)
		return 1;

	// Let the kernel know, that we will read the file sequentially
	r = posix_fadvise(fileno(archive->f), 0, 0, POSIX_FADV_SEQUENTIAL);
	if (r) {
		ERROR(archive->pakfire, "posix_fadvise() failed: %m\n");
		goto ERROR;
	}

	// Call stat() on f
	r = fstat(fileno(archive->f), &archive->stat);
	if (r) {
		ERROR(archive->pakfire, "Could not stat archive: %m\n");
		goto ERROR;
	}

	// Read all package metadata
	r = pakfire_archive_read_metadata(archive);
	if (r) {
		ERROR(archive->pakfire, "Could not open archive: %m\n");
		goto ERROR;
	}

ERROR:
	return r;
}

PAKFIRE_EXPORT int pakfire_archive_open(struct pakfire_archive** archive, struct pakfire* pakfire, const char* path) {
	int r = pakfire_archive_create(archive, pakfire);
	if (r)
		return r;

	r = pakfire_archive_try_open(*archive, path);
	if (r)
		goto ERROR;

	return 0;

ERROR:
	pakfire_archive_unref(*archive);
	*archive = NULL;

	return r;
}

static struct json_object* pakfire_archive_metadata_get_object(
		struct pakfire_archive* archive, const char* key1, const char* key2) {
	struct json_object* object = archive->metadata;
	int r;

	const char* keys[] = {
		key1,
		key2,
		NULL,
	};

	// Walk through all keys
	for (const char** key = keys; *key; key++) {
		// Try finding a matching JSON object
		r = json_object_object_get_ex(object, *key, &object);
		if (!r) {
			DEBUG(archive->pakfire, "Could not find JSON object at '%s': %m\n", *key);
			break;
		}
	}

	return object;
}

static const char* pakfire_archive_metadata_get(
		struct pakfire_archive* archive, const char* key1, const char* key2) {
	// Try finding an object
	struct json_object* object = pakfire_archive_metadata_get_object(archive, key1, key2);
	if (!object)
		return NULL;

	// Return the object as string
	return json_object_get_string(object);
}

static int64_t pakfire_archive_metadata_get_int64(
		struct pakfire_archive* archive, const char* key1, const char* key2) {
	// Try finding an object
	struct json_object* object = pakfire_archive_metadata_get_object(archive, key1, key2);
	if (!object)
		return 0;

	// Return the object as integer
	return json_object_get_int64(object);
}

static int __pakfire_archive_filter_payload(struct pakfire* pakfire,
		struct archive* a, struct archive_entry* entry, void* p) {
	const char* path = archive_entry_pathname(entry);
	if (!path)
		return PAKFIRE_WALK_ERROR;

	switch (*path) {
		case 'p':
			if (strcmp(path, "pakfire-format") == 0)
				return PAKFIRE_WALK_SKIP;

			// Fallthrough

		case '.':
			return PAKFIRE_WALK_SKIP;

		// The first file that isn't metadata, so we are done calling the filter callback
		default:
			return PAKFIRE_WALK_DONE;
	}
}

/*
	Read files from the archive
*/
struct pakfire_archive_read_cookie {
	// A reference to the archive
	struct pakfire_archive* archive;

	// A copy of the underlying file descriptor
	FILE* f;

	// The opened archive
	struct archive* a;
};

static ssize_t __pakfire_archive_cookie_read(void* c, char* buffer, size_t size) {
	struct pakfire_archive_read_cookie* cookie = (struct pakfire_archive_read_cookie*)c;

	// Read the data directly from the archive
	return archive_read_data(cookie->a, buffer, size);
}

static int __pakfire_archive_cookie_close(void* c) {
	struct pakfire_archive_read_cookie* cookie = (struct pakfire_archive_read_cookie*)c;

	if (cookie->archive)
		pakfire_archive_unref(cookie->archive);
	if (cookie->a)
		archive_read_free(cookie->a);
	if (cookie->f)
		fclose(cookie->f);

	// Free the cookie
	free(cookie);

	return 0;
}

static cookie_io_functions_t pakfire_archive_read_functions = {
	.read  = __pakfire_archive_cookie_read,
	.close = __pakfire_archive_cookie_close,
};

PAKFIRE_EXPORT FILE* pakfire_archive_read(struct pakfire_archive* archive, const char* path) {
	struct pakfire_archive_read_cookie* cookie = NULL;
	FILE* f = NULL;
	int r;

	// Check if path is absolute
	if (!path || *path != '/') {
		errno = EINVAL;
		return NULL;
	}

	// Strip leading / from filenames, because tarballs don't use any leading slashes
	path = pakfire_path_relpath("/", path);
	if (!path)
		return NULL;

	// Allocate a cookie
	cookie = calloc(1, sizeof(*cookie));
	if (!cookie) {
		ERROR(archive->pakfire, "Could not allocate a cookie: %m\n");
		goto ERROR;
	}

	// Store a reference to the archive
	cookie->archive = pakfire_archive_ref(archive);

	// Clone the archive file descriptor to read the file independently
	cookie->f = pakfire_archive_clone_file(archive);
	if (!cookie->f) {
		ERROR(archive->pakfire, "Could not duplicate file descriptor for %s: %m\n",
			archive->path);
		goto ERROR;
	}

	// Open the archive
	cookie->a = open_archive(archive, cookie->f);
	if (!cookie->a)
		goto ERROR;

	// Tries to find a matching file in the archive
	int __pakfire_archive_read_filter(struct pakfire* pakfire, struct archive* a,
			struct archive_entry* e, void* data) {
		// Stop reading the archive after we have found our file
		if (f)
			return PAKFIRE_WALK_END;

		// Fetch path
		const char* p = archive_entry_pathname(e);
		if (!p)
			return PAKFIRE_WALK_ERROR;

		// We found a match
		if (strcmp(path, p) == 0)
			return PAKFIRE_WALK_OK;

		// Otherwise we skip the file
		return PAKFIRE_WALK_SKIP;
	}

	// Reads a matching file into memory
	int __pakfire_archive_read(struct pakfire* pakfire, struct archive* a,
			struct archive_entry* e, void* data) {
		// Create a file descriptor
		f = fopencookie(cookie, "r", pakfire_archive_read_functions);
		if (!f) {
			ERROR(pakfire, "Could not open /%s: %m\n", path);
			return PAKFIRE_WALK_ERROR;
		}

		return PAKFIRE_WALK_DONE;
	}

	// Walk through the archive
	r = pakfire_walk(archive->pakfire, cookie->a,
		__pakfire_archive_read, __pakfire_archive_read_filter, NULL);
	if (r)
		goto ERROR;

	// Nothing found
	if (!f) {
		ERROR(archive->pakfire, "Could not find /%s\n", path);

		// No such file or directory
		errno = ENOENT;
		goto ERROR;
	}

	return f;

ERROR:
	if (cookie)
		__pakfire_archive_cookie_close(cookie);

	return NULL;
}

int pakfire_archive_copy(struct pakfire_archive* archive, const char* path) {
	if (!path) {
		errno = EINVAL;
		return 1;
	}

	// Determine the file size
	ssize_t size = pakfire_archive_get_size(archive);
	if (size < 0)
		return 1;

	DEBUG(archive->pakfire, "Copying %s to %s...\n", archive->path, path);

	// Ensure we copy from the very beginning
	rewind(archive->f);

	// Ensure the parent directory exists
	pakfire_mkparentdir(path, 0755);

	// Open destination file
	FILE* f = fopen(path, "w");
	if (!f)
		return 1;

	int r = 1;

	// Copy everything
	ssize_t bytes_written = sendfile(fileno(f), fileno(archive->f), NULL, size);
	if (bytes_written < size) {
		ERROR(archive->pakfire, "Could not copy archive (%zu byte(s) written): %m\n",
			bytes_written);
		goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	fclose(f);

	// Delete the file on error
	if (r)
		unlink(path);

	return r;
}

static int pakfire_archive_link(struct pakfire_archive* archive, const char* path) {
	int r;

	// Check if path is set
	if (!path) {
		errno = EINVAL;
		return 1;
	}

	DEBUG(archive->pakfire, "Linking %s to %s...\n", archive->path, path);

	// Delete the destination file (if it exists)
	unlink(path);

	// Create the new link
	r = link(archive->path, path);
	if (r) {
		DEBUG(archive->pakfire, "Could not create hardlink %s: %m\n", path);
		return r;
	}

	return 0;
}

int pakfire_archive_link_or_copy(struct pakfire_archive* archive, const char* path) {
	int r;

	// Try to create a hardlink
	r = pakfire_archive_link(archive, path);
	if (r) {
		switch (errno) {
			// Try to copy the file if we could not create a hardlink
			case EPERM:
				r = pakfire_archive_copy(archive, path);

			default:
				break;
		}
	}

	return r;
}

static int __pakfire_archive_extract(struct pakfire_archive* archive, int flags) {
	struct pakfire_filelist* filelist = NULL;
	struct pakfire_package* pkg = NULL;
	struct archive* a = NULL;
	char prefix[PATH_MAX] = "";
	int r = 1;

	// Fetch package
	pkg = pakfire_archive_get_package(archive);
	if (!pkg)
		goto ERROR;

	// Fetch NEVRA
	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

	DEBUG(archive->pakfire, "Extracting %s\n", archive->path);

	// Set prefix for source packages
	if (pakfire_package_is_source(pkg)) {
		r = pakfire_string_format(prefix, "/usr/src/packages/%s", nevra);
		if (r)
			goto ERROR;
	}

	// Load the filelist (if not done already)
	if (!archive->filelist) {
		r = pakfire_filelist_create(&filelist, archive->pakfire);
		if (r)
			goto ERROR;
	}

	// Open the archive
	a = open_archive(archive, NULL);
	if (!a) {
		r = 1;
		goto ERROR;
	}

	// Extract
	r = pakfire_extract(archive->pakfire, a, archive->stat.st_size,
		filelist, prefix, nevra, __pakfire_archive_filter_payload, flags);
	if (r)
		goto ERROR;

	// Store the filelist permanently
	if (!archive->filelist)
		archive->filelist = pakfire_filelist_ref(filelist);

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);
	if (pkg)
		pakfire_package_unref(pkg);
	if (a)
		close_archive(archive, a);

	return r;
}

PAKFIRE_EXPORT int pakfire_archive_extract(struct pakfire_archive* archive) {
	return __pakfire_archive_extract(archive, 0);
}

PAKFIRE_EXPORT const char* pakfire_archive_get_path(struct pakfire_archive* archive) {
	return archive->path;
}

PAKFIRE_EXPORT unsigned int pakfire_archive_get_format(struct pakfire_archive* archive) {
	return archive->format;
}

static int pakfire_archive_load_filelist(struct pakfire_archive* archive) {
	// Perform a dry-run extraction
	return __pakfire_archive_extract(archive,
		PAKFIRE_EXTRACT_DRY_RUN|PAKFIRE_EXTRACT_NO_PROGRESS);
}

PAKFIRE_EXPORT struct pakfire_filelist* pakfire_archive_get_filelist(struct pakfire_archive* archive) {
	if (!archive->filelist) {
		int r = pakfire_archive_load_filelist(archive);
		if (r)
			return NULL;
	}

	if (!archive->filelist)
		return NULL;

	return pakfire_filelist_ref(archive->filelist);
}

PAKFIRE_EXPORT int pakfire_archive_verify(struct pakfire_archive* archive, int* status) {
	// XXX currently not implemented
	return 0;
}

PAKFIRE_EXPORT ssize_t pakfire_archive_get_size(struct pakfire_archive* archive) {
	return archive->stat.st_size;
}

int pakfire_archive_check_digest(struct pakfire_archive* archive,
		const enum pakfire_digest_types type, const unsigned char* digest, const size_t length) {
	size_t computed_length = 0;
	int r;

	// Compute the digest
	r = pakfire_archive_compute_digests(archive);
	if (r)
		return r;

	// Compare computed with expected digest
	r = pakfire_digests_compare_one(archive->pakfire, &archive->digests,
		type, digest, length);
	if (r) {
		const unsigned char* computed_digest = pakfire_digest_get(
			&archive->digests, type, &computed_length);

		char* expected_hexdigest = __pakfire_hexlify(digest, length);
		char* computed_hexdigest = __pakfire_hexlify(computed_digest, computed_length);

		ERROR(archive->pakfire, "Archive digest does not match for %s:\n", archive->path);
		ERROR(archive->pakfire, "  Expected: %s\n", expected_hexdigest);
		ERROR(archive->pakfire, "  Computed: %s\n", computed_hexdigest);

		if (expected_hexdigest)
			free(expected_hexdigest);
		if (computed_hexdigest)
			free(computed_hexdigest);

		return r;
	}

	return r;
}

static int pakfire_archive_import_filelist_from_json(
		struct pakfire_archive* archive, struct pakfire_package* package) {
	struct json_object* array = NULL;
	int r;

	// Fetch the array with the filelist
	array = pakfire_archive_metadata_get_object(archive, "filelist", NULL);
	if (!array) {
		ERROR(archive->pakfire, "Archive has no filelist: %m\n");
		return 1;
	}

	// Determine the length of the array
	const size_t length = json_object_array_length(array);

	// End here if the array is empty
	if (!length)
		return 0;

	// Walk through all items in this array
	for (unsigned int i = 0; i < length; i++) {
		struct json_object* item = json_object_array_get_idx(array, i);
		if (!item)
			continue;

		// Extract the path value
		const char* path = json_object_get_string(item);
		if (!path)
			continue;

		// Append the file to the package
		r = pakfire_package_append_file(package, path);
		if (r)
			return r;
	}

	return 0;
}

static int pakfire_archive_make_package_from_json(struct pakfire_archive* archive,
		struct pakfire_repo* repo, struct pakfire_package** package) {
	struct pakfire_package* pkg = NULL;
	int r;

	// Calculate digest
	r = pakfire_archive_compute_digests(archive);
	if (r)
		return r;

	// Fetch the most basic package information
	const char* name = pakfire_archive_metadata_get(archive, "name", NULL);
	const char* evr  = pakfire_archive_metadata_get(archive, "evr", NULL);
	const char* arch = pakfire_archive_metadata_get(archive, "arch", NULL);

	// Create a new package object
	r = pakfire_package_create(&pkg, archive->pakfire, repo, name, evr, arch);
	if (r)
		return r;

#ifdef ENABLE_DEBUG
	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

	DEBUG(archive->pakfire, "Created package %s (%p) from archive %p\n",
		nevra, pkg, archive);
#endif

	// Set path
	pakfire_package_set_string(pkg, PAKFIRE_PKG_PATH, archive->path);

	// Set digest
	switch (PAKFIRE_ARCHIVE_CHECKSUM) {
		case PAKFIRE_DIGEST_SHA2_512:
			pakfire_package_set_digest(pkg, PAKFIRE_ARCHIVE_CHECKSUM,
				archive->digests.sha2_512, sizeof(archive->digests.sha2_512));
			break;

		case PAKFIRE_DIGEST_SHA2_256:
			pakfire_package_set_digest(pkg, PAKFIRE_ARCHIVE_CHECKSUM,
				archive->digests.sha2_256, sizeof(archive->digests.sha2_256));
			break;

		case PAKFIRE_DIGEST_UNDEFINED:
			r = 1;
			goto ERROR;
	}

	// Vendor
	const char* vendor = pakfire_archive_metadata_get(archive, "vendor", NULL);
	if (vendor) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_VENDOR, vendor);
		if (r)
			goto ERROR;
	}

	// UUID
	const char* uuid = pakfire_archive_metadata_get(archive, "uuid", NULL);
	if (uuid) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_UUID, uuid);
		if (r)
			goto ERROR;
	}

	// Groups
	const char* groups = pakfire_archive_metadata_get(archive, "groups", NULL);
	if (groups) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_GROUPS, groups);
		if (r)
			goto ERROR;
	}

	// Distribution
	const char* distro = pakfire_archive_metadata_get(archive, "distribution", NULL);
	if (distro) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_DISTRO, distro);
		if (r)
			goto ERROR;
	}

	// Packager
	const char* packager = pakfire_archive_metadata_get(archive, "packager", NULL);
	if (packager) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_PACKAGER, packager);
		if (r)
			goto ERROR;
	}

	// URL
	const char* url = pakfire_archive_metadata_get(archive, "url", NULL);
	if (url) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_URL, url);
		if (r)
			goto ERROR;
	}

	// License
	const char* license = pakfire_archive_metadata_get(archive, "license", NULL);
	if (license) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_LICENSE, license);
		if (r)
			goto ERROR;
	}

	// Summary
	const char* summary = pakfire_archive_metadata_get(archive, "summary", NULL);
	if (summary) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SUMMARY, summary);
		if (r)
			goto ERROR;
	}

	// Description
	const char* description = pakfire_archive_metadata_get(archive, "description", NULL);
	if (description) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_DESCRIPTION, description);
		if (r)
			goto ERROR;
	}

	// Installed package size
	size_t installsize = pakfire_archive_metadata_get_int64(archive, "size", NULL);
	if (installsize) {
		r = pakfire_package_set_num(pkg, PAKFIRE_PKG_INSTALLSIZE, installsize);
		if (r)
			goto ERROR;
	}

	// Download size
	r = pakfire_package_set_num(pkg,
		PAKFIRE_PKG_DOWNLOADSIZE, pakfire_archive_get_size(archive));
	if (r)
		goto ERROR;

	// Build Host
	const char* build_host = pakfire_archive_metadata_get(archive, "build", "host");
	if (build_host) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_BUILD_HOST, build_host);
		if (r)
			goto ERROR;
	}

	// Build ID
	const char* build_id = pakfire_archive_metadata_get(archive, "build", "id");
	if (build_id) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_BUILD_ID, build_id);
		if (r)
			goto ERROR;
	}

	// Build Time
	time_t build_time = pakfire_archive_metadata_get_int64(archive, "build", "time");
	if (build_time) {
		r = pakfire_package_set_num(pkg, PAKFIRE_PKG_BUILD_TIME, build_time);
		if (r)
			goto ERROR;
	}

	// Source package
	const char* source_name = pakfire_archive_metadata_get(archive, "build", "source-name");
	if (source_name) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_NAME, source_name);
		if (r)
			goto ERROR;
	}

	// Source EVR
	const char* source_evr = pakfire_archive_metadata_get(archive, "build", "source-evr");
	if (source_evr) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_EVR, source_evr);
		if (r)
			goto ERROR;
	}

	// Source arch
	const char* source_arch = pakfire_archive_metadata_get(archive, "build", "source-arch");
	if (source_arch) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_ARCH, source_arch);
		if (r)
			goto ERROR;
	}

	// Dependencies
	for (const struct pakfire_dep* dep = pakfire_deps; dep->key; dep++) {
		struct json_object* array = pakfire_archive_metadata_get_object(
			archive, "dependencies", dep->name);
		if (!array)
			continue;

		// Determine the length of the array
		const size_t length = json_object_array_length(array);
		if (!length)
			continue;

		// Walk through all items in this array
		for (unsigned int i = 0; i < length; i++) {
			struct json_object* item = json_object_array_get_idx(array, i);
			if (!item)
				continue;

			// Extract the string value
			const char* string = json_object_get_string(item);
			if (!string)
				continue;

			// Add the dependency to the package
			r = pakfire_package_add_dep(pkg, dep->key, string);
			if (r)
				goto ERROR;
		}
	}

	// Import the filelist
	r = pakfire_archive_import_filelist_from_json(archive, pkg);
	if (r)
		goto ERROR;

	// Success!
	*package = pkg;
	r = 0;

ERROR:
	return r;
}

/*
	Copy all metadata from this archive to the package object
*/
PAKFIRE_EXPORT int pakfire_archive_make_package(struct pakfire_archive* archive,
		struct pakfire_repo* repo, struct pakfire_package** package) {
	struct pakfire_repo* dummy = NULL;
	int r;

	// Use dummy repo if no repository was passed
	if (!repo) {
		dummy = pakfire_get_repo(archive->pakfire, PAKFIRE_REPO_DUMMY);
		if (!dummy)
			return 1;

		repo = dummy;
	}

	// Make package from JSON metadata
	r = pakfire_archive_make_package_from_json(archive, repo, package);

	// Free dummy repository
	if (dummy)
		pakfire_repo_unref(dummy);

	return r;
}

struct pakfire_scriptlet* pakfire_archive_get_scriptlet(
		struct pakfire_archive* archive, const char* type) {
	struct pakfire_scriptlet* scriptlet = NULL;

	for (unsigned int i = 0; i < archive->num_scriptlets; i++) {
		scriptlet = archive->scriptlets[i];

		// Fetch type
		const char* t = pakfire_scriptlet_get_type(scriptlet);

		// Compare type
		if (strcmp(t, type) == 0)
			return pakfire_scriptlet_ref(scriptlet);
	}

	return NULL;
}

/*
	systemd sysusers
*/
static int __pakfire_archive_filter_systemd_sysusers(struct pakfire* pakfire,
		struct archive* a, struct archive_entry* e, void* data) {
	const char* path = archive_entry_pathname(e);

	if (!pakfire_path_match("/usr/lib/sysusers.d/*.conf", path))
		return PAKFIRE_WALK_SKIP;

	return PAKFIRE_WALK_OK;
}

static int pakfire_archive_stream_payload(struct pakfire* pakfire, void* data, int fd) {
	char buffer[1024];

	struct archive* a = (struct archive*)data;

	// Read a block from the input archive
	ssize_t bytes_read = archive_read_data(a, buffer, sizeof(buffer));
	if (bytes_read < 0) {
		ERROR(pakfire, "Could not read from archive: %s\n", archive_error_string(a));
		return 1;
	}

	// We have consumed everything
	if (bytes_read == 0)
		return EOF;

	// Write the data to the output file descriptor
	ssize_t bytes_written = write(fd, buffer, bytes_read);
	if (bytes_written < 0) {
		ERROR(pakfire, "Could not stream output: %m\n");
		return 1;
	}

	return 0;
}

static int __pakfire_archive_handle_systemd_sysusers(struct pakfire* pakfire,
		struct archive* a, struct archive_entry* e, void* data) {
	struct pakfire_jail* jail = NULL;
	char replace[PATH_MAX];
	int r;

	// Fetch path
	const char* path = archive_entry_pathname(e);

	// Format --replace
	r = pakfire_string_format(replace, "--replace=/%s", path);
	if (r)
		goto ERROR;

	const char* argv[] = { "/usr/bin/systemd-sysusers", replace, "-", NULL };

	// Create a new jail
	r = pakfire_jail_create(&jail, pakfire);
	if (r)
		goto ERROR;

	r = pakfire_jail_exec(jail, argv, pakfire_archive_stream_payload, NULL, a,
		PAKFIRE_JAIL_NOENT_OK);

ERROR:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

int pakfire_archive_apply_systemd_sysusers(struct pakfire_archive* archive) {
	pakfire_archive_walk(archive,
			__pakfire_archive_filter_systemd_sysusers, __pakfire_archive_handle_systemd_sysusers, NULL);

	return 0;
}
