/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include <pakfire/i18n.h>
#include <pakfire/pakfire.h>
#include <pakfire/problem.h>
#include <pakfire/request.h>
#include <pakfire/ui.h>

static int pakfire_ui_is_interactive(struct pakfire* pakfire) {
	if (!isatty(STDIN_FILENO))
		return 1;

	if (!isatty(STDOUT_FILENO))
		return 1;

	if (!isatty(STDERR_FILENO))
		return 1;

	return 0;
}

static int pakfire_ui_enter_number(struct pakfire* pakfire, const char* question,
		unsigned int* choice, unsigned int min, unsigned int max) {
	char* line = NULL;
	size_t length = 0;
	char* remainder = NULL;
	int r = 1;

	while (1) {
		// Print question
		printf("%s ", question);

		// Do not wait for any input if the terminal isn't interactive
		if (!pakfire_ui_is_interactive(pakfire))
			break;

		// Wait for the user to enter something
		ssize_t bytes_read = getline(&line, &length, stdin);
		if (bytes_read < 0)
			goto ERROR;

		// Convert input into an integer
		unsigned long int value = strtoul(line, &remainder, 10);

		// The remainder must point to newline
		if (!remainder || *remainder != '\n')
			goto AGAIN;

		// The value must be within bounds
		if (value < min || value > max)
			goto AGAIN;

		// Store choice
		*choice = value;
		r = 0;
		break;

AGAIN:
		printf(_("Invalid value\n"));
	}

ERROR:
	if (line)
		free(line);

	return r;
}

static struct pakfire_solution** pakfire_ui_append_solution(
		struct pakfire_solution** list, struct pakfire_solution* solution) {
	unsigned int length = 0;

	// Count length of list
	if (list) {
		for (struct pakfire_solution** s = list; *s; s++)
			length++;
	}

	// Increase array size
	list = reallocarray(list, length + 2, sizeof(*list));
	if (!list)
		return NULL;

	// Append the new element to the list
	list[length] = pakfire_solution_ref(solution);

	// Terminate the list
	list[length + 1] = NULL;

	return list;
}

int pakfire_ui_pick_solution(struct pakfire* pakfire, struct pakfire_request* request) {
	struct pakfire_problem* problem = NULL;
	struct pakfire_solution** solutions = NULL;
	struct pakfire_solution* solution = NULL;
	unsigned int num_solutions = 0;
	int r;

	// Print a headline
	printf("%s\n", _("One or more problems have occurred solving your request:"));

	// Print all problems
	for (;;) {
		r = pakfire_request_next_problem(request, &problem);
		if (r)
			goto ERROR;

		// No more problems
		if (!problem)
			break;

		const char* s = pakfire_problem_to_string(problem);

		printf("  * %s\n", s);

		// Show a little headline
		printf("    %s\n", _("Possible solutions:"));

		for (;;) {
			r = pakfire_problem_next_solution(problem, &solution);
			if (r)
				goto ERROR;

			// No more solutions
			if (!solution)
				break;

			// Append the solution to the list
			solutions = pakfire_ui_append_solution(solutions, solution);
			if (!solutions)
				goto ERROR;

			s = pakfire_solution_to_string(solution);

			printf("      [%d] %s\n", ++num_solutions, s);
		}

		// Empty line
		printf("\n");
	}

	unsigned int choice = 0;

	// Let the user choose which solution they want
	r = pakfire_ui_enter_number(pakfire, _("Please select a solution:"),
		&choice, 1, num_solutions);
	if (r)
		goto ERROR;

	// Choice is invalid
	if (!choice)
		goto ERROR;

	// Fetch selected solution into the solver
	r = pakfire_request_take_solution(request, solutions[choice - 1]);
	if (r)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (solutions) {
		for (struct pakfire_solution** s = solutions; *s; s++)
			pakfire_solution_unref(*s);
		free(solutions);
	}

	return r;
}
