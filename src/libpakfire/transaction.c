/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

#include <solv/transaction.h>

#include <pakfire/archive.h>
#include <pakfire/db.h>
#include <pakfire/digest.h>
#include <pakfire/downloader.h>
#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/i18n.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/private.h>
#include <pakfire/repo.h>
#include <pakfire/string.h>
#include <pakfire/transaction.h>
#include <pakfire/ui.h>
#include <pakfire/util.h>

struct pakfire_transaction {
	struct pakfire* pakfire;
	int nrefs;

	Transaction* transaction;
	char** userinstalled;

	struct pakfire_archive** archives;
	struct pakfire_package** packages;
	size_t num;
	size_t progress;

	// Callbacks
	struct pakfire_transaction_callbacks {
		// Status
		pakfire_status_callback status;
		void* status_data;
	} callbacks;
};

enum pakfire_actions {
	PAKFIRE_ACTION_NOOP = 0,
	PAKFIRE_ACTION_VERIFY,
	PAKFIRE_ACTION_EXECUTE,
	PAKFIRE_ACTION_PRETRANS,
	PAKFIRE_ACTION_POSTTRANS,
};

enum pakfire_steps {
	PAKFIRE_STEP_IGNORE = 0,
	PAKFIRE_STEP_INSTALL,
	PAKFIRE_STEP_REINSTALL,
	PAKFIRE_STEP_ERASE,
	PAKFIRE_STEP_UPGRADE,
	PAKFIRE_STEP_DOWNGRADE,
	PAKFIRE_STEP_OBSOLETE,
};

static enum pakfire_steps pakfire_transaction_get_step_type(
		struct pakfire_transaction* transaction, struct pakfire_package* pkg) {
	int type = transaction_type(transaction->transaction, pakfire_package_id(pkg),
		SOLVER_TRANSACTION_SHOW_ACTIVE|SOLVER_TRANSACTION_CHANGE_IS_REINSTALL);

	// Translate solver types into our own types
	switch (type) {
		case SOLVER_TRANSACTION_INSTALL:
		case SOLVER_TRANSACTION_MULTIINSTALL:
			return PAKFIRE_STEP_INSTALL;

		case SOLVER_TRANSACTION_REINSTALL:
		case SOLVER_TRANSACTION_MULTIREINSTALL:
			return PAKFIRE_STEP_REINSTALL;

		case SOLVER_TRANSACTION_ERASE:
			return PAKFIRE_STEP_ERASE;

		case SOLVER_TRANSACTION_DOWNGRADE:
			return PAKFIRE_STEP_DOWNGRADE;

		case SOLVER_TRANSACTION_UPGRADE:
			return PAKFIRE_STEP_UPGRADE;

		case SOLVER_TRANSACTION_OBSOLETES:
			return PAKFIRE_STEP_OBSOLETE;

		// Anything we don't care about
		case SOLVER_TRANSACTION_IGNORE:
		case SOLVER_TRANSACTION_REINSTALLED:
		case SOLVER_TRANSACTION_DOWNGRADED:
		default:
				return PAKFIRE_STEP_IGNORE;
	}
}

static void pakfire_transaction_free_archives_and_packages(
		struct pakfire_transaction* transaction) {
	if (transaction->archives) {
		for (unsigned int i = 0; i < transaction->num; i++)
			if (transaction->archives[i])
				pakfire_archive_unref(transaction->archives[i]);
		free(transaction->archives);

		transaction->archives = NULL;
	}

	if (transaction->packages) {
		for (unsigned int i = 0; i < transaction->num; i++)
			if (transaction->packages[i])
				pakfire_package_unref(transaction->packages[i]);
		free(transaction->packages);

		transaction->packages = NULL;
	}
}

static void pakfire_transaction_free(struct pakfire_transaction* transaction) {
	pakfire_transaction_free_archives_and_packages(transaction);

	if (transaction->userinstalled) {
		for (char** userinstalled = transaction->userinstalled; *userinstalled; userinstalled++)
			free(*userinstalled);
		free(transaction->userinstalled);
	}
	transaction_free(transaction->transaction);

	pakfire_unref(transaction->pakfire);
	free(transaction);
}

static int pakfire_transaction_import_transaction(
		struct pakfire_transaction* transaction, Solver* solver) {
	int r;

	// Clone the transaction to keep a copy of it
	Transaction* t = solver_create_transaction(solver);
	if (!t)
		return 1;

	transaction->transaction = t;

	// Order the transaction
	transaction_order(t, 0);

	// Free any previous content
	pakfire_transaction_free_archives_and_packages(transaction);

	// How many steps?
	transaction->num = t->steps.count;

	// Allocate space for packages
	transaction->packages = calloc(transaction->num, sizeof(*transaction->packages));
	if (!transaction->packages)
		return 1;

	// Allocate space for archives
	transaction->archives = calloc(transaction->num, sizeof(*transaction->archives));
	if (!transaction->archives)
		return 1;

	// Create all packages
	for (unsigned int i = 0; i < transaction->num; i++) {
		r = pakfire_package_create_from_solvable(&transaction->packages[i],
			transaction->pakfire, t->steps.elements[i]);
		if (r)
			return r;
	}

	return 0;
}

static int pakfire_transaction_import_userinstalled(
		struct pakfire_transaction* t, Solver* solver) {
	if (t->userinstalled) {
		free(t->userinstalled);
		t->userinstalled = NULL;
	}

	Queue userinstalled;
	queue_init(&userinstalled);

	// Fetch a list of all packages that are installed by the user
	solver_get_userinstalled(solver, &userinstalled, GET_USERINSTALLED_NAMES);

	// Skip everything if the queue is empty
	if (!userinstalled.count)
		return 0;

	t->userinstalled = calloc(userinstalled.count + 1, sizeof(*t->userinstalled));
	if (!t->userinstalled) {
		ERROR(t->pakfire, "Could not allocate userinstalled\n");
		return 1;
	}

	Pool* pool = pakfire_get_solv_pool(t->pakfire);

	// Store the names of all userinstalled packages
	for (int i = 0; i < userinstalled.count; i++) {
		const char* package = pool_id2str(pool, userinstalled.elements[i]);
		t->userinstalled[i] = strdup(package);
	}

	return 0;
}

int pakfire_transaction_create(struct pakfire_transaction** transaction,
		struct pakfire* pakfire, Solver* solver) {
	struct pakfire_transaction* t = calloc(1, sizeof(*t));
	if (!t)
		return ENOMEM;

	// Store reference to Pakfire
	t->pakfire = pakfire_ref(pakfire);

	// Initialize the reference counter
	t->nrefs = 1;

	// Import transaction
	int r = pakfire_transaction_import_transaction(t, solver);
	if (r)
		goto ERROR;

	// Import userinstalled
	r = pakfire_transaction_import_userinstalled(t, solver);
	if (r)
		goto ERROR;

	*transaction = t;
	return 0;

ERROR:
	pakfire_transaction_free(t);
	return 1;
}

PAKFIRE_EXPORT struct pakfire_transaction* pakfire_transaction_ref(
		struct pakfire_transaction* transaction) {
	transaction->nrefs++;

	return transaction;
}

PAKFIRE_EXPORT struct pakfire_transaction* pakfire_transaction_unref(
		struct pakfire_transaction* transaction) {
	if (--transaction->nrefs > 0)
		return transaction;

	pakfire_transaction_free(transaction);
	return NULL;
}

void pakfire_transaction_set_status_callback(struct pakfire_transaction* transaction,
		pakfire_status_callback callback, void* data) {
	transaction->callbacks.status = callback;
	transaction->callbacks.status_data = data;
}

static int pakfire_transaction_get_progress(struct pakfire_transaction* transaction) {
	return transaction->progress * 100 / transaction->num;
}

static void pakfire_transaction_status(struct pakfire_transaction* transaction,
		const char* message, ...) {
	char* buffer = NULL;
	va_list args;
	int r;

	// Do nothing if callback isn't set
	if (!transaction->callbacks.status)
		return;

	// Format the message
	if (message) {
		va_start(args, message);
		r = vasprintf(&buffer, message, args);
		va_end(args);

		if (r < 0)
			return;
	}

	// Fetch progress
	const int progress = pakfire_transaction_get_progress(transaction);

	// Call the callback
	transaction->callbacks.status(transaction->pakfire,
		transaction->callbacks.status_data, progress, buffer);

	// Cleanup
	if (buffer)
		free(buffer);
}

PAKFIRE_EXPORT size_t pakfire_transaction_count(struct pakfire_transaction* transaction) {
	return transaction->num;
}

static ssize_t pakfire_transaction_installsizechange(struct pakfire_transaction* transaction) {
	ssize_t sizechange = transaction_calc_installsizechange(transaction->transaction);

	// Convert from kbytes to bytes
	return sizechange * 1024;
}

static ssize_t pakfire_transaction_downloadsize(struct pakfire_transaction* transaction) {
	ssize_t size = 0;

	for (unsigned int i = 0; i < transaction->num; i++)
		size += pakfire_package_get_num(
			transaction->packages[i], PAKFIRE_PKG_DOWNLOADSIZE, 0);

	return size;
}

static void pakfire_transaction_append_line(char*** lines, const char* format, ...) {
	if (!lines)
		return;

	char* buffer = NULL;
	va_list args;
	int r;

	va_start(args, format);
	r = vasprintf(&buffer, format, args);
	va_end(args);

	if (r < 0)
		return;

	// Count lines
	unsigned int count = 0;
	if (*lines) {
		for (char** l = *lines; *l; l++)
			count++;
	}

	// Increase size of array
	*lines = reallocarray(*lines, count + 2, sizeof(**lines));
	if (!*lines)
		return;

	// Append line and terminate lines
	(*lines)[count] = buffer;
	(*lines)[count + 1] = NULL;
}

static void pakfire_transaction_add_headline(char*** lines, size_t width, const char* headline) {
	pakfire_transaction_append_line(lines, "%s\n", headline);
}

static void pakfire_transaction_add_newline(char*** lines, size_t width) {
	pakfire_transaction_append_line(lines, "\n");
}

static void pakfire_transaction_add_line(char*** lines, size_t width, const char* name,
		const char* arch, const char* version, const char* repo, const char* size) {
	pakfire_transaction_append_line(lines, " %-21s %-8s %-21s %-18s %6s \n",
		name, arch, version, repo, size);
}

static void pakfire_transaction_add_package(char*** lines, size_t width, struct pakfire_package* pkg) {
	char size[128];

	struct pakfire_repo* repo = pakfire_package_get_repo(pkg);

	// Format size
	int r = pakfire_format_size(size, pakfire_package_get_size(pkg));
	if (r < 0)
		return;

	pakfire_transaction_add_line(lines, width,
		pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME),
		pakfire_package_get_string(pkg, PAKFIRE_PKG_ARCH),
		pakfire_package_get_string(pkg, PAKFIRE_PKG_EVR),
		pakfire_repo_get_name(repo),
		size
	);

	pakfire_repo_unref(repo);
}

static void pakfire_transaction_add_package_change(char*** lines, size_t width,
		struct pakfire_package* old_pkg, struct pakfire_package* new_pkg) {
	// Print the new package first
	pakfire_transaction_add_package(lines, width, new_pkg);

	pakfire_transaction_append_line(lines,
		"   --> %s\n", pakfire_package_get_string(old_pkg, PAKFIRE_PKG_NEVRA));
}

static void pakfire_transaction_add_separator(char*** lines, size_t width) {
	char* separator = alloca(width + 1);

	for (unsigned int i = 0; i < width; i++)
		separator[i] = '=';
	separator[width] = '\0';

	pakfire_transaction_append_line(lines, "%s\n", separator);
}

static void pakfire_transaction_add_usage_line(char*** lines, size_t width,
		const char* headline, ssize_t size) {
	char buffer[128];

	int r = pakfire_format_size(buffer, size);
	if (r < 0)
		return;

	pakfire_transaction_append_line(lines,  "%-21s: %s\n", headline, buffer);
}

static char* pakfire_transaction_join_lines(char** lines) {
	if (!lines)
		return NULL;

	size_t size = 0;

	// Determine total length
	for (char** line = lines; *line; line++)
		size += strlen(*line);

	// Allocate memory large enough to hold the result
	char* s = calloc(1, size + 1);
	if (!s)
		return s;

	char* p = s;

	for (char** line = lines; *line; line++) {
		p += snprintf(p, s - p - 1, "%s", *line);
	}

	return s;
}

PAKFIRE_EXPORT char* pakfire_transaction_dump(struct pakfire_transaction* transaction, size_t width) {
	char headline[1024];
	int r;

	Pool* pool = transaction->transaction->pool;
	const int mode =
		SOLVER_TRANSACTION_SHOW_OBSOLETES |
		SOLVER_TRANSACTION_OBSOLETE_IS_UPGRADE;

	char** lines = NULL;

	// Header
	pakfire_transaction_add_separator(&lines, width);
	pakfire_transaction_add_line(&lines, width,
		_("Package"),
		_("Arch"),
		_("Version"),
		_("Repository"),
		_("Size")
	);
	pakfire_transaction_add_separator(&lines, width);

	Queue classes;
	queue_init(&classes);

	// Get all classes
	transaction_classify(transaction->transaction, mode, &classes);

	Queue pkgs;
	queue_init(&pkgs);

	/*
		The classes queue now contains a list of all classes as a tuple of:
			* The class type
			* The number of packages in this class
			* The from ID (for arch/vendor change)
			* The to ID (for arch/vendor change)
	*/
	for (int i = 0; i < classes.count; i += 4) {
		Id class = classes.elements[i];
		unsigned int count = classes.elements[i+1];

		const char* from = pool_id2str(pool, classes.elements[i+2]);
		const char* to   = pool_id2str(pool, classes.elements[i+3]);

		switch (class) {
			case SOLVER_TRANSACTION_INSTALL:
				if (count)
					pakfire_string_format(headline, _("Installing %u packages:"), count);
				else
					pakfire_string_set(headline, _("Installing one package:"));
				break;

			case SOLVER_TRANSACTION_REINSTALLED:
				if (count)
					pakfire_string_format(headline, _("Reinstalling %u packages:"), count);
				else
					pakfire_string_set(headline, _("Reinstalling one package:"));
				break;

			case SOLVER_TRANSACTION_ERASE:
				if (count)
					pakfire_string_format(headline, _("Removing %u packages:"), count);
				else
					pakfire_string_set(headline, _("Removing one package:"));
				break;

			case SOLVER_TRANSACTION_UPGRADED:
				if (count)
					pakfire_string_format(headline, _("Updating %u packages:"), count);
				else
					pakfire_string_set(headline, _("Updating one package:"));
				break;

			case SOLVER_TRANSACTION_DOWNGRADED:
				if (count)
					pakfire_string_format(headline, _("Downgrading %u packages:"), count);
				else
					pakfire_string_set(headline, _("Downgrading one package:"));
				break;

			case SOLVER_TRANSACTION_CHANGED:
				if (count)
					pakfire_string_format(headline, _("Changing %u packages:"), count);
				else
					pakfire_string_set(headline, _("Changing one package:"));
				break;

			case SOLVER_TRANSACTION_ARCHCHANGE:
				if (count)
					pakfire_string_format(headline,
						_("%u architecture changes from '%s' to '%s':"), count, from, to);
				else
					pakfire_string_format(headline,
						_("One architecture change from '%s' to '%s':"), from, to);
				break;

			case SOLVER_TRANSACTION_VENDORCHANGE:
				if (count)
					pakfire_string_format(headline,
						_("%u vendor changes from '%s' to '%s':"), count, from, to);
				else
					pakfire_string_format(headline,
						_("One vendor change from '%s' to '%s':"), from, to);
				break;

			case SOLVER_TRANSACTION_IGNORE:
				continue;
		}

		// Show what we are doing
		pakfire_transaction_add_headline(&lines, width, headline);

		// Fetch packages in this class
		transaction_classify_pkgs(transaction->transaction, mode, class,
			classes.elements[i+2], classes.elements[i+3], &pkgs);

		// List all packages
		for (int j = 0; j < pkgs.count; j++) {
			struct pakfire_package* old_pkg = NULL;
			struct pakfire_package* new_pkg = NULL;

			r = pakfire_package_create_from_solvable(&old_pkg, transaction->pakfire,
				pkgs.elements[j]);
			if (r)
				continue;

			switch (class) {
				case SOLVER_TRANSACTION_UPGRADED:
				case SOLVER_TRANSACTION_DOWNGRADED:
					r = pakfire_package_create_from_solvable(&new_pkg, transaction->pakfire,
						transaction_obs_pkg(transaction->transaction, pkgs.elements[j]));
					if (r)
						continue;

					pakfire_transaction_add_package_change(&lines, width, old_pkg, new_pkg);
					break;

				default:
					pakfire_transaction_add_package(&lines, width, old_pkg);
					break;
			}

			pakfire_package_unref(old_pkg);
			if (new_pkg)
				pakfire_package_unref(new_pkg);
		}

		// Newline
		pakfire_transaction_add_newline(&lines, width);
	}

	queue_free(&classes);
	queue_free(&pkgs);

	// Summary
	pakfire_transaction_add_headline(&lines, width, _("Transaction Summary"));
	pakfire_transaction_add_separator(&lines, width);

	// How much do we need to download?
	size_t downloadsize = pakfire_transaction_downloadsize(transaction);

	if (downloadsize > 0)
		pakfire_transaction_add_usage_line(&lines, width,
			_("Total Download Size"), downloadsize);

	// How much more space do we need?
	ssize_t sizechange = pakfire_transaction_installsizechange(transaction);
	pakfire_transaction_add_usage_line(&lines, width,
		(sizechange >= 0) ? _("Required Space") : _("Freed Space"), abs(sizechange));

	// Join all lines together
	char* string = pakfire_transaction_join_lines(lines);

	if (string)
		DEBUG(transaction->pakfire, "Transaction: %s\n", string);

	// Free lines
	if (lines) {
		for (char** line = lines; *line; line++)
			free(*line);
		free(lines);
	}

	return string;
}

static int pakfire_transaction_check_fileconflicts(
		struct pakfire_transaction* transaction) {
	/*
		XXX TODO

		Because of a dependency to librpm, we cannot use the functionality from
		libsolv and need to re-implement this.
	*/
	return 0;
}

static int pakfire_transaction_check(struct pakfire_transaction* transaction) {
	int r;

	// Check for any file conflicts
	r = pakfire_transaction_check_fileconflicts(transaction);
	if (r)
		return r;

	return 0;
}

static int pakfire_transaction_verify(struct pakfire_transaction* transaction,
		struct pakfire_package* pkg, struct pakfire_archive* archive) {
	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

	// Nothing to do if this step does not have an archive
	if (!archive) {
		DEBUG(transaction->pakfire, "Package %s requires no archive\n", nevra);
		return 0;
	}

	enum pakfire_digest_types digest_type = PAKFIRE_DIGEST_UNDEFINED;
	size_t length = 0;

	// Fetch digest from package
	const unsigned char* expected_digest = pakfire_package_get_digest(pkg, &digest_type, &length);
	if (!expected_digest) {
		DEBUG(transaction->pakfire, "Package %s has no digest\n", nevra);
		return 0;
	}

	// Check against the digest of the archive
	return pakfire_archive_check_digest(archive, digest_type, expected_digest, length);
}

static int pakfire_transaction_run_script(struct pakfire_transaction* transaction,
		struct pakfire_db* db, const char* type, struct pakfire_package* pkg, struct pakfire_archive* archive) {
	struct pakfire_scriptlet* scriptlet = NULL;

	// Fetch scriptlet from archive if possible
	if (archive)
		scriptlet = pakfire_archive_get_scriptlet(archive, type);
	else
		scriptlet = pakfire_db_get_scriptlet(db, pkg, type);

	// Nothing to do if there are no scriptlets
	if (!scriptlet)
		return 0;

	// Execute the scriptlet
	pakfire_scriptlet_execute(scriptlet);

	pakfire_scriptlet_unref(scriptlet);

	return 0;
}

static int pakfire_transaction_extract(struct pakfire_transaction* transaction,
		struct pakfire_package* pkg, struct pakfire_archive* archive) {
	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

	// Update status
	pakfire_transaction_status(transaction, _("Installing %s..."), nevra);

	// Extract payload
	int r = pakfire_archive_extract(archive);
	if (r) {
		ERROR(transaction->pakfire, "Could not extract package %s: %m\n",
			nevra);
		return r;
	}

	// Is it necessary to call ldconfig?
	struct pakfire_filelist* filelist = pakfire_archive_get_filelist(archive);
	if (filelist) {
		int need_ldconfig = pakfire_filelist_contains(filelist, "*/lib*.so.?");

		// Update the runtime linker cache
		if (need_ldconfig)
			pakfire_jail_ldconfig(transaction->pakfire);

		pakfire_filelist_unref(filelist);
	}

	return r;
}

static int pakfire_transaction_erase(struct pakfire_transaction* transaction,
		struct pakfire_db* db, struct pakfire_package* pkg) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	// Fetch filelist
	r = pakfire_db_package_filelist(db, &filelist, pkg);
	if (r)
		goto ERROR;

	// Remove all files on the filelist
	r = pakfire_filelist_cleanup(filelist, 0);
	if (r)
		goto ERROR;

	// Update the runtime linker cache after all files have been removed
	pakfire_jail_ldconfig(transaction->pakfire);

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

static const char* pakfire_action_type_string(enum pakfire_actions type) {
	switch (type) {
		case PAKFIRE_ACTION_NOOP:
			return "NOOP";

		case PAKFIRE_ACTION_VERIFY:
			return "VERIFY";

		case PAKFIRE_ACTION_EXECUTE:
			return "EXECUTE";

		case PAKFIRE_ACTION_PRETRANS:
			return "PRETRANS";

		case PAKFIRE_ACTION_POSTTRANS:
			return "POSTTRANS";
	}

	return NULL;
}

static int pakfire_transaction_package_is_userinstalled(
		struct pakfire_transaction* transaction, struct pakfire_package* pkg) {
	// No packages on the list
	if (!transaction->userinstalled)
		return 0;

	const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME);

	// Check if the package is on the list
	for (char** elem = transaction->userinstalled; *elem; elem++) {
		if (strcmp(name, *elem) == 0)
			return 1;
	}

	// Not found
	return 0;
}

static int pakfire_transaction_apply_systemd_sysusers(struct pakfire_transaction* transaction,
		struct pakfire_package* pkg, struct pakfire_archive* archive) {
	// Walk through the archive and find all sysuser files
	if (pakfire_package_matches_dep(pkg, PAKFIRE_PKG_REQUIRES, "pakfire(systemd-sysusers)"))
		return pakfire_archive_apply_systemd_sysusers(archive);

	return 0;
}

static int pakfire_transaction_apply_systemd_tmpfiles(
		struct pakfire_transaction* transaction, struct pakfire_package* pkg) {
	// Apply any tmpfiles (ignore any errors)
	if (pakfire_package_matches_dep(pkg, PAKFIRE_PKG_REQUIRES, "pakfire(systemd-tmpfiles)"))
		pakfire_jail_run_systemd_tmpfiles(transaction->pakfire);

	return 0;
}

static int pakfire_transaction_run_step(struct pakfire_transaction* transaction,
		struct pakfire_db* db, const enum pakfire_actions action, struct pakfire_package* pkg, struct pakfire_archive* archive) {
	if (!pkg) {
		errno = EINVAL;
		return 1;
	}

	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);
	const enum pakfire_steps type = pakfire_transaction_get_step_type(transaction, pkg);

	DEBUG(transaction->pakfire, "Running %s for %s\n", pakfire_action_type_string(action), nevra);

	int r = 0;
	switch (action) {
		// Verify this step
		case PAKFIRE_ACTION_VERIFY:
			r = pakfire_transaction_verify(transaction, pkg, archive);
			break;

		// Run the pre-transaction scripts
		case PAKFIRE_ACTION_PRETRANS:
			switch (type) {
				case PAKFIRE_STEP_INSTALL:
				case PAKFIRE_STEP_REINSTALL:
					r = pakfire_transaction_run_script(transaction, db,
						"pretransin", pkg, archive);
					break;

				case PAKFIRE_STEP_UPGRADE:
				case PAKFIRE_STEP_DOWNGRADE:
					r = pakfire_transaction_run_script(transaction, db,
						"pretransup", pkg, archive);
					break;

				case PAKFIRE_STEP_ERASE:
				case PAKFIRE_STEP_OBSOLETE:
					r = pakfire_transaction_run_script(transaction, db,
						"pretransun", pkg, archive);
					break;

				case PAKFIRE_STEP_IGNORE:
					break;
			}
			break;

		// Run the post-transaction scripts
		case PAKFIRE_ACTION_POSTTRANS:
			switch (type) {
				case PAKFIRE_STEP_INSTALL:
				case PAKFIRE_STEP_REINSTALL:
					r = pakfire_transaction_run_script(transaction, db,
						"posttransin", pkg, archive);
					break;

				case PAKFIRE_STEP_UPGRADE:
				case PAKFIRE_STEP_DOWNGRADE:
					r = pakfire_transaction_run_script(transaction, db,
						"posttransup", pkg, archive);
					break;

				case PAKFIRE_STEP_ERASE:
				case PAKFIRE_STEP_OBSOLETE:
					r = pakfire_transaction_run_script(transaction, db,
						"posttransun", pkg, archive);
					break;

				case PAKFIRE_STEP_IGNORE:
					break;
			}
			break;

		// Execute the action of this script
		case PAKFIRE_ACTION_EXECUTE:
			// Increment progress
			transaction->progress++;

			// Update progress callback
			pakfire_transaction_status(transaction, NULL);

			switch (type) {
				case PAKFIRE_STEP_INSTALL:
				case PAKFIRE_STEP_REINSTALL:
					// Apply systemd sysusers
					r = pakfire_transaction_apply_systemd_sysusers(transaction, pkg, archive);
					if (r)
						break;

					r = pakfire_transaction_run_script(transaction, db,
						"prein", pkg, archive);
					if (r)
						break;

					r = pakfire_transaction_extract(transaction, pkg, archive);
					if (r)
						break;

					// Remove package metadata first when reinstalling
					if (type == PAKFIRE_STEP_REINSTALL) {
						r = pakfire_db_remove_package(db, pkg);
						if (r)
							break;
					}

					r = pakfire_db_add_package(db, pkg, archive,
							pakfire_transaction_package_is_userinstalled(transaction, pkg));
					if (r)
						break;

					// Apply systemd tmpfiles
					r = pakfire_transaction_apply_systemd_tmpfiles(transaction, pkg);
					if (r)
						break;

					r = pakfire_transaction_run_script(transaction, db,
						"postin", pkg, archive);
					break;

				case PAKFIRE_STEP_UPGRADE:
				case PAKFIRE_STEP_DOWNGRADE:
					// Apply systemd sysusers
					r = pakfire_transaction_apply_systemd_sysusers(transaction, pkg, archive);
					if (r)
						break;

					r = pakfire_transaction_run_script(transaction, db,
						"preup", pkg, archive);
					if (r)
						break;

					r = pakfire_transaction_extract(transaction, pkg, archive);
					if (r)
						break;

					r = pakfire_db_add_package(db, pkg, archive,
							pakfire_transaction_package_is_userinstalled(transaction, pkg));
					if (r)
						break;

					// Apply systemd tmpfiles
					r = pakfire_transaction_apply_systemd_tmpfiles(transaction, pkg);
					if (r)
						break;

					r = pakfire_transaction_run_script(transaction, db,
						"postup", pkg, archive);
					break;

				case PAKFIRE_STEP_ERASE:
				case PAKFIRE_STEP_OBSOLETE:
					r = pakfire_transaction_run_script(transaction, db,
						"preun", pkg, archive);
					if (r)
						break;

					r = pakfire_transaction_erase(transaction, db, pkg);
					if (r)
						break;

					r = pakfire_db_remove_package(db, pkg);
					if (r)
						break;

					r = pakfire_transaction_run_script(transaction, db,
						"postun", pkg, archive);
					break;

				case PAKFIRE_STEP_IGNORE:
					break;
			}
			break;

		// Do nothing
		case PAKFIRE_ACTION_NOOP:
			break;
	}

	if (r)
		ERROR(transaction->pakfire, "Step %s (%d) for %s has failed: %m\n",
			pakfire_action_type_string(action), type, nevra);

	return r;
}

static int pakfire_transaction_run_steps(struct pakfire_transaction* transaction,
		struct pakfire_db* db, enum pakfire_actions action) {
	int r = 0;

	// Update status
	switch (action) {
		case PAKFIRE_ACTION_VERIFY:
			pakfire_transaction_status(transaction, _("Verifying packages..."));
			break;

		case PAKFIRE_ACTION_PRETRANS:
			pakfire_transaction_status(transaction, _("Preparing installation..."));
			break;

		case PAKFIRE_ACTION_POSTTRANS:
			pakfire_transaction_status(transaction, _("Finishing up..."));
			break;

		default:
			break;
	}

	// Walk through all steps
	for (unsigned int i = 0; i < transaction->num; i++) {
		r = pakfire_transaction_run_step(transaction, db, action,
			transaction->packages[i], transaction->archives[i]);

		// End loop if action was unsuccessful
		if (r) {
			DEBUG(transaction->pakfire, "Step %d failed: %m\n", i);
			break;
		}
	}

	return r;
}

static int pakfire_transaction_open_archives(struct pakfire_transaction* transaction) {
	for (unsigned int i = 0; i < transaction->num; i++) {
		struct pakfire_package* pkg = transaction->packages[i];

		// Fetch the type
		enum pakfire_steps type = pakfire_transaction_get_step_type(transaction, pkg);

		// Do we need the archive?
		switch (type) {
			case PAKFIRE_STEP_INSTALL:
			case PAKFIRE_STEP_REINSTALL:
			case PAKFIRE_STEP_UPGRADE:
			case PAKFIRE_STEP_DOWNGRADE:
			case PAKFIRE_STEP_OBSOLETE:
				break;

			case PAKFIRE_STEP_ERASE:
			case PAKFIRE_STEP_IGNORE:
				continue;
		}

		transaction->archives[i] = pakfire_package_get_archive(pkg);
		if (!transaction->archives[i]) {
			ERROR(transaction->pakfire, "Could not open archive for %s: %m\n",
				pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA));
			return 1;
		}
	}

	return 0;
}

static int pakfire_usrmove_symlink(struct pakfire* pakfire,
		const char* src, const char* dst) {
	char link[PATH_MAX];
	char path[PATH_MAX];
	int r;

	// Compose the link path
	r = pakfire_path(pakfire, link, "%s", src);
	if (r)
		return r;

	// Exit if the link exists
	if (pakfire_path_exists(link))
		return 0;

	// Compose the destination path
	r = pakfire_path(pakfire, path, "%s", dst);
	if (r)
		return r;

	// Make sure the destination exists
	r = pakfire_mkdir(path, 755);
	if (r)
		return r;

	// Create the symlink
	r = symlink(dst, link);
	if (r) {
		DEBUG(pakfire, "Could not create symlink %s to %s: %m\n", dst, link);
		return r;
	}

	DEBUG(pakfire, "Created symlink %s --> %s\n", link, dst);

	return 0;
}

/*
	This is an ugly helper function that helps us to make sure
	that /bin, /sbin, /lib and /lib64 are symlinks to their
	corresponding path in /usr.
*/
static int pakfire_usrmove(struct pakfire* pakfire) {
	int r;

	r = pakfire_usrmove_symlink(pakfire, "/bin", "usr/bin");
	if (r)
		return r;

	r = pakfire_usrmove_symlink(pakfire, "/sbin", "usr/sbin");
	if (r)
		return r;

	r = pakfire_usrmove_symlink(pakfire, "/lib", "usr/lib");
	if (r)
		return r;

	r = pakfire_usrmove_symlink(pakfire, "/lib64", "usr/lib64");
	if (r)
		return r;

	return r;
}

static int pakfire_transaction_perform(struct pakfire_transaction* transaction) {
	struct pakfire_repo* repo = NULL;
	struct pakfire_db* db;
	int r;

	DEBUG(transaction->pakfire, "Running Transaction %p\n", transaction);

	// Open all archives
	r = pakfire_transaction_open_archives(transaction);
	if (r)
		return r;

	// Open the database
	r = pakfire_db_open(&db, transaction->pakfire, PAKFIRE_DB_READWRITE);
	if (r) {
		ERROR(transaction->pakfire, "Could not open the database\n");
		return r;
	}

	// Verify steps
	r = pakfire_transaction_run_steps(transaction, db, PAKFIRE_ACTION_VERIFY);
	if (r)
		goto ERROR;

	// Make sure /usr-move is working
	r = pakfire_usrmove(transaction->pakfire);
	if (r)
		goto ERROR;

	// Execute all pre transaction actions
	r = pakfire_transaction_run_steps(transaction, db, PAKFIRE_ACTION_PRETRANS);
	if (r)
		goto ERROR;

	r = pakfire_transaction_run_steps(transaction, db, PAKFIRE_ACTION_EXECUTE);
	if (r)
		goto ERROR;

	// Execute all post transaction actions
	r = pakfire_transaction_run_steps(transaction, db, PAKFIRE_ACTION_POSTTRANS);
	if (r)
		goto ERROR;

	DEBUG(transaction->pakfire, "The transaction has finished successfully\n");

	// Reload database for next transaction

	repo = pakfire_get_installed_repo(transaction->pakfire);
	if (!repo)
		goto ERROR;

	// Reload the database
	r = pakfire_db_load(db, repo);

ERROR:
	if (repo)
		pakfire_repo_unref(repo);
	pakfire_db_unref(db);

	return r;
}

static int pakfire_transaction_download_package(struct pakfire_transaction* transaction,
		struct pakfire_downloader* downloader, struct pakfire_package* pkg) {
	int r = 1;
	struct pakfire_repo* repo = NULL;
	struct pakfire_mirrorlist* mirrorlist = NULL;

	// Fetch the repository to download from
	repo = pakfire_package_get_repo(pkg);
	if (!repo)
		goto ERROR;

	// Fetch baseurl
	const char* baseurl = pakfire_repo_get_baseurl(repo);

	// Fetch mirrorlist
	mirrorlist = pakfire_repo_get_mirrorlist(repo);

	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);
	if (!nevra)
		goto ERROR;

	// Where to store the package?
	const char* path = pakfire_package_get_string(pkg, PAKFIRE_PKG_PATH);
	if (!path) {
		ERROR(transaction->pakfire, "Could not retrieve package path for %s: %m\n", nevra);
		goto ERROR;
	}

	// What file to download?
	const char* filename = pakfire_package_get_string(pkg, PAKFIRE_PKG_FILENAME);
	if (!filename) {
		ERROR(transaction->pakfire, "Could not retrieve filename for package %s: %m\n", nevra);
		goto ERROR;
	}

	enum pakfire_digest_types digest_type = 0;
	size_t digest_length = 0;

	// Retrieve package digest
	const unsigned char* digest = pakfire_package_get_digest(pkg, &digest_type, &digest_length);
	if (!digest) {
		ERROR(transaction->pakfire, "Package %s has no digest set: %m\n", nevra);
		goto ERROR;
	}

	// Add transfer to downloader
	r = pakfire_downloader_add_transfer(downloader, baseurl, mirrorlist,
		nevra, filename, path, digest_type, digest, digest_length, 0);

ERROR:
	if (mirrorlist)
		pakfire_mirrorlist_unref(mirrorlist);
	if (repo)
		pakfire_repo_unref(repo);

	return r;
}

static int pakfire_transaction_package_needs_download(
		struct pakfire_transaction* transaction, struct pakfire_package* pkg) {
	enum pakfire_steps type = pakfire_transaction_get_step_type(transaction, pkg);
	switch (type) {
		case PAKFIRE_STEP_INSTALL:
		case PAKFIRE_STEP_REINSTALL:
		case PAKFIRE_STEP_DOWNGRADE:
		case PAKFIRE_STEP_UPGRADE:
			break;

		// No need to download for these steps
		default:
			return 0;
	}

	// No download required if this package is already installed
	if (pakfire_package_is_installed(pkg))
		return 0;

	const char* path = pakfire_package_get_string(pkg, PAKFIRE_PKG_PATH);

	// Does the file exist?
	int r = access(path, R_OK);
	if (r == 0)
		return 0;

	// This package needs to be downloaded
	return 1;
}

PAKFIRE_EXPORT int pakfire_transaction_download(struct pakfire_transaction* transaction) {
	struct pakfire_downloader* downloader;
	int r;

	// Initialize the downloader
	r = pakfire_downloader_create(&downloader, transaction->pakfire);
	if (r) {
		ERROR(transaction->pakfire, "Could not initialize downloader: %m\n");
		return 1;
	}

	// Add all packages that need to be downloaded
	for (unsigned int i = 0; i < transaction->num; i++) {
		struct pakfire_package* pkg = transaction->packages[i];

		if (!pakfire_transaction_package_needs_download(transaction, pkg))
			continue;

		// Enqueue download
		r = pakfire_transaction_download_package(transaction, downloader, pkg);
		if (r) {
			const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

			ERROR(transaction->pakfire, "Could not add download to queue: %s: %m\n", nevra);
			goto ERROR;
		}
	}

	// Run the downloader
	r = pakfire_downloader_run(downloader);

ERROR:
	pakfire_downloader_unref(downloader);

	return r;
}

PAKFIRE_EXPORT int pakfire_transaction_run(
		struct pakfire_transaction* transaction, int flags) {
	int r;

	// Skip running an empty transaction
	if (!transaction->num) {
		DEBUG(transaction->pakfire, "Empty transaction. Skipping...\n");
		return 0;
	}

	// Show what would be done
	char* dump = pakfire_transaction_dump(transaction, 80);

	// Check if we should continue
	r = pakfire_confirm(transaction->pakfire, dump, _("Is this okay?"));
	if (r) {
		ERROR(transaction->pakfire, "Transaction aborted upon user request\n");
		goto ERROR;
	}

	// Perform a check if this can actually be run
	r = pakfire_transaction_check(transaction);
	if (r)
		goto ERROR;

	// End here for a dry run
	if (flags & PAKFIRE_TRANSACTION_DRY_RUN)
		goto ERROR;

	// Download what we need
	r = pakfire_transaction_download(transaction);
	if (r)
		goto ERROR;

	// Perform all steps
	r = pakfire_transaction_perform(transaction);
	if (r)
		goto ERROR;

ERROR:
	// Cleanup
	free(dump);

	return r;
}
