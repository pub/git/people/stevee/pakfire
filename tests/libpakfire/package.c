/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdio.h>

#include <uuid/uuid.h>

#include <pakfire/package.h>
#include <pakfire/util.h>

#include "../testsuite.h"

/*
	This tests creates a new package and sets various attributes and reads them back again.
*/
static int test_create(const struct test* t) {
	struct pakfire_package* pkg = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_package_create(&pkg, t->pakfire, NULL, "test", "1.0-1", "x86_64"));

	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME), "test");
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_EVR), "1.0-1");
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_ARCH), "x86_64");

	// NEVRA
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA), "test-1.0-1.x86_64");

	// UUID
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_UUID, "7a7cb0e3-d4c7-4bf8-85ef-8e4faa22c128"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_UUID), "7a7cb0e3-d4c7-4bf8-85ef-8e4faa22c128");

	// Database ID
	ASSERT_SUCCESS(pakfire_package_set_num(pkg, PAKFIRE_PKG_DBID, 1));
	ASSERT_EQUALS(pakfire_package_get_num(pkg, PAKFIRE_PKG_DBID, 0), 1);

	// Summary
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_SUMMARY, "SUMMARY"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_SUMMARY), "SUMMARY");

	// Description
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_DESCRIPTION, "DESCRIPTION"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_DESCRIPTION), "DESCRIPTION");

	// License
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_LICENSE, "LICENSE"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_LICENSE), "LICENSE");

	// URL
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_URL, "URL"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_URL), "URL");

	// Groups
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_GROUPS, "GROUPS"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_GROUPS), "GROUPS");

	// Vendor
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_VENDOR, "VENDOR"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_VENDOR), "VENDOR");

	// Distro
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_DISTRO, "DISTRO"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_DISTRO), "DISTRO");

	// Packager
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_PACKAGER, "PACKAGER"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_PACKAGER), "PACKAGER");

	// Set Path
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_PATH, "/tmp/test-1.0-1.x86_64.pfm"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_PATH), "/tmp/test-1.0-1.x86_64.pfm");

	// Set filename
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_FILENAME, "test-1.0-1.x86_64.pfm"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_FILENAME), "test-1.0-1.x86_64.pfm");

	// Download Size
	ASSERT_SUCCESS(pakfire_package_set_num(pkg, PAKFIRE_PKG_DOWNLOADSIZE, 123456));
	ASSERT_EQUALS(pakfire_package_get_num(pkg, PAKFIRE_PKG_DOWNLOADSIZE, 0), 123456);

	// Install Size
	ASSERT_SUCCESS(pakfire_package_set_num(pkg, PAKFIRE_PKG_INSTALLSIZE, 123456));
	ASSERT_EQUALS(pakfire_package_get_num(pkg, PAKFIRE_PKG_INSTALLSIZE, 0), 123456);

	// Install Time
	ASSERT_SUCCESS(pakfire_package_set_num(pkg, PAKFIRE_PKG_INSTALLTIME, 123456));
	ASSERT_EQUALS(pakfire_package_get_num(pkg, PAKFIRE_PKG_INSTALLTIME, 0), 123456);

	// Build Host
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_BUILD_HOST, "localhost"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_BUILD_HOST), "localhost");

	// Build ID
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_BUILD_ID, "5220bb03-d5b6-41f7-be2f-c398b721a9ba"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_BUILD_ID), "5220bb03-d5b6-41f7-be2f-c398b721a9ba");

	// Build Time
	ASSERT_SUCCESS(pakfire_package_set_num(pkg, PAKFIRE_PKG_BUILD_TIME, 123456));
	ASSERT_EQUALS(pakfire_package_get_num(pkg, PAKFIRE_PKG_BUILD_TIME, 0), 123456);

	// Source Name
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_NAME, "test"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_NAME), "test");

	// Source EVR
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_EVR, "1.0-1"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_EVR), "1.0-1");

	// Source Arch
	ASSERT_SUCCESS(pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_ARCH, "src"));
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_ARCH), "src");

	// Fetch the source name
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_SOURCE_PKG), "test-1.0-1.src");

	// We cannot set the source package name
	ASSERT_ERRNO(pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_PKG, "XXX"), EINVAL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (pkg)
		pakfire_package_unref(pkg);

	return r;
}

static int test_invalid_inputs(const struct test* t) {
	struct pakfire_package* pkg = NULL;
	int r = EXIT_FAILURE;

	// Try to create a package with some values missing
	ASSERT_ERRNO(pakfire_package_create(&pkg, t->pakfire, NULL, NULL, "1.0-1", "src"), EINVAL);
	ASSERT_ERRNO(pakfire_package_create(&pkg, t->pakfire, NULL, "test", NULL, "src"), EINVAL);
	ASSERT_ERRNO(pakfire_package_create(&pkg, t->pakfire, NULL, "test", "1.0-1", NULL), EINVAL);

	// Finally create a package
	ASSERT_SUCCESS(pakfire_package_create(&pkg, t->pakfire, NULL, "test", "1.0-1", "x86_64"));

	// Try to set non-sense for strings
	ASSERT_ERRNO(pakfire_package_set_string(pkg, PAKFIRE_PKG_INSTALLTIME, "today"), EINVAL);

	// Try to set a non-sense numeric value
	ASSERT_ERRNO(pakfire_package_set_num(pkg, PAKFIRE_PKG_NAME, 0), EINVAL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (pkg)
		pakfire_package_unref(pkg);

	return r;
}

static int test_uuid(const struct test* t) {
	struct pakfire_package* pkg = NULL;
	int r = EXIT_FAILURE;

	uuid_t uuid;
	char uuid_string[UUID_STR_LEN];

	// Generate a new random UUID
	uuid_generate_random(uuid);

	// Convert the UUID to string
	uuid_unparse(uuid, uuid_string);

	// Create a new package
	ASSERT_SUCCESS(pakfire_package_create(&pkg, t->pakfire, NULL, "test", "1.0-1", "src"));

	// Set the UUID
	ASSERT_SUCCESS(pakfire_package_set_uuid(pkg, PAKFIRE_PKG_UUID, uuid));

	// Fetch the UUID as string
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_UUID), uuid_string);

	// Set the Build ID
	ASSERT_SUCCESS(pakfire_package_set_uuid(pkg, PAKFIRE_PKG_BUILD_ID, uuid));

	// Fetch the Build ID as string
	ASSERT_STRING_EQUALS(pakfire_package_get_string(pkg, PAKFIRE_PKG_BUILD_ID), uuid_string);

	// Try setting the UUID as something else
	ASSERT_ERRNO(pakfire_package_set_uuid(pkg, PAKFIRE_PKG_SUMMARY, uuid), EINVAL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (pkg)
		pakfire_package_unref(pkg);

	return r;
}

static int test_deps(const struct test* t) {
	struct pakfire_package* pkg = NULL;
	char** deps = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_package_create(&pkg, t->pakfire, NULL, "test", "1.0-1", "src"));

	// Add a "provides"
	ASSERT_SUCCESS(pakfire_package_add_dep(pkg, PAKFIRE_PKG_PROVIDES, "a"));

	// Fetch the provides
	ASSERT(deps = pakfire_package_get_deps(pkg, PAKFIRE_PKG_PROVIDES));

	// There should be one element called "a"
	for (char** dep = deps; *dep; dep++) {
		ASSERT(strcmp(*dep, "test = 1.0-1") == 0 || strcmp(*dep, "a") == 0);
	}

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (pkg)
		pakfire_package_unref(pkg);

	return r;
}

static int test_filelist(const struct test* t) {
	struct pakfire_filelist* filelist = NULL;
	struct pakfire_file* file = NULL;
	struct pakfire_package* pkg = NULL;
	int r = EXIT_FAILURE;

	// Create a filelist
	ASSERT_SUCCESS(pakfire_filelist_create(&filelist, t->pakfire));

	// Create a file
	ASSERT_SUCCESS(pakfire_file_create(&file, t->pakfire));

	// Store some path
	ASSERT_SUCCESS(pakfire_file_set_path(file, "/bin/bash"));

	// Append the file to the filelist
	ASSERT_SUCCESS(pakfire_filelist_add(filelist, file));

	// Free the file
	pakfire_file_unref(file);
	file = NULL;

	ASSERT_EQUALS(pakfire_filelist_length(filelist), 1);

	// Create a new package
	ASSERT_SUCCESS(pakfire_package_create(&pkg, t->pakfire, NULL,
		"bash", "1.0-1", "i386"));

	// Set filelist
	ASSERT_SUCCESS(pakfire_package_set_filelist(pkg, filelist));

	// Drop the filelist
	pakfire_filelist_unref(filelist);
	filelist = NULL;

	// Fetch the filelist back from the package
	ASSERT(filelist = pakfire_package_get_filelist(pkg));

	// The filelist must have a length of one
	ASSERT_EQUALS(pakfire_filelist_length(filelist), 1);

	for (unsigned int i = 0; i < pakfire_filelist_length(filelist); i++) {
		ASSERT(file = pakfire_filelist_get(filelist, i));

		// Check the path
		ASSERT_STRING_EQUALS(pakfire_file_get_path(file), "/bin/bash");
	}

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (filelist)
		pakfire_filelist_unref(filelist);
	if (file)
		pakfire_file_unref(file);
	if (pkg)
		pakfire_package_unref(pkg);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create);
	testsuite_add_test(test_invalid_inputs);
	testsuite_add_test(test_uuid);
	testsuite_add_test(test_deps);
	testsuite_add_test(test_filelist);

	return testsuite_run(argc, argv);
}
