/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <unistd.h>

#include <pakfire/cgroup.h>
#include <pakfire/jail.h>

#include "../testsuite.h"

static int test_create_and_destroy(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	int r = EXIT_FAILURE;

	// Open a new cgroup
	ASSERT_SUCCESS(pakfire_cgroup_open(&cgroup, t->pakfire, "pakfire-test", 0));

	// Destroy the cgroup again
	ASSERT_SUCCESS(pakfire_cgroup_destroy(cgroup));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (cgroup)
		pakfire_cgroup_unref(cgroup);

	return r;
}

static int test_stats(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	struct pakfire_cgroup_stats stats;

	const char* argv[] = {
		"/command", "sleep", "1", NULL,
	};

	// Open the cgroup
	ASSERT_SUCCESS(pakfire_cgroup_open(&cgroup, t->pakfire, "pakfire-test",
		PAKFIRE_CGROUP_ENABLE_ACCOUNTING));

#if 0
	// Enable CPU & Memory controllers
	ASSERT_SUCCESS(pakfire_cgroup_enable_controllers(cgroup,
		PAKFIRE_CGROUP_CONTROLLER_CPU|PAKFIRE_CGROUP_CONTROLLER_MEMORY));
#endif

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Connect jail to the cgroup
	ASSERT_SUCCESS(pakfire_jail_set_cgroup(jail, cgroup));

	// Run a few things
	for (unsigned int i = 0; i < 3; i++) {
		ASSERT_SUCCESS(pakfire_jail_exec(jail, argv, NULL, NULL, NULL, 0));
	}

	// Try reading the stats into some invalid space
	ASSERT_ERRNO(pakfire_cgroup_stat(cgroup, NULL), EINVAL);

	// Read the stats
	ASSERT_SUCCESS(pakfire_cgroup_stat(cgroup, &stats));

	// We must have had some CPU usage
	ASSERT(stats.cpu.usage_usec > 0);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (cgroup) {
		pakfire_cgroup_destroy(cgroup);
		pakfire_cgroup_unref(cgroup);
	}
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create_and_destroy);
	testsuite_add_test(test_stats);

	return testsuite_run(argc, argv);
}
