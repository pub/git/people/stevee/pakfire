/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/config.h>

#include "../testsuite.h"

static int test_get_and_set(const struct test* t) {
	struct pakfire_config* config = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_config_create(&config));

	// Set some values
	ASSERT_SUCCESS(pakfire_config_set(config, "section1", "A", "1"));
	ASSERT_SUCCESS(pakfire_config_set(config, "section1", "B", "2"));
	ASSERT_SUCCESS(pakfire_config_set(config, "section1", "C", "3"));

	// And some more in another section
	ASSERT_SUCCESS(pakfire_config_set(config, "section2", "A", "1"));
	ASSERT_SUCCESS(pakfire_config_set(config, "section2", "B", "2"));
	ASSERT_SUCCESS(pakfire_config_set(config, "section2", "C", "3"));

	// Try reading back the values
	ASSERT_STRING_EQUALS(pakfire_config_get(config, "section1", "A", NULL), "1");
	ASSERT_STRING_EQUALS(pakfire_config_get(config, "section2", "A", NULL), "1");

	// Read other data types
	ASSERT(pakfire_config_get_int(config, "section1", "A", 0) == 1);
	ASSERT(pakfire_config_get_int(config, "section1", "B", 0) == 2);
	ASSERT(pakfire_config_get_int(config, "section1", "C", 0) == 3);
	ASSERT(pakfire_config_get_bool(config, "section1", "A", 0) == 1);
	ASSERT(pakfire_config_get_bool(config, "section1", "B", 0) == 0);
	ASSERT(pakfire_config_get_bool(config, "section1", "C", 0) == 0);

	// Change a value
	ASSERT_SUCCESS(pakfire_config_set(config, "section1", "A", "ABC"));
	ASSERT_STRING_EQUALS(pakfire_config_get(config, "section1", "A", NULL), "ABC");

	// Try to access a non-existant value
	ASSERT_NULL(pakfire_config_get(config, "section1", "D", NULL));

	// Return default instead of NULL
	ASSERT_STRING_EQUALS(pakfire_config_get(config, "section1", "D", "XXX"), "XXX");

	// Check if sections exist
	ASSERT(pakfire_config_has_section(config, "section1"));
	ASSERT(!pakfire_config_has_section(config, "section3"));

	// Dump the configuration file
	ASSERT_SUCCESS(pakfire_config_dump(config, stdout));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	pakfire_config_unref(config);

	return r;
}

static int test_parse(const struct test* t) {
	int r = EXIT_FAILURE;
	char* TEST_INPUT =
		"key1 = value1\n"
		"\n"
		"# This is a comment\n"
		"[section1]\n"
		"key1 = value1\n"
		"key2 = value2\n"
		"[section2]\n"
		"key1 = value1\n"
		"key2 = value2\n"
		"[bytes]\n"
		"key0 = 0\n"
		"key1 = 1\n"
		"key1k = 1k\n"
		"key1X = 1X\n";

	FILE* f = fmemopen(TEST_INPUT, strlen(TEST_INPUT), "r");
	ASSERT(f);

	struct pakfire_config* config = NULL;

	ASSERT_SUCCESS(pakfire_config_create(&config));
	ASSERT_SUCCESS(pakfire_config_read(config, f));

	// Dump the configuration file
	ASSERT_SUCCESS(pakfire_config_dump(config, stdout));

	ASSERT_STRING_EQUALS(pakfire_config_get(config, "", "key1", NULL), "value1");
	ASSERT_STRING_EQUALS(pakfire_config_get(config, "section1", "key1", NULL), "value1");
	ASSERT_STRING_EQUALS(pakfire_config_get(config, "section1", "key2", NULL), "value2");
	ASSERT_STRING_EQUALS(pakfire_config_get(config, "section2", "key1", NULL), "value1");
	ASSERT_STRING_EQUALS(pakfire_config_get(config, "section2", "key2", NULL), "value2");

	// Check bytes
	ASSERT(pakfire_config_get_bytes(config, "bytes", "key0", 123) == 0);
	ASSERT(pakfire_config_get_bytes(config, "bytes", "key1", 123) == 1);
	ASSERT(pakfire_config_get_bytes(config, "bytes", "key1k", 123) == 1024);

	// Check an invalid value for bytes
	ASSERT_ERRNO(pakfire_config_get_bytes(config, "bytes", "key1X", 123) == 0, EINVAL);

	// Check for a non-existant key
	ASSERT(pakfire_config_get_bytes(config, "bytes", "keyXX", 123) == 123);

	// Get sections
	char** sections = pakfire_config_sections(config);
	ASSERT(sections);

	// Count sections
	size_t counter = 0;
	for (char** section = sections; *section; section++)
		counter++;

	ASSERT(counter == 3);

	// Check for section names
	ASSERT_STRING_EQUALS(sections[0], "section1");
	ASSERT_STRING_EQUALS(sections[1], "section2");
	ASSERT_STRING_EQUALS(sections[2], "bytes");
	ASSERT_NULL(sections[3]);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (f)
		fclose(f);
	pakfire_config_unref(config);

	return r;
}

static int test_parse_multiline(const struct test* t) {
	int r = EXIT_FAILURE;

	char* TEST_INPUT =
		"key1 =\n"
		" value1\n"
		" value2\n"
		"\n"
		"key2 = value1\n";

	FILE* f = fmemopen(TEST_INPUT, strlen(TEST_INPUT), "r");
	ASSERT(f);

	struct pakfire_config* config = NULL;

	ASSERT_SUCCESS(pakfire_config_create(&config));
	ASSERT_SUCCESS(pakfire_config_read(config, f));

	// Dump the configuration file
	ASSERT_SUCCESS(pakfire_config_dump(config, stdout));

	ASSERT_STRING_EQUALS(pakfire_config_get(config, "", "key1", NULL), "value1\nvalue2");
	ASSERT_STRING_EQUALS(pakfire_config_get(config, "", "key2", NULL), "value1");

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (f)
		fclose(f);
	pakfire_config_unref(config);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_get_and_set);
	testsuite_add_test(test_parse);
	testsuite_add_test(test_parse_multiline);

	return testsuite_run(argc, argv);
}
