/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <errno.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <sys/utsname.h>

#include <solv/queue.h>
#include <solv/selection.h>
#include <solv/solver.h>
#include <solv/transaction.h>

#ifdef ENABLE_DEBUG
# include <solv/solverdebug.h>
#endif

#include <pakfire/archive.h>
#include <pakfire/dependencies.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/problem.h>
#include <pakfire/request.h>
#include <pakfire/string.h>
#include <pakfire/transaction.h>
#include <pakfire/ui.h>
#include <pakfire/util.h>

struct pakfire_request {
	struct pakfire* pakfire;
	int nrefs;

	Solver* solver;
	Queue jobs;

	// Set if the request has been solved
	int solved:1;
};

/*
	These packages can be installed multiple times simultaneously
*/
const char* pakfire_multiinstall_packages[] = {
	"kernel",
	"kernel-devel",
	NULL,
};

static void pakfire_request_free(struct pakfire_request* request) {
	if (request->solver)
		solver_free(request->solver);
	queue_free(&request->jobs);

	pakfire_unref(request->pakfire);
	free(request);
}

/*
	This function translates any modifying flags into flags for libsolv
*/
static int pakfire_request_job_flags(int flags) {
	int solver_flags = 0;

	// Essential jobs
	if (flags & PAKFIRE_REQUEST_ESSENTIAL)
		solver_flags |= SOLVER_ESSENTIAL;

	// Keep any dependencies
	if (!(flags & PAKFIRE_REQUEST_KEEP_DEPS))
		solver_flags |= SOLVER_CLEANDEPS;

	// Drop orphans
	if (!(flags & PAKFIRE_REQUEST_KEEP_ORPHANED))
		solver_flags |= SOLVER_DROP_ORPHANED;

	return solver_flags;
}

static void pakfire_request_add_packages(struct pakfire_request* request,
		int flags, const char** packages) {
	Pool* pool = pakfire_get_solv_pool(request->pakfire);
	if (!pool)
		return;

	// Translate flags
	flags = pakfire_request_job_flags(flags);

	for (const char** package = packages; *package; package++) {
		Id id = pool_str2id(pool, *package, 1);
		if (id)
			queue_push2(&request->jobs, flags, id);
	}
}

static void pakfire_request_lock_running_kernel(struct pakfire_request* request) {
	struct utsname utsname;
	char buffer[NAME_MAX];

	// Call uname()
	int r = uname(&utsname);
	if (r) {
		ERROR(request->pakfire, "uname() failed: %m\n");
		return;
	}

	DEBUG(request->pakfire, "Locking running kernel %s\n", utsname.release);

	r = pakfire_string_format(buffer, "kernel(%s)", utsname.release);
	if (r)
		return;

	Pool* pool = pakfire_get_solv_pool(request->pakfire);

	// Add a locking pool job
	Id id = pool_str2id(pool, buffer, 1);
	if (id)
		queue_push2(&request->jobs, SOLVER_LOCK|SOLVER_SOLVABLE_PROVIDES, id);
}

static int setup_solver(struct pakfire_request* request, int flags) {
	// Can the solver downgrade packages?
	if (flags & PAKFIRE_REQUEST_ALLOW_DOWNGRADE)
		solver_set_flag(request->solver, SOLVER_FLAG_ALLOW_DOWNGRADE, 1);

	// Can the solver uninstall packages?
	if (flags & PAKFIRE_REQUEST_ALLOW_UNINSTALL)
		solver_set_flag(request->solver, SOLVER_FLAG_ALLOW_UNINSTALL, 1);

	// Do not install any recommended packages
	if (flags & PAKFIRE_REQUEST_WITHOUT_RECOMMENDED)
		solver_set_flag(request->solver, SOLVER_FLAG_IGNORE_RECOMMENDED, 1);

	// Add multiinstall packages
	pakfire_request_add_packages(request,
		SOLVER_MULTIVERSION|SOLVER_SOLVABLE_PROVIDES, pakfire_multiinstall_packages);

	// Lock the running kernel
	if (pakfire_on_root(request->pakfire))
		pakfire_request_lock_running_kernel(request);

	return 0;
}

int pakfire_request_create(struct pakfire_request** request,
		struct pakfire* pakfire, int flags) {
	Pool* pool = pakfire_get_solv_pool(pakfire);
	int r = 1;

	// Cannot create a solver when no installed repository has been set
	if (!pool->installed)
		return -EINVAL;

	// Allocate request
	struct pakfire_request* req = calloc(1, sizeof(*req));
	if (!req)
		return ENOMEM;

	req->pakfire = pakfire_ref(pakfire);
	req->nrefs = 1;

	// Allocate a job queue
	queue_init(&req->jobs);

	// Allocate solver
	req->solver = solver_create(pool);
	if (!req->solver) {
		ERROR(pakfire, "Could not allocate solver: %m\n");
		goto ERROR;
	}

	// Set up solver
	r = setup_solver(req, flags);
	if (r)
		goto ERROR;

	*request = req;
	return 0;

ERROR:
	pakfire_request_free(req);
	return r;
}

struct pakfire_request* pakfire_request_ref(struct pakfire_request* request) {
	request->nrefs++;

	return request;
}

struct pakfire_request* pakfire_request_unref(struct pakfire_request* request) {
	if (--request->nrefs > 0)
		return request;

	pakfire_request_free(request);
	return NULL;
}

Solver* pakfire_request_get_solver(struct pakfire_request* request) {
	return request->solver;
}

static int pakfire_request_pick_solution(struct pakfire_request* request) {
	// XXX TODO this is just a dummy
	return pakfire_ui_pick_solution(request->pakfire, request);
}

int pakfire_request_solve(struct pakfire_request* request) {
	int r;

	// Prepare pool
	pakfire_pool_internalize(request->pakfire);

#ifdef ENABLE_DEBUG
	Pool* pool = pakfire_get_solv_pool(request->pakfire);

	const char* selection = pool_selection2str(pool, &request->jobs, 0);
	if (selection) {
		DEBUG(request->pakfire, "Solving: %s\n", selection);
	}
#endif

	// Save time when we starting solving
	clock_t solving_start = clock();

	for (;;) {
		r = solver_solve(request->solver, &request->jobs);
		if (r == 0)
			break;

		// We land here, if the request could not be solved

#ifdef ENABLE_DEBUG
		// Print all solutions
		solver_printallsolutions(request->solver);
#endif

		// Ask the user to pick a solution
		r = pakfire_request_pick_solution(request);
		if (r)
			return r;
	}

	// Mark the request as solved
	request->solved = 1;

	// Save time when we finished solving
	clock_t solving_end = clock();

	DEBUG(request->pakfire, "Solved request in %.4fms\n",
		(double)(solving_end - solving_start) * 1000 / CLOCKS_PER_SEC);

#ifdef ENABLE_DEBUG
	solver_printdecisions(request->solver);
#endif

	// All okay
	return 0;
}

static int pakfire_request_is_file(const char* what) {
	return pakfire_string_endswith(what, ".pfm");
}

static int __pakfire_request_add(struct pakfire_request* request,
		const enum pakfire_request_action action, const Id type, const Id id, int flags) {
	Id _action = ID_NULL;
	int permitted_flags = PAKFIRE_REQUEST_ESSENTIAL;

	// Check if type/ID is not set for actions that don't support it
	switch (action) {
		case PAKFIRE_REQ_INSTALL:
		case PAKFIRE_REQ_ERASE:
		case PAKFIRE_REQ_UPDATE:
		case PAKFIRE_REQ_LOCK:
			break;

		default:
			if (type || id) {
				errno = EINVAL;
				return 1;
			}
	}

	// Select the correct action
	switch (action) {
		case PAKFIRE_REQ_INSTALL:
			_action = SOLVER_INSTALL;
			break;

		case PAKFIRE_REQ_ERASE:
			_action = SOLVER_ERASE;

			permitted_flags |= PAKFIRE_REQUEST_KEEP_DEPS;
			break;

		case PAKFIRE_REQ_UPDATE:
			_action = SOLVER_UPDATE;
			break;

		case PAKFIRE_REQ_UPDATE_ALL:
			_action = SOLVER_UPDATE|SOLVER_SOLVABLE_ALL;
			break;

		case PAKFIRE_REQ_SYNC:
			_action = SOLVER_DISTUPGRADE|SOLVER_SOLVABLE_ALL;

			permitted_flags |= PAKFIRE_REQUEST_KEEP_ORPHANED;
			break;

		case PAKFIRE_REQ_LOCK:
			_action = SOLVER_LOCK;
			break;

		case PAKFIRE_REQ_VERIFY:
			_action = SOLVER_VERIFY|SOLVER_SOLVABLE_ALL;
			break;
	}

	// Filter out any unsupported flags for the selected action
	flags &= permitted_flags;

	// Translate flags
	flags = pakfire_request_job_flags(flags);

	// XXX reset flags because the translation is broken
	flags = 0;

	// Add it to the job queue
	queue_push2(&request->jobs, _action|type|flags, id);

	return 0;
}

static int pakfire_request_add_job(struct pakfire_request* request,
		const enum pakfire_request_action action, const char* what, int extra_flags) {
	Pool* pool = pakfire_get_solv_pool(request->pakfire);

	// Make the pool ready
	pakfire_pool_internalize(request->pakfire);

	Queue jobs;
	queue_init(&jobs);

	int flags =
		// Select packages by name
		SELECTION_NAME|
		// Select packages by what they provide
		SELECTION_PROVIDES|
		// Process globbing patterns
		SELECTION_GLOB|
		// Select packages by their canonical name
		SELECTION_CANON|
		// Process .arch
		SELECTION_DOTARCH|
		// Process -release
		SELECTION_REL;

	// Process filelists?
	if (*what == '/')
		flags |= SELECTION_FILELIST;

	// Select all packages
	selection_make(pool, &jobs, what, flags);

	// Did we find anything?
	if (jobs.count == 0) {
		Id id = pakfire_str2dep(request->pakfire, what);
		if (!id)
			return 1;

		queue_push2(&jobs, SOLVER_SOLVABLE_PROVIDES, id);
	}

	DEBUG(request->pakfire, "Found %d match(es) for '%s'\n", jobs.count / 2, what);

	// Set action and global flags
	for (int i = 0; i < jobs.count; i += 2)
		__pakfire_request_add(request, action, jobs.elements[i], jobs.elements[i+1], extra_flags);

	queue_free(&jobs);

	return 0;
}

int pakfire_request_add(struct pakfire_request* request,
		const enum pakfire_request_action action, const char* what, int flags) {
	struct pakfire_package* package = NULL;
	int r;

	// Remove leading whitespace
	if (what) {
		while (*what && isspace(*what))
			what++;
	}

	if (!what) {
		r = __pakfire_request_add(request, action, ID_NULL, ID_NULL, flags);
		if (r)
			goto ERROR;

	// Check if we got given a URL or some file
	} else if (pakfire_string_is_url(what) || pakfire_request_is_file(what)) {
		// Add them to the commandline repository
		r = pakfire_commandline_add(request->pakfire, what, &package);
		if (r)
			goto ERROR;

		// Then add the package
		r = pakfire_request_add_package(request, action, package, flags);
		if (r)
			goto ERROR;

	// Otherwise try adding this as a job
	} else {
		r = pakfire_request_add_job(request, action, what, flags);
		if (r)
			goto ERROR;
	}

ERROR:
	if (package)
		pakfire_package_unref(package);

	return r;
}

int pakfire_request_add_package(struct pakfire_request* request,
		const enum pakfire_request_action action, struct pakfire_package* package, int flags) {
	// Get the solvable ID
	Id id = pakfire_package_id(package);

	// Add it to the job queue
	return __pakfire_request_add(request, action, SOLVER_SOLVABLE, id, flags);
}

int pakfire_request_next_problem(
		struct pakfire_request* request, struct pakfire_problem** problem) {
	Id id = 0;

	// Check input
	if (!problem) {
		errno = EINVAL;
		return 1;
	}

	// Fetch the ID of the previous problem
	if (*problem) {
		id = pakfire_problem_get_id(*problem);

		// Free the previous problem
		pakfire_problem_unref(*problem);
		*problem = NULL;
	}

	// Fetch the ID of the next problem
	id = solver_next_problem(request->solver, id);
	if (!id)
		return 0;

	// Create problem
	return pakfire_problem_create(problem, request->pakfire, request, id);
}

int pakfire_request_take_solution(struct pakfire_request* request,
		struct pakfire_solution* solution) {
	struct pakfire_problem* problem = pakfire_solution_get_problem(solution);

	// Fetch IDs
	Id problem_id = pakfire_problem_get_id(problem);
	Id solution_id = pakfire_solution_get_id(solution);

	// Feed the solution into the solver
	solver_take_solution(request->solver, problem_id, solution_id, &request->jobs);

	pakfire_problem_unref(problem);
	return 0;
}

int pakfire_request_get_transaction(struct pakfire_request* request,
		struct pakfire_transaction** transaction) {
	// XXX Check if we have solved successfully

	// Create the transaction
	return pakfire_transaction_create(transaction, request->pakfire, request->solver);
}
