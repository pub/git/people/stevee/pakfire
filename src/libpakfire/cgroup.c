/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>

#include <pakfire/cgroup.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define ROOT				"/sys/fs/cgroup"
#define BUFFER_SIZE			64 * 1024

enum pakfire_cgroup_controllers {
	PAKFIRE_CGROUP_CONTROLLER_CPU    = (1 << 0),
	PAKFIRE_CGROUP_CONTROLLER_MEMORY = (1 << 1),
	PAKFIRE_CGROUP_CONTROLLER_PIDS   = (1 << 2),
	PAKFIRE_CGROUP_CONTROLLER_IO     = (1 << 3),
};

static const enum pakfire_cgroup_controllers pakfire_cgroup_accounting_controllers =
	PAKFIRE_CGROUP_CONTROLLER_CPU |
	PAKFIRE_CGROUP_CONTROLLER_MEMORY |
	PAKFIRE_CGROUP_CONTROLLER_PIDS |
	PAKFIRE_CGROUP_CONTROLLER_IO;

struct pakfire_cgroup {
	struct pakfire* pakfire;
	int nrefs;

	// Flags
	int flags;

	// Store the path
	char path[PATH_MAX];

	// File descriptor to cgroup
	int fd;
};

// Returns true if this is the root cgroup
static int pakfire_cgroup_is_root(struct pakfire_cgroup* cgroup) {
	return !*cgroup->path;
}

static int pakfire_cgroup_has_flag(struct pakfire_cgroup* cgroup, int flag) {
	return cgroup->flags & flag;
}

static const char* pakfire_cgroup_name(struct pakfire_cgroup* cgroup) {
	if (pakfire_cgroup_is_root(cgroup))
		return "(root)";

	return cgroup->path;
}

static const char* pakfire_cgroup_controller_name(
		enum pakfire_cgroup_controllers controller) {
	switch (controller) {
		case PAKFIRE_CGROUP_CONTROLLER_CPU:
			return "cpu";

		case PAKFIRE_CGROUP_CONTROLLER_MEMORY:
			return "memory";

		case PAKFIRE_CGROUP_CONTROLLER_PIDS:
			return "pids";

		case PAKFIRE_CGROUP_CONTROLLER_IO:
			return "io";
	}

	return NULL;
}

static enum pakfire_cgroup_controllers pakfire_cgroup_find_controller_by_name(
		const char* name) {
	const char* n = NULL;

	// Walk through the bitmap
	for (unsigned int i = 1; i; i <<= 1) {
		n = pakfire_cgroup_controller_name(i);
		if (!n)
			break;

		// Match
		if (strcmp(name, n) == 0)
			return i;
	}

	// Nothing found
	return 0;
}

static struct pakfire_cgroup* pakfire_cgroup_parent(struct pakfire_cgroup* cgroup) {
	struct pakfire_cgroup* parent = NULL;
	int r;

	// Cannot return parent for root group
	if (pakfire_cgroup_is_root(cgroup))
		return NULL;

	// Determine the path of the parent
	const char* path = pakfire_dirname(cgroup->path);
	if (!path) {
		ERROR(cgroup->pakfire, "Could not determine path for parent cgroup: %m\n");
		return NULL;
	}

	// dirname() returns . if no directory component could be found
	if (strcmp(path, ".") == 0)
		path = NULL;

	// Open the cgroup
	r = pakfire_cgroup_open(&parent, cgroup->pakfire, path, 0);
	if (r) {
		ERROR(cgroup->pakfire, "Could not open parent cgroup: %m\n");
		parent = NULL;
	}

	return parent;
}

static void pakfire_cgroup_free(struct pakfire_cgroup* cgroup) {
	DEBUG(cgroup->pakfire, "Releasing cgroup %s at %p\n",
		pakfire_cgroup_name(cgroup), cgroup);

	// Close the file descriptor
	if (cgroup->fd)
		close(cgroup->fd);

	pakfire_unref(cgroup->pakfire);
	free(cgroup);
}

static int pakfire_cgroup_open_root(struct pakfire_cgroup* cgroup) {
	int fd = open(ROOT, O_DIRECTORY|O_PATH|O_CLOEXEC);
	if (fd < 0) {
		ERROR(cgroup->pakfire, "Could not open %s: %m\n", ROOT);
		return -1;
	}

	return fd;
}

static int __pakfire_cgroup_create(struct pakfire_cgroup* cgroup) {
	char path[PATH_MAX];
	int r;

	DEBUG(cgroup->pakfire, "Trying to create cgroup %s\n", pakfire_cgroup_name(cgroup));

	// Compose the absolute path
	r = pakfire_path_join(path, ROOT, cgroup->path);
	if (r)
		return 1;

	// Try creating the directory
	return pakfire_mkdir(path, 0755);
}

/*
	Opens the cgroup and returns a file descriptor.

	If the cgroup does not exist, it will try to create it.

	This function returns a negative value on error.
*/
static int __pakfire_cgroup_open(struct pakfire_cgroup* cgroup) {
	int fd = -1;
	int r;

	// Open file descriptor of the cgroup root
	int rootfd = pakfire_cgroup_open_root(cgroup);
	if (rootfd < 0)
		return -1;

	// Return the rootfd for the root group
	if (pakfire_cgroup_is_root(cgroup))
		return rootfd;

RETRY:
	fd = openat(rootfd, cgroup->path, O_DIRECTORY|O_PATH|O_CLOEXEC);
	if (fd < 0) {
		switch (errno) {
			// If the cgroup doesn't exist yet, try to create it
			case ENOENT:
				r = __pakfire_cgroup_create(cgroup);
				if (r)
					goto ERROR;

				// Retry open after successful creation
				goto RETRY;

			// Exit on all other errors
			default:
				ERROR(cgroup->pakfire, "Could not open cgroup %s: %m\n",
					pakfire_cgroup_name(cgroup));
				goto ERROR;
		}
	}

ERROR:
	if (rootfd > 0)
		close(rootfd);

	return fd;
}

static int pakfire_cgroup_access(struct pakfire_cgroup* cgroup, const char* path,
		int mode, int flags) {
	return faccessat(cgroup->fd, path, mode, flags);
}

static FILE* pakfire_cgroup_open_file(struct pakfire_cgroup* cgroup,
		const char* path, const char* mode) {
	FILE* f = NULL;

	// Open cgroup.procs
	int fd = openat(cgroup->fd, "cgroup.procs", O_CLOEXEC);
	if (fd < 0) {
		ERROR(cgroup->pakfire, "%s: Could not open %s: %m\n",
			pakfire_cgroup_name(cgroup), path);
		goto ERROR;
	}

	// Convert into file handle
	f = fdopen(fd, mode);
	if (!f)
		goto ERROR;

ERROR:
	if (fd)
		close(fd);

	return f;
}

static ssize_t pakfire_cgroup_read(struct pakfire_cgroup* cgroup, const char* path,
		char* buffer, size_t length) {
	ssize_t bytes_read = -1;

	// Check if this cgroup has been destroyed already
	if (!cgroup->fd) {
		ERROR(cgroup->pakfire, "Trying to read from destroyed cgroup\n");
		return -1;
	}

	// Open the file
	int fd = openat(cgroup->fd, path, O_CLOEXEC);
	if (fd < 0) {
		DEBUG(cgroup->pakfire, "Could not open %s/%s: %m\n",
			pakfire_cgroup_name(cgroup), path);
		goto ERROR;
	}

	// Read file content into buffer
	bytes_read = read(fd, buffer, length);
	if (bytes_read < 0) {
		DEBUG(cgroup->pakfire, "Could not read from %s/%s: %m\n",
			pakfire_cgroup_name(cgroup), path);
		goto ERROR;
	}

	// Terminate the buffer
	if ((size_t)bytes_read < length)
		buffer[bytes_read] = '\0';

ERROR:
	if (fd > 0)
		close(fd);

	return bytes_read;
}

static int pakfire_cgroup_write(struct pakfire_cgroup* cgroup,
		const char* path, const char* format, ...) {
	va_list args;
	int r = 0;

	// Check if this cgroup has been destroyed already
	if (!cgroup->fd) {
		ERROR(cgroup->pakfire, "Trying to write to destroyed cgroup\n");
		errno = EPERM;
		return 1;
	}

	// Open the file
	int fd = openat(cgroup->fd, path, O_WRONLY|O_CLOEXEC);
	if (fd < 0) {
		DEBUG(cgroup->pakfire, "Could not open %s/%s for writing: %m\n",
			pakfire_cgroup_name(cgroup), path);
		return 1;
	}

	// Write buffer
	va_start(args, format);
	ssize_t bytes_written = vdprintf(fd, format, args);
	va_end(args);

	// Check if content was written okay
	if (bytes_written < 0) {
		DEBUG(cgroup->pakfire, "Could not write to %s/%s: %m\n",
			pakfire_cgroup_name(cgroup), path);
		r = 1;
	}

	// Close fd
	close(fd);

	return r;
}

static int pakfire_cgroup_read_controllers(
		struct pakfire_cgroup* cgroup, const char* name) {
	char buffer[BUFFER_SIZE];
	char* p = NULL;

	// Discovered controllers
	int controllers = 0;

	// Read cgroup.controllers file
	ssize_t bytes_read = pakfire_cgroup_read(cgroup, name, buffer, sizeof(buffer));
	if (bytes_read < 0)
		return -1;

	// If the file was empty, there is nothing more to do
	if (bytes_read == 0)
		return 0;

	char* token = strtok_r(buffer, " \n", &p);

	while (token) {
		DEBUG(cgroup->pakfire, "Found controller '%s'\n", token);

		// Try finding this controller
		int controller = pakfire_cgroup_find_controller_by_name(token);
		if (controller)
			controllers |= controller;

		// Move on to next token
		token = strtok_r(NULL, " \n", &p);
	}

	// Return discovered controllers
	return controllers;
}

/*
	Returns a bitmap of all available controllers
*/
static int pakfire_cgroup_available_controllers(struct pakfire_cgroup* cgroup) {
	return pakfire_cgroup_read_controllers(cgroup, "cgroup.controllers");
}

/*
	Returns a bitmap of all enabled controllers
*/
static int pakfire_cgroup_enabled_controllers(struct pakfire_cgroup* cgroup) {
	return pakfire_cgroup_read_controllers(cgroup, "cgroup.subtree_control");
}

/*
	This function takes a bitmap of controllers that should be enabled.
*/
static int pakfire_cgroup_enable_controllers(struct pakfire_cgroup* cgroup,
		enum pakfire_cgroup_controllers controllers) {
	struct pakfire_cgroup* parent = NULL;
	int r = 1;

	// Find all enabled controllers
	const int enabled_controllers = pakfire_cgroup_enabled_controllers(cgroup);
	if (enabled_controllers < 0) {
		ERROR(cgroup->pakfire, "Could not fetch enabled controllers: %m\n");
		goto ERROR;
	}

	// Filter out anything that is already enabled
	controllers = (controllers & ~enabled_controllers);

	// Exit if everything is already enabled
	if (!controllers) {
		DEBUG(cgroup->pakfire, "All controllers are already enabled\n");
		return 0;
	}

	// Find all available controllers
	const int available_controllers = pakfire_cgroup_available_controllers(cgroup);
	if (available_controllers < 0) {
		ERROR(cgroup->pakfire, "Could not fetch available controllers: %m\n");
		goto ERROR;
	}

	// Are all controllers we need available, yet?
	if (controllers & ~available_controllers) {
		DEBUG(cgroup->pakfire, "Not all controllers are available, yet\n");

		parent = pakfire_cgroup_parent(cgroup);

		// Enable everything we need on the parent group
		if (parent) {
			r = pakfire_cgroup_enable_controllers(parent, controllers);
			if (r)
				goto ERROR;
		}
	}

	// Determine how many iterations we will need
	const int iterations = 1 << (sizeof(controllers) * 8 - __builtin_clz(controllers));

	// Iterate over all known controllers
	for (int controller = 1; controller < iterations; controller <<= 1) {
		// Skip enabling this controller if not requested
		if (!(controller & controllers))
			continue;

		// Fetch name
		const char* name = pakfire_cgroup_controller_name(controller);

		DEBUG(cgroup->pakfire, "Enabling controller %s in cgroup %s\n",
			name, pakfire_cgroup_name(cgroup));

		// Try enabling the controller (this will succeed if it already is enabled)
		r = pakfire_cgroup_write(cgroup, "cgroup.subtree_control", "+%s\n", name);
		if (r) {
			ERROR(cgroup->pakfire, "Could not enable controller %s in cgroup %s\n",
				name, pakfire_cgroup_name(cgroup));
			goto ERROR;
		}
	}

ERROR:
	if (parent)
		pakfire_cgroup_unref(parent);

	return r;
}

static int pakfire_cgroup_enable_accounting(struct pakfire_cgroup* cgroup) {
	// Enable all accounting controllers
	return pakfire_cgroup_enable_controllers(cgroup,
		pakfire_cgroup_accounting_controllers);
}

/*
	Entry function to open a new cgroup.

	If the cgroup doesn't exist, it will be created including any parent cgroups.
*/
int pakfire_cgroup_open(struct pakfire_cgroup** cgroup,
		struct pakfire* pakfire, const char* path, int flags) {
	int r = 1;

	// Allocate the cgroup struct
	struct pakfire_cgroup* c = calloc(1, sizeof(*c));
	if (!c)
		return 1;

	DEBUG(pakfire, "Allocated cgroup %s at %p\n", path, c);

	// Keep a reference to pakfire
	c->pakfire = pakfire_ref(pakfire);

	// Initialize reference counter
	c->nrefs = 1;

	// Copy path
	pakfire_string_set(c->path, path);

	// Copy flags
	c->flags = flags;

	// Open a file descriptor
	c->fd = __pakfire_cgroup_open(c);
	if (c->fd < 0)
		goto ERROR;

	// Enable accounting if requested
	if (pakfire_cgroup_has_flag(c, PAKFIRE_CGROUP_ENABLE_ACCOUNTING)) {
		r = pakfire_cgroup_enable_accounting(c);
		if (r)
			goto ERROR;
	}

	*cgroup = c;
	return 0;

ERROR:
	pakfire_cgroup_free(c);
	return r;
}

struct pakfire_cgroup* pakfire_cgroup_ref(struct pakfire_cgroup* cgroup) {
	++cgroup->nrefs;

	return cgroup;
}

struct pakfire_cgroup* pakfire_cgroup_unref(struct pakfire_cgroup* cgroup) {
	if (--cgroup->nrefs > 0)
		return cgroup;

	pakfire_cgroup_free(cgroup);
	return NULL;
}

// Open a child cgroup
int pakfire_cgroup_child(struct pakfire_cgroup** child,
		struct pakfire_cgroup* cgroup, const char* name, int flags) {
	char path[PATH_MAX];
	int r;

	// Check input
	if (!name) {
		errno = EINVAL;
		return 1;
	}

	// Join paths
	r = pakfire_path_join(path, cgroup->path, name);
	if (r)
		return 1;

	// Open the child group
	return pakfire_cgroup_open(child, cgroup->pakfire, path, flags);
}

static int pakfire_cgroup_procs_callback(struct pakfire_cgroup* cgroup,
		int (*callback)(struct pakfire_cgroup* cgroup, pid_t pid, void* data), void* data) {
	int r = 0;

	// Check if we have a callback
	if (!callback) {
		errno = EINVAL;
		return 1;
	}

	// Open cgroup.procs
	FILE* f = pakfire_cgroup_open_file(cgroup, "cgroup.procs", "r");
	if (!f)
		return 1;

	char* line = NULL;
	size_t l = 0;

	// Walk through all PIDs
	while (1) {
		ssize_t bytes_read = getline(&line, &l, f);
		if (bytes_read < 0)
			break;

		// Parse PID
		pid_t pid = strtol(line, NULL, 10);

		// Call callback function
		r = callback(cgroup, pid, data);
		if (r)
			break;
	}

	// Cleanup
	if (line)
		free(line);
	if (f)
		fclose(f);

	return r;
}

static int send_sigkill(struct pakfire_cgroup* cgroup, const pid_t pid, void* data) {
	DEBUG(cgroup->pakfire, "Sending signal SIGKILL to PID %d\n", pid);

	int r = kill(pid, SIGKILL);
	if (r < 0 && errno != ESRCH) {
		ERROR(cgroup->pakfire, "Could not send signal SIGKILL to PID %d: %m\n", pid);
		return r;
	}

	return r;
}

/*
	Immediately kills all processes in this cgroup
*/
static int pakfire_cgroup_killall(struct pakfire_cgroup* cgroup) {
	DEBUG(cgroup->pakfire, "%s: Killing all processes\n", pakfire_cgroup_name(cgroup));

	// Do we have support for cgroup.kill?
	int r = pakfire_cgroup_access(cgroup, "cgroup.kill", F_OK, 0);

	// Fall back to the legacy version
	if (r && errno == ENOENT) {
		return pakfire_cgroup_procs_callback(cgroup, send_sigkill, NULL);
	}

	return pakfire_cgroup_write(cgroup, "cgroup.kill", "1");
}

/*
	Immediately destroys this cgroup
*/
int pakfire_cgroup_destroy(struct pakfire_cgroup* cgroup) {
	int r;

	// Cannot call this for the root group
	if (pakfire_cgroup_is_root(cgroup)) {
		errno = EPERM;
		return 1;
	}

	DEBUG(cgroup->pakfire, "Destroying cgroup %s\n", pakfire_cgroup_name(cgroup));

	// Kill everything in this group
	r = pakfire_cgroup_killall(cgroup);
	if (r)
		return r;

	// Close the file descriptor
	if (cgroup->fd) {
		close(cgroup->fd);
		cgroup->fd = 0;
	}

	// Open the root directory
	int fd = pakfire_cgroup_open_root(cgroup);
	if (fd < 0)
		return 1;

	// Delete the directory
	r = unlinkat(fd, cgroup->path, AT_REMOVEDIR);
	if (r)
		ERROR(cgroup->pakfire, "Could not destroy cgroup: %m\n");

	// Close fd
	close(fd);

	return r;
}

int pakfire_cgroup_fd(struct pakfire_cgroup* cgroup) {
	return cgroup->fd;
}

// Memory

int pakfire_cgroup_set_guaranteed_memory(struct pakfire_cgroup* cgroup, size_t mem) {
	int r;

	// Enable memory controller
	r = pakfire_cgroup_enable_controllers(cgroup, PAKFIRE_CGROUP_CONTROLLER_MEMORY);
	if (r)
		return r;

	DEBUG(cgroup->pakfire, "%s: Setting guaranteed memory to %zu byte(s)\n",
		pakfire_cgroup_name(cgroup), mem);

	// Set value
	r = pakfire_cgroup_write(cgroup, "memory.min", "%zu\n", mem);
	if (r)
		ERROR(cgroup->pakfire, "%s: Could not set guaranteed memory: %m\n",
			pakfire_cgroup_name(cgroup));

	return r;
}

int pakfire_cgroup_set_memory_limit(struct pakfire_cgroup* cgroup, size_t mem) {
	int r;

	// Enable memory controller
	r = pakfire_cgroup_enable_controllers(cgroup, PAKFIRE_CGROUP_CONTROLLER_MEMORY);
	if (r)
		return r;

	DEBUG(cgroup->pakfire, "%s: Setting memory limit to %zu byte(s)\n",
		pakfire_cgroup_name(cgroup), mem);

	// Set value
	r = pakfire_cgroup_write(cgroup, "memory.max", "%zu\n", mem);
	if (r)
		ERROR(cgroup->pakfire, "%s: Could not set memory limit: %m\n",
			pakfire_cgroup_name(cgroup));

	return r;
}

// PIDs

int pakfire_cgroup_set_pid_limit(struct pakfire_cgroup* cgroup, size_t limit) {
	int r;

	// Enable PID controller
	r = pakfire_cgroup_enable_controllers(cgroup, PAKFIRE_CGROUP_CONTROLLER_PIDS);
	if (r)
		return r;

	DEBUG(cgroup->pakfire, "%s: Setting PID limit to %zu\n",
		pakfire_cgroup_name(cgroup), limit);

	// Set value
	r = pakfire_cgroup_write(cgroup, "pids.max", "%zu\n", limit);
	if (r)
		ERROR(cgroup->pakfire, "%s: Could not set PID limit: %m\n",
			pakfire_cgroup_name(cgroup));

	return r;
}

// Stats

static int __pakfire_cgroup_read_stats_line(struct pakfire_cgroup* cgroup,
		int (*callback)(struct pakfire_cgroup* cgroup, const char* key, unsigned long val, void* data),
		void* data, char* line) {
	char* p = NULL;

	DEBUG(cgroup->pakfire, "Parsing line: %s\n", line);

	char key[NAME_MAX];
	unsigned long val = 0;

	// Number of the field
	int i = 0;

	char* elem = strtok_r(line, " ", &p);
	while (elem) {
		switch (i++) {
			// First field is the key
			case 0:
				// Copy the key
				pakfire_string_set(key, elem);
				break;

			// The second field is some value
			case 1:
				val = strtoul(elem, NULL, 10);
				break;

			// Ignore the rest
			default:
				DEBUG(cgroup->pakfire, "%s: Unknown value in cgroup stats (%d): %s\n",
					pakfire_cgroup_name(cgroup), i, elem);
				break;
		}

		elem = strtok_r(NULL, " ", &p);
	}

	// Check if we parsed both fields
	if (i < 2) {
		ERROR(cgroup->pakfire, "Could not parse line\n");
		return 1;
	}

	// Call the callback
	return callback(cgroup, key, val, data);
}

static int __pakfire_cgroup_read_stats(struct pakfire_cgroup* cgroup, const char* path,
		int (*callback)(struct pakfire_cgroup* cgroup, const char* key, unsigned long val, void* data),
		void* data) {
	char* p = NULL;
	int r;

	char buffer[BUFFER_SIZE];

	DEBUG(cgroup->pakfire, "%s: Reading stats from %s\n", pakfire_cgroup_name(cgroup), path);

	// Open the file
	r = pakfire_cgroup_read(cgroup, path, buffer, sizeof(buffer));
	if (r < 0)
		goto ERROR;

	char* line = strtok_r(buffer, "\n", &p);
	while (line) {
		// Parse the line
		r = __pakfire_cgroup_read_stats_line(cgroup, callback, data, line);
		if (r)
			goto ERROR;

		// Move to the next line
		line = strtok_r(NULL, "\n", &p);
	}

ERROR:
	return r;
}

struct pakfire_cgroup_stat_entry {
	const char* key;
	unsigned long* val;
};

static int __pakfire_cgroup_parse_cpu_stats(struct pakfire_cgroup* cgroup,
		const char* key, unsigned long val, void* data) {
	struct pakfire_cgroup_cpu_stats* stats = (struct pakfire_cgroup_cpu_stats*)data;

	const struct pakfire_cgroup_stat_entry entries[] = {
		{ "system_usec", &stats->system_usec },
		{ "usage_usec", &stats->usage_usec },
		{ "user_usec", &stats->user_usec },
		{ NULL, NULL },
	};
	// Find and store value
	for (const struct pakfire_cgroup_stat_entry* entry = entries; entry->key; entry++) {
		if (strcmp(entry->key, key) == 0) {
			*entry->val = val;
			return 0;
		}
	}

	DEBUG(cgroup->pakfire, "Unknown key for CPU stats: %s = %ld\n", key, val);

	return 0;
}

static int __pakfire_cgroup_parse_memory_stats(struct pakfire_cgroup* cgroup,
		const char* key, unsigned long val, void* data) {
	struct pakfire_cgroup_memory_stats* stats = (struct pakfire_cgroup_memory_stats*)data;

	const struct pakfire_cgroup_stat_entry entries[] = {
		{ "anon", &stats->anon },
		{ "file", &stats->file },
		{ "kernel", &stats->kernel },
		{ "kernel_stack", &stats->kernel_stack },
		{ "pagetables", &stats->pagetables },
		{ "percpu", &stats->percpu },
		{ "sock", &stats->sock },
		{ "vmalloc", &stats->vmalloc },
		{ "shmem", &stats->shmem },
		{ "zswap", &stats->zswap },
		{ "zswapped", &stats->zswapped },
		{ "file_mapped", &stats->file_mapped },
		{ "file_dirty", &stats->file_dirty },
		{ "file_writeback", &stats->file_writeback },
		{ "swapcached", &stats->swapcached },
		{ "anon_thp", &stats->anon_thp },
		{ "file_thp", &stats->file_thp },
		{ "shmem_thp", &stats->shmem_thp },
		{ "inactive_anon", &stats->inactive_anon },
		{ "active_anon", &stats->active_anon },
		{ "inactive_file", &stats->inactive_file },
		{ "active_file", &stats->active_file },
		{ "unevictable", &stats->unevictable },
		{ "slab_reclaimable", &stats->slab_reclaimable },
		{ "slab_unreclaimable", &stats->slab_unreclaimable },
		{ "slab", &stats->slab },
		{ "workingset_refault_anon", &stats->workingset_refault_anon },
		{ "workingset_refault_file", &stats->workingset_refault_file },
		{ "workingset_activate_anon", &stats->workingset_activate_anon },
		{ "workingset_activate_file", &stats->workingset_activate_file },
		{ "workingset_restore_anon", &stats->workingset_restore_anon },
		{ "workingset_restore_file", &stats->workingset_restore_file },
		{ "workingset_nodereclaim", &stats->workingset_nodereclaim },
		{ "pgfault", &stats->pgfault },
		{ "pgmajfault", &stats->pgmajfault },
		{ "pgrefill", &stats->pgrefill },
		{ "pgscan", &stats->pgscan },
		{ "pgsteal", &stats->pgsteal },
		{ "pgactivate", &stats->pgactivate },
		{ "pgdeactivate", &stats->pgdeactivate },
		{ "pglazyfree", &stats->pglazyfree },
		{ "pglazyfreed", &stats->pglazyfreed },
		{ "thp_fault_alloc", &stats->thp_fault_alloc },
		{ "thp_collapse_alloc", &stats->thp_collapse_alloc },
		{ NULL, NULL },
	};

	// Find and store value
	for (const struct pakfire_cgroup_stat_entry* entry = entries; entry->key; entry++) {
		if (strcmp(entry->key, key) == 0) {
			*entry->val = val;
			return 0;
		}
	}

	// Log any unknown keys
	DEBUG(cgroup->pakfire, "Unknown key for memory stats: %s = %ld\n", key, val);

	return 0;
}

int pakfire_cgroup_stat(struct pakfire_cgroup* cgroup,
		struct pakfire_cgroup_stats* stats) {
	int r;

	// Check input
	if (!stats) {
		errno = EINVAL;
		return 1;
	}

	// Read CPU stats
	r = __pakfire_cgroup_read_stats(cgroup, "cpu.stat",
		__pakfire_cgroup_parse_cpu_stats, &stats->cpu);
	if (r)
		goto ERROR;

	// Read memory stats
	r = __pakfire_cgroup_read_stats(cgroup, "memory.stat",
		__pakfire_cgroup_parse_memory_stats, &stats->memory);
	if (r)
		goto ERROR;

ERROR:
	if (r)
		ERROR(cgroup->pakfire, "%s: Could not read cgroup stats: %m\n",
			pakfire_cgroup_name(cgroup));

	return r;
}

int pakfire_cgroup_stat_dump(struct pakfire_cgroup* cgroup,
		const struct pakfire_cgroup_stats* stats) {
	// Check input
	if (!stats) {
		errno = EINVAL;
		return 1;
	}

	DEBUG(cgroup->pakfire, "%s: Total CPU time usage: %lu\n",
		pakfire_cgroup_name(cgroup), stats->cpu.usage_usec);

	return 0;
}
