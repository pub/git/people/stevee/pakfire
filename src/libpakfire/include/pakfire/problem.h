/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PROBLEM_H
#define PAKFIRE_PROBLEM_H

struct pakfire_problem;

#include <pakfire/solution.h>

struct pakfire_problem* pakfire_problem_ref(struct pakfire_problem* problem);
struct pakfire_problem* pakfire_problem_unref(struct pakfire_problem* problem);

const char* pakfire_problem_to_string(struct pakfire_problem* problem);

struct pakfire_request* pakfire_problem_get_request(struct pakfire_problem* problem);

int pakfire_problem_next_solution(
	struct pakfire_problem* problem, struct pakfire_solution** solution);

#ifdef PAKFIRE_PRIVATE

#include <pakfire/request.h>

int pakfire_problem_create(struct pakfire_problem** problem, struct pakfire* pakfire,
	struct pakfire_request* request, Id id);

struct pakfire* pakfire_problem_get_pakfire(struct pakfire_problem* problem);
Id pakfire_problem_get_id(struct pakfire_problem* problem);

#endif

#endif /* PAKFIRE_PROBLEM_H */
