/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/util.h>

#include "../testsuite.h"

static int test_basename(const struct test* t) {
	const char* dir = "/a/b/c";

	ASSERT_STRING_EQUALS(pakfire_basename(dir), "c");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_dirname(const struct test* t) {
	const char* dir = "/a/b/c";

	ASSERT_STRING_EQUALS(pakfire_dirname(dir), "/a/b");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_mkdir(const struct test* t) {
	char path[PATH_MAX];

	// Create some random path
	ASSERT_SUCCESS(pakfire_path(t->pakfire, path, "%s", "/a/b/c/d/e"));

	// Create this directory
	ASSERT_SUCCESS(pakfire_mkdir(path, 0755));

	// Check if this exists
	ASSERT_SUCCESS(!pakfire_path_exists(path));

	// Create path again (nothing should happen)
	ASSERT_SUCCESS(pakfire_mkdir(path, 0755));

	// Create the parent directory for /test/1/file.txt
	ASSERT_SUCCESS(pakfire_mkparentdir("/test/1/file.txt", 0755));

	// Check that only the parent directory exists
	ASSERT_SUCCESS(!pakfire_path_exists("/test/1"));
	ASSERT_SUCCESS(!!pakfire_path_exists("/test/1/file.txt"));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_path_match(const struct test* t) {
	// Simple string match
	ASSERT_TRUE(pakfire_path_match("/abc", "/abc"));

	// Simple negative string match
	ASSERT_FALSE(pakfire_path_match("/abc", "/def"));

	// Simple star match
	ASSERT_TRUE(pakfire_path_match("/usr/*", "/usr/bin"));
	ASSERT_FALSE(pakfire_path_match("/usr/*", "/usr/bin/bash"));

	// Double star match
	ASSERT_TRUE(pakfire_path_match("/usr/**", "/usr/bin"));
	ASSERT_TRUE(pakfire_path_match("/usr/**", "/usr/bin/bash"));

	// Tripe star matches are invalid
	ASSERT_ERRNO(pakfire_path_match("/usr/***", "/usr/bin"), EINVAL);
	ASSERT_ERRNO(pakfire_path_match("/usr/***", "/usr/bin/bash"), EINVAL);

	// Partial paths shouldn't match
	ASSERT_FALSE(pakfire_path_match("/usr", "/usr/bin/bash"));

	// Check for stars in the middle
	ASSERT_TRUE(pakfire_path_match("/usr/lib64/*.so.*", "/usr/lib64/libblah.so.1"));
	ASSERT_TRUE(pakfire_path_match("/usr/lib64/**/*.so", "/usr/lib64/blah/module.so"));
	ASSERT_FALSE(pakfire_path_match("/usr/lib64/*.so.*", "/usr/lib64/beep"));

	// Relatve vs. absolute?
	ASSERT_FALSE(pakfire_path_match("/usr/lib/*.so", "relative-path"));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_basename);
	testsuite_add_test(test_dirname);
	testsuite_add_test(test_mkdir);
	testsuite_add_test(test_path_match);

	return testsuite_run(argc, argv);
}
