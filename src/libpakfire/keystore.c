/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fts.h>

#include <gpgme.h>

#include <pakfire/key.h>
#include <pakfire/keystore.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

static int pakfire_init_gpgme(struct pakfire* pakfire) {
	static int gpgme_initialized = 0;

	// Do nothing if gpgme is already initialized
	if (gpgme_initialized)
		return 0;

	// Initialize gpgme
	const char* version = gpgme_check_version(NULL);
	DEBUG(pakfire, "Loaded gpgme %s\n", version);

	// Check if we support OpenPGP
	gpgme_error_t error = gpgme_engine_check_version(GPGME_PROTOCOL_OpenPGP);
	if (gpg_err_code(error) != GPG_ERR_NO_ERROR) {
		ERROR(pakfire, "GPGME does not support OpenPGP\n");
		errno = ENOTSUP;
		return 1;
	}

	// Success
	gpgme_initialized = 1;
	return 0;
}

static int pakfire_keystore_import(struct pakfire* pakfire, gpgme_ctx_t ctx) {
	char path[PATH_MAX];

	// Make path
	int r = pakfire_path(pakfire, path, "%s", "/etc/pakfire/trusted.keys.d");
	if (r)
		return r;

	DEBUG(pakfire, "Loading keys from %s\n", path);

	char* paths[] = {
		path, NULL,
	};

	FTS* fts = fts_open(paths, FTS_NOCHDIR|FTS_NOSTAT, NULL);
	if (!fts)
		goto ERROR;

	for (;;) {
		FTSENT* fent = fts_read(fts);
		if (!fent)
			break;

		// Only handle files
		if (fent->fts_info == FTS_F) {
			FILE* f = fopen(fent->fts_path, "r");
			if (!f) {
				ERROR(pakfire, "Could not open %s: %m\n", fent->fts_path);
				continue;
			}

			// Import keys from file
			r = pakfire_key_import(pakfire, f, NULL);
			fclose(f);

			// End if key import was unsuccessful
			if (r)
				break;
		}
	}

	// Success
	r = 0;

ERROR:
	if (fts)
		fts_close(fts);

	return r;
}

int pakfire_keystore_init(struct pakfire* pakfire, gpgme_ctx_t* ctx) {
	char path[PATH_MAX] = PAKFIRE_TMP_DIR "/pakfire-keystore.XXXXXX";
	char* tmp = NULL;

	// Initialise GPGME
	int r = pakfire_init_gpgme(pakfire);
	if (r)
		return r;

	gpgme_error_t error = gpgme_new(ctx);
	if (gpg_err_code(error) != GPG_ERR_NO_ERROR)
		goto ERROR;

	// Enable offline mode?
	if (pakfire_has_flag(pakfire, PAKFIRE_FLAGS_OFFLINE))
		gpgme_set_offline(*ctx, 1);

	// Set output to be ASCII armoured
	gpgme_set_armor(*ctx, 1);

	// Create a temporary directory
	tmp = pakfire_mkdtemp(path);
	if (!tmp)
		goto ERROR;

	DEBUG(pakfire, "Using PGP database at %s\n", path);

	// Setup engine
	error = gpgme_ctx_set_engine_info(*ctx, GPGME_PROTOCOL_OpenPGP, NULL, path);
	if (gpg_err_code(error) != GPG_ERR_NO_ERROR)
		goto ERROR;

	// Show engine status
	gpgme_engine_info_t engine_info = gpgme_ctx_get_engine_info(*ctx);
	DEBUG(pakfire, "GPGME engine info: %s, path = %s\n",
		engine_info->file_name, engine_info->home_dir);

	// Import keys
	r = pakfire_keystore_import(pakfire, *ctx);
	if (r) {
		ERROR(pakfire, "Could not import keys: %m\n");
		goto ERROR;
	}

	// Success
	return 0;

ERROR:
	gpgme_release(*ctx);
	*ctx = NULL;

	// Cleanup temporary files
	if (tmp)
		pakfire_rmtree(tmp, 0);

	return r;
}

int pakfire_keystore_destroy(struct pakfire* pakfire, gpgme_ctx_t* ctx) {
	char path[PATH_MAX];

	// Retrieve engine info
	gpgme_engine_info_t engine_info = gpgme_ctx_get_engine_info(*ctx);

	// Store a copy of the home directory
	pakfire_string_set(path, engine_info->home_dir);

	DEBUG(pakfire, "Destroying keystore %s...\n", path);

	// Free GPGME context
	gpgme_release(*ctx);
	*ctx = NULL;

	// Remove home directory
	return pakfire_rmtree(path, 0);
}
