/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <glob.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>

#include <pakfire/dist.h>
#include <pakfire/package.h>
#include <pakfire/parser.h>
#include <pakfire/repo.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#include "../testsuite.h"

static const char* makefiles[] = {
	"data/beep.nm",
	"data/kernel.nm",
	NULL,
};

static int load_macros(struct pakfire_parser* parser) {
	const char* macros = TEST_SRC_PATH "../macros/*.macro";

	glob_t buffer;
	int r = glob(macros, 0, NULL, &buffer);
	ASSERT(r == 0);

	for (unsigned int i = 0; i < buffer.gl_pathc; i++) {
		ASSERT_SUCCESS(
			pakfire_parser_read_file(parser, buffer.gl_pathv[i], NULL)
		);
	}

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_parse(const struct test* t) {
	struct pakfire_parser* parser = NULL;
	const char** makefile = makefiles;
	char path[PATH_MAX];
	int r = EXIT_FAILURE;

	while (*makefile) {
		ASSERT_SUCCESS(pakfire_string_format(path, "%s/%s", TEST_SRC_PATH, *makefile));

		// Open file
		FILE* f = fopen(path, "r");
		ASSERT(f);

		parser = pakfire_parser_create(t->pakfire, NULL, NULL, 0);

		ASSERT_SUCCESS(pakfire_parser_read(parser, f, NULL));

		pakfire_parser_unref(parser);
		parser = NULL;
		fclose(f);

		// Next file
		makefile++;
	}

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (parser)
		pakfire_parser_unref(parser);

	return r;
}

static int test_macros(const struct test* t) {
	int r = EXIT_FAILURE;

	struct pakfire_parser* parser = pakfire_parser_create(t->pakfire, NULL, NULL, 0);
	ASSERT(parser);

	// Load 'em all
	ASSERT_SUCCESS(load_macros(parser));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (parser)
		pakfire_parser_unref(parser);

	return r;
}

static int test_packages(const struct test* t) {
	struct pakfire_parser* parser = NULL;
	struct pakfire_package* pkg = NULL;
	struct pakfire_repo* repo = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_repo_create(&repo, t->pakfire, "test"));
	ASSERT(repo);

	parser = pakfire_parser_create(t->pakfire, NULL, NULL,
		PAKFIRE_PARSER_FLAGS_EXPAND_COMMANDS);
	ASSERT(parser);

	// Set some architecture
	pakfire_parser_set(parser, NULL, "DISTRO_ARCH", "x86_64", 0);

	// Load macros
	ASSERT_SUCCESS(load_macros(parser));

	// Read beep.nm
	const char* path = TEST_SRC_PATH "data/beep.nm";

	ASSERT_SUCCESS(pakfire_parser_read_file(parser, path, NULL));

	// Create package
	r = pakfire_parser_create_package(parser, &pkg, repo, NULL, NULL);
	ASSERT(r == 0);
	ASSERT(pkg);

	// Dump package
	char* s = pakfire_package_dump(pkg, PAKFIRE_PKG_DUMP_LONG);
	ASSERT(s);

	printf("%s\n", s);

	// Check name
	const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME);
	ASSERT_STRING_EQUALS(name, "beep");

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (parser)
		pakfire_parser_unref(parser);
	if (pkg)
		pakfire_package_unref(pkg);
	if (repo)
		pakfire_repo_unref(repo);

	return r;
}

/*
	This test tries to dist() on a dummy package
*/
static int test_dist_dummy(const struct test* t) {
	int r = EXIT_FAILURE;

	struct pakfire_archive* archive = NULL;
	struct pakfire_package* package = NULL;
	struct pakfire_filelist* filelist = NULL;

	// Create a directory to write packages to
	char* tmp = test_mkdtemp();
	ASSERT(tmp);

	char* filename = NULL;

	// Attempt to dist the dummy package
	ASSERT_SUCCESS(pakfire_dist(t->pakfire,
		TEST_SRC_PATH "data/packages/dummy/dummy.nm", tmp, &filename));

	// Check if filename is set
	ASSERT(filename);

	// Check if we can read the archive
	ASSERT_SUCCESS(pakfire_archive_open(&archive, t->pakfire, filename));

	// Extract all package metadata
	ASSERT_SUCCESS(pakfire_archive_make_package(archive, NULL, &package));

	// Dump package information
	char* dump = pakfire_package_dump(package, PAKFIRE_PKG_DUMP_LONG|PAKFIRE_PKG_DUMP_FILELIST);
	if (dump) {
		printf("%s\n", dump);
		free(dump);
	}

	// Check name
	const char* name = pakfire_package_get_string(package, PAKFIRE_PKG_NAME);
	ASSERT_STRING_EQUALS(name, "dummy");

	// Check EVR
	const char* evr = pakfire_package_get_string(package, PAKFIRE_PKG_EVR);
	ASSERT_STRING_EQUALS(evr, "1.0-1");

	// Check arch
	const char* arch = pakfire_package_get_string(package, PAKFIRE_PKG_ARCH);
	ASSERT_STRING_EQUALS(arch, "src");

	// Check vendor
	const char* vendor = pakfire_package_get_string(package, PAKFIRE_PKG_VENDOR);
	ASSERT_STRING_EQUALS(vendor, pakfire_get_distro_vendor(t->pakfire));

	// Check UUID
	const char* uuid = pakfire_package_get_string(package, PAKFIRE_PKG_UUID);
	ASSERT(uuid);

	// Check groups
	const char* groups = pakfire_package_get_string(package, PAKFIRE_PKG_GROUPS);
	ASSERT_STRING_EQUALS(groups, "Dummy");

	// Check URL
	const char* url = pakfire_package_get_string(package, PAKFIRE_PKG_URL);
	ASSERT_STRING_EQUALS(url, "https://www.example.org");

	// Check license
	const char* license = pakfire_package_get_string(package, PAKFIRE_PKG_LICENSE);
	ASSERT_STRING_EQUALS(license, "GPLv2+");

	// Check summary
	const char* summary = pakfire_package_get_string(package, PAKFIRE_PKG_SUMMARY);
	ASSERT_STRING_EQUALS(summary, "This is a dummy package");

	// Check description
	const char* description = pakfire_package_get_string(package, PAKFIRE_PKG_DESCRIPTION);
	ASSERT(description);

	// Check size
	size_t installed_size = pakfire_package_get_num(package, PAKFIRE_PKG_INSTALLSIZE, 0);
	ASSERT(installed_size == 0);

	// Fetch the filelist
	filelist = pakfire_archive_get_filelist(archive);
	ASSERT(filelist);

	// There must be exactly one file in this package
	ASSERT(pakfire_filelist_length(filelist) == 1);

	// Everything okay
	r = EXIT_SUCCESS;

FAIL:
	if (archive)
		pakfire_archive_unref(archive);
	if (filelist)
		pakfire_filelist_unref(filelist);
	if (package)
		pakfire_package_unref(package);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_macros);
	testsuite_add_test(test_parse);
	testsuite_add_test(test_packages);
	testsuite_add_test(test_dist_dummy);

	return testsuite_run(argc, argv);
}
