/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_CONFIG_H
#define PAKFIRE_CONFIG_H

#ifdef PAKFIRE_PRIVATE

#include <stdio.h>

struct pakfire_config;

int pakfire_config_create(struct pakfire_config** config);
struct pakfire_config* pakfire_config_ref(struct pakfire_config* config);
struct pakfire_config* pakfire_config_unref(struct pakfire_config* config);

int pakfire_config_set(struct pakfire_config* config,
	const char* section, const char* key, const char* value);
int pakfire_config_set_format(struct pakfire_config* config,
	const char* section, const char* key, const char* format, ...);

const char* pakfire_config_get(struct pakfire_config* config,
	const char* section, const char* key, const char* _default);
long int pakfire_config_get_int(struct pakfire_config* config,
	const char* section, const char* key, long int _default);
int pakfire_config_get_bool(struct pakfire_config* config,
	const char* section, const char* key, int _default);
size_t pakfire_config_get_bytes(struct pakfire_config* config,
	const char* section, const char* key, const size_t _default);
FILE* pakfire_config_get_fopen(struct pakfire_config* config,
	const char* section, const char* key);

char** pakfire_config_sections(struct pakfire_config* config);
int pakfire_config_has_section(struct pakfire_config* config, const char* section);

int pakfire_config_read(struct pakfire_config* config, FILE* f);

int pakfire_config_dump(struct pakfire_config* config, FILE* f);

#endif

#endif /* PAKFIRE_CONFIG_H */
