/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PACKAGELIST_H
#define PAKFIRE_PACKAGELIST_H

struct pakfire_packagelist;

#include <pakfire/package.h>
#include <pakfire/pakfire.h>

int pakfire_packagelist_create(struct pakfire_packagelist** list, struct pakfire* pakfire);
struct pakfire_packagelist* pakfire_packagelist_ref(struct pakfire_packagelist* list);
struct pakfire_packagelist* pakfire_packagelist_unref(struct pakfire_packagelist* list);

size_t pakfire_packagelist_length(struct pakfire_packagelist* list);
struct pakfire_package* pakfire_packagelist_get(struct pakfire_packagelist* list, unsigned int index);

int pakfire_packagelist_push(struct pakfire_packagelist* list, struct pakfire_package* pkg);

#ifdef PAKFIRE_PRIVATE

#include <solv/queue.h>

typedef int (*pakfire_packagelist_walk_callback)
	(struct pakfire* pakfire, struct pakfire_package* pkg, void* p);

int pakfire_packagelist_walk(struct pakfire_packagelist* list,
	pakfire_packagelist_walk_callback callback, void* p);

int pakfire_packagelist_import_solvables(struct pakfire_packagelist* list, Queue* q);

#endif

#endif /* PAKFIRE_PACKAGELIST_H */
