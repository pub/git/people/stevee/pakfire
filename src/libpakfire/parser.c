/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fnmatch.h>
#include <linux/limits.h>
#include <regex.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include <pakfire/dependencies.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/parser.h>
#include <pakfire/pakfire.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_parser {
	struct pakfire* pakfire;
	int nrefs;

	struct pakfire_parser* parent;
	int flags;
	char* namespace;

	struct pakfire_parser_declaration** declarations;
	size_t num_declarations;

	// Regular expressions
	pcre2_code* regex_command;
	pcre2_code* regex_variable;
};

static int pakfire_parser_compile_regexes(struct pakfire_parser* parser) {
	int r;

	// Commands
	if (!parser->regex_command && (parser->flags & PAKFIRE_PARSER_FLAGS_EXPAND_COMMANDS)) {
		r = pakfire_compile_regex(parser->pakfire, &parser->regex_command,
			"%(\\(((?>[^()]|(?1))*)\\))");
		if (r)
			return r;
	}

	// Variables
	if (!parser->regex_variable) {
		r = pakfire_compile_regex(parser->pakfire, &parser->regex_variable,
			"%\\{([A-Za-z0-9_\\-]+)\\}");
		if (r)
			return r;
	}

	return 0;
}

struct pakfire_parser* pakfire_parser_create(struct pakfire* pakfire,
		struct pakfire_parser* parent, const char* namespace, int flags) {
	struct pakfire_parser* parser = calloc(1, sizeof(*parser));
	if (parser) {
		parser->pakfire = pakfire_ref(pakfire);
		parser->nrefs = 1;

		// Store a reference to the parent parser if we have one
		if (parent)
			parser->parent = pakfire_parser_ref(parent);

		// Store flags
		parser->flags = flags;

		// Make namespace
		pakfire_parser_set_namespace(parser, namespace);
	}

	return parser;
}

struct pakfire_parser* pakfire_parser_create_child(struct pakfire_parser* parser, const char* namespace) {
	return pakfire_parser_create(parser->pakfire, parser, namespace, parser->flags);
}

struct pakfire_parser* pakfire_parser_ref(struct pakfire_parser* parser) {
	++parser->nrefs;

	return parser;
}

struct pakfire* pakfire_parser_get_pakfire(struct pakfire_parser* parser) {
	return pakfire_ref(parser->pakfire);
}

static void pakfire_parser_free_declarations(struct pakfire_parser* parser) {
	if (!parser->declarations)
		return;

	for (unsigned int i = 0; i < parser->num_declarations; i++) {
		struct pakfire_parser_declaration* d = parser->declarations[i];

		// Free everything
		if (d->value)
			free(d->value);
		free(d);
	}

	free(parser->declarations);
}

static void pakfire_parser_free(struct pakfire_parser* parser) {
	// Release regular expressions
	if (parser->regex_variable)
		pcre2_code_free(parser->regex_variable);
	if (parser->regex_command)
		pcre2_code_free(parser->regex_command);

	pakfire_parser_free_declarations(parser);

	if (parser->namespace)
		free(parser->namespace);

	if (parser->parent)
		pakfire_parser_unref(parser->parent);

	pakfire_unref(parser->pakfire);
	free(parser);
}

struct pakfire_parser* pakfire_parser_unref(struct pakfire_parser* parser) {
	if (--parser->nrefs > 0)
		return parser;

	pakfire_parser_free(parser);
	return NULL;
}

struct pakfire_parser* pakfire_parser_get_parent(struct pakfire_parser* parser) {
	if (parser->parent)
		return pakfire_parser_ref(parser->parent);

	return NULL;
}

static struct pakfire_parser_declaration* pakfire_parser_get_declaration(
		struct pakfire_parser* parser, const char* namespace, const char* name) {
	if (!name) {
		errno = EINVAL;
		return NULL;
	}

	if (!namespace)
		namespace = "";

	if (*namespace)
		DEBUG(parser->pakfire, "%p: Looking up %s.%s\n", parser, namespace, name);
	else
		DEBUG(parser->pakfire, "%p: Looking up %s\n", parser, name);

	struct pakfire_parser_declaration* d;
	for (unsigned i = 0; i < parser->num_declarations; i++) {
		d = parser->declarations[i];
		if (!d)
			break;

		// Return if namespace and name match
		if ((strcmp(d->namespace, namespace) == 0) && (strcmp(d->name, name) == 0)) {
			DEBUG(parser->pakfire, "%p: Found result = %s\n", parser, d->value);

			return d;
		}
	}

	DEBUG(parser->pakfire, "%p: Nothing found\n", parser);

	return NULL;
}

static struct pakfire_parser_declaration* pakfire_parser_get_declaration_recursive(
		struct pakfire_parser* parser, const char* namespace, const char* name) {
	struct pakfire_parser_declaration* d = pakfire_parser_get_declaration(
			parser, namespace, name);
	if (d)
		return d;

	// Continue searching in parent parser
	if (parser->parent)
		return pakfire_parser_get_declaration_recursive(parser->parent, namespace, name);

	return NULL;
}

static void pakfire_parser_strip_namespace(char** s) {
	char* pos = strrchr(*s, '.');

	if (pos)
		(*s)[pos - *s] = '\0';
	else
		(*s)[0] = '\0';
}

static char* pakfire_parser_join(const char* fmt, const char* val1, const char* val2) {
	char* result = NULL;
	int r;

	// Merge both values if both are set
	if (val1 && *val1 && val2 && *val2) {
		r = asprintf(&result, fmt, val1, val2);
		if (r < 0)
			return NULL;

		return result;
	}

	// If only one value is set, return that one
	if (val1 && *val1)
		return strdup(val1);
	else if (val2 && *val2)
		return strdup(val2);

	// Return an empty string if no value is set
	return strdup("");
}

static struct pakfire_parser_declaration* pakfire_parser_find_declaration(
		struct pakfire_parser* parser, const char* namespace, const char* name) {
	struct pakfire_parser_declaration* d;
	char* n = NULL;

	// Create a working copy of namespace
	if (namespace)
		n = strdupa(namespace);

	while (1) {
		d = pakfire_parser_get_declaration_recursive(parser, n, name);
		if (d && d->value)
			return d;

		// End if we have exhausted the namespace
		if (!n || !*n)
			break;

		// Strip namespace
		pakfire_parser_strip_namespace(&n);
	}

	// Nothing found
	return NULL;
}

int pakfire_parser_set(struct pakfire_parser* parser,
		const char* namespace, const char* name, const char* value, int flags) {
	char* buffer = NULL;

	if (!name)
		return -EINVAL;

	if (!namespace)
		namespace = "";

	// Handle when name already exists
	struct pakfire_parser_declaration* d = pakfire_parser_get_declaration(parser, namespace, name);
	if (d) {
		if (flags & PAKFIRE_PARSER_DECLARATION_APPEND) {
			buffer = pakfire_parser_join("%s %s", d->value, value);
			if (!buffer)
				return 1;

			// Reset the append flag
			flags &= ~PAKFIRE_PARSER_DECLARATION_APPEND;
		} else {
			buffer = strdup(value);
		}

		// Replace value
		if (d->value)
			free(d->value);

		d->value = buffer;

		// Update flags
		if (flags)
			d->flags = flags;

		DEBUG(parser->pakfire, "%p: Updated declaration: %s.%s = %s\n",
			parser, d->namespace, d->name, d->value);

		// All done
		return 0;
	}

	// Allocate a new declaration
	d = calloc(1, sizeof(*d));
	if (!d)
		return -1;

	// Store namespace
	pakfire_string_set(d->namespace, namespace);

	// Import name & value
	pakfire_string_set(d->name, name);
	if (value)
		d->value = strdup(value);

	// Import flags
	d->flags = flags;

	DEBUG(parser->pakfire, "%p: New declaration (%d): %s.%s %s= %s\n",
		parser,
		d->flags,
		d->namespace,
		d->name,
		(d->flags & PAKFIRE_PARSER_DECLARATION_APPEND) ? "+" : "",
		d->value);

	// Assign new declaration to array
	parser->declarations = reallocarray(parser->declarations,
		sizeof(*parser->declarations), parser->num_declarations + 1);
	parser->declarations[parser->num_declarations++] = d;

	return 0;
}

int pakfire_parser_apply_declaration(struct pakfire_parser* parser,
		struct pakfire_parser_declaration* declaration) {
	DEBUG(parser->pakfire, "GOT HERE %d\n", declaration->flags);

	return pakfire_parser_set(parser, declaration->namespace,
		declaration->name, declaration->value, declaration->flags);
}

static int pakfire_parser_find_template(struct pakfire_parser* parser,
		char* template, const size_t length, const char* namespace) {
	DEBUG(parser->pakfire, "Looking up template in namespace '%s'\n", namespace);

	struct pakfire_parser_declaration* d = pakfire_parser_get_declaration(
			parser, namespace, "template");

	const char* value = (d && *d->value) ? d->value : "MAIN";

	// Format full variable name
	return __pakfire_string_format(template, length, "packages.template:%s", value);
}

static const char* pakfire_parser_get_raw(struct pakfire_parser* parser, const char* namespace, const char* name) {
	struct pakfire_parser_declaration* d = NULL;
	char template[NAME_MAX];
	int r;

	// First, perform a simple lookup
	d = pakfire_parser_get_declaration_recursive(parser, namespace, name);

	// Return a match when it actually contains a string
	if (d && d->value)
		return d->value;

	// If we couldn't find anything, we check if there is a template, and if that
	// has our value...
	if (namespace && pakfire_string_startswith(namespace, "packages.package:")) {
		r = pakfire_parser_find_template(parser, template, sizeof(template), namespace);
		if (r)
			return NULL;

		d = pakfire_parser_find_declaration(parser, template, name);
		if (d && d->value)
			return d->value;

		// If still nothing was found, search in the "build" namespace
		d = pakfire_parser_find_declaration(parser, "build", name);
		if (d && d->value)
			return d->value;
	}

	// Otherwise we walk up the namespace to find a match
	d = pakfire_parser_find_declaration(parser, namespace, name);
	if (d && d->value)
		return d->value;

	return NULL;
}

int pakfire_parser_append(struct pakfire_parser* parser,
		const char* namespace, const char* name, const char* value) {
	char* buffer = NULL;

	// Fetch the value of the current declaration
	const char* old_value = pakfire_parser_get_raw(parser, namespace, name);

	DEBUG(parser->pakfire, "%p: Old value for %s.%s = %s\n",
		parser, namespace, name, old_value);

	// Set the new value when there is no old one
	if (!old_value)
		return pakfire_parser_set(parser, namespace, name, value, 0);

	// Concat value
	int r = asprintf(&buffer, "%s %s", old_value, value);
	if (r < 0)
		return r;

	// Set the new value
	r = pakfire_parser_set(parser, namespace, name, buffer, 0);
	free(buffer);

	return r;
}

static int pakfire_parser_expand_commands(struct pakfire_parser* parser, char** buffer) {
	int r = 0;
	PCRE2_UCHAR* command = NULL;
	PCRE2_SIZE command_length;
	PCRE2_UCHAR* pattern = NULL;
	PCRE2_SIZE pattern_length;

	DEBUG(parser->pakfire, "Searching for commands in:\n%s\n", *buffer);

	// Allocate memory for results
	pcre2_match_data* match = pcre2_match_data_create_from_pattern(
		parser->regex_command, NULL);

	// Arguments passed to pakfire_execute
	const char* argv[4] = {
		"/bin/sh", "-c", NULL /* will be replaced by command later */, NULL,
	};

	while (1) {
		// Perform matching
		r = pcre2_jit_match(parser->regex_command,
			(PCRE2_UCHAR*)*buffer, strlen(*buffer), 0, 0, match, NULL);

		// End loop when we have expanded all variables
		if (r == PCRE2_ERROR_NOMATCH) {
			DEBUG(parser->pakfire, "No (more) matches found\n");
			r = 0;
			break;
		}

		// Extract the command
		r = pcre2_substring_get_bynumber(match, 2, &command, &command_length);
		if (r)
			goto ERROR;

		DEBUG(parser->pakfire, "Expanding command: %s\n", command);

		// Update argv
		argv[2] = (const char*)command;

		// The output of the command
		char* output = NULL;

		// Execute the command inside the Pakfire environment
		r = pakfire_jail_run(parser->pakfire, argv, 0, &output);
		if (r) {
			// Just log this and continue
			DEBUG(parser->pakfire, "Command '%s' failed with return code %d\n", command, r);
		}

		// Strip newline from output
		if (output)
			pakfire_remove_trailing_newline(output);

		// Find the entire matched pattern
		r = pcre2_substring_get_bynumber(match, 0, &pattern, &pattern_length);
		if (r) {
			if (output)
				free(output);

			goto ERROR;
		}

		// Replace all occurrences
		char* tmp = pakfire_string_replace(*buffer, (const char*)pattern, output);
		if (!tmp) {
			if (output)
				free(output);

			goto ERROR;
		}

		// Replace buffer
		free(*buffer);
		*buffer = tmp;

		// Free resources
		pcre2_substring_free(command);
		command = NULL;

		pcre2_substring_free(pattern);
		pattern = NULL;

		if (output)
			free(output);
	}

ERROR:
	pcre2_match_data_free(match);

	if (command)
		pcre2_substring_free(command);
	if (pattern)
		pcre2_substring_free(pattern);

	return r;
}

static int pakfire_parser_expand_variables(struct pakfire_parser* parser,
		const char* namespace, char** buffer) {
	int r = 0;
	PCRE2_UCHAR* variable = NULL;
	PCRE2_SIZE variable_length;
	PCRE2_UCHAR* pattern = NULL;
	PCRE2_SIZE pattern_length;

	// Allocate memory for results
	pcre2_match_data* match = pcre2_match_data_create_from_pattern(
		parser->regex_variable, NULL);

	// Search for any variables
	while (1) {
		// Perform matching
		r = pcre2_jit_match(parser->regex_variable,
			(PCRE2_UCHAR*)*buffer, strlen(*buffer), 0, 0, match, NULL);

		// End loop when we have expanded all variables
		if (r == PCRE2_ERROR_NOMATCH) {
			DEBUG(parser->pakfire, "No (more) matches found in: %s\n", *buffer);
			r = 0;
			break;
		}

		// Find the entire matched pattern
		r = pcre2_substring_get_bynumber(match, 0, &pattern, &pattern_length);
		if (r)
			goto ERROR;

		// Find the variable name
		r = pcre2_substring_get_bynumber(match, 1, &variable, &variable_length);
		if (r)
			goto ERROR;

		DEBUG(parser->pakfire, "Expanding variable: %s\n", variable);

		// Search for a declaration of this variable
		const char* repl = pakfire_parser_get_raw(parser, namespace, (const char*)variable);

		// Is this a recursive pattern?
		if (repl && pakfire_string_matches(repl, (const char*)pattern)) {
			DEBUG(parser->pakfire, "Recursion detected in %s\n", pattern);

			// Move up one step and lookup there
			if (namespace && *namespace) {
				char* parent_namespace = strdupa(namespace);
				pakfire_parser_strip_namespace(&parent_namespace);

				repl = pakfire_parser_get_raw(parser, parent_namespace, (const char*)variable);

			// If we have already reached the top namespace, we replace with an empty string
			} else {
				repl = NULL;
			}
		}

		// What is its value?
		if (repl) {
			DEBUG(parser->pakfire, "Replacing %%{%s} with '%s'\n", variable, repl);
		} else {
			DEBUG(parser->pakfire, "Replacing %%{%s} with an empty string\n", variable);
		}

		// Replace all occurrences
		char* tmp = pakfire_string_replace(*buffer, (const char*)pattern, (repl) ? repl : "");
		if (!tmp)
			goto ERROR;

		// Replace buffer
		free(*buffer);
		*buffer = tmp;

		// Free resources
		pcre2_substring_free(variable);
		variable = NULL;

		pcre2_substring_free(pattern);
		pattern = NULL;
	}

ERROR:
	pcre2_match_data_free(match);

	if (variable)
		pcre2_substring_free(variable);
	if (pattern)
		pcre2_substring_free(pattern);

	return r;
}

char* pakfire_parser_expand(struct pakfire_parser* parser,
		const char* namespace, const char* value) {
	// Return NULL when the value is NULL
	if (!value)
		return NULL;

	// Create a working copy of the string we are expanding
	char* buffer = strdup(value);

	// Fast path to check if there are any variables in here whatsoever
	char* pos = strchr(value, '%');
	if (!pos)
		return buffer;

	// Compile all regular expressions
	int r = pakfire_parser_compile_regexes(parser);
	if (r) {
		DEBUG(parser->pakfire, "Could not compile regular expressions: %m\n");
		goto ERROR;
	}

	// Expand all variables
	r = pakfire_parser_expand_variables(parser, namespace, &buffer);
	if (r) {
		DEBUG(parser->pakfire, "Failed to expand variables in '%s': %m\n", value);
		goto ERROR;
	}

	// Expand all commands
	if (parser->flags & PAKFIRE_PARSER_FLAGS_EXPAND_COMMANDS) {
		r = pakfire_parser_expand_commands(parser, &buffer);
		if (r) {
			DEBUG(parser->pakfire, "Failed to expand commands in '%s': %m\n", value);
			goto ERROR;
		}
	}

	return buffer;

ERROR:
	if (buffer)
		free(buffer);

	return NULL;
}

char* pakfire_parser_get(struct pakfire_parser* parser, const char* namespace, const char* name) {
	const char* value = pakfire_parser_get_raw(parser, namespace, name);

	// Return NULL when nothing was found
	if (!value)
		return NULL;

	// Otherwise return the expanded value
	return pakfire_parser_expand(parser, namespace, value);
}

static int append_to_array(char*** array, const char* s) {
	unsigned int length = 0;

	// Determine the length of the existing array
	if (*array) {
		for (char** element = *array; *element; element++)
			length++;
	}

	// Allocate space
	*array = reallocarray(*array, length + 2, sizeof(**array));
	if (!*array)
		return 1;

	// Copy the string to the heap
	char* p = strdup(s);
	if (!p)
		return 1;

	// Append p and terminate the array
	(*array)[length] = p;
	(*array)[length + 1] = NULL;

	return 0;
}

int pakfire_parser_get_filelist(struct pakfire_parser* parser, const char* namespace,
		const char* name, char*** includes, char*** excludes) {
	char* p = NULL;
	int r = 0;

	// Fetch the list
	char* list = pakfire_parser_get(parser, namespace, name);

	// Nothing to do for empty lists
	if (!list)
		return 0;

	const char* file = strtok_r(list, " \n", &p);

	// Split into includes and excludes
	while (file) {
		if (excludes && *file == '!') {
			r = append_to_array(excludes, file + 1);
			if (r)
				goto ERROR;
		} else {
			r = append_to_array(includes, file);
			if (r)
				goto ERROR;
		}

		// Move on to the next token
		file = strtok_r(NULL, " \n", &p);
	}

ERROR:
	if (list)
		free(list);

	return r;
}

char** pakfire_parser_get_split(struct pakfire_parser* parser,
		const char* namespace, const char* name, char delim) {
	char* value = pakfire_parser_get(parser, namespace, name);
	if (!value)
		return NULL;

	// Split the string
	char** list = pakfire_string_split(value, delim);
	free(value);

	return list;
}

char** pakfire_parser_list_namespaces(struct pakfire_parser* parser,
		const char* filter) {
	char** namespaces = NULL;
	unsigned int counter = 0;

	for (unsigned int i = 0; i < parser->num_declarations; i++) {
		struct pakfire_parser_declaration* d = parser->declarations[i];

		if (filter) {
			int r = fnmatch(filter, d->namespace, 0);

			// Skip declaration if it does not match
			if (r == FNM_NOMATCH)
				continue;

			// Check for any errors
			else if (r > 0) {
				ERROR(parser->pakfire, "fnmatch failed: %m\n");
				if (namespaces)
					free(namespaces);

				return NULL;
			}
		}

		// Does this already exist on the list?
		if (namespaces) {
			int found = 0;

			for (char** namespace = namespaces; *namespace; namespace++) {
				if (strcmp(d->namespace, *namespace) == 0) {
					found = 1;
					break;
				}
			}

			// Skip adding if it already exists
			if (found)
				continue;
		}

		namespaces = reallocarray(namespaces, counter + 2, sizeof(*namespaces));
		if (!namespaces)
			return NULL;

		// Add namespace and terminate the array
		namespaces[counter++] = d->namespace;
		namespaces[counter] = NULL;
	}

	return namespaces;
}

int pakfire_parser_merge(struct pakfire_parser* parser1, struct pakfire_parser* parser2) {
	struct pakfire_parser_declaration* d = NULL;
	char* namespace = NULL;
	char* value = NULL;
	int r;

	DEBUG(parser1->pakfire, "Merging parsers %p and %p\n", parser1, parser2);

	if (!parser2) {
		errno = EINVAL;
		return 1;
	}

	// Do not try to merge a parser with itself
	if (parser1 == parser2)
		return EINVAL;

	for (unsigned int i = 0; i < parser2->num_declarations; i++) {
		d = parser2->declarations[i];
		if (!d)
			break;

		// Make the new namespace
		namespace = pakfire_parser_join("%s.%s", parser2->namespace, d->namespace);
		if (!namespace) {
			r = 1;
			goto OUT;
		}

		const char* old_value = NULL;

		// Fetch the old value if we are supposed to be appending
		if (d->flags & PAKFIRE_PARSER_DECLARATION_APPEND) {
			old_value = pakfire_parser_get_raw(parser1, namespace, d->name);

			// XXX This is not ideal, to only reset once we have found an old
			// value because we might carry this over too far.
			// However, this is the only way to fix #12997
			if (old_value)
				d->flags &= ~PAKFIRE_PARSER_DECLARATION_APPEND;
		}

		// Make the new value
		value = pakfire_parser_join("%s %s", old_value, d->value);
		if (!value) {
			r = 1;
			goto OUT;
		}

		// Set everything in parser 1
		r = pakfire_parser_set(parser1, namespace, d->name, value, d->flags);
		if (r)
			goto OUT;

OUT:
		if (namespace)
			free(namespace);
		if (value)
			free(value);

		if (r)
			break;
	}

	return r;
}

int pakfire_parser_read(struct pakfire_parser* parser, FILE* f,
		struct pakfire_parser_error** error) {
	char* data;
	size_t len;

	int r = pakfire_read_file_into_buffer(f, &data, &len);
	if (r)
		return r;

	r = pakfire_parser_parse_data(parser, data, len, error);

	if (data)
		free(data);

	return r;
}

int pakfire_parser_read_file(struct pakfire_parser* parser, const char* path,
		struct pakfire_parser_error** error) {
	DEBUG(parser->pakfire, "Parsing %s...\n", path);

	FILE* f = fopen(path, "r");
	if (!f)
		return 1;

	int r = pakfire_parser_read(parser, f, error);
	fclose(f);

	return r;
}

int pakfire_parser_parse(struct pakfire_parser* parser,
		const char* data, size_t size, struct pakfire_parser_error** error) {
	return pakfire_parser_parse_data(parser, data, size, error);
}

char* pakfire_parser_dump(struct pakfire_parser* parser) {
	char buffer[NAME_MAX*2 + 1];
	char* s = NULL;

	for (unsigned int i = 0; i < parser->num_declarations; i++) {
		struct pakfire_parser_declaration* d = parser->declarations[i];

		if (d) {
			if (*d->namespace)
				pakfire_string_format(buffer, "%s.%s", d->namespace, d->name);
			else
				pakfire_string_set(buffer, d->name);

			asprintf(&s, "%s%-24s = %s\n", (s) ? s : "", buffer, d->value);
		}
	}

	return s;
}

const char* pakfire_parser_get_namespace(struct pakfire_parser* parser) {
	return parser->namespace;
}

int pakfire_parser_set_namespace(struct pakfire_parser* parser, const char* namespace) {
	if (parser->namespace)
		free(parser->namespace);

	if (namespace)
		parser->namespace = strdup(namespace);
	else
		parser->namespace = NULL;

	DEBUG(parser->pakfire, "%p: Set namespace to: %s\n", parser, parser->namespace);

	return 0;
}

char** pakfire_parser_make_environ(struct pakfire_parser* parser) {
	char** envp = NULL;
	unsigned int num = 0;

	for (unsigned int i = 0; i < parser->num_declarations; i++) {
		struct pakfire_parser_declaration* d = parser->declarations[i];
		if (!d)
			continue;

		// Is the export flag set?
		if (d->flags & PAKFIRE_PARSER_DECLARATION_EXPORT) {
			char* buffer = NULL;

			char* value = pakfire_parser_expand(parser, d->namespace, d->value);
			if (!value)
				goto ERROR;

			// Build line
			int r = asprintf(&buffer, "%s=%s", d->name, value);
			free(value);
			if (r < 0)
				goto ERROR;

			// Extend the array
			envp = reallocarray(envp, num + 2, sizeof(*envp));
			if (!envp)
				goto ERROR;

			envp[num++] = buffer;
			envp[num] = NULL;
		}
	}

	return envp;

ERROR:
	if (envp) {
		for (char** e = envp; *e; e++)
			free(*e);
		free(envp);
	}

	return NULL;
}

int pakfire_parser_create_package(struct pakfire_parser* parser,
		struct pakfire_package** pkg, struct pakfire_repo* repo, const char* namespace, const char* default_arch) {
	int r = 1;

	char* name = NULL;
	char* evr = NULL;
	char* arch = NULL;
	char* deps = NULL;

	DEBUG(parser->pakfire, "Building package from namespace '%s'\n", namespace);

	// Fetch name
	name = pakfire_parser_get(parser, namespace, "name");
	if (!name || !*name) {
		ERROR(parser->pakfire, "Name is empty\n");
		goto CLEANUP;
	}

	// Fetch EVR
	evr = pakfire_parser_get(parser, namespace, "evr");
	if (!evr || !*evr) {
		ERROR(parser->pakfire, "EVR is empty\n");
		goto CLEANUP;
	}

	// Fetch arch
	arch = pakfire_parser_get(parser, namespace, "arch");
	if (!arch || !*arch) {
		if (default_arch) {
			arch = strdup(default_arch);
			if (!arch)
				goto CLEANUP;
		} else {
			ERROR(parser->pakfire, "Arch is empty\n");
			goto CLEANUP;
		}
	}

	// Create a new package object
	r = pakfire_package_create(pkg, parser->pakfire, repo, name, evr, arch);
	if (r) {
		ERROR(parser->pakfire, "Could not create package: %m\n");
		goto CLEANUP;
	}

	// Is this a source package?
	int is_source = pakfire_package_is_source(*pkg);

	// Assign a new UUID to this package
	char* uuid = pakfire_generate_uuid();
	if (!uuid) {
		ERROR(parser->pakfire, "Generating a UUID failed: %m\n");
		goto CLEANUP;
	}

	pakfire_package_set_string(*pkg, PAKFIRE_PKG_UUID, uuid);
	free(uuid);

	// Set build time
	time_t now = time(NULL);

	pakfire_package_set_num(*pkg, PAKFIRE_PKG_BUILD_TIME, now);

	// Assign more attributes
	const struct attribute {
		const char* name;
		const enum pakfire_package_key key;
	} attributes[] = {
		{ "summary", PAKFIRE_PKG_SUMMARY },
		{ "description", PAKFIRE_PKG_DESCRIPTION, },
		{ "license", PAKFIRE_PKG_LICENSE, },
		{ "url", PAKFIRE_PKG_URL, },
		{ "groups", PAKFIRE_PKG_GROUPS, },
		{ "vendor", PAKFIRE_PKG_VENDOR, },
		{ "packager", PAKFIRE_PKG_PACKAGER, },
		{ NULL },
	};

	for (const struct attribute* a = attributes; a->name; a++) {
		char* value = pakfire_parser_get(parser, namespace, a->name);
		if (!value)
			continue;

		r = pakfire_package_set_string(*pkg, a->key, value);
		free(value);

		if (r) {
			ERROR(parser->pakfire, "Could not set %s: %m\n", a->name);
			goto CLEANUP;
		}
	}

	// Fetch build dependencies
	if (is_source) {
		deps = pakfire_parser_get(parser, "build", "requires");
		if (deps) {
			r = pakfire_str2deps(parser->pakfire, *pkg, PAKFIRE_PKG_REQUIRES, deps);
			if (r)
				goto CLEANUP;
		}
	} else {
		for (const struct pakfire_dep* dep = pakfire_deps; dep->key; dep++) {
			deps = pakfire_parser_get(parser, namespace, dep->name);
			if (deps) {
				r = pakfire_str2deps(parser->pakfire, *pkg, dep->key, deps);
				if (r)
					goto CLEANUP;
			}
		}
	}

	// All okay
	r = 0;

CLEANUP:
	if (r)
		ERROR(parser->pakfire, "Could not create package: %m\n");

	if (name)
		free(name);
	if (evr)
		free(evr);
	if (arch)
		free(arch);
	if (deps)
		free(deps);

	return r;
}

// Error

struct pakfire_parser_error {
	struct pakfire_parser* parser;
	int nrefs;

	char* filename;
	int line;
	char* message;
};

int pakfire_parser_error_create(struct pakfire_parser_error** error,
		struct pakfire_parser* parser, const char* filename, int line, const char* message) {
	struct pakfire_parser_error* e = calloc(1, sizeof(*e));
	if (!e)
		return ENOMEM;

	// Initialize reference counter
	e->nrefs = 1;

	e->parser = pakfire_parser_ref(parser);

	// Copy all input values
	if (filename)
		e->filename = strdup(filename);

	e->line = line;

	if (message)
		e->message = strdup(message);

	*error = e;

	return 0;
}

struct pakfire_parser_error* pakfire_parser_error_ref(
		struct pakfire_parser_error* error) {
	++error->nrefs;

	return error;
}

static void pakfire_parser_error_free(struct pakfire_parser_error* error) {
	pakfire_parser_unref(error->parser);

	if (error->filename)
		free(error->filename);

	if (error->message)
		free(error->message);

	free(error);
}

struct pakfire_parser_error* pakfire_parser_error_unref(
		struct pakfire_parser_error* error) {
	if (--error->nrefs > 0)
		return error;

	pakfire_parser_error_free(error);

	return NULL;
}

const char* pakfire_parser_error_get_filename(
		struct pakfire_parser_error* error) {
	return error->filename;
}

int pakfire_parser_error_get_line(struct pakfire_parser_error* error) {
	return error->line;
}

const char* pakfire_parser_error_get_message(
		struct pakfire_parser_error* error) {
	return error->message;
}
