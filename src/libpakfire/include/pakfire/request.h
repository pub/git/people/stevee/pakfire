/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_REQUEST_H
#define PAKFIRE_REQUEST_H

enum pakfire_request_flags {
	PAKFIRE_REQUEST_WITHOUT_RECOMMENDED = 1 << 0,
	PAKFIRE_REQUEST_ALLOW_UNINSTALL     = 1 << 1,
	PAKFIRE_REQUEST_ALLOW_DOWNGRADE     = 1 << 2,
};

enum pakfire_request_job_flags {
	PAKFIRE_REQUEST_KEEP_DEPS           = 1 << 0,
	PAKFIRE_REQUEST_KEEP_ORPHANED       = 1 << 1,
	PAKFIRE_REQUEST_ESSENTIAL           = 1 << 2,
};

#ifdef PAKFIRE_PRIVATE

#include <solv/solver.h>

#include <pakfire/package.h>
#include <pakfire/problem.h>
#include <pakfire/solution.h>
#include <pakfire/transaction.h>

struct pakfire_request;

enum pakfire_request_action {
	PAKFIRE_REQ_INSTALL,
	PAKFIRE_REQ_ERASE,
	PAKFIRE_REQ_UPDATE,
	PAKFIRE_REQ_UPDATE_ALL,
	PAKFIRE_REQ_SYNC,
	PAKFIRE_REQ_LOCK,
	PAKFIRE_REQ_VERIFY,
};

int pakfire_request_create(struct pakfire_request** request, struct pakfire* pakfire, int flags);

struct pakfire_request* pakfire_request_ref(struct pakfire_request* request);
struct pakfire_request* pakfire_request_unref(struct pakfire_request* request);

int pakfire_request_solve(struct pakfire_request* request);

int pakfire_request_add(struct pakfire_request* request,
	const enum pakfire_request_action action, const char* what, int flags);
int pakfire_request_add_package(struct pakfire_request* request,
	const enum pakfire_request_action action, struct pakfire_package* package, int flags);

int pakfire_request_take_solution(struct pakfire_request* request,
	struct pakfire_solution* solution);

Solver* pakfire_request_get_solver(struct pakfire_request* request);

int pakfire_request_next_problem(
	struct pakfire_request* request, struct pakfire_problem** problem);

int pakfire_request_get_transaction(struct pakfire_request* request,
	struct pakfire_transaction** transaction);

#endif

#endif /* PAKFIRE_REQUEST_H */
