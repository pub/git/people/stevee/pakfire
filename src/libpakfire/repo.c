/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <linux/limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <solv/repo.h>
#include <solv/repo_solv.h>
#include <solv/repo_write.h>

#include <json.h>

#include <pakfire/archive.h>
#include <pakfire/compress.h>
#include <pakfire/config.h>
#include <pakfire/constants.h>
#include <pakfire/downloader.h>
#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/i18n.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/private.h>
#include <pakfire/progressbar.h>
#include <pakfire/repo.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

// Refresh mirrorlists once a day
#define REFRESH_AGE_MIRRORLIST			24 * 3600

// Refresh repository metadata every 10 minutes
#define REFRESH_AGE_METADATA			600

struct pakfire_repo_appdata {
	char* description;
	char* baseurl;

	// Key fingerprint
	char key[PAKFIRE_KEY_FPR_MAXLEN];

	// Mirrorlist
	char* mirrorlist_url;
	char mirrorlist[PATH_MAX];

	// Markers
	int ready:1;
};

struct pakfire_repo {
	struct pakfire* pakfire;
	int nrefs;

	Repo* repo;
	struct pakfire_repo_appdata* appdata;

	struct pakfire_mirrorlist* mirrorlist;
};

int pakfire_repo_is_internal(struct pakfire_repo* repo) {
	const char* name = pakfire_repo_get_name(repo);
	if (!name)
		return 0;

	return (*name == '@');
}

int pakfire_repo_is_local(struct pakfire_repo* repo) {
	if (!repo->appdata->baseurl)
		return 0;

	return pakfire_string_startswith(repo->appdata->baseurl, "file://");
}

int pakfire_repo_name_equals(struct pakfire_repo* repo, const char* name) {
	const char* n = pakfire_repo_get_name(repo);
	if (!n)
		return 0;

	return strcmp(n, name) == 0;
}

static int pakfire_repo_is_commandline(struct pakfire_repo* repo) {
	return pakfire_repo_name_equals(repo, PAKFIRE_REPO_COMMANDLINE);
}

#define pakfire_repo_path(repo, path, format, ...) \
	__pakfire_repo_path(repo, path, sizeof(path), format, __VA_ARGS__)

static int __pakfire_repo_path(struct pakfire_repo* repo,
		char* path, const size_t length, const char* format, ...) {
	char buffer[PATH_MAX];
	va_list args;
	int r;

	// Format the path
	va_start(args, format);
	r = __pakfire_string_vformat(buffer, sizeof(buffer), format, args);
	va_end(args);
	if (r)
		return r;

	// For local repositories return the local path
	if (pakfire_repo_is_local(repo))
		return __pakfire_string_format(path, length,
			"%s/%s", pakfire_repo_get_path(repo), buffer);

	const char* name = pakfire_repo_get_name(repo);

	// Otherwise return the cached path
	return __pakfire_cache_path(repo->pakfire, path, length, "%s/%s", name, buffer);
}

static int pakfire_repo_retrieve(
		struct pakfire_repo* repo,
		const char* title,
		const char* url,
		const char* path,
		enum pakfire_digest_types md,
		const unsigned char* expected_digest,
		const size_t expected_digest_length,
		enum pakfire_transfer_flags flags) {
	struct pakfire_downloader* downloader;
	int r = pakfire_downloader_create(&downloader, repo->pakfire);
	if (r)
		return r;

#if 0
	// Fetch mirrorlist
	struct pakfire_mirrorlist* mirrorlist = pakfire_repo_get_mirrorlist(repo);
#endif

	// Retrieve the database file
	r = pakfire_downloader_retrieve(downloader, repo->appdata->baseurl, NULL,
		title, url, path, md, expected_digest, expected_digest_length, flags);

#if 0
	if (mirrorlist)
		pakfire_mirrorlist_unref(mirrorlist);
#endif
	pakfire_downloader_unref(downloader);

	return r;
}

static int pakfire_repo_import_key(struct pakfire_repo* repo, FILE* f) {
	struct pakfire_key** keys = NULL;
	int r;

	// Import any keys from the file descriptor
	r = pakfire_key_import(repo->pakfire, f, &keys);
	if (r)
		return r;

	/*
		XXX It can happen, that more than one repository are using the same
		key which can only be imported once. The second time, the key won't
		be returned which lets us end up with that repository having no key.
	*/

	for (struct pakfire_key** key = keys; *key; key++) {
		char* dump = pakfire_key_dump(*key);

		if (dump)
			DEBUG(repo->pakfire, "Imported key:\n%s\n", dump);

		// Fetch the key's fingerprint
		const char* fingerprint = pakfire_key_get_fingerprint(*key);
		if (!fingerprint) {
			r = 1;
			goto ERROR;
		}

		// Store the fingerprint
		r = pakfire_string_set(repo->appdata->key, fingerprint);
		if (r)
			goto ERROR;
	}

ERROR:
	if (keys) {
		for (struct pakfire_key** key = keys; *key; key++)
			pakfire_key_unref(*key);
		free(keys);
	}

	return r;
}

int pakfire_repo_import(struct pakfire* pakfire, struct pakfire_config* config) {
	char** sections = pakfire_config_sections(config);

	// The configuration seems to be empty
	if (!sections)
		return 0;

	struct pakfire_repo* repo = NULL;
	int r = 0;

	// Walk through all sections
	for (char** section = sections; *section; section++) {
		// Skip sections that do not start with "repo:"
		if (!pakfire_string_startswith(*section, "repo:"))
			continue;

		const char* name = (*section) + strlen("repo:");
		if (!*name)
			continue;

		DEBUG(pakfire, "Creating repository %s\n", name);

		// Create a new repository
		r = pakfire_repo_create(&repo, pakfire, name);
		if (r) {
			ERROR(pakfire, "Could not create repository '%s': %m\n", name);
			goto ERROR;
		}

		// Enabled
		int enabled = pakfire_config_get_bool(config, *section, "enabled", 1);
		pakfire_repo_set_enabled(repo, enabled);

		// Priority
		int priority = pakfire_config_get_int(config, *section, "priority", 0);
		if (priority)
			pakfire_repo_set_priority(repo, priority);

		// Description
		const char* description = pakfire_config_get(config, *section, "description", NULL);
		if (description)
			pakfire_repo_set_description(repo, description);

		// baseurl
		const char* baseurl = pakfire_config_get(config, *section, "baseurl", NULL);
		if (baseurl)
			pakfire_repo_set_baseurl(repo, baseurl);

		// mirrors
		const char* mirrors = pakfire_config_get(config, *section, "mirrors", NULL);
		if (mirrors)
			pakfire_repo_set_mirrorlist_url(repo, mirrors);

		// Key
		FILE* key = pakfire_config_get_fopen(config, *section, "key");
		if (key) {
			r = pakfire_repo_import_key(repo, key);
			if (r) {
				pakfire_repo_unref(repo);
				goto ERROR;
			}
		}

		pakfire_repo_unref(repo);
	}

	// Refresh repositories
	r = pakfire_refresh(pakfire, 0);
	if (r)
		goto ERROR;

ERROR:
	for (char** section = sections; *section; section++)
		free(*section);
	free(sections);

	return r;
}

Id pakfire_repo_add_solvable(struct pakfire_repo* repo) {
	Id id = repo_add_solvable(repo->repo);

	// Mark this repository as changed
	pakfire_repo_has_changed(repo);

	return id;
}

int pakfire_repo_add_archive(struct pakfire_repo* repo,
		struct pakfire_archive* archive, struct pakfire_package** package) {
	return pakfire_archive_make_package(archive, repo, package);
}

struct pakfire_mirrorlist* pakfire_repo_get_mirrorlist(struct pakfire_repo* repo) {
	if (!repo->mirrorlist) {
		// No cache path set
		if (!*repo->appdata->mirrorlist)
			return NULL;

		int r = pakfire_mirrorlist_create(&repo->mirrorlist, repo->pakfire);
		if (r) {
			ERROR(repo->pakfire, "Could not create mirrorlist: %m\n");
			return NULL;
		}

		// Read the mirrorlist
		r = pakfire_mirrorlist_read(repo->mirrorlist, repo->appdata->mirrorlist);
		if (r) {
			ERROR(repo->pakfire, "Could not read mirrorlist %s: %m\n",
				repo->appdata->mirrorlist);

			// Destroy what we have created
			pakfire_mirrorlist_unref(repo->mirrorlist);
			repo->mirrorlist = NULL;

			return NULL;
		}
	}

	return pakfire_mirrorlist_ref(repo->mirrorlist);
}

static int pakfire_repo_download_database(struct pakfire_repo* repo,
		const char* filename, const char* path) {
	char title[NAME_MAX];
	char url[PATH_MAX];
	int r;

	// Do nothing if the file already exists
	if (pakfire_path_exists(path)) {
		DEBUG(repo->pakfire, "Database %s already present. Skipping download\n", filename);
		return 0;
	}

	const char* name = pakfire_repo_get_name(repo);

	// Make title
	r = pakfire_string_format(title, _("Package Database: %s"), name);
	if (r)
		return r;

	// Make download URL
	r = pakfire_string_format(url, "repodata/%s", filename);
	if (r)
		return r;

	return pakfire_repo_retrieve(repo, title, url, path, 0, NULL, 0, 0);
}

static int pakfire_repo_read_database(struct pakfire_repo* repo, const char* path) {
	FILE* f = NULL;
	int r;

	DEBUG(repo->pakfire, "Read package database from %s...\n", path);

	// Open the database file
	f = fopen(path, "r");
	if (!f) {
		ERROR(repo->pakfire, "Could not open package database at %s: %m\n", path);
		r = 1;
		goto ERROR;
	}

	// Drop any previous data
	r = pakfire_repo_clear(repo);
	if (r)
		goto ERROR;

	// Read database
	r = pakfire_repo_read_solv(repo, f, 0);
	if (r)
		goto ERROR;

ERROR:
	if (f)
		fclose(f);

	return r;
}

static int pakfire_repo_read_metadata(struct pakfire_repo* repo, const char* path) {
	char database_path[PATH_MAX];
	int r;

	DEBUG(repo->pakfire, "Reading repository metadata from %s...\n", path);

	struct json_object* json = pakfire_json_parse_from_file(repo->pakfire, path);
	if (!json) {
		switch (errno) {
			case ENOENT:
				// If the repository is local, we will just scan it
				if (pakfire_repo_is_local(repo)) {
					DEBUG(repo->pakfire, "No metadata available on local repository."
						" Falling back to scan...\n");

					return pakfire_repo_scan(repo, 0);
				}
				break;
		}

		ERROR(repo->pakfire, "Could not parse metadata from %s: %m\n", path);
		return 1;
	}

	struct json_object* database = NULL;

	// Search for the database name
	int found = json_object_object_get_ex(json, "database", &database);
	if (!found) {
		ERROR(repo->pakfire, "Could not read database name from metadata\n");
		r = 1;
		goto ERROR;
	}

	const char* filename = json_object_get_string(database);

	// Make the path to the database
	r = pakfire_repo_path(repo, database_path, "repodata/%s", filename);
	if (r)
		goto ERROR;

	// Download the database
	if (!pakfire_repo_is_local(repo) && !pakfire_has_flag(repo->pakfire, PAKFIRE_FLAGS_OFFLINE)) {
		r = pakfire_repo_download_database(repo, filename, database_path);
		if (r)
			goto ERROR;
	}

	// Read the database
	r = pakfire_repo_read_database(repo, database_path);
	if (r)
		goto ERROR;

ERROR:
	// Free the parsed JSON object
	json_object_put(json);

	return r;
}

static int pakfire_repo_refresh_mirrorlist(struct pakfire_repo* repo, const int force) {
	int r;

	// Local repositories don't need a mirrorlist
	if (pakfire_repo_is_local(repo))
		return 0;

	// Do nothing if running in offline mode
	if (pakfire_has_flag(repo->pakfire, PAKFIRE_FLAGS_OFFLINE))
		return 0;

	// This repository does not have a mirrorlist
	if (!repo->appdata->mirrorlist_url)
		return 0;

	// Make path to mirrorlist
	r = pakfire_cache_path(repo->pakfire, repo->appdata->mirrorlist,
		"repodata/%s/mirrorlist", pakfire_repo_get_name(repo));
	if (r)
		return r;

	// Check if this needs to be refreshed
	if (!force) {
		time_t age = pakfire_path_age(repo->appdata->mirrorlist);

		if (age > 0 && age < REFRESH_AGE_MIRRORLIST) {
			DEBUG(repo->pakfire, "Skip refreshing mirrorlist which is %lds old\n", age);
			return 0;
		}
	}

	return pakfire_repo_retrieve(repo, NULL,
		repo->appdata->mirrorlist_url, repo->appdata->mirrorlist,
		0, NULL, 0, PAKFIRE_TRANSFER_NOPROGRESS);
}

static int pakfire_repo_download_metadata(struct pakfire_repo* repo, const char* path, const int force) {
	// Check if this needs to be refreshed
	if (!force) {
		time_t age = pakfire_path_age(path);

		if (age > 0 && age < REFRESH_AGE_METADATA) {
			DEBUG(repo->pakfire, "Skip refreshing metadata which is %lds old\n", age);
			return 0;
		}
	}

	return pakfire_repo_retrieve(repo, NULL, "repodata/repomd.json", path,
		0, NULL, 0, PAKFIRE_TRANSFER_NOPROGRESS);
}

static int pakfire_repo_refresh_metadata(struct pakfire_repo* repo, const int force) {
	char path[PATH_MAX];
	int r;

	// Make the metadata path
	r = pakfire_repo_path(repo, path, "%s", "repodata/repomd.json");
	if (r)
		return r;

	// Don't try to refresh the metadata for local repositories and if we are running offline
	if (!pakfire_repo_is_local(repo) && !pakfire_has_flag(repo->pakfire, PAKFIRE_FLAGS_OFFLINE)) {
		r = pakfire_repo_download_metadata(repo, path, force);
		if (r)
			return r;
	}

	// Parse metadata
	return pakfire_repo_read_metadata(repo, path);
}

static void free_repo_appdata(struct pakfire_repo_appdata* appdata) {
	if (!appdata)
		return;

	if (appdata->description)
		free(appdata->description);

	if (appdata->mirrorlist_url)
		free(appdata->mirrorlist_url);

	free(appdata);
}

static void pakfire_repo_free(struct pakfire_repo* repo, const int free_repo) {
	if (free_repo) {
		free_repo_appdata(repo->repo->appdata);
		repo_free(repo->repo, 0);
	}

	if (repo->mirrorlist)
		pakfire_mirrorlist_unref(repo->mirrorlist);
	pakfire_unref(repo->pakfire);
	free(repo);
}

void pakfire_repo_free_all(struct pakfire* pakfire) {
	Pool* pool = pakfire_get_solv_pool(pakfire);
	if (!pool)
		return;

	Repo* repo;
	int i;

	FOR_REPOS(i, repo) {
		free_repo_appdata(repo->appdata);
		repo_free(repo, 0);
	}
}

PAKFIRE_EXPORT int pakfire_repo_create(struct pakfire_repo** repo,
		struct pakfire* pakfire, const char* name) {
	int r = 1;

	// Reset repo pointer
	*repo = NULL;

	// Return existing repositories with the same name
	struct pakfire_repo* rep = pakfire_get_repo(pakfire, name);
	if (rep)
		goto SUCCESS;

	// Create a new one
	rep = calloc(1, sizeof(*rep));
	if (!rep)
		return 1;

	rep->pakfire = pakfire_ref(pakfire);
	rep->nrefs = 1;

	Pool* pool = pakfire_get_solv_pool(pakfire);

	// Allocate a libsolv repository
	rep->repo = repo_create(pool, name);
	if (!rep->repo) {
		ERROR(rep->pakfire, "Could not allocate repo: %m\n");
		goto ERROR;
	}

	// Allocate repository appdata
	rep->appdata = rep->repo->appdata = calloc(1, sizeof(*rep->appdata));
	if (!rep->appdata) {
		ERROR(rep->pakfire, "Could not allocate repo appdata\n");
		goto ERROR;
	}

	// Setup/clear repository data
	r = pakfire_repo_clear(rep);
	if (r)
		goto ERROR;

SUCCESS:
	*repo = rep;
	return 0;

ERROR:
	pakfire_repo_free(rep, 1);

	return r;
}

struct pakfire_repo* pakfire_repo_create_from_repo(struct pakfire* pakfire, Repo* r) {
	struct pakfire_repo* repo = calloc(1, sizeof(*repo));
	if (repo) {
		DEBUG(pakfire, "Allocated Repo at %p\n", repo);
		repo->nrefs = 1;

		repo->pakfire = pakfire_ref(pakfire);

		// Reference repository
		repo->repo = r;
		repo->appdata = r->appdata;
	}

	return repo;
}

PAKFIRE_EXPORT struct pakfire_repo* pakfire_repo_ref(struct pakfire_repo* repo) {
	repo->nrefs++;

	return repo;
}

PAKFIRE_EXPORT struct pakfire_repo* pakfire_repo_unref(struct pakfire_repo* repo) {
	if (--repo->nrefs > 0)
		return repo;

	pakfire_repo_free(repo, 0);

	return NULL;
}

PAKFIRE_EXPORT struct pakfire* pakfire_repo_get_pakfire(struct pakfire_repo* repo) {
	return pakfire_ref(repo->pakfire);
}

int pakfire_repo_clear(struct pakfire_repo* repo) {
	repo_empty(repo->repo, 0);

	return 0;
}

Repo* pakfire_repo_get_repo(struct pakfire_repo* repo) {
	return repo->repo;
}

Repodata* pakfire_repo_get_repodata(struct pakfire_repo* repo) {
	return repo_last_repodata(repo->repo);
}

PAKFIRE_EXPORT int pakfire_repo_identical(struct pakfire_repo* repo1, struct pakfire_repo* repo2) {
	Repo* r1 = repo1->repo;
	Repo* r2 = repo2->repo;

	return strcmp(r1->name, r2->name);
}

PAKFIRE_EXPORT int pakfire_repo_cmp(struct pakfire_repo* repo1, struct pakfire_repo* repo2) {
	Repo* r1 = repo1->repo;
	Repo* r2 = repo2->repo;

	if (r1->priority > r2->priority)
		return 1;

	else if (r1->priority < r2->priority)
		return -1;

	return strcmp(r1->name, r2->name);
}

PAKFIRE_EXPORT int pakfire_repo_count(struct pakfire_repo* repo) {
	Pool* pool = pakfire_get_solv_pool(repo->pakfire);
	int cnt = 0;

	for (int i = 2; i < pool->nsolvables; i++) {
		Solvable* s = pool->solvables + i;
		if (s->repo && s->repo == repo->repo)
			cnt++;
	}

	return cnt;
}

void pakfire_repo_has_changed(struct pakfire_repo* repo) {
	repo->appdata->ready = 0;

	// Mark pool as changed, too
	pakfire_pool_has_changed(repo->pakfire);
}

int pakfire_repo_internalize(struct pakfire_repo* repo, int flags) {
	if (repo->appdata->ready)
		return 0;

	// Internalize all repository data
	repo_internalize(repo->repo);

	// Mark this repository as ready
	repo->appdata->ready = 1;

	return 0;
}

PAKFIRE_EXPORT const char* pakfire_repo_get_name(struct pakfire_repo* repo) {
	return repo->repo->name;
}

PAKFIRE_EXPORT const char* pakfire_repo_get_description(struct pakfire_repo* repo) {
	return repo->appdata->description;
}

PAKFIRE_EXPORT int pakfire_repo_set_description(struct pakfire_repo* repo, const char* description) {
	if (repo->appdata->description)
		free(repo->appdata->description);

	if (description)
		repo->appdata->description = strdup(description);
	else
		repo->appdata->description = NULL;

	return 0;
}

PAKFIRE_EXPORT int pakfire_repo_get_enabled(struct pakfire_repo* repo) {
	return !repo->repo->disabled;
}

PAKFIRE_EXPORT void pakfire_repo_set_enabled(struct pakfire_repo* repo, int enabled) {
	repo->repo->disabled = !enabled;

	pakfire_repo_has_changed(repo);
}

PAKFIRE_EXPORT int pakfire_repo_get_priority(struct pakfire_repo* repo) {
	return repo->repo->priority;
}

PAKFIRE_EXPORT void pakfire_repo_set_priority(struct pakfire_repo* repo, int priority) {
	repo->repo->priority = priority;
}

PAKFIRE_EXPORT const char* pakfire_repo_get_baseurl(struct pakfire_repo* repo) {
	return repo->appdata->baseurl;
}

static char* pakfire_repo_url_replace(struct pakfire_repo* repo, const char* url) {
	if (!url)
		return NULL;

	const struct replacement {
		const char* pattern;
		const char* replacement;
	} replacements[] = {
		{ "%{name}", pakfire_repo_get_name(repo) },
		{ "%{arch}", pakfire_get_arch(repo->pakfire) },
		{ "%{distro}", pakfire_get_distro_id(repo->pakfire) },
		{ "%{version}", pakfire_get_distro_version_id(repo->pakfire) },
		{ NULL, NULL },
	};

	char* buffer = strdup(url);
	if (!buffer)
		return NULL;

	for (const struct replacement* repl = replacements; repl->pattern; repl++) {
		char* r = pakfire_string_replace(
			buffer, repl->pattern, repl->replacement);
		free(buffer);

		// Break on any errors
		if (!r)
			return NULL;

		// Free the old buffer and continue working with the new data
		buffer = r;
	}

#ifdef ENABLE_DEBUG
	if (strcmp(url, buffer) != 0) {
		DEBUG(repo->pakfire, "Repository URL updated:");
		DEBUG(repo->pakfire, "  From: %s\n", url);
		DEBUG(repo->pakfire, "  To  : %s\n", buffer);
	}
#endif

	return buffer;
}

PAKFIRE_EXPORT int pakfire_repo_set_baseurl(struct pakfire_repo* repo, const char* baseurl) {
	if (repo->appdata->baseurl)
		free(repo->appdata->baseurl);

	// Store URL
	repo->appdata->baseurl = pakfire_repo_url_replace(repo, baseurl);
	if (!repo->appdata->baseurl)
		return 1;

	return 0;
}

const char* pakfire_repo_get_path(struct pakfire_repo* repo) {
	if (!pakfire_repo_is_local(repo))
		return NULL;

	return repo->appdata->baseurl + strlen("file://");
}

PAKFIRE_EXPORT struct pakfire_key* pakfire_repo_get_key(struct pakfire_repo* repo) {
	return pakfire_key_get(repo->pakfire, repo->appdata->key);
}

PAKFIRE_EXPORT const char* pakfire_repo_get_mirrorlist_url(struct pakfire_repo* repo) {
	return repo->appdata->mirrorlist_url;
}

PAKFIRE_EXPORT int pakfire_repo_set_mirrorlist_url(struct pakfire_repo* repo, const char* url) {
	if (repo->appdata->mirrorlist_url)
		free(repo->appdata->mirrorlist_url);

	// Store URL
	repo->appdata->mirrorlist_url = pakfire_repo_url_replace(repo, url);
	if (!repo->appdata->mirrorlist_url)
		return 1;

	return 0;
}

PAKFIRE_EXPORT int pakfire_repo_write_config(struct pakfire_repo* repo, FILE* f) {
	int r;

	// Do nothing for the installed repository
	if (pakfire_repo_is_installed_repo(repo))
		return 0;

	struct pakfire_config* config = NULL;
	char* section = NULL;

	struct pakfire_key* key = NULL;
	char* buffer = NULL;
	size_t length = 0;

	// Create section name
	r = asprintf(&section, "repo:%s", pakfire_repo_get_name(repo));
	if (r < 0)
		goto ERROR;

	// Create a new configuration
	r = pakfire_config_create(&config);
	if (r)
		goto ERROR;

	// Description
	const char* description = pakfire_repo_get_description(repo);
	if (description) {
		r = pakfire_config_set(config, section, "description", description);
		if (r) {
			ERROR(repo->pakfire, "Could not set description: %m\n");
			goto ERROR;
		}
	}

	// Disabled?
	if (!pakfire_repo_get_enabled(repo)) {
		r = pakfire_config_set(config, section, "enabled", "0");
		if (r) {
			ERROR(repo->pakfire, "Could not set enabled: %m\n");
			goto ERROR;
		}
	}

	// Base URL
	const char* baseurl = pakfire_repo_get_baseurl(repo);
	if (baseurl) {
		r = pakfire_config_set(config, section, "baseurl", baseurl);
		if (r) {
			ERROR(repo->pakfire, "Could not set base URL: %m\n");
			goto ERROR;
		}
	}

	// Mirror List
	const char* mirrorlist = pakfire_repo_get_mirrorlist_url(repo);
	if (mirrorlist) {
		r = pakfire_config_set(config, section, "mirrors", mirrorlist);
		if (r) {
			ERROR(repo->pakfire, "Could not set mirrors: %m\n");
			goto ERROR;
		}
	}

	// Key
	key = pakfire_repo_get_key(repo);
	if (key) {
		r = pakfire_key_get_public_key(key, &buffer, &length);
		if (r) {
			ERROR(repo->pakfire, "Could not fetch public key: %m\n");
			goto ERROR;
		}

		r = pakfire_config_set_format(config, section, "key", "%.*s", length, buffer);
		if (r) {
			ERROR(repo->pakfire, "Could not set key: %m\n");
			goto ERROR;
		}
	}

	// Priority
	unsigned int priority = pakfire_repo_get_priority(repo);
	if (priority) {
		r = pakfire_config_set_format(config, section, "priority", "%d", priority);
		if (r) {
			ERROR(repo->pakfire, "Could not set priority: %m\n");
			goto ERROR;
		}
	}

	// Write the configuration
	r = pakfire_config_dump(config, f);

ERROR:
	if (key)
		pakfire_key_unref(key);
	if (config)
		pakfire_config_unref(config);
	if (section)
		free(section);
	if (buffer)
		free(buffer);

	return r;
}

PAKFIRE_EXPORT int pakfire_repo_is_installed_repo(struct pakfire_repo* repo) {
	struct pakfire_repo* installed_repo = pakfire_get_installed_repo(repo->pakfire);
	if (!installed_repo)
		return 0;

	int r = pakfire_repo_identical(repo, installed_repo);

	pakfire_repo_unref(installed_repo);

	return (r == 0);
}

static int pakfire_repo_download(struct pakfire_repo* repo, const char* url,
		struct pakfire_package** package) {
	struct pakfire_downloader* downloader = NULL;
	FILE* f = NULL;
	int r;

	char path[PATH_MAX] = PAKFIRE_TMP_DIR "/pakfire-download.XXXXXX";

	// Allocate a temporary file name
	f = pakfire_mktemp(path, 0);
	if (!f) {
		r = 1;
		goto ERROR;
	}

	// Create the downloader
	r = pakfire_downloader_create(&downloader, repo->pakfire);
	if (r)
		goto ERROR;

	// Download the file
	r = pakfire_downloader_retrieve(downloader, NULL, NULL, NULL,
		url, path, 0, NULL, 0, PAKFIRE_TRANSFER_NOTEMP);
	if (r)
		goto ERROR;

	// Try to add it to this repository
	r = pakfire_repo_add(repo, path, package);
	if (r)
		goto ERROR;

ERROR:
	if (downloader)
		pakfire_downloader_unref(downloader);
	if (f)
		fclose(f);

	return r;
}

int pakfire_repo_add(struct pakfire_repo* repo, const char* path,
		struct pakfire_package** package) {
	struct pakfire_archive* archive = NULL;
	int r;

	// This operation is only supported for the command line repository
	if (!pakfire_repo_is_commandline(repo)) {
		errno = ENOTSUP;
		return 1;
	}

	// Download the package if we got given a URL
	if (pakfire_string_is_url(path))
		return pakfire_repo_download(repo, path, package);

	// Try to open the archive
	r = pakfire_archive_open(&archive, repo->pakfire, path);
	if (r)
		goto ERROR;

	// Add it to this repository
	r = pakfire_repo_add_archive(repo, archive, package);
	if (r)
		goto ERROR;

	const char* nevra = pakfire_package_get_string(*package, PAKFIRE_PKG_NEVRA);
	if (!nevra) {
		r = 1;
		goto ERROR;
	}

	DEBUG(repo->pakfire, "Added %s to repository '%s'\n",
		nevra, pakfire_repo_get_name(repo));

ERROR:
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

PAKFIRE_EXPORT int pakfire_repo_read_solv(struct pakfire_repo* repo, FILE *f, int flags) {
	// Automatically detect compression
	f = pakfire_xfopen(f, "r");
	if (!f) {
		errno = ENOTSUP;
		return 1;
	}

	int ret = repo_add_solv(repo->repo, f, flags);
	switch (ret) {
		// Everything OK
		case 0:
			break;

		// Not SOLV format
		case 1:
		// Unsupported version
		case 2:
			errno = ENOTSUP;
			return 1;

		// End of file
		case 3:
			errno = EIO;
			return 1;

		// Corrupted
		case 4:
		case 5:
		case 6:
		default:
			errno = EBADMSG;
			return 1;
	}

	pakfire_repo_has_changed(repo);

	return 0;
}

PAKFIRE_EXPORT int pakfire_repo_write_solv(struct pakfire_repo* repo, FILE *f, int flags) {
	Repodata* meta = NULL;
	Queue addedfileprovides;
	int r;

	pakfire_pool_internalize(repo->pakfire);

	// Initialize file provides
	queue_init(&addedfileprovides);

	// Create another repodata section for meta information
	meta = repo_add_repodata(repo->repo, 0);
	if (!meta) {
		ERROR(repo->pakfire, "Could not create meta repodata: %m\n");
		r = 1;
		goto ERROR;
	}

	// Set tool version
	repodata_set_str(meta, SOLVID_META, REPOSITORY_TOOLVERSION, LIBSOLV_TOOLVERSION);

	// Do not propagate this
	repodata_unset(meta, SOLVID_META, REPOSITORY_EXTERNAL);

	// Fetch file provides from pool
	pool_addfileprovides_queue(repo->repo->pool, &addedfileprovides, 0);
	if (addedfileprovides.count)
		repodata_set_idarray(meta, SOLVID_META,
			REPOSITORY_ADDEDFILEPROVIDES, &addedfileprovides);
	else
		repodata_unset(meta, SOLVID_META, REPOSITORY_ADDEDFILEPROVIDES);

	// Free some memory
	pool_freeidhashes(repo->repo->pool);

	// Internalize the repository data
	repodata_internalize(meta);

	// Export repository data
	r = repo_write(repo->repo, f);
	if (r)
		goto ERROR;

	// Flush buffers
	r = fflush(f);
	if (r) {
		ERROR(repo->pakfire, "Could not flush after writing repository: %m\n");
		goto ERROR;
	}

ERROR:
	if (meta)
		repodata_free(meta);
	queue_free(&addedfileprovides);

	return r;
}

static int pakfire_repo_delete_all_packages(
		struct pakfire_repo* repo, const char* prefix) {
	struct pakfire_package* pkg = NULL;
	Solvable* s = NULL;
	int i = 0;
	int r;

	Pool* pool = repo->repo->pool;

	FOR_REPO_SOLVABLES(repo->repo, i, s) {
		Id id = pool_solvable2id(pool, s);

		// Allocate package
		r = pakfire_package_create_from_solvable(&pkg, repo->pakfire, id);
		if (r)
			return 1;

		const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);
		const char* path = pakfire_package_get_string(pkg, PAKFIRE_PKG_PATH);

		// If prefix is set, skip anything that doesn't match
		if (prefix && !pakfire_string_startswith(path, prefix))
			goto NEXT;

		DEBUG(repo->pakfire, "Removing %s at %s\n", nevra, path);

		// Delete the file
		r = unlink(path);
		if (r) {
			ERROR(repo->pakfire, "Could not unlink %s at %s: %m\n",
				nevra, path);
		}

NEXT:
		pakfire_package_unref(pkg);
	}

	return 0;
}

PAKFIRE_EXPORT int pakfire_repo_clean(struct pakfire_repo* repo, int flags) {
	char cache_path[PATH_MAX];
	int r;

	// Delete all temporary files from the command line repository
	if (pakfire_repo_name_equals(repo, PAKFIRE_REPO_COMMANDLINE)) {
		r = pakfire_repo_delete_all_packages(repo, PAKFIRE_TMP_DIR);
		if (r)
			return r;
	}

	// Drop all meta-data from memory
	repo_empty(repo->repo, 0);

	// End here for all "internal" repositories
	if (pakfire_repo_is_internal(repo))
		return 0;

	// Do not destroy files in local repositories
	if (pakfire_repo_is_local(repo)) {
		if (flags & PAKFIRE_REPO_CLEAN_FLAGS_DESTROY) {
			// Find path to repository
			const char* path = pakfire_repo_get_path(repo);
			if (!path)
				return 1;

			// Destroy it
			r = pakfire_rmtree(path, 0);
			if (r)
				return r;
		}

		return 0;
	}

	// Destroy all files in the cache directory
	r = pakfire_cache_path(repo->pakfire, cache_path,
		"repodata/%s", pakfire_repo_get_name(repo));
	if (r)
		return r;

	return pakfire_rmtree(cache_path, 0);
}

static int pakfire_repo_scan_file(struct pakfire_repo* repo, const char* path) {
	struct pakfire_archive* archive = NULL;
	struct pakfire_package* package = NULL;
	int r;

	DEBUG(repo->pakfire, "Scanning %s...\n", path);

	// Open archive
	r = pakfire_archive_open(&archive, repo->pakfire, path);
	if (r)
		goto ERROR;

	// Import package into the repository
	r = pakfire_archive_make_package(archive, repo, &package);

ERROR:
	if (package)
		pakfire_package_unref(package);
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

PAKFIRE_EXPORT int pakfire_repo_scan(struct pakfire_repo* repo, int flags) {
	struct pakfire_filelist* filelist = NULL;
	struct pakfire_progressbar* progressbar = NULL;
	int r;

	const char* path = pakfire_repo_get_path(repo);
	if (!path) {
		errno = EINVAL;
		return 1;
	}

	// Create a new filelist
	r = pakfire_filelist_create(&filelist, repo->pakfire);
	if (r)
		goto ERROR;

	static const char* includes[] = { "*.pfm", NULL };

	// Find all package files
	r = pakfire_filelist_scan(filelist, path, includes, NULL);
	if (r)
		goto ERROR;

	// Fetch how many files have been found
	const size_t num_files = pakfire_filelist_length(filelist);

	// Create progressbar
	r = pakfire_progressbar_create(&progressbar, NULL);
	if (r)
		goto ERROR;

	const char* name = pakfire_repo_get_name(repo);

	// Add title to progressbar
	r = pakfire_progressbar_add_string(progressbar, _("Scanning %s"), name);
	if (r)
		goto ERROR;

	// Add the bar
	r = pakfire_progressbar_add_bar(progressbar);
	if (r)
		goto ERROR;

	// Add a counter
	r = pakfire_progressbar_add_counter(progressbar);
	if (r)
		goto ERROR;

	// Start progress bar
	r = pakfire_progressbar_start(progressbar, num_files);

	for (unsigned int i = 0; i < num_files; i++) {
		struct pakfire_file* file = pakfire_filelist_get(filelist, i);

		// Increment progress bar
		pakfire_progressbar_increment(progressbar, 1);

		// Skip anything that isn't a regular file
		int type = pakfire_file_get_type(file);
		if (type != S_IFREG) {
			pakfire_file_unref(file);
			continue;
		}

		path = pakfire_file_get_abspath(file);

		// Scan the file
		r = pakfire_repo_scan_file(repo, path);
		if (r) {
			pakfire_file_unref(file);
			goto ERROR;
		}

		pakfire_file_unref(file);
	}

	// Mark repository data as changed
	pakfire_repo_has_changed(repo);

	// Finish the progress bar
	r = pakfire_progressbar_finish(progressbar);
	if (r)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (progressbar)
		pakfire_progressbar_unref(progressbar);
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

PAKFIRE_EXPORT int pakfire_repo_refresh(struct pakfire_repo* repo, int force) {
	const char* name = pakfire_repo_get_name(repo);
	int r;

	// Skip refreshing any "internal" repositories
	if (pakfire_repo_is_internal(repo))
		return 0;

	// Do nothing if this repository is not enabled
	int enabled = pakfire_repo_get_enabled(repo);
	if (!enabled) {
		DEBUG(repo->pakfire, "Skip refreshing repository '%s'\n", name);
		return 0;
	}

	// Refresh mirrorlist
	r = pakfire_repo_refresh_mirrorlist(repo, force);
	if (r) {
		ERROR(repo->pakfire, "Could not refresh mirrorlist, but will continue anyways...\n");
	}

	// Refresh metadata
	return pakfire_repo_refresh_metadata(repo, force);
}

static int pakfire_repo_write_database(struct pakfire_repo* repo, const char* path,
		char* filename, size_t length) {
	char database[PATH_MAX];
	char tmp[PATH_MAX];
	int r;

	// Create path for a temporary database export file
	r = pakfire_string_format(tmp, "%s/repodata/.pakfire-solv.XXXXXX", path);
	if (r)
		return r;

	// Create a temporary file to write to
	FILE* f = pakfire_mktemp(tmp, 0644);
	if (!f) {
		ERROR(repo->pakfire, "Could not open temporary file for writing: %m\n");
		return 1;
	}

	// Initialize the output being compressed
	f = pakfire_zstdfopen(f, "w");
	if (!f) {
		ERROR(repo->pakfire, "Could not initialize compression: %m\n");
		return 1;
	}

	// Write the SOLV database to the temporary file
	r = pakfire_repo_write_solv(repo, f, 0);
	if (r) {
		ERROR(repo->pakfire, "Could not write SOLV data: %m\n");
		goto ERROR;
	}

	// Close file
	fclose(f);
	f = NULL;

	// Create a filename for the database file
	r = __pakfire_strftime_now(filename, length, "%Y-%m-%d-%H%M.%s.solv.zst");
	if (r) {
		ERROR(repo->pakfire, "Could not format database filename: %m\n");
		goto ERROR;
	}

	// Make final database path
	r = pakfire_string_format(database, "%s/repodata/%s", path, filename);
	if (r) {
		ERROR(repo->pakfire, "Could not join database path: %m\n");
		goto ERROR;
	}

	// Link database to its destination
	r = link(tmp, database);
	if (r) {
		ERROR(repo->pakfire, "Could not link database %s: %m\n", database);
		goto ERROR;
	}

ERROR:
	if (f)
		fclose(f);

	// Remove temporary file
	unlink(tmp);

	return r;
}

static int pakfire_repo_write_metadata(struct pakfire_repo* repo) {
	struct json_object* repomd = NULL;
	FILE* f = NULL;
	int r = 1;

	// This can only be called for local repositories
	if (!pakfire_repo_is_local(repo)) {
		errno = ENOTSUP;
		return 1;
	}

	// Fetch the base path
	const char* path = pakfire_repo_get_path(repo);
	if (!path)
		return 1;

	char database[PATH_MAX];

	// Write the database
	r = pakfire_repo_write_database(repo, path, database, sizeof(database));
	if (r)
		return r;

	// Compose JSON object
	repomd = json_object_new_object();
	if (!repomd) {
		ERROR(repo->pakfire, "Could not allocate a new JSON object: %m\n");
		r = 1;
		goto ERROR;
	}

	// Set version
	struct json_object* repomd_version = json_object_new_int64(0);
	if (!repomd_version) {
		ERROR(repo->pakfire, "Could not create version: %m\n");
		r = 1;
		goto ERROR;
	}

	r = json_object_object_add(repomd, "version", repomd_version);
	if (r) {
		ERROR(repo->pakfire, "Could not add version to repomd.json: %m\n");
		goto ERROR;
	}

	time_t revision = time(NULL);

	// Set revision
	struct json_object* repomd_revision = json_object_new_int64(revision);
	if (!repomd_revision) {
		ERROR(repo->pakfire, "Could not create revision: %m\n");
		r = 1;
		goto ERROR;
	}

	r = json_object_object_add(repomd, "revision", repomd_revision);
	if (r) {
		ERROR(repo->pakfire, "Could not add revision to repomd.json: %m\n");
		goto ERROR;
	}

	// Set database
	struct json_object* repomd_database = json_object_new_string(database);
	if (!repomd_database) {
		ERROR(repo->pakfire, "Could not create database string: %m\n");
		r = 1;
		goto ERROR;
	}

	r = json_object_object_add(repomd, "database", repomd_database);
	if (r) {
		ERROR(repo->pakfire, "Could not add database to repomd.json: %m\n");
		goto ERROR;
	}

	// XXX database hash is missing

	char repomd_path[PATH_MAX];

	// Make path to repomd.json
	r = pakfire_string_format(repomd_path, "%s/repodata/repomd.json", path);
	if (r)
		goto ERROR;

	// Open repomd.json for writing
	f = fopen(repomd_path, "w");
	if (!f) {
		ERROR(repo->pakfire, "Could not open %s for writing: %m\n", repomd_path);
		r = 1;
		goto ERROR;
	}

	// Write out repomd.json
	r = json_object_to_fd(fileno(f), repomd, JSON_C_TO_STRING_PRETTY);
	if (r) {
		ERROR(repo->pakfire, "Could not write repomd.json: %m\n");
		goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (f)
		fclose(f);
	json_object_put(repomd);

	return r;
}

PAKFIRE_EXPORT int pakfire_repo_compose(struct pakfire* pakfire, const char* path,
		int flags, const char** files) {
	struct pakfire_repo* repo = NULL;
	char realpath[PATH_MAX];
	char baseurl[PATH_MAX];
	int r;

	// path cannot be NULL
	if (!path) {
		errno = EINVAL;
		return 1;
	}

	size_t num_files = 0;

	// Count files
	if (files) {
		for (const char** file = files; *file; file++)
			num_files++;
	}

	// Create the destination if it doesn't exist, yet
	pakfire_mkdir(path, 0755);

	// Make path absolute
	r = pakfire_path_realpath(realpath, path);
	if (r)
		return r;

	// Prefix path with file:// to form baseurl
	r = pakfire_string_format(baseurl, "file://%s", realpath);
	if (r)
		return 1;

	// Create a new temporary repository at path
	r = pakfire_repo_create(&repo, pakfire, "tmp");
	if (r) {
		ERROR(pakfire, "Could not create a temporary repository: %m\n");
		return r;
	}

	// Set baseurl to path
	r = pakfire_repo_set_baseurl(repo, baseurl);
	if (r) {
		ERROR(pakfire, "Could not set baseurl %s: %m\n", baseurl);
		goto ERROR;
	}

	// Find everything that is already part of the repository
	r = pakfire_repo_scan(repo, 0);
	if (r) {
		ERROR(pakfire, "Could not refresh repository: %m\n");
		goto ERROR;
	}

	struct pakfire_archive* archive = NULL;
	struct pakfire_package* package = NULL;
	char destination_path[PATH_MAX];

	DEBUG(pakfire, "Adding %zu file(s) to the repository\n", num_files);

	// Add more files
	if (files) {
		for (const char** file = files; *file; file++) {
			DEBUG(pakfire, "Adding %s to repository...\n", *file);

			// Open source archive
			r = pakfire_archive_open(&archive, pakfire, *file);
			if (r) {
				ERROR(pakfire, "Could not open %s: %m\n", *file);
				goto OUT;
			}

			// Add package metadata
			r = pakfire_repo_add_archive(repo, archive, &package);
			if (r) {
				ERROR(pakfire, "Could not add %s to the repository: %m\n", *file);
				goto OUT;
			}

			const char* filename = pakfire_package_get_string(package, PAKFIRE_PKG_FILENAME);

			// Make new path
			r = pakfire_path_join(destination_path, realpath, filename);
			if (r)
				goto OUT;

			// Copying archive to destination
			r = pakfire_archive_link_or_copy(archive, destination_path);
			if (r) {
				ERROR(pakfire, "Could not copy archive to %s: %m\n", destination_path);
				goto OUT;
			}

			// Reset the path
			pakfire_package_set_string(package, PAKFIRE_PKG_PATH, NULL);

OUT:
			if (archive) {
				pakfire_archive_unref(archive);
				archive = NULL;
			}
			if (package) {
				pakfire_package_unref(package);
				package = NULL;
			}

			if (r)
				goto ERROR;
		}
	}

	// Write metadata to disk
	r = pakfire_repo_write_metadata(repo);
	if (r)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	pakfire_repo_clear(repo);
	pakfire_repo_unref(repo);

	return r;
}

int pakfire_repo_to_packagelist(struct pakfire_repo* repo,
		struct pakfire_packagelist* list) {
	struct pakfire_package* pkg = NULL;
	Solvable* s = NULL;
	Id id;
	int i;
	int r;

	FOR_REPO_SOLVABLES(repo->repo, i, s) {
		// Convert the solvable into an ID
		id = pool_solvable2id(repo->repo->pool, s);

		// Create a new package
		r = pakfire_package_create_from_solvable(&pkg, repo->pakfire, id);
		if (r)
			return r;

		// Append the package to the list
		r = pakfire_packagelist_push(list, pkg);
		pakfire_package_unref(pkg);
		if (r)
			return r;
	}

	return 0;
}
