/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_REPO_H
#define PAKFIRE_REPO_H

#include <stdio.h>
#include <time.h>
#include <unistd.h>

struct pakfire_repo;

#include <pakfire/key.h>
#include <pakfire/pakfire.h>

int pakfire_repo_create(struct pakfire_repo** repo, struct pakfire* pakfire, const char* name);

struct pakfire_repo* pakfire_repo_ref(struct pakfire_repo* repo);
struct pakfire_repo* pakfire_repo_unref(struct pakfire_repo* repo);
struct pakfire* pakfire_repo_get_pakfire(struct pakfire_repo* repo);

int pakfire_repo_clear(struct pakfire_repo* repo);

int pakfire_repo_identical(struct pakfire_repo* repo1, struct pakfire_repo* repo2);
int pakfire_repo_cmp(struct pakfire_repo* repo1, struct pakfire_repo* repo2);
int pakfire_repo_count(struct pakfire_repo* repo);

const char* pakfire_repo_get_name(struct pakfire_repo* repo);

const char* pakfire_repo_get_description(struct pakfire_repo* repo);
int pakfire_repo_set_description(struct pakfire_repo* repo, const char* description);

int pakfire_repo_get_enabled(struct pakfire_repo* repo);
void pakfire_repo_set_enabled(struct pakfire_repo* repo, int enabled);

int pakfire_repo_get_priority(struct pakfire_repo* repo);
void pakfire_repo_set_priority(struct pakfire_repo* repo, int priority);

const char* pakfire_repo_get_baseurl(struct pakfire_repo* repo);
int pakfire_repo_set_baseurl(struct pakfire_repo* repo, const char* baseurl);

struct pakfire_key* pakfire_repo_get_key(struct pakfire_repo* repo);

const char* pakfire_repo_get_mirrorlist_url(struct pakfire_repo* repo);
int pakfire_repo_set_mirrorlist_url(struct pakfire_repo* repo, const char* url);

struct pakfire_mirrorlist* pakfire_repo_get_mirrors(struct pakfire_repo* repo);

int pakfire_repo_write_config(struct pakfire_repo* repo, FILE* f);

int pakfire_repo_is_installed_repo(struct pakfire_repo* repo);

int pakfire_repo_read_solv(struct pakfire_repo* repo, FILE *f, int flags);
int pakfire_repo_write_solv(struct pakfire_repo* repo, FILE *f, int flags);

// Cache

enum pakfire_repo_clean_flags {
	PAKFIRE_REPO_CLEAN_FLAGS_NONE     = 0,
	PAKFIRE_REPO_CLEAN_FLAGS_DESTROY  = (1 << 0),
};

int pakfire_repo_clean(struct pakfire_repo* repo, int flags);

// Scan

int pakfire_repo_scan(struct pakfire_repo* repo, int flags);

// Refresh

int pakfire_repo_refresh(struct pakfire_repo* repo, int force);

// Compose

int pakfire_repo_compose(struct pakfire* pakfire, const char* path, int flags,
	const char** files);

#ifdef PAKFIRE_PRIVATE

#include <solv/repo.h>

#include <pakfire/archive.h>
#include <pakfire/config.h>
#include <pakfire/downloader.h>
#include <pakfire/package.h>
#include <pakfire/packagelist.h>

#define PAKFIRE_REPO_COMMANDLINE		"@commandline"
#define PAKFIRE_REPO_DUMMY				"@dummy"
#define PAKFIRE_REPO_LOCAL              "local"
#define PAKFIRE_REPO_RESULT             "@build"
#define PAKFIRE_REPO_SYSTEM				"@system"

int pakfire_repo_name_equals(struct pakfire_repo* repo, const char* name);
int pakfire_repo_is_internal(struct pakfire_repo* repo);
int pakfire_repo_is_local(struct pakfire_repo* repo);

int pakfire_repo_import(struct pakfire* pakfire, struct pakfire_config* config);
const char* pakfire_repo_get_path(struct pakfire_repo* repo);

void pakfire_repo_has_changed(struct pakfire_repo* repo);
int pakfire_repo_internalize(struct pakfire_repo* repo, int flags);
Id pakfire_repo_add_solvable(struct pakfire_repo* repo);
int pakfire_repo_add_archive(struct pakfire_repo* repo,
	struct pakfire_archive* archive, struct pakfire_package** package);

int pakfire_repo_add(struct pakfire_repo* repo, const char* path,
	struct pakfire_package** package);

struct pakfire_repo* pakfire_repo_create_from_repo(struct pakfire* pakfire, Repo* r);
void pakfire_repo_free_all(struct pakfire* pakfire);

Repo* pakfire_repo_get_repo(struct pakfire_repo* repo);
Repodata* pakfire_repo_get_repodata(struct pakfire_repo* repo);

struct pakfire_mirrorlist* pakfire_repo_get_mirrorlist(struct pakfire_repo* repo);

int pakfire_repo_to_packagelist(struct pakfire_repo* repo,
	struct pakfire_packagelist* list);

#endif

#endif /* PAKFIRE_REPO_H */
