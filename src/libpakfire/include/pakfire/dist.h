/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_DIST_H
#define PAKFIRE_DIST_H

#include <pakfire/pakfire.h>

int pakfire_dist(struct pakfire* pakfire, const char* path, const char* target,
	char** result);

#ifdef PAKFIRE_PRIVATE

#include <pakfire/parser.h>

int pakfire_read_makefile(struct pakfire_parser** parser, struct pakfire* pakfire,
	const char* path, struct pakfire_parser_error** error);

#endif

#endif /* PAKFIRE_DIST_H */
