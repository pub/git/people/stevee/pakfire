/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <linux/limits.h>
#include <mntent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

static const char* characters =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz"
	"0123456789";

static int random_string(char* s, size_t length) {
	// Fill the buffer with random characters
	for (unsigned int i = 0; i < length - 1; i++) {
		// Select a random character
		int c = rand() % (strlen(characters) - 1);

		s[i] = characters[c];
	}

	// Terminate the buffer
	s[length - 1] = '\0';

	return 0;
}

static int check_mountpoint(int argc, char* argv[]) {
	if (argc < 1) {
		fprintf(stderr, "check-mountpoint requires an argument\n");
		return EXIT_FAILURE;
	}

	const char* mp = argv[0];
	int r = 1;

	FILE* f = fopen("/proc/self/mounts", "r");
	if (!f) {
		fprintf(stderr, "Could not open /proc/self/mounts: %m\n");
		return 1;
	}

	// Walk through all mountpoints
	for (;;) {
		struct mntent* entry = getmntent(f);
		if (!entry)
			break;

		// Check for match
		if (strcmp(entry->mnt_dir, mp) == 0) {
			r = 0;
			break;
		}
	}

	fclose(f);

	// Print something useful
	if (r)
		printf("%s is not a mountpoint\n", mp);
	else
		printf("%s is a mointpoint\n", mp);

	return r;
}

static int check_open_file_descriptors(int argc, char* argv[]) {
	struct rlimit limits;
	int r;

	// Determine the maximum amount of files that could be open
	r = getrlimit(RLIMIT_NOFILE, &limits);
	if (r) {
		fprintf(stderr, "getrlimit() failed: %m\n");
		return EXIT_FAILURE;
	}

	// Count any open file descriptors
	unsigned int open_fds = 0;

	for (unsigned int fd = 3; fd < limits.rlim_max; fd++) {
		r = close(fd);
		if (r < 0)
			continue;

		fprintf(stderr, "FD %u is open\n", fd);
		open_fds++;
	}

	// Return failure if we have found any open file descriptors
	return (open_fds > 0) ? EXIT_FAILURE : EXIT_SUCCESS;
}

static int echo(int argc, char* argv[]) {
	for (int i = 0; i < argc; i++) {
		printf("%s\n", argv[i]);
	}

	return 0;
}

static int echo_environ(int argc, char* argv[]) {
	for (int i = 0; i < argc; i++) {
		const char* value = secure_getenv(argv[i]);
		if (!value) {
			fprintf(stderr, "Variable %s is not set\n", argv[i]);
			return 1;
		}

		printf("%s\n", value);
	}

	return 0;
}

static int exhaust_memory(int argc, char* argv[]) {
	const size_t block = 1048576; // 1 MiB
	size_t bytes = 0;

	while (1) {
		char* p = malloc(block);
		if (!p)
			break;

		bytes += block;

		printf("Allocated %zu MiB\n", bytes / block);
	}

	printf("Successfully allocated %f MiB\n", (double)bytes / 1048576);

	return 0;
}

static int exit_with_code(int argc, char* argv[]) {
	if (argc < 1) {
		fprintf(stderr, "exit-with-code requires an argument\n");
		return EXIT_FAILURE;
	}

	// Return the given code
	return strtoul(argv[0], NULL, 10);
}

static int fork_bomb(int argc, char* argv[]) {
	int status = 0;

	// Fork this process
	pid_t pid = fork();

	// Catch any errors
	if (pid < 0) {
		fprintf(stderr, "Could not fork(): %m\n");

	// Parent
	} else if (pid) {
		printf("Forked child process with PID %d\n", pid);

		// Wait until the child process went away
		waitpid(pid, &status, 0);

		if (WIFSIGNALED(status)) {
			fprintf(stderr, "Child %d was terminated by signal %d\n",
				pid, WTERMSIG(status));

			return 128 + WTERMSIG(status);
		}

		// Pass on the exit code
		if (WIFEXITED(status))
			return WEXITSTATUS(status);

	// Child
	} else {
		// Fork again...
		return fork_bomb(0, NULL);
	}

	return EXIT_FAILURE;
}

static int lines(int argc, char* argv[]) {
	size_t length = 0;

	if (argc < 1) {
		fprintf(stderr, "lines requires a number of lines to print\n");
		return EXIT_FAILURE;
	}

	// How many lines to print?
	const unsigned int lines = strtoul(argv[0], NULL, 10);

	// Optionally take the length of the line
	if (argc > 1)
		length = strtoul(argv[1], NULL, 10);

	// Set a default length if none set
	if (!length)
		length = 40;

	// Add space for NULL
	length++;

	char* buffer = alloca(length);

	for (unsigned int line = 0; line < lines; line++) {
		// Generate a random line
		int r = random_string(buffer, length);
		if (r)
			return r;

		// Print the line
		printf("%s\n", buffer);
	}

	return 0;
}

static int print_nice(int argc, char* argv[]) {
	int nice = getpriority(PRIO_PROCESS, 0);

	// Print the nice level
	printf("%d\n", nice);

	return 0;
}

static int print_pid(int argc, char* argv[]) {
	pid_t pid = getpid();
	if (!pid)
		return 1;

	// Print the PID
	printf("%d\n", pid);

	return 0;
}

static int send_signal(int argc, char* argv[]) {
	int r;

	if (argc < 1) {
		fprintf(stderr, "No signal given\n");
		return EXIT_FAILURE;
	}

	// Fetch our own PID
	pid_t pid = getpid();

	// Parse signal from command line
	int signum = strtoul(argv[0], NULL, 10);

	printf("Sending signal %d to ourselves (PID %d)\n", signum, pid);

	r = kill(pid, signum);
	if (r < 0) {
		fprintf(stderr, "Error sending signal: %m\n");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

static int _sleep(int argc, char* argv[]) {
	if (argc < 1) {
		fprintf(stderr, "No timeout given\n");
		return EXIT_FAILURE;
	}

	// Fetch timeout
	unsigned int timeout = strtoul(argv[0], NULL, 10);

	printf("Sleeping for %u second(s)\n", timeout);

	// Sleep for given timeout
	sleep(timeout);

	return EXIT_SUCCESS;
}

static int stat_self(struct stat* st) {
	char path[PATH_MAX];
	int r;

	// Find path to ourselves
	r = readlink("/proc/self/exe", path, sizeof(path));
	if (r < 0) {
		fprintf(stderr, "readlink() failed: %m\n");
		return 1;
	}

	// stat()
	r = stat(path, st);
	if (r < 0) {
		fprintf(stderr, "stat() on %s failed: %m\n", path);
		return 1;
	}

	return 0;
}

/*
	Returns who owns this binary
*/
static int stat_ownership(int argc, char* argv[]) {
	struct stat st;
	int r;

	// Stat ourselves
	r = stat_self(&st);
	if (r)
		return r;

	// Print result
	printf("uid=%d gid=%d\n", st.st_uid, st.st_gid);

	return 0;
}

/*
	Reads from stdin and writes it back to stdout
*/
static int _pipe(int argc, char* argv[]) {
	int r;

	for (;;) {
		// Read one character from stdin
		int c = fgetc(stdin);

		// Break if we are done reading
		if (c == EOF)
			break;

		// Write the character to stdout
		r = fputc(c, stdout);
		if (r == EOF) {
			fprintf(stderr, "Could not write to stdout: %m\n");
			break;
		}

		r = 0;
	}

	return r;
}

/*
	Causes a segmentation fault so this program is being terminated
*/
static int _segv(int argc, char* argv[]) {
	char* p = NULL;

	// Storing a value to the NULL pointer
	*p = 'a';

	return 0;
}

int main(int argc, char* argv[]) {
	if (argc < 2) {
		fprintf(stderr, "No command given\n");
		return EXIT_FAILURE;
	}

	int (*callback)(int argc, char* argv[]) = NULL;
	const char* command = argv[1];

	if (strcmp(command, "check-mountpoint") == 0)
		callback = check_mountpoint;

	else if (strcmp(command, "check-open-file-descriptors") == 0)
		callback = check_open_file_descriptors;

	// Echo
	else if (strcmp(command, "echo") == 0)
		callback = echo;

	// Echo Environment
	else if (strcmp(command, "echo-environ") == 0)
		callback = echo_environ;

	// Exhaust memory
	else if (strcmp(command, "exhaust-memory") == 0)
		callback = exhaust_memory;

	// Exit with code
	else if (strcmp(command, "exit-with-code") == 0)
		callback = exit_with_code;

	// Fork
	else if (strcmp(command, "fork-bomb") == 0)
		callback = fork_bomb;

	// Print random lines
	else if (strcmp(command, "lines") == 0)
		callback = lines;

	// Pipe
	else if (strcmp(command, "pipe") == 0)
		callback = _pipe;

	// Print nice level
	else if (strcmp(command, "print-nice") == 0)
		callback = print_nice;

	// Print PID
	else if (strcmp(command, "print-pid") == 0)
		callback = print_pid;

	// SEGV
	else if (strcmp(command, "segv") == 0)
		callback = _segv;

	// Send signal
	else if (strcmp(command, "send-signal") == 0)
		callback = send_signal;

	// Sleep
	else if (strcmp(command, "sleep") == 0)
		callback = _sleep;

	// Stat ownership
	else if (strcmp(command, "stat-ownership") == 0)
		callback = stat_ownership;

	// Exit if no callback has been set
	if (!callback) {
		fprintf(stderr, "Unknown command: %s\n", command);
		return EXIT_FAILURE;
	}

	return callback(argc - 2, argv + 2);
}
