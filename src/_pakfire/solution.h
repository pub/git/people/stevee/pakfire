/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PYTHON_PAKFIRE_SOLUTION_H
#define PYTHON_PAKFIRE_SOLUTION_H

#include <Python.h>

#include <pakfire/solution.h>

typedef struct {
	PyObject_HEAD
	struct pakfire_solution* solution;
} SolutionObject;

extern PyTypeObject SolutionType;

PyObject* new_solution(struct pakfire_solution* solution);

#endif /* PYTHON_PAKFIRE_SOLUTION_H */
