/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdlib.h>
#include <sys/queue.h>

#include <solv/queue.h>

#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/packagelist.h>
#include <pakfire/pakfire.h>
#include <pakfire/private.h>

struct pakfire_packagelist_element {
	TAILQ_ENTRY(pakfire_packagelist_element) nodes;

	struct pakfire_package* pkg;
};

struct pakfire_packagelist {
	struct pakfire* pakfire;
	int nrefs;

	TAILQ_HEAD(entries, pakfire_packagelist_element) packages;
};

static void pakfire_packagelist_clear(struct pakfire_packagelist* list) {
	struct pakfire_packagelist_element* element = NULL;

	while (!TAILQ_EMPTY(&list->packages)) {
		// Fetch the first element
		element = TAILQ_FIRST(&list->packages);

		// Remove it from the list
		TAILQ_REMOVE(&list->packages, element, nodes);

		// Dereference the file
		pakfire_package_unref(element->pkg);

		// Free it all
		free(element);
	}
}

static void pakfire_packagelist_free(struct pakfire_packagelist* list) {
	pakfire_packagelist_clear(list);
	pakfire_unref(list->pakfire);
	free(list);
}

PAKFIRE_EXPORT int pakfire_packagelist_create(
		struct pakfire_packagelist** list, struct pakfire* pakfire) {
	struct pakfire_packagelist* l = calloc(1, sizeof(*l));
	if (!l)
		return 1;

	// Store a reference to Pakfire
	l->pakfire = pakfire_ref(pakfire);

	// Initialize the reference counter
	l->nrefs = 1;

	// Initialise packages
	TAILQ_INIT(&l->packages);

	*list = l;
	return 0;
}

PAKFIRE_EXPORT struct pakfire_packagelist* pakfire_packagelist_ref(struct pakfire_packagelist* list) {
	list->nrefs++;

	return list;
}

PAKFIRE_EXPORT struct pakfire_packagelist* pakfire_packagelist_unref(struct pakfire_packagelist* list) {
	if (--list->nrefs > 0)
		return list;

	pakfire_packagelist_free(list);
	return NULL;
}

PAKFIRE_EXPORT size_t pakfire_packagelist_length(struct pakfire_packagelist* list) {
	struct pakfire_packagelist_element* element = NULL;
	size_t size = 0;

	TAILQ_FOREACH(element, &list->packages, nodes)
		size++;

	return size;
}

PAKFIRE_EXPORT struct pakfire_package* pakfire_packagelist_get(struct pakfire_packagelist* list, unsigned int index) {
	struct pakfire_packagelist_element* element = NULL;

	// Fetch the first element
	element = TAILQ_FIRST(&list->packages);

	while (element && index--)
		element = TAILQ_NEXT(element, nodes);

	return pakfire_package_ref(element->pkg);
}

PAKFIRE_EXPORT int pakfire_packagelist_push(struct pakfire_packagelist* list, struct pakfire_package* pkg) {
	struct pakfire_packagelist_element* element = NULL;
	struct pakfire_packagelist_element* e = NULL;

	// Allocate a new element
	element = calloc(1, sizeof *element);
	if (!element) {
		ERROR(list->pakfire, "Could not allocate a new packagelist element: %m\n");
		return 1;
	}

	// Reference the package
	element->pkg = pakfire_package_ref(pkg);

	// Fetch the last element
	e = TAILQ_LAST(&list->packages, entries);

	// Skip all elements that are "greater than" the element we want to add
	while (e) {
		if (pakfire_package_cmp(e->pkg, pkg) <= 0)
			break;

		e = TAILQ_PREV(e, entries, nodes);
	}

	// If we found an element on the list, we will append after it,
	// otherwise we add right at the start.
	if (e)
		TAILQ_INSERT_AFTER(&list->packages, e, element, nodes);
	else
		TAILQ_INSERT_HEAD(&list->packages, element, nodes);

	return 0;
}

int pakfire_packagelist_walk(struct pakfire_packagelist* list,
		pakfire_packagelist_walk_callback callback, void* data) {
	struct pakfire_packagelist_element* element = NULL;
	int r = 0;

	// Call the callback once for every element on the list
	TAILQ_FOREACH(element, &list->packages, nodes) {
		// Call the callback
		r = callback(list->pakfire, element->pkg, data);
		if (r)
			break;
	}

	return r;
}

int pakfire_packagelist_import_solvables(struct pakfire_packagelist* list, Queue* q) {
	struct pakfire_package* pkg = NULL;
	int r;

	// Walk through all elements on the queue
	for (int i = 0; i < q->count; i++) {
		r = pakfire_package_create_from_solvable(&pkg, list->pakfire, q->elements[i]);
		if (r)
			return r;

		r = pakfire_packagelist_push(list, pkg);
		pakfire_package_unref(pkg);
		if (r)
			return r;
	}

	return 0;
}
