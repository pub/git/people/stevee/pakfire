/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PWD_H
#define PAKFIRE_PWD_H

#ifdef PAKFIRE_PRIVATE

#include <grp.h>
#include <pwd.h>

struct pakfire_subid {
	char name[NAME_MAX];
	unsigned int id;
	size_t length;
};

struct passwd* pakfire_getpwnam(struct pakfire* pakfire, const char* name);
struct passwd* pakfire_getpwuid(struct pakfire* pakfire, uid_t uid);

struct group* pakfire_getgrnam(struct pakfire* pakfire, const char* name);
struct group* pakfire_getgrgid(struct pakfire* pakfire, gid_t gid);

int pakfire_getsubid(struct pakfire* pakfire, const char* path,
	const uid_t uid, struct pakfire_subid* subid);

#endif

#endif /* PAKFIRE_PWD_H */
