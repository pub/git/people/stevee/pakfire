/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <Python.h>

#include "archive_file.h"

PyObject* new_archive_file(PyTypeObject* type, FILE* f) {
	ArchiveFileObject* self = (ArchiveFileObject *)type->tp_alloc(type, 0);
	if (self) {
		self->f = f;
	}

	return (PyObject*)self;
}

static void ArchiveFile_dealloc(ArchiveFileObject* self) {
	if (self->f)
		fclose(self->f);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static PyObject* ArchiveFile_close(ArchiveFileObject* self) {
	int r;

	if (self->f) {
		// Close the underlying file descriptor
		r = fclose(self->f);
		self->f = NULL;

		// Raise any errors
		if (r) {
			PyErr_SetFromErrno(PyExc_OSError);
			return NULL;
		}
	}

	Py_RETURN_NONE;
}

static PyObject* ArchiveFile_readblock(ArchiveFileObject* self, ssize_t size) {
	PyObject* result = NULL;
	char* buffer = NULL;
	ssize_t bytes_read = 0;

	Py_BEGIN_ALLOW_THREADS;

	// Allocate a buffer that is large enough to hold the result
	buffer = malloc(size);
	if (!buffer) {
		Py_BLOCK_THREADS;
		goto ERROR;
	}

	// Read data into the buffer
	bytes_read = fread(buffer, 1, size, self->f);
	if (bytes_read < 0) {
		Py_BLOCK_THREADS;
		goto ERROR;
	}

	Py_END_ALLOW_THREADS;

	// Copy buffer to Python
	result = PyBytes_FromStringAndSize(buffer, bytes_read);

ERROR:
	if (buffer)
		free(buffer);

	if (result)
		return result;

	PyErr_SetFromErrno(PyExc_OSError);
	return NULL;
}

static PyObject* ArchiveFile_readall(ArchiveFileObject* self) {
	PyObject* result = NULL;
	const size_t block_length = 8192;
	char* buffer = NULL;
	size_t buffer_length = 0;
	size_t length = 0;

	// Cannot run this after f has been closed
	if (!self->f) {
		PyErr_SetString(PyExc_ValueError, "I/O operation on closed file");
		return NULL;
	}

	Py_BEGIN_ALLOW_THREADS

	for (;;) {
		// Increase the buffer by one block
		buffer_length += block_length;

		// Allocate the space
		buffer = realloc(buffer, buffer_length);
		if (!buffer) {
			Py_BLOCK_THREADS;
			PyErr_SetFromErrno(PyExc_OSError);
			goto ERROR;
		}

		// Determine the beginning of the new block
		char* b = buffer + length;

		// Try reading another block
		ssize_t bytes_read = fread(b, 1, block_length, self->f);
		if (bytes_read < 0) {
			Py_BLOCK_THREADS;
			PyErr_SetFromErrno(PyExc_OSError);
			goto ERROR;

		// EOF
		} else if (bytes_read == 0)
			break;

		// Increment length
		length += bytes_read;
	}

	Py_END_ALLOW_THREADS

	// Copy buffer to Python
	result = PyBytes_FromStringAndSize(buffer, length);

ERROR:
	if (buffer)
		free(buffer);

	return result;
}

static PyObject* ArchiveFile_read(ArchiveFileObject* self, PyObject* args) {
	PyObject* result = NULL;
	ssize_t size = -1;

	if (!PyArg_ParseTuple(args, "|n", &size))
		return NULL;

	// Cannot run this after f has been closed
	if (!self->f) {
		PyErr_SetString(PyExc_ValueError, "I/O operation on closed file");
		return NULL;
	}

	if (size > 0)
		result = ArchiveFile_readblock(self, size);
	else
		result = ArchiveFile_readall(self);

	return result;
}

static struct PyMethodDef Archive_methods[] = {
	{
		"close",
		(PyCFunction)ArchiveFile_close,
		METH_NOARGS,
		NULL,
	},
	{
		"read",
		(PyCFunction)ArchiveFile_read,
		METH_VARARGS,
		NULL,
	},
	{
		"readall",
		(PyCFunction)ArchiveFile_readall,
		METH_NOARGS,
		NULL,
	},
	{ NULL },
};

PyTypeObject ArchiveFileType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	tp_name:            "_pakfire.ArchiveFile",
	tp_basicsize:       sizeof(ArchiveFileObject),
	tp_flags:           Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	tp_dealloc:         (destructor)ArchiveFile_dealloc,
	tp_doc:             "Archive File object",
	tp_methods:         Archive_methods,
};
