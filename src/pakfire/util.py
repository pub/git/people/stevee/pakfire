#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2011 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import platform

def get_distro_name():
	"""
		Returns the distro name of the host
	"""
	try:
		release = platform.freedesktop_os_release()
	except AttributeError:
		release = _read_os_release()

	return release.get("PRETTY_NAME") or release.get("NAME")

def _read_os_release():
	"""
		This is a simple implementation to read /etc/os-release and /var/lib/os-release
	"""
	for file in ("/etc/os-release", "/var/lib/os-release"):
		try:
			with open(file, "r") as f:
				release = {}

				for line in f:
					key, delim, value = line.partition("=")

					release[key] = value

				return release
		except FileNotFoundError:
			pass

		raise OSError("Could not find os-release")
