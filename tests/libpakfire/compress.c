/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <linux/limits.h>
#include <stdio.h>
#include <string.h>

#include <pakfire/compress.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#include "../testsuite.h"

const char TEST_DATA[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ\n";

static int read_test(const struct test* t,
		FILE* (function)(FILE* f, const char* mode), const char* file) {
	int r = EXIT_FAILURE;

	FILE* f = NULL;
	char path[PATH_MAX];
	char buffer[1024];

	ASSERT_SUCCESS(pakfire_string_format(path, "%s/%s", TEST_SRC_PATH, file));

	// Open file
	ASSERT(f = fopen(path, "r"));

	// Engage decompressor
	f = function(f, "r");
	ASSERT(f);

	// Read into buffer
	size_t bytes_read = fread(buffer, 1, sizeof(buffer), f);
	ASSERT(bytes_read);

	// Buffer should equal the test data
	ASSERT(bytes_read >= sizeof(TEST_DATA) - 1);
	ASSERT_SUCCESS(memcmp(buffer, TEST_DATA, sizeof(TEST_DATA) - 1));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (f)
		fclose(f);

	return r;
}

static int write_test(const struct test* t, FILE* (function)(FILE* f, const char* mode)) {
	int r = EXIT_FAILURE;

	FILE* f = NULL;

	// Create a backend storage file
	ASSERT(f = test_mktemp(NULL));

	// Open compressed file for writing
	f = function(f, "w");
	ASSERT(f);

	// Write some data and close fwrite
	for (unsigned int i = 0; i < 1000; i++) {
		size_t bytes_written = fwrite(TEST_DATA, 1, sizeof(TEST_DATA) - 1, f);
		ASSERT(bytes_written == sizeof(TEST_DATA) - 1);
	}

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (f)
		fclose(f);

	return r;
}

static int test_xzfopen_read(const struct test* t) {
	return read_test(t, pakfire_xzfopen, "data/compress/data.xz");
}

static int test_xzfopen_write(const struct test* t) {
	return write_test(t, pakfire_xzfopen);
}

static int test_zstdfopen_read(const struct test* t) {
	return read_test(t, pakfire_zstdfopen, "data/compress/data.zst");
}

static int test_zstdfopen_write(const struct test* t) {
	return write_test(t, pakfire_zstdfopen);
}

static int test_xfopen(const struct test* t) {
	ASSERT_SUCCESS(read_test(t, pakfire_xfopen, "data/compress/data.xz"));
	ASSERT_SUCCESS(read_test(t, pakfire_xfopen, "data/compress/data.zst"));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

int main(int argc, const char* argv[]) {
	// XZ
	testsuite_add_test(test_xzfopen_read);
	testsuite_add_test(test_xzfopen_write);

	// ZSTD
	testsuite_add_test(test_zstdfopen_read);
	testsuite_add_test(test_zstdfopen_write);

	testsuite_add_test(test_xfopen);

	return testsuite_run(argc, argv);
}
