/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdio.h>

#include <openssl/crypto.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/sha.h>

#include <pakfire/digest.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/private.h>
#include <pakfire/util.h>

static const struct _pakfire_digest_name {
	const char* name;
	const enum pakfire_digest_types type;
} PAKFIRE_DIGEST_NAMES[] = {
	// SHA-2
	{ "sha2-512", PAKFIRE_DIGEST_SHA2_512, },
	{ "sha2-256", PAKFIRE_DIGEST_SHA2_256, },

	// BLAKE2
	{ "blake2b512", PAKFIRE_DIGEST_BLAKE2B512, },
	{ "blake2s256", PAKFIRE_DIGEST_BLAKE2S256, },

	// SHA-3
	{ "sha3-512", PAKFIRE_DIGEST_SHA3_512, },
	{ "sha3-256", PAKFIRE_DIGEST_SHA3_256, },

	{ NULL, PAKFIRE_DIGEST_UNDEFINED, },
};

PAKFIRE_EXPORT const char* pakfire_digest_name(const enum pakfire_digest_types type) {
	for (const struct _pakfire_digest_name* n = PAKFIRE_DIGEST_NAMES; n->name; n++) {
		if (n->type == type)
			return n->name;
	}

	return NULL;
}

PAKFIRE_EXPORT int pakfire_digest_get_by_name(const char* name) {
	// Check that name is not NULL
	if (!name) {
		errno = EINVAL;
		return PAKFIRE_DIGEST_UNDEFINED;
	}

	for (const struct _pakfire_digest_name* n = PAKFIRE_DIGEST_NAMES; n->name; n++) {
		if (strcmp(n->name, name) == 0)
			return n->type;
	}

	return PAKFIRE_DIGEST_UNDEFINED;
}

size_t pakfire_digest_length(const enum pakfire_digest_types digest) {
	switch (digest) {
		case PAKFIRE_DIGEST_SHA3_512:
			return SHA512_DIGEST_LENGTH;

		case PAKFIRE_DIGEST_SHA3_256:
			return SHA256_DIGEST_LENGTH;

		case PAKFIRE_DIGEST_BLAKE2B512:
			return BLAKE2B512_DIGEST_LENGTH;

		case PAKFIRE_DIGEST_BLAKE2S256:
			return BLAKE2S256_DIGEST_LENGTH;

		case PAKFIRE_DIGEST_SHA2_512:
			return SHA512_DIGEST_LENGTH;

		case PAKFIRE_DIGEST_SHA2_256:
			return SHA256_DIGEST_LENGTH;

		case PAKFIRE_DIGEST_UNDEFINED:
			return 0;
	}

	return 0;
}

const unsigned char* pakfire_digest_get(struct pakfire_digests* digests,
		const enum pakfire_digest_types type, size_t* length) {
	// Set length
	if (length)
		*length = pakfire_digest_length(type);

	// Return a pointer to the digest (if set)
	switch (type) {
		case PAKFIRE_DIGEST_SHA3_512:
			if (pakfire_digest_set(digests->sha3_512))
				return digests->sha3_512;

		case PAKFIRE_DIGEST_SHA3_256:
			if (pakfire_digest_set(digests->sha3_256))
				return digests->sha3_256;

		case PAKFIRE_DIGEST_BLAKE2B512:
			if (pakfire_digest_set(digests->blake2b512))
				return digests->blake2b512;

		case PAKFIRE_DIGEST_BLAKE2S256:
			if (pakfire_digest_set(digests->blake2s256))
				return digests->blake2s256;

		case PAKFIRE_DIGEST_SHA2_512:
			if (pakfire_digest_set(digests->sha2_512))
				return digests->sha2_512;

		case PAKFIRE_DIGEST_SHA2_256:
			if (pakfire_digest_set(digests->sha2_256))
				return digests->sha2_256;

		case PAKFIRE_DIGEST_UNDEFINED:
			break;
	}

	return NULL;
}

/*
	Returns one if the digest is not all zeros.
*/
int __pakfire_digest_set(const unsigned char* digest, const size_t length) {
	for (unsigned int i = 0; i < length; i++) {
		if (digest[i])
			return 1;
	}

	return 0;
}

/*
	Returns a bitmap of all digests that are set.
*/
int pakfire_digest_has_any(const struct pakfire_digests* digests) {
	int types = PAKFIRE_DIGEST_UNDEFINED;

	if (pakfire_digest_set(digests->sha3_512))
		types |= PAKFIRE_DIGEST_SHA3_512;

	if (pakfire_digest_set(digests->sha3_256))
		types |= PAKFIRE_DIGEST_SHA3_256;

	if (pakfire_digest_set(digests->blake2b512))
		types |= PAKFIRE_DIGEST_BLAKE2B512;

	if (pakfire_digest_set(digests->blake2s256))
		types |= PAKFIRE_DIGEST_BLAKE2S256;

	if (pakfire_digest_set(digests->sha2_512))
		types |= PAKFIRE_DIGEST_SHA2_512;

	if (pakfire_digest_set(digests->sha2_256))
		types |= PAKFIRE_DIGEST_SHA2_256;

	return types;
}

/*
	Returns a bitmap of all digests that are not set, yet
*/
static int pakfire_digest_needed(const struct pakfire_digests* digests, const int needed) {
	// Fetch any currently set digests
	const int have = pakfire_digest_has_any(digests);

	return (~have & needed);
}

void pakfire_digests_reset(struct pakfire_digests* digests, int types) {
	if (!types)
		types = ~types;

	// Reset SHA-3-512
	if (types & PAKFIRE_DIGEST_SHA3_512)
		memset(digests->sha3_512, 0, sizeof(digests->sha3_512));

	// Reset SHA-3-256
	if (types & PAKFIRE_DIGEST_SHA3_256)
		memset(digests->sha3_256, 0, sizeof(digests->sha3_256));

	// Reset BLAKE2b512
	if (types & PAKFIRE_DIGEST_BLAKE2B512)
		memset(digests->blake2b512, 0, sizeof(digests->blake2b512));

	// Reset BLAKE2s256
	if (types & PAKFIRE_DIGEST_BLAKE2S256)
		memset(digests->blake2s256, 0, sizeof(digests->blake2s256));

	// Reset SHA-2-512
	if (types & PAKFIRE_DIGEST_SHA2_512)
		memset(digests->sha2_512, 0, sizeof(digests->sha2_512));

	// Reset SHA-2-256
	if (types & PAKFIRE_DIGEST_SHA2_256)
		memset(digests->sha2_256, 0, sizeof(digests->sha2_256));
}

static int pakfire_digests_check_length(struct pakfire* pakfire,
		const enum pakfire_digest_types type, const size_t length) {
	const size_t l = pakfire_digest_length(type);

	// Return if length matches
	if (length == l)
		return 0;

	// Otherwise set an error
	ERROR(pakfire, "Digest is of an unexpected length\n");
	errno = ENOMSG;

	return 1;
}

static EVP_MD_CTX* __pakfire_digest_setup(struct pakfire* pakfire, const EVP_MD* md) {
	EVP_MD_CTX* ctx = NULL;
	int r;

	// Setup a new context
	ctx = EVP_MD_CTX_new();
	if (!ctx) {
		ERROR(pakfire, "Could not initialize OpenSSL context: %s\n",
			ERR_error_string(ERR_get_error(), NULL));
		r = 1;
		goto ERROR;
	}

	// Setup digest
	r = EVP_DigestInit_ex(ctx, md, NULL);
	if (r != 1) {
		ERROR(pakfire, "Could not setup digest: %s\n",
			ERR_error_string(ERR_get_error(), NULL));
		r = 1;
		goto ERROR;
	}

	return ctx;

ERROR:
	if (ctx)
		EVP_MD_CTX_free(ctx);

	return NULL;
}

static int __pakfire_digest_update(struct pakfire* pakfire, EVP_MD_CTX* ctx,
		const char* buffer, const size_t length) {
	int r;

	// Nothing to do if digest not initialized
	if (!ctx)
		return 0;

	// Update digest
	r = EVP_DigestUpdate(ctx, buffer, length);
	if (r != 1) {
		ERROR(pakfire, "EVP_Digest_Update() failed: %s\n",
			ERR_error_string(ERR_get_error(), NULL));
		return 1;
	}

	return 0;
}

static int __pakfire_digest_finalize(struct pakfire* pakfire,
		EVP_MD_CTX* ctx, unsigned char* digest) {
	int r;

	// Nothing to do if digest not initialized
	if (!ctx)
		return 0;

	// Finalize digest
	r = EVP_DigestFinal_ex(ctx, digest, NULL);
	if (r != 1) {
		ERROR(pakfire, "EVP_DigestFinal_ex() failed: %s\n",
			ERR_error_string(ERR_get_error(), NULL));
		return 1;
	}

	return 0;
}

int pakfire_digests_compute_from_file(struct pakfire* pakfire,
		struct pakfire_digests* digests, int types, FILE* f) {
	EVP_MD_CTX* sha3_512_ctx = NULL;
	EVP_MD_CTX* sha3_256_ctx = NULL;
	EVP_MD_CTX* blake2b512_ctx = NULL;
	EVP_MD_CTX* blake2s256_ctx = NULL;
	EVP_MD_CTX* sha2_512_ctx = NULL;
	EVP_MD_CTX* sha2_256_ctx = NULL;
	char buffer[PAKFIRE_BUFFER_SIZE];
	int r = 1;

	// Check if any digests have been computed before and select only those that we need
	types = pakfire_digest_needed(digests, types);

	// Nothing to do?
	if (!types)
		return 0;

	// Initialize context for SHA-3-512
	if (types & PAKFIRE_DIGEST_SHA3_512) {
		sha3_512_ctx = __pakfire_digest_setup(pakfire, EVP_sha3_512());
		if (!sha3_512_ctx)
			goto ERROR;
	}

	// Initialize context for SHA-3-256
	if (types & PAKFIRE_DIGEST_SHA3_256) {
		sha3_256_ctx = __pakfire_digest_setup(pakfire, EVP_sha3_256());
		if (!sha3_256_ctx)
			goto ERROR;
	}

	// Initialize context for BLAKE2B512
	if (types & PAKFIRE_DIGEST_BLAKE2B512) {
		blake2b512_ctx = __pakfire_digest_setup(pakfire, EVP_blake2b512());
		if (!blake2b512_ctx)
			goto ERROR;
	}

	// Initialize context for BLAKE2S256
	if (types & PAKFIRE_DIGEST_BLAKE2S256) {
		blake2s256_ctx = __pakfire_digest_setup(pakfire, EVP_blake2s256());
		if (!blake2s256_ctx)
			goto ERROR;
	}

	// Initialize context for SHA-2-512
	if (types & PAKFIRE_DIGEST_SHA2_512) {
		sha2_512_ctx = __pakfire_digest_setup(pakfire, EVP_sha512());
		if (!sha2_512_ctx)
			goto ERROR;
	}

	// Initialize context for SHA-2-256
	if (types & PAKFIRE_DIGEST_SHA2_256) {
		sha2_256_ctx = __pakfire_digest_setup(pakfire, EVP_sha256());
		if (!sha2_256_ctx)
			goto ERROR;
	}

	// Read the file into the hash functions
	while (!feof(f)) {
		size_t bytes_read = fread(buffer, 1, sizeof(buffer), f);

		// Raise any reading errors
		if (ferror(f)) {
			r = 1;
			goto ERROR;
		}

		// SHA-3-512
		r = __pakfire_digest_update(pakfire, sha3_512_ctx, buffer, bytes_read);
		if (r)
			goto ERROR;

		// SHA-3-256
		r = __pakfire_digest_update(pakfire, sha3_256_ctx, buffer, bytes_read);
		if (r)
			goto ERROR;

		// BLAKE2B512
		r = __pakfire_digest_update(pakfire, blake2b512_ctx, buffer, bytes_read);
		if (r)
			goto ERROR;

		// BLAKE2S256
		r = __pakfire_digest_update(pakfire, blake2s256_ctx, buffer, bytes_read);
		if (r)
			goto ERROR;

		// SHA-2-512
		r = __pakfire_digest_update(pakfire, sha2_512_ctx, buffer, bytes_read);
		if (r)
			goto ERROR;

		// SHA-2-256
		r = __pakfire_digest_update(pakfire, sha2_256_ctx, buffer, bytes_read);
		if (r)
			goto ERROR;
	}

	// Finalize SHA-3-512
	r = __pakfire_digest_finalize(pakfire, sha3_512_ctx, digests->sha3_512);
	if (r)
		goto ERROR;

	// Finalize SHA-3-256
	r = __pakfire_digest_finalize(pakfire, sha3_256_ctx, digests->sha3_256);
	if (r)
		goto ERROR;

	// Finalize BLAKE2b512
	r = __pakfire_digest_finalize(pakfire, blake2b512_ctx, digests->blake2b512);
	if (r)
		goto ERROR;

	// Finalize BLAKE2s256
	r = __pakfire_digest_finalize(pakfire, blake2s256_ctx, digests->blake2s256);
	if (r)
		goto ERROR;

	// Finalize SHA-2-512
	r = __pakfire_digest_finalize(pakfire, sha2_512_ctx, digests->sha2_512);
	if (r)
		goto ERROR;

	// Finalize SHA-2-256
	r = __pakfire_digest_finalize(pakfire, sha2_256_ctx, digests->sha2_256);
	if (r)
		goto ERROR;

	// Done!
	r = 0;

ERROR:
	if (sha3_512_ctx)
		EVP_MD_CTX_free(sha3_512_ctx);
	if (sha3_256_ctx)
		EVP_MD_CTX_free(sha3_256_ctx);
	if (blake2b512_ctx)
		EVP_MD_CTX_free(blake2b512_ctx);
	if (blake2s256_ctx)
		EVP_MD_CTX_free(blake2s256_ctx);
	if (sha2_512_ctx)
		EVP_MD_CTX_free(sha2_512_ctx);
	if (sha2_256_ctx)
		EVP_MD_CTX_free(sha2_256_ctx);

	// Reset file offset to the beginning
	rewind(f);

	return r;
}

static void pakfire_digests_compare_mismatch(struct pakfire* pakfire, const char* what,
		const unsigned char* digest1, const unsigned char* digest2, const size_t length) {
	char* hexdigest1 = __pakfire_hexlify(digest1, length);
	char* hexdigest2 = __pakfire_hexlify(digest2, length);

	DEBUG(pakfire, "%s digest does not match:\n", what);

	if (hexdigest1)
		DEBUG(pakfire, "  Digest 1: %s\n", hexdigest1);
	if (hexdigest2)
		DEBUG(pakfire, "  Digest 2: %s\n", hexdigest2);

	if (hexdigest1)
		free(hexdigest1);
	if (hexdigest2)
		free(hexdigest2);
}

int pakfire_digests_compare(struct pakfire* pakfire, const struct pakfire_digests* digests1,
		const struct pakfire_digests* digests2, const int types) {
	int r;

	// Check if we are at least comparing one type
	if (!types) {
		errno = EINVAL;
		return 1;
	}

	// Check SHA-3-512
	if (types & PAKFIRE_DIGEST_SHA3_512) {
		r = CRYPTO_memcmp(digests1->sha3_512, digests2->sha3_512, sizeof(digests1->sha3_512));
		if (r) {
			pakfire_digests_compare_mismatch(pakfire, "SHA-3-512",
				digests1->sha3_512, digests2->sha3_512, sizeof(digests1->sha3_512));
			return 1;
		}
	}

	// Check SHA-3-256
	if (types & PAKFIRE_DIGEST_SHA3_256) {
		r = CRYPTO_memcmp(digests1->sha3_256, digests2->sha3_256, sizeof(digests1->sha3_256));
		if (r) {
			pakfire_digests_compare_mismatch(pakfire, "SHA-3-256",
				digests1->sha3_256, digests2->sha3_256, sizeof(digests1->sha3_256));
			return 1;
		}
	}

	// Check BLAKE2b512
	if (types & PAKFIRE_DIGEST_BLAKE2B512) {
		r = CRYPTO_memcmp(digests1->blake2b512, digests2->blake2b512, sizeof(digests1->blake2b512));
		if (r) {
			pakfire_digests_compare_mismatch(pakfire, "BLAKE2b512",
				digests1->blake2b512, digests2->blake2b512, sizeof(digests1->blake2b512));
			return 1;
		}
	}

	// Check BLAKE2s256
	if (types & PAKFIRE_DIGEST_BLAKE2S256) {
		r = CRYPTO_memcmp(digests1->blake2s256, digests2->blake2s256, sizeof(digests1->blake2s256));
		if (r) {
			pakfire_digests_compare_mismatch(pakfire, "BLAKE2s256",
				digests1->blake2s256, digests2->blake2s256, sizeof(digests1->blake2s256));
			return 1;
		}
	}

	// Check SHA-2-512
	if (types & PAKFIRE_DIGEST_SHA2_512) {
		r = CRYPTO_memcmp(digests1->sha2_512, digests2->sha2_512, sizeof(digests1->sha2_512));
		if (r) {
			pakfire_digests_compare_mismatch(pakfire, "SHA-2-512",
				digests1->sha2_512, digests2->sha2_512, sizeof(digests1->sha2_512));
			return 1;
		}
	}

	// Check SHA-2-256
	if (types & PAKFIRE_DIGEST_SHA2_256) {
		r = CRYPTO_memcmp(digests1->sha2_256, digests2->sha2_256, sizeof(digests1->sha2_256));
		if (r) {
			pakfire_digests_compare_mismatch(pakfire, "SHA-2-256",
				digests1->sha2_256, digests2->sha2_256, sizeof(digests1->sha2_256));
			return 1;
		}
	}

	// All digests match
	return 0;
}

int pakfire_digests_compare_one(struct pakfire* pakfire, struct pakfire_digests* digests1,
		const enum pakfire_digest_types type, const unsigned char* digest, const size_t length) {
	struct pakfire_digests digests2;
	int r;

	// Check for valid inputs
	r = pakfire_digests_check_length(pakfire, type, length);
	if (r)
		return r;

	switch (type) {
		case PAKFIRE_DIGEST_SHA3_512:
			memcpy(digests2.sha3_512, digest, sizeof(digests2.sha3_512));
			break;

		case PAKFIRE_DIGEST_SHA3_256:
			memcpy(digests2.sha3_256, digest, sizeof(digests2.sha3_256));
			break;

		case PAKFIRE_DIGEST_BLAKE2B512:
			memcpy(digests2.blake2b512, digest, sizeof(digests2.blake2b512));
			break;

		case PAKFIRE_DIGEST_BLAKE2S256:
			memcpy(digests2.blake2s256, digest, sizeof(digests2.blake2s256));
			break;

		case PAKFIRE_DIGEST_SHA2_512:
			memcpy(digests2.sha2_512, digest, sizeof(digests2.sha2_512));
			break;

		case PAKFIRE_DIGEST_SHA2_256:
			memcpy(digests2.sha2_256, digest, sizeof(digests2.sha2_256));
			break;

		case PAKFIRE_DIGEST_UNDEFINED:
			break;
	}

	return pakfire_digests_compare(pakfire, digests1, &digests2, type);
}
