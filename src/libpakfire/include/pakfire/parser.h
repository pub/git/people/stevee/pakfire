/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PARSER_H
#define PAKFIRE_PARSER_H

#ifdef PAKFIRE_PRIVATE

#include <linux/limits.h>
#include <stdio.h>

struct pakfire_parser;

#include <pakfire/package.h>
#include <pakfire/parser.h>
#include <pakfire/repo.h>

enum pakfire_parser_flags {
	PAKFIRE_PARSER_FLAGS_NONE            = 0,
	PAKFIRE_PARSER_FLAGS_EXPAND_COMMANDS = (1 << 0),
};

struct pakfire_parser_error;

struct pakfire_parser* pakfire_parser_create(struct pakfire* pakfire, struct pakfire_parser* parser,
	const char* namespace, int flags);
struct pakfire_parser* pakfire_parser_create_child(struct pakfire_parser* parser,
	const char* namespace);
struct pakfire_parser* pakfire_parser_ref(struct pakfire_parser* parser);
struct pakfire_parser* pakfire_parser_unref(struct pakfire_parser* parser);
struct pakfire_parser* pakfire_parser_get_parent(struct pakfire_parser* parser);

int pakfire_parser_set(struct pakfire_parser* parser,
	const char* namespace, const char* name, const char* value, int flags);
int pakfire_parser_append(struct pakfire_parser* parser,
	const char* namespace, const char* name, const char* value);

char* pakfire_parser_expand(struct pakfire_parser* parser, const char* namespace, const char* value);
char* pakfire_parser_get(struct pakfire_parser* parser, const char* namespace, const char* name);
int pakfire_parser_get_filelist(struct pakfire_parser* parser, const char* namespace,
	const char* name, char*** includes, char*** excludes);
char** pakfire_parser_get_split(struct pakfire_parser* parser,
	const char* namespace, const char* name, char delim);
char** pakfire_parser_list_namespaces(struct pakfire_parser* parser, const char* filter);

int pakfire_parser_merge(struct pakfire_parser* parser1, struct pakfire_parser* parser2);

int pakfire_parser_read(struct pakfire_parser* parser, FILE* f, struct pakfire_parser_error** error);
int pakfire_parser_read_file(struct pakfire_parser* parser, const char* path,
	struct pakfire_parser_error** error);
int pakfire_parser_parse(struct pakfire_parser* parser, const char* data, size_t size,
	struct pakfire_parser_error** error);
char* pakfire_parser_dump(struct pakfire_parser* parser);

const char* pakfire_parser_get_namespace(struct pakfire_parser* parser);
int pakfire_parser_set_namespace(struct pakfire_parser* parser, const char* namespace);

int pakfire_parser_create_package(struct pakfire_parser* parser,
	struct pakfire_package** pkg, struct pakfire_repo* repo, const char* namespace, const char* default_arch);

// Errors
int pakfire_parser_error_create(struct pakfire_parser_error** error,
		struct pakfire_parser* parser, const char* filename, int line, const char* message);
struct pakfire_parser_error* pakfire_parser_error_ref(struct pakfire_parser_error* error);
struct pakfire_parser_error* pakfire_parser_error_unref(struct pakfire_parser_error* error);

const char* pakfire_parser_error_get_filename(struct pakfire_parser_error* error);
int pakfire_parser_error_get_line(struct pakfire_parser_error* error);
const char* pakfire_parser_error_get_message(struct pakfire_parser_error* error);

struct pakfire* pakfire_parser_get_pakfire(struct pakfire_parser* parser);

struct pakfire_parser_declaration {
	char namespace[NAME_MAX];
	char name[NAME_MAX];
	char* value;
	enum flags {
		PAKFIRE_PARSER_DECLARATION_NONE				= 0,
		PAKFIRE_PARSER_DECLARATION_EXPORT 			= (1 << 0),
		PAKFIRE_PARSER_DECLARATION_APPEND 			= (1 << 1),
	} flags;
};

int pakfire_parser_apply_declaration(
	struct pakfire_parser* parser, struct pakfire_parser_declaration* declaration);

int pakfire_parser_parse_data(struct pakfire_parser* parent, const char* data, size_t len,
	struct pakfire_parser_error** error);

char** pakfire_parser_make_environ(struct pakfire_parser* parser);

#endif /* PAKFIRE_PRIVATE */

#endif /* PAKFIRE_PARSER_H */
