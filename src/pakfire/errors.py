#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2011 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

from .i18n import _

from ._pakfire import (
	CommandExecutionError,
	DependencyError,
)

class Error(Exception):
	exit_code = 1

	message = _("An unhandled error occured.")


class BuildError(Error):
	pass


class DownloadError(Error):
	message = _("An error occured when pakfire tried to download files.")


class TransportError(Error):
	pass


class TransportMaxTriesExceededError(TransportError):
	pass
