/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <grp.h>
#include <linux/limits.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>

#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/pwd.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define ETC_SUBUID "/etc/subuid"
#define ETC_SUBGID "/etc/subgid"

static struct passwd* pakfire_getpwent(struct pakfire* pakfire,
		int(*cmp)(struct passwd* entry, const void* value), const void* value) {
	struct passwd* entry = NULL;
	char path[PATH_MAX];
	int r;

	// Get path to /etc/passwd
	r = pakfire_path(pakfire, path, "%s", "/etc/passwd");
	if (r)
		return NULL;

	FILE* f = fopen(path, "r");
	if (!f)
		goto END;

	for (;;) {
		// Parse entry
		entry = fgetpwent(f);
		if (!entry)
			goto END;

		// Check if this is what we are looking for
		if (cmp(entry, value))
			break;
		else
			entry = NULL;
	}

END:
	if (f)
		fclose(f);

	return entry;
}

static int __pakfire_getpwnam(struct passwd* entry, const void* value) {
	const char* name = (const char*)value;

	if (strcmp(entry->pw_name, name) == 0)
		return 1;

	return 0;
}

struct passwd* pakfire_getpwnam(struct pakfire* pakfire, const char* name) {
	return pakfire_getpwent(pakfire, __pakfire_getpwnam, name);
}

static int __pakfire_getpwuid(struct passwd* entry, const void* value) {
	uid_t* uid = (uid_t*)value;

	if (entry->pw_uid == *uid)
		return 1;

	return 0;
}

struct passwd* pakfire_getpwuid(struct pakfire* pakfire, uid_t uid) {
	return pakfire_getpwent(pakfire, __pakfire_getpwuid, &uid);
}

static struct group* pakfire_getgrent(struct pakfire* pakfire,
		int(*cmp)(struct group* entry, const void* value), const void* value) {
	struct group* entry = NULL;
	char path[PATH_MAX];
	int r;

	// Get path to /etc/group
	r = pakfire_path(pakfire, path, "%s", "/etc/group");
	if (r)
		return NULL;

	FILE* f = fopen(path, "r");
	if (!f)
		goto END;

	for (;;) {
		// Parse entry
		entry = fgetgrent(f);
		if (!entry)
			goto END;

		// Check if this is what we are looking for
		if (cmp(entry, value))
			break;
		else
			entry = NULL;
	}

END:
	if (f)
		fclose(f);

	return entry;
}

static int __pakfire_getgrnam(struct group* entry, const void* value) {
	const char* name = (const char*)value;

	if (strcmp(entry->gr_name, name) == 0)
		return 1;

	return 0;
}

struct group* pakfire_getgrnam(struct pakfire* pakfire, const char* name) {
	return pakfire_getgrent(pakfire, __pakfire_getgrnam, name);
}

static int __pakfire_getgrgid(struct group* entry, const void* value) {
	gid_t* gid = (gid_t*)value;

	if (entry->gr_gid == *gid)
		return 1;

	return 0;
}

struct group* pakfire_getgrgid(struct pakfire* pakfire, gid_t gid) {
	return pakfire_getgrent(pakfire, __pakfire_getgrgid, &gid);
}

// SUBUID/SUBGID

static struct pakfire_subid* pakfire_fgetsubid(struct pakfire* pakfire, FILE* f) {
	static struct pakfire_subid subid;
	int r;

	char* line = NULL;
	size_t length = 0;
	char* p = NULL;

	// Read the next line
	while (1) {
		r = getline(&line, &length, f);
		if (r < 0)
			goto ERROR;

		// Try reading the next line if this one was empty
		else if (r == 0)
			continue;

		// Fall through
		else
			break;
	}

	// Reset r
	r = 0;

	int i = 0;

	char* token = strtok_r(line, ":", &p);
	while (token) {
		switch (i++) {
			// First field has the name
			case 0:
				pakfire_string_set(subid.name, token);
				break;

			// Second field has the ID
			case 1:
				subid.id = strtoul(token, NULL, 10);
				break;

			// Third field has the length
			case 2:
				subid.length = strtoul(token, NULL, 10);
				break;
		}

		token = strtok_r(NULL, ":", &p);
	}

	// Check if length is greater than zero
	if (subid.length == 0) {
		DEBUG(pakfire, "Length equals zero: %s\n", line);
		r = 1;
	}

ERROR:
	if (line)
		free(line);

	if (r)
		return NULL;

	DEBUG(pakfire, "Parsed SUBID entry: name=%s, id=%d, length=%zu\n",
		subid.name, subid.id, subid.length);

	return &subid;
}

int pakfire_getsubid(struct pakfire* pakfire, const char* path, const uid_t uid,
		struct pakfire_subid* subid) {
	struct pakfire_subid* entry = NULL;
	int r = 1;

	// Do not lookup root user and set the entire available UID/GID range
	if (uid == 0) {
		subid->id     = 0;
		subid->length = 0xffffffff - 1;

		return 0;
	}

	// Fetch information about the running user
	struct passwd* passwd = getpwuid(uid);
	if (!passwd) {
		ERROR(pakfire, "Could not fetch passwd entry for UID %d: %m\n", uid);
		return 1;
	}

	DEBUG(pakfire, "Fetching SUBID from %s for %s (%d)\n", path, passwd->pw_name, uid);

	// Open /etc/subuid
	FILE* f = fopen(path, "r");
	if (!f) {
		ERROR(pakfire, "Could not open %s: %m\n", ETC_SUBUID);
		goto ERROR;
	}

	// Walk through all entries
	while (1) {
		entry = pakfire_fgetsubid(pakfire, f);
		if (!entry)
			break;

		// TODO Check if name matches UID

		// Check for match
		if (strcmp(entry->name, passwd->pw_name) == 0) {
			subid->id     = entry->id;
			subid->length = entry->length;
			r = 0;

			break;
		}
	}

ERROR:
	if (f)
		fclose(f);

	return r;
}
