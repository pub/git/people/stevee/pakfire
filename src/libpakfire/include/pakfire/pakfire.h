/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PAKFIRE_H
#define PAKFIRE_PAKFIRE_H

#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/stat.h>
#include <time.h>

struct pakfire;

#include <pakfire/filelist.h>
#include <pakfire/key.h>
#include <pakfire/packagelist.h>
#include <pakfire/parser.h>
#include <pakfire/repo.h>
#include <pakfire/repolist.h>

enum pakfire_flags {
	PAKFIRE_FLAGS_OFFLINE			= (1 << 0),
};

// Callbacks
typedef void (*pakfire_log_callback)(void* data, int priority, const char* file,
	 int line, const char* fn, const char* format, va_list args);
void pakfire_set_log_callback(struct pakfire* pakfire,
	pakfire_log_callback callback, void* data);
typedef int (*pakfire_confirm_callback)(struct pakfire* pakfire, void* data,
	const char* message, const char* question);
void pakfire_set_confirm_callback(struct pakfire* pakfire,
	pakfire_confirm_callback callback, void* data);
typedef void (*pakfire_status_callback)(struct pakfire* pakfire, void* data,
	int progress, const char* status);

int pakfire_create(struct pakfire** pakfire, const char* path, const char* arch,
	const char* conf, int flags,
	int loglevel, pakfire_log_callback log_callback, void* log_data);

struct pakfire* pakfire_ref(struct pakfire* pakfire);
struct pakfire* pakfire_unref(struct pakfire* pakfire);

const char* pakfire_get_path(struct pakfire* pakfire);

int pakfire_clean(struct pakfire* pakfire, int flags);
int pakfire_refresh(struct pakfire* pakfire, int flags);

int pakfire_list_keys(struct pakfire* pakfire, struct pakfire_key*** keys);

int pakfire_copy_in(struct pakfire* pakfire, const char* src, const char* dst);
int pakfire_copy_out(struct pakfire* pakfire, const char* src, const char* dst);

const char* pakfire_get_arch(struct pakfire* pakfire);

int pakfire_version_compare(struct pakfire* pakfire, const char* evr1, const char* evr2);

struct pakfire_repolist* pakfire_get_repos(struct pakfire* pakfire);
struct pakfire_repo* pakfire_get_repo(struct pakfire* pakfire, const char* name);

int pakfire_whatprovides(struct pakfire* pakfire, const char* what, int flags,
	struct pakfire_packagelist* list);
int pakfire_whatrequires(struct pakfire* pakfire, const char* what, int flags,
	struct pakfire_packagelist* list);

// Search

enum pakfire_search_flags {
	PAKFIRE_SEARCH_NAME_ONLY        = (1 << 0),
};

int pakfire_search(struct pakfire* pakfire, const char* what, int flags,
	struct pakfire_packagelist* list);

// Logging

int pakfire_log_get_priority(struct pakfire* pakfire);
void pakfire_log_set_priority(struct pakfire* pakfire, int priority);

// Install/Erase/Update

int pakfire_install(struct pakfire* pakfire, int transaction_flags, int solver_flags,
	const char** packages, const char** locks, int job_flags, int* changed,
	pakfire_status_callback status_callback, void* status_callback_data);
int pakfire_erase(struct pakfire* pakfire, int transaction_flags, int solver_flags,
	const char** packages, const char** locks, int job_flags, int* changed,
	pakfire_status_callback status_callback, void* status_callback_data);
int pakfire_update(struct pakfire* pakfire, int transaction_flags, int solver_flags,
	const char** packages, const char** locks, int jobs_flags, int* changed,
	pakfire_status_callback status_callback, void* status_callback_data);

// Check

int pakfire_check(struct pakfire* pakfire, struct pakfire_filelist* errors);

// Sync

int pakfire_sync(struct pakfire* pakfire, int solver_flags, int flags, int* changed,
	pakfire_status_callback status_callback, void* status_callback_data);

#ifdef PAKFIRE_PRIVATE

#include <sys/types.h>

#include <gpgme.h>
#include <magic.h>
#include <solv/pool.h>

#include <pakfire/config.h>
#include <pakfire/pwd.h>

int pakfire_on_root(struct pakfire* pakfire);

uid_t pakfire_uid(struct pakfire* pakfire);
gid_t pakfire_gid(struct pakfire* pakfire);

const struct pakfire_subid* pakfire_subuid(struct pakfire* pakfire);
const struct pakfire_subid* pakfire_subgid(struct pakfire* pakfire);

void pakfire_log(struct pakfire* pakfire, int priority, const char *file,
	int line, const char *fn, const char *format, ...)
	__attribute__((format(printf, 6, 7)));

int pakfire_has_flag(struct pakfire* pakfire, int flag);

// Locking
int pakfire_acquire_lock(struct pakfire* pakfire);
void pakfire_release_lock(struct pakfire* pakfire);

struct pakfire_config* pakfire_get_config(struct pakfire* pakfire);
int pakfire_is_mountpoint(struct pakfire* pakfire, const char* path);

int pakfire_confirm(struct pakfire* pakfire, const char* message, const char* question);

magic_t pakfire_get_magic(struct pakfire* pakfire);
gpgme_ctx_t pakfire_get_gpgctx(struct pakfire* pakfire);

const char* pakfire_get_distro_name(struct pakfire* pakfire);
const char* pakfire_get_distro_id(struct pakfire* pakfire);
const char* pakfire_get_distro_vendor(struct pakfire* pakfire);
const char* pakfire_get_distro_version(struct pakfire* pakfire);
const char* pakfire_get_distro_version_id(struct pakfire* pakfire);
const char* pakfire_get_distro_tag(struct pakfire* pakfire);

const char* pakfire_get_keystore_path(struct pakfire* pakfire);

#define pakfire_path(pakfire, path, format, ...) \
	__pakfire_path(pakfire, path, sizeof(path), format, __VA_ARGS__)
int __pakfire_path(struct pakfire* pakfire, char* path, const size_t length,
	const char* format, ...) __attribute__((format(printf, 4, 5)));

const char* pakfire_relpath(struct pakfire* pakfire, const char* path);

#define pakfire_cache_path(pakfire, path, format, ...) \
	__pakfire_cache_path(pakfire, path, sizeof(path), format, __VA_ARGS__)
int __pakfire_cache_path(struct pakfire* pakfire, char* path, size_t length,
	const char* format, ...) __attribute__((format(printf, 4, 5)));

void pakfire_pool_has_changed(struct pakfire* pakfire);
void pakfire_pool_internalize(struct pakfire* pakfire);

Pool* pakfire_get_solv_pool(struct pakfire* pakfire);

struct pakfire_repo* pakfire_get_installed_repo(struct pakfire* pakfire);

int pakfire_commandline_add(struct pakfire* pakfire, const char* path,
	struct pakfire_package** package);

typedef int (*pakfire_repo_walk_callback)
	(struct pakfire* pakfire, struct pakfire_repo* repo, void* p);
int pakfire_repo_walk(struct pakfire* pakfire,
	pakfire_repo_walk_callback callback, void* p);

// Archive helpers
struct archive* pakfire_make_archive_disk_reader(struct pakfire* pakfire, int internal);
struct archive* pakfire_make_archive_disk_writer(struct pakfire* pakfire, int internal);

#endif

#endif /* PAKFIRE_PAKFIRE_H */
