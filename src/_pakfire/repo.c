/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2011 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <Python.h>

#include <pakfire/package.h>
#include <pakfire/repo.h>
#include <pakfire/util.h>

#include "archive.h"
#include "key.h"
#include "package.h"
#include "repo.h"

PyObject* new_repo(PyTypeObject* type, struct pakfire_repo* repo) {
	RepoObject* self = (RepoObject *)type->tp_alloc(type, 0);
	if (self) {
		self->repo = pakfire_repo_ref(repo);
	}

	return (PyObject*)self;
}

static PyObject* Repo_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	RepoObject* self = (RepoObject *)type->tp_alloc(type, 0);
	if (self) {
		self->repo = NULL;
	}

	return (PyObject *)self;
}

static void Repo_dealloc(RepoObject* self) {
	if (self->repo)
		pakfire_repo_unref(self->repo);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static int Repo_init(RepoObject* self, PyObject* args, PyObject* kwds) {
	PakfireObject* pakfire;
	const char* name;
	int clean = 0;

	if (!PyArg_ParseTuple(args, "O!s|i", &PakfireType, &pakfire, &name, &clean))
		return -1;

	// Create a new repository
	int r = pakfire_repo_create(&self->repo, pakfire->pakfire, name);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return -1;
	}

	self->clean = clean;

	return 0;
}

static PyObject* Repo_repr(RepoObject* self) {
	return PyUnicode_FromFormat("<_pakfire.Repo %s>", pakfire_repo_get_name(self->repo));
}

static long Repo_hash(RepoObject* self) {
	return (long)self->repo;
}

static PyObject* Repo_richcompare(RepoObject* self, RepoObject* other, int op) {
	int r;

	switch (op) {
		case Py_EQ:
			if (pakfire_repo_identical(self->repo, other->repo) == 0)
				Py_RETURN_TRUE;

			Py_RETURN_FALSE;
			break;

		case Py_LT:
			r = pakfire_repo_cmp(self->repo, other->repo);
			if (r < 0)
				Py_RETURN_TRUE;

			Py_RETURN_FALSE;
			break;

		default:
			break;
	}

	Py_RETURN_NOTIMPLEMENTED;
}

static Py_ssize_t Repo_len(RepoObject* self) {
	return pakfire_repo_count(self->repo);
}

static PyObject* Repo_get_name(RepoObject* self) {
	const char* name = pakfire_repo_get_name(self->repo);

	return PyUnicode_FromString(name);
}

static PyObject* Repo_get_description(RepoObject* self) {
	const char* description = pakfire_repo_get_description(self->repo);

	return PyUnicode_FromString(description);
}

static int Repo_set_description(RepoObject* self, PyObject* value) {
	const char* description = NULL;

	if (value != Py_None)
		description = PyUnicode_AsUTF8(value);

	return pakfire_repo_set_description(self->repo, description);
}

static PyObject* Repo_get_enabled(RepoObject* self) {
	if (pakfire_repo_get_enabled(self->repo))
		Py_RETURN_TRUE;

	Py_RETURN_FALSE;
}

static int Repo_set_enabled(RepoObject* self, PyObject* value) {
	if (PyObject_IsTrue(value))
		pakfire_repo_set_enabled(self->repo, 1);
	else
		pakfire_repo_set_enabled(self->repo, 0);

	return 0;
}

static PyObject* Repo_get_priority(RepoObject* self) {
	int priority = pakfire_repo_get_priority(self->repo);

	return PyLong_FromLong(priority);
}

static int Repo_set_priority(RepoObject* self, PyObject* value) {
	long priority = PyLong_AsLong(value);

	if (priority > INT_MAX || priority < INT_MIN)
		return -1;

	pakfire_repo_set_priority(self->repo, priority);
	return 0;
}

static PyObject* Repo_get_baseurl(RepoObject* self) {
	const char* baseurl = pakfire_repo_get_baseurl(self->repo);

	if (baseurl)
		return PyUnicode_FromString(baseurl);

	Py_RETURN_NONE;
}

static int Repo_set_baseurl(RepoObject* self, PyObject* value) {
	const char* baseurl = NULL;

	if (value != Py_None)
		baseurl = PyUnicode_AsUTF8(value);

	return pakfire_repo_set_baseurl(self->repo, baseurl);
}

static PyObject* Repo_get_key(RepoObject* self) {
	struct pakfire_key* key = pakfire_repo_get_key(self->repo);

	if (key) {
		PyObject* ret = new_key(&KeyType, key);
		pakfire_key_unref(key);

		return ret;
	}

	Py_RETURN_NONE;
}

static PyObject* Repo_get_mirrorlist(RepoObject* self) {
	const char* mirrorlist = pakfire_repo_get_mirrorlist_url(self->repo);

	if (mirrorlist)
		return PyUnicode_FromString(mirrorlist);

	Py_RETURN_NONE;
}

static int Repo_set_mirrorlist(RepoObject* self, PyObject* value) {
	const char* mirrorlist = NULL;

	if (value != Py_None)
		mirrorlist = PyUnicode_AsUTF8(value);

	return pakfire_repo_set_mirrorlist_url(self->repo, mirrorlist);
}

static PyObject* Repo_refresh(RepoObject* self, PyObject* args, PyObject* kwds) {
	char* kwlist[] = { "force", NULL };

	int force = 0;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "|p", kwlist, &force))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	int r = pakfire_repo_refresh(self->repo, force);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	Py_RETURN_NONE;
}

static PyObject* Repo_clean(RepoObject* self, PyObject* args) {
	Py_BEGIN_ALLOW_THREADS

	int r = pakfire_repo_clean(self->repo, 0);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	Py_RETURN_NONE;
}

static PyObject* Repo_scan(RepoObject* self, PyObject* args) {
	int flags = 0;

	if (!PyArg_ParseTuple(args, "|i", &flags))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	int r = pakfire_repo_scan(self->repo, flags);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	Py_RETURN_NONE;
}

static PyObject* Repo_enter(RepoObject* self) {
	Py_INCREF(self);

	return (PyObject*)self;
}

static PyObject* Repo_exit(RepoObject* self, PyObject* args) {
	PyObject* type;
	PyObject* value;
	PyObject* traceback;

	if (!PyArg_ParseTuple(args, "OOO", &type, &value, &traceback))
		return NULL;

	// Automatically cleanup the repository
	if (self->clean) {
		int r = pakfire_repo_clean(self->repo, 0);

		if (r) {
			PyErr_SetFromErrno(PyExc_OSError);
			return NULL;
		}
	}

	Py_RETURN_NONE;
}

static struct PyMethodDef Repo_methods[] = {
	{
		"__enter__",
		(PyCFunction)Repo_enter,
		METH_NOARGS,
		NULL
	},
	{
		"__exit__",
		(PyCFunction)Repo_exit,
		METH_VARARGS,
		NULL
	},
	{
		"clean",
		(PyCFunction)Repo_clean,
		METH_VARARGS,
		NULL,
	},
	{
		"refresh",
		(PyCFunction)Repo_refresh,
		METH_VARARGS|METH_KEYWORDS,
		NULL,
	},
	{
		"scan",
		(PyCFunction)Repo_scan,
		METH_VARARGS,
		NULL
	},
	{ NULL }
};

static struct PyGetSetDef Repo_getsetters[] = {
	{
		"baseurl",
		(getter)Repo_get_baseurl,
		(setter)Repo_set_baseurl,
		"The base URL of this repository",
		NULL
	},
	{
		"description",
		(getter)Repo_get_description,
		(setter)Repo_set_description,
		NULL,
		NULL
	},
	{
		"key",
		(getter)Repo_get_key,
		NULL,
		NULL,
		NULL
	},
	{
		"mirrorlist",
		(getter)Repo_get_mirrorlist,
		(setter)Repo_set_mirrorlist,
		NULL,
		NULL
	},
	{
		"name",
		(getter)Repo_get_name,
		NULL,
		"The name of the repository",
		NULL
	},
	{
		"enabled",
		(getter)Repo_get_enabled,
		(setter)Repo_set_enabled,
		NULL,
		NULL
	},
	{
		"priority",
		(getter)Repo_get_priority,
		(setter)Repo_set_priority,
		"The priority of the repository",
		NULL
	},
	{ NULL }
};

static PySequenceMethods Repo_sequence = {
	sq_length:          (lenfunc)Repo_len,
};

PyTypeObject RepoType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	tp_name:            "_pakfire.Repo",
	tp_basicsize:       sizeof(RepoObject),
	tp_flags:           Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	tp_new:             Repo_new,
	tp_dealloc:         (destructor)Repo_dealloc,
	tp_init:            (initproc)Repo_init,
	tp_doc:             "Repo object",
	tp_methods:         Repo_methods,
	tp_getset:          Repo_getsetters,
	tp_repr:            (reprfunc)Repo_repr,
	tp_as_sequence:     &Repo_sequence,
	tp_hash:            (hashfunc)Repo_hash,
	tp_richcompare:     (richcmpfunc)Repo_richcompare,
};
