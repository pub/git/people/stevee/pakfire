/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdlib.h>
#include <string.h>

#include <pakfire/key.h>
#include <pakfire/util.h>

#include "../testsuite.h"
#include "key.h"

static int test_init(const struct test* t) {
	struct pakfire_key** keys = NULL;
	int r = EXIT_FAILURE;

	// Try loading any keys & delete them all
	ASSERT_SUCCESS(pakfire_list_keys(t->pakfire, &keys));
	while (keys && *keys) {
		struct pakfire_key* key = *keys++;

		pakfire_key_delete(key);
		pakfire_key_unref(key);
	}

	// Load list of keys again
	ASSERT_SUCCESS(pakfire_list_keys(t->pakfire, &keys));

	// Must be empty now
	ASSERT(keys == NULL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	return r;
}

static int test_generate_key(const struct test* t, const char* algo, const char* userid) {
	struct pakfire_key* key = NULL;
	char* dump = NULL;
	int r = EXIT_FAILURE;

	// Generate a new key
	ASSERT_SUCCESS(pakfire_key_generate(&key, t->pakfire, algo, userid));

	// Export the key
	ASSERT_SUCCESS(pakfire_key_export(key, stdout, PAKFIRE_KEY_EXPORT_MODE_SECRET));

	// Dump the key details
	ASSERT(dump = pakfire_key_dump(key));
	printf("%s\n", dump);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (key)
		pakfire_key_unref(key);
	if (dump)
		free(dump);

	return r;
}

static int test_generate(const struct test* t) {
	struct pakfire_key* key = NULL;
	int r = EXIT_FAILURE;

	// Try to call pakfire_key_generate() with some invalid inputs
	ASSERT_ERRNO(pakfire_key_generate(&key, t->pakfire, NULL, NULL), EINVAL);
	ASSERT_ERRNO(pakfire_key_generate(&key, t->pakfire, "rsa2048", NULL), EINVAL);
	ASSERT_ERRNO(pakfire_key_generate(&key, t->pakfire, NULL, "Invalid Test Key"), EINVAL);

	// Generate a new key using RSA2048
	ASSERT_SUCCESS(test_generate_key(t, "rsa2048", "Test Key #1"));

	// Generate a new key using ed25519
	ASSERT_SUCCESS(test_generate_key(t, "ed25519", "Test Key #2"));

	// Try to generate a key with the same ID
	ASSERT_ERRNO(pakfire_key_generate(&key, t->pakfire, "rsa2048", "Test Key #1"), EINVAL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (key)
		pakfire_key_unref(key);

	return r;
}

static int test_import_export(const struct test* t) {
	// Try to delete the key just in case it
	// has been imported before
	struct pakfire_key* key = pakfire_key_get(t->pakfire, TEST_KEY_FINGERPRINT);
	if (key) {
		pakfire_key_delete(key);
		pakfire_key_unref(key);
	}

#if 0
	// Import a key
	struct pakfire_key** keys = pakfire_key_import(t->pakfire, TEST_KEY_DATA);

	// We should have a list with precisely one key object
	ASSERT(keys);
	ASSERT(keys[0] != NULL);
	ASSERT(keys[1] == NULL);

	// Get the imported key
	key = *keys;

	// Check the fingerprint
	const char* fingerprint = pakfire_key_get_fingerprint(key);
	ASSERT(strcmp(fingerprint, TEST_KEY_FINGERPRINT) == 0);

	// Dump key description
	char* dump = pakfire_key_dump(key);
	ASSERT(dump);
	LOG("%s\n", dump);
	free(dump);

	// Export the key
	char* data = pakfire_key_export(key, 0);
	ASSERT(data);

	LOG("Exported key:\n%s\n", data);
	free(data);

	pakfire_key_unref(key);
#endif

	return EXIT_SUCCESS;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_init);
	testsuite_add_test(test_generate);
	testsuite_add_test(test_import_export);

	return testsuite_run(argc, argv);
}
