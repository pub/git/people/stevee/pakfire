/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2011 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <Python.h>

#include <pakfire/digest.h>
#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/package.h>
#include <pakfire/repo.h>
#include <pakfire/util.h>

#include "package.h"
#include "pakfire.h"
#include "repo.h"
#include "util.h"

PyObject* new_package(PyTypeObject* type, struct pakfire_package* pkg) {
	PackageObject* self = (PackageObject *)type->tp_alloc(type, 0);
	if (self) {
		self->package = pakfire_package_ref(pkg);
	}

	return (PyObject *)self;
}

static PyObject* Package_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	PackageObject* self = (PackageObject *)type->tp_alloc(type, 0);
	if (self) {
		self->package = NULL;
	}

	return (PyObject *)self;
}

static void Package_dealloc(PackageObject* self) {
	pakfire_package_unref(self->package);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static int Package_init(PackageObject* self, PyObject* args, PyObject* kwds) {
	PakfireObject* pakfire;
	RepoObject* repo;
	const char* name = NULL;
	const char* evr = NULL;
	const char* arch = NULL;

	if (!PyArg_ParseTuple(args, "O!O!sss", &PakfireType, &pakfire, &RepoType, &repo,
			&name, &evr, &arch))
		return -1;

	int r = pakfire_package_create(&self->package, pakfire->pakfire, repo->repo, name, evr, arch);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return -1;
	}

	return 0;
}

static Py_hash_t Package_hash(PackageObject* self) {
	return pakfire_package_id(self->package);
}

static PyObject* Package_repr(PackageObject* self) {
	const char* nevra = pakfire_package_get_string(self->package, PAKFIRE_PKG_NEVRA);

	return PyUnicode_FromFormat("<_pakfire.Package %s>", nevra);
}

static PyObject* Package_str(PackageObject* self) {
	const char* nevra = pakfire_package_get_string(self->package, PAKFIRE_PKG_NEVRA);

	return PyUnicode_FromString(nevra);
}

static PyObject* Package_richcompare(PackageObject* self, PyObject* _other, int op) {
	if (!PyType_IsSubtype(_other->ob_type, &PackageType)) {
		PyErr_SetString(PyExc_TypeError, "Expected a Package object");
		return NULL;
	}

	PackageObject* other = (PackageObject *)_other;

	long result = pakfire_package_cmp(self->package, other->package);

	switch (op) {
		case Py_EQ:
			if (result == 0)
				Py_RETURN_TRUE;
			break;

		case Py_NE:
			if (result != 0)
				Py_RETURN_TRUE;
			break;

		case Py_LE:
			if (result <= 0)
				Py_RETURN_TRUE;
			break;

		case Py_GE:
			if (result >= 0)
				Py_RETURN_TRUE;
			break;

		case Py_LT:
			if (result < 0)
				Py_RETURN_TRUE;
			break;

		case Py_GT:
			if (result > 0)
				Py_RETURN_TRUE;
			break;

		default:
			PyErr_BadArgument();
			return NULL;
	}

	Py_RETURN_FALSE;
}

static const char* PyUnicode_FromValue(PyObject* value) {
	if (value == Py_None)
		return NULL;

	return PyUnicode_AsUTF8(value);
}

static PyObject* Package_get_name(PackageObject* self) {
	const char* name = pakfire_package_get_string(self->package, PAKFIRE_PKG_NAME);
	if (!name)
		Py_RETURN_NONE;

	return PyUnicode_FromString(name);
}

static PyObject* Package_get_evr(PackageObject* self) {
	const char* evr = pakfire_package_get_string(self->package, PAKFIRE_PKG_EVR);
	if (!evr)
		Py_RETURN_NONE;

	return PyUnicode_FromString(evr);
}

static PyObject* Package_get_arch(PackageObject* self) {
	const char* arch = pakfire_package_get_string(self->package, PAKFIRE_PKG_ARCH);
	if (!arch)
		Py_RETURN_NONE;

	return PyUnicode_FromString(arch);
}

static PyObject* Package_get_uuid(PackageObject* self) {
	const char* uuid = pakfire_package_get_string(self->package, PAKFIRE_PKG_UUID);
	if (!uuid)
		Py_RETURN_NONE;

	return PyUnicode_FromString(uuid);
}

static void Package_set_uuid(PackageObject* self, PyObject* value) {
	const char* uuid = PyUnicode_FromValue(value);

	pakfire_package_set_string(self->package, PAKFIRE_PKG_UUID, uuid);
}

static PyObject* Package_get_digest(PackageObject* self) {
	enum pakfire_digest_types type = PAKFIRE_DIGEST_UNDEFINED;
	const unsigned char* digest = NULL;
	size_t length = 0;

	// Fetch the digest
	digest = pakfire_package_get_digest(self->package, &type, &length);
	if (!digest)
		Py_RETURN_NONE;

	return Py_BuildValue("(sy#)", pakfire_digest_name(type), digest, length);
}

static PyObject* Package_get_summary(PackageObject* self) {
	const char* summary = pakfire_package_get_string(self->package, PAKFIRE_PKG_SUMMARY);
	if (!summary)
		Py_RETURN_NONE;

	return PyUnicode_FromString(summary);
}

static void Package_set_summary(PackageObject* self, PyObject* value) {
	const char* summary = PyUnicode_FromValue(value);

	pakfire_package_set_string(self->package, PAKFIRE_PKG_SUMMARY, summary);
}

static PyObject* Package_get_description(PackageObject* self) {
	const char* description = pakfire_package_get_string(self->package, PAKFIRE_PKG_DESCRIPTION);
	if (!description)
		Py_RETURN_NONE;

	return PyUnicode_FromString(description);
}

static void Package_set_description(PackageObject* self, PyObject* value) {
	const char* description = PyUnicode_FromValue(value);

	pakfire_package_set_string(self->package, PAKFIRE_PKG_DESCRIPTION, description);
}

static PyObject* Package_get_license(PackageObject* self) {
	const char* license = pakfire_package_get_string(self->package, PAKFIRE_PKG_LICENSE);
	if (!license)
		Py_RETURN_NONE;

	return PyUnicode_FromString(license);
}

static void Package_set_license(PackageObject* self, PyObject* value) {
	const char* license = PyUnicode_FromValue(value);

	pakfire_package_set_string(self->package, PAKFIRE_PKG_LICENSE, license);
}

static PyObject* Package_get_url(PackageObject* self) {
	const char* url = pakfire_package_get_string(self->package, PAKFIRE_PKG_URL);
	if (!url)
		Py_RETURN_NONE;

	return PyUnicode_FromString(url);
}

static void Package_set_url(PackageObject* self, PyObject* value) {
	const char* url = PyUnicode_FromValue(value);

	pakfire_package_set_string(self->package, PAKFIRE_PKG_URL, url);
}

static PyObject* Package_get_groups(PackageObject* self) {
	const char* s = pakfire_package_get_string(self->package, PAKFIRE_PKG_GROUPS);

	return PyUnicode_FromString(s);
}

static int Package_set_groups(PackageObject* self, PyObject* value) {
	const char* groups = PyUnicode_AsUTF8(value);

	if (groups)
		pakfire_package_set_string(self->package, PAKFIRE_PKG_GROUPS, groups);

	return 0;
}

static PyObject* Package_get_vendor(PackageObject* self) {
	const char* vendor = pakfire_package_get_string(self->package, PAKFIRE_PKG_VENDOR);
	if (!vendor)
		Py_RETURN_NONE;

	return PyUnicode_FromString(vendor);
}

static void Package_set_vendor(PackageObject* self, PyObject* value) {
	const char* vendor = PyUnicode_FromValue(value);

	pakfire_package_set_string(self->package, PAKFIRE_PKG_VENDOR, vendor);
}

static PyObject* Package_get_distribution(PackageObject* self) {
	const char* distribution = pakfire_package_get_string(self->package, PAKFIRE_PKG_DISTRO);
	if (!distribution)
		Py_RETURN_NONE;

	return PyUnicode_FromString(distribution);
}

static void Package_set_distribution(PackageObject* self, PyObject* value) {
	const char* distribution = PyUnicode_AsUTF8(value);

	pakfire_package_set_string(self->package, PAKFIRE_PKG_DISTRO, distribution);
}

static PyObject* Package_get_packager(PackageObject* self) {
	const char* packager = pakfire_package_get_string(self->package, PAKFIRE_PKG_PACKAGER);
	if (!packager)
		Py_RETURN_NONE;

	return PyUnicode_FromString(packager);
}

static void Package_set_packager(PackageObject* self, PyObject* value) {
	const char* packager = PyUnicode_FromValue(value);

	if (packager)
		pakfire_package_set_string(self->package, PAKFIRE_PKG_PACKAGER, packager);
}

static PyObject* Package_get_filename(PackageObject* self) {
	const char* filename = pakfire_package_get_string(self->package, PAKFIRE_PKG_FILENAME);
	if (!filename)
		Py_RETURN_NONE;

	return PyUnicode_FromString(filename);
}

static void Package_set_filename(PackageObject* self, PyObject* value) {
	const char* filename = PyUnicode_FromValue(value);

	pakfire_package_set_string(self->package, PAKFIRE_PKG_FILENAME, filename);
}

static PyObject* Package_get_downloadsize(PackageObject* self) {
	size_t size = pakfire_package_get_num(self->package, PAKFIRE_PKG_DOWNLOADSIZE, 0);

	return PyLong_FromUnsignedLongLong(size);
}

static void Package_set_downloadsize(PackageObject* self, PyObject* value) {
	unsigned long downloadsize = PyLong_AsUnsignedLong(value);

	pakfire_package_set_num(self->package, PAKFIRE_PKG_DOWNLOADSIZE, downloadsize);
}

static PyObject* Package_get_installsize(PackageObject* self) {
	size_t size = pakfire_package_get_num(self->package, PAKFIRE_PKG_INSTALLSIZE, 0);

	return PyLong_FromUnsignedLongLong(size);
}

static void Package_set_installsize(PackageObject* self, PyObject* value) {
	unsigned long installsize = PyLong_AsUnsignedLong(value);

	pakfire_package_set_num(self->package, PAKFIRE_PKG_INSTALLSIZE, installsize);
}

static PyObject* Package_get_size(PackageObject* self) {
	size_t size = pakfire_package_get_size(self->package);

	return PyLong_FromUnsignedLongLong(size);
}

static PyObject* Package_get_buildhost(PackageObject* self) {
	const char* build_host = pakfire_package_get_string(self->package, PAKFIRE_PKG_BUILD_HOST);
	if (!build_host)
		Py_RETURN_NONE;

	return PyUnicode_FromString(build_host);
}

static void Package_set_buildhost(PackageObject* self, PyObject* value) {
	const char* build_host = PyUnicode_FromValue(value);

	pakfire_package_set_string(self->package, PAKFIRE_PKG_BUILD_HOST, build_host);
}

static PyObject* Package_get_buildtime(PackageObject* self) {
	time_t build_time = pakfire_package_get_num(self->package, PAKFIRE_PKG_BUILD_TIME, 0);

	return PyLong_FromUnsignedLongLong(build_time);
}

static void Package_set_buildtime(PackageObject* self, PyObject* value) {
	time_t build_time = PyLong_AsUnsignedLongLong(value);

	pakfire_package_set_num(self->package, PAKFIRE_PKG_BUILD_TIME, build_time);
}

static PyObject* Package_get_repo(PackageObject* self) {
	struct pakfire_repo* repo = pakfire_package_get_repo(self->package);
	if (!repo)
		Py_RETURN_NONE;

	PyObject* obj = new_repo(&RepoType, repo);
	pakfire_repo_unref(repo);

	return obj;
}

static PyObject* Package_get_path(PackageObject* self) {
	const char* path = pakfire_package_get_string(self->package, PAKFIRE_PKG_PATH);
	if (!path) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicode_FromString(path);
}

static PyObject* PyList_FromRelationList(char** deps) {
	PyObject* list = PyList_New(0);
	if (list == NULL)
		return NULL;

	// Empty list?
	if (!deps)
		return list;

	for (char** dep = deps; *dep; dep++) {
		PyObject* obj = PyUnicode_FromString(*dep);
		if (!obj)
			break;

		PyList_Append(list, obj);
		Py_DECREF(obj);
	}

	return list;
}

static PyObject* Package_get_provides(PackageObject* self) {
	char** deps = pakfire_package_get_deps(self->package, PAKFIRE_PKG_PROVIDES);

	PyObject* list = PyList_FromRelationList(deps);

	if (deps) {
		for (char** dep = deps; *dep; dep++)
			free(*dep);
		free(deps);
	}

	return list;
}

static PyObject* Package_get_requires(PackageObject* self) {
	char** deps = pakfire_package_get_deps(self->package, PAKFIRE_PKG_REQUIRES);

	PyObject* list = PyList_FromRelationList(deps);

	if (deps) {
		for (char** dep = deps; *dep; dep++)
			free(*dep);
		free(deps);
	}

	return list;
}

static PyObject* Package_get_obsoletes(PackageObject* self) {
	char** deps = pakfire_package_get_deps(self->package, PAKFIRE_PKG_OBSOLETES);

	PyObject* list = PyList_FromRelationList(deps);

	if (deps) {
		for (char** dep = deps; *dep; dep++)
			free(*dep);
		free(deps);
	}

	return list;
}

static PyObject* Package_get_conflicts(PackageObject* self) {
	char** deps = pakfire_package_get_deps(self->package, PAKFIRE_PKG_CONFLICTS);

	PyObject* list = PyList_FromRelationList(deps);

	if (deps) {
		for (char** dep = deps; *dep; dep++)
			free(*dep);
		free(deps);
	}

	return list;
}

static PyObject* Package_get_recommends(PackageObject* self) {
	char** deps = pakfire_package_get_deps(self->package, PAKFIRE_PKG_RECOMMENDS);

	PyObject* list = PyList_FromRelationList(deps);

	if (deps) {
		for (char** dep = deps; *dep; dep++)
			free(*dep);
		free(deps);
	}

	return list;
}

static PyObject* Package_get_suggests(PackageObject* self) {
	char** deps = pakfire_package_get_deps(self->package, PAKFIRE_PKG_SUGGESTS);

	PyObject* list = PyList_FromRelationList(deps);

	if (deps) {
		for (char** dep = deps; *dep; dep++)
			free(*dep);
		free(deps);
	}

	return list;
}

static PyObject* Package_get_reverse_requires(PackageObject* self) {
	struct pakfire_packagelist* list = NULL;
	PyObject* ret = NULL;
	int r;

	struct pakfire* pakfire = pakfire_package_get_pakfire(self->package);

	// Create a new packagelist
	r = pakfire_packagelist_create(&list, pakfire);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Search for all reverse requires
	r = pakfire_package_get_reverse_requires(self->package, list);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	ret = PyList_FromPackageList(list);

ERROR:
	if (list)
		pakfire_packagelist_unref(list);
	if (pakfire)
		pakfire_unref(pakfire);

	return ret;
}

static PyObject* Package_get_filelist(PackageObject* self, PyObject* args) {
	PyObject* list = PyList_New(0);
	if (list == NULL)
		return NULL;

	struct pakfire_filelist* filelist = pakfire_package_get_filelist(self->package);
	for (unsigned int i = 0; i < pakfire_filelist_length(filelist); i++) {
		struct pakfire_file* file = pakfire_filelist_get(filelist, i);
		const char* path = pakfire_file_get_path(file);
		pakfire_file_unref(file);

		PyObject* obj = PyUnicode_FromString(path);

		int ret = PyList_Append(list, obj);
		Py_DECREF(obj);

		if (ret) {
			pakfire_filelist_unref(filelist);
			Py_DECREF(list);

			return NULL;
		}
	}

	pakfire_filelist_unref(filelist);

	return list;
}

static int Package_set_filelist(PackageObject* self, PyObject* value) {
	if (!PySequence_Check(value)) {
		PyErr_SetString(PyExc_AttributeError, "Expected a sequence.");
		return -1;
	}

	struct pakfire* pakfire = pakfire_package_get_pakfire(self->package);

	// Create a new filelist
	struct pakfire_filelist* filelist;

	int r = pakfire_filelist_create(&filelist, pakfire);
	if (r) {
		pakfire_unref(pakfire);
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);

		return -1;
	}

	const int length = PySequence_Length(value);
	for (int i = 0; i < length; i++) {
		PyObject* item = PySequence_GetItem(value, i);

		if (!PyUnicode_Check(item)) {
			Py_DECREF(item);
			pakfire_filelist_unref(filelist);
			pakfire_unref(pakfire);

			PyErr_SetString(PyExc_AttributeError, "Expected a string");
			return -1;
		}

		const char* path = PyUnicode_AsUTF8(item);
		Py_DECREF(item);

		// Create a new file
		struct pakfire_file* file;

		r = pakfire_file_create(&file, pakfire);
		if (r) {
			errno = -r;
			PyErr_SetFromErrno(PyExc_OSError);
			pakfire_filelist_unref(filelist);
			pakfire_unref(pakfire);
			return -1;
		}

		// Set the path
		pakfire_file_set_path(file, path);

		// Append the file to the filelist
		pakfire_filelist_add(filelist, file);
		pakfire_file_unref(file);
	}

	// Set filelist
	r = pakfire_package_set_filelist(self->package, filelist);
	pakfire_filelist_unref(filelist);

	if (r) {
		pakfire_unref(pakfire);

		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);

		return -1;
	}

	pakfire_unref(pakfire);

	return 0;
}

static PyObject* Package_dump(PackageObject* self, PyObject *args, PyObject* kwds) {
	static char* kwlist[] = {"long", "filelist", NULL};

	int long_format = 0;
	int filelist = 0;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "|ii", kwlist, &long_format, &filelist))
		return NULL;

	int flags = 0;
	if (long_format)
		flags |= PAKFIRE_PKG_DUMP_LONG;

	if (filelist)
		flags |= PAKFIRE_PKG_DUMP_FILELIST;

	char* dump = pakfire_package_dump(self->package, flags);
	if (!dump)
		Py_RETURN_NONE;

	PyObject* ret = PyUnicode_FromString(dump);
	free(dump);

	return ret;
}

static PyObject* Package_get_source_package(PackageObject* self) {
	const char* source_package = pakfire_package_get_string(self->package, PAKFIRE_PKG_SOURCE_PKG);
	if (!source_package)
		Py_RETURN_NONE;

	return PyUnicode_FromString(source_package);
}

static PyObject* Package_get_source_name(PackageObject* self) {
	const char* source_name = pakfire_package_get_string(self->package, PAKFIRE_PKG_SOURCE_NAME);
	if (!source_name)
		Py_RETURN_NONE;

	return PyUnicode_FromString(source_name);
}

static PyObject* Package_set_source_name(PackageObject* self, PyObject* value) {
	const char* source_name = PyUnicode_AsUTF8(value);
	if (!source_name)
		return NULL;

	pakfire_package_set_string(self->package, PAKFIRE_PKG_SOURCE_NAME, source_name);
	Py_RETURN_NONE;
}

static PyObject* Package_get_source_evr(PackageObject* self) {
	const char* source_evr = pakfire_package_get_string(self->package, PAKFIRE_PKG_SOURCE_EVR);
	if (!source_evr)
		Py_RETURN_NONE;

	return PyUnicode_FromString(source_evr);
}

static PyObject* Package_set_source_evr(PackageObject* self, PyObject* value) {
	const char* source_evr = PyUnicode_AsUTF8(value);
	if (!source_evr)
		return NULL;

	pakfire_package_set_string(self->package, PAKFIRE_PKG_SOURCE_EVR, source_evr);
	Py_RETURN_NONE;
}

static PyObject* Package_get_source_arch(PackageObject* self) {
	const char* source_arch = pakfire_package_get_string(self->package, PAKFIRE_PKG_SOURCE_ARCH);
	if (!source_arch)
		Py_RETURN_NONE;

	return PyUnicode_FromString(source_arch);
}

static PyObject* Package_set_source_arch(PackageObject* self, PyObject* value) {
	const char* source_arch = PyUnicode_AsUTF8(value);
	if (!source_arch)
		return NULL;

	pakfire_package_set_string(self->package, PAKFIRE_PKG_SOURCE_ARCH, source_arch);
	Py_RETURN_NONE;
}

static struct PyMethodDef Package_methods[] = {
	{
		"dump",
		(PyCFunction)Package_dump,
		METH_VARARGS|METH_KEYWORDS,
		NULL,
	},
	{ NULL },
};

static struct PyGetSetDef Package_getsetters[] = {
	{
		"name",
		(getter)Package_get_name,
		NULL,
		NULL,
		NULL
	},
	{
		"evr",
		(getter)Package_get_evr,
		NULL,
		NULL,
		NULL
	},
	{
		"arch",
		(getter)Package_get_arch,
		NULL,
		NULL,
		NULL
	},
	{
		"uuid",
		(getter)Package_get_uuid,
		(setter)Package_set_uuid,
		NULL,
		NULL
	},
	{
		"digest",
		(getter)Package_get_digest,
		NULL,
		NULL,
		NULL,
	},
	{
		"summary",
		(getter)Package_get_summary,
		(setter)Package_set_summary,
		NULL,
		NULL
	},
	{
		"description",
		(getter)Package_get_description,
		(setter)Package_set_description,
		NULL,
		NULL
	},
	{
		"license",
		(getter)Package_get_license,
		(setter)Package_set_license,
		NULL,
		NULL
	},
	{
		"url",
		(getter)Package_get_url,
		(setter)Package_set_url,
		NULL,
		NULL
	},
	{
		"groups",
		(getter)Package_get_groups,
		(setter)Package_set_groups,
		NULL,
		NULL
	},
	{
		"vendor",
		(getter)Package_get_vendor,
		(setter)Package_set_vendor,
		NULL,
		NULL
	},
	{
		"distribution",
		(getter)Package_get_distribution,
		(setter)Package_set_distribution,
		NULL,
		NULL
	},
	{
		"packager",
		(getter)Package_get_packager,
		(setter)Package_set_packager,
		NULL,
		NULL
	},
	{
		"filename",
		(getter)Package_get_filename,
		(setter)Package_set_filename,
		NULL,
		NULL
	},
	{
		"downloadsize",
		(getter)Package_get_downloadsize,
		(setter)Package_set_downloadsize,
		NULL,
		NULL
	},
	{
		"installsize",
		(getter)Package_get_installsize,
		(setter)Package_set_installsize,
		NULL,
		NULL
	},
	{
		"size",
		(getter)Package_get_size,
		NULL,
		NULL,
		NULL
	},
	{
		"buildhost",
		(getter)Package_get_buildhost,
		(setter)Package_set_buildhost,
		NULL,
		NULL
	},
	{
		"buildtime",
		(getter)Package_get_buildtime,
		(setter)Package_set_buildtime,
		NULL,
		NULL
	},
	{
		"source_package",
		(getter)Package_get_source_package,
		NULL,
		NULL,
		NULL
	},
	{
		"source_name",
		(getter)Package_get_source_name,
		(setter)Package_set_source_name,
		NULL,
		NULL
	},
	{
		"source_evr",
		(getter)Package_get_source_evr,
		(setter)Package_set_source_evr,
		NULL,
		NULL
	},
	{
		"source_arch",
		(getter)Package_get_source_arch,
		(setter)Package_set_source_arch,
		NULL,
		NULL
	},

	// Dependencies
	{
		"provides",
		(getter)Package_get_provides,
		NULL,
		NULL,
		NULL
	},
	{
		"requires",
		(getter)Package_get_requires,
		NULL,
		NULL,
		NULL
	},
	{
		"obsoletes",
		(getter)Package_get_obsoletes,
		NULL,
		NULL,
		NULL
	},
	{
		"conflicts",
		(getter)Package_get_conflicts,
		NULL,
		NULL,
		NULL
	},
	{
		"recommends",
		(getter)Package_get_recommends,
		NULL,
		NULL,
		NULL
	},
	{
		"suggests",
		(getter)Package_get_suggests,
		NULL,
		NULL,
		NULL
	},
	{
		"reverse_requires",
		(getter)Package_get_reverse_requires,
		NULL,
		NULL,
		NULL
	},

	// Repository
	{
		"repo",
		(getter)Package_get_repo,
		NULL,
		NULL,
		NULL
	},
	{
		"path",
		(getter)Package_get_path,
		NULL,
		NULL,
		NULL
	},

	// Filelist
	{
		"filelist",
		(getter)Package_get_filelist,
		(setter)Package_set_filelist,
		NULL,
		NULL
	},

	{ NULL }
};

PyTypeObject PackageType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	tp_name:            "_pakfire.Package",
	tp_basicsize:       sizeof(PackageObject),
	tp_flags:           Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	tp_new:             Package_new,
	tp_dealloc:         (destructor)Package_dealloc,
	tp_init:            (initproc)Package_init,
	tp_doc:             "Package object",
	tp_methods:         Package_methods,
	tp_getset:          Package_getsetters,
	tp_hash:            (hashfunc)Package_hash,
	tp_repr:            (reprfunc)Package_repr,
	tp_str:             (reprfunc)Package_str,
	tp_richcompare:     (richcmpfunc)Package_richcompare,
};
