/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fnmatch.h>
#include <fts.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>

#include <archive.h>

#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/i18n.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/private.h>
#include <pakfire/progressbar.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_filelist_element {
	TAILQ_ENTRY(pakfire_filelist_element) nodes;

	struct pakfire_file* file;
};

struct pakfire_filelist {
	struct pakfire* pakfire;
	int nrefs;

	TAILQ_HEAD(entries, pakfire_filelist_element) files;
};

PAKFIRE_EXPORT int pakfire_filelist_create(struct pakfire_filelist** list, struct pakfire* pakfire) {
	struct pakfire_filelist* l = calloc(1, sizeof(*l));
	if (!l)
		return -ENOMEM;

	l->pakfire = pakfire_ref(pakfire);
	l->nrefs = 1;

	// Initialise files
	TAILQ_INIT(&l->files);

	*list = l;
	return 0;
}

static void pakfire_filelist_free(struct pakfire_filelist* list) {
	pakfire_filelist_clear(list);
	pakfire_unref(list->pakfire);
	free(list);
}

PAKFIRE_EXPORT struct pakfire_filelist* pakfire_filelist_ref(struct pakfire_filelist* list) {
	list->nrefs++;

	return list;
}

PAKFIRE_EXPORT struct pakfire_filelist* pakfire_filelist_unref(struct pakfire_filelist* list) {
	if (--list->nrefs > 0)
		return list;

	pakfire_filelist_free(list);
	return NULL;
}

PAKFIRE_EXPORT size_t pakfire_filelist_length(struct pakfire_filelist* list) {
	struct pakfire_filelist_element* element = NULL;
	size_t size = 0;

	TAILQ_FOREACH(element, &list->files, nodes)
		size++;

	return size;
}

size_t pakfire_filelist_total_size(struct pakfire_filelist* list) {
	struct pakfire_filelist_element* element = NULL;
	size_t size = 0;

	TAILQ_FOREACH(element, &list->files, nodes)
		size += pakfire_file_get_size(element->file);

	return size;
}

PAKFIRE_EXPORT int pakfire_filelist_is_empty(struct pakfire_filelist* list) {
	return TAILQ_EMPTY(&list->files);
}

PAKFIRE_EXPORT void pakfire_filelist_clear(struct pakfire_filelist* list) {
	struct pakfire_filelist_element* element = NULL;

	while (!TAILQ_EMPTY(&list->files)) {
		// Fetch the first element
		element = TAILQ_FIRST(&list->files);

		// Remove it from the list
		TAILQ_REMOVE(&list->files, element, nodes);

		// Dereference the file
		pakfire_file_unref(element->file);

		// Free it all
		free(element);
	}
}

PAKFIRE_EXPORT struct pakfire_file* pakfire_filelist_get(struct pakfire_filelist* list, size_t index) {
	struct pakfire_filelist_element* element = NULL;

	// Fetch the first element
	element = TAILQ_FIRST(&list->files);

	while (element && index--)
		element = TAILQ_NEXT(element, nodes);

	return pakfire_file_ref(element->file);
}

PAKFIRE_EXPORT int pakfire_filelist_add(struct pakfire_filelist* list, struct pakfire_file* file) {
	struct pakfire_filelist_element* element = NULL;
	struct pakfire_filelist_element* e = NULL;

	// Allocate a new element
	element = calloc(1, sizeof *element);
	if (!element) {
		ERROR(list->pakfire, "Could not allocate a new filelist element: %m\n");
		return 1;
	}

	// Reference the file
	element->file = pakfire_file_ref(file);

	// Fetch the last element
	e = TAILQ_LAST(&list->files, entries);

	// Skip all elements that are "greater than" the element we want to add
	while (e) {
		if (pakfire_file_cmp(e->file, file) <= 0)
			break;

		e = TAILQ_PREV(e, entries, nodes);
	}

	// If we found an element on the list, we will append after it,
	// otherwise we add right at the start.
	if (e)
		TAILQ_INSERT_AFTER(&list->files, e, element, nodes);
	else
		TAILQ_INSERT_HEAD(&list->files, element, nodes);

	return 0;
}

static int pakfire_filelist_remove(struct pakfire_filelist* list, struct pakfire_file* file) {
	struct pakfire_filelist_element* element = NULL;

	TAILQ_FOREACH(element, &list->files, nodes) {
		if (element->file == file) {
			// Remove the element from the list
			TAILQ_REMOVE(&list->files, element, nodes);

			// Free the element
			pakfire_file_unref(element->file);
			free(element);

			return 0;
		}
	}

	return 0;
}

static int __pakfire_filelist_remove_one(
		struct pakfire* pakfire, struct pakfire_file* file, void* data) {
	struct pakfire_filelist* list = (struct pakfire_filelist*)data;

	// Remove the file from the given filelist
	return pakfire_filelist_remove(list, file);
}

int pakfire_filelist_remove_all(
		struct pakfire_filelist* list, struct pakfire_filelist* removees) {
	return pakfire_filelist_walk(removees, __pakfire_filelist_remove_one, list, 0);
}

// Returns true if s contains globbing characters
static int is_glob(const char* s) {
	if (strchr(s, '*'))
		return 1;

	if (strchr(s, '?'))
		return 1;

	return 0;
}

static int pakfire_filelist_match_patterns(const char* path, const char** patterns) {
	int flags = 0;
	int r;

	for (const char** pattern = patterns; *pattern; pattern++) {
		// Match any subdirectories
		if (pakfire_string_startswith(path, *pattern))
			return 1;

		// Skip fnmatch if the pattern doesn't have any globbing characters
		if (!is_glob(*pattern))
			continue;

		// Reset flags
		flags = 0;

		/*
			fnmatch is way too eager for patterns line /usr/lib/lib*.so which will also match
			things like /usr/lib/python3.x/blah/libblubb.so.
			To prevent this for absolute file paths, we set the FNM_FILE_NAME flag so that
			asterisk (*) won't match any slashes (/).
		*/
		if (**pattern == '/')
			flags |= FNM_FILE_NAME;

		// Perform matching
		r = fnmatch(*pattern, path, flags);

		// Found a match
		if (r == 0)
			return 1;

		// No match found
		else if (r == FNM_NOMATCH)
			continue;

		// Any other error
		else
			return r;
	}

	// No match
	return 0;
}

int pakfire_filelist_scan(struct pakfire_filelist* list, const char* root,
		const char** includes, const char** excludes) {
	struct pakfire_file* file = NULL;
	struct archive_entry* entry = NULL;
	int r = 1;

	// Root must be absolute
	if (!pakfire_path_is_absolute(root)) {
		errno = EINVAL;
		return 1;
	}

	DEBUG(list->pakfire, "Scanning %s...\n", root);

	if (includes) {
		DEBUG(list->pakfire, "  Includes:\n");

		for (const char** include = includes; *include; include++)
			DEBUG(list->pakfire, "    %s\n", *include);
	}

	if (excludes) {
		DEBUG(list->pakfire, "  Excludes:\n");

		for (const char** exclude = excludes; *exclude; exclude++)
			DEBUG(list->pakfire, "    %s\n", *exclude);
	}

	struct archive* reader = pakfire_make_archive_disk_reader(list->pakfire, 1);
	if (!reader)
		return 1;

	// Allocate a new file entry
	entry = archive_entry_new();
	if (!entry)
		goto ERROR;

	char* paths[] = {
		(char*)root, NULL,
	};

	// Walk through the whole file system tree and find all matching files
	FTS* tree = fts_open(paths, FTS_NOCHDIR, 0);
	if (!tree)
		goto ERROR;

	FTSENT* node = NULL;
	const char* path = NULL;

	while ((node = fts_read(tree))) {
		// Ignore any directories in post order
		if (node->fts_info == FTS_DP)
			continue;

		// Compute the relative path
		path = pakfire_path_relpath(root, node->fts_path);
		if (!path || !*path)
			continue;

		// Skip excludes
		if (excludes && pakfire_filelist_match_patterns(path, excludes)) {
			DEBUG(list->pakfire, "Skipping %s...\n", path);

			r = fts_set(tree, node, FTS_SKIP);
			if (r)
				goto ERROR;

			continue;
		}

		// Skip what is not included
		if (includes && !pakfire_filelist_match_patterns(path, includes)) {
			DEBUG(list->pakfire, "Skipping %s...\n", path);

			// We do not mark the whole tree as to skip because some matches might
			// look for file extensions, etc.
			continue;
		}

		DEBUG(list->pakfire, "Processing %s...\n", path);

		// Reset the file entry
		entry = archive_entry_clear(entry);

		// Set path
		archive_entry_set_pathname(entry, path);

		// Set source path
		archive_entry_copy_sourcepath(entry, node->fts_path);

		// Read all file attributes from disk
		r = archive_read_disk_entry_from_file(reader, entry, -1, node->fts_statp);
		if (r) {
			ERROR(list->pakfire, "Could not read from %s: %m\n", node->fts_path);
			goto ERROR;
		}

		// Create file
		r = pakfire_file_create_from_archive_entry(&file, list->pakfire, entry);
		if (r)
			goto ERROR;

		// Append it to the list
		r = pakfire_filelist_add(list, file);
		if (r) {
			pakfire_file_unref(file);
			goto ERROR;
		}

		pakfire_file_unref(file);
	}

	// Success
	r = 0;

ERROR:
	if (entry)
		archive_entry_free(entry);
	archive_read_free(reader);

	return r;
}

int pakfire_filelist_contains(struct pakfire_filelist* list, const char* pattern) {
	struct pakfire_filelist_element* element = NULL;

	if (!pattern) {
		errno = EINVAL;
		return -1;
	}

	TAILQ_FOREACH(element, &list->files, nodes) {
		const char* path = pakfire_file_get_path(element->file);
		if (!path)
			return -1;

		int r = fnmatch(pattern, path, 0);
		if (r == 0)
			return 1;
	}

	return 0;
}

static int pakfire_filelist_progressbar_create(
		struct pakfire_progressbar** progressbar, const char* message) {
	struct pakfire_progressbar* p = NULL;
	int r;

	// Create the progressbar
	r = pakfire_progressbar_create(&p, NULL);
	if (r)
		goto ERROR;

	// Add message
	if (message) {
		r = pakfire_progressbar_add_string(p, "%s", message);
		if (r)
			goto ERROR;
	}

	// Add bar
	r = pakfire_progressbar_add_bar(p);
	if (r)
		goto ERROR;

	// Add percentage
	r = pakfire_progressbar_add_percentage(p);
	if (r)
		goto ERROR;

	// Add ETA
	r = pakfire_progressbar_add_eta(p);
	if (r)
		goto ERROR;

	*progressbar = p;

	return 0;

ERROR:
	if (p)
		pakfire_progressbar_unref(p);

	return r;
}

int pakfire_filelist_walk(struct pakfire_filelist* list,
		pakfire_filelist_walk_callback callback, void* data, int flags) {
	struct pakfire_progressbar* progressbar = NULL;
	struct pakfire_filelist_element* element = NULL;
	int r = 0;

	// Show a progressbar when iterating over the filelist
	if (flags & PAKFIRE_FILELIST_SHOW_PROGRESS) {
		r = pakfire_filelist_progressbar_create(&progressbar, "Processing Files...");
		if (r)
			goto ERROR;

		const size_t length = pakfire_filelist_length(list);

		// Start progressbar
		pakfire_progressbar_start(progressbar, length);
	}

	// Call the callback once for every element on the list
	TAILQ_FOREACH(element, &list->files, nodes) {
		// Increment the progressbar
		if (progressbar)
			pakfire_progressbar_increment(progressbar, 1);

		// Call the callback
		r = callback(list->pakfire, element->file, data);
		if (r)
			break;
	}

	// Done!
	if (progressbar)
		pakfire_progressbar_finish(progressbar);

ERROR:
	// Finish progress
	if (progressbar)
		pakfire_progressbar_unref(progressbar);

	return r;
}

static int __pakfire_filelist_dump(
		struct pakfire* pakfire, struct pakfire_file* file, void* data) {
	int* flags = (int*)data;

	char* s = pakfire_file_dump(file, *flags);
	if (s) {
		INFO(pakfire, "%s\n", s);
		free(s);
	}

	return 0;
}

int pakfire_filelist_dump(struct pakfire_filelist* list, int flags) {
	return pakfire_filelist_walk(list, __pakfire_filelist_dump, &flags, 0);
}

/*
	Verifies all files on the filelist
*/
int pakfire_filelist_verify(struct pakfire_filelist* list, struct pakfire_filelist* errors) {
	struct pakfire_filelist_element* element = NULL;
	struct pakfire_progressbar* progressbar = NULL;
	int status;
	int r;

	const size_t length = pakfire_filelist_length(list);

	DEBUG(list->pakfire, "Verifying filelist (%zu file(s))...\n", length);

	// Setup progressbar
	r = pakfire_progressbar_create(&progressbar, NULL);
	if (r)
		goto ERROR;

	// Add status
	r = pakfire_progressbar_add_string(progressbar, _("Verifying Files..."));
	if (r)
		goto ERROR;

	// Add bar
	r = pakfire_progressbar_add_bar(progressbar);
	if (r)
		goto ERROR;

	// Add percentage
	r = pakfire_progressbar_add_percentage(progressbar);
	if (r)
		goto ERROR;

	// Add ETA
	r = pakfire_progressbar_add_eta(progressbar);
	if (r)
		goto ERROR;

	// Start the progressbar
	r = pakfire_progressbar_start(progressbar, length);
	if (r)
		goto ERROR;

	// Iterate over the entire list
	TAILQ_FOREACH(element, &list->files, nodes) {
		// Verify the file
		r = pakfire_file_verify(element->file, &status);
		if (r)
			goto ERROR;

		// If the verification failed, we append it to the errors list
		if (status) {
			// Append the file to the error list
			r = pakfire_filelist_add(errors, element->file);
			if (r)
				goto ERROR;
		}

		// Increment progressbar
		r = pakfire_progressbar_increment(progressbar, 1);
		if (r)
			goto ERROR;
	}

	// Finish
	r = pakfire_progressbar_finish(progressbar);
	if (r)
		goto ERROR;

ERROR:
	if (progressbar)
		pakfire_progressbar_unref(progressbar);

	return r;
}

int pakfire_filelist_cleanup(struct pakfire_filelist* list, int flags) {
	struct pakfire_filelist_element* element = NULL;
	int r;

	// Nothing to do if the filelist is empty
	if (pakfire_filelist_is_empty(list))
		return 0;

	// Walk through the list backwards
	TAILQ_FOREACH_REVERSE(element, &list->files, entries, nodes) {
		r = pakfire_file_cleanup(element->file, flags);
		if (r)
			return r;
	}

	return 0;
}

static int __pakfire_filelist_matches_class(struct pakfire* pakfire,
		struct pakfire_file* file, void* p) {
	int* class = (int*)p;

	return pakfire_file_matches_class(file, *class);
}

/*
	Returns true if any file on the list matches class
*/
int pakfire_filelist_matches_class(struct pakfire_filelist* list, int class) {
	return pakfire_filelist_walk(list, __pakfire_filelist_matches_class, &class, 0);
}
