/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/linter.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/parser.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

static int pakfire_lint_makefile_name(struct pakfire* pakfire,
		const char* path, struct pakfire_package* pkg) {
	char basename[PATH_MAX];
	int r;

	// Fetch the package name
	const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME);

	// Check if the makefile ended on .nm
	if (!pakfire_string_endswith(path, ".nm")) {
		ERROR(pakfire, "Invalid extension for makefile: %s\n", path);
		return 1;
	}

	// Determine the basename of the file
	r = pakfire_string_set(basename, pakfire_basename(path));
	if (r)
		return r;

	// Remove the file extension
	r = pakfire_path_strip_extension(basename);
	if (r) {
		ERROR(pakfire, "Could not strip extension from makefile: %m\n");
		return r;
	}

	// Check if the package name matches
	if (strcmp(basename, name) != 0) {
		ERROR(pakfire, "%s: Makefile has a different package name (%s)\n",
			path, name);
		return 1;
	}

	return 0;
}

int pakfire_lint_makefile(struct pakfire* pakfire, struct pakfire_parser* makefile,
		const char* path, struct pakfire_package* pkg) {
	int r;

	// Check makefile name
	r = pakfire_lint_makefile_name(pakfire, path, pkg);
	if (r)
		return r;

	return 0;
}
