/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_TESTSUITE_H
#define PAKFIRE_TESTSUITE_H

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <pakfire/pakfire.h>

#define MAX_TESTS 128

#define TEST_SRC_PATH	ABS_TOP_SRCDIR "/tests/"

struct test {
	const char* name;
	int (*func)(const struct test* t);
	struct pakfire* pakfire;
};

struct testsuite {
	struct test tests[MAX_TESTS];
	size_t num;
};

extern struct testsuite ts;

int __testsuite_add_test(const char* name, int (*func)(const struct test* t));
int testsuite_run(int argc, const char* argv[]);

#define _LOG(prefix, fmt, ...) fprintf(stderr, "TESTS: " prefix fmt, ## __VA_ARGS__);
#define LOG(fmt, ...) _LOG("", fmt, ## __VA_ARGS__);
#define LOG_WARN(fmt, ...) _LOG("WARN: ", fmt, ## __VA_ARGS__);
#define LOG_ERROR(fmt, ...) _LOG("ERROR: ", fmt, ## __VA_ARGS__);

#define testsuite_add_test(func) __testsuite_add_test(#func, func)

static inline size_t testsuite_array_length(char* array[]) {
	size_t length = 0;

	for (char** line = array; *line; line++)
		length++;

	return length;
}

static inline int testsuite_compare_string_arrays(char* array1[], char* array2[]) {
	const size_t length1 = testsuite_array_length(array1);
	const size_t length2 = testsuite_array_length(array2);

	// Check if length matches
	if (length1 != length2) {
		LOG_ERROR("Arrays differ in length (%zu != %zu)\n", length1, length2);
		goto ERROR;
	}

	char** line1 = array1;
	char** line2 = array2;
	size_t num = 0;

	// Check if all lines match
	for (; *line1 && *line2; line1++, line2++, num++) {
		if (strcmp(*line1, *line2) != 0) {
			LOG_ERROR("Line %zu does not match\n", num);
			goto ERROR;
		}
	}

	// Success
	return 0;

ERROR:
	// Dump both arrays
	for (line1 = array1, line2 = array2, num = 0; *line1 && *line2; line1++, line2++, num++) {
		printf("Line %zu:\n");
		printf(" 1: %s", *line1);
		printf(" 2: %s", *line2);
	}

	return 1;
}

#define ASSERT(expr) \
	do { \
		if ((!(expr))) { \
			LOG_ERROR("Failed assertion: " #expr " %s:%d %s\n", \
				__FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_NULL(expr) \
	do { \
		if (!((expr) == NULL)) { \
			LOG_ERROR("Failed assertion: " #expr " == NULL %s:%d %s\n", \
				__FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_SUCCESS(expr) \
	do { \
		if ((expr)) { \
			LOG_ERROR("Failed assertion: %s (errno = %s) at %s:%d %s\n", \
				#expr, strerror(errno), __FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_FAILURE(expr) \
	do { \
		if (!(expr)) { \
			LOG_ERROR("Failed assertion: " #expr " unexpectedly didn't fail in %s:%d %s\n", \
				__FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_TRUE(expr) \
	do { \
		if (!(expr)) { \
			LOG_ERROR("Failed assertion: " #expr " unexpectedly didn't return true in %s:%d: %m\n", \
				__FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_FALSE(expr) \
	do { \
		if ((expr)) { \
			LOG_ERROR("Failed assertion: " #expr " unexpectedly didn't return false in %s:%d: %m\n", \
				__FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_ERRNO(expr, e) \
	do { \
		if (!(expr)) { \
			LOG_ERROR("Failed assertion: " #expr " unexpectedly didn't fail in %s:%d %s\n", \
				__FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
		if (errno != e) { \
			LOG_ERROR("Failed assertion: " #expr " failed with (%d - %s) " \
				"but was expected to fail with (%d - %s) in %s:%d\n", \
				errno, strerror(errno), e, strerror(e), __FILE__, __LINE__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_EQUALS(value1, value2) \
	do { \
		if (value1 != value2) { \
			LOG_ERROR("Failed assertion: " #value1 " (%lld) != " #value2 " (%lld) %s:%d %s\n", \
				value1, value2, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_STRING_EQUALS(value, string) \
	do { \
		if (!value) { \
			LOG_ERROR("Failed assertion: %s is NULL, expected %s at %s:%d %s\n", \
				#value, #string, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
		if (strcmp(string, value) != 0) { \
			LOG_ERROR("Failed assertion: " #value " (%s) != " #string " (%s) %s:%d %s\n", \
				value, string, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_STRING_ARRAY_EQUALS(array1, array2) \
	do { \
		if (testsuite_compare_string_arrays(array1, array2)) { \
			LOG_ERROR("Failed assertion: " #array1 " != " #array2 " %s:%d %s\n", \
				__FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while(0)

#define ASSERT_STRING_NOT_EQUALS(value1, value2) \
	do { \
		if (strcmp(value1, value2) == 0) { \
			LOG_ERROR("Failed assertion: " #value1 " (%s) != " #value2 " (%s) %s:%d %s\n", \
				value1, value2, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_STRING_STARTSWITH(string, start) \
	do { \
		if (strncmp(string, start, strlen(start)) != 0) { \
			LOG_ERROR("Failed assertion: " #string " does not start with " #start " %s:%d %s\n", \
				__FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

#define ASSERT_COMPARE(value1, value2, length) \
	do { \
		if (!value1) { \
			LOG_ERROR("Failed assertion: %s is NULL at %s:%d %s\n", \
				#value1, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
		if (!value2) { \
			LOG_ERROR("Failed assertion: %s is NULL at %s:%d %s\n", \
				#value2, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
		if (!length) { \
			LOG_ERROR("Failed assertion: %s is zero at %s:%d %s\n", \
				#length, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
		if (memcmp(value1, value2, length) != 0) { \
			LOG_ERROR("Failed assertion: %s != %s (length = %zu) at %s:%d %s\n", \
				#value1, #value2, length, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
			goto FAIL; \
		} \
	} while (0)

// Helper functions
FILE* test_mktemp(char** path);
char* test_mkdtemp(void);

#endif /* PAKFIRE_TESTSUITE_H */
