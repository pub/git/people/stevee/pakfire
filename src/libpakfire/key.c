/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <gpgme.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include <pakfire/constants.h>
#include <pakfire/i18n.h>
#include <pakfire/key.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/private.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_key {
	struct pakfire* pakfire;
	int nrefs;

	gpgme_key_t gpgkey;
};

int pakfire_key_create(struct pakfire_key** key, struct pakfire* pakfire, gpgme_key_t gpgkey) {
	if (!gpgkey) {
		errno = EINVAL;
		return 1;
	}

	// Allocate memory
	struct pakfire_key* k = calloc(1, sizeof(*k));
	if (!k)
		return 1;

	// Initialize pakfire and reference counter
	k->pakfire = pakfire_ref(pakfire);
	k->nrefs = 1;

	// Keep a reference to this key
	gpgme_key_ref(gpgkey);
	k->gpgkey = gpgkey;

	*key = k;
	return 0;
}

static int pakfire_key_extract_email(const char* uid, char** email) {
	if (!uid)
		return 1;

	// Find a start
	char* start = strrchr(uid, '<');
	if (!start)
		return 1;

	// Find the end
	char* end = strchr(start, '>');
	if (!end)
		return 1;

	// Copy email address to new memory
	int r = asprintf(email, "%.*s", (int)(end - start - 1), start + 1);
	if (r < 0)
		return 1;

	return 0;
}

static int __pakfire_key_fetch(gpgme_key_t* key, struct pakfire* pakfire,
		const char* what, gpgme_keylist_mode_t flags) {
	// Fetch GPGME context
	gpgme_ctx_t gpgctx = pakfire_get_gpgctx(pakfire);
	if (!gpgctx)
		return 1;

	int r = 1;

	// Fetch current keylist mode
	gpgme_keylist_mode_t mode = gpgme_get_keylist_mode(gpgctx);

	// Set keylist mode
	gpgme_error_t error = gpgme_set_keylist_mode(gpgctx, (mode|flags) & ~GPGME_KEYLIST_MODE_LOCAL);
	if (error != GPG_ERR_NO_ERROR) {
		ERROR(pakfire, "Could not set GPG keylist mode: %s\n",
			gpgme_strerror(error));
		goto ERROR;
	}

	// Fetch the key
	error = gpgme_get_key(gpgctx, what, key, 0);
	switch (gpg_err_code(error)) {
		case GPG_ERR_NO_ERROR:
		case GPG_ERR_EOF:
			break;

		default:
			ERROR(pakfire, "Could not fetch key %s: %s\n", what, gpgme_strerror(error));
			r = 1;
			goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (r && *key)
		gpgme_key_unref(*key);

	// Reset keylist mode
	gpgme_set_keylist_mode(gpgctx, mode);

	return r;
}

static int pakfire_key_fetch_from_wkd(gpgme_key_t* key, struct pakfire* pakfire, const char* email) {
	return __pakfire_key_fetch(key, pakfire, email, GPGME_KEYLIST_MODE_LOCATE);
}

static int pakfire_key_fetch_from_keyserver(gpgme_key_t* key, struct pakfire* pakfire, const char* fpr) {
	return __pakfire_key_fetch(key, pakfire, fpr, GPGME_KEYLIST_MODE_EXTERN);
}

PAKFIRE_EXPORT int pakfire_key_fetch(struct pakfire_key** key, struct pakfire* pakfire,
		const char* uid, const char* fingerprint) {
	// At least one (uid or fingerprint) must be set
	if (!uid && !fingerprint) {
		errno = EINVAL;
		return 1;
	}

	// Reset key
	*key = NULL;

	gpgme_key_t gpgkey = NULL;
	char* email = NULL;
	int r;

	// Extract email address from uid
	if (uid) {
		r = pakfire_key_extract_email(uid, &email);
		if (r)
			goto ERROR;
	}

	// Try importing the key using Web Key Directory
	if (email) {
		r = pakfire_key_fetch_from_wkd(&gpgkey, pakfire, email);
		if (r)
			goto ERROR;
	}

	// If nothing was found and we have a fingerprint, let's try a keyserver
	if (!gpgkey && fingerprint) {
		r = pakfire_key_fetch_from_keyserver(&gpgkey, pakfire, fingerprint);
		if (r)
			goto ERROR;
	}

	// Create a pakfire_key out of the gpg key object
	if (gpgkey) {
		r = pakfire_key_create(key, pakfire, gpgkey);
		if (r)
			goto ERROR;
	}

ERROR:
	if (gpgkey)
		gpgme_key_unref(gpgkey);
	if (email)
		free(email);

	return r;
}

static void pakfire_key_free(struct pakfire_key* key) {
	gpgme_key_unref(key->gpgkey);
	pakfire_unref(key->pakfire);
	free(key);
}

PAKFIRE_EXPORT struct pakfire_key* pakfire_key_ref(struct pakfire_key* key) {
	++key->nrefs;

	return key;
}

PAKFIRE_EXPORT void pakfire_key_unref(struct pakfire_key* key) {
	if (--key->nrefs > 0)
		return;

	pakfire_key_free(key);
}

static int pakfire_find_key(struct pakfire_key** key, struct pakfire* pakfire, const char* fingerprint) {
	if (!fingerprint) {
		errno = EINVAL;
		return 1;
	}

	// Reset key
	*key = NULL;

	// Fetch GPGME context
	gpgme_ctx_t gpgctx = pakfire_get_gpgctx(pakfire);
	if (!gpgctx)
		return 1;

	DEBUG(pakfire, "Seaching for key with fingerprint %s\n", fingerprint);

	gpgme_key_t gpgkey = NULL;
	int r;

	gpgme_error_t error = gpgme_get_key(gpgctx, fingerprint, &gpgkey, 0);
	switch (gpg_error(error)) {
		// Create a key object if we found something
		case GPG_ERR_NO_ERROR:
			r = pakfire_key_create(key, pakfire, gpgkey);
			gpgme_key_unref(gpgkey);
			if (r)
				return r;
			break;

		// Nothing found
		case GPG_ERR_EOF:
			break;
	}

	return 0;
}

PAKFIRE_EXPORT struct pakfire_key* pakfire_key_get(struct pakfire* pakfire, const char* fingerprint) {
	struct pakfire_key* key = NULL;

	int r = pakfire_find_key(&key, pakfire, fingerprint);
	if (r)
		return NULL;

	return key;
}

PAKFIRE_EXPORT int pakfire_key_delete(struct pakfire_key* key) {
	gpgme_ctx_t gpgctx = pakfire_get_gpgctx(key->pakfire);

	int r = 0;
	gpgme_error_t error = gpgme_op_delete(gpgctx, key->gpgkey, 1);
	if (error != GPG_ERR_NO_ERROR)
		r = 1;

	return r;
}

PAKFIRE_EXPORT const char* pakfire_key_get_fingerprint(struct pakfire_key* key) {
	return key->gpgkey->fpr;
}

PAKFIRE_EXPORT const char* pakfire_key_get_uid(struct pakfire_key* key) {
	if (key->gpgkey->uids)
		return key->gpgkey->uids->uid;

	return NULL;
}

PAKFIRE_EXPORT const char* pakfire_key_get_name(struct pakfire_key* key) {
	if (key->gpgkey->uids)
		return key->gpgkey->uids->name;

	return NULL;
}

PAKFIRE_EXPORT const char* pakfire_key_get_email(struct pakfire_key* key) {
	if (key->gpgkey->uids)
		return key->gpgkey->uids->email;

	return NULL;
}

PAKFIRE_EXPORT const char* pakfire_key_get_pubkey_algo(struct pakfire_key* key) {
	if (!key->gpgkey->subkeys)
		return NULL;

	switch (key->gpgkey->subkeys->pubkey_algo) {
		case GPGME_PK_RSA:
		case GPGME_PK_RSA_E:
		case GPGME_PK_RSA_S:
			return "RSA";

		case GPGME_PK_DSA:
			return "DSA";

		case GPGME_PK_ECDSA:
			return "ECDSA";

		case GPGME_PK_ECDH:
			return "ECDH";

		case GPGME_PK_ECC:
			return "ECC";

		case GPGME_PK_EDDSA:
			return "EDDSA";

		case GPGME_PK_ELG:
		case GPGME_PK_ELG_E:
			return "ELG";
	}

	return NULL;
}

PAKFIRE_EXPORT size_t pakfire_key_get_pubkey_length(struct pakfire_key* key) {
	if (key->gpgkey->subkeys)
		return key->gpgkey->subkeys->length;

	return 0;
}

PAKFIRE_EXPORT int pakfire_key_has_secret(struct pakfire_key* key) {
	if (key->gpgkey)
		return key->gpgkey->secret;

	return 0;
}

PAKFIRE_EXPORT time_t pakfire_key_get_created(struct pakfire_key* key) {
	if (key->gpgkey->subkeys)
		return key->gpgkey->subkeys->timestamp;

	return 0;
}

PAKFIRE_EXPORT time_t pakfire_key_get_expires(struct pakfire_key* key) {
	if (key->gpgkey->subkeys)
		return key->gpgkey->subkeys->expires;

	return 0;
}

PAKFIRE_EXPORT int pakfire_key_is_revoked(struct pakfire_key* key) {
	if (key->gpgkey->subkeys)
		return key->gpgkey->subkeys->revoked;

	return 0;
}

static int pakfire_key_write_to_keystore(struct pakfire_key* key) {
	// Fetch keystore path
	const char* keystore_path = pakfire_get_keystore_path(key->pakfire);
	if (!keystore_path)
		return 1;

	// Fetch fingerprint
	const char* fpr = pakfire_key_get_fingerprint(key);
	if (!fpr)
		return 1;

	char path[PATH_MAX];

	// Make path
	int r = pakfire_string_format(path, "%s/%s.key", keystore_path, fpr);
	if (r)
		return r;

	// Create parent directory
	r = pakfire_mkparentdir(path, 0700);
	if (r)
		return r;

	// Create file
	FILE* f = fopen(path, "w");
	if (!f) {
		ERROR(key->pakfire, "Could not open %s for writing: %m\n", path);
		return 1;
	}

	// Make files with secret keys non-world-readable
	if (pakfire_key_has_secret(key)) {
		r = chmod(path, 0600);
		if (r) {
			ERROR(key->pakfire, "Could not chmod %s: %m\n", path);
			fclose(f);
			return r;
		}
	}

	// Write key to file
	r = pakfire_key_export(key, f, 0);
	if (r) {
		ERROR(key->pakfire, "Could not export key %s: %m\n", fpr);
		unlink(path);
	}

	// Close file
	fclose(f);

	return r;
}

PAKFIRE_EXPORT int pakfire_key_generate(struct pakfire_key** key, struct pakfire* pakfire,
		const char* algo, const char* userid) {
	int r;

	// Check input
	if (!algo || !userid) {
		errno = EINVAL;
		return 1;
	}

	// Fetch GPGME context
	gpgme_ctx_t gpgctx = pakfire_get_gpgctx(pakfire);
	if (!gpgctx)
		return 1;

	const unsigned int flags =
		// Key should be able to be used to sign
		GPGME_CREATE_SIGN |
		// Don't set a password
		GPGME_CREATE_NOPASSWD |
		// The key should never expire
		GPGME_CREATE_NOEXPIRE;

	// Generate the key
	gpgme_error_t error = gpgme_op_createkey(gpgctx, userid,
		algo, 0, 0, NULL, flags);

	if (error != GPG_ERR_NO_ERROR) {
		switch (gpg_err_code(error)) {
			case GPG_ERR_USER_ID_EXISTS:
			case GPG_ERR_NAME_EXISTS:
				errno = EINVAL;
				break;

			default:
				break;
		}

		ERROR(pakfire, "%s\n", gpgme_strerror(error));
		return 1;
	}

	// Retrieve the result
	gpgme_genkey_result_t result = gpgme_op_genkey_result(gpgctx);

	// Retrieve the key by its fingerprint
	r = pakfire_find_key(key, pakfire, result->fpr);
	if (r)
		return r;

	// Store the key in the keystore
	return pakfire_key_write_to_keystore(*key);
}

static int pakfire_key_data(struct pakfire_key* key, char** buffer, size_t* length,
		const pakfire_key_export_mode_t mode) {
	char* output = NULL;

	gpgme_ctx_t gpgctx = pakfire_get_gpgctx(key->pakfire);
	if (!gpgctx)
		return 1;

	gpgme_export_mode_t gpgmode = 0;
	switch (mode) {
		case PAKFIRE_KEY_EXPORT_MODE_SECRET:
			gpgmode |= GPGME_EXPORT_MODE_SECRET;
			break;

		default:
			break;
	}

	const char* fingerprint = pakfire_key_get_fingerprint(key);

	DEBUG(key->pakfire, "Exporting key %s\n", fingerprint);

	gpgme_data_t data = NULL;
	int r = 1;

	// Initialize the buffer
	gpgme_error_t e = gpgme_data_new(&data);
	if (gpg_err_code(e) != GPG_ERR_NO_ERROR)
		goto ERROR;

	// Encode output as ASCII
	e = gpgme_data_set_encoding(data, GPGME_DATA_ENCODING_ARMOR);
	if (gpg_err_code(e) != GPG_ERR_NO_ERROR)
		goto ERROR;

	// Copy the key to the buffer
	e = gpgme_op_export(gpgctx, fingerprint, gpgmode, data);
	if (gpg_err_code(e) != GPG_ERR_NO_ERROR)
		goto ERROR;

	// Fetch data from buffer
	output = gpgme_data_release_and_get_mem(data, length);

	// Reset data so it won't be freed again
	data = NULL;

	// Allocate buffer
	*buffer = malloc(*length);
	if (!*buffer) {
		r = 1;
		goto ERROR;
	}

	// Copy the output buffer
	memcpy(*buffer, output, *length);

	// Success
	r = 0;

ERROR:
	if (output)
		gpgme_free(output);
	if (data)
		gpgme_data_release(data);

	return r;
}

PAKFIRE_EXPORT int pakfire_key_get_public_key(struct pakfire_key* key,
		char** buffer, size_t* length) {
	// Fetch the public key
	return pakfire_key_data(key, buffer, length, PAKFIRE_KEY_EXPORT_MODE_PUBLIC);
}

PAKFIRE_EXPORT int pakfire_key_get_secret_key(struct pakfire_key* key,
		char** buffer, size_t* length) {
	// Fetch the secret key
	return pakfire_key_data(key, buffer, length, PAKFIRE_KEY_EXPORT_MODE_SECRET);
}

PAKFIRE_EXPORT int pakfire_key_export(struct pakfire_key* key, FILE* f,
		pakfire_key_export_mode_t mode) {
	char* buffer = NULL;
	size_t length = 0;
	int r;

	// Check input
	if (!f) {
		errno = EINVAL;
		return 1;
	}

	r = pakfire_key_data(key, &buffer, &length, mode);
	if (r)
		return r;

	// Write key to file
	size_t bytes_written = fwrite(buffer, 1, length, f);
	if (bytes_written < length) {
		r = 1;
		goto ERROR;
	}

	// Flush f
	r = fflush(f);
	if (r)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (buffer)
		gpgme_free(buffer);

	return r;
}

PAKFIRE_EXPORT int pakfire_key_import(struct pakfire* pakfire, FILE* f,
		struct pakfire_key*** keys) {
	gpgme_data_t data;
	int r = 1;

	if (!f) {
		errno = EINVAL;
		return 1;
	}

	// Reset keys
	if (keys)
		*keys = NULL;

	// Fetch GPGME context
	gpgme_ctx_t gpgctx = pakfire_get_gpgctx(pakfire);
	if (!gpgctx)
		return 1;

	gpgme_import_result_t result = NULL;

	// Import key data
	gpgme_error_t e = gpgme_data_new_from_stream(&data, f);
	if (gpg_error(e) != GPG_ERR_NO_ERROR)
		goto ERROR;

	// Try importing the key(s)
	e = gpgme_op_import(gpgctx, data);

	switch (gpg_error(e)) {
		// Everything went fine
		case GPG_ERR_NO_ERROR:
			result = gpgme_op_import_result(gpgctx);

			// Keep the result
			gpgme_result_ref(result);

			// Did we import any keys?
			gpgme_import_status_t status = result->imports;
			if (!status) {
				errno = ENOENT;
				goto ERROR;
			}

			DEBUG(pakfire, "Keys considered   = %d\n", result->considered);
			DEBUG(pakfire, "Keys imported     = %d\n", result->imported);
			DEBUG(pakfire, "Keys not imported = %d\n", result->not_imported);

			if (keys) {
				// Allocate array
				*keys = calloc(result->imported + 1, sizeof(**keys));
				if (!*keys)
					goto ERROR;

				struct pakfire_key* key = NULL;

				// Retrieve all imported keys
				for (int i = 0; i < result->imported && status; i++, status = status->next) {
					DEBUG(pakfire, "Imported key %s - %s\n",
						status->fpr, gpgme_strerror(status->result));

					// Fetch the key by its fingerprint
					r = pakfire_find_key(&key, pakfire, status->fpr);
					if (r)
						goto ERROR;

					// Append to array
					(*keys)[i] = key;

					// Write key to keystore
					r = pakfire_key_write_to_keystore(key);
					if (r)
						goto ERROR;
				}
			}
			break;

		// Input was invalid
		case GPG_ERR_INV_VALUE:
			errno = EINVAL;
			break;

		// Fall through for any other errors
		default:
			ERROR(pakfire, "Failed with gpgme error: %s\n", gpgme_strerror(e));
			break;
	}

	// Success
	r = 0;

ERROR:
	if (result)
		gpgme_result_unref(result);
	gpgme_data_release(data);

	// Free keys on error
	if (r && keys && *keys) {
		for (struct pakfire_key** key = *keys; *key; key++)
			pakfire_key_unref(*key);
		free(*keys);

		*keys = NULL;
	}

	return r;
}

PAKFIRE_EXPORT char* pakfire_key_dump(struct pakfire_key* key) {
	char date[1024];
	char* s = NULL;
	int r;

	time_t created = pakfire_key_get_created(key);

	// Format creation time
	r = pakfire_strftime(date, "%Y-%m-%d", created);
	if (r)
		goto ERROR;

	asprintf(&s, "pub %s%zu/%s %s",
		pakfire_key_get_pubkey_algo(key),
		pakfire_key_get_pubkey_length(key),
		pakfire_key_get_fingerprint(key),
		date
	);

	const char* uid = pakfire_key_get_uid(key);
	if (uid) {
		asprintf(&s, "%s\n    %s", s, uid);
	}

	time_t expires = pakfire_key_get_expires(key);
	if (expires) {
		r = pakfire_strftime(date, "%Y-%m-%d", expires);
		if (r)
			goto ERROR;

		asprintf(&s, "%s\n    %s: %s", s, _("Expires"), date);
	}

	return s;

ERROR:
	if (s)
		free(s);

	return NULL;
}

int pakfire_key_sign(struct pakfire_key* key, const char* buffer, const size_t buffer_length,
		char** signature, size_t* signature_length, time_t* timestamp) {
	// Fetch GPGME context
	gpgme_ctx_t gpgctx = pakfire_get_gpgctx(key->pakfire);
	if (!gpgctx)
		return 1;

	// Remove any previous signers
	gpgme_signers_clear(gpgctx);

	gpgme_data_t data = NULL;
	gpgme_data_t sign = NULL;
	gpgme_error_t e;
	char* __signature = NULL;
	int r = 1;

	// Enable the key
	e = gpgme_signers_add(gpgctx, key->gpgkey);
	if (gpgme_err_code(e)) {
		ERROR(key->pakfire, "Could not select key for signing: %s\n",
			gpgme_strerror(e));
		goto ERROR;
	}

	// Initialize data buffer
	e = gpgme_data_new_from_mem(&data, buffer, buffer_length, 0);
	if (gpgme_err_code(e)) {
		ERROR(key->pakfire, "Could not initialize data buffer: %s\n",
			gpgme_strerror(e));
		goto ERROR;
	}

	// Initialize signature buffer
	e = gpgme_data_new(&sign);
	if (gpgme_err_code(e)) {
		ERROR(key->pakfire, "Could not initialize signature buffer: %s\n",
			gpgme_strerror(e));
		goto ERROR;
	}

	// Create the signature
	e = gpgme_op_sign(gpgctx, data, sign, GPGME_SIG_MODE_DETACH);
	switch (gpgme_err_code(e)) {
		// Everything went OK
		case GPG_ERR_NO_ERROR:
			break;

		default:
			ERROR(key->pakfire, "Could not sign: %s\n", gpgme_strerror(e));

			// Set errno to something useful
			errno = gpgme_err_code_to_errno(e);
			goto ERROR;
	}

	// Print some status details
	gpgme_sign_result_t result = gpgme_op_sign_result(gpgctx);
	if (result) {
		for (gpgme_new_signature_t s = result->signatures; s; s = s->next) {
			DEBUG(key->pakfire, "Signature created\n");
			DEBUG(key->pakfire, "  Key       : %s\n", s->fpr);
			DEBUG(key->pakfire, "  Algorithm : %s\n", gpgme_pubkey_algo_name(s->pubkey_algo));
			DEBUG(key->pakfire, "  Hash      : %s\n", gpgme_hash_algo_name(s->hash_algo));
			DEBUG(key->pakfire, "  Timestamp : %ld\n", s->timestamp);

			// Store timestamp
			if (timestamp)
				*timestamp = s->timestamp;
		}
	}

	// Extract the signature
	__signature = gpgme_data_release_and_get_mem(sign, signature_length);
	if (!__signature) {
		ERROR(key->pakfire, "The signature was unexpectedly empty: %m\n");
		goto ERROR;
	}

	// Sign is now released and we should not do this again below
	sign = NULL;

	// Because GPGME could be using its internal allocator, we need to copy the signature
	// and release the memory that we got from GPGME using gpgme_free().
	*signature = malloc(*signature_length);
	if (!*signature)
		goto ERROR;

	memcpy(*signature, __signature, *signature_length);

	// Success
	r = 0;

ERROR:
	if (__signature)
		gpgme_free(__signature);
	if (data)
		gpgme_data_release(data);
	if (sign)
		gpgme_data_release(sign);

	return r;
}
